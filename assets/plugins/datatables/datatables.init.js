$(document).ready(function () {
  // Add date column sorting
  $.fn.dataTable.moment( 'DD/MM/YYYY' );

  const datatable = $('.table-datatable').DataTable({
    searching: false,
    lengthChange: true,
    pagingType: 'numbers',
    "language": {
      "url": "/dist/plugins/datatables/French.json"
    },
    "pageLength": 30,
    "lengthMenu": [[20, 30, 50, 100, 250], [20, 30, 50, 100, 250]],
  });

  datatable.on('draw', function (e) {
    htmx.process(e.target);
  });

});

function initDataTableFromData(selector, data, columns) {
  $(selector).DataTable({
    data: data,
    columns: columns,
    searching: false,
    lengthChange: true,
    pagingType: 'numbers',
    "language": {
      "url": "/dist/plugins/datatables/French.json"
    },
    "pageLength": 30,
    "lengthMenu": [[20, 30, 50, 100, 250, -1], [20, 30, 50, 100, 250, "Tous"]],
  });
}
