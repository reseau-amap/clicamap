/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

function pageAmapDistributionAjout()
{
  if($('body.page_amap_distribution_ajout').length === 0) {
    return;
  }

  function hideFieldOnChange(e) {
    const $row = $('input[name="amap_distribution_ajout[period][endAt]"]').parents('.form-group');
    if(e.target.value === "REP_AUCUNE") {
      $row.hide();
    } else {
      $row.show();
    }
  }

  $('select[name="amap_distribution_ajout[repetition]"]').on('change', hideFieldOnChange).trigger('change');
}

function initFormUserEmails(id) {
  const addFormToCollection = (e) => {
    const collectionHolder = document.getElementById(id);

    const item = document.createElement('div');

    item.innerHTML = collectionHolder
      .dataset
      .prototype
      .replace(
        /__name__/g,
        collectionHolder.dataset.index
      );
    const child = item.firstChild;

    collectionHolder.appendChild(child);

    collectionHolder.dataset.index++;

    addItemFormDeleteLink(child)
  };
  const addItemFormDeleteLink = (item) => {
    const removeFormButton = document.createElement('button');
    removeFormButton.classList.add('btn');
    removeFormButton.classList.add('btn-danger');
    removeFormButton.setAttribute('title', 'Supprimer l\'adresse email')

    const removeIcon = document.createElement('span');
    removeIcon.classList.add('glyphicon');
    removeIcon.classList.add('glyphicon-trash');
    removeFormButton.appendChild(removeIcon);

    item.querySelector('.col-xs-11').after(removeFormButton);

    removeFormButton.addEventListener('click', (e) => {
      e.preventDefault();
      // remove the li for the tag form
      item.remove();
    });
  }

  // Init data index
  const collectionHolder = document.getElementById(id);
  const existingItemsCount = collectionHolder.children.length;
  collectionHolder.dataset.index = existingItemsCount;

  document
    .querySelector('#add_email')
    .addEventListener("click", addFormToCollection)
  ;
  document
    .querySelectorAll(`#${id} > .form-group`)
    .forEach((items) => {
      addItemFormDeleteLink(items)
    })
}

window.initSummerNote = () => {
  $('.summernote').summernote({
    inheritPlaceholder: true,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['table', ['table']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['insert', ['link']],
    ],
  });
  $('.summernote-lite').summernote({
    inheritPlaceholder: true,
    toolbar: [
      ['font', ['bold', 'italic', 'underline', 'clear']],
      ['para', ['paragraph']],
      ['fontsize', ['fontsize']],
      ['color', ['forecolor']],
      ['insert', ['link']],
    ],
  });
  $('.summernote-singleline').summernote({
    inheritPlaceholder: true,
    height: 25,
    minHeight: 25,
    maxHeight: 25,
    toolbar: [
      ['font', ['bold', 'underline', 'clear']],
    ]
  })
}

$(document).ready(function () {

  // form search
  $('form.js-form-search').on('change', 'select', function () {
    this.form.submit();
  })
  $('form.js-form-search input[type="checkbox"]').on('change', function () {
    this.form.submit();
  })

  // Js select all
  $('.js-tableselect-all').click(function () {
    if ($(this).prop('checked') === false) {
      $('.js-tableselect-row').prop('checked', false).trigger('change');
    } else {
      $('.js-tableselect-row').prop('checked', true).trigger('change');
    }
  })
  $('.js-tableselect-row').click(function () {
    $('.js-tableselect-all').prop('checked', false);
  })
  function updateTableSelectButtons() {
    if($('.js-tableselect-row:checked').length > 0) {
      $('.js-tableselect-button').removeAttr('disabled');
    } else {
      $('.js-tableselect-button').attr('disabled', true);
    }
  }
  updateTableSelectButtons();
  function tableSelectGetSelectedIds() {
    return Array.from($('.js-tableselect-row:checked').map((i, elt) => elt.value));
  }
  function updateTableSelectModalForm() {
    const form = document.querySelector('.js-tableselect-modal-form-update-action.modal form');
    if(form === null) {
      return;
    }

    const url = new URL(form.getAttribute('action'));
    const params = new URLSearchParams(url.search);
    params.set('selected', tableSelectGetSelectedIds().join(','));
    url.search = params.toString();
    form.setAttribute('action', url.toString());
  }
  updateTableSelectModalForm();
  function updateTableSelectLink() {
    const selectedIds = tableSelectGetSelectedIds();
    $('.js-tableselect-link').each((i, elt) => {
      if(selectedIds.length === 0) {
        elt.setAttribute('href', '#');
        return;
      }

      const url = new URL(elt.getAttribute('data-base-href'));
      const params = new URLSearchParams(url.search);
      params.set('selected', selectedIds.join(','));
      url.search = params.toString();
        elt.setAttribute('href', url.toString());
    });
  }
  updateTableSelectLink();

  $('.js-tableselect-row').change(function () {
    updateTableSelectButtons();
    updateTableSelectModalForm();
    updateTableSelectLink();
  });

  // Js bind
  $('input[type="checkbox"].js-binder').change(function () {
    var $this = $(this);
    var target = $this.attr('data-target-name');
    $('input[type="checkbox"][name="' + target + '"]').prop('checked', $this.prop('checked')).trigger('change');
  })

  // Dynamic disable
  $('.js-input-binded').on('change', function () {
    if($('.js-input-binded:checked').length > 0) {
      $('.js-dynamic-disabled').removeAttr('disabled');
    } else {
      $('.js-dynamic-disabled').attr('disabled', 'disabled');
    }
  });

  // Modal
  var submitValid = false;
  $('button.js-trigger-modal').click(function (e) {
    var $this = $(this);

    function getData(label, def) {
      var res = $this.attr(label);
      if (res === null) {
        return def;
      }
      return res;
    }

    if (submitValid) {
      submitValid = false;
      return;
    }

    e.preventDefault();
    bootbox.confirm({
      title: getData('data-title', null),
      message: getData('data-message', 'Message'),
      buttons: {
        confirm: {
          label: getData('data-confirm-label', 'Confirmer'),
          className: getData('data-confirm-class', null)
        },
        cancel: {
          label: getData('data-cancel-label', 'Annuler'),
          className: getData('data-cancel-class', null)
        }
      },
      locale: 'fr',
      callback: function (result) {
        if (result) {
          submitValid = true; // Use global submit valid to keep trace of submit button used
          $this.trigger('click');
          return;
        }
        submitValid = false;
      }
    })
  });

  $(".admin_cp_ville_autocomplete").autocomplete({
    source: '/ajax/get_cp_ville_suggestion',
    dataType: "json",
    minLength: 4,
    appendTo: "#container_admin"
  });

  // Init summernote
  window.initSummerNote();


  // Form amap creation and edition
  function hideOnFieldChange(e) {
    if(e.target.value === "1") {
      $('.js-hide-association').hide();
    } else {
      $('.js-hide-association').show();
    }
  }
  $('.js-form-amap input[name="amap_edition[associationDeFait]"]').on('change', hideOnFieldChange);
  $('.js-form-amap input[name="amap_edition[associationDeFait]"][checked]').trigger('change');

  $('.js-form-amap input[name="amap_creation[associationDeFait]"]').on('change', hideOnFieldChange);
  $('.js-form-amap input[name="amap_creation[associationDeFait]"][checked]').trigger('change');

  // Init jquery mask
  $('.js-input-mask-phone').mask('00.00.00.00.00');

  // Init select 2
  $('.js-select2').select2();

  // Init flatpickr
  if(typeof flatpickr !== "undefined") {
    flatpickr(".flatpickr_range", {
      mode: "range",
      locale: "fr",
      dateFormat: "d/m/Y",
      // Auto submit on search form
      onClose: function(selectedDates, dateStr, instance) {
        const $parentSearchForm = $(instance.input).parents('.js-form-search');
        if($parentSearchForm.length === 0) {
          return;
        }

        $parentSearchForm.submit();
      },
    });
  }

  // Init Form autocomplete
  $('.js-autocomplete-single').each((i, elt) => {
    console.log(elt);
    const target = elt.getAttribute('data-autocomplete-url');

    $(elt).autocomplete({
      source: target,
      dataType: "json",
      minLength: 4,
      // Autosubmit search form on click
      select: function(e, ui) {
        e.target.value = ui.item.value;
        const searchFrom = $(e.target).parents('form.js-form-search');
        if(searchFrom.length > 0) {
            searchFrom.submit();
        }
      }
    });
  });


  pageAmapDistributionAjout();
});

function initFormUsernameAutoFill(usernameField, firstnameField, lastnameField) {
  let disabled = usernameField.value !== "";
  function onChange() {
    if(disabled) {
      return;
    }

    const formData = new FormData();
    formData.append('firstName', firstnameField.value);
    formData.append('lastName', lastnameField.value);

    fetch('/ajax/username_generate', {
      'method': 'POST',
      'body': formData
    }).then((response) => {
      return response.json();
    })
      .then((data) => {
        usernameField.value = data.username;
      });
  }

  const onChangeDebounced = _.debounce(onChange, 250);

    $(firstnameField).on('input', onChangeDebounced);
    $(lastnameField).on('input', onChangeDebounced);
    $(usernameField).on('input', () => {
      disabled = true;
    });
}


function initDataToggle() {
  $('[data-toggle="jqshow"]').on('click', function (e) {
    const elt = document.querySelector(e.target.getAttribute('data-target'));
    if(elt === null) {
      return;
    }
    $(elt).show(500);
  });
}
$(document).ready(initDataToggle);


function initFormUploadField() {
  $('.form-control-file').on('change', function (e) {
    const target = e.target;
    const filename = e.target.files[0].name;
    $(target).parents('.input-group').find('.form-control').val(filename);
  });
}
$(document).ready(initFormUploadField);

$(document).ready(function () {
  const modal = $('#modal_actualite');

    if(modal.length === 0) {
        return;
    }

    modal.modal('show');

  const buttonAll = modal.find('button.btn-default');
  const buttonSuccess = modal.find('button.btn-success');

  function disableButtons() {
    buttonSuccess.attr('disabled', 'disabled');
    buttonAll.attr('disabled', 'disabled');
  }
    buttonSuccess.on('click', function (e) {
      disableButtons();
      const target = buttonSuccess.attr('data-target');
      fetch(target, {
        'method': 'POST',
      }).then(() => {
        modal.modal('hide')
      })
    });

  buttonAll.on('click', function (e) {
    disableButtons();
    const target = buttonAll.attr('data-target');
    fetch(target, {
      'method': 'POST',
    }).then(() => {
       window.location.href = buttonAll.attr('data-redirect');
    })
  });
});


$(document).on('click', '#order-form-container .btn-copy', function (e) {
  e.preventDefault();

  let i = 3;
  while (true) {
    const inputs = document.querySelectorAll(`.table-simulation-container tbody tr.input-row td:nth-child(${i}) input.simulation-input:not([readonly])`);
    if (inputs.length === 0) {
      break;
    }

    const firstInput = inputs[0];
    for (const input of inputs) {
      input.value = firstInput.value;
    }

    i++;
  }

  htmx.trigger(document.querySelector('.table-simulation-container .simulation-input'), 'keyup')
});