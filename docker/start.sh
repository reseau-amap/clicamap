#!/usr/bin/env bash

chmod -R 777 /var/www/html/application/writable
chmod -R 777 /var/www/html/application/logs
chmod -R 777 /var/www/html/public/uploads

composer install

# load DB
while ! echo exit | nc mariadb 3306; do sleep 1; done # Wait for server initialized
docker/fixtures.sh

# Init cdp jwt keys
docker/jwt.sh

# Init grunt
node_modules/.bin/grunt

# Default entrypoint
apache2-foreground
