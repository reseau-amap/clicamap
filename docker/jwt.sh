#!/usr/bin/env bash

set -xe

# Generate JWT keys
mkdir -p application/writable/keys/
openssl genrsa -out application/writable/keys/jwtCdp.key 2048
openssl rsa -in application/writable/keys/jwtCdp.key -pubout -out  application/writable/keys/jwtCdp.pub
