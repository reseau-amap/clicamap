FROM php:8.1-apache AS production

# Use the default dev configuration
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Composer install
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

# Dependencies
RUN apt-get update && apt-get install -y \
			mariadb-client unzip netcat-openbsd sudo git rsync \
			libfreetype6-dev libjpeg62-turbo-dev libpng-dev libzip-dev libicu-dev \
			nodejs npm libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb &&\
        docker-php-ext-configure opcache --enable-opcache && \
	 	docker-php-ext-install -j "$(nproc)" gd mysqli pdo_mysql intl zip calendar opcache bcmath && \
        apt clean && rm -rf /var/lib/apt/lists/*

COPY ./upload.ini /usr/local/etc/php/conf.d/upload.ini
COPY ./memory.ini /usr/local/etc/php/conf.d/memory.ini
COPY ./execution_time.ini /usr/local/etc/php/conf.d/execution_time.ini
COPY ./opcache_prod.ini /usr/local/etc/php/conf.d/opcache.ini

# Apache configuration
RUN a2enmod rewrite
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

ENTRYPOINT ["docker-php-entrypoint", "/var/www/html/docker/start.sh"]

FROM production AS dev
RUN pecl install xdebug-3.2.0 && docker-php-ext-enable xdebug
COPY ./xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY ./opcache_dev.ini /usr/local/etc/php/conf.d/opcache.ini
#
#RUN git clone https://github.com/NoiseByNorthwest/php-spx.git &&\
#    cd php-spx &&\
#    git checkout release/latest&&\
#    phpize &&\
#    ./configure &&\
#    make &&\
#    make install
#COPY ./spx.ini /usr/local/etc/php/conf.d/spx.ini
