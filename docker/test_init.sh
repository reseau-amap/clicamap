#!/usr/bin/env bash

chmod -R 777 $CI_PROJECT_DIR

composer install

# Launch apache
apache2-foreground > /dev/null &
