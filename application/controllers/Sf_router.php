<?php

declare(strict_types=1);

use DI\Attribute\Inject;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;

class Sf_router extends AppController
{
    #[Inject]
    private readonly UrlMatcher $matcher;

    /**
     * On 404 error, fallback to symfony routing system
     * Support argument autowiring injection on methods
     * Expect method controller return a Symfony response.
     */
    public function index(): void
    {
        try {
            $pathInfo = $this->request->getPathInfo();
            $pathInfo = rtrim($pathInfo, '/');
            $attributes = $this->matcher->match($pathInfo);
        } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
            $this->logger->info('Path not found');
            show_404();

            return;
        }

        $controller = $this
            ->container
            ->get($attributes['_controller_class'])
        ;
        $methodAttributes = array_map(function (ReflectionParameter $parameter) use ($attributes) {
            $paramType = $parameter->getType();
            if (null === $paramType) {
                throw new \InvalidArgumentException('Missing parameter type '.$parameter->getName());
            }
            if (\Symfony\Component\HttpFoundation\Request::class === $paramType) {
                return $this->request;
            }
            if ($paramType->isBuiltin()) {
                $param = $this->extractQueryParam($attributes, $parameter);
                if (null === $param) {
                    if ($paramType->allowsNull()) {
                        return null;
                    }
                    throw new \InvalidArgumentException('Missing parameter '.$parameter->getName());
                }

                if ('int' === $paramType->getName()) {
                    return (int) $param;
                }
                if ('string' === $paramType->getName()) {
                    return $param;
                }

                throw new \InvalidArgumentException('Invalid parameter type'.$paramType->getName());
            }

            return $this->container->get($paramType->getName());
        }, $attributes['_controller_method_parameters']);

        $response = $controller->{$attributes['_controller_method']}(...$methodAttributes);
        if (null === $response) {
            $response = new Response();
        }

        if ($response instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse) {
            $response->prepare($this->request);
            $response->send();

            return;
        }
        if ($response instanceof \Symfony\Component\HttpFoundation\Response) {
            $response->sendHeaders();
            echo $response->getContent(); // Use echo to keep the calls to post controller hooks

            return;
        }

        throw new \InvalidArgumentException('Invalid response type');
    }

    private function extractQueryParam(array $attributes, ReflectionParameter $param): ?string
    {
        $paramName = $param->getName();
        if (!isset($attributes[$paramName])) {
            return null;
        }

        return $attributes[$paramName];
    }
}
