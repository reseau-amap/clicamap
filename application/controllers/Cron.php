<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Carbon\Carbon;
use DI\Attribute\Inject;
use PsrLib\Services\AmapDistributionNotificationSender;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cron extends AppController
{
    #[Inject]
    public \PsrLib\Services\MPdfGeneration $mpdfgeneration;

    protected $data;

    #[Inject]
    private AmapDistributionNotificationSender $amapDistributionNotificationSender;

    #[Inject]
    private \PsrLib\Services\ModeleContratDelaiNotificationSender $modeleContratDelaiNotificationSender;

    #[Inject]
    private \PsrLib\Services\Email_Sender $emailSender;

    /**
     * Cron constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->data = [];
        $this->load->helper('array');
        $this->load->helper('email');
        $this->load->helper('string');

        $this->load->library('email');
    }

    public function index(): void
    {
        $dateSet = $this->setTestDateFromRequest();
        if (!(PHP_SAPI === 'cli' || true === $dateSet)) {
            $this->exit_error();
        }

        $this->cron_distribution_amapien_rappel();
        $this->cron_distribution_amapien_relance();
        $this->cron_distribution_amap_fin();
        $this->cron_paysan_alerte();

        // Healthcheck check
        if (isset($_ENV['HEALTHCHECK_ENDPOINT'])) {
            $client = new \GuzzleHttp\Client();
            $client->get($_ENV['HEALTHCHECK_ENDPOINT']);
        }
    }

    /**
     * ALERTE PAYSAN EN FONCTION DU DÉLAI DE LA FERME.
     */
    private function cron_paysan_alerte(): void
    {
        echo 'Envoi des alertes paysan : ';

        $this->modeleContratDelaiNotificationSender->mcDelai0();

        echo 'ok'.PHP_EOL;
    }

    private function setTestDateFromRequest(): bool
    {
        if ('true' !== getenv('CRON_ALLOW_FORCE_DATE')) {
            return false;
        }

        $timestamp = $this->request->get('timestamp');
        if (null === $timestamp) {
            return true;
        }

        \Carbon\Carbon::setTestNow(Carbon::createFromTimestamp($timestamp));

        return true;
    }

    //  TÂCHES CRON : ACTIVÉES À 1H DU MATIN ---------------------------------------

    private function cron_distribution_amapien_rappel(): void
    {
        echo 'Envoi du rappel distribution aux amapiens : ';
        $this->amapDistributionNotificationSender->amapienRappels();
        echo 'ok'.PHP_EOL;
    }

    private function cron_distribution_amapien_relance(): void
    {
        echo 'Envoi de la relance distribution aux amapiens : ';
        $this->amapDistributionNotificationSender->amapRelance();
        echo 'ok'.PHP_EOL;
    }

    private function cron_distribution_amap_fin(): void
    {
        echo 'Envoi de l\'alerte de fin des distributions pour les amaps : ';
        $this->amapDistributionNotificationSender->amapAlerteFin();
        echo 'ok'.PHP_EOL;
    }
}
