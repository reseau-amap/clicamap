<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Sentry\Integration\RequestIntegration;

function addUserInfoToScope(Sentry\State\Scope $scope): void
{
    $userInfo = [];

    if (!isset($_SESSION) || !is_array($_SESSION)) {
        return;
    }

    $session = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
        ->get(Symfony\Component\HttpFoundation\Session\Session::class)
    ;

    if ($session->has('statut')) {
        $userInfo['statut'] = $session->get('statut');
    }
    if ($session->has('ID')) {
        $userInfo['id'] = $session->get('ID');
    }

    $scope->setUser($userInfo);
}

function sentryCapture($e): void
{
    // Configure sentry
    Sentry\configureScope(function (Sentry\State\Scope $scope): void {
        $scope->setLevel(Sentry\Severity::fatal());

        addUserInfoToScope($scope);
    });

    // Capture exception on sentry
    Sentry\captureException($e);
}

function initSentry(): void
{
    Sentry\init([
        'dsn' => $_ENV['SENTRY_DSN'],
        'environment' => $_ENV['SENTRY_ENV'],
        'default_integrations' => false, // Disable default integration to avoid CodeIgniter core functions override
        'integrations' => [new RequestIntegration()], // Add request informations
        'error_types' => E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED,
    ]);

    // Replace default exception handler with custom one to pass exception to sentry
    set_exception_handler(function ($e): void {
        // Process here permission error exception
        // Quite ugly but as we do not want to pass them on sentry this is the easier way
        if ($e instanceof \PsrLib\Exception\PermissionErrorException) {
            redirect('/');

            return;
        }

        sentryCapture($e);

        // Call default exception handler
        _exception_handler($e);
    });
}
