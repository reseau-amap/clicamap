<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class DateMock
{
    public const MOCK_DATE_FILE = '/.date.override';

    /**
     * Mock date.
     * Used for functional testing.
     */
    public function mockDate(): void
    {
        $mockFilePath = \PsrLib\ProjectLocation::PROJECT_ROOT.self::MOCK_DATE_FILE;
        $fs = new Symfony\Component\Filesystem\Filesystem();
        if (!$fs->exists($mockFilePath)) {
            return;
        }

        $date = file_get_contents($mockFilePath);
        if ('' === $date) {
            return;
        }
        \Carbon\Carbon::setTestNow(\Carbon\Carbon::parse($date));
        \Carbon\CarbonImmutable::setTestNow(\Carbon\CarbonImmutable::parse($date));
    }
}
