<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * As codeigniter templates does not filter XSS on print, we do it on input.
 * Better than nothing but not perfect security....
 */
class CleanInput
{
    /**
     * Whitelist clean input for given paths
     * Use only for enpoints migrated with Symfony form management
     * And Twig templates.
     */
    public const PATH_WHITELIST = [
        '/campagne',
        '/actualites/form',
        '/evenement/add_2',
        '/evenement/edit',
    ];

    /**
     * @var \PsrLib\Services\Purifier
     */
    private $purifier;

    public function __construct()
    {
        $this->purifier = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\PsrLib\Services\Purifier::class)
        ;
    }

    public function cleanAllInput(): void
    {
        if ($this->isCurrentPathWhitelisted()) {
            return;
        }
        if (is_array($_POST)) {
            $this->cleanArray($_POST);
        }

        if (is_array($_GET)) {
            $this->cleanArray($_GET);
        }
    }

    private function cleanArray(array &$array): void
    {
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                $this->cleanArray($array[$key]);
            } else {
                $purified = $this->purifier->purify($item);
                // Restore &amp; to & to avoid display error on twig templates
                $purified = str_replace('&amp;', '&', $purified);
                $array[$key] = $purified;
            }
        }
    }

    private function isCurrentPathWhitelisted()
    {
        $currentPath = $_SERVER['REQUEST_URI'] ?? null;
        if (null === $currentPath) {
            return false;
        }

        foreach (self::PATH_WHITELIST as $path) {
            if (str_starts_with($currentPath, $path)) {
                return true;
            }
        }

        return false;
    }
}
