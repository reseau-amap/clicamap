<i>Ceci est un message automatique. Merci de ne pas y répondre.</i><br />
Nous vous confirmons l'envoi du message suivant par le formulaire de contact Clic'AMAP<br />
<b>Nom : </b> {nom} <br />
<b>Prénom : </b> {prenom} <br />
<b>Email : </b> {email} <br />
<b>Code postal : </b> {cp} <br />
<b>Message : </b> <br />
{message}
