<?php

declare(strict_types=1);

use Psr\Log\LoggerInterface;

function is_url_valid_codeigniter($uri = ''): bool
{
    if ('/' === $uri) {
        return true;
    }
    if (file_exists(\PsrLib\ProjectLocation::PROJECT_ROOT.'/public/'.$uri)) {
        return true;
    }
    $uriSplited = explode(
        '/',
        explode(
            '?',
            trim($uri, '/')
        )[0]
    );

    $class = ucfirst($uriSplited[0] ?? '');
    $method = $uriSplited[1] ?? '';
    if ('' === $method) {
        $method = 'index';
    }

    return method_exists($class, $method);
}

function is_url_valid_sf_routing($uri = ''): bool
{
    $path = parse_url($uri, PHP_URL_PATH);
    if (!is_string($path)) {
        return false;
    }
    try {
        \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Symfony\Component\Routing\Matcher\UrlMatcher::class)
            ->match($path)
        ;
    } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
        return false;
    } catch (\Symfony\Component\Routing\Exception\MethodNotAllowedException $e) {
        return true;
    }

    return true;
}

function is_url_valid($uri = ''): bool
{
    return is_url_valid_codeigniter($uri) || is_url_valid_sf_routing($uri);
}

/**
 * Replace base site url helper to add a test on target existence.
 * Does not support full codeigniter routing, only the one used in this project.
 * Does not support method parameter validation.
 *
 * @param mixed      $uri
 * @param mixed|null $protocol
 */
function site_url($uri = '', $protocol = null)
{
    // Override default behaviour. Keep full urls
    if (1 === preg_match('/^http(s?):\/\/.*/', $uri)) {
        return $uri;
    }

    if (!is_url_valid($uri)) {
        \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(LoggerInterface::class)
            ->error(sprintf('Invalid route : %s', $uri))
        ;
//        if ('production' !== getenv('CI_ENV')) {
//            throw new \PsrLib\Exception\InvalidRouteException($uri);
//        }
    }

    return get_instance()->config->site_url($uri, $protocol);
}
