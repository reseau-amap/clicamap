<?php

declare(strict_types=1);

if (!function_exists('bouton_action')) {
    /**
     * @param string|null $name  Nom du bouton
     * @param string[]    $items ["Condition d'affichage", "URL de l'action", "Icône de l'action", "Description courte de l'action"],
     *
     * @return string
     */
    function bouton_action(string $name = null, $items = [], bool $disabled = false)
    {
        $request = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()
            ->get(\Symfony\Component\HttpFoundation\Request::class)
        ;

        $a = 2;

        $res = sprintf('
<!-- Dropdown button : actions -->
<div class="btn-group btn-group-action" role="group">
  <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Cliquer pour voir les actions possibles"  %s>
    <div class="pull-left"><span class="glyphicon glyphicon-option-vertical"></span><span class="text-muted text-normal">%s</span></div>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">', $disabled ? 'disabled' : '', $name);
        foreach ($items as $key => $item) {
            // item = [condition, url, icon, text]
            if ($item[0]) {
                if ('#' == $item[1][0]) {
                    $href = sprintf('href="#" data-toggle="modal" data-target="%s"', $item[1]);
                } else {
                    if (preg_match('/^https?:/', $item[1]) && !str_starts_with($item[1], $request->getSchemeAndHttpHost())) {
                        $href = sprintf('href="%s" target="_blank"', $item[1]);
                    } else {
                        $href = sprintf('href="%s"', site_url($item[1]));
                    }
                }
                if ('disabled' == $item[1]) {
                    $href = '';
                }
                $res .= sprintf('
    <li><a %s><span class="glyphicon glyphicon-%s">&nbsp;</span>%s</a></li>', $href, $item[2], $item[3]);
            }
        }
        $res .= sprintf('
  </ul>
</div>
<!-- Dropdown button -->');

        return $res;
    }
}

if (!function_exists('modal_oui_non')) {
    /**
     * @param string|null $id       Identifiant du modal
     * @param string|null $question Question à afficher (sans le '?')
     * @param string|null $url      URL utilisé si réponse='Oui'
     * @param string|null $alert    Message optionnel d'alerte
     *
     * @return string Contenu de la modal
     */
    function modal_oui_non($id, string $question = null, string $url = null, string $alert = null)
    {
        $res = sprintf('
<!-- Modal : %s -->
<div class="modal fade text-left" id="%s" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title" id="myModalLabel">%s ?</h5>
      </div>
      <div class="modal-body">
        <form method="POST" action="%s">', $id, $id, $question, site_url($url));
        if ($alert) {
            $res .= sprintf('
        <div class="alert alert-danger">%s</div>', $alert);
        }
        $res .= sprintf('
        <p class="text-right">
          <a href="#" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>&nbsp;NON</a>
          <button type="submit" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-ok"></span>&nbsp;OUI</button>
        </p>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->');

        return $res;
    }
}
