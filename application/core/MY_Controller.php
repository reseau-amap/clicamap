<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use DI\Attribute\Inject;
use PsrLib\Services\PhpDiContrainerSingleton;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Class AppController.
 */
class AppController extends CI_Controller
{
    use \PsrLib\Controller\AbstractControllerTrait {
        exit_error as protected exit_error_sf;
        redirect as protected redirect_sf;
        redirectToRoute as protected redirectToRoute_sf;
    }

    public Appfunction $appfunction;
    public MY_Form_validation $form_validation;
    public CI_Session $session;
    public CI_Input $input;
    public Redirect $redirect;
    public CI_URI  $uri;

    protected Di\Container $container;

    /**
     * AppController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->container = PhpDiContrainerSingleton::getContainer();

        // The framework doesn't let us control how the controller is created, so
        // we can't use the container to create the controller
        // So we ask the container to inject dependencies
        $this->container->injectOn($this);

        $this->load->library('appfunction');
        $this->load->library('form_validation');
        $this->load->library('Redirect');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    protected function redirect(string $url, int $status = 302): void
    {
        redirect($url, 'auto', $status);
    }

    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): void
    {
        redirect($this->generateUrl($route, $parameters), 'auto', $status);
    }

    protected function twigDisplay(string $template, array $data = []): void
    {
        $rendered = $this->render($template, $data);
        echo $rendered->getContent();
    }

    /**
     * Load given view with template.
     */
    protected function loadViewWithTemplate(string $view, array $data = [], string $template = 'template', array $templateRenderOptions = []): void
    {
        $page = $this->load->view($view, $data, true);

        $options = array_merge(
            ['page' => $page],
            $templateRenderOptions
        );
        $this->load->view($template, $options);
    }
}
