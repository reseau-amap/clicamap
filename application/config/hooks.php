<?php

declare(strict_types=1);

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'][] = [
    'class' => 'LogRequestStart',
    'function' => 'logRequest',
    'filename' => 'LogRequestStart.php',
    'filepath' => 'hooks',
];
$hook['post_system'][] = [
    'class' => 'LogRequestEnd',
    'function' => 'logRequest',
    'filename' => 'LogRequestEnd.php',
    'filepath' => 'hooks',
];
$hook['pre_system'][] = [
    'function' => 'initSentry',
    'filename' => 'sentry.php',
    'filepath' => 'hooks',
];

$hook['pre_system'][] = [
    'class' => 'CleanInput',
    'function' => 'cleanAllInput',
    'filename' => 'CleanInput.php',
    'filepath' => 'hooks',
];

$hook['pre_system'][] = [
    'class' => 'TimeZone',
    'function' => 'setDefaultTimezone',
    'filename' => 'TimeZone.php',
    'filepath' => 'hooks',
];

$hook['pre_system'][] = [
    'class' => 'DateMock',
    'function' => 'mockDate',
    'filename' => 'DateMock.php',
    'filepath' => 'hooks',
];

$hook['pre_controller'][] = [
    'class' => 'MaintenanceHook',
    'function' => 'redirectOnMaintenanceEnabled',
    'filename' => 'MaintenanceHook.php',
    'filepath' => 'hooks',
];
