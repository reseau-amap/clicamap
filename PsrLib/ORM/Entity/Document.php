<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\DocumentAnonymeMax;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Auditable]
#[ORM\Table(name: 'ak_document')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\DocumentRepository::class)]
#[DocumentAnonymeMax]
class Document
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: 'nom', type: 'string', length: 255, nullable: false)]
    #[Assert\NotBlank(message: 'Le champ "Nom" est requis.')]
    #[Assert\Length(max: 200)]
    private string $nom = '';

    #[ORM\Column(name: 'lien', type: 'string', length: 255, nullable: false)]
    #[Assert\NotBlank(message: 'Le champ "Lien" est requis.')]
    #[Assert\Length(max: 200)]
    #[Assert\Url(message: 'Le champ "Lien" doit contenir une URL valide.')]
    private string $lien = '';

    #[ORM\Column(name: 'permission_anonyme', type: 'boolean', nullable: false)]
    private bool $permissionAnonyme = false;

    #[ORM\Column(name: 'permission_admin', type: 'boolean', nullable: false)]
    private bool $permissionAdmin = false;

    #[ORM\Column(name: 'permission_paysan', type: 'boolean', nullable: false)]
    private bool $permissionPaysan = false;

    #[ORM\Column(name: 'permission_amap', type: 'boolean', nullable: false)]
    private bool $permissionAmap = false;

    #[ORM\Column(name: 'permission_amapienref', type: 'boolean', nullable: false)]
    private bool $permissionAmapienref = false;

    #[ORM\Column(name: 'permission_amapien', type: 'boolean', nullable: false)]
    private bool $permissionAmapien = false;

    public function nbPermissionsSelected(): int
    {
        $nbSelected = 0;
        // @phpstan-ignore-next-line
        foreach ($this as $key => $value) { // Loop over all permissions properties
            if (str_starts_with((string) $key, 'permission')
                && true === $this->{$key}) {
                ++$nbSelected;
            }
        }

        return $nbSelected;
    }

    #[Assert\Callback]
    public function validatePermissionCount(ExecutionContextInterface $context, mixed $payload): void
    {
        if (0 === $this->nbPermissionsSelected()) {
            $context
                ->buildViolation('Merci de selectionner au moins un type d\'utilisateur')
                ->addViolation()
            ;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Document
    {
        $this->nom = (string) $nom;

        return $this;
    }

    public function getLien(): string
    {
        return $this->lien;
    }

    public function setLien(?string $lien): Document
    {
        $this->lien = (string) $lien;

        return $this;
    }

    public function isPermissionAnonyme(): bool
    {
        return $this->permissionAnonyme;
    }

    public function setPermissionAnonyme(bool $permissionAnonyme): Document
    {
        $this->permissionAnonyme = $permissionAnonyme;

        return $this;
    }

    public function isPermissionAdmin(): bool
    {
        return $this->permissionAdmin;
    }

    public function setPermissionAdmin(bool $permissionAdmin): Document
    {
        $this->permissionAdmin = $permissionAdmin;

        return $this;
    }

    public function isPermissionPaysan(): bool
    {
        return $this->permissionPaysan;
    }

    public function setPermissionPaysan(bool $permissionPaysan): Document
    {
        $this->permissionPaysan = $permissionPaysan;

        return $this;
    }

    public function isPermissionAmap(): bool
    {
        return $this->permissionAmap;
    }

    public function setPermissionAmap(bool $permissionAmap): Document
    {
        $this->permissionAmap = $permissionAmap;

        return $this;
    }

    public function isPermissionAmapienref(): bool
    {
        return $this->permissionAmapienref;
    }

    public function setPermissionAmapienref(bool $permissionAmapienref): Document
    {
        $this->permissionAmapienref = $permissionAmapienref;

        return $this;
    }

    public function isPermissionAmapien(): bool
    {
        return $this->permissionAmapien;
    }

    public function setPermissionAmapien(bool $permissionAmapien): Document
    {
        $this->permissionAmapien = $permissionAmapien;

        return $this;
    }
}
