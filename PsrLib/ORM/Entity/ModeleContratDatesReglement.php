<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Auditable]
#[ORM\Table(name: 'ak_modele_contrat_dates_reglement')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ModeleContratDatesReglementRepository::class)]
class ModeleContratDatesReglement
{
    /**
     * @var ?int
     */
    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard', 'wizardContract'])]
    private ?int $id = null;

    #[ORM\Column(name: 'date', type: 'date', nullable: true)]
    #[Groups(['wizard'])]
    private ?\DateTime $date = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\ModeleContrat::class, inversedBy: 'dateReglements')]
    #[ORM\JoinColumn(name: 'fk_modele_contrat', referencedColumnName: 'mc_id')]
    private ?\PsrLib\ORM\Entity\ModeleContrat $modeleContrat = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): ModeleContratDatesReglement
    {
        $this->date = $date;

        return $this;
    }

    public function getModeleContrat(): ?ModeleContrat
    {
        return $this->modeleContrat;
    }

    public function setModeleContrat(?ModeleContrat $modeleContrat): ModeleContratDatesReglement
    {
        $this->modeleContrat = $modeleContrat;

        return $this;
    }
}
