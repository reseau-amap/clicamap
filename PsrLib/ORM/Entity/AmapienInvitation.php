<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapienInvitationRepository::class)]
#[ORM\HasLifecycleCallbacks]
class AmapienInvitation
{
    use TimestampableTrait;
    public const TOKEN_VALIDITY_IN_HOURS = 240;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\User::class)]
    #[ORM\JoinColumn(name: 'user_id', nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'amapienInvitations')]
    #[ORM\JoinColumn(referencedColumnName: 'amap_id', nullable: false)]
    private Amap $amap;

    #[ORM\OneToOne(targetEntity: Token::class, cascade: ['all'], orphanRemoval: true)]
    #[ORM\JoinColumn(nullable: false)]
    private \PsrLib\ORM\Entity\Token $token;

    public function __construct(User $user, ?Amap $amap)
    {
        $this->user = $user;
        $this->amap = $amap;
        $this->token = new Token(self::TOKEN_VALIDITY_IN_HOURS);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }
}
