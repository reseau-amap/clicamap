<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

interface EvenementWithLogo
{
    public function getLogo(): string;
}
