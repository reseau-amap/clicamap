<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_ville')]
#[ORM\UniqueConstraint(columns: ['v_cp', 'v_nom'])]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\VilleRepository::class)]
class Ville implements LocalisableEntity, \Stringable
{
    final public const SRC_OLD = 'old';
    final public const SRC_OSM = 'osm';

    #[ORM\Id]
    #[ORM\Column(type: 'integer', name: 'v_id')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer', name: 'v_cp', columnDefinition: 'INT(5) UNSIGNED ZEROFILL NOT NULL')]
    private int $cp;

    #[ORM\Column(type: 'string', length: 255, name: 'v_nom')]
    private string $nom;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Departement::class, fetch: 'EAGER', inversedBy: 'villes')]
    #[ORM\JoinColumn(name: 'v_fk_dep_id', referencedColumnName: 'dep_id', nullable: false)]
    private \PsrLib\ORM\Entity\Departement $departement;

    public function __construct(int $cp, string $nom, Departement $departement)
    {
        $this->cp = $cp;
        $this->nom = $nom;
        $this->departement = $departement;
    }

    public function __toString(): string
    {
        return $this->getCpString().', '.$this->getNom();
    }

    public function getCpString(): string
    {
        return sprintf('%05d', $this->getCp());
    }

    public function getRegion(): ?Region
    {
        return $this->getDepartement()->getRegion();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCp(): int
    {
        return $this->cp;
    }

    public function setCp(int $cp): Ville
    {
        $this->cp = $cp;

        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Ville
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDepartement(): Departement
    {
        return $this->departement;
    }

    public function setDepartement(Departement $departement): Ville
    {
        $this->departement = $departement;

        return $this;
    }
}
