<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_evenement_abonnement_region')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\EvenementAbonnementRegionRepository::class)]
class EvenementAbonnementRegion extends EvenementAbonnementWithRelatedEntity implements EvenementRelatedEntity
{
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Region::class)]
    #[ORM\JoinColumn(referencedColumnName: 'reg_id', nullable: false)]
    private \PsrLib\ORM\Entity\Region $related;

    public function getRelated(): Region
    {
        return $this->related;
    }

    /**
     * @param Region $related
     */
    public function setRelated(EvenementRelatedEntity $related): EvenementAbonnementRegion
    {
        $this->related = $related;

        return $this;
    }
}
