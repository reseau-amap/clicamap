<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AdhesionFermeRepository::class)]
class AdhesionFerme extends AdhesionCreatorUser
{
    /**
     * @var ?int
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[GeneratedValue]
    protected ?int $id = null;

    /**
     * @var ?Ferme
     */
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'adhesions')]
    #[ORM\JoinColumn(referencedColumnName: 'f_id', nullable: true, onDelete: 'SET NULL')]
    protected ?Ferme $ferme = null;

    /**
     * @var AdhesionValueFerme
     */
    #[ORM\OneToOne(targetEntity: AdhesionValueFerme::class, cascade: ['ALL'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Valid]
    protected $value;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Expression('value.isAdminOf(this.getFerme())', groups: ['import'], message: "Erreur d'accès à la ferme")]
    protected User $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(Ferme $ferme, AdhesionValueFerme $value, User $creator)
    {
        $this->ferme = $ferme;
        $this->value = $value;
        $this->creator = $creator;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): AdhesionFerme
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getValue(): AdhesionValueFerme
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueFerme $value
     *
     * @return AdhesionFerme
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
