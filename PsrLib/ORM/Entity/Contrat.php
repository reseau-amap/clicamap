<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Enum\ContratPaiementStatut;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\Workflow\ContratStatusWorkflow;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Auditable]
#[ORM\Table(name: 'ak_contrat')]
#[ORM\Index(name: 'IND_CONTRAT_FK_MODELE_CONTRAT', columns: ['c_fk_modele_contrat_id'])]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ContratRepository::class)]
class Contrat
{
    #[ORM\Column(name: 'c_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizardContract'])]
    private ?int $id = null;

    #[ORM\Column(name: 'c_date_creation', type: 'datetime', nullable: true)]
    #[Groups(['wizardContract'])]
    private ?\DateTime $dateCreation = null;

    #[ORM\Column(name: 'c_date_modification', type: 'datetime', nullable: true)]
    #[Groups(['wizardContract'])]
    private ?\DateTime $dateModification = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\ModeleContrat::class, inversedBy: 'contrats')]
    #[ORM\JoinColumn(name: 'c_fk_modele_contrat_id', referencedColumnName: 'mc_id')]
    #[Groups(['wizardContract'])]
    private ?\PsrLib\ORM\Entity\ModeleContrat $modeleContrat = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class, inversedBy: 'contrats')]
    #[Groups(['wizardContract'])]
    #[ORM\JoinColumn(name: 'c_fk_amapien_id', referencedColumnName: 'a_id')]
    private ?\PsrLib\ORM\Entity\Amapien $amapien = null;

    #[ORM\Column(name: 'c_reglement_type', type: 'string', length: 255, nullable: true)]
    #[Groups(['wizardContract'])]
    #[Assert\NotBlank(groups: ['subscription_2'], message: 'Merci de sélectionner un mode de règlement.')]
    private ?string $reglementType = null;

    #[ORM\Column(name: 'c_contrat_deplacements_effectues', type: 'integer', nullable: true)]
    #[Groups(['wizardContract'])]
    private ?int $deplacementsEffectues = null;

    #[ORM\Column(name: 'c_contrat_report_effectues', type: 'integer', nullable: true)]
    #[Groups(['wizardContract'])]
    private ?int $reportEffectues = null;

    /**
     * @var ArrayCollection<ContratCellule>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ContratCellule::class, mappedBy: 'contrat', cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['wizardContract'])]
    private \Doctrine\Common\Collections\Collection $cellules;

    /**
     * @var ArrayCollection<ContratDatesReglement>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ContratDatesReglement::class, mappedBy: 'contrat', cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['wizardContract'])]
    #[Assert\Valid(groups: ['subscription_2'])]
    #[Assert\Count(min: 1, groups: ['subscription_2'])]
    private \Doctrine\Common\Collections\Collection $datesReglements;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\ContratPdf::class, cascade: ['persist'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\ContratPdf $pdf = null;

    #[ORM\Column(type: 'string')]
    #[Groups(['wizardContract'])]
    private string $uuid;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['wizardContract'])]
    private bool $cdpPaid = false;

    #[ORM\Column(type: 'string', enumType: ContratPaiementStatut::class)]
    #[Groups(['wizardContract'])]
    private ContratPaiementStatut $contratPaiementStatut = ContratPaiementStatut::NON_PAYE;

    #[ORM\Column(type: 'string')]
    #[Groups(['wizardContract'])]
    private string $state = ContratStatusWorkflow::STATE_VALIDATED;

    /**
     * Contrat constructor.
     */
    public function __construct()
    {
        $this->cellules = new ArrayCollection();
        $this->datesReglements = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }

    public static function create(ModeleContrat $mc, Amapien $amapien, Carbon $dateCreation): Contrat
    {
        $contrat = new self();
        $contrat->setModeleContrat($mc);
        $contrat->setAmapien($amapien);
        $contrat->setDateCreation($dateCreation);

        foreach ($mc->getProduits() as $produit) {
            foreach ($mc->getDatesOrderedAsc() as $date) {
                $contrat->addCellule(ContratCellule::create($produit, $date, $contrat));
            }
        }

        return $contrat;
    }

    #[Assert\Callback(groups: ['subscription_2'])]
    public function validateNoDuplication(ExecutionContextInterface $context): void
    {
        $dates = array_map(fn (ContratDatesReglement $date) => $date->getModeleContratDatesReglement()?->getId(), $this->getDatesReglements()->toArray());
        $dates = array_filter($dates, fn ($date) => null !== $date);
        if (count($dates) !== count(array_unique($dates))) {
            $context
                ->buildViolation('Merci de ne pas sélectionner plus d\'une fois chaque date.')
                ->atPath('datesReglements')
                ->addViolation()
            ;
        }
    }

    public function isCdp(): bool
    {
        return $this->getModeleContrat()->isCdp();
    }

    /**
     * @return ContratCellule[]
     */
    public function getCellulesDate(Carbon $date): array
    {
        return $this
            ->cellules
            ->filter(fn (ContratCellule $cellule) => $date->eq($cellule->getModeleContratDate()->getDateLivraison()))
            ->toArray()
        ;
    }

    public function getCellulesDateTotalQty(Carbon $date): float
    {
        $cells = $this->getCellulesDate($date);
        $total = 0.0;
        foreach ($cells as $cell) {
            $total += $cell->getQuantite();
        }

        return $total;
    }

    public function getCelluleDateProduit(ModeleContratProduit $produit, ModeleContratDate $date): ?ContratCellule
    {
        $cellule = $this
            ->cellules
            ->filter(fn (ContratCellule $cellule) => $cellule->getModeleContratDate() === $date
                && $cellule->getModeleContratProduit() === $produit)
            ->first()
        ;

        return false === $cellule ? null : $cellule;
    }

    public function hasRegulPds(): bool
    {
        /** @var ContratCellule $cellule */
        foreach ($this->getCellules() as $cellule) {
            if (true === $cellule->getModeleContratProduit()->getRegulPds()) {
                return true;
            }
        }

        return false;
    }

    public function hasNoRegulPds(): bool
    {
        /** @var ContratCellule $cellule */
        foreach ($this->getCellules() as $cellule) {
            if (false === $cellule->getModeleContratProduit()->getRegulPds()) {
                return true;
            }
        }

        return false;
    }

    public function totalQtyByProduct(ModeleContratProduit $modeleContratProduit): float
    {
        $total = 0.0;
        foreach ($this->getCellules() as $cellule) {
            if ($cellule->getModeleContratProduit() === $modeleContratProduit) {
                $total += $cellule->getQuantite();
            }
        }

        return $total;
    }

    public function getFirstLivraisonDateWithFermeDelai(): ?Carbon
    {
        $contratDates = $this
            ->getCellules()
            ->filter(fn (ContratCellule $cellule) => $cellule->getQuantite() > 0)
            ->map(fn (ContratCellule $cellule) => $cellule->getModeleContratDate()->getDateLivraison())
            ->toArray()
        ;
        sort($contratDates);

        $firstDate = $contratDates[0] ?? null;
        if (null === $firstDate) {
            return null;
        }

        return Carbon::instance($firstDate)->subDays($this->getModeleContrat()->getDelaiModifContrat());
    }

    public function isNowBeforeOrEqualFirstLivraisonDateWithFermeDelai(): bool
    {
        $delai = $this->getFirstLivraisonDateWithFermeDelai();
        if (null === $delai) {
            return false;
        }

        return Carbon::now()->startOfDay()->lessThanOrEqualTo($delai);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): Contrat
    {
        $this->id = $id;

        return $this;
    }

    public function getModeleContrat(): ?ModeleContrat
    {
        return $this->modeleContrat;
    }

    public function setModeleContrat(?ModeleContrat $modeleContrat): Contrat
    {
        $this->modeleContrat = $modeleContrat;

        return $this;
    }

    public function getDateCreation(): ?\DateTime
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTime $dateCreation): Contrat
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateModification(): ?\DateTime
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTime $dateModification): Contrat
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): Contrat
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getReglementType(): ?string
    {
        return $this->reglementType;
    }

    public function setReglementType(?string $reglementType): Contrat
    {
        $this->reglementType = $reglementType;

        return $this;
    }

    public function getDeplacementsEffectues(): ?int
    {
        return $this->deplacementsEffectues;
    }

    public function setDeplacementsEffectues(?int $deplacementsEffectues): Contrat
    {
        $this->deplacementsEffectues = $deplacementsEffectues;

        return $this;
    }

    public function getReportEffectues(): ?int
    {
        return $this->reportEffectues;
    }

    public function setReportEffectues(?int $reportEffectues): Contrat
    {
        $this->reportEffectues = $reportEffectues;

        return $this;
    }

    /**
     * Add cellule.
     *
     * @return Contrat
     */
    public function addCellule(ContratCellule $cellule)
    {
        $this->cellules[] = $cellule;
        $cellule->setContrat($this);

        return $this;
    }

    /**
     * Remove cellule.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCellule(ContratCellule $cellule)
    {
        $cellule->setContrat(null);

        return $this->cellules->removeElement($cellule);
    }

    /**
     * Get cellules.
     *
     * @return Collection<ContratCellule>
     */
    public function getCellules()
    {
        return $this->cellules;
    }

    /**
     * Add cellule.
     *
     * @return Contrat
     */
    public function addDatesReglement(ContratDatesReglement $date)
    {
        $this->datesReglements[] = $date;
        $date->setContrat($this);

        return $this;
    }

    /**
     * Remove cellule.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDatesReglement(ContratDatesReglement $date)
    {
        $date->setContrat(null);

        return $this->datesReglements->removeElement($date);
    }

    /**
     * @return Collection<ContratDatesReglement>
     */
    public function getDatesReglements()
    {
        return $this->datesReglements;
    }

    public function getPdf(): ?ContratPdf
    {
        return $this->pdf;
    }

    public function setPdf(?ContratPdf $pdf): Contrat
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function isCdpPaid(): bool
    {
        return $this->cdpPaid;
    }

    public function setCdpPaid(bool $cdpPaid): Contrat
    {
        $this->cdpPaid = $cdpPaid;

        return $this;
    }

    public function getContratPaiementStatut(): ContratPaiementStatut
    {
        return $this->contratPaiementStatut;
    }

    public function setContratPaiementStatut(ContratPaiementStatut $contratPaiementStatut): Contrat
    {
        $this->contratPaiementStatut = $contratPaiementStatut;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): Contrat
    {
        $this->state = $state;

        return $this;
    }
}
