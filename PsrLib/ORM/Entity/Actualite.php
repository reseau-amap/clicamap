<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Repository\ActualiteRepository;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ActualiteRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Auditable]
class Actualite
{
    use TimestampableTrait;

    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 100)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    private ?string $titre = null;

    #[ORM\Column(type: 'datetime')]
    #[Assert\NotNull]
    private ?\DateTime $date = null;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    #[Assert\Length(max: 3000, maxMessage: 'La description est trop longue.')]
    private ?string $description = null;

    /**
     * @var Collection<User>
     */
    #[ORM\ManyToMany(targetEntity: User::class)]
    private Collection $utilsateursLus;

    public function __construct()
    {
        $this->utilsateursLus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): Actualite
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Actualite
    {
        $this->description = $description;

        return $this;
    }

    public function getUtilsateursLus(): Collection
    {
        return $this->utilsateursLus;
    }

    public function addUtilsateursLus(User $utilsateursLus): Actualite
    {
        if (!$this->utilsateursLus->contains($utilsateursLus)) {
            $this->utilsateursLus[] = $utilsateursLus;
        }

        return $this;
    }

    public function removeUtilsateursLus(User $utilsateursLus): Actualite
    {
        $this->utilsateursLus->removeElement($utilsateursLus);

        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): Actualite
    {
        $this->date = $date;

        return $this;
    }
}
