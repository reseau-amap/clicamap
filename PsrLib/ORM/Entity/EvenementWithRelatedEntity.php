<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

interface EvenementWithRelatedEntity extends EvenementWithLogo
{
    public function getCreatedAt(): \DateTimeInterface;

    public function getDateDeb(): ?\DateTime;

    public function getDateFin(): ?\DateTime;

    public function getRelated(): EvenementRelatedEntity;
}
