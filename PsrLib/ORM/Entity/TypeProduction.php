<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Auditable]
#[ORM\Table(name: 'ak_type_production')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\TypeProductionRepository::class)]
class TypeProduction implements \Stringable
{
    #[ORM\Column(name: 'tp_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard'])]
    private ?int $id = null;

    /**
     * @var string
     */
    #[ORM\Column(name: 'tp_categorie_lib', type: 'string', length: 255, nullable: false)]
    private $nom; // @phpstan-ignore-line
    /**
     * @var string
     */
    #[ORM\Column(name: 'tp_categorie_lib_lien', type: 'string', length: 255, nullable: false)]
    #[Groups(['wizard'])]
    private $slug; // @phpstan-ignore-line
    /**
     * @var TypeProduction|null
     */
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class, inversedBy: 'childs')]
    #[ORM\JoinColumn(name: 'tp_categorie_mere', referencedColumnName: 'tp_id')]
    private $parent; // @phpstan-ignore-line
    /**
     * @var ArrayCollection<TypeProduction>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class, mappedBy: 'parent')]
    private \Doctrine\Common\Collections\Collection $childs;

    /**
     * @var ArrayCollection<ModeleContratProduit>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ModeleContratProduit::class, mappedBy: 'typeProduction')]
    private \Doctrine\Common\Collections\Collection $mcProduits;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->childs = new ArrayCollection();
        $this->mcProduits = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    public function getNomComplet(): string
    {
        if (null === $this->parent) {
            return (string) $this;
        }

        return $this->parent.' -> '.$this;
    }

    public function getNomCompletParenthese(): string
    {
        if (null === $this->parent) {
            return (string) $this;
        }

        return $this->parent.' ('.$this.')';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getParent(): ?TypeProduction
    {
        return $this->parent;
    }

    /**
     * Get childs.
     *
     * @return Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    public function hasChilds(): bool
    {
        return !$this->childs->isEmpty();
    }

    /**
     * @return Collection<ModeleContratProduit>
     */
    public function getMcProduits(): Collection
    {
        return $this->mcProduits;
    }
}
