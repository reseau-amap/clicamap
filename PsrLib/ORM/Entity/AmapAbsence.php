<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\AmapAbsenceNoOverlap;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[AmapAbsenceNoOverlap]
#[ORM\Table(name: 'ak_amap_absence')]
#[ORM\Index(name: 'absence_fk_amap_id', columns: ['absence_fk_amap_id'])]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapAbsenceRepository::class)]
#[ORM\HasLifecycleCallbacks]
class AmapAbsence
{
    /**
     * @var ?int
     */
    #[ORM\Column(name: 'absence_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: 'absence_titre', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $titre = null;

    #[ORM\Column(name: 'absence_debut', type: 'carbon')]
    #[Assert\NotNull]
    private ?\Carbon\Carbon $absenceDebut = null;

    #[ORM\Column(name: 'absence_fin', type: 'carbon')]
    #[Assert\NotNull]
    #[Assert\Expression('(this.getAbsenceDebut() != null and this.getAbsenceFin() != null) and this.getAbsenceFin().gt(this.getAbsenceDebut())', message: 'La date de début doit être avant la date de fin')]
    private ?\Carbon\Carbon $absenceFin = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'absences')]
    #[ORM\JoinColumn(name: 'absence_fk_amap_id', referencedColumnName: 'amap_id', nullable: false)]
    private \PsrLib\ORM\Entity\Amap $amap;

    /**
     * AmapAbsence constructor.
     */
    public function __construct(Amap $amap)
    {
        $this->amap = $amap;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): AmapAbsence
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAbsenceDebut(): ?Carbon
    {
        return $this->absenceDebut;
    }

    public function setAbsenceDebut(?Carbon $absenceDebut): AmapAbsence
    {
        $this->absenceDebut = $absenceDebut;

        return $this;
    }

    public function getAbsenceFin(): ?Carbon
    {
        return $this->absenceFin;
    }

    public function setAbsenceFin(?Carbon $absenceFin): AmapAbsence
    {
        $this->absenceFin = $absenceFin;

        return $this;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap): AmapAbsence
    {
        $this->amap = $amap;

        return $this;
    }
}
