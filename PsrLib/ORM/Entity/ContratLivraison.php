<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['amapien', 'ferme', 'date'])]
#[ORM\Table(name: 'ak_contrat_livraison')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ContratLivraisonRepository::class)]
class ContratLivraison
{
    /**
     * @var ?int
     */
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class, inversedBy: 'livraisons')]
    #[ORM\JoinColumn(referencedColumnName: 'a_id', nullable: false)]
    private \PsrLib\ORM\Entity\Amapien $amapien;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class)]
    #[ORM\JoinColumn(referencedColumnName: 'f_id', nullable: false)]
    private \PsrLib\ORM\Entity\Ferme $ferme;

    #[ORM\Column(type: 'carbon')]
    private \Carbon\Carbon $date;

    #[ORM\Column(type: 'boolean')]
    private bool $livre = false;

    /**
     * @var ArrayCollection<ContratLivraisonCellule>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ContratLivraisonCellule::class, mappedBy: 'contratLivraison', cascade: ['PERSIST', 'REMOVE'], fetch: 'EAGER')]
    #[Assert\Valid]
    private \Doctrine\Common\Collections\Collection $cellules;

    public function __construct(Amapien $amapien, Ferme $ferme, Carbon $date)
    {
        $this->amapien = $amapien;
        $this->ferme = $ferme;
        $this->date = $date;
        $this->cellules = new ArrayCollection();
    }

    public function getCelluleByContratCellule(ContratCellule $cellule): ?ContratLivraisonCellule
    {
        $res = $this
            ->cellules
            ->filter(fn (ContratLivraisonCellule $livCellule) => $livCellule->getContratCellule() === $cellule)
            ->first()
        ;

        return false === $res ? null : $res;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmapien(): Amapien
    {
        return $this->amapien;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }

    public function getDate(): Carbon
    {
        return $this->date;
    }

    public function isLivre(): bool
    {
        return $this->livre;
    }

    public function setLivre(bool $livre): ContratLivraison
    {
        $this->livre = $livre;

        return $this;
    }

    public function getCellules(): Collection
    {
        return $this->cellules;
    }

    public function addCellule(ContratLivraisonCellule $cellule): ContratLivraison
    {
        $this->cellules[] = $cellule;

        return $this;
    }
}
