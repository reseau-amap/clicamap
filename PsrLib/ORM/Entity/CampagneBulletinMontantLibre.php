<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use PsrLib\Validator\MoneyGreaterThanOrEqual;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['campagne', 'ordre'])]
#[ORM\Table(name: 'ak_campagne_bulletin_montant_libre')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\CampagneBulletinMontantLibreRepository::class)]
#[ORM\HasLifecycleCallbacks]
class CampagneBulletinMontantLibre
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'money')]
    #[Groups(['wizardCampagneSubscription'])]
    #[Assert\NotNull(groups: ['form1'], message: 'Le montant est obligatoire')]
    #[MoneyGreaterThanOrEqual(0, groups: ['form1'])]
    private ?Money $montant = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\CampagneBulletin::class, inversedBy: 'montants')]
    private ?CampagneBulletin $bulletin;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\CampagneMontant::class)]
    #[Groups(['wizardCampagneSubscription'])]
    private ?CampagneMontant $campagneMontant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMontant(): ?Money
    {
        return $this->montant;
    }

    public function setMontant(?Money $montant): CampagneBulletinMontantLibre
    {
        $this->montant = $montant;

        return $this;
    }

    public function getBulletin(): ?CampagneBulletin
    {
        return $this->bulletin;
    }

    public function setBulletin(?CampagneBulletin $bulletin): CampagneBulletinMontantLibre
    {
        $this->bulletin = $bulletin;

        return $this;
    }

    public function getCampagneMontant(): ?CampagneMontant
    {
        return $this->campagneMontant;
    }

    public function setCampagneMontant(?CampagneMontant $campagneMontant): CampagneBulletinMontantLibre
    {
        $this->campagneMontant = $campagneMontant;

        return $this;
    }
}
