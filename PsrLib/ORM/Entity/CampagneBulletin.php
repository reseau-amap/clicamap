<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity(fields: ['campagne', 'amapien'])]
#[Auditable]
#[ORM\Table(name: 'ak_campagne_bulletin')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\CampagneBulletinRepository::class)]
#[ORM\HasLifecycleCallbacks]
class CampagneBulletin
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['wizardCampagneSubscription'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255, groups: ['form2'])]
    #[Assert\NotBlank(groups: ['form2'])]
    #[Groups(['wizardCampagneSubscription'])]
    private ?string $payeur = null;

    #[ORM\Column(type: 'carbon')]
    #[Assert\NotBlank(groups: ['form2'])]
    #[Groups(['wizardCampagneSubscription'])]
    private ?\Carbon\Carbon $paiementDate = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Campagne::class, inversedBy: 'bulletins')]
    #[Assert\NotBlank()]
    #[Groups(['wizardCampagneSubscription'])]
    private ?Campagne $campagne;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class, inversedBy: 'bulletins')]
    #[ORM\JoinColumn(referencedColumnName: 'a_id')]
    #[Assert\NotBlank()]
    #[Groups(['wizardCampagneSubscription'])]
    private ?Amapien $amapien;

    /**
     * @var \Doctrine\Common\Collections\Collection<CampagneBulletinMontantLibre>
     */
    #[Assert\Valid(groups: ['form1'])]
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\CampagneBulletinMontantLibre::class, mappedBy: 'bulletin', cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(['wizardCampagneSubscription'])]
    private \Doctrine\Common\Collections\Collection $montants;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['wizardCampagneSubscription'])]
    private ?bool $permissionImage = false;

    /**
     * Non persisted field used for wizard.
     * Stored here to manage back on previous step.
     */
    #[Groups(['wizardCampagneSubscription'])]
    #[Assert\IsTrue(groups: ['form2'], message: 'Cette valeur est obligatoire.')]
    private bool $chartre = false;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\CampagneBulletin::class, cascade: ['persist'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\CampagneBulletin $pdf = null;

    #[ORM\OneToOne(inversedBy: 'bulletin', targetEntity: \PsrLib\ORM\Entity\CampagneBulletinRecus::class, cascade: ['persist'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\CampagneBulletinRecus $recus = null;

    public function __construct()
    {
        $this->montants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getMontantTotal(): Money
    {
        $campagneTotal = $this->getCampagne()->montantTotal();
        foreach ($this->getMontants() as $montant) {
            $campagneTotal = $campagneTotal->add($montant->getMontant());
        }

        return $campagneTotal;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampagne(): ?Campagne
    {
        return $this->campagne;
    }

    public function setCampagne(?Campagne $campagne): CampagneBulletin
    {
        $this->campagne = $campagne;

        return $this;
    }

    public function addMontant(CampagneBulletinMontantLibre $montant): CampagneBulletin
    {
        $this->montants[] = $montant;
        $montant->setBulletin($this);

        return $this;
    }

    /**
     * Remove Montants.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeMontant(CampagneBulletinMontantLibre $comments)
    {
        return $this->montants->removeElement($comments);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<CampagneBulletinMontantLibre>
     */
    public function getMontants()
    {
        return $this->montants;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): CampagneBulletin
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getPayeur(): ?string
    {
        return $this->payeur;
    }

    public function setPayeur(?string $payeur): CampagneBulletin
    {
        $this->payeur = $payeur;

        return $this;
    }

    public function getPaiementDate(): ?\Carbon\Carbon
    {
        return $this->paiementDate;
    }

    public function setPaiementDate(?\Carbon\Carbon $paiementDate): CampagneBulletin
    {
        $this->paiementDate = $paiementDate;

        return $this;
    }

    public function getPermissionImage(): ?bool
    {
        return $this->permissionImage;
    }

    public function setPermissionImage(?bool $permissionImage): CampagneBulletin
    {
        $this->permissionImage = $permissionImage;

        return $this;
    }

    public function isChartre(): bool
    {
        return $this->chartre;
    }

    public function setChartre(bool $chartre): CampagneBulletin
    {
        $this->chartre = $chartre;

        return $this;
    }

    public function getPdf(): ?Files\CampagneBulletin
    {
        return $this->pdf;
    }

    public function setPdf(?Files\CampagneBulletin $pdf): CampagneBulletin
    {
        $this->pdf = $pdf;

        return $this;
    }

    public function getRecus(): ?CampagneBulletinRecus
    {
        return $this->recus;
    }

    public function setRecus(?CampagneBulletinRecus $recus): CampagneBulletin
    {
        $this->recus = $recus;

        return $this;
    }
}
