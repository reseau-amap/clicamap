<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

interface EvenementRelatedEntity
{
    public function getId(): int|string|null;
}
