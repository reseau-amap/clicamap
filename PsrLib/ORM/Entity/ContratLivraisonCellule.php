<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_contrat_livraison_cellule')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ContratLivraisonCelluleRepository::class)]
class ContratLivraisonCellule
{
    /**
     * @var ?int
     */
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\ContratLivraison::class, inversedBy: 'cellules')]
    #[ORM\JoinColumn(nullable: false)]
    private \PsrLib\ORM\Entity\ContratLivraison $contratLivraison;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\ContratCellule::class, inversedBy: 'contratLivraisonCellule')]
    #[ORM\JoinColumn(referencedColumnName: 'c_c_id', nullable: false)]
    private \PsrLib\ORM\Entity\ContratCellule $contratCellule;

    #[ORM\Column(type: 'float', precision: 10, scale: 0)]
    #[Assert\NotBlank]
    private ?float $quantite = null;

    public function __construct(ContratLivraison $contratLivraison, ContratCellule $contratCellule)
    {
        $this->contratLivraison = $contratLivraison;
        $this->contratCellule = $contratCellule;
        if ($contratCellule->getModeleContratProduit()->getRegulPds()) {
            $this->quantite = null; // Managed by form
        } else {
            $this->quantite = $contratCellule->getQuantite();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContratLivraison(): ContratLivraison
    {
        return $this->contratLivraison;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    /**
     * @param float $quantite
     */
    public function setQuantite(?float $quantite): ContratLivraisonCellule
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getContratCellule(): ContratCellule
    {
        return $this->contratCellule;
    }
}
