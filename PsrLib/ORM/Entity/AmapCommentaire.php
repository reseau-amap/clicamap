<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_amap_commentaire')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapCommentaireRepository::class)]
class AmapCommentaire
{
    /**
     * @var ?int
     */
    #[ORM\Column(name: 'amap_com_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: 'amap_com_com', type: 'text', length: 65535, nullable: true)]
    private ?string $comment = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'comments')]
    #[ORM\JoinColumn(name: 'amap_com_fk_amap_id', referencedColumnName: 'amap_id')]
    private ?\PsrLib\ORM\Entity\Amap $amap = null;

    #[ORM\Column(name: 'amap_com_date', type: 'date')]
    private \DateTime $date;

    /**
     * AmapCommentaire constructor.
     */
    public function __construct(Amap $amap, \DateTime $date, string $comment)
    {
        $this->amap = $amap;
        $this->date = $date;
        $this->comment = $comment;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): AmapCommentaire
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Amap
     */
    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap = null): AmapCommentaire
    {
        $this->amap = $amap;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): AmapCommentaire
    {
        $this->date = $date;

        return $this;
    }
}
