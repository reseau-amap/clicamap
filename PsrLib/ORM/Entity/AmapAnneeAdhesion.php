<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_amap_annee_adhesion')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapAnneeAdhesionRepository::class)]
class AmapAnneeAdhesion implements \Stringable
{
    /**
     * @var ?int
     */
    #[ORM\Column(type: 'integer', name: 'amap_aa_id')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column(type: 'integer', name: 'amap_aa_annee')]
    private int $annee;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'anneeAdhesions')]
    #[ORM\JoinColumn(name: 'amap_aa_fk_amap_id', referencedColumnName: 'amap_id')]
    private ?\PsrLib\ORM\Entity\Amap $amap = null;

    /**
     * AmapAnneeAdhesion constructor.
     */
    public function __construct(int $annee, Amap $amap)
    {
        $this->annee = $annee;
        $this->amap = $amap;
    }

    public function __toString(): string
    {
        return (string) $this->annee;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): AmapAnneeAdhesion
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap = null): AmapAnneeAdhesion
    {
        $this->amap = $amap;

        return $this;
    }
}
