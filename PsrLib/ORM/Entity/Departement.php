<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use PsrLib\ORM\Entity\Files\Logo;

#[Auditable]
#[ORM\Table(name: 'ak_departement')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\DepartementRepository::class)]
class Departement implements EntityWithLogoInterface, LocalisableEntity, \Stringable, EvenementRelatedEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'dep_id', length: 3)]
    private string $id; // @phpstan-ignore-line

    #[ORM\Column(name: 'dep_nom', length: 55, nullable: false)]
    private ?string $nom = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Region::class, fetch: 'EAGER', inversedBy: 'departements')]
    #[ORM\JoinColumn(name: 'dep_fk_reg_id', referencedColumnName: 'reg_id')]
    private ?\PsrLib\ORM\Entity\Region $region = null;

    /**
     * @var ArrayCollection<User>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\User::class, mappedBy: 'adminDepartments')]
    private \Doctrine\Common\Collections\Collection $admins;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\Logo::class, cascade: ['ALL'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\Logo $logo = null;

    #[ORM\Column(name: 'dep_reseau_url', type: 'string', length: 255, nullable: true)]
    private ?string $url = null;

    /**
     * @var ArrayCollection<Ville>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Ville::class, mappedBy: 'departement')]
    private \Doctrine\Common\Collections\Collection $villes;

    /**
     * OSM entity related to departement. Used for city updates.
     *
     * @var string
     */
    #[ORM\Column(type: 'string', length: 255)]
    private $osmId; // @phpstan-ignore-line

    public function __construct()
    {
        $this->admins = new ArrayCollection();
        $this->villes = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getNom();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getDepartement(): ?Departement
    {
        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Departement
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): Departement
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Departement
     */
    public function addAdmin(User $admin)
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(User $admin)
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<User>
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): Departement
    {
        $this->logo = $logo;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Departement
    {
        $this->url = $url;

        return $this;
    }

    public function getOsmId(): string
    {
        return $this->osmId;
    }

    public function getVilles(): \Doctrine\Common\Collections\Collection
    {
        return $this->villes;
    }
}
