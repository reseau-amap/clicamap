<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ProjectLocation;

#[ORM\MappedSuperclass]
abstract class Adhesion
{
    final public const ADHESION_PATH = ProjectLocation::PROJECT_ROOT.'/public/uploads/adhesion/';

    final public const STATE_DRAFT = 'draft';
    final public const STATE_GENERATING = 'generating';
    final public const STATE_GENERATED = 'generated';

    /**
     * @var ?int
     */
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    protected string $state = self::STATE_DRAFT;

    /**
     * @var AdhesionValue Use phpdoc typing for inheritance
     */
    protected $value;

    /**
     * @var ?Carbon
     */
    #[ORM\Column(type: 'carbon', nullable: true)]
    protected ?Carbon $generationDate = null;

    /**
     * @var ?string
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $voucherFileName = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return AdhesionValue
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param AdhesionValue $value
     *
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGenerationDate()
    {
        return $this->generationDate;
    }

    public function setGenerationDate(mixed $generationDate): self
    {
        $this->generationDate = $generationDate;

        return $this;
    }

    public function getVoucherFileName(): ?string
    {
        return $this->voucherFileName;
    }

    public function setVoucherFileName(?string $voucherFileName): self
    {
        $this->voucherFileName = $voucherFileName;

        return $this;
    }
}
