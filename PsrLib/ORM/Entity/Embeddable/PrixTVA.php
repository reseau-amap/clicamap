<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Money\Money;
use PsrLib\Services\MoneyHelper;
use PsrLib\Validator\MoneyGreaterThanOrEqual;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Embeddable]
class PrixTVA implements \Stringable
{
    final public const TVA_CHOICES = [
        null,
        0,
        5.5,
        10,
        20,
    ];

    #[ORM\Column(type: 'float', nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    #[Groups(['wizard'])]
    private ?float $tva = null;

    #[MoneyGreaterThanOrEqual(0)]
    #[ORM\Column(type: 'money')]
    #[Assert\NotNull(message: 'Le prix est obligatoire')]
    #[Groups(['wizard'])]
    private ?\Money\Money $prixTTC;

    public function __construct()
    {
        $this->prixTTC = Money::EUR(0);
    }

    public function __toString(): string
    {
        return MoneyHelper::toString($this->getPrixTTC());
    }

    public function hasTVA(): bool
    {
        return null !== $this->tva;
    }

    public function getPrixHT(): Money
    {
        $tva = $this->getTva();
        if (null === $tva) {
            return $this->getPrixTTC();
        }

        return $this->getPrixTTC()->multiply(100)->divide((string) (100 + $this->getTva()));
    }

    public function getTvaAmount(): Money
    {
        return $this->getPrixTTC()->subtract($this->getPrixHT());
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(?float $tva): void
    {
        $this->tva = $tva;
    }

    public function getPrixTTC(): ?Money
    {
        return $this->prixTTC;
    }

    public function setPrixTTC(?Money $prixTTC): void
    {
        $this->prixTTC = $prixTTC;
    }
}
