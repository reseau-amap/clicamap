<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Symfony\Component\Validator\Constraints as Assert;

#[Embeddable]
class Name implements \Stringable
{
    #[Assert\Length(max: 255)]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $firstName = null;

    #[Assert\Length(max: 255)]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $lastName = null;

    public function getFullName(): string
    {
        return ucfirst(strtolower((string) $this->firstName)).' '.mb_strtoupper((string) $this->lastName);
    }

    public function getFullNameInversed(): string
    {
        return mb_strtoupper((string) $this->lastName).' '.ucfirst(strtolower((string) $this->firstName));
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }
}
