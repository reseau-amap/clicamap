<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Symfony\Component\Serializer\Annotation\Groups;

#[Embeddable]
class CampagneAutheurInfo
{
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $nom;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $adresse;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $ville;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $site;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $telephone;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $siret;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $rna;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['wizardCampagne'])]
    private string $emails;

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): CampagneAutheurInfo
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): CampagneAutheurInfo
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function setVille(string $ville): CampagneAutheurInfo
    {
        $this->ville = $ville;

        return $this;
    }

    public function getSite(): string
    {
        return $this->site;
    }

    public function setSite(string $site): CampagneAutheurInfo
    {
        $this->site = $site;

        return $this;
    }

    public function getTelephone(): string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): CampagneAutheurInfo
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getSiret(): string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): CampagneAutheurInfo
    {
        $this->siret = $siret;

        return $this;
    }

    public function getRna(): string
    {
        return $this->rna;
    }

    public function setRna(string $rna): CampagneAutheurInfo
    {
        $this->rna = $rna;

        return $this;
    }

    public function getEmails(): string
    {
        return $this->emails;
    }

    public function setEmails(string $emails): CampagneAutheurInfo
    {
        $this->emails = $emails;

        return $this;
    }
}
