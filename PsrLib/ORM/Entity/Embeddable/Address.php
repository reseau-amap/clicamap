<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Symfony\Component\Validator\Constraints as Assert;

#[Embeddable]
class Address
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $adress = null;

    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $adressCompl = null;

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): void
    {
        $this->adress = $adress;
    }

    public function getAdressCompl(): ?string
    {
        return $this->adressCompl;
    }

    public function setAdressCompl(?string $adressCompl): void
    {
        $this->adressCompl = $adressCompl;
    }
}
