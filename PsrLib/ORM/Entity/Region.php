<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Files\Logo;

#[Auditable]
#[ORM\Table(name: 'ak_region')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\RegionRepository::class)]
class Region implements EntityWithLogoInterface, \Stringable, EvenementRelatedEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'reg_id', type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(name: 'reg_nom', length: 55, nullable: false)]
    private ?string $nom = null;

    /**
     * @var ArrayCollection<User>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\User::class, mappedBy: 'adminRegions')]
    private \Doctrine\Common\Collections\Collection $admins;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\Logo::class, cascade: ['ALL'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\Logo $logo = null;

    #[ORM\Column(name: 'reg_reseau_url', type: 'string', length: 255, nullable: true)]
    private ?string $url = null;

    /**
     * @var ArrayCollection<Departement>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Departement::class, mappedBy: 'region')]
    private \Doctrine\Common\Collections\Collection $departements;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->admins = new ArrayCollection();
        $this->departements = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getNom();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Region
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Region
     */
    public function addAdmin(User $admin)
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(User $admin)
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<User>
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Region
    {
        $this->url = $url;

        return $this;
    }

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): Region
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return Region
     */
    public function addDepartement(Departement $admin)
    {
        $this->departements[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDepartement(Departement $admin)
    {
        return $this->departements->removeElement($admin);
    }

    /**
     * Get departements.
     *
     * @return ArrayCollection<Departement>
     */
    public function getDepartements()
    {
        return $this->departements;
    }
}
