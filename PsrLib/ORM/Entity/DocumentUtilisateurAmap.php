<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_document_utilisateur_amap')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\DocumentUtilisateurAmapRepository::class)]
class DocumentUtilisateurAmap extends DocumentUtilisateur implements LocalisableEntity
{
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class)]
    #[ORM\JoinColumn(referencedColumnName: 'amap_id', nullable: false)]
    private Amap $amap;

    public function __construct(int $annee, string $nom, Files\DocumentUtilisateur $ficher, Amap $amap)
    {
        parent::__construct($annee, $nom, $ficher);
        $this->amap = $amap;
    }

    public function getRegion(): ?Region
    {
        return $this->getAmap()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getAmap()->getDepartement();
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }
}
