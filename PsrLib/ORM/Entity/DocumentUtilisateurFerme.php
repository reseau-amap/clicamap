<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_document_utilisateur_ferme')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\DocumentUtilisateurFermeRepository::class)]
class DocumentUtilisateurFerme extends DocumentUtilisateur implements LocalisableEntity
{
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class)]
    #[ORM\JoinColumn(referencedColumnName: 'f_id', nullable: false)]
    private Ferme $ferme;

    public function __construct(int $annee, string $nom, Files\DocumentUtilisateur $ficher, Ferme $ferme)
    {
        parent::__construct($annee, $nom, $ficher);
        $this->ferme = $ferme;
    }

    public function getRegion(): ?Region
    {
        return $this->getFerme()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getFerme()->getDepartement();
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }
}
