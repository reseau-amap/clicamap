<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity\Files;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\EntityListeners;
use PsrLib\ORM\EventSubscriber\FilePostRemoveListener;
use PsrLib\ProjectLocation;

#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\Files\FileRepository::class)]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[EntityListeners([FilePostRemoveListener::class])]
class File
{
    private const UPLOAD_RELATIVE_PATH = 'uploads/';

    /**
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $originalFileName = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $fileName;

    public function __construct(string $fileName, ?string $originalFileName = null)
    {
        $this->fileName = $fileName;
        $this->originalFileName = $originalFileName;
    }

    public static function getUploadPath(): string
    {
        return ProjectLocation::PROJECT_ROOT.'/public/'.self::UPLOAD_RELATIVE_PATH.static::getUploadFolder();
    }

    public function getFileRelativePath(): string
    {
        return self::UPLOAD_RELATIVE_PATH.static::getUploadFolder().'/'.$this->getFileName();
    }

    public function getFileFullPath(): string
    {
        return self::getUploadPath().'/'.$this->getFileName();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalFileName(): ?string
    {
        return $this->originalFileName;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * Folder where upload file are stored. Override in extended class.
     */
    protected static function getUploadFolder(): string
    {
        return 'files';
    }
}
