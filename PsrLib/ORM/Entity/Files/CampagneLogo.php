<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity\Files;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Campagne;

#[Auditable]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\Files\CampagneLogoRepository::class)]
class CampagneLogo extends File
{
    /**
     * @var Collection<Campagne>
     */
    #[ORM\OneToMany(mappedBy: 'autheurLogo', targetEntity: \PsrLib\ORM\Entity\Campagne::class, orphanRemoval: true)]
    private Collection $campagnes;

    public function __construct(string $fileName, ?string $originalFileName = null)
    {
        parent::__construct($fileName, $originalFileName);
        $this->campagnes = new ArrayCollection();
    }

    protected static function getUploadFolder(): string
    {
        return 'logo_campagne';
    }

    public function addCampagne(Campagne $campagne): CampagneLogo
    {
        $this->campagnes[] = $campagne;
        $campagne->setAutheurLogo($this);

        return $this;
    }

    /**
     * Remove Campagnes.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCampagne(Campagne $campagne)
    {
        return $this->campagnes->removeElement($campagne);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<Campagne>
     */
    public function getCampagnes()
    {
        return $this->campagnes;
    }
}
