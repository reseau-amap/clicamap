<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\ORM\Repository\FermeProduitRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_ferme_produit')]
#[ORM\Entity(repositoryClass: FermeProduitRepository::class)]
class FermeProduit
{
    final public const AVAILABLE_CERTIFICATIONS = [
        'ab',
        'np',
        'conversion',
        'aucun',
    ];

    /**
     * @var ?int
     */
    #[ORM\Column(name: 'f_pro_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard'])]
    private ?int $id = null;

    #[ORM\Column(name: 'f_pro_nom', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $nom = null;

    #[ORM\Column(name: 'f_pro_conditionnement', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $conditionnement = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'produits')]
    #[ORM\JoinColumn(name: 'f_pro_fk_ferme_id', referencedColumnName: 'f_id')]
    #[Assert\NotNull]
    private ?\PsrLib\ORM\Entity\Ferme $ferme = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class)]
    #[ORM\JoinColumn(referencedColumnName: 'tp_id')]
    #[Assert\NotNull]
    private ?\PsrLib\ORM\Entity\TypeProduction $typeProduction = null;

    #[ORM\Embedded(class: \PsrLib\ORM\Entity\Embeddable\PrixTVA::class, columnPrefix: 'prix_')]
    #[Assert\Valid]
    private \PsrLib\ORM\Entity\Embeddable\PrixTVA $prix;

    #[ORM\Column(name: 'f_pro_regul_pds', type: 'boolean', nullable: false)]
    #[Assert\NotNull]
    private bool $regulPds = false;

    #[ORM\Column(name: 'f_pro_certification', type: 'string', length: 255, nullable: true)]
    #[Assert\Choice(choices: FermeProduit::AVAILABLE_CERTIFICATIONS)]
    private ?string $certification = null;

    #[ORM\Column(name: 'f_pro_enabled', type: 'boolean', nullable: false)]
    private bool $enabled = true;

    public function __construct()
    {
        $this->prix = new PrixTVA();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): FermeProduit
    {
        $this->nom = $nom;

        return $this;
    }

    public function getConditionnement(): ?string
    {
        return $this->conditionnement;
    }

    public function setConditionnement(?string $conditionnement): FermeProduit
    {
        $this->conditionnement = $conditionnement;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): FermeProduit
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): FermeProduit
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }

    public function getPrix(): PrixTVA
    {
        return $this->prix;
    }

    public function setPrix(PrixTVA $prix): void
    {
        $this->prix = $prix;
    }

    public function isRegulPds(): bool
    {
        return $this->regulPds;
    }

    public function setRegulPds(bool $regulPds): FermeProduit
    {
        $this->regulPds = $regulPds;

        return $this;
    }

    public function getCertification(): ?string
    {
        return $this->certification;
    }

    public function setCertification(?string $certification): FermeProduit
    {
        $this->certification = $certification;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): FermeProduit
    {
        $this->enabled = $enabled;

        return $this;
    }
}
