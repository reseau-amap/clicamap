<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

interface ActivableUserInterface
{
    final public const ETAT_ACTIF = 'actif';
    final public const ETAT_INACTIF = 'inactif';
    final public const ETAT_AVALIABLE = [
        ActivableUserInterface::ETAT_ACTIF, ActivableUserInterface::ETAT_INACTIF,
    ];

    public function getEtat(): string;

    public function setEtat(string $etat): void;

    public function estActif(): bool;
}
