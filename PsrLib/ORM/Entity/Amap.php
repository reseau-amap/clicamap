<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use PsrLib\ORM\Entity\Files\AmapLogo;
use PsrLib\ORM\Repository\AmapCommentaireRepository;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['email'])]
#[ORM\Table(name: 'ak_amap')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapRepository::class)]
class Amap implements \Stringable, LocalisableEntity, EvenementRelatedEntity
{
    final public const ETAT_CREATION = 'creation';
    final public const ETAT_ATTENTE = 'attente';
    final public const ETAT_FONCIONNEMENT = 'fonctionnement';
    final public const ETATS = [
        self::ETAT_CREATION,
        self::ETAT_ATTENTE,
        self::ETAT_FONCIONNEMENT,
    ];

    #[ORM\Id]
    #[ORM\Column(name: 'amap_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: 'amap_email_public', length: 255, nullable: true)]
    #[Assert\Email(mode: 'strict')]
    #[Groups(['wizardAmap'])]
    private ?string $email = null;

    #[ORM\Column(name: 'amap_nom', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Groups(['wizardAmap'])]
    private ?string $nom = null;

    #[ORM\Column(name: 'amap_adresse_admin_lib_adresse', type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Groups(['wizardAmap'])]
    private ?string $adresseAdmin = null;

    #[ORM\Column(name: 'amap_adresse_admin_lib_adresse_nom', type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Groups(['wizardAmap'])]
    private ?string $adresseAdminNom = null;

    #[ORM\Column(name: 'amap_password', type: 'string', length: 255, nullable: true)]
    private ?string $password = null;

    #[ORM\Column(name: 'amap_url', type: 'string', length: 255, nullable: true)]
    #[Groups(['wizardAmap'])]
    private ?string $url = null;

    #[ORM\Column(name: 'amap_date_creation', type: 'string', length: 4, nullable: true)]
    #[Assert\Length(min: 4, max: 4, exactMessage: "L'année doivent avoir exactement 4 caractères")]
    #[Assert\Type(type: 'digit', message: "L'année doit être constituée de chiffres")]
    #[Groups(['wizardAmap'])]
    private ?string $anneeCreation = null;

    #[ORM\Column(name: 'amap_nb_adherents', type: 'string', length: 55, nullable: true)]
    #[Assert\Type(type: 'digit', message: "Le nombre d'adhérents doit etre constitué de chiffres")]
    #[Assert\Length(max: 55)]
    #[Groups(['wizardAmap'])]
    private ?string $nbAdherents = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class)]
    #[ORM\JoinColumn(name: 'amap_fk_contact_referent_reseau_id', referencedColumnName: 'a_id', onDelete: 'SET NULL')]
    private ?\PsrLib\ORM\Entity\Amapien $amapienRefReseau = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class)]
    #[ORM\JoinColumn(name: 'amap_fk_contact_referent_reseau_secondaire_id', referencedColumnName: 'a_id', onDelete: 'SET NULL')]
    private ?\PsrLib\ORM\Entity\Amapien $amapienRefReseauSecondaire = null;

    #[Groups(['wizardAmap'])]
    #[ORM\Column(name: 'amap_etat', type: 'string', length: 255, nullable: true)]
    private ?string $etat;

    #[ORM\Column(name: 'possede_assurance', type: 'boolean', nullable: true)]
    #[Assert\NotNull(message: 'Le champ Assurance réseau est requis')]
    #[Groups(['wizardAmap'])]
    private ?bool $possedeAssurance = null;

    #[ORM\Column(name: 'amap_uuid', type: 'string', length: 255, nullable: true)]
    private ?string $uuid;

    #[ORM\Column(name: 'amap_etudiante', type: 'boolean', nullable: true)]
    #[Assert\NotNull(message: 'Le champ AMAP étudiante est requis')]
    #[Groups(['wizardAmap'])]
    private ?bool $amapEtudiante = null;

    /**
     * @var ArrayCollection<AmapLivraisonLieu>
     */
    #[ORM\OneToMany(targetEntity: AmapLivraisonLieu::class, mappedBy: 'amap', cascade: ['persist', 'remove'])]
    private \Doctrine\Common\Collections\Collection $livraisonLieux;

    /**
     * @var ArrayCollection<AmapAnneeAdhesion>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapAnneeAdhesion::class, mappedBy: 'amap', cascade: ['PERSIST'], orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $anneeAdhesions;

    /**
     * @var ArrayCollection<AmapCommentaire>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapCommentaire::class, mappedBy: 'amap', orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $comments;

    /**
     * @var ArrayCollection<TypeProduction>
     */
    #[JoinTable(name: 'ak_amap_type_production_propose')]
    #[JoinColumn(name: 'amap_tpp_fk_amap_id', referencedColumnName: 'amap_id')]
    #[InverseJoinColumn(name: 'amap_tpp_fk_type_production_id', referencedColumnName: 'tp_id')]
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class)]
    #[Groups(['wizardAmap'])]
    private \Doctrine\Common\Collections\Collection $tpPropose;

    /**
     * @var ArrayCollection<TypeProduction>
     */
    #[JoinTable(name: 'ak_amap_type_production_recherche')]
    #[JoinColumn(name: 'amap_tpr_fk_amap_id', referencedColumnName: 'amap_id')]
    #[InverseJoinColumn(name: 'amap_tpr_fk_type_production_id', referencedColumnName: 'tp_id')]
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class)]
    #[Groups(['wizardAmap'])]
    private \Doctrine\Common\Collections\Collection $tpRecherche;

    /**
     * @var Collection<int, \PsrLib\ORM\Entity\Amapien>
     */
    #[ORM\OneToMany(mappedBy: 'amap', targetEntity: \PsrLib\ORM\Entity\Amapien::class, cascade: ['persist', 'remove'])]
    private \Doctrine\Common\Collections\Collection $amapiens;

    /**
     * @var ArrayCollection<AmapCollectif>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapCollectif::class, mappedBy: 'amap', orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $collectifs;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ville::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(referencedColumnName: 'v_id')]
    #[Groups(['wizardAmap'])]
    private ?\PsrLib\ORM\Entity\Ville $adresseAdminVille = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Assert\NotNull(message: 'Le champ Association de fait est requis.')]
    #[Groups(['wizardAmap'])]
    private ?bool $associationDeFait = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min: 14, max: 14, exactMessage: 'Le SIRET doit faire exactement 14 caractères.')]
    #[Assert\Luhn(message: 'Le SIRET est invalide.')]
    #[Groups(['wizardAmap'])]
    private ?string $siret = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Regex(pattern: '~^W\d{9}$~', message: 'Le numéro RNA doit être composé de W suivi de 9 chiffres.')]
    #[Groups(['wizardAmap'])]
    private ?string $rna = null;

    /**
     * @var ArrayCollection<AdhesionAmap>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AdhesionAmap::class, mappedBy: 'amap')]
    private \Doctrine\Common\Collections\Collection $adhesions;

    /**
     * @var ArrayCollection<AmapRecherchePaysan>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapRecherchePaysan::class, mappedBy: 'amap', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $amapRecherchePaysans;

    /**
     * @var ArrayCollection<AdhesionAmapAmapien>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AdhesionAmapAmapien::class, mappedBy: 'creator', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $adhesionsAmapAmapienAsCreator;

    /**
     * @var ArrayCollection<User>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\User::class, mappedBy: 'adminAmaps')]
    #[Groups(['wizardAmap'])]
    private \Doctrine\Common\Collections\Collection $admins;

    /**
     * @var ArrayCollection<AmapienInvitation>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapienInvitation::class, mappedBy: 'amap', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $amapienInvitations;

    /**
     * @var ArrayCollection<AmapAbsence>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapAbsence::class, mappedBy: 'amap', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $absences;

    #[Groups(['wizardAmap'])]
    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\AmapLogo::class, cascade: ['ALL'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\AmapLogo $logo = null;

    /**
     * @var ArrayCollection<Campagne>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Campagne::class, mappedBy: 'amap', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $campagnes;

    /**
     * @var Collection<EvenementAbonnementAmap>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\EvenementAbonnementAmap::class, mappedBy: 'related', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $evementAbonnements;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->livraisonLieux = new ArrayCollection();
        $this->anneeAdhesions = new ArrayCollection();
        $this->tpPropose = new ArrayCollection();
        $this->amapiens = new ArrayCollection();
        $this->tpRecherche = new ArrayCollection();
        $this->adhesions = new ArrayCollection();
        $this->collectifs = new ArrayCollection();
        $this->admins = new ArrayCollection();
        $this->amapRecherchePaysans = new ArrayCollection();
        $this->adhesionsAmapAmapienAsCreator = new ArrayCollection();
        $this->amapienInvitations = new ArrayCollection();
        $this->absences = new ArrayCollection();
        $this->campagnes = new ArrayCollection();
        $this->evementAbonnements = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }

    /**
     * @return Ferme[]
     */
    public function getFermesWithRef()
    {
        // TODO: unit test
        $fermes = [];
        foreach ($this->getAmapiens() as $amapien) {
            foreach ($amapien->getRefProdFermes() as $ferme) {
                $fermes[] = $ferme;
            }
        }

        return array_unique($fermes);
    }

    public function hasContractWithWithTpActive(TypeProduction $tp): bool
    {
        $now = Carbon::now();
        foreach ($this->getLivraisonLieux() as $ll) {
            foreach ($ll->getModeleContrats() as $mc) {
                foreach ($mc->getProduits() as $produit) {
                    if ($produit->getTypeProduction() === $tp) {
                        foreach ($mc->getDates() as $date) {
                            if ($now->diffInDays($date->getDateLivraison()) <= 365) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function __toString(): string
    {
        return (string) $this->nom;
    }

    /**
     * @return AmapCommentaire[]
     */
    public function getCommentsOrdered()
    {
        return $this
            ->comments
            ->matching(AmapCommentaireRepository::criteriaOrderDate())
            ->toArray()
        ;
    }

    public function countActiveAmapiens(): int
    {
        return $this
            ->amapiens
            ->filter(fn (Amapien $amapien) => $amapien->estActif())
            ->count()
        ;
    }

    public function countAmapiensRef(): int
    {
        return count($this->getAmapiensRef());
    }

    /**
     * @return Amapien[]
     */
    public function getAmapiensRef(): array
    {
        return $this
            ->amapiens
            ->filter(fn (Amapien $amapien) => $amapien->getUser()->isRefProduitOfAmap($this))
            ->toArray()
        ;
    }

    public function getRegion(): ?Region
    {
        if (0 === $this->livraisonLieux->count()) {
            return null;
        }

        return $this->livraisonLieux[0]->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        if (0 === $this->livraisonLieux->count()) {
            return null;
        }

        return $this->livraisonLieux[0]->getDepartement();
    }

    /**
     * Get amapiens gestionnaires.
     *
     * @return Collection<int, Amapien>
     */
    public function getAmapiensGestionnaires()
    {
        return $this
            ->amapiens
            ->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire())
        ;
    }

    /**
     * @return ?int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): Amap
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Amap
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Add livraisonLieux.
     *
     * @return Amap
     */
    public function addLivraisonLieux(AmapLivraisonLieu $livraisonLieux)
    {
        $this->livraisonLieux[] = $livraisonLieux;

        return $this;
    }

    /**
     * Remove livraisonLieux.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeLivraisonLieux(AmapLivraisonLieu $livraisonLieux)
    {
        return $this->livraisonLieux->removeElement($livraisonLieux);
    }

    /**
     * @return ArrayCollection<AmapLivraisonLieu>
     */
    public function getLivraisonLieux()
    {
        return $this->livraisonLieux;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Amap
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): Amap
    {
        $this->password = $password;

        return $this;
    }

    public function getAmapienRefReseau(): ?Amapien
    {
        return $this->amapienRefReseau;
    }

    public function setAmapienRefReseau(?Amapien $amapienRefReseau): Amap
    {
        $this->amapienRefReseau = $amapienRefReseau;

        return $this;
    }

    public function getAmapienRefReseauSecondaire(): ?Amapien
    {
        return $this->amapienRefReseauSecondaire;
    }

    public function setAmapienRefReseauSecondaire(?Amapien $amapienRefReseauSecondaire): Amap
    {
        $this->amapienRefReseauSecondaire = $amapienRefReseauSecondaire;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): Amap
    {
        $this->etat = $etat;

        return $this;
    }

    public function getAmapEtudiante(): ?bool
    {
        return $this->amapEtudiante;
    }

    public function setAmapEtudiante(?bool $amapEtudiante): Amap
    {
        $this->amapEtudiante = $amapEtudiante;

        return $this;
    }

    /**
     * Add Comments.
     *
     * @return Amap
     */
    public function addComment(AmapCommentaire $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove Comments.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeComment(AmapCommentaire $comments)
    {
        return $this->comments->removeElement($comments);
    }

    /**
     * @return ArrayCollection<AmapCommentaire>
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function getAnneeCreation(): ?string
    {
        return $this->anneeCreation;
    }

    public function setAnneeCreation(?string $anneeCreation): Amap
    {
        $this->anneeCreation = $anneeCreation;

        return $this;
    }

    public function getNbAdherents(): ?string
    {
        return $this->nbAdherents;
    }

    public function setNbAdherents(?string $nbAdherents): Amap
    {
        $this->nbAdherents = $nbAdherents;

        return $this;
    }

    /**
     * Add tppPropose.
     *
     * @return Amap
     */
    public function addTpPropose(TypeProduction $tppPropose)
    {
        $this->tpPropose[] = $tppPropose;

        return $this;
    }

    /**
     * Remove tppPropose.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTpPropose(TypeProduction $tppPropose)
    {
        return $this->tpPropose->removeElement($tppPropose);
    }

    /**
     * Get tppPropose.
     *
     * @return Collection
     */
    public function getTpPropose()
    {
        return $this->tpPropose;
    }

    /**
     * @return TypeProduction[]
     */
    public function getTpProposeOrdered()
    {
        return $this
            ->tpPropose
            ->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())
            ->toArray()
        ;
    }

    /**
     * Add tpRecherche.
     *
     * @return Amap
     */
    public function addTpRecherche(TypeProduction $tpRecherche)
    {
        $this->tpRecherche[] = $tpRecherche;

        return $this;
    }

    /**
     * Remove tpRecherche.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTpRecherche(TypeProduction $tpRecherche)
    {
        return $this->tpRecherche->removeElement($tpRecherche);
    }

    /**
     * Get tpRecherche.
     *
     * @return Collection
     */
    public function getTpRecherche()
    {
        return $this->tpRecherche;
    }

    /**
     * Get tpRecherche.
     *
     * @return TypeProduction[]
     */
    public function getTpRechercheOrdered()
    {
        return $this->tpRecherche
            ->matching(\PsrLib\ORM\Repository\TypeProductionRepository::criteriaOrderId())
            ->toArray()
        ;
    }

    public function getAdresseAdmin(): ?string
    {
        return $this->adresseAdmin;
    }

    public function setAdresseAdmin(?string $adresseAdmin): Amap
    {
        $this->adresseAdmin = $adresseAdmin;

        return $this;
    }

    /**
     * Add amapien.
     *
     * @return Amap
     */
    public function addAmapien(Amapien $amapien)
    {
        $this->amapiens[] = $amapien;
        $amapien->setAmap($this);

        return $this;
    }

    /**
     * Remove amapien.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapien(Amapien $amapien)
    {
        return $this->amapiens->removeElement($amapien);
    }

    /**
     * Get amapiens.
     *
     * @return Collection<Amapien>
     */
    public function getAmapiens()
    {
        return $this->amapiens;
    }

    /**
     * Get amapiens par ordre alphabétique.
     */
    public function getAmapiensOrderedByName(): array
    {
        $amapiens = $this->amapiens->toArray();
        usort($amapiens, fn (Amapien $a1, Amapien $a2) => strcasecmp(
            (string) $a1->getUser()->getName()->getLastName(),
            (string) $a2->getUser()->getName()->getLastName()
        ));

        return $amapiens;
    }

    /**
     * Add anneeAdhesion.
     *
     * @return Amap
     */
    public function addAnneeAdhesion(AmapAnneeAdhesion $anneeAdhesion)
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    /**
     * Remove anneeAdhesion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnneeAdhesion(AmapAnneeAdhesion $anneeAdhesion)
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    /**
     * Get anneeAdhesions.
     *
     * @return Collection<AmapAnneeAdhesion>
     */
    public function getAnneeAdhesions()
    {
        return $this->anneeAdhesions;
    }

    /**
     * @return AmapAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, fn (AmapAnneeAdhesion $a, AmapAnneeAdhesion $b) => $b->getAnnee() - $a->getAnnee());

        return $adhesions;
    }

    /**
     * Add collectif.
     *
     * @return Amap
     */
    public function addCollectif(AmapCollectif $collectif)
    {
        $this->collectifs[] = $collectif;

        return $this;
    }

    /**
     * Remove collectif.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCollectif(AmapCollectif $collectif)
    {
        return $this->collectifs->removeElement($collectif);
    }

    /**
     * Get collectifs.
     *
     * @return Collection
     */
    public function getCollectifs()
    {
        return $this->collectifs;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Amap
    {
        $this->url = $url;

        return $this;
    }

    public function isPossedeAssurance(): ?bool
    {
        return $this->possedeAssurance;
    }

    public function setPossedeAssurance(?bool $possedeAssurance): Amap
    {
        $this->possedeAssurance = $possedeAssurance;

        return $this;
    }

    public function getAdresseAdminVille(): ?Ville
    {
        return $this->adresseAdminVille;
    }

    public function setAdresseAdminVille(?Ville $adresseAdminVille): Amap
    {
        $this->adresseAdminVille = $adresseAdminVille;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getAssociationDeFait(): ?bool
    {
        return $this->associationDeFait;
    }

    public function setAssociationDeFait(?bool $associationDeFait): Amap
    {
        $this->associationDeFait = $associationDeFait;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): Amap
    {
        $this->siret = $siret;

        return $this;
    }

    public function getRna(): ?string
    {
        return $this->rna;
    }

    public function setRna(?string $rna): Amap
    {
        $this->rna = $rna;

        return $this;
    }

    public function getAdresseAdminNom(): ?string
    {
        return $this->adresseAdminNom;
    }

    public function setAdresseAdminNom(?string $adresseAdminNom): Amap
    {
        $this->adresseAdminNom = $adresseAdminNom;

        return $this;
    }

    /**
     * Add admin.
     *
     * @return self
     */
    public function addAdmin(User $admin)
    {
        $this->admins[] = $admin;

        return $this;
    }

    /**
     * Remove admin.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAdmin(User $admin)
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * Get admins.
     *
     * @return ArrayCollection<User>
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    public function getAdhesionsAmapAmapienAsCreator(): Collection
    {
        return $this->adhesionsAmapAmapienAsCreator;
    }

    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function getLogo(): ?AmapLogo
    {
        return $this->logo;
    }

    public function setLogo(?AmapLogo $logo): Amap
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Add campagne.
     */
    public function addCampagne(Campagne $campagne): Amap
    {
        $this->campagnes[] = $campagne;

        return $this;
    }

    /**
     * Remove campagne.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCampagne(Campagne $campagne)
    {
        return $this->campagnes->removeElement($campagne);
    }

    /**
     * Get campagnes.
     *
     * @return ArrayCollection<Campagne>
     */
    public function getCampagnes()
    {
        return $this->campagnes;
    }

    public function getEvementAbonnements(): Collection
    {
        return $this->evementAbonnements;
    }
}
