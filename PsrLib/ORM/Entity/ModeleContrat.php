<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Repository\ModeleContratDateRepository;
use PsrLib\ORM\Repository\ModeleContratDatesReglementRepository;
use PsrLib\Workflow\ContratStatusWorkflow;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Auditable]
#[ORM\Table(name: 'ak_modele_contrat')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ModeleContratRepository::class)]
class ModeleContrat implements \Stringable
{
    final public const frequences_available = [
        'Toutes les semaines' => 7,
        'Tous les quinze jours' => 14,
    ];

    public const mode_carte = 'carte';
    public const mode_cheque = 'cheque';

    public const mode_available = [
        self::mode_cheque,
        'virement',
        'sepa',
        self::mode_carte,
    ];

    #[ORM\Column(name: 'mc_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard', 'wizardContract'])]
    private ?int $id = null;

    #[ORM\Column(name: 'mc_date_activation', type: 'datetime', nullable: true)]
    #[Groups(['wizard'])]
    private ?\DateTime $dateActivation = null;

    #[ORM\Column(name: 'mc_nom', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank(groups: ['form1'])]
    #[Assert\Length(max: 200, groups: ['form1'])]
    #[Groups(['wizard'])]
    private ?string $nom = null;

    #[ORM\Column(name: 'mc_description', type: 'text', length: 65535, nullable: true)]
    #[Groups(['wizard'])]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\AmapLivraisonLieu::class, inversedBy: 'modeleContrats')]
    #[ORM\JoinColumn(name: 'mc_livraison_lieu', referencedColumnName: 'amap_liv_lieu_id')]
    #[Assert\NotNull(message: 'Merci de sélectionner un lieu de livraison', groups: ['form1'])]
    #[Groups(['wizard'])]
    private ?\PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu = null;

    #[ORM\Column(name: 'mc_frequence', type: 'integer', nullable: true)]
    #[Assert\Choice(choices: ModeleContrat::frequences_available, groups: ['form2'])]
    #[Groups(['wizard'])]
    private ?int $frequence = null;

    #[ORM\Column(name: 'mc_forclusion', type: 'date', nullable: true)]
    #[Assert\NotNull(groups: ['form2'])]
    #[Groups(['wizard'])]
    private ?\DateTime $forclusion = null;

    #[ORM\Column(name: 'mc_modalites', type: 'integer', nullable: true)]
    #[Groups(['wizard'])]
    private ?int $modalites = null;

    #[ORM\Column(name: 'mc_choix_identiques', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    private ?bool $choixIdentiques = null;

    #[ORM\Column(name: 'mc_frequence_articles', type: 'integer', nullable: true)]
    #[Groups(['wizard'])]
    private ?int $frequenceArticles = null;

    #[ORM\Column(name: 'mc_etat', type: 'string', length: 255, nullable: true, enumType: ModeleContratEtat::class)]
    #[Groups(['wizard'])]
    private ?ModeleContratEtat $etat = ModeleContratEtat::ETAT_BROUILLON;

    #[ORM\Column(name: 'mc_version', type: 'integer')]
    #[Groups(['wizard'])]
    private int $version = 2;

    #[ORM\Column(name: 'mc_filiere', type: 'text', length: 65535, nullable: true)]
    #[Assert\NotBlank(groups: ['form1'])]
    #[Assert\Length(max: 200, groups: ['form1'])]
    #[Groups(['wizard'])]
    private ?string $filiere = null;

    #[ORM\Column(name: 'mc_specificite', type: 'text', length: 65535, nullable: true)]
    #[Assert\Length(max: 400, groups: ['form1'])]
    #[Groups(['wizard'])]
    private ?string $specificite = null;

    #[ORM\Column(name: 'mc_regul_poid', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    private ?bool $regulPoid = null;

    #[ORM\Column(name: 'mc_amapien_permission_intervenir_planning', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    private ?bool $amapienPermissionIntervenirPlanning = null;

    #[ORM\Column(name: 'mc_reglement_type', type: 'simple_array', nullable: true)]
    #[Assert\Count(min: 1, minMessage: 'Merci de sélectionner au moins un mode de règlement', groups: ['form6'])]
    #[Groups(['wizard'])]
    private ?array $reglementType = [];

    #[ORM\Column(name: 'mc_reglement_modalite', type: 'text', length: 65535, nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\Length(max: 65535)]
    private ?string $reglementModalite = null;

    #[ORM\Column(name: 'mc_reglement_nb_max', type: 'integer', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form6'])]
    #[Assert\Range(min: 1, minMessage: 'Le nombre de règlements maximum doit être supérieur ou égal à 1', groups: ['form6'])]
    private ?int $reglementNbMax = null;

    #[ORM\Column(name: 'mc_contrat_annexes', type: 'text', length: 65535, nullable: true)]
    #[Groups(['wizard'])]
    private ?string $contratAnnexes = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'modeleContrats')]
    #[ORM\JoinColumn(name: 'mc_fk_ferme_id', referencedColumnName: 'f_id')]
    #[Groups(['wizard'])]
    private ?\PsrLib\ORM\Entity\Ferme $ferme = null;

    /**
     * @var ArrayCollection<Contrat>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Contrat::class, mappedBy: 'modeleContrat')]
    #[Groups(['wizard'])]
    private \Doctrine\Common\Collections\Collection $contrats;

    /**
     * @var ArrayCollection<ModeleContratDate>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ModeleContratDate::class, mappedBy: 'modeleContrat', orphanRemoval: true, cascade: ['persist', 'remove'])]
    #[Assert\Count(min: 1, minMessage: "Les dates de livraisons sont obligatoires, veuillez cliquer sur le bouton 'générer des dates'", groups: ['form2'])]
    #[Assert\All(new Assert\Expression(expression: 'value.getDateLivraison() !== null', message: 'Merci de sélectionner une date de livraison', groups: ['form2']))]
    #[Groups(['wizard'])]
    private \Doctrine\Common\Collections\Collection $dates;

    /**
     * @var ArrayCollection<ModeleContratDatesReglement>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ModeleContratDatesReglement::class, mappedBy: 'modeleContrat', orphanRemoval: true, cascade: ['persist', 'remove'])]
    #[Groups(['wizard'])]
    private \Doctrine\Common\Collections\Collection $dateReglements;

    /**
     * @var ArrayCollection<ModeleContratProduit>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ModeleContratProduit::class, mappedBy: 'modeleContrat', orphanRemoval: true, cascade: ['persist'])]
    #[Groups(['wizard'])]
    #[Assert\Valid(groups: ['form3'])]
    #[Assert\Count(min: 1, minMessage: 'Merci de sélectionner au moins un produit', groups: ['form3'])]
    private \Doctrine\Common\Collections\Collection $produits;

    #[Groups(['wizard'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\NotBlank(groups: ['form2', 'form_delay'])]
    #[Assert\GreaterThan(0, groups: ['form2'])]
    #[Assert\GreaterThanOrEqual(0, groups: ['form_delay'])]
    private ?int $delaiModifContrat = null;

    #[ORM\Column(type: 'string')]
    private string $uuid;

    #[Groups(['wizard'])]
    #[ORM\Embedded(class: ModeleContratInformationLivraison::class)]
    #[Assert\Valid(groups: ['form4'])]
    private ModeleContratInformationLivraison $informationLivraison;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->contrats = new ArrayCollection();
        $this->dates = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->dateReglements = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
        $this->informationLivraison = new ModeleContratInformationLivraison();
    }

    public function __toString(): string
    {
        return (string) $this->getNom();
    }

    #[Assert\Callback(groups: ['form6'])]
    public function validateDateReglementNoDuplication(ExecutionContextInterface $context): void
    {
        $dates = array_map(fn (ModeleContratDatesReglement $date) => $date->getDate()?->getTimestamp(), $this->getDateReglements()->toArray());
        $dates = array_filter($dates, fn ($date) => null !== $date);
        if (count($dates) !== count(array_unique($dates))) {
            $context
                ->buildViolation('Les dates de règlement ne doivent pas être identiques')
                ->atPath('dateReglements')
                ->addViolation()
            ;
        }
    }

    #[Assert\Callback(groups: ['form6'])]
    public function validateDateReglementForclusion(ExecutionContextInterface $context): void
    {
        if (null === $this->getForclusion()) {
            return;
        }

        $forclusion = Carbon::instance($this->getForclusion());

        foreach ($this->getDateReglements() as $dateReglement) {
            $date = $dateReglement->getDate();

            if (null === $date) {
                continue;
            }

            if ($forclusion->gte($date)) {
                $context
                    ->buildViolation('Les dates de règlement ne peuvent pas être inférieures ou égales à la date de fin de souscription')
                    ->atPath('dateReglements')
                    ->addViolation()
                ;

                return;
            }
        }
    }

    #[Assert\Callback(groups: ['form6'])]
    public function validateDateReglementNotAfterLastDate(ExecutionContextInterface $context): void
    {
        $reglementDates = array_map(fn (ModeleContratDatesReglement $date) => $date->getDate(), $this->getDateReglements()->toArray());
        $reglementDates = array_filter($reglementDates, fn ($date) => null !== $date);

        $dates = array_map(fn (ModeleContratDate $date) => $date->getDateLivraison(), $this->getDates()->toArray());
        $dates = array_filter($dates, fn ($date) => null !== $date);

        if (empty($dates) || empty($reglementDates)) {
            return;
        }

        // Inserse sort
        usort($reglementDates, fn ($a, $b) => $b->getTimestamp() - $a->getTimestamp());
        usort($dates, fn ($a, $b) => $b->getTimestamp() - $a->getTimestamp());

        if (Carbon::instance($dates[0])->lt($reglementDates[0])) {
            $context
                ->buildViolation('Les dates de règlement ne peuvent pas être après la fin des livraisons')
                ->atPath('dateReglements')
                ->addViolation()
            ;

            return;
        }
    }

    #[Assert\Callback(groups: ['form3'])]
    public function validateDateDuplicationMcp(ExecutionContextInterface $context): void
    {
        $fermeProduitIds = $this
            ->getProduits()
            ->map(fn (ModeleContratProduit $produit) => $produit->getFermeProduit()->getId())
            ->toArray()
        ;
        if (count($fermeProduitIds) !== count(array_unique($fermeProduitIds))) {
            $context
                ->buildViolation('Impossible d\'indiquer plusieurs fois le même produit')
                ->atPath('produits')
                ->addViolation()
            ;
        }
    }

    public function isCdp(): bool
    {
        $ferme = $this->getFerme();
        if (null === $ferme) {
            return false;
        }

        $regroupement = $ferme->getRegroupement();
        if (null === $regroupement) {
            return false;
        }

        return $regroupement->isCdp();
    }

    public function getAmap(): Amap
    {
        return $this->getLivraisonLieu()?->getAmap();
    }

    public function isRegul(): bool
    {
        foreach ($this->getProduits() as $produit) {
            if (true === $produit->getRegulPds()) {
                return true;
            }
        }

        return false;
    }

    public function getNbLivPlafond(): ?int
    {
        $infoLivraison = $this->getInformationLivraison();
        if ($infoLivraison->getNblivPlancherDepassement()) {
            return $this->getDates()->count();
        }

        return $infoLivraison->getNblivPlancher();
    }

    public function getDatePosition(ModeleContratDate $date): ?int
    {
        $dates = $this->getDates()->toArray();
        if (0 === count($dates)) {
            return null;
        }
        usort($dates, function (ModeleContratDate $mcd1, ModeleContratDate $mcd2) {
            $mcd1d = $mcd1->getDateLivraison();
            $mcd2d = $mcd2->getDateLivraison();
            if (null === $mcd1d && null === $mcd2d) {
                return 0;
            }

            return $mcd1d->getTimestamp() - $mcd2d->getTimestamp();
        });
        $key = array_search($date, $dates, true);
        if (false === $key) {
            return null;
        }

        return $key + 1;
    }

    public function isArchived(): bool
    {
        $now = new Carbon();
        $now->startOfDay();

        /** @var ModeleContratDate $date */
        foreach ($this->getDates() as $date) {
            if ($now->lessThanOrEqualTo($date->getDateLivraison())) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return ModeleContratDate[]
     */
    public function getFutureDates(): array
    {
        $now = Carbon::today();
        $res = $this
            ->getDates()
            ->matching(ModeleContratDateRepository::criteriaOrderDateAsc())
            ->filter(fn (ModeleContratDate $date) => $now->lte($date->getDateLivraison()))
            ->toArray()
        ;

        return array_values($res);
    }

    public function getLastDate(): ?ModeleContratDate
    {
        $dates = $this->getDates()->toArray();
        if (0 === count($dates)) {
            return null;
        }

        usort($dates, function (ModeleContratDate $a, ModeleContratDate $b) {
            $ac = Carbon::instance($a->getDateLivraison());
            $bc = Carbon::instance($b->getDateLivraison());

            return $bc->getTimestamp() - $ac->getTimestamp();
        });

        return $dates[0];
    }

    public function isNowBeforeLastDate(): bool
    {
        $now = new Carbon();
        $lastDate = $this->getLastDate();

        return null !== $lastDate && $now->lessThanOrEqualTo($lastDate->getDateLivraison());
    }

    /**
     * @return ModeleContratDate[]
     */
    public function getDatesOrderedAsc()
    {
        return $this
            ->dates
            ->matching(ModeleContratDateRepository::criteriaOrderDateAsc())
            ->toArray()
        ;
    }

    /**
     * @return Collection<ModeleContratDatesReglement>
     */
    public function getDatesReglementOrderedAsc()
    {
        return $this
            ->dateReglements
            ->matching(ModeleContratDatesReglementRepository::criteriaOrderDateAsc())
        ;
    }

    public function getPremiereDateLivrableAvecDelais(\DateTime $date): Carbon
    {
        $dateCarbon = Carbon::instance($date)->startOfDay();
        $delais = $this->getDelaiModifContrat();
        if (null === $delais || $delais <= 0) {
            $delais = 0;
        }

        return $dateCarbon->addDays($delais);
    }

    /**
     * @param \PsrLib\ORM\Entity\ContratCellule[] $cellules
     */
    public function getMcDateFromMcCellules($cellules): ?ModeleContratDate
    {
        foreach ($cellules as $cellule) {
            if ($cellule->getContrat()->getModeleContrat()->getId() === $this->getId()) {
                return $cellule->getModeleContratDate();
            }
        }

        return null;
    }

    public function isForclusionPassed(): bool
    {
        $now = Carbon::now()->subDay();

        return $now->greaterThan($this->getForclusion());
    }

    #[Assert\Callback(groups: ['form6'])]
    public function validateReglementNbMax(ExecutionContextInterface $context): void
    {
        if (null !== $this->getReglementNbMax()
            && $this->getReglementNbMax() > $this->getDateReglements()->count()) {
            $context
                ->buildViolation('Le nombre de règlements maximum doit être inférieur ou égal au nombre de dates des règlements')
                ->atPath('reglementNbMax')
                ->addViolation()
            ;

            return;
        }
    }

    #[Assert\Callback(groups: ['form4'])]
    public function validatePermissionDeplacementLivraison(ExecutionContextInterface $context): void
    {
        $mcInfoLivraison = $this->getInformationLivraison();
        if (true === $mcInfoLivraison->getProduitsIdentiqueAmapien()
            && true === $mcInfoLivraison->getProduitsIdentiquePaysan()
        ) {
            if (null === $mcInfoLivraison->getAmapienPermissionDeplacementLivraison()
                && $mcInfoLivraison->getNblivPlancher() < $this->getDates()->count()) {
                $context
                    ->buildViolation('Le champ ci-dessous est requis à la saisie pour passer à l\'étape suivante')
                    ->atPath('amapienPermissionDeplacementLivraison')
                    ->addViolation()
                ;

                return;
            }
        }
    }

    #[Assert\Callback(groups: ['form2'])]
    public function validateDateForclusion(ExecutionContextInterface $context): void
    {
        $dateForclusion = $this->getForclusion();
        if (null === $dateForclusion) {
            return;
        }

        $dateForclusionAvecDelaisFerme = $this->getPremiereDateLivrableAvecDelais($dateForclusion);

        foreach ($this->getDates()->toArray() as $dateLivraison) {
            if ($dateForclusionAvecDelaisFerme->gt($dateLivraison->getDateLivraison())) {
                $context
                    ->buildViolation('La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)')
                    ->atPath('forclusion')
                    ->addViolation()
                ;

                return;
            }
        }
    }

    #[Assert\Callback(groups: ['form2'])]
    public function validateDateUnicity(ExecutionContextInterface $context): void
    {
        $dates = $this->getDates()->map(function (ModeleContratDate $date) {
            if (null === $date->getDateLivraison()) {
                return null;
            }

            return $date->getDateLivraison()->format('d/m/Y');
        })->toArray();
        if (count($dates) !== count(array_unique($dates))) {
            $context
                ->buildViolation('Merci de sélectionner une seule fois chaque date')
                ->atPath('dates')
                ->addViolation()
            ;
        }
    }

    public function getContratsValidated(): Collection
    {
        return $this->contrats->filter(fn (Contrat $contrat) => ContratStatusWorkflow::STATE_VALIDATED === $contrat->getState());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ModeleContrat
    {
        $this->id = $id;

        return $this;
    }

    public function getLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }

    public function setLivraisonLieu(?AmapLivraisonLieu $livraisonLieu): ModeleContrat
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }

    public function getFerme(): ?Ferme
    {
        return $this->ferme;
    }

    public function setFerme(?Ferme $ferme): ModeleContrat
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getDateActivation(): ?\DateTime
    {
        return $this->dateActivation;
    }

    public function setDateActivation(?\DateTime $dateActivation): ModeleContrat
    {
        $this->dateActivation = $dateActivation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): ModeleContrat
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ModeleContrat
    {
        $this->description = $description;

        return $this;
    }

    public function getFrequence(): ?int
    {
        return $this->frequence;
    }

    public function setFrequence(?int $frequence): ModeleContrat
    {
        $this->frequence = $frequence;

        return $this;
    }

    public function getForclusion(): ?\DateTime
    {
        return $this->forclusion;
    }

    public function setForclusion(?\DateTime $forclusion): ModeleContrat
    {
        $this->forclusion = $forclusion;

        return $this;
    }

    public function getModalites(): ?int
    {
        return $this->modalites;
    }

    public function setModalites(?int $modalites): ModeleContrat
    {
        $this->modalites = $modalites;

        return $this;
    }

    public function getChoixIdentiques(): ?bool
    {
        return $this->choixIdentiques;
    }

    public function setChoixIdentiques(?bool $choixIdentiques): ModeleContrat
    {
        $this->choixIdentiques = $choixIdentiques;

        return $this;
    }

    public function getFrequenceArticles(): ?int
    {
        return $this->frequenceArticles;
    }

    public function setFrequenceArticles(?int $frequenceArticles): ModeleContrat
    {
        $this->frequenceArticles = $frequenceArticles;

        return $this;
    }

    public function getEtat(): ?ModeleContratEtat
    {
        return $this->etat;
    }

    public function setEtat(?ModeleContratEtat $etat): ModeleContrat
    {
        $this->etat = $etat;

        return $this;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): ModeleContrat
    {
        $this->version = $version;

        return $this;
    }

    public function getFiliere(): ?string
    {
        return $this->filiere;
    }

    public function setFiliere(?string $filiere): ModeleContrat
    {
        $this->filiere = $filiere;

        return $this;
    }

    public function getSpecificite(): ?string
    {
        return $this->specificite;
    }

    public function setSpecificite(?string $specificite): ModeleContrat
    {
        $this->specificite = $specificite;

        return $this;
    }

    public function getRegulPoid(): ?bool
    {
        return $this->regulPoid;
    }

    public function setRegulPoid(?bool $regulPoid): ModeleContrat
    {
        $this->regulPoid = $regulPoid;

        return $this;
    }

    public function getAmapienPermissionIntervenirPlanning(): ?bool
    {
        return $this->amapienPermissionIntervenirPlanning;
    }

    public function setAmapienPermissionIntervenirPlanning(?bool $amapienPermissionIntervenirPlanning): ModeleContrat
    {
        $this->amapienPermissionIntervenirPlanning = $amapienPermissionIntervenirPlanning;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getReglementType(): array
    {
        if (null === $this->reglementType) {
            return [];
        }

        return $this->reglementType;
    }

    /**
     * @param string[] $reglementType
     */
    public function setReglementType(array $reglementType): ModeleContrat
    {
        $this->reglementType = $reglementType;

        return $this;
    }

    public function getReglementModalite(): ?string
    {
        return $this->reglementModalite;
    }

    public function setReglementModalite(?string $reglementModalite): ModeleContrat
    {
        $this->reglementModalite = $reglementModalite;

        return $this;
    }

    public function getReglementNbMax(): ?int
    {
        return $this->reglementNbMax;
    }

    public function setReglementNbMax(?int $reglementNbMax): ModeleContrat
    {
        $this->reglementNbMax = $reglementNbMax;

        return $this;
    }

    public function getContratAnnexes(): ?string
    {
        return $this->contratAnnexes;
    }

    public function setContratAnnexes(?string $contratAnnexes): ModeleContrat
    {
        $this->contratAnnexes = $contratAnnexes;

        return $this;
    }

    /**
     * Add contrat.
     *
     * @return ModeleContrat
     */
    public function addContrat(Contrat $contrat)
    {
        $this->contrats[] = $contrat;

        return $this;
    }

    /**
     * Remove contrat.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeContrat(Contrat $contrat)
    {
        return $this->contrats->removeElement($contrat);
    }

    /**
     * Get contrats.
     *
     * @return Collection<Contrat>
     */
    public function getContrats()
    {
        return $this->contrats;
    }

    /**
     * Add date.
     *
     * @return ModeleContrat
     */
    public function addDate(ModeleContratDate $date)
    {
        $this->dates[] = $date;
        $date->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove date.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDate(ModeleContratDate $date)
    {
        $date->setModeleContrat(null);

        return $this->dates->removeElement($date);
    }

    /**
     * Get dates.
     *
     * @return Collection<ModeleContratDate>
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Add dateReglement.
     *
     * @return ModeleContrat
     */
    public function addDateReglement(ModeleContratDatesReglement $dateReglement)
    {
        $this->dateReglements[] = $dateReglement;
        $dateReglement->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove dateReglement.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDateReglement(ModeleContratDatesReglement $dateReglement)
    {
        $dateReglement->setModeleContrat(null);

        return $this->dateReglements->removeElement($dateReglement);
    }

    /**
     * Get dateReglements.
     *
     * @return Collection<ModeleContratDatesReglement>
     */
    public function getDateReglements()
    {
        return $this->dateReglements;
    }

    /**
     * Add produit.
     *
     * @return ModeleContrat
     */
    public function addProduit(ModeleContratProduit $produit)
    {
        $this->produits[] = $produit;
        $produit->setModeleContrat($this);

        return $this;
    }

    /**
     * Remove produit.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProduit(ModeleContratProduit $produit)
    {
        $produit->setModeleContrat(null);

        return $this->produits->removeElement($produit);
    }

    /**
     * Get produits.
     *
     * @return Collection<ModeleContratProduit>
     */
    public function getProduits()
    {
        return $this->produits;
    }

    public function getDelaiModifContrat(): ?int
    {
        return $this->delaiModifContrat;
    }

    public function setDelaiModifContrat(?int $delaiModifContrat): ModeleContrat
    {
        $this->delaiModifContrat = $delaiModifContrat;

        return $this;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getInformationLivraison(): ModeleContratInformationLivraison
    {
        return $this->informationLivraison;
    }

    public function setInformationLivraison(ModeleContratInformationLivraison $informationLivraison): ModeleContrat
    {
        $this->informationLivraison = $informationLivraison;

        return $this;
    }
}
