<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_document_utilisateur')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\DocumentUtilisateurRepository::class)]
#[ORM\InheritanceType('JOINED')]
class DocumentUtilisateur
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer', nullable: false)]
    private int $annee;

    #[ORM\Column(type: 'string', nullable: false, length: 255)]
    private string $nom;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\DocumentUtilisateur::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Assert\NotNull]
    #[ORM\JoinColumn(nullable: false)]
    private \PsrLib\ORM\Entity\Files\DocumentUtilisateur $ficher;

    public function __construct(int $annee, string $nom, Files\DocumentUtilisateur $ficher)
    {
        $this->annee = $annee;
        $this->nom = $nom;
        $this->ficher = $ficher;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getFicher(): Files\DocumentUtilisateur
    {
        return $this->ficher;
    }
}
