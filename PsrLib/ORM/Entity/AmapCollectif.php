<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['profil', 'amapien', 'amap'], message: "Le rôle est déjà pris dans l'AMAP", errorPath: '')]
#[ORM\Table(name: 'ak_amap_collectif')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapCollectifRepository::class)]
class AmapCollectif
{
    final public const PROFIL_PRESIDENT = 'president';
    final public const PROFIL_SECRETAIRE = 'secretaire';
    final public const PROFIL_TRESORIER = 'tresorier';
    final public const PROFIL_AUTRE = 'autre';
    final public const PROFILS = [
        self::PROFIL_PRESIDENT,
        self::PROFIL_SECRETAIRE,
        self::PROFIL_TRESORIER,
        self::PROFIL_AUTRE,
    ];

    /**
     * @var ?int
     */
    #[ORM\Column(name: 'amap_coll_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: 'amap_coll_profil', type: 'string', length: 150, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: AmapCollectif::PROFILS)]
    private ?string $profil = null;

    #[ORM\Column(name: 'amap_coll_precision', type: 'string', length: 50, nullable: true)]
    #[Assert\Length(max: 50)]
    private ?string $precision = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'collectifs')]
    #[ORM\JoinColumn(name: 'amap_coll_fk_amap_id', referencedColumnName: 'amap_id', nullable: false)]
    #[Assert\NotNull]
    private \PsrLib\ORM\Entity\Amap $amap;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amapien::class, inversedBy: 'collectifs')]
    #[Assert\NotNull]
    #[ORM\JoinColumn(name: 'amap_coll_fk_amapien_id', referencedColumnName: 'a_id')]
    private ?\PsrLib\ORM\Entity\Amapien $amapien = null;

    /**
     * AmapCollectif constructor.
     */
    public function __construct(Amap $amap)
    {
        $this->amap = $amap;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap): AmapCollectif
    {
        $this->amap = $amap;

        return $this;
    }

    public function getProfil(): ?string
    {
        return $this->profil;
    }

    public function setProfil(?string $profil): AmapCollectif
    {
        $this->profil = $profil;

        return $this;
    }

    public function getPrecision(): ?string
    {
        return $this->precision;
    }

    public function setPrecision(?string $precision): AmapCollectif
    {
        $this->precision = $precision;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): void
    {
        $this->amapien = $amapien;
    }
}
