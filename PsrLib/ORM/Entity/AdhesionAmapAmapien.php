<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AdhesionAmapAmapienRepository::class)]
class AdhesionAmapAmapien extends Adhesion
{
    /**
     * @var ?int
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[GeneratedValue]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Amapien::class)]
    #[ORM\JoinColumn(referencedColumnName: 'a_id')]
    protected ?Amapien $amapien;

    /**
     * @var AdhesionValueAmapAmapien
     */
    #[ORM\OneToOne(targetEntity: AdhesionValueAmapAmapien::class, cascade: ['ALL'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Valid]
    protected $value;

    /**
     * @var Amap
     */
    #[ORM\ManyToOne(targetEntity: Amap::class, inversedBy: 'adhesionsAmapAmapienAsCreator')]
    #[ORM\JoinColumn(referencedColumnName: 'amap_id', nullable: false)]
    #[Assert\Expression('value.getAmapiens().contains(this.getAmapien())', groups: ['import'], message: "Erreur d'accès à l'amapien")]
    protected $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(Amapien $amapien, AdhesionValueAmapAmapien $value, Amap $creator)
    {
        $this->amapien = $amapien;
        $this->value = $value;
        $this->creator = $creator;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): void
    {
        $this->amapien = $amapien;
    }

    public function getValue(): AdhesionValueAmapAmapien
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueAmapAmapien $value
     *
     * @return AdhesionAmapAmapien
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function getCreator(): Amap
    {
        return $this->creator;
    }

    public function setCreator(Amap $creator): AdhesionAmapAmapien
    {
        $this->creator = $creator;

        return $this;
    }
}
