<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

#[Auditable]
#[ORM\UniqueConstraint(columns: ['user_id', 'amap_id'])]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapienRepository::class)]
#[ORM\Table(name: 'ak_amapien')]
#[SoftDeleteable(fieldName: 'deletedAt', hardDelete: false)]
class Amapien implements \Stringable, ActivableUserInterface, LocalisableEntity
{
    use ActivableUserTrait;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\Column(name: 'a_id', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\User::class, inversedBy: 'amaps', cascade: ['PERSIST'])]
    #[ORM\JoinColumn(name: 'user_id', nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'amapiens')]
    #[ORM\JoinColumn(name: 'amap_id', referencedColumnName: 'amap_id')]
    private ?Amap $amap;

    /**
     * @var ArrayCollection<int, AmapienAnneeAdhesion>
     */
    #[ORM\OneToMany(
        mappedBy: 'amapienAmap',
        targetEntity: \PsrLib\ORM\Entity\AmapienAnneeAdhesion::class,
        cascade: ['PERSIST'],
        orphanRemoval: true
    )]
    private \Doctrine\Common\Collections\Collection $anneeAdhesions;

    /**
     * @var ArrayCollection<Ferme>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Ferme::class, mappedBy: 'amapienRefs')]
    #[ORM\InverseJoinColumn(referencedColumnName: 'f_id')]
    private \Doctrine\Common\Collections\Collection $refProdFermes;

    /**
     * @var ArrayCollection<Ferme>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'amapienAttentes')]
    #[JoinTable(name: 'ak_ferme_amapien_liste_attente')]
    #[ORM\JoinColumn(name: 'f_a_la_fk_amapien_id', referencedColumnName: 'a_id')]
    #[ORM\InverseJoinColumn(name: 'f_a_la_fk_ferme_id', referencedColumnName: 'f_id')]
    private \Doctrine\Common\Collections\Collection $fermeAttentes;

    #[ORM\Column(type: 'boolean')]
    private bool $gestionnaire = false;

    /**
     * @var ArrayCollection<int, AmapDistribution>
     */
    #[ORM\ManyToMany(targetEntity: AmapDistribution::class, mappedBy: 'amapiens')]
    private \Doctrine\Common\Collections\Collection $distributions;

    /**
     * @var ArrayCollection<Contrat>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Contrat::class, mappedBy: 'amapien')]
    private \Doctrine\Common\Collections\Collection $contrats;

    #[ORM\Column(name: 'a_etat', type: 'string', length: 50, nullable: false)]
    private string $etat = ActivableUserInterface::ETAT_INACTIF;

    /**
     * @var ArrayCollection<AmapCollectif>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AmapCollectif::class, mappedBy: 'amapien', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $collectifs;

    /**
     * @var ArrayCollection<ContratLivraison>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ContratLivraison::class, mappedBy: 'amapien', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $livraisons;

    /**
     * @var \Doctrine\Common\Collections\Collection<CampagneBulletin>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\CampagneBulletin::class, mappedBy: 'amapien', cascade: ['ALL'], orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $bulletins;

    public function __construct(User $user, Amap $amap)
    {
        $amap->addAmapien($this);
        $this->user = $user;
        $this->amap = $amap;
        $this->anneeAdhesions = new ArrayCollection();
        $this->refProdFermes = new ArrayCollection();
        $this->fermeAttentes = new ArrayCollection();
        $this->distributions = new ArrayCollection();
        $this->collectifs = new ArrayCollection();
        $this->livraisons = new ArrayCollection();
        $this->bulletins = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->user;
    }

    public function isRefProduit(): bool
    {
        return $this->refProdFermes->count() > 0;
    }

    /**
     * @return AmapienAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, fn (AmapienAnneeAdhesion $a, AmapienAnneeAdhesion $b) => $b->getAnnee() - $a->getAnnee());

        return $adhesions;
    }

    public function getRegion(): ?Region
    {
        return $this->getAmap()?->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getAmap()?->getDepartement();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): void
    {
        $this->amap = $amap;
    }

    public function addAnneeAdhesion(AmapienAnneeAdhesion $anneeAdhesion): self
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    public function removeAnneeAdhesion(AmapienAnneeAdhesion $anneeAdhesion): bool
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    public function getAnneeAdhesions(): Collection
    {
        return $this->anneeAdhesions;
    }

    public function addRefProdFerme(Ferme $refProdFerme): self
    {
        $this->refProdFermes[] = $refProdFerme;

        return $this;
    }

    public function removeRefProdFerme(Ferme $refProdFerme): bool
    {
        return $this->refProdFermes->removeElement($refProdFerme);
    }

    public function getRefProdFermes(): Collection
    {
        return $this->refProdFermes;
    }

    public function addFermeAttente(Ferme $fermeAttente): self
    {
        $this->fermeAttentes[] = $fermeAttente;

        return $this;
    }

    public function removeFermeAttente(Ferme $fermeAttente): bool
    {
        return $this->fermeAttentes->removeElement($fermeAttente);
    }

    public function getFermeAttentes(): Collection
    {
        return $this->fermeAttentes;
    }

    public function isGestionnaire(): bool
    {
        return $this->gestionnaire;
    }

    public function setGestionnaire(bool $gestionnaire): void
    {
        $this->gestionnaire = $gestionnaire;
    }

    /**
     * @return Collection<AmapDistribution>
     */
    public function getDistributions(): Collection
    {
        return $this->distributions;
    }

    public function addContrat(Contrat $contrat): self
    {
        $this->contrats[] = $contrat;

        return $this;
    }

    public function removeContrat(Contrat $contrat): bool
    {
        return $this->contrats->removeElement($contrat);
    }

    public function getContrats(): Collection
    {
        return $this->contrats;
    }

    public function getEtat(): string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): void
    {
        $this->etat = $etat;
    }

    public function addBulletin(CampagneBulletin $bulletin): Amapien
    {
        $this->bulletins[] = $bulletin;
        $bulletin->setAmapien($this);

        return $this;
    }

    /**
     * Remove Bulletins.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBulletin(CampagneBulletin $comments)
    {
        return $this->bulletins->removeElement($comments);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<CampagneBulletin>
     */
    public function getBulletins()
    {
        return $this->bulletins;
    }
}
