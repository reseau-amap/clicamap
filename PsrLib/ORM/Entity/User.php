<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use DH\Auditor\User\UserInterface as AuditorUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Embeddable\Address;
use PsrLib\ORM\Entity\Embeddable\Name;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Auditable]
#[ORM\Table(name: 'ak_user')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\UserRepository::class)]
#[ORM\UniqueConstraint(columns: ['username'])]
#[UniqueEntity(fields: ['username'])]
class User implements UserInterface, LocalisableEntity, \Stringable, AuditorUserInterface
{
    use UserPermissionTrait;

    #[ORM\Column(type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $username = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $password = null;

    #[ORM\Embedded(class: \PsrLib\ORM\Entity\Embeddable\Name::class)]
    #[Assert\Valid]
    private Name $name;

    #[ORM\Embedded(class: \PsrLib\ORM\Entity\Embeddable\Address::class)]
    private \PsrLib\ORM\Entity\Embeddable\Address $address;

    /**
     * @var Collection<int, \PsrLib\ORM\Entity\UserEmail>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: \PsrLib\ORM\Entity\UserEmail::class, cascade: ['persist', 'remove'], fetch: 'EAGER', orphanRemoval: true)]
    #[Assert\Valid]
    private Collection $emails;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $numTel1 = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $numTel2 = null;

    #[ORM\Column(name: 'newsletter', type: 'boolean')]
    private bool $newsletter = true;

    // Secondary key used for external sync via API
    #[ORM\Column(name: 'uuid', type: 'string', length: 255, nullable: true)]
    private ?string $uuid;

    #[ORM\Column(type: 'boolean')]
    private bool $superAdmin = false;

    /**
     * @var Collection<Region>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Region::class, inversedBy: 'admins')]
    #[ORM\InverseJoinColumn(name: 'region_id', referencedColumnName: 'reg_id')]
    private \Doctrine\Common\Collections\Collection $adminRegions;

    /**
     * @var Collection<Amap>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'admins')]
    #[ORM\InverseJoinColumn(name: 'amap_id', referencedColumnName: 'amap_id')]
    private \Doctrine\Common\Collections\Collection $adminAmaps;

    /**
     * @var Collection<Departement>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Departement::class, inversedBy: 'admins')]
    #[ORM\JoinTable(options: ['charset' => 'utf8'])]
    #[ORM\InverseJoinColumn(name: 'departement_id', referencedColumnName: 'dep_id')]
    private \Doctrine\Common\Collections\Collection $adminDepartments;

    /**
     * @var Collection<int, \PsrLib\ORM\Entity\Amapien>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: \PsrLib\ORM\Entity\Amapien::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private Collection $amaps;

    /**
     * @var Collection<FermeRegroupement>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\FermeRegroupement::class, mappedBy: 'admins')]
    private \Doctrine\Common\Collections\Collection $adminFermeRegroupements;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ville::class)]
    #[ORM\JoinColumn(referencedColumnName: 'v_id')]
    private ?\PsrLib\ORM\Entity\Ville $ville = null;

    /**
     * @var ArrayCollection<int, AdhesionAmapien>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AdhesionAmapien::class, mappedBy: 'amapien', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $adhesionsAdmin;

    #[ORM\OneToOne(targetEntity: Token::class, cascade: ['all'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Token $passwordResetToken = null;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: Paysan::class, cascade: ['all'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Paysan $paysan = null;

    /**
     * Additional information to generate network adhesion by this user.
     * Used for region and dept admin.
     */
    #[ORM\Embedded(class: \PsrLib\ORM\Entity\AdhesionInfo::class)]
    private \PsrLib\ORM\Entity\AdhesionInfo $adhesionInfo;

    public function __construct()
    {
        $this->name = new Name();
        $this->uuid = Uuid::uuid4()->toString();
        $this->address = new Address();
        $this->emails = new ArrayCollection();
        $this->adminAmaps = new ArrayCollection();
        $this->adminRegions = new ArrayCollection();
        $this->adminDepartments = new ArrayCollection();
        $this->amaps = new ArrayCollection();
        $this->adminFermeRegroupements = new ArrayCollection();
        $this->adhesionsAdmin = new ArrayCollection();
        $this->adhesionInfo = new AdhesionInfo();
    }

    public function getIdentifier(): ?string
    {
        $id = $this->getId();

        return null === $id ? null : (string) $id;
    }

    // Avoid username as email to avoir error in auth eg : userA with email userA@domain and userB with username userA@domain
    #[Assert\Callback()]
    public function validateUsername(ExecutionContextInterface $context): void
    {
        $username = $this->getUsername();
        if (null === $username || '' === $username) {
            return;
        }

        $violations = $context->getValidator()->validate($username, [
            new Assert\Email(['mode' => 'strict']),
        ]);
        if (0 === $violations->count()) {
            $context->buildViolation('Le nom d\'utilisateur ne peut pas être un email')
                ->atPath('username')
                ->addViolation()
            ;
        }
    }

    // As we can add multiple emails in batch, check not duplication in provided data
    // because this check is not done in UniqueEntity constraint
    #[Assert\Callback()]
    public function validateEmailsNoDuplication(ExecutionContextInterface $context): void
    {
        $existingEmails = [];
        foreach ($this->getEmails() as $key => $email) {
            $emailString = $email->getEmail();
            if (in_array($emailString, $existingEmails, true)) {
                $context->buildViolation('Impossible d\'indiquer plusieurs fois la même adresse mail.')
                    ->atPath(sprintf('emails[%d].email', $key))
                    ->addViolation()
                ;
            }
            $existingEmails[] = $emailString;
        }
    }

    public function getAdresseComplete(): string
    {
        $ville = $this->getVille();
        if (null === $ville) {
            return '';
        }

        return $this->getAddress()->getAdress()
            .' '
            .$this->getAddress()->getAdressCompl()
            .' '
            .$ville->getCpString()
            .' '
            .$ville->getNom()
        ;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return Ferme[]
     */
    public function getFermesAsRegroupement(): array
    {
        $res = [];
        foreach ($this->getAdminFermeRegroupements() as $fermeRegroupement) {
            $res[] = $fermeRegroupement->getFermes()->toArray();
        }

        return array_merge_recursive($res);
    }

    /**
     * @return Ferme[]
     */
    public function getFermesAsRefproduit(): array
    {
        $res = [];
        foreach ($this->getAmapienAmaps() as $amapienAmap) {
            if ($amapienAmap->isRefProduit()) {
                $res[] = $amapienAmap->getRefProdFermes()->toArray();
            }
        }

        return array_merge_recursive(...$res);
    }

    /**
     * @return Ferme[]
     */
    public function getFermesAsPaysan(): array
    {
        $paysan = $this->getPaysan();
        if (null === $paysan) {
            return [];
        }

        return $paysan->getFermes()->toArray();
    }

    public function getFermesAsRegroupementOrPaysan(): array
    {
        return array_merge($this->getFermesAsRegroupement(), $this->getFermesAsPaysan());
    }

    public function getUserAmapienAmapForAmap(Amap $amap): ?Amapien
    {
        foreach ($this->getAmapienAmaps() as $amapienAmap) {
            if ($amapienAmap->getAmap() === $amap) {
                return $amapienAmap;
            }
        }

        return null;
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()?->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()?->getDepartement();
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    public function eraseCredentials(): void
    {
        throw new \LogicException('This method is not implemented.');
    }

    // Role not used for authorization check
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getSalt(): ?string
    {
        throw new \LogicException('This method is not implemented.');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return Collection<int, UserEmail>
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(UserEmail $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
            $email->setUser($this);
        }

        return $this;
    }

    public function removeEmail(UserEmail $email): self
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getUser() === $this) {
                $email->setUser(null);
            }
        }

        return $this;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function setName(Name $name): void
    {
        $this->name = $name;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }

    public function getNumTel1(): ?string
    {
        return $this->numTel1;
    }

    public function setNumTel1(?string $numTel1): void
    {
        $this->numTel1 = $numTel1;
    }

    public function getNumTel2(): ?string
    {
        return $this->numTel2;
    }

    public function setNumTel2(?string $numTel2): void
    {
        $this->numTel2 = $numTel2;
    }

    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): void
    {
        $this->newsletter = $newsletter;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function isSuperAdmin(): bool
    {
        return $this->superAdmin;
    }

    public function setSuperAdmin(bool $superAdmin): void
    {
        $this->superAdmin = $superAdmin;
    }

    public function addAdminRegion(Region $adminRegion): self
    {
        $this->adminRegions[] = $adminRegion;
        $adminRegion->addAdmin($this);

        return $this;
    }

    public function removeAdminRegion(Region $adminRegion): bool
    {
        return $this->adminRegions->removeElement($adminRegion);
    }

    public function getAdminRegions(): Collection
    {
        return $this->adminRegions;
    }

    public function addAdminDepartment(Departement $adminDepartment): self
    {
        $this->adminDepartments[] = $adminDepartment;
        $adminDepartment->addAdmin($this);

        return $this;
    }

    public function removeAdminDepartment(Departement $adminDepartment): bool
    {
        return $this->adminDepartments->removeElement($adminDepartment);
    }

    public function getAdminDepartments(): Collection
    {
        return $this->adminDepartments;
    }

    public function addAmapienAmap(Amapien $amapienAmap): self
    {
        $this->amaps[] = $amapienAmap;
        $amapienAmap->setUser($this);

        return $this;
    }

    public function removeAmapienAmap(Amapien $amapienAmap): bool
    {
        return $this->amaps->removeElement($amapienAmap);
    }

    /**
     * @return Collection<int, Amapien>
     */
    public function getAmapienAmaps(): Collection
    {
        return $this->amaps;
    }

    public function addAdminFermeRegroupement(FermeRegroupement $adminFermeRegroupement): self
    {
        $this->adminFermeRegroupements[] = $adminFermeRegroupement;
        $adminFermeRegroupement->addAdmin($this);

        return $this;
    }

    public function removeAdminFermeRegroupement(FermeRegroupement $adminFermeRegroupement): bool
    {
        return $this->adminFermeRegroupements->removeElement($adminFermeRegroupement);
    }

    /**
     * @return Collection<int, FermeRegroupement>
     */
    public function getAdminFermeRegroupements(): Collection
    {
        return $this->adminFermeRegroupements;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): void
    {
        $this->ville = $ville;
    }

    public function getPasswordResetToken(): ?Token
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?Token $passwordResetToken): User
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    public function getPaysan(): ?Paysan
    {
        return $this->paysan;
    }

    public function setPaysan(?Paysan $paysan): void
    {
        $this->paysan = $paysan;
    }

    /**
     * @return ArrayCollection
     */
    public function getAdhesionsAdmin(): ArrayCollection|Collection
    {
        return $this->adhesionsAdmin;
    }

    public function getAdhesionInfo(): AdhesionInfo
    {
        return $this->adhesionInfo;
    }

    public function setAdhesionInfo(AdhesionInfo $adhesionInfo): self
    {
        $this->adhesionInfo = $adhesionInfo;

        return $this;
    }

    public function addAdminAmap(Amap $adminAmap): self
    {
        $this->adminAmaps[] = $adminAmap;
        $adminAmap->addAdmin($this);

        return $this;
    }

    public function removeAdminAmap(Amap $adminAmap): bool
    {
        return $this->adminAmaps->removeElement($adminAmap);
    }

    /**
     * @return Collection<int, Amap>
     */
    public function getAdminAmaps(): Collection
    {
        return $this->adminAmaps;
    }
}
