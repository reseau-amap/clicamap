<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Auditable]
#[ORM\Table(name: 'ak_modele_contrat_produit_exclure')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\ModeleContratProduitExclureRepository::class)]
class ModeleContratProduitExclure
{
    /**
     * @var ?int
     */
    #[ORM\Column(name: 'mc_pro_ex_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\ModeleContratDate::class, inversedBy: 'exclusions')]
    #[ORM\JoinColumn(name: 'mc_pro_ex_fk_modele_contrat_date_id', referencedColumnName: 'mc_d_id')]
    #[Groups(['wizard'])]
    private ?\PsrLib\ORM\Entity\ModeleContratDate $modeleContratDate = null;

    /**
     * @var ModeleContratProduit|null
     */
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\ModeleContratProduit::class, inversedBy: 'exclusions')]
    #[ORM\JoinColumn(name: 'mc_pro_ex_fk_modele_contrat_produit_id', referencedColumnName: 'mc_pro_id')]
    #[Groups(['wizard'])]
    private $modeleContratProduit;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModeleContratDate(): ?ModeleContratDate
    {
        return $this->modeleContratDate;
    }

    public function setModeleContratDate(?ModeleContratDate $modeleContratDate): ModeleContratProduitExclure
    {
        $this->modeleContratDate = $modeleContratDate;

        return $this;
    }

    public function getModeleContratProduit(): ?ModeleContratProduit
    {
        return $this->modeleContratProduit;
    }

    public function setModeleContratProduit(mixed $modeleContratProduit): ModeleContratProduitExclure
    {
        $this->modeleContratProduit = $modeleContratProduit;

        return $this;
    }
}
