<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\Heure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_amap_livraison_horaire')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AmapLivraisonHoraireRepository::class)]
class AmapLivraisonHoraire
{
    final public const SAISON_ETE = 'ete';
    final public const SAISON_HIVER = 'hiver';
    final public const SAISONS = [
        self::SAISON_ETE, self::SAISON_HIVER,
    ];

    final public const JOUR_A_DEFINIR = 'a_definir';
    final public const JOUR_LUNDI = 'lundi';
    final public const JOUR_MARDI = 'mardi';
    final public const JOUR_MERCREDI = 'mercredi';
    final public const JOUR_JEUDI = 'jeudi';
    final public const JOUR_VENDREDI = 'vendredi';
    final public const JOUR_SAMEDI = 'samedi';
    final public const JOUR_DIMANCHE = 'dimanche';

    final public const JOURS = [
        self::JOUR_A_DEFINIR,
        self::JOUR_LUNDI,
        self::JOUR_MARDI,
        self::JOUR_MERCREDI,
        self::JOUR_JEUDI,
        self::JOUR_VENDREDI,
        self::JOUR_SAMEDI,
        self::JOUR_DIMANCHE,
    ];

    /**
     * @var ?int
     */
    #[ORM\Column(name: 'amap_liv_hor_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\AmapLivraisonLieu::class, inversedBy: 'livraisonHoraires')]
    #[ORM\JoinColumn(name: 'amap_liv_hor_fk_amap_liv_lieu_id', referencedColumnName: 'amap_liv_lieu_id', nullable: false)]
    private \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu;

    #[ORM\Column(name: 'amap_liv_hor_saison', type: 'string', length: 5, nullable: true)]
    #[Assert\NotBlank]
    #[Groups(['wizardAmap'])]
    private ?string $saison = self::SAISON_ETE;

    #[ORM\Column(name: 'amap_liv_hor_jour', type: 'string', length: 50, nullable: true)]
    #[Assert\NotBlank]
    #[Groups(['wizardAmap'])]
    private ?string $jour = self::JOUR_LUNDI;

    #[Heure]
    #[ORM\Column(name: 'amap_liv_hor_heure_debut', type: 'string', length: 5, nullable: true)]
    #[Groups(['wizardAmap'])]
    private ?string $heureDebut = null;

    #[Heure]
    #[ORM\Column(name: 'amap_liv_hor_heure_fin', type: 'string', length: 5, nullable: true)]
    #[Groups(['wizardAmap'])]
    private ?string $heureFin = null;

    /**
     * AmapLivraisonHoraire constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSaison(): ?string
    {
        return $this->saison;
    }

    public function setSaison(?string $saison): AmapLivraisonHoraire
    {
        $this->saison = $saison;

        return $this;
    }

    public function getJour(): ?string
    {
        return $this->jour;
    }

    public function setJour(?string $jour): AmapLivraisonHoraire
    {
        $this->jour = $jour;

        return $this;
    }

    public function getHeureDebut(): ?string
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(?string $heureDebut): AmapLivraisonHoraire
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin(): ?string
    {
        return $this->heureFin;
    }

    public function setHeureFin(?string $heureFin): AmapLivraisonHoraire
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Set livraisonLieu.
     */
    public function setLivraisonLieu(AmapLivraisonLieu $livraisonLieu): AmapLivraisonHoraire
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }

    /**
     * Get livraisonLieu.
     */
    public function getLivraisonLieu(): AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }
}
