<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use PsrLib\Enum\CampagneEtat;
use PsrLib\ORM\Entity\Embeddable\CampagneAutheurInfo;
use PsrLib\ORM\Entity\Embeddable\Period;
use PsrLib\ORM\Entity\Files\CampagneLogo;
use PsrLib\Validator\ValidEmbeddable;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_campagne')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\CampagneRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Campagne
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['wizardCampagne'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255, groups: ['form1'])]
    #[Assert\NotBlank(groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private ?string $nom = null;

    #[ValidEmbeddable(groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    #[ORM\Embedded(class: \PsrLib\ORM\Entity\Embeddable\Period::class)]
    private \PsrLib\ORM\Entity\Embeddable\Period $period;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThan(value: 0, groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private int $anneeAdhesionAmapien;

    /**
     * @var \Doctrine\Common\Collections\Collection<CampagneMontant>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\CampagneMontant::class, mappedBy: 'campagne', cascade: ['ALL'], orphanRemoval: true)]
    #[ORM\OrderBy(['ordre' => 'ASC'])]
    #[Assert\Count(min: 1, minMessage: 'Merci d\'indiquer au moins un montant', groups: ['form1'])]
    #[Assert\Valid(groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private \Doctrine\Common\Collections\Collection $montants;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Amap::class, inversedBy: 'campagnes')]
    #[ORM\JoinColumn(referencedColumnName: 'amap_id', nullable: false)]
    #[Groups(['wizardCampagne'])]
    private Amap $amap;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Campagne::class, cascade: ['REMOVE'])]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['wizardCampagne'])]
    private ?Campagne $versionSuivante = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255, groups: ['form2'])]
    #[Assert\NotBlank(groups: ['form2'])]
    #[Groups(['wizardCampagne'])]
    private ?string $paiementMethode = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\Length(max: 2000, groups: ['form2'])]
    #[Groups(['wizardCampagne'])]
    private ?string $paiementDescription = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['wizardCampagne'])]
    private ?string $champLibre = null;

    /**
     * @var \Doctrine\Common\Collections\Collection<CampagneBulletin>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\CampagneBulletin::class, mappedBy: 'campagne', cascade: ['ALL'], orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $bulletins;

    #[Groups(['wizardCampagne'])]
    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Files\CampagneLogo::class, inversedBy: 'campagnes', cascade: ['persist'])]
    private ?\PsrLib\ORM\Entity\Files\CampagneLogo $autheurLogo = null;

    #[ORM\Embedded(class: CampagneAutheurInfo::class)]
    #[Groups(['wizardCampagne'])]
    private CampagneAutheurInfo $autheurInfo;

    public function __construct()
    {
        $this->period = new Period();
        $this->montants = new ArrayCollection();
        $this->bulletins = new ArrayCollection();
        $this->autheurInfo = new CampagneAutheurInfo();
    }

    public function __clone()
    {
        if (!$this->id) {
            // do nothing, do NOT throw an exception!
            // @ref https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/cookbook/implementing-wakeup-or-clone.html#safely-implementing-clone
            return;
        }

        $oldMontants = $this->getMontants()->toArray();
        $this->montants = new ArrayCollection();
        foreach ($oldMontants as $montant) {
            $this->addMontant(clone $montant);
        }
    }

    public function countMontantLibres(): int
    {
        return $this
            ->getMontantLibres()
            ->count()
        ;
    }

    /**
     * @return \Doctrine\Common\Collections\ReadableCollection<CampagneMontant>
     */
    public function getMontantLibres()
    {
        return $this
            ->getMontants()
            ->filter(fn (CampagneMontant $montant) => $montant->isMontantLibre())
        ;
    }

    public function getEtat(): CampagneEtat
    {
        $carbonPeriod = $this->getPeriod()->getCarbonPeriod();
        if (null === $carbonPeriod) {
            throw new \RuntimeException('Period not initialized');
        }

        $now = Carbon::now();
        if ($carbonPeriod->startsAfter($now)) {
            return CampagneEtat::ETAT_BROUILLON;
        }
        if ($carbonPeriod->endsBefore($now)) {
            return CampagneEtat::ETAT_TERMINE;
        }

        return CampagneEtat::ETAT_ENCOURS;
    }

    public function montantTotal(): Money
    {
        $total = Money::EUR(0);
        foreach ($this->montants as $montant) {
            if (!$montant->isMontantLibre()) {
                $total = $total->add($montant->getMontant());
            }
        }

        return $total;
    }

    public static function createFromAmap(Amap $amap): self
    {
        $campagne = new self();
        $campagne->setAmap($amap);

        return $campagne;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Campagne
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPeriod(): Embeddable\Period
    {
        return $this->period;
    }

    public function setPeriod(Embeddable\Period $period): Campagne
    {
        $this->period = $period;

        return $this;
    }

    public function getAnneeAdhesionAmapien(): int
    {
        return $this->anneeAdhesionAmapien;
    }

    public function setAnneeAdhesionAmapien(int $anneeAdhesionAmapien): Campagne
    {
        $this->anneeAdhesionAmapien = $anneeAdhesionAmapien;

        return $this;
    }

    public function addMontant(CampagneMontant $montant): Campagne
    {
        $this->montants[] = $montant;
        $montant->setCampagne($this);

        return $this;
    }

    /**
     * Remove Montants.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeMontant(CampagneMontant $comments)
    {
        return $this->montants->removeElement($comments);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<CampagneMontant>
     */
    public function getMontants()
    {
        return $this->montants;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function setAmap(Amap $amap): Campagne
    {
        $this->amap = $amap;

        return $this;
    }

    public function getVersionSuivante(): ?Campagne
    {
        return $this->versionSuivante;
    }

    public function setVersionSuivante(?Campagne $versionSuivante): Campagne
    {
        $this->versionSuivante = $versionSuivante;

        return $this;
    }

    public function getPaiementMethode(): ?string
    {
        return $this->paiementMethode;
    }

    public function setPaiementMethode(?string $paiementMethode): Campagne
    {
        $this->paiementMethode = $paiementMethode;

        return $this;
    }

    public function getPaiementDescription(): ?string
    {
        return $this->paiementDescription;
    }

    public function setPaiementDescription(?string $paiementDescription): Campagne
    {
        $this->paiementDescription = $paiementDescription;

        return $this;
    }

    public function getChampLibre(): ?string
    {
        return $this->champLibre;
    }

    public function setChampLibre(?string $champLibre): Campagne
    {
        $this->champLibre = $champLibre;

        return $this;
    }

    public function addBulletin(CampagneBulletin $bulletin): Campagne
    {
        $this->bulletins[] = $bulletin;
        $bulletin->setCampagne($this);

        return $this;
    }

    /**
     * Remove Bulletins.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeBulletin(CampagneBulletin $comments)
    {
        return $this->bulletins->removeElement($comments);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection<CampagneBulletin>
     */
    public function getBulletins()
    {
        return $this->bulletins;
    }

    public function getAutheurLogo(): ?CampagneLogo
    {
        return $this->autheurLogo;
    }

    public function setAutheurLogo(?CampagneLogo $autheurLogo): Campagne
    {
        $this->autheurLogo = $autheurLogo;

        return $this;
    }

    public function getAutheurInfo(): CampagneAutheurInfo
    {
        return $this->autheurInfo;
    }

    public function setAutheurInfo(CampagneAutheurInfo $autheurInfo): Campagne
    {
        $this->autheurInfo = $autheurInfo;

        return $this;
    }
}
