<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Table(name: 'ak_ferme_regroupement')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\FermeRegroupementRepository::class)]
class FermeRegroupement implements \Stringable
{
    /**
     * @var ?int
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $nom = null;

    /**
     * @var ArrayCollection<Ferme>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\Ferme::class, mappedBy: 'regroupement')]
    private \Doctrine\Common\Collections\Collection $fermes;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 14, max: 14, exactMessage: 'Le SIRET doit faire exactement 14 caractères.')]
    #[Assert\Luhn(message: 'Le SIRET est invalide.')]
    private ?string $siret = null;

    /**
     * @var ArrayCollection<User>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\User::class, inversedBy: 'adminFermeRegroupements')]
    private \Doctrine\Common\Collections\Collection $admins;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Assert\NotBlank]
    private ?string $adresseAdmin;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ville::class)]
    #[ORM\JoinColumn(referencedColumnName: 'v_id')]
    #[Assert\NotBlank()]
    private ?\PsrLib\ORM\Entity\Ville $ville = null;

    /**
     * Indique si les contrats associés doivent être associés au système de paiement Champ des possible.
     * Champ non administrable, déterminé directement via la base de donnée.
     */
    #[ORM\Column(type: 'boolean', name: 'cdp')]
    private bool $cdp = false;

    public function __construct()
    {
        $this->fermes = new ArrayCollection();
        $this->admins = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getNom();
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()->getRegion();
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()->getDepartement();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): FermeRegroupement
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Add ferme.
     *
     * @return FermeRegroupement
     */
    public function addFerme(Ferme $ferme)
    {
        $this->fermes[] = $ferme;
        $ferme->setRegroupement($this);

        return $this;
    }

    /**
     * Remove ferme.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeFerme(Ferme $ferme)
    {
        $ferme->setRegroupement(null);

        return $this->fermes->removeElement($ferme);
    }

    /**
     * @return Collection<int, Ferme>
     */
    public function getFermes()
    {
        return $this->fermes;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): FermeRegroupement
    {
        $this->siret = $siret;

        return $this;
    }

    public function addAdmin(User $admin): self
    {
        $this->admins[] = $admin;

        return $this;
    }

    public function removeAdmin(User $admin): bool
    {
        return $this->admins->removeElement($admin);
    }

    /**
     * @return Collection<int, User>
     */
    public function getAdmins(): Collection
    {
        return $this->admins;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): void
    {
        $this->ville = $ville;
    }

    public function getAdresseAdmin(): ?string
    {
        return $this->adresseAdmin;
    }

    public function setAdresseAdmin(?string $adresseAdmin): void
    {
        $this->adresseAdmin = $adresseAdmin;
    }

    public function isCdp(): bool
    {
        return $this->cdp;
    }

    public function setCdp(bool $cdp): void
    {
        $this->cdp = $cdp;
    }
}
