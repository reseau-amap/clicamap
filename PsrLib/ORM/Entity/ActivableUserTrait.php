<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

trait ActivableUserTrait
{
    public function estActif(): bool
    {
        return ActivableUserInterface::ETAT_ACTIF === $this->getEtat();
    }
}
