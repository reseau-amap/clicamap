<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * Compatibility table for codeigniter captcha.
 */
#[ORM\Table(name: 'captcha')]
#[ORM\Entity]
class Captcha
{
    #[ORM\Id]
    #[ORM\Column(name: 'captcha_id', type: 'bigint', length: 13, options: ['unsigned' => true])]
    #[GeneratedValue]
    private ?string $id = null;

    #[ORM\Column(name: 'captcha_time', type: 'integer', options: ['unsigned' => true], nullable: true)]
    private ?int $time = null;

    #[ORM\Column(name: 'ip_address', type: 'string', length: 45, nullable: false)]
    private string $ipAddress; // @phpstan-ignore-line

    #[ORM\Column(name: 'word', type: 'string', length: 20, nullable: false)]
    private string $word; // @phpstan-ignore-line

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getWord(): string
    {
        return $this->word;
    }
}
