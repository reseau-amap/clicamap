<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification;
use PsrLib\Validator\Numeric;
use PsrLib\Validator\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['ferme', 'typeProduction'], errorPath: 'typeProduction', message: 'Le produit existe déjà dans la base de donnée.')]
#[ORM\Table(name: 'ak_ferme_type_production_propose')]
#[ORM\Entity]
class FermeTypeProductionPropose
{
    #[ORM\Column(name: 'ferme_tpp_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'typeProductionProposes')]
    #[ORM\JoinColumn(name: 'ferme_tpp_fk_ferme_id', referencedColumnName: 'f_id', nullable: false)]
    #[Assert\NotNull]
    private \PsrLib\ORM\Entity\Ferme $ferme;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\TypeProduction::class)]
    #[ORM\JoinColumn(referencedColumnName: 'tp_id')]
    #[Assert\NotNull]
    private ?\PsrLib\ORM\Entity\TypeProduction $typeProduction = null;

    #[Numeric]
    #[ORM\Column(name: 'ferme_tpp_nb_amap_livrees', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $nbAmapLivrees = null;

    #[Numeric]
    #[ORM\Column(name: 'ferme_tpp_nb_contrats_total_amap', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $nbContratsTotalAmap = null;

    #[Numeric]
    #[ORM\Column(name: 'ferme_tpp_ca_annuel_amap', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $caAnnuelAmap = null;

    #[Numeric]
    #[ORM\Column(name: 'ferme_tpp_p_ca_total', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    #[Assert\LessThanOrEqual(100)]
    private ?string $caTotal = null;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification::class, cascade: ['remove', 'persist'])]
    private ?\PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification $attestationCertification = null;

    public function __construct(Ferme $ferme)
    {
        $this->ferme = $ferme;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }

    public function setFerme(Ferme $ferme): FermeTypeProductionPropose
    {
        $this->ferme = $ferme;

        return $this;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): FermeTypeProductionPropose
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }

    public function getNbAmapLivrees(): ?string
    {
        return $this->nbAmapLivrees;
    }

    public function setNbAmapLivrees(?string $nbAmapLivrees): FermeTypeProductionPropose
    {
        $this->nbAmapLivrees = $nbAmapLivrees;

        return $this;
    }

    public function getNbContratsTotalAmap(): ?string
    {
        return $this->nbContratsTotalAmap;
    }

    public function setNbContratsTotalAmap(?string $nbContratsTotalAmap): FermeTypeProductionPropose
    {
        $this->nbContratsTotalAmap = $nbContratsTotalAmap;

        return $this;
    }

    public function getCaAnnuelAmap(): ?string
    {
        return $this->caAnnuelAmap;
    }

    public function setCaAnnuelAmap(?string $caAnnuelAmap): FermeTypeProductionPropose
    {
        $this->caAnnuelAmap = $caAnnuelAmap;

        return $this;
    }

    public function getCaTotal(): ?string
    {
        return $this->caTotal;
    }

    public function setCaTotal(?string $caTotal): FermeTypeProductionPropose
    {
        $this->caTotal = $caTotal;

        return $this;
    }

    public function getAttestationCertification(): ?FermeTypeProductionAttestationCertification
    {
        return $this->attestationCertification;
    }

    public function setAttestationCertification(?FermeTypeProductionAttestationCertification $attestationCertification): FermeTypeProductionPropose
    {
        $this->attestationCertification = $attestationCertification;

        return $this;
    }
}
