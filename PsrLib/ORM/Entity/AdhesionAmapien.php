<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\GeneratedValue;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\AdhesionAmapienRepository::class)]
class AdhesionAmapien extends AdhesionCreatorUser
{
    /**
     * @var ?int
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[GeneratedValue]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\User::class, inversedBy: 'adhesionsAdmin')]
    protected ?User $amapien = null;

    /**
     * @var AdhesionValueAmapien
     */
    #[ORM\OneToOne(targetEntity: AdhesionValueAmapien::class, cascade: ['ALL'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Valid]
    protected $value;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Expression('value.isAdminOf(this.getAmapien())', groups: ['import'], message: "Erreur d'accès à l'amapien")]
    protected User $creator;

    /**
     * AdhesionFerme constructor.
     */
    public function __construct(User $amapien, AdhesionValueAmapien $value, User $creator)
    {
        $this->amapien = $amapien;
        $this->value = $value;
        $this->creator = $creator;
    }

    public function getAmapien(): ?User
    {
        return $this->amapien;
    }

    public function setAmapien(?User $amapien): void
    {
        $this->amapien = $amapien;
    }

    public function getValue(): AdhesionValueAmapien
    {
        return $this->value;
    }

    /**
     * @param AdhesionValueAmapien $value
     *
     * @return AdhesionAmapien
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
