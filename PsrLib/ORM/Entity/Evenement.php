<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use PsrLib\ORM\Entity\Files\EvenementImg;
use PsrLib\ORM\Entity\Files\EvenementPj;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[InheritanceType('JOINED')]
#[ORM\Table(name: 'ak_evenement')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\EvenementRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Evenement
{
    use TimestampableTrait;

    /**
     * @var ?int
     */
    #[ORM\Column(name: 'ev_id', type: 'integer')]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[Assert\Length(max: 150)]
    #[ORM\Column(type: 'string', length: 150)]
    private string $titre;

    #[Assert\NotBlank]
    #[Assert\Length(max: 10000, maxMessage: 'La description est trop longue')]
    #[ORM\Column(type: 'text')]
    private string $description;

    #[Assert\NotBlank]
    #[Assert\Length(max: 250, maxMessage: 'La description est trop longue')]
    #[ORM\Column(type: 'text')]
    private string $descriptionCourte;

    #[Assert\Length(max: 150)]
    #[ORM\Column(type: 'string', length: 150, nullable: true)]
    private ?string $lieu;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?\DateTime $dateDeb = null;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?\DateTime $dateFin = null;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\EvenementImg::class, cascade: ['persist'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\EvenementImg $img = null;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\EvenementPj::class, cascade: ['persist'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\EvenementPj $pj = null;

    #[ORM\Column(type: 'boolean')]
    private bool $accesAbonnesDefautUniquement = false;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private \PsrLib\ORM\Entity\User $auteur;

    /**
     * Constructor.
     */
    public function __construct(User $auteur)
    {
        $this->auteur = $auteur;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): Evenement
    {
        $this->titre = (string) $titre;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Evenement
    {
        $this->description = (string) $description;

        return $this;
    }

    public function getDescriptionCourte(): string
    {
        return $this->descriptionCourte;
    }

    public function setDescriptionCourte(?string $descriptionCourte): Evenement
    {
        $this->descriptionCourte = (string) $descriptionCourte;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(?string $lieu): Evenement
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getDateDeb(): ?\DateTime
    {
        return $this->dateDeb;
    }

    public function setDateDeb(?\DateTime $dateDeb): Evenement
    {
        $this->dateDeb = $dateDeb;

        return $this;
    }

    public function getDateFin(): ?\DateTime
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTime $dateFin): Evenement
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getImg(): ?EvenementImg
    {
        return $this->img;
    }

    public function setImg(?EvenementImg $img): Evenement
    {
        $this->img = $img;

        return $this;
    }

    public function getPj(): ?EvenementPj
    {
        return $this->pj;
    }

    public function setPj(?EvenementPj $pj): Evenement
    {
        $this->pj = $pj;

        return $this;
    }

    public function isAccesAbonnesDefautUniquement(): bool
    {
        return $this->accesAbonnesDefautUniquement;
    }

    public function setAccesAbonnesDefautUniquement(bool $accesAbonnesDefautUniquement): Evenement
    {
        $this->accesAbonnesDefautUniquement = $accesAbonnesDefautUniquement;

        return $this;
    }

    public function getAuteur(): User
    {
        return $this->auteur;
    }
}
