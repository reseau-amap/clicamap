<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

// Simple trait to code factorization
use Doctrine\Common\Collections\Collection;

trait UserPermissionTrait
{
    public function hasNoRights(): bool
    {
        return !$this->isAdmin()
            && !$this->isAmapAdmin()
            && !$this->isPaysan()
            && !$this->isAmapien()
            && !$this->isFermeRegroupementAdmin()
        ;
    }

    /**
     * @return Amap[]
     */
    public function getAmapsAsAmapien(): array
    {
        $amapAdmins = [];
        foreach ($this->getAmapienAmaps() as $amapienAmap) {
            $amapAdmins[] = $amapienAmap->getAmap();
        }

        return $amapAdmins;
    }

    /**
     * @return Amap[]
     */
    public function getAmapsAsAmapienOrAdmin(): array
    {
        return array_merge($this->getAmapsAsAmapien(), $this->getAdminAmaps()->toArray());
    }

    /**
     * @return Amap[]
     */
    public function getAmapsAsAmapienActif(): array
    {
        $amapAdmins = [];
        foreach ($this->getAmapienAmaps() as $amapienAmap) {
            if (ActivableUserInterface::ETAT_ACTIF === $amapienAmap->getEtat()) {
                $amapAdmins[] = $amapienAmap->getAmap();
            }
        }

        return $amapAdmins;
    }

    /**
     * @return Amap[]
     */
    public function getAmapsAsRefProduit(): array
    {
        $amapAdmins = [];
        foreach ($this->getAmapienAmaps() as $amapienAmap) {
            if ($amapienAmap->isRefProduit()) {
                $amapAdmins[] = $amapienAmap->getAmap();
            }
        }

        return $amapAdmins;
    }

    public function isAdminRegion(): bool
    {
        return $this->adminRegions->count() > 0;
    }

    public function isAdminDepartment(): bool
    {
        return $this->adminDepartments->count() > 0;
    }

    public function isAdminReseau(): bool
    {
        return $this->isAdminDepartment() // Admin de région
            || $this->isAdminRegion() // Admin de département
        ;
    }

    public function isAdmin(): bool
    {
        return $this->isSuperAdmin() // Super admin
            || $this->isAdminReseau()
        ;
    }

    public function isPaysan(): bool
    {
        return null !== $this->getPaysan();
    }

    public function isPaysanOf(Ferme $ferme): bool
    {
        return null !== $this->getPaysan() && $this->getPaysan()->getFermes()->contains($ferme);
    }

    public function isPaysanActifOf(Ferme $ferme): bool
    {
        return $this->isPaysanOf($ferme) && $this->getPaysan()->estActif();
    }

    public function isFermeRegroupementAdmin(): bool
    {
        return $this->adminFermeRegroupements->count() > 0;
    }

    public function isFermeRegroupementAdminOf(Ferme $ferme): bool
    {
        foreach ($this->adminFermeRegroupements as $fermeRegroupement) {
            if ($fermeRegroupement->getFermes()->contains($ferme)) {
                return true;
            }
        }

        return false;
    }

    public function isAmapien(): bool
    {
        return $this->amaps->count() > 0;
    }

    public function isAmapienActif(): bool
    {
        foreach ($this->amaps as $userAmap) {
            if (ActivableUserInterface::ETAT_ACTIF === $userAmap->getEtat()) {
                return true;
            }
        }

        return false;
    }

    public function isAmapAdmin(): bool
    {
        return !$this->getAdminAmaps()->isEmpty();
    }

    public function isAmapienOfAmap(Amap $amap): bool
    {
        foreach ($this->amaps as $userAmap) {
            if ($userAmap->getAmap() === $amap) {
                return true;
            }
        }

        return false;
    }

    public function isAmapAdminOfAmap(Amap $amap): bool
    {
        return $this->getAdminAmaps()->contains($amap);
    }

    public function isAmapAdminOfAmapien(User $amapien): bool
    {
        foreach ($amapien->getAmapienAmaps() as $amapienAmap) {
            if ($this->isAmapAdminOfAmap($amapienAmap->getAmap())) {
                return true;
            }
        }

        return false;
    }

    public function isAdminOf(?LocalisableEntity $entity): bool
    {
        if (null === $entity) {
            return false;
        }

        if ($this->isSuperAdmin()) {
            return true;
        }

        if ($this->isAdminRegion() && $this->adminRegions->contains($entity->getRegion())) {
            return true;
        }

        if ($this->isAdminDepartment() && $this->adminDepartments->contains($entity->getDepartement())) {
            return true;
        }

        return false;
    }

    public function isRefProduit(): bool
    {
        return !empty($this->getAmapsAsRefProduit());
    }

    public function isRefProduitOfAmap(Amap $amap): bool
    {
        foreach ($this->amaps as $userAmap) {
            if ($userAmap->isRefProduit() && $userAmap->getAmap() === $amap) {
                return true;
            }
        }

        return false;
    }

    public function isRefProduitOfFerme(Ferme $ferme): bool
    {
        foreach ($this->amaps as $userAmap) {
            if ($userAmap->isRefProduit() && $userAmap->getRefProdFermes()->contains($ferme)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Collection<int, Amapien>
     */
    public function getAmapienAmapsActif()
    {
        return $this
            ->amaps
            ->filter(fn (Amapien $amapienAmap) => ActivableUserInterface::ETAT_ACTIF === $amapienAmap->getEtat())
        ;
    }
}
