<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[ORM\Entity]
class AdhesionValueAmapAmapien extends AdhesionValue
{
    #[SerializedName('N° de reçu')]
    protected ?string $voucherNumber = null;

    #[SerializedName('Montant Total (€)')]
    protected ?Money  $amount = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[SerializedName('Email de l’AMAPien')]
    #[Assert\NotBlank(message: 'Le mail ne peut pas être vide.')]
    private ?string $emailAmapien = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[SerializedName('Nb de foyers')]
    private ?int $householdCount = null;

    public function getEmailAmapien(): ?string
    {
        return $this->emailAmapien;
    }

    public function setEmailAmapien(?string $emailAmapien): AdhesionValueAmapAmapien
    {
        $this->emailAmapien = $emailAmapien;

        return $this;
    }

    public function getHouseholdCount(): ?int
    {
        return $this->householdCount;
    }

    public function setHouseholdCount(?int $householdCount): AdhesionValueAmapAmapien
    {
        $this->householdCount = $householdCount;

        return $this;
    }
}
