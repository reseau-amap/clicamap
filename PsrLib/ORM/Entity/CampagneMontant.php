<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use PsrLib\Validator\MoneyGreaterThanOrEqual;
use PsrLib\Validator\UniqueEntity;
use PsrLib\Validator\When;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Auditable]
#[UniqueEntity(fields: ['campagne', 'ordre'])]
#[ORM\Table(name: 'ak_campagne_montant')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\CampagneMontantRepository::class)]
#[ORM\HasLifecycleCallbacks]
class CampagneMontant
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['wizardCampagne', 'wizardCampagneSubscription'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255, groups: ['form1'])]
    #[Assert\NotBlank(groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private ?string $titre = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['wizardCampagne'])]
    private bool $montantLibre = false;

    #[ORM\Column(type: 'money', nullable: true)]
    #[When(
        expression: 'this.isMontantLibre() == false',
        constraints: [
            new Assert\NotNull(message: 'Le montant ne peut pas être vide.'),
            new MoneyGreaterThanOrEqual(value: 0),
        ],
        groups: ['form1']
    )]
    #[Groups(['wizardCampagne'])]
    private ?Money $montant = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\Length(max: 2048, groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private ?string $description = null;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(value: 0, groups: ['form1'])]
    #[Groups(['wizardCampagne'])]
    private int $ordre = 0;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Campagne::class, inversedBy: 'montants')]
    private ?Campagne $campagne;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): CampagneMontant
    {
        $this->titre = $titre;

        return $this;
    }

    public function isMontantLibre(): bool
    {
        return $this->montantLibre;
    }

    public function setMontantLibre(bool $montantLibre): CampagneMontant
    {
        $this->montantLibre = $montantLibre;

        return $this;
    }

    public function getMontant(): ?Money
    {
        return $this->montant;
    }

    public function setMontant(?Money $montant): CampagneMontant
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): CampagneMontant
    {
        $this->description = $description;

        return $this;
    }

    public function getOrdre(): int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): CampagneMontant
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getCampagne(): ?Campagne
    {
        return $this->campagne;
    }

    public function setCampagne(?Campagne $campagne): CampagneMontant
    {
        $this->campagne = $campagne;

        return $this;
    }
}
