<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use Carbon\Carbon;
use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use PsrLib\Validator\Numeric;
use PsrLib\Validator\UniqueEntity;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

#[Auditable]
#[UniqueEntity(fields: ['siret'], message: 'Ce SIRET existe déjà dans la base de donnée.')]
#[ORM\Table(name: 'ak_ferme')]
#[ORM\UniqueConstraint(name: 'f_siret', columns: ['f_siret'])]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\FermeRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Assert\GroupSequenceProvider]
class Ferme implements LocalisableEntity, GroupSequenceProviderInterface, \Stringable, EvenementRelatedEntity
{
    final public const AVAILABLE_CERTIFICATIONS = [
        '' => null,
        'N&P' => 'np',
        'AB' => 'ab',
        'Partiellement AB' => 'ab_part',
        'En conversion' => 'conversion',
    ];

    /**
     * @var ?int
     */
    #[ORM\Column(name: 'f_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['wizard'])]
    private ?int $id = null;

    #[ORM\Column(name: 'f_nom', type: 'string', length: 100, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    private ?string $nom = null;

    #[ORM\Column(name: 'f_siret', type: 'string', length: 14, nullable: true)]
    #[Assert\Length(max: 14, min: 14, exactMessage: 'Le champ "SIRET" doit contenir exactement 14 caractères.')]
    #[Assert\NotBlank(groups: ['SansRegroupement'])]
    private ?string $siret = null;

    #[ORM\Column(name: 'f_description', type: 'text', length: 65535, nullable: true)]
    #[Assert\Length(max: 65535)]
    private ?string $description = null;

    #[ORM\Column(name: 'f_lib_adr', type: 'string', length: 255, nullable: true)]
    private ?string $libAdr = null;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\Ville::class)]
    #[ORM\JoinColumn(referencedColumnName: 'v_id')]
    #[Assert\NotNull]
    private ?\PsrLib\ORM\Entity\Ville $ville = null;

    #[Numeric]
    #[ORM\Column(name: 'f_gps_latitude', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $gpsLatitude = null;

    #[Numeric]
    #[ORM\Column(name: 'f_gps_longitude', type: 'string', length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $gpsLongitude = null;

    #[ORM\Column(name: 'f_tva', type: 'boolean')]
    private bool $tva = true;

    #[ORM\Column(name: 'f_tva_taux', type: 'string', length: 50, nullable: true)]
    #[Assert\Length(max: 50)]
    private ?string $tvaTaux = null;

    #[ORM\Column(name: 'f_tva_date', type: 'date', nullable: true)]
    private ?\DateTime $tvaDate = null;

    #[ORM\Column(name: 'f_url', type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $url = null;

    #[ORM\Column(name: 'f_uuid', type: 'string', length: 255, nullable: true)]
    private ?string $uuid;

    #[ORM\Column(name: 'f_surface', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $surface = null;

    #[ORM\Column(name: 'f_uta', type: 'decimal', precision: 30, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $uta = null;

    #[ORM\Column(name: 'f_certification', type: 'string', length: 255, nullable: true)]
    #[Assert\Choice(choices: Ferme::AVAILABLE_CERTIFICATIONS)]
    private ?string $certification = null;

    /**
     * @var ArrayCollection<Paysan>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Paysan::class, mappedBy: 'fermes')]
    private \Doctrine\Common\Collections\Collection $paysans;

    /**
     * @var ArrayCollection<Amapien>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Amapien::class, inversedBy: 'refProdFermes')]
    #[ORM\JoinColumn(referencedColumnName: 'f_id', onDelete: 'CASCADE')]
    #[ORM\InverseJoinColumn(referencedColumnName: 'a_id', onDelete: 'CASCADE')]
    private \Doctrine\Common\Collections\Collection $amapienRefs;

    /**
     * @var ArrayCollection<FermeProduit>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\FermeProduit::class, mappedBy: 'ferme', cascade: ['remove'])]
    private \Doctrine\Common\Collections\Collection $produits;

    /**
     * @var ArrayCollection<FermeAnneeAdhesion>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\FermeAnneeAdhesion::class, mappedBy: 'ferme', cascade: ['PERSIST'], orphanRemoval: true)]
    private \Doctrine\Common\Collections\Collection $anneeAdhesions;

    /**
     * @var ArrayCollection<FermeTypeProductionPropose>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\FermeTypeProductionPropose::class, mappedBy: 'ferme')]
    private \Doctrine\Common\Collections\Collection $typeProductionProposes;

    /**
     * @var ArrayCollection<ModeleContrat>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\ModeleContrat::class, mappedBy: 'ferme', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $modeleContrats;

    /**
     * @var ArrayCollection<Amapien>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Amapien::class, mappedBy: 'fermeAttentes')]
    private \Doctrine\Common\Collections\Collection $amapienAttentes;

    #[ORM\ManyToOne(targetEntity: \PsrLib\ORM\Entity\FermeRegroupement::class, inversedBy: 'fermes')]
    private ?\PsrLib\ORM\Entity\FermeRegroupement $regroupement = null;

    /**
     * @var ArrayCollection<AdhesionFerme>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\AdhesionFerme::class, cascade: ['REMOVE'], mappedBy: 'ferme')]
    private \Doctrine\Common\Collections\Collection $adhesions;

    #[ORM\Column(name: 'f_annee_deb_commercialisation_amap', type: 'string', length: 4, nullable: true)]
    #[Assert\Length(min: 4, max: 4)]
    #[Assert\Type(type: 'digit')]
    private ?string $anneeDebCommercialisationAmap = null;

    #[ORM\Column(name: 'f_date_installation', type: 'date', nullable: true)]
    private ?\DateTime $dateInstallation = null;

    #[ORM\Column(name: 'f_msa', type: 'string', length: 255, nullable: true)]
    private ?string $msa = null;

    #[ORM\Column(name: 'f_spg', type: 'string', length: 4, nullable: true)]
    #[Assert\Length(min: 4, max: 4)]
    #[Assert\Type(type: 'digit')]
    private ?string $spg = null;

    /**
     * @var Collection<FermeCommentaire>
     */
    #[ORM\OneToMany(targetEntity: FermeCommentaire::class, mappedBy: 'ferme')]
    #[ORM\OrderBy(['payComDate' => 'DESC'])]
    private \Doctrine\Common\Collections\Collection $commentaires;

    #[ORM\Column(type: 'string', length: 100)]
    #[Assert\NotBlank()]
    #[Assert\Length(max: 100)]
    private ?string $ordreCheque;

    /**
     * @var Collection<EvenementAbonnementFerme>
     */
    #[ORM\OneToMany(targetEntity: \PsrLib\ORM\Entity\EvenementAbonnementFerme::class, mappedBy: 'related', cascade: ['REMOVE'])]
    private \Doctrine\Common\Collections\Collection $evementAbonnements;

    public function __construct()
    {
        $this->paysans = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->anneeAdhesions = new ArrayCollection();
        $this->typeProductionProposes = new ArrayCollection();
        $this->modeleContrats = new ArrayCollection();
        $this->amapienAttentes = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
        $this->amapienRefs = new ArrayCollection();
        $this->adhesions = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->evementAbonnements = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getNom();
    }

    public function isAdhesionActive(): bool
    {
        $currentYear = Carbon::now()->year;

        /** @var FermeAnneeAdhesion $adhesion */
        foreach ($this->getAnneeAdhesions() as $adhesion) {
            if ($currentYear - $adhesion->getAnnee() < 3) {
                return true;
            }
        }

        return false;
    }

    public function getGroupSequence()
    {
        $groups = ['Ferme'];
        if ($this->inRegroupement()) {
            $groups[] = 'Regroupement';
        } else {
            $groups[] = 'SansRegroupement';
        }

        return [$groups];
    }

    public function inRegroupement(): bool
    {
        return null !== $this->regroupement;
    }

    /**
     * @return TypeProduction[]
     */
    public function getTypeProductionFromProduits()
    {
        $tpps = [];

        /** @var FermeProduit $produit */
        foreach ($this->getProduits() as $produit) {
            $tpps[] = $produit->getTypeProduction();
        }

        return array_unique($tpps);
    }

    #[ORM\PreRemove]
    public function dissociatePaysanOnRemove(): void
    {
        /** @var Paysan $paysan */
        foreach ($this->getPaysans() as $paysan) {
            $paysan->removeFerme($this);
        }
    }

    public function getDepartement(): ?Departement
    {
        return $this->getVille()->getDepartement();
    }

    public function getRegion(): ?Region
    {
        return $this->getVille()->getDepartement()->getRegion();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): Ferme
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): Ferme
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Add paysan.
     *
     * @return Ferme
     */
    public function addPaysan(Paysan $paysan)
    {
        $this->paysans[] = $paysan;

        return $this;
    }

    /**
     * Remove paysan.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removePaysan(Paysan $paysan)
    {
        return $this->paysans->removeElement($paysan);
    }

    /**
     * Get paysans.
     *
     * @return Collection<Paysan>
     */
    public function getPaysans()
    {
        return $this->paysans;
    }

    /**
     * Add amapienRef.
     *
     * @return Ferme
     */
    public function addAmapienRef(Amapien $amapienRef)
    {
        $this->amapienRefs[] = $amapienRef;

        return $this;
    }

    /**
     * Remove amapienRef.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapienRef(Amapien $amapienRef)
    {
        return $this->amapienRefs->removeElement($amapienRef);
    }

    /**
     * Get amapienRefs.
     *
     * @return Collection<Amapien>
     */
    public function getAmapienRefs()
    {
        return $this->amapienRefs;
    }

    /**
     * Add produit.
     *
     * @return Ferme
     */
    public function addProduit(FermeProduit $produit)
    {
        $this->produits[] = $produit;

        return $this;
    }

    /**
     * Remove produit.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeProduit(FermeProduit $produit)
    {
        return $this->produits->removeElement($produit);
    }

    /**
     * Get produits.
     *
     * @return Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Add anneeAdhesion.
     *
     * @return Ferme
     */
    public function addAnneeAdhesion(FermeAnneeAdhesion $anneeAdhesion)
    {
        $this->anneeAdhesions[] = $anneeAdhesion;

        return $this;
    }

    /**
     * Remove anneeAdhesion.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAnneeAdhesion(FermeAnneeAdhesion $anneeAdhesion)
    {
        return $this->anneeAdhesions->removeElement($anneeAdhesion);
    }

    /**
     * Get anneeAdhesions.
     *
     * @return Collection
     */
    public function getAnneeAdhesions()
    {
        return $this->anneeAdhesions;
    }

    /**
     * @return FermeAnneeAdhesion[]
     */
    public function getAnneeAdhesionsOrdered()
    {
        $adhesions = $this->anneeAdhesions->toArray();
        usort($adhesions, fn (FermeAnneeAdhesion $a, FermeAnneeAdhesion $b) => $b->getAnnee() - $a->getAnnee());

        return $adhesions;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): Ferme
    {
        $this->ville = $ville;

        return $this;
    }

    public function getGpsLatitude(): ?string
    {
        return $this->gpsLatitude;
    }

    public function setGpsLatitude(?string $gpsLatitude): Ferme
    {
        $this->gpsLatitude = $gpsLatitude;

        return $this;
    }

    public function getGpsLongitude(): ?string
    {
        return $this->gpsLongitude;
    }

    public function setGpsLongitude(?string $gpsLongitude): Ferme
    {
        $this->gpsLongitude = $gpsLongitude;

        return $this;
    }

    public function getLibAdr(): ?string
    {
        return $this->libAdr;
    }

    public function setLibAdr(?string $libAdr): Ferme
    {
        $this->libAdr = $libAdr;

        return $this;
    }

    public function getSurface(): ?string
    {
        return $this->surface;
    }

    public function setSurface(?string $surface): Ferme
    {
        $this->surface = $surface;

        return $this;
    }

    public function getUta(): ?string
    {
        return $this->uta;
    }

    public function setUta(?string $uta): Ferme
    {
        $this->uta = $uta;

        return $this;
    }

    public function getCertification(): ?string
    {
        return $this->certification;
    }

    public function setCertification(?string $certification): Ferme
    {
        $this->certification = $certification;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Ferme
    {
        $this->description = $description;

        return $this;
    }

    public function isTva(): bool
    {
        return $this->tva;
    }

    public function setTva(bool $tva): Ferme
    {
        $this->tva = $tva;

        return $this;
    }

    public function getTvaTaux(): ?string
    {
        return $this->tvaTaux;
    }

    public function setTvaTaux(?string $tvaTaux): Ferme
    {
        $this->tvaTaux = $tvaTaux;

        return $this;
    }

    public function getTvaDate(): ?\DateTime
    {
        return $this->tvaDate;
    }

    public function setTvaDate(?\DateTime $tvaDate): Ferme
    {
        $this->tvaDate = $tvaDate;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Ferme
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Add typeProductionPropose.
     *
     * @return Ferme
     */
    public function addTypeProductionPropose(FermeTypeProductionPropose $typeProductionPropose)
    {
        $this->typeProductionProposes[] = $typeProductionPropose;

        return $this;
    }

    /**
     * Remove typeProductionPropose.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeTypeProductionPropose(FermeTypeProductionPropose $typeProductionPropose)
    {
        return $this->typeProductionProposes->removeElement($typeProductionPropose);
    }

    /**
     * Get typeProductionProposes.
     *
     * @return Collection
     */
    public function getTypeProductionProposes()
    {
        return $this->typeProductionProposes;
    }

    /**
     * Add modeleContrat.
     *
     * @return Ferme
     */
    public function addModeleContrat(ModeleContrat $modeleContrat)
    {
        $this->modeleContrats[] = $modeleContrat;

        return $this;
    }

    /**
     * Remove modeleContrat.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeModeleContrat(ModeleContrat $modeleContrat)
    {
        return $this->modeleContrats->removeElement($modeleContrat);
    }

    /**
     * Get modeleContrats.
     *
     * @return Collection
     */
    public function getModeleContrats()
    {
        return $this->modeleContrats;
    }

    /**
     * Add fermeAttente.
     *
     * @return Ferme
     */
    public function addAmapienAttente(Amapien $amapienAttente)
    {
        $this->amapienAttentes[] = $amapienAttente;

        return $this;
    }

    /**
     * Remove amapienAttente.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAmapienAttente(Amapien $amapienAttente)
    {
        return $this->amapienAttentes->removeElement($amapienAttente);
    }

    /**
     * Get amapienAttentes.
     *
     * @return Collection
     */
    public function getAmapienAttentes()
    {
        return $this->amapienAttentes;
    }

    /**
     * @return string|null
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function getRegroupement(): ?FermeRegroupement
    {
        return $this->regroupement;
    }

    public function setRegroupement(?FermeRegroupement $regroupement): Ferme
    {
        $this->regroupement = $regroupement;

        return $this;
    }

    public function getAnneeDebCommercialisationAmap(): ?string
    {
        return $this->anneeDebCommercialisationAmap;
    }

    public function setAnneeDebCommercialisationAmap(?string $anneeDebCommercialisationAmap): void
    {
        $this->anneeDebCommercialisationAmap = $anneeDebCommercialisationAmap;
    }

    public function getDateInstallation(): ?\DateTime
    {
        return $this->dateInstallation;
    }

    public function setDateInstallation(?\DateTime $dateInstallation): void
    {
        $this->dateInstallation = $dateInstallation;
    }

    /**
     * @return ArrayCollection<Adhesion>
     */
    public function getAdhesions(): Collection
    {
        return $this->adhesions;
    }

    public function addCommentaire(FermeCommentaire $commentaire): self
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    public function removeCommentaire(FermeCommentaire $commentaire): bool
    {
        return $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires.
     *
     * @return Collection<FermeCommentaire>
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    public function getMsa(): ?string
    {
        return $this->msa;
    }

    public function setMsa(?string $msa): void
    {
        $this->msa = $msa;
    }

    public function getSpg(): ?string
    {
        return $this->spg;
    }

    public function setSpg(?string $spg): void
    {
        $this->spg = $spg;
    }

    public function getOrdreCheque(): ?string
    {
        return $this->ordreCheque;
    }

    public function setOrdreCheque(?string $ordreCheque): Ferme
    {
        $this->ordreCheque = $ordreCheque;

        return $this;
    }

    public function getEvementAbonnements(): Collection
    {
        return $this->evementAbonnements;
    }
}
