<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\ORM\Mapping as ORM;
use PsrLib\Validator\UniqueEntity;

#[UniqueEntity(fields: ['campagne', 'amapien'])]
#[Auditable]
#[ORM\Table(name: 'ak_campagne_bulletin_recus')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\CampagneBulletinRecusRepository::class)]
#[ORM\HasLifecycleCallbacks]
class CampagneBulletinRecus
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    private int $numeroInterne;

    #[ORM\Column(type: 'string', length: 255)]
    private string $numero;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\CampagneBulletin::class, mappedBy: 'recus')]
    private ?\PsrLib\ORM\Entity\CampagneBulletin $bulletin = null;

    #[ORM\OneToOne(targetEntity: \PsrLib\ORM\Entity\Files\CampagneBulletinRecus::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private ?\PsrLib\ORM\Entity\Files\CampagneBulletinRecus $pdf;

    public function __construct(int $numeroInterne, string $numero, ?Files\CampagneBulletinRecus $pdf)
    {
        $this->numeroInterne = $numeroInterne;
        $this->numero = $numero;
        $this->pdf = $pdf;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): CampagneBulletinRecus
    {
        $this->id = $id;

        return $this;
    }

    public function getPdf(): ?Files\CampagneBulletinRecus
    {
        return $this->pdf;
    }

    public function setPdf(?Files\CampagneBulletinRecus $pdf): CampagneBulletinRecus
    {
        $this->pdf = $pdf;

        return $this;
    }

    public function getNumeroInterne(): int
    {
        return $this->numeroInterne;
    }

    public function setNumeroInterne(int $numeroInterne): CampagneBulletinRecus
    {
        $this->numeroInterne = $numeroInterne;

        return $this;
    }

    public function getNumero(): string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): CampagneBulletinRecus
    {
        $this->numero = $numero;

        return $this;
    }

    public function getBulletin(): ?CampagneBulletin
    {
        return $this->bulletin;
    }

    public function setBulletin(?CampagneBulletin $bulletin): CampagneBulletinRecus
    {
        $this->bulletin = $bulletin;

        return $this;
    }
}
