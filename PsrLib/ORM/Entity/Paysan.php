<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Entity;

use DH\Auditor\Provider\Doctrine\Auditing\Annotation\Auditable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[Auditable]
#[ORM\Table(name: 'ak_paysan')]
#[ORM\Entity(repositoryClass: \PsrLib\ORM\Repository\PaysanRepository::class)]
class Paysan implements \Stringable, ActivableUserInterface
{
    use ActivableUserTrait;

    #[ORM\Column(name: 'pay_id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    /**
     * @var Collection<Ferme>
     */
    #[ORM\ManyToMany(targetEntity: \PsrLib\ORM\Entity\Ferme::class, inversedBy: 'paysans', cascade: ['PERSIST'])]
    #[ORM\JoinColumn(name: 'fp_paysan_id', referencedColumnName: 'pay_id')]
    #[ORM\InverseJoinColumn(name: 'fp_ferme_id', referencedColumnName: 'f_id')]
    private Collection $fermes;

    #[ORM\OneToOne(inversedBy: 'paysan', targetEntity: User::class)]
    private ?\PsrLib\ORM\Entity\User $user = null;

    #[ORM\Column(name: 'pay_etat', type: 'string', length: 50, nullable: false)]
    private string $etat = ActivableUserInterface::ETAT_INACTIF;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->fermes = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->user;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function addFerme(Ferme $ferme): Paysan
    {
        $this->fermes[] = $ferme;

        return $this;
    }

    public function removeFerme(Ferme $ferme): bool
    {
        return $this->fermes->removeElement($ferme);
    }

    /**
     * @return Collection<Ferme>
     */
    public function getFermes()
    {
        return $this->fermes;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getEtat(): string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): void
    {
        $this->etat = $etat;
    }
}
