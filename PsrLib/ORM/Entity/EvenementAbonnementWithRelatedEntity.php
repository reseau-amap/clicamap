<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

abstract class EvenementAbonnementWithRelatedEntity extends EvenementAbonnement
{
    abstract public function getRelated(): EvenementRelatedEntity;

    abstract public function setRelated(EvenementRelatedEntity $related): self;

    public static function create(User $user, EvenementRelatedEntity $related): self
    {
        $evt = new static($user);
        $evt->setRelated($related);

        return $evt;
    }
}
