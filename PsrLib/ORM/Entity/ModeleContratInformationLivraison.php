<?php

declare(strict_types=1);

namespace PsrLib\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[Embeddable]
class ModeleContratInformationLivraison
{
    #[ORM\Column(name: 'mc_amapien_gestion_dpcmt', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci de sélectionner une option')]
    private ?bool $amapienGestionDeplacement = null;

    #[ORM\Column(name: 'mc_produits_identique_paysan', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci de sélectionner une option')]
    private ?bool $produitsIdentiquePaysan = null;

    #[ORM\Column(name: 'mc_produits_identique_amapien', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci de sélectionner une option')]
    private ?bool $produitsIdentiqueAmapien = null;

    #[ORM\Column(name: 'mc_nbliv_plancher', type: 'integer', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci d\'indiquer une valeur')]
    private ?int $nblivPlancher = null;

    #[ORM\Column(name: 'mc_amapien_permission_dpcmt_livraison', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci de sélectionner une option')]
    private ?bool $amapienPermissionDeplacementLivraison = false;

    #[ORM\Column(name: 'mc_amapien_deplacement_mode', type: 'string', length: 255, nullable: true)]
    #[Groups(['wizard'])]
    /**
     * @deprecated removed "demi" option so this field has only one avaliable value
     * Remove it after all active contracts with old option are terminated
     */
    private ?string $amapienDeplacementMode = 'entier';

    #[ORM\Column(name: 'mc_amapien_deplacement_nb', type: 'integer', nullable: true)]
    #[Groups(['wizard'])]
    private ?int $amapienDeplacementNb = null;

    #[ORM\Column(name: 'mc_amapien_permission_report_livraison', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'], message: 'Merci de sélectionner une option')]
    private ?bool $amapienPermissionReportLivraison = false;

    #[ORM\Column(name: 'mc_nbliv_plancher_depassement', type: 'boolean', nullable: true)]
    #[Groups(['wizard'])]
    #[Assert\NotNull(groups: ['form4'])]
    private ?bool $nblivPlancherDepassement = false;

    public function getAmapienGestionDeplacement(): ?bool
    {
        return $this->amapienGestionDeplacement;
    }

    public function setAmapienGestionDeplacement(?bool $amapienGestionDeplacement): ModeleContratInformationLivraison
    {
        $this->amapienGestionDeplacement = $amapienGestionDeplacement;

        return $this;
    }

    public function getProduitsIdentiquePaysan(): ?bool
    {
        return $this->produitsIdentiquePaysan;
    }

    public function setProduitsIdentiquePaysan(?bool $produitsIdentiquePaysan): ModeleContratInformationLivraison
    {
        $this->produitsIdentiquePaysan = $produitsIdentiquePaysan;

        return $this;
    }

    public function getProduitsIdentiqueAmapien(): ?bool
    {
        return $this->produitsIdentiqueAmapien;
    }

    public function setProduitsIdentiqueAmapien(?bool $produitsIdentiqueAmapien): ModeleContratInformationLivraison
    {
        $this->produitsIdentiqueAmapien = $produitsIdentiqueAmapien;

        return $this;
    }

    public function getAmapienPermissionDeplacementLivraison(): ?bool
    {
        return $this->amapienPermissionDeplacementLivraison;
    }

    public function setAmapienPermissionDeplacementLivraison(?bool $amapienPermissionDeplacementLivraison): ModeleContratInformationLivraison
    {
        $this->amapienPermissionDeplacementLivraison = $amapienPermissionDeplacementLivraison;

        return $this;
    }

    public function getAmapienDeplacementMode(): ?string
    {
        return $this->amapienDeplacementMode;
    }

    public function setAmapienDeplacementMode(?string $amapienDeplacementMode): ModeleContratInformationLivraison
    {
        $this->amapienDeplacementMode = $amapienDeplacementMode;

        return $this;
    }

    public function getAmapienDeplacementNb(): ?int
    {
        return $this->amapienDeplacementNb;
    }

    public function setAmapienDeplacementNb(?int $amapienDeplacementNb): ModeleContratInformationLivraison
    {
        $this->amapienDeplacementNb = $amapienDeplacementNb;

        return $this;
    }

    public function getAmapienPermissionReportLivraison(): ?bool
    {
        return $this->amapienPermissionReportLivraison;
    }

    public function setAmapienPermissionReportLivraison(?bool $amapienPermissionReportLivraison): ModeleContratInformationLivraison
    {
        $this->amapienPermissionReportLivraison = $amapienPermissionReportLivraison;

        return $this;
    }

    public function getNblivPlancherDepassement(): ?bool
    {
        return $this->nblivPlancherDepassement;
    }

    public function setNblivPlancherDepassement(?bool $nblivPlancherDepassement): ModeleContratInformationLivraison
    {
        $this->nblivPlancherDepassement = $nblivPlancherDepassement;

        return $this;
    }

    public function getNblivPlancher(): ?int
    {
        return $this->nblivPlancher;
    }

    public function setNblivPlancher(?int $nblivPlancher): ModeleContratInformationLivraison
    {
        $this->nblivPlancher = $nblivPlancher;

        return $this;
    }
}
