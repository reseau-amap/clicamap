<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\EventSubscriber;

use Doctrine\ORM\Event\PreRemoveEventArgs;
use PsrLib\ORM\Entity\Files\File;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class FilePostRemoveListener
{
    /**
     * @var string[]
     */
    private $filesToRemove = [];

    public function preRemove(File $file, PreRemoveEventArgs $args): void
    {
        $this->filesToRemove[] = $file->getFileFullPath();
    }

    public function postRemove(): void
    {
        $fs = new Filesystem();
        foreach ($this->filesToRemove as $fileToRemove) {
            try {
                $fs->remove($fileToRemove);
            } catch (IOException) {
                // Silently pass exception as file can not exist in amapk
            }
        }
    }
}
