<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchAmapienAmapState;
use PsrLib\DTO\SearchAmapienState;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Entity\User;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @extends EntityRepository<Amapien>
 */
class AmapienRepository extends EntityRepository
{
    public function add(Amapien $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(Amapien $entity, bool $flush = false): void
    {
        $amap = $entity->getAmap();
        foreach ($amap->getCollectifs() as $collectif) {
            if ($collectif->getAmapien() === $entity) {
                $this->_em->remove($collectif);
            }
        }
        if ($amap->getAmapienRefReseau() === $entity) {
            $amap->setAmapienRefReseau(null);
        }
        if ($amap->getAmapienRefReseauSecondaire() === $entity) {
            $amap->setAmapienRefReseauSecondaire(null);
        }

        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public static function criteriaGestionaire(): Criteria
    {
        return Criteria::create()
            ->where(Criteria::expr()->eq('gestionnaire', true))
        ;
    }

    public function qbByAmap(Amap $amap): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->orderBy('u.name.lastName', 'ASC')
            ->addOrderBy('u.name.firstName', 'ASC')
        ;
    }

    /**
     * @return Amapien[]
     */
    public function search(SearchAmapienAmapState $amapienSearchState): array
    {
        if ($amapienSearchState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('a')
            ->orderBy('u.name.lastName', 'ASC')

            // optimisation
            ->leftJoin('a.amap', 'amap')
            ->addSelect('amap')
            ->leftJoin('amap.livraisonLieux', 'll')
            ->addSelect('ll')
            ->leftJoin('ll.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
            ->leftJoin('a.fermeAttentes', 'ferme_attentes')
            ->addSelect('ferme_attentes')
            ->leftJoin('a.user', 'u')
            ->addSelect('u')
            ->leftJoin('u.emails', 'emails')
            ->addSelect('emails')
            ->leftJoin('u.ville', 'uVille')
            ->addSelect('uVille')
            ->leftJoin('u.amaps', 'user_amaps')
            ->addSelect('user_amaps')
            ->leftJoin('u.paysan', 'paysan')
            ->addSelect('paysan')
        ;

        if (null !== $amapienSearchState->getAmap()) {
            $qb
                ->andWhere('a.amap = :amap')
                ->setParameter('amap', $amapienSearchState->getAmap())
            ;
        }

        if (null !== $amapienSearchState->getAdhesion()) {
            $qb
                ->leftJoin('a.anneeAdhesions', 'aa')
                ->andWhere('aa.annee = :annee')
                ->setParameter('annee', $amapienSearchState->getAdhesion())
            ;
        }

        if (null !== $amapienSearchState->getKeyWord()) {
            $qb
                ->andWhere('u.name.lastName LIKE :keyword')
                ->setParameter('keyword', '%'.$amapienSearchState->getKeyWord().'%')
            ;
        }

        if (true === $amapienSearchState->getFilter()) {
            $qb
                ->andWhere('a.etat = :etat')
                ->setParameter('etat', ActivableUserInterface::ETAT_ACTIF)
            ;
        }

        if (false === $amapienSearchState->getFilter()) {
            $qb
                ->andWhere('a.etat = :etat')
                ->setParameter('etat', ActivableUserInterface::ETAT_INACTIF)
            ;
        }

        if ($amapienSearchState  instanceof SearchAmapienState) {
            if (null !== $amapienSearchState->getRegion()) {
                $qb
                    ->andWhere('departement.region = :region')
                    ->setParameter('region', $amapienSearchState->getRegion())
                ;
            }

            if (null !== $amapienSearchState->getDepartement()) {
                $qb
                    ->andWhere('departement = :departement')
                    ->setParameter('departement', $amapienSearchState->getDepartement())
                ;
            }
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    public function qbRefProduitFromAmap(Amap $amap): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->orderBy('u.name.lastName', 'ASC')
            ->where('a.refProdFermes IS NOT EMPTY')
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $amap)
        ;
    }

    public function qbNotRefProduitFromAmapFerme(Amap $amap, Ferme $ferme): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->orderBy('u.name.lastName', 'ASC')
            ->where(':ferme NOT MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $amap)
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmap(Amap $amap): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromMc(ModeleContrat $mc): array
    {
        return $this
            ->createQueryBuilder('a')
            ->where(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $mc->getFerme())
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $mc->getAmap())
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmapFerme(Amap $amap, Ferme $ferme): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->andWhere(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Amap[] $amaps
     *
     * @return Amapien[]
     */
    public function getRefProduitFromMultipleAmapsFerme($amaps, Ferme $ferme): array
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->orderBy('u.name.lastName', 'ASC')
            ->andWhere(':ferme MEMBER OF a.refProdFermes')
            ->setParameter('ferme', $ferme)
            ->andWhere('a.amap IN (:amaps)')
            ->setParameter('amaps', $amaps)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getRefProduitFromAmapTp(Amap $amap, TypeProduction $typeProduction): array
    {
        return $this
            ->qbRefProduitFromAmap($amap)
            ->leftJoin('a.refProdFermes', 'ferme')
            ->leftJoin('ferme.produits', 'p')
            ->where('a.amap = :amap')
            ->andWhere('p.typeProduction = :typeproduction')
            ->setParameter('typeproduction', $typeProduction)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getAllForceableFromModeleContract(ModeleContrat $modeleContrat): array
    {
        /** @var Amapien[] $amapienWithContractFromMc */
        $amapienWithContractFromMc = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.contrats', 'contrats')
            ->where('contrats.modeleContrat = :modeleContrat')
            ->setParameter('modeleContrat', $modeleContrat)
            ->getQuery()
            ->getResult()
        ;

        $amap = $modeleContrat->getLivraisonLieu()->getAmap();

        $qb = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->leftJoin('a.contrats', 'contrats')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('a.etat = :etat')
            ->setParameter('etat', ActivableUserInterface::ETAT_ACTIF)
            ->andWhere(':mcFerme NOT MEMBER OF a.fermeAttentes')
            ->setParameter('mcFerme', $modeleContrat->getFerme())
            ->orderBy('u.name.lastName', 'ASC')
        ;

        if (count($amapienWithContractFromMc) > 0) {
            $qb
                ->andWhere('a NOT IN (:amapienWithContract)')
                ->setParameter(':amapienWithContract', $amapienWithContractFromMc)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function qbDistributionAmapienAvailiable(AmapDistribution $amapDistribution): QueryBuilder
    {
        $qb = $this
            ->qbByAmap($amapDistribution->getAmapLivraisonLieu()->getAmap())
            ->andWhere('a.etat = :etat')
            ->setParameter('etat', ActivableUserInterface::ETAT_ACTIF)
        ;

        $amapiens = $amapDistribution->getAmapiens()->toArray();
        if (count($amapiens) > 0) {
            $qb
                ->andWhere('a.id NOT IN (:amapiens)')
                ->setParameter('amapiens', $amapDistribution->getAmapiens()->toArray())
            ;
        }

        return $qb;
    }

    public function qbActiveAmapienWithContratValidatedDate(Amap $amap, Ferme $ferme, Carbon $date): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.contrats', 'c')
            ->leftJoin('c.modeleContrat', 'mc')
            ->leftJoin('c.cellules', 'cellules')
            ->leftJoin('cellules.modeleContratDate', 'mcd')
            ->leftJoin('a.user', 'u')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('a.etat = :etat')
            ->setParameter('etat', ActivableUserInterface::ETAT_ACTIF)
            ->andWhere('mcd.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('cellules.quantite > 0')
            ->andWhere('c.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_VALIDATED)
            ->orderBy('u.name.lastName', 'ASC')
            ->addOrderBy('u.name.firstName', 'ASC')
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getActiveAmapienWithContractValidatedDate(Amap $amap, Ferme $ferme, Carbon $date)
    {
        return $this
            ->qbActiveAmapienWithContratValidatedDate($amap, $ferme, $date)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amapien[]
     */
    public function getActiveAmapienWithContractDateNotDelivered(Amap $amap, Ferme $ferme, Carbon $date): array
    {
        return $this
            ->qbActiveAmapienWithContratValidatedDate($amap, $ferme, $date)
            ->andWhere('a NOT IN (SELECT amapien FROM PsrLib\ORM\Entity\Amapien amapien JOIN PsrLib\ORM\Entity\ContratLivraison cl WITH cl.amapien = amapien WHERE cl.ferme = :ferme AND cl.date = :date)')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getOneByEmail(string $email): ?Amapien
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->leftJoin('u.emails', 'e')
            ->where('e.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return string[]
     */
    public function getAllEmails(Amap $amap): array
    {
        $emails = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->leftJoin('u.emails', 'e')
            ->leftJoin('a.amap', 'amap')
            ->select('e.email')
            ->where('e.email IS NOT NULL')
            ->andWhere('amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('e.email != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    public function getByAmapEmail(Amap $amap, string $email): ?Amapien
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.user', 'u')
            ->leftJoin('u.emails', 'e')
            ->where('e.email = :email')
            ->setParameter('email', $email)
            ->andWhere('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneDeletedByAmapUser(Amap $amap, User $user): ?Amapien
    {
        if (null === $amap->getId() || null === $user->getId()) {
            return null;
        }

        // @phpstan-ignore-next-line
        $this->getEntityManager()->getFilters()->getFilter('soft-deleteable')->disableForEntity(Amapien::class);
        $exisingAmapien = $this
            ->createQueryBuilder('a')
            ->where('a.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('a.user = :user')
            ->setParameter('user', $user)
            ->andWhere('a.deletedAt IS NOT NULL')
            ->getQuery()
            ->getOneOrNullResult()
        ;
        // @phpstan-ignore-next-line
        $this->getEntityManager()->getFilters()->getFilter('soft-deleteable')->enableForEntity(Amapien::class);

        return $exisingAmapien;
    }
}
