<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<Departement>
 */
class DepartementRepository extends EntityRepository
{
    private array $cachedUserDepartementMap = [];

    public function qbAllOrdered(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('r')
            ->orderBy('r.nom', 'ASC')
        ;
    }

    public function qbByRegionOrdered(Region $region): QueryBuilder
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.region = :region')
            ->setParameter('region', $region)
            ->orderBy('d.nom', 'ASC')
        ;
    }

    /**
     * @return Departement[]
     */
    public function findWithLogo()
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.logo IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Region[] $regions
     *
     * @return Departement[]
     */
    public function findByMultipleRegion($regions)
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.region in (:regions)')
            ->setParameter('regions', $regions)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get accessible region for user. Memory cache result to avoid recurring database requests.
     *
     * @return Departement[]
     */
    public function getDepartementsForUserCached(User $user, ?Region $region)
    {
        $userId = $user->getId();
        if (isset($this->cachedUserDepartementMap[$userId])) {
            return $this->cachedUserDepartementMap[$userId];
        }

        if ($user->isAdminDepartment()) {
            $departements = $user->getAdminDepartments()->toArray();
        } else {
            $departements = $this->findBy(['region' => $region]);
        }

        $this->cachedUserDepartementMap[$userId] = $departements;

        return $departements;
    }

    /**
     * @return string[]
     */
    public function getDepartementNames()
    {
        return $this
            ->createQueryBuilder('d')
            ->select('d.nom AS name', 'd.osmId AS osmId')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findSingleByNameLiked(string $name): ?Departement
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.nom LIKE :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function qbDepartementAsAdmin(User $user): QueryBuilder
    {
        if ($user->isSuperAdmin()) {
            return $this->qbAllOrdered();
        }

        return $this
            ->createQueryBuilder('d')
            ->where(':user MEMBER OF d.admins')
            ->setParameter('user', $user)
            ->orderBy('d.nom', 'ASC')
        ;
    }
}
