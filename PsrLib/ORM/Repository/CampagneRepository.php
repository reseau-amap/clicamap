<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Carbon\Carbon;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use PsrLib\DTO\SearchCampagneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<Campagne>
 */
class CampagneRepository extends EntityRepository
{
    /**
     * @return Campagne[]
     */
    public function search(SearchCampagneState $state)
    {
        $amap = $state->getAmap();
        if (null === $amap) {
            return [];
        }

        return $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amap', 'a')
            ->where('c.versionSuivante IS NULL')
            ->andWhere('a = :amap')
            ->setParameter('amap', $amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Campagne[]
     */
    public function findPendingSubscribableForUser(User $user)
    {
        $now = Carbon::now();

        $campagneIds = $this
            ->createQueryBuilder('c')
            ->select('c.id')
            ->leftJoin('c.amap', 'a')
            ->leftJoin('a.amapiens', 'amapien')
            ->where('c.versionSuivante IS NULL')
            ->andWhere('amapien.user = :user')
            ->andWhere('c.period.startAt <= :now')
            ->andWhere('c.period.endAt >= :now')
            ->setParameter('user', $user)
            ->setParameter('now', $now)
            ->getQuery()
            ->getSingleColumnResult()
        ;

        // Filter subscribed
        $campagneIds = array_filter($campagneIds, function (int $campagneId) use ($user) {
            $rsm = new ResultSetMapping();
            $rsm->addscalarResult('id', 'id');

            $ancestorIds = $this->getEntityManager()->createNativeQuery('
                WITH RECURSIVE ancestors AS (
                    SELECT id, version_suivante_id, 0 AS level FROM ak_campagne
                       WHERE id=:id
                     UNION ALL
                        SELECT c.id, c.version_suivante_id, level+1
                    FROM ak_campagne as c, ancestors as a
                        WHERE c.version_suivante_id=a.id
                )
                SELECT id FROM ancestors;
            ', $rsm)->setParameter('id', $campagneId)->getSingleColumnResult();

            $campagneBulletinCount = $this
                ->getEntityManager()
                ->createQueryBuilder()
                ->select('COUNT(b)')
                ->from('PsrLib\ORM\Entity\CampagneBulletin', 'b')
                ->leftJoin('b.amapien', 'amapien')
                ->where('b.campagne IN (:ancestorIds)')
                ->andWhere('amapien.user = :user')
                ->setParameter('ancestorIds', $ancestorIds)
                ->setParameter('user', $user)
                ->getQuery()
                ->getSingleScalarResult()
            ;

            return 0 === $campagneBulletinCount;
        });

        return $this->findBy(['id' => $campagneIds]);
    }

    /**
     * @return string[]
     */
    public function findAnneeByAmap(Amap $amap)
    {
        return $this
            ->createQueryBuilder('c')
            ->select('DISTINCT c.anneeAdhesionAmapien')
            ->where('c.amap = :amap')
            ->setParameter('amap', $amap)
            ->getQuery()
            ->getSingleColumnResult()
        ;
    }

    /**
     * @return Campagne[]
     */
    public function findForAdmin(User $user)
    {
        $qb = $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amap', 'a')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
            ->where('c.versionSuivante IS NULL')
            ->andWhere('a.associationDeFait = :associationDeFait')
        ;
        $qb
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->in('d', ':departements'),
                    $qb->expr()->in('r', ':regions'),
                )
            )
            ->setParameter('associationDeFait', true)
            ->setParameter('departements', $user->getAdminDepartments()->toArray())
            ->setParameter('regions', $user->getAdminRegions()->toArray())
        ;

        return $qb->getQuery()->getResult();
    }
}
