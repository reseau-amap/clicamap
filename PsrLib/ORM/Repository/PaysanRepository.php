<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchPaysanState;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\Reseau;

class PaysanRepository extends BaseUserRepository
{
    /**
     * @return Ferme[]
     */
    public function search(SearchPaysanState $searchPaysanState): array
    {
        if ($searchPaysanState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('p')
            ->orderBy('u.name.lastName', 'ASC')

            // optimisation
            ->leftJoin('p.fermes', 'f')
            ->addSelect('f')
            ->leftJoin('f.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
            ->leftJoin('departement.region', 'region')
            ->addSelect('region')
            ->leftJoin('f.produits', 'produits')
            ->addSelect('produits')
            ->leftJoin('p.user', 'u')
            ->addSelect('u')
        ;

        $qb
            ->where('p.fermes IS NOT EMPTY')
        ;

        if (null !== $searchPaysanState->getRegion()) {
            $qb
                ->andWhere('departement.region = :region')
                ->setParameter('region', $searchPaysanState->getRegion())
            ;
        }

        if (null !== $searchPaysanState->getDepartement()) {
            $qb
                ->andWhere('departement = :departement')
                ->setParameter('departement', $searchPaysanState->getDepartement())
            ;
        }

        if (null !== $searchPaysanState->getKeyWord()) {
            $qb
                ->andWhere('u.name.lastName LIKE :keyword')
                ->setParameter('keyword', '%'.$searchPaysanState->getKeyWord().'%')
            ;
        }

        if (null !== $searchPaysanState->getTypeProduction()) {
            $qb
                ->andWhere('produits.typeProduction = :tp')
                ->setParameter('tp', $searchPaysanState->getTypeProduction())
            ;
        }

        if (null !== $searchPaysanState->getAdhesion()) {
            $qb
                ->leftJoin('f.anneeAdhesions', 'aa')
                ->andWhere('aa.annee = :annee')
                ->setParameter('annee', $searchPaysanState->getAdhesion())
            ;
        }

        if (null !== $searchPaysanState->getCommercialisation()) {
            $qb
                ->andWhere('f.anneeDebCommercialisationAmap = :anneeDebCommercialisationAmap')
                ->setParameter('anneeDebCommercialisationAmap', $searchPaysanState->getCommercialisation())
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return string[]
     */
    public function getAllEmails()
    {
        $emails = $this
            ->createQueryBuilder('p')
            ->leftJoin('p.user', 'u')
            ->leftJoin('u.emails', 'e')
            ->select('e.email')
            ->where('e.email IS NOT NULL')
            ->andWhere('e.email != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    /**
     * @return Paysan[]
     */
    public function searchByName(string $nom): array
    {
        return $this
            ->createQueryBuilder('p')
            ->where('p.nom LIKE :nom')
            ->setParameter('nom', '%'.$nom.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int[]    $reg_ids
     * @param int[]    $dep_ids
     * @param int[]    $res_ids
     * @param string[] $tp_slugs
     * @param string   $en_recherche_partenariat
     * @param string   $adh_oui
     *
     * @return Paysan[]
     */
    public function publipostage_mdr($reg_ids, $dep_ids, $res_ids, $tp_slugs, $en_recherche_partenariat, $adh_oui)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.fermes', 'f')
            ->leftJoin('f.anneeAdhesions', 'aa')
            ->leftJoin('f.ville', 'ville')
            ->leftJoin(Reseau::class, 'reseau', Join::WITH, 'ville MEMBER OF reseau.villes')
            ->leftJoin('ville.departement', 'd')
            ->leftJoin('d.region', 'region')
        ;

        if ($res_ids) {
            $qb
                ->andWhere('reseau.id IN (:res_ids)')
                ->setParameter('res_ids', $res_ids)
            ;
        }

        if ($tp_slugs) {
            $qb
                ->leftJoin('f.produits', 'produits')
                ->leftJoin('produits.typeProduction', 'tp')
                ->where('tp.slug IN (:tp_slugs)')
                ->setParameter('tp_slugs', $tp_slugs)
            ;
        }

        if ('1' === $adh_oui) {
            $annee_en_cours = date('Y');
            $annee_passee = $annee_en_cours - 1;
            $annees = [$annee_en_cours, $annee_passee];

            $qb
                ->andWhere('aa.annee IN (:annees)')
                ->setParameter('annees', $annees)
            ;
        }
        // -----------------------------------------------------------------------
        if ($res_ids) {
            $qb
                ->andWhere('reseau.id IN (:res_ids)')
                ->setParameter('res_ids', $res_ids)
            ;
        } elseif ($dep_ids) {
            $qb
                ->andWhere('d.id IN (:dep_ids)')
                ->setParameter('dep_ids', $dep_ids)
            ;
        } elseif ($reg_ids) {
            $qb
                ->andWhere('region.id IN (:reg_ids)')
                ->setParameter('reg_ids', $reg_ids)
            ;
        }
        // -----------------------------------------------------------------------

        $qb->orderBy('u.name.lastName', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Ferme[] $fermes
     *
     * @return Paysan[]
     */
    public function findByFermeMultiple($fermes): array
    {
        return $this
            ->createQueryBuilder('p')
            ->leftJoin('p.fermes', 'f')
            ->where('f IN (:fermes)')
            ->setParameter('fermes', $fermes)
            ->getQuery()
            ->getResult()
        ;
    }

    public function qbNoInFerme(Ferme $ferme): QueryBuilder
    {
        return $this
            ->createQueryBuilder('p')
            ->where(':ferme NOT MEMBER OF p.fermes')
            ->setParameter('ferme', $ferme)
        ;
    }
}
