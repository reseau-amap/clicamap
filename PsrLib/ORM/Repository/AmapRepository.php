<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use DH\Auditor\Auditor;
use DH\Auditor\Provider\Doctrine\DoctrineProvider;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use PsrLib\DTO\SearchAmapState;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\Reseau;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\PhpDiContrainerSingleton;

class AmapRepository extends BaseUserRepository
{
    private function getSoftDeleteableListener(): SoftDeleteableListener
    {
        $evm = $this->_em->getEventManager();
        $listeners = $evm->getListeners();
        foreach ($listeners['onFlush'] as $listener) {
            if ($listener instanceof SoftDeleteableListener) {
                return $listener;
            }
        }
        throw new \RuntimeException('SoftDeleteableListener not found');
    }

    public function remove(Amap $entity): void
    {
        // Remove event subscriber to force remove all amapiens
        // @phpstan-ignore-next-line
        $this->_em->getFilters()->getFilter('soft-deleteable')->disableForEntity(Amapien::class);
        $listener = $this->getSoftDeleteableListener();
        $evm = $this->_em->getEventManager();
        $evm->removeEventSubscriber($listener);

        // Refresh to force load deleted users
        $this->_em->refresh($entity);

        // Performance optimisation
        $auditor = PhpDiContrainerSingleton::getContainer()->get(Auditor::class);
        $auditorProviderConfig = $auditor->getProvider(DoctrineProvider::class)->getConfiguration();
        $auditorProviderConfig->disableAuditFor(ContratCellule::class);

        // Does not use DQL query for audit log compatibility
        $this->_em->getConnection()->transactional(function () use ($entity): void {
            // Cleanup as SET NULL is not triggered in this case
            $entity->setAmapienRefReseau(null);
            $entity->setAmapienRefReseauSecondaire(null);

            // Remove on amap suppression but not on amapien
            foreach ($entity->getAdhesionsAmapAmapienAsCreator() as $adhesion) {
                $this->_em->remove($adhesion);
            }
            $this->_em->flush();

            $contrats = [];
            $modeleContrats = [];
            // Remove on amap suppression but not on amapien
            foreach ($entity->getLivraisonLieux() as $ll) {
                foreach ($ll->getModeleContrats() as $modeleContrat) {
                    foreach ($modeleContrat->getContrats() as $contrat) {
                        $contrats[] = $contrat;
                    }
                    $modeleContrats[] = $modeleContrat;
                }
            }
            foreach ($entity->getAmapiens() as $amapien) {
                foreach ($amapien->getContrats() as $contrat) { // Get in both ways for database inconsistency
                    $contrats[] = $contrat;
                }
            }

            $this->_em->getRepository(ContratCellule::class)->removeAllFromContrats($contrats); // Optimisation
            foreach ($contrats as $contrat) {
                $this->_em->remove($contrat);
            }
            $this->_em->flush();
            foreach ($modeleContrats as $modeleContrat) {
                $this->_em->remove($modeleContrat);
            }
            $this->_em->flush();

            // Remove on amap suppression but not on amapien
            foreach ($entity->getAmapiens() as $amapien) {
                $this->_em->remove($amapien);
            }

            $this->_em->flush();

            $this->_em->remove($entity);
            $this->_em->flush();
        });

        // @phpstan-ignore-next-line
        $this->_em->getFilters()->getFilter('soft-deleteable')->enableForEntity(Amapien::class);
        $evm->addEventSubscriber(new SoftDeleteableListener());
        $auditorProviderConfig->enableAuditFor(ContratCellule::class);
    }

    public function qbAllOrdered(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
        ;
    }

    public function qbAmapAccessibleForAmapien(User $amapien): QueryBuilder
    {
        if ($amapien->isSuperAdmin()) {
            return $this->qbBaseForLocationSearch();
        }

        if ($amapien->isAdminRegion()) {
            return $this
                ->qbBaseForLocationSearch()
                ->andWhere('departement.region IN (:regions)')
                ->setParameter('regions', $amapien->getAdminRegions()->toArray())
            ;
        }

        if ($amapien->isAdminDepartment()) {
            return $this
                ->qbBaseForLocationSearch()
                ->andWhere('departement IN (:departements)')
                ->setParameter('departements', $amapien->getAdminDepartments()->toArray())
            ;
        }

        // Defaut amapien's amap
        return $this
            ->qbBaseForLocationSearch()
            ->leftJoin('a.amapiens', 'amapien')
            ->andWhere('amapien.user = :amapien')
            ->setParameter('amapien', $amapien)
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByRegion(Region $region): array
    {
        return $this
            ->qbBaseForLocationSearch()
            ->where('departement.region = :region')
            ->setParameter('region', $region)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByReseau(Reseau $reseau)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'ville')
            ->leftJoin(Reseau::class, 'reseau', Join::WITH, 'reseau.ville = :ville')
            ->where('reseau = :reseau')
            ->setParameter('reseau', $reseau)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByDepartement(Departement $departement): array
    {
        return $this
            ->qbBaseForLocationSearch()
            ->where('departement = :departement')
            ->setParameter('departement', $departement)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Ferme[] $fermes
     */
    public function qbByFermes($fermes): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.amapiens', 'amapiens')
            ->leftJoin('amapiens.refProdFermes', 'ferme')
            ->andWhere('ferme in (:fermes)')
            ->setParameter('fermes', $fermes)
        ;
    }

    /**
     * @return Amap[]
     */
    public function findByFerme(Ferme $ferme)
    {
        return $this
            ->qbByFermes([$ferme])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Ferme[]
     */
    public function findByFermes(mixed $fermes)
    {
        return $this
            ->qbByFermes($fermes)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Amap[]
     */
    public function search(SearchAmapState $amapSearchState): array
    {
        $qb = $this->qbBaseForLocationSearch()
            // optimisation
            ->leftJoin('a.amapienRefReseau', 'amapienRefReseau')
            ->addSelect('amapienRefReseau')
            ->leftJoin('a.amapienRefReseauSecondaire', 'amapienRefReseauSecondaire')
            ->addSelect('amapienRefReseauSecondaire')
        ;

        if ($amapSearchState->isEmpty()) {
            return [];
        }
        if (null !== $amapSearchState->getRegion()) {
            $qb
                ->andWhere('departement.region = :region')
                ->setParameter('region', $amapSearchState->getRegion())
            ;
        }

        if (null !== $amapSearchState->getDepartement()) {
            $qb
                ->andWhere('departement = :departement')
                ->setParameter('departement', $amapSearchState->getDepartement())
            ;
        }

        if (null !== $amapSearchState->getReseaux()) {
            $qb
                ->andWhere('ville IN (:villes)')
                ->setParameter('villes', $amapSearchState->getReseaux()->getVilles())
            ;
        }

        if (null !== $amapSearchState->getAdhesion()) {
            $qb
                ->leftJoin('a.anneeAdhesions', 'aa')
                ->andWhere('aa.annee = :annee')
                ->setParameter('annee', $amapSearchState->getAdhesion())
            ;
        }

        if (null !== $amapSearchState->getEtudiante()) {
            $qb
                ->andWhere('a.amapEtudiante = :etudiante')
                ->setParameter('etudiante', $amapSearchState->getEtudiante())
            ;
        }

        if (null !== $amapSearchState->getKeyWord()) {
            $qb
                ->andWhere('a.nom LIKE :keyword')
                ->setParameter('keyword', '%'.$amapSearchState->getKeyWord().'%')
            ;
        }

        if ($amapSearchState->getInsurance()) {
            $qb
                ->andWhere('a.possedeAssurance = TRUE')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return string[]
     */
    public function getAllEmails()
    {
        $emails = $this
            ->createQueryBuilder('a')
            ->select('a.email')
            ->where('a.email IS NOT NULL')
            ->andWhere('a.email != \'\'')
            ->getQuery()
            ->getArrayResult()
        ;

        return array_column($emails, 'email');
    }

    /**
     * @param int[]  $reg_ids
     * @param int[]  $dep_ids
     * @param int[]  $res_ids
     * @param string $en_recherche_partenariat
     * @param string $adh_oui
     *
     * @return Amap[]
     */
    public function publipostage_mdr($reg_ids, $dep_ids, $res_ids, $en_recherche_partenariat, $adh_oui): array
    {
        $qb = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.anneeAdhesions', 'aa')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
            ->leftJoin(Reseau::class, 'reseau', Join::WITH, 'v MEMBER OF reseau.villes')
        ;

        if ($adh_oui) {
            $annee_en_cours = date('Y');
            $annee_passee = $annee_en_cours - 1;
            $annees = [$annee_en_cours, $annee_passee];

            $qb
                ->andWhere('aa.annee IN (:annees)')
                ->setParameter('annees', $annees)
            ;
        }

        // -----------------------------------------------------------------------
        if ($res_ids) {
            $qb
                ->andWhere('reseau.id IN (:res_ids)')
                ->setParameter('res_ids', $res_ids)
            ;
        } elseif ($dep_ids) {
            $qb
                ->andWhere('d.id IN (:dep_ids)')
                ->setParameter('dep_ids', $dep_ids)
            ;
        } elseif ($reg_ids) {
            $qb
                ->andWhere('d.id IN (:reg_ids)')
                ->setParameter('reg_ids', $reg_ids)
            ;
        }
        $qb->orderBy('a.nom', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Amap[]
     */
    public function getAmapsWithModeleContractValidated(Ferme $ferme, \DateTime $date): array
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.modeleContrats', 'mc')
            ->leftJoin('mc.dates', 'd')
            ->where('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('d.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('mc.etat = :etat')
            ->setParameter('etat', ModeleContratEtat::ETAT_VALIDE)
            ->getQuery()
            ->getResult()
        ;
    }

    private function qbBaseForLocationSearch(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.nom', 'ASC')
            ->leftJoin('a.livraisonLieux', 'll')
            ->addSelect('ll')
            ->leftJoin('ll.ville', 'ville')
            ->addSelect('ville')
            ->leftJoin('ville.departement', 'departement')
            ->addSelect('departement')
        ;
    }

    /**
     * @return Amap[]
     */
    public function findAllAmapDeFaitForAdmin(User $user)
    {
        $qb = $this
            ->createQueryBuilder('a')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'ville')
            ->leftJoin('ville.departement', 'departement')
            ->leftJoin('departement.region', 'region')
            ->where('a.associationDeFait = TRUE')
        ;

        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->in('departement', ':departements'),
            $qb->expr()->in('region', ':regions')
        ));
        $qb->setParameter('departements', $user->getAdminDepartments()->toArray());
        $qb->setParameter('regions', $user->getAdminRegions()->toArray());

        return $qb->getQuery()->getResult();
    }

    public function qbEvenementCreationForUser(User $user): QueryBuilder
    {
        if ($user->isSuperAdmin()) {
            return $this->qbAllOrdered();
        }

        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adresseAdminVille', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
            ->where(':user MEMBER OF a.admins')
            ->orWhere(':user MEMBER OF r.admins')
            ->orWhere(':user MEMBER OF d.admins')
            ->orWhere('a IN (:amaps_ref_produit)')
            ->setParameter('user', $user)
            ->setParameter('amaps_ref_produit', $user->getAmapsAsRefProduit())
            ->orderBy('a.nom', 'ASC')
        ;
    }
}
