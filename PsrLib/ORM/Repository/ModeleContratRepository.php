<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchContratViergeState;
use PsrLib\DTO\SearchFermeLivraisonAmapState;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<ModeleContrat>
 */
class ModeleContratRepository extends EntityRepository
{
    /**
     * @return ModeleContrat[]
     */
    public function search(SearchContratViergeState $contratViergeState)
    {
        if (null === $contratViergeState->getAmap()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $contratViergeState->getAmap())
            ->andWhere('mc.etat in (:states)')
            ->setParameter('states', [ModeleContratEtat::ETAT_BROUILLON, ModeleContratEtat::ETAT_CREATION, ModeleContratEtat::ETAT_VALIDATION_PAYSAN, ModeleContratEtat::ETAT_REFUS])
            ->andWhere('mc.version = 2')
        ;

        if (null !== $contratViergeState->getFerme()) {
            $qb
                ->andWhere('mc.ferme = :ferme')
                ->setParameter('ferme', $contratViergeState->getFerme())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function findToValidateForUser(User $user, Amap $amap = null)
    {
        $qb = $this
            ->createQueryBuilder('mc')
            ->where('mc.ferme IN (:fermes)')
            ->setParameter('fermes', $user->getFermesAsRegroupementOrPaysan())
            ->andWhere('mc.etat = :etat')
            ->setParameter('etat', ModeleContratEtat::ETAT_VALIDATION_PAYSAN)
        ;

        if (null !== $amap) {
            $qb
                ->leftJoin('mc.livraisonLieu', 'll')
                ->andWhere('ll.amap = :amap')
                ->setParameter('amap', $amap)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function findValidatedByFermeAmap(Ferme $ferme, Amap $amap)
    {
        return $this->findValidatedByFermesAmap([$ferme], $amap);
    }

    /**
     * @param Ferme[] $fermes
     *
     * @return ModeleContrat[]
     */
    public function findValidatedByFermesAmap($fermes, Amap $amap)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('mc.ferme IN (:fermes)')
            ->setParameter('fermes', $fermes)
            ->andWhere('mc.etat IN (:state)')
            ->setParameter('state', [ModeleContratEtat::ETAT_VALIDE, ModeleContratEtat::ETAT_ACTIF])
            ->orderBy('mc.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getSubscribableContractForAmapien(User $amapien, bool $excludeSigned = true): array
    {
        $qb = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('ll.amap', 'amap')
            ->leftJoin('amap.amapiens', 'amapiens')
            ->where('amapiens.user =  :amapien')
            ->setParameter('amapien', $amapien)
            ->andWhere('amapiens.etat = :amapien_etat')
            ->setParameter('amapien_etat', ActivableUserInterface::ETAT_ACTIF)
            ->andWhere('mc.etat = :etat')
            ->setParameter('etat', ModeleContratEtat::ETAT_VALIDE)
            ->andWhere('mc NOT IN (SELECT umc FROM PsrLib\ORM\Entity\ModeleContrat umc LEFT JOIN PsrLib\ORM\Entity\Contrat c WITH c.modeleContrat = umc LEFT JOIN PsrLib\ORM\Entity\Amapien uaa WITH c.amapien = uaa WHERE uaa.user = :amapien)')
        ;

        return $qb->getQuery()->getResult();
    }

    public function qbV2ByAmapFermeOrderByForclusion(Amap $amap, Ferme $ferme): QueryBuilder
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('mc.version = 2')
            ->orderBy('mc.forclusion', 'DESC')
        ;
    }

    public function countV2ByAmapFerme(Amap $amap, Ferme $ferme): int
    {
        return (int) $this
            ->qbV2ByAmapFermeOrderByForclusion($amap, $ferme)
            ->select('COUNT(mc)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromAmapDate(Amap $amap, string $date)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.dates', 'dates')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Amap[] $amaps
     *
     * @return array<int|string, array<int, ModeleContrat>>
     */
    public function getFromAmapMultipleByAmap($amaps)
    {
        /** @var ModeleContrat[] $contrats */
        $contrats = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.dates', 'dates')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('ll.amap', 'amap')
            ->addSelect('ll')
            ->addSelect('amap')
            ->andWhere('amap IN (:amaps)')
            ->setParameter('amaps', $amaps)
            ->getQuery()
            ->getResult()
        ;

        $res = [];
        foreach ($contrats as $contrat) {
            $amapId = $contrat->getLivraisonLieu()->getAmap()->getId();
            if (!array_key_exists($amapId, $res)) {
                $res[$amapId] = [];
            }
            $res[$amapId][] = $contrat;
        }

        return $res;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromFermeDate(Ferme $ferme, string $date): array
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.dates', 'dates')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function getFromAmapienDate(Amapien $amapien, string $date)
    {
        return $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.contrats', 'c')
            ->leftJoin('mc.dates', 'dates')
            ->where('dates.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('c.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ModeleContrat[]
     */
    public function searchLivraison(SearchFermeLivraisonAmapState $dto): array
    {
        $qb = $this
            ->createQueryBuilder('mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('mc.dates', 'd')
            ->andWhere('d.dateLivraison = :date')
            ->setParameter('date', $dto->getDate())
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $dto->getFerme())
        ;

        if (null !== $dto->getAmap()) {
            $qb
                ->andWhere('ll.amap = :amap')
                ->setParameter('amap', $dto->getAmap())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ModeleContrat[]
     */
    public function findDelai0NonArchived(): array
    {
        $mc = $this
            ->createQueryBuilder('mc')
            ->where('mc.delaiModifContrat = 0')
            ->getQuery()
            ->getResult()
        ;

        return array_filter($mc, fn (ModeleContrat $mc) => !$mc->isArchived());
    }
}
