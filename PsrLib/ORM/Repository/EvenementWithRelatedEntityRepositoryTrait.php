<?php

declare(strict_types=1);

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\QueryBuilder;
use PsrLib\ORM\Entity\EvenementRelatedEntity;
use PsrLib\ORM\Entity\EvenementWithRelatedEntity;

trait EvenementWithRelatedEntityRepositoryTrait
{
    /**
     * @param EvenementRelatedEntity[] $related
     */
    public function qbByMultipleRelated(array $related): QueryBuilder
    {
        return $this
            ->createQueryBuilder('e')
            ->where('e.related IN (:related)')
            ->setParameter('related', $related)
        ;
    }

    /**
     * @return EvenementWithRelatedEntity[]
     */
    public function findByMultipleRelated(array $related): array
    {
        return $this
            ->qbByMultipleRelated($related)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return EvenementWithRelatedEntity[]
     */
    public function findByMultipleRelatedWithoutDefaultLimited(array $related): array
    {
        return $this  // @phpstan-ignore-line Cause PHP does not support property type hierarchy in inherance
            ->qbByMultipleRelated($related)
            ->andWhere('e.accesAbonnesDefautUniquement = :accesAbonnes')
            ->setParameter('accesAbonnes', false)
            ->getQuery()
            ->getResult()
        ;
    }
}
