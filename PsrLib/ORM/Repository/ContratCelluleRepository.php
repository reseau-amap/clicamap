<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Carbon\CarbonImmutable;
use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraisonCellule;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @extends EntityRepository<ContratCellule>
 */
class ContratCelluleRepository extends EntityRepository
{
    /**
     * @param Contrat[]$contrats
     */
    public function removeAllFromContrats($contrats): void
    {
        $cellulesIds = $this
            ->createQueryBuilder('cc')
            ->select('cc.id')
            ->where('cc.contrat IN (:contrats)')
            ->setParameter('contrats', $contrats)
            ->getQuery()
            ->getSingleColumnResult()
        ;

        $livraisonsIds = $this
            ->getEntityManager()
            ->getRepository(ContratLivraisonCellule::class)
            ->createQueryBuilder('ccl')
            ->leftJoin('ccl.contratCellule', 'cc')
            ->select('ccl.id')
            ->where('cc.id IN (:cellulesIds)')
            ->setParameter('cellulesIds', $cellulesIds)
            ->getQuery()
            ->getSingleColumnResult()
        ;

        $this
            ->createQueryBuilder('cc')
            ->delete(ContratLivraisonCellule::class, 'ccl')
            ->where('ccl.id IN (:livraisonsIds)')
            ->setParameter('livraisonsIds', $livraisonsIds)
            ->getQuery()
            ->execute()
        ;

        $this
            ->createQueryBuilder('cc')
            ->delete(ContratCellule::class, 'cc')
            ->where('cc.id IN (:cellulesIds)')
            ->setParameter('cellulesIds', $cellulesIds)
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * @param ContratCellule[] $commandes
     *
     * @return float
     */
    public static function findCelluleCount(array $commandes, ModeleContratDate $date, ModeleContratProduit $produit)
    {
        foreach ($commandes as $commande) {
            if ($commande->getModeleContratProduit()->getId() === $produit->getId()
                && $commande->getModeleContratDate()->getId() === $date->getId()) {
                return $commande->getQuantite();
            }
        }

        return 0;
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public static function countNbLivraisonsByDate(array $commandes, ModeleContratDate $date): float
    {
        $nbLiv = 0.0;
        foreach ($commandes as $commande) {
            if ((int) $commande->getModeleContratDate()->getId() === (int) $date->getId()) {
                $nbLiv += (float) $commande->getQuantite();
            }
        }

        return $nbLiv;
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public static function countNbLivraisonsByProduct(array $commandes, ModeleContratProduit $produit): float
    {
        $nbLiv = 0.0;
        foreach ($commandes as $commande) {
            if ((int) $commande->getModeleContratProduit()->getId() === (int) $produit->getId()) {
                $nbLiv += (float) $commande->getQuantite();
            }
        }

        return $nbLiv;
    }

    /**
     * @return ContratCellule[]
     */
    public function getFromAmapDateForValidatedContract(Amap $amap, string $date)
    {
        return $this
            ->createQueryBuilder('cc')
            ->leftJoin('cc.contrat', 'contrat')
            ->leftJoin('contrat.modeleContrat', 'mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->leftJoin('cc.modeleContratProduit', 'mcp')
            ->addSelect(['contrat', 'mc', 'll', 'date', 'mcp'])
            ->where('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('date.dateLivraison = :dateLivraison')
            ->setParameter('dateLivraison', $date)
            ->andWhere('contrat.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_VALIDATED)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ContratCellule[]
     */
    public function getFromAmapienDateForValidatedContract(Amapien $amapien, string $date)
    {
        return $this
            ->createQueryBuilder('cc')
            ->leftJoin('cc.contrat', 'contrat')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->where('contrat.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->andWhere('date.dateLivraison = :dateLivraison')
            ->setParameter('dateLivraison', $date)
            ->andWhere('contrat.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_VALIDATED)
            ->getQuery()
            ->getResult()
        ;
    }

    public function countTotalForAmapienInFutureWithQtyPositive(Amapien $amapien): int
    {
        $now = CarbonImmutable::now()->subDay()->endOfDay();

        return (int) $this
            ->createQueryBuilder('cc')
            ->select('COUNT(cc)')
            ->leftJoin('cc.modeleContratDate', 'date')
            ->leftJoin('cc.contrat', 'contrat')
            ->where('contrat.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->andWhere('date.dateLivraison > :dateLivraison')
            ->setParameter('dateLivraison', $now)
            ->andWhere('cc.quantite > 0')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
