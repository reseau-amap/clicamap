<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\ORM\Entity\Actualite;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<Actualite>
 */
class ActualiteRepository extends EntityRepository
{
    /**
     * @param string[] $ids
     *
     * @return Actualite[]
     */
    public function findByMultipleIds(array $ids): array
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getLastNotReadByUser(User $user): ?Actualite
    {
        return $this
            ->createQueryBuilder('a')
            ->where(':user NOT MEMBER OF a.utilsateursLus')
            ->setParameter('user', $user)
            ->orderBy('a.date', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function qbNotReadByUser(User $user): QueryBuilder
    {
        return $this
            ->createQueryBuilder('a')
            ->where(':user NOT MEMBER OF a.utilsateursLus')
            ->setParameter('user', $user)
            ->orderBy('a.date', 'DESC')
        ;
    }

    /**
     * @return Actualite[]
     */
    public function getNotReadAllByUser(User $user): array
    {
        return $this->qbNotReadByUser($user)->getQuery()->getResult();
    }

    public function countNotReadByUser(User $user): int
    {
        return $this
            ->qbNotReadByUser($user)
            ->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
