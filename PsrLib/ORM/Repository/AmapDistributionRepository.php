<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Carbon\Carbon;
use Doctrine\ORM\QueryBuilder;
use PsrLib\DTO\SearchDistributionAmapienState;
use PsrLib\DTO\SearchDistributionAmapState;
use PsrLib\DTO\SearchDistributionBaseState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;

/**
 * @extends ServiceEntityRepository<AmapDistribution>
 */
class AmapDistributionRepository extends ServiceEntityRepository
{
    /**
     * @return AmapDistribution[]
     */
    public function search(?SearchDistributionAmapState $searchAmapDistributionState): array
    {
        if (null === $searchAmapDistributionState) {
            return [];
        }

        $qb = $this->buildSearchQb($searchAmapDistributionState);
        if (null === $qb) {
            return [];
        }

        $res = $qb->getQuery()->getResult();

        if (!$searchAmapDistributionState->isComplete()) {
            return array_filter($res, fn (AmapDistribution $distribution) => !$distribution->isCompleted());
        }

        return $res;
    }

    /**
     * @param string[] $ids
     *
     * @return AmapDistribution[]
     */
    public function findByMultipleIds(array $ids): array
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return AmapDistribution[]
     */
    public function searchAmapienState(?SearchDistributionAmapienState $state): array
    {
        if (null === $state) {
            return [];
        }

        $qb = $this->buildSearchQb($state);
        if (null === $qb) {
            return [];
        }

        if (SearchDistributionAmapienState::FILTER_MINE === $state->getFilter()) {
            $qb
                ->leftJoin('d.amapiens', 'a')
                ->andWhere('a.user = :user')
                ->setParameter('user', $state->getUser())
            ;
        }

        $res = $qb->getQuery()->getResult();

        if (SearchDistributionAmapienState::FILTER_AVAILABLE === $state->getFilter()) {
            return array_filter($res, function (AmapDistribution $distribution) use ($state) {
                return !$distribution->isCompleted()
                && 0 === $distribution->getAmapiens()->filter(fn ($amapien) => $amapien->getUser() === $state->getUser())->count()
                && $distribution->getPlacesRestantes() > 0;
            });
        }

        return $res;
    }

    /**
     * @return AmapDistribution[]
     */
    public function getDistributionInDaysFromNow(int $days)
    {
        $this->refreshConnection();

        $targetDate = Carbon::now()->addDays($days)->startOfDay();

        return $this
            ->createQueryBuilder('d')
            ->where('d.date = :date')
            ->setParameter('date', $targetDate)
            ->leftJoin('d.amapiens', 'a')
            ->addSelect('a')
            ->leftJoin('d.amapLivraisonLieu', 'll')
            ->addSelect('ll')
            ->leftJoin('ll.amap', 'amap')
            ->addSelect('amap')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return AmapDistribution[]
     */
    public function getLastDistributionByAmap()
    {
        $this->refreshConnection();

        return $this
            ->createQueryBuilder('ad')
            ->leftJoin('ad.amapLivraisonLieu', 'll')
            ->where('ad.date >= (SELECT MAX(ad2.date) FROM PsrLib\ORM\Entity\AmapDistribution ad2 LEFT JOIN ad2.amapLivraisonLieu ll2 WHERE ll2.amap = ll.amap)')
            ->groupBy('ll.amap')
            ->orderBy('ad.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Amap[] $amaps
     *
     * @return array<int, int>
     */
    public function countDistributionWithAmapienByDateAmapMultiple($amaps, Carbon $start, Carbon $end)
    {
        $result = $this
            ->createQueryBuilder('d')
            ->select('count(d) as count, amap.id as id')
            ->leftJoin('d.amapLivraisonLieu', 'll')
            ->leftJoin('ll.amap', 'amap')
            ->where('amap IN (:amaps)')
            ->setParameter('amaps', $amaps)
            ->andWhere('d.date >= :start')
            ->setParameter('start', $start->clone()->startOfDay())
            ->andWhere('d.date <= :end')
            ->setParameter('end', $end->clone()->endOfDay())
            ->andWhere('d.amapiens IS NOT EMPTY')
            ->groupBy('amap.id')
            ->getQuery()
            ->getResult()
        ;

        return array_column($result, 'count', 'id');
    }

    private function buildSearchQb(SearchDistributionBaseState $distributionBaseState): ?QueryBuilder
    {
        $ll = $distributionBaseState->getLivLieu();
        $period = $distributionBaseState->getPeriod();
        if (null === $ll || null === $period) {
            return null;
        }

        return $this
            ->createQueryBuilder('d')
            ->where('d.amapLivraisonLieu = :ll')
            ->setParameter('ll', $ll)
            ->andWhere('d.date >= :start')
            ->setParameter('start', $period->getStartAt())
            ->andWhere('d.date <= :end')
            ->setParameter('end', $period->getEndAt())
        ;
    }
}
