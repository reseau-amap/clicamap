<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\DTO\SearchDocumentUtilisateurState;
use PsrLib\ORM\Entity\DocumentUtilisateur;
use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;

/**
 * @extends EntityRepository<DocumentUtilisateur>
 */
class DocumentUtilisateurRepository extends EntityRepository
{
    /**
     * @return DocumentUtilisateur[]
     */
    public function search(SearchDocumentUtilisateurState $searchDocumentUtilisateurState): array
    {
        if ($searchDocumentUtilisateurState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('du')
            ->select('du')
            ->leftJoin(DocumentUtilisateurAmap::class, 'dua', 'WITH', 'dua.id = du.id')
            ->leftJoin(DocumentUtilisateurFerme::class, 'duf', 'WITH', 'duf.id = du.id')
            ->leftJoin('dua.amap', 'a')
            ->leftJoin('duf.ferme', 'f')
            ->leftJoin('a.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'av')
            ->leftJoin('av.departement', 'avd')
            ->leftJoin('avd.region', 'avr')
            ->leftJoin('f.ville', 'fv')
            ->leftJoin('fv.departement', 'fvd')
            ->leftJoin('fvd.region', 'fvr')
            ->orderBy('du.nom', 'ASC')
        ;

        if (null !== $searchDocumentUtilisateurState->getRegion()) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->eq('avr', ':region'),
                        $qb->expr()->eq('fvr', ':region')
                    )
                )
                ->setParameter('region', $searchDocumentUtilisateurState->getRegion())
            ;
        }

        if (null !== $searchDocumentUtilisateurState->getDepartement()) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->eq('avd', ':departement'),
                        $qb->expr()->eq('fvd', ':departement')
                    )
                )
                ->setParameter('departement', $searchDocumentUtilisateurState->getDepartement())
            ;
        }

        if (null !== $searchDocumentUtilisateurState->getYear()) {
            $qb
                ->andWhere('du.annee = :year')
                ->setParameter('year', $searchDocumentUtilisateurState->getYear())
            ;
        }

        if (null !== $searchDocumentUtilisateurState->getKeyword()) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like('du.nom', ':keyword'),
                        $qb->expr()->like('a.nom', ':keyword'),
                        $qb->expr()->like('f.nom', ':keyword'),
                    )
                )
                ->setParameter('keyword', '%'.$searchDocumentUtilisateurState->getKeyword().'%')
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
