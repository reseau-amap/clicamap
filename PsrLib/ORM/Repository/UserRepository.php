<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use PsrLib\DTO\SearchUserState;
use PsrLib\DTO\UserRepositoryReseauAdminFromAmapDTO;
use PsrLib\Enum\SearchUserStatePermissionEnum;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Embeddable\Name;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\Ville;

/**
 * @extends EntityRepository<User>
 */
class UserRepository extends EntityRepository
{
    public function add(User $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findByUsernameOrEmail(string $usernameOrEmail): ?User
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.emails', 'e')
            ->where('u.username = :usernameOrEmail')
            ->orWhere('e.email = :usernameOrEmail')
            ->setParameter('usernameOrEmail', $usernameOrEmail)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return User[]
     */
    public function getAdminDepFromCP(string $cp)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adminDepartments', 'd')
            ->leftJoin(Ville::class, 'v', Join::WITH, 'v.departement = d')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return User[]
     */
    public function getAdminRegFromCP(string $cp)
    {
        return $this
            ->createQueryBuilder('a')
            ->leftJoin('a.adminRegions', 'r')
            ->leftJoin(Departement::class, 'd', Join::WITH, 'd.region = r')
            ->leftJoin(Ville::class, 'v', Join::WITH, 'v.departement = d')
            ->where('v.cp = :cp')
            ->setParameter('cp', $cp)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return string[]
     */
    public function getAllEmails()
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.emails', 'e')
            ->select('e.email')
            ->getQuery()
            ->getSingleColumnResult()
        ;
    }

    /**
     * @return User[]
     */
    public function searchByName(string $name)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->where('u.name.lastName LIKE :name')
            ->setParameter('name', '%'.$name.'%')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return User[]
     */
    public function getFermeRegroupemetAdmins(FermeRegroupement $fermeRegroupement)
    {
        return $this
            ->createQueryBuilder('u')
            ->where(':fermeRegroupement MEMBER OF u.adminFermeRegroupements')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getOneByEmail(string $email): ?User
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.emails', 'e')
            ->where('e.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getUserFromTokenMdpReinit(string $token): ?User
    {
        /** @var ?User $user */
        $user = $this
            ->createQueryBuilder('u')
            ->leftJoin('u.passwordResetToken', 't')
            ->addSelect('t')
            ->where('t.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if (null === $user) {
            return null;
        }

        if (!$user->getPasswordResetToken()->isValid()) {
            return null;
        }

        return $user;
    }

    /**
     * @return array<string, int>
     */
    public function getUsersNames(): array
    {
        $res = $this
            ->createQueryBuilder('u')
            ->select('u.id', 'u.name.firstName', 'u.name.lastName', 'u.username')
            ->indexBy('u', 'u.id')
            ->getQuery()
            ->getArrayResult()
        ;

        $res = array_map(function ($user) {
            $name = new Name();
            $name->setFirstName($user['name.firstName']);
            $name->setLastName($user['name.lastName']);

            return sprintf('%s (%s)', $name, $user['username']);
        }, $res);

        return array_flip($res);
    }

    /**
     * @return User[]
     */
    public function search(SearchUserState $searchUserState)
    {
        if ($searchUserState->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('u')
            ->leftJoin('u.emails', 'e')
            ->leftJoin('u.amaps', 'a')
            ->leftJoin('u.adminAmaps', 'aa')
            ->leftJoin('u.paysan', 'p')
            ->leftJoin('u.adminRegions', 'r')
            ->leftJoin('u.adminDepartments', 'd')
            ->leftJoin('u.ville', 'v')
            ->leftJoin('v.departement', 'dep')
            ->addSelect('a', 'aa', 'p', 'r', 'd', 'e', 'v', 'dep')
            ->orderBy('u.name.lastName', 'ASC')
        ;

        if (null !== $searchUserState->getRegion()) {
            $qb
                ->andWhere('dep.region = :region')
                ->setParameter('region', $searchUserState->getRegion())
            ;
        }

        if (null !== $searchUserState->getDepartement()) {
            $qb
                ->andWhere('v.departement = :departement')
                ->setParameter('departement', $searchUserState->getDepartement())
            ;
        }

        if (SearchUserStatePermissionEnum::BLANK !== $searchUserState->getSuperAdmin()) {
            $qb->andWhere('u.superAdmin = :superAdmin')
                ->setParameter('superAdmin', SearchUserStatePermissionEnum::Y === $searchUserState->getSuperAdmin())
            ;
        }

        if (SearchUserStatePermissionEnum::BLANK !== $searchUserState->getAdmin()) {
            if (SearchUserStatePermissionEnum::Y === $searchUserState->getAdmin()) {
                $qb
                    ->andWhere($qb->expr()->orX(
                        'u.adminRegions IS NOT EMPTY',
                        'u.adminDepartments IS NOT EMPTY',
                        'u.superAdmin = true'
                    ))
                ;
            } else {
                $qb
                    ->andWhere($qb->expr()->andX(
                        'u.adminRegions IS EMPTY',
                        'u.adminDepartments IS EMPTY',
                        'u.superAdmin = false'
                    ))
                ;
            }
        }

        if (SearchUserStatePermissionEnum::BLANK !== $searchUserState->getPaysan()) {
            if (SearchUserStatePermissionEnum::Y === $searchUserState->getPaysan()) {
                $qb->andWhere('p IS NOT NULL');
            } else {
                $qb->andWhere('p IS NULL');
            }
        }

        if (SearchUserStatePermissionEnum::BLANK !== $searchUserState->getAmap()) {
            if (SearchUserStatePermissionEnum::Y === $searchUserState->getAmap()) {
                $qb->andWhere('u.adminAmaps IS NOT EMPTY');
            } else {
                $qb->andWhere('u.adminAmaps IS EMPTY');
            }
        }

        if (SearchUserStatePermissionEnum::BLANK !== $searchUserState->getAmapien()) {
            if (SearchUserStatePermissionEnum::Y === $searchUserState->getAmapien()) {
                $qb->andWhere('u.amaps IS NOT EMPTY');
            } else {
                $qb->andWhere('u.amaps IS EMPTY');
            }
        }

        return $qb->getQuery()->getResult();
    }

    public function getReseauAdminFromAmap(Amap $amap): ?UserRepositoryReseauAdminFromAmapDTO
    {
        /** @var AmapLivraisonLieu|false $amapLL */
        $amapLL = $amap->getLivraisonLieux()->first();
        if (false === $amapLL) {
            return null;
        }
        $adminDep = $this
            ->createQueryBuilder('u')
            ->select('u.id AS Uid', 'd.id AS Did')
            ->leftJoin('u.adminDepartments', 'd')
            ->andWhere(':ville MEMBER OF d.villes')
            ->setParameter('ville', $amap->getLivraisonLieux()->first()->getVille())
            ->getQuery()
            ->getArrayResult()
        ;
        if (count($adminDep) > 0) {
            return new UserRepositoryReseauAdminFromAmapDTO(
                $this->_em->getReference(User::class, $adminDep[0]['Uid']),
                $this->_em->getReference(Departement::class, $adminDep[0]['Did'])
            );
        }

        $adminReg = $this
            ->createQueryBuilder('u')
            ->select('u.id AS Uid', 'r.id AS Rid')
            ->leftJoin('u.adminRegions', 'r')
            ->leftJoin('r.departements', 'd')
            ->andWhere(':ville MEMBER OF d.villes')
            ->setParameter('ville', $amap->getLivraisonLieux()->first()->getVille())
            ->getQuery()
            ->getResult()
        ;
        if (count($adminReg) > 0) {
            return new UserRepositoryReseauAdminFromAmapDTO(
                $this->_em->getReference(User::class, $adminReg[0]['Uid']),
                $this->_em->getReference(Region::class, $adminReg[0]['Rid'])
            );
        }

        return null;
    }
}
