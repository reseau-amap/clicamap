<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\DTO\SearchCampagneBulletinState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<CampagneBulletin>
 */
class CampagneBulletinRepository extends EntityRepository
{
    /**
     * @return CampagneBulletin[]
     */
    public function search(SearchCampagneBulletinState $state)
    {
        $qb = $this
            ->createQueryBuilder('cb')
            ->leftJoin('cb.campagne', 'c')
            ->where('c.amap = :amap')
            ->setParameter('amap', $state->getAmap())
        ;

        if (null !== $state->getCampagne()) {
            $qb->andWhere('cb.campagne = :campagne')
                ->setParameter('campagne', $state->getCampagne())
            ;
        }

        if (null !== $state->getAnnee()) {
            $qb->andWhere('c.anneeAdhesionAmapien = :annee')
                ->setParameter('annee', $state->getAnnee())
            ;
        }

        if (null !== $state->getKeyword()) {
            $qb->andWhere('c.nom LIKE :keyword')
                ->setParameter('keyword', '%'.$state->getKeyword().'%')
            ;
        }

        if ($state->isOnlyDraft()) {
            $qb->andWhere('cb.recus IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return CampagneBulletin[]
     */
    public function findByUser(User $user)
    {
        $qb = $this->createQueryBuilder('cb')
            ->join('cb.amapien', 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return CampagneBulletin[]
     */
    public function findPendingForAmap(Amap $amap)
    {
        $qb = $this->createQueryBuilder('cb')
            ->join('cb.campagne', 'c')
            ->where('c.amap = :amap')
            ->andWhere('cb.recus IS NULL')
            ->setParameter('amap', $amap)
        ;

        return $qb->getQuery()->getResult();
    }
}
