<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Assert\Assertion;
use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\User;

abstract class AdhesionRepository extends EntityRepository
{
    /**
     * @param User $creator
     *
     * @return array|false
     */
    public function getVoucherNumbers($creator)
    {
        // By default assert creator is User
        Assertion::isInstanceOf($creator, User::class);
        $res = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->select('value.voucherNumber as voucherNumber')
            ->where('aa.creator = :creator')
            ->setParameter('creator', $creator)
            ->getQuery()
            ->getScalarResult()
        ;

        return array_column($res, 'voucherNumber');
    }

    /**
     * @return int[]
     */
    public function getAllYears()
    {
        $res = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->select('value.year')
            ->where('value.year IS NOT NULL')
            ->distinct()
            ->orderBy('value.year', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        return array_column($res, 'year');
    }

    /**
     * @return Adhesion[]
     */
    public function getAdhesionsToGenerate(): array
    {
        return $this
            ->createQueryBuilder('aa')
            ->where('aa.state = :state')
            ->setParameter('state', Adhesion::STATE_GENERATING)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string[]|int[] $ids
     *
     * @return Adhesion[]
     */
    public function findByMultipleIds(array $ids, string $state = null)
    {
        $qb = $this
            ->createQueryBuilder('aa')
            ->where('aa.id in (:ids)')
            ->setParameter('ids', $ids)
        ;

        if (null !== $state) {
            $qb
                ->andWhere('aa.state = :state')
                ->setParameter('state', $state)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
