<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use PsrLib\DTO\SearchAdhesionAmapState;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\User;

class AdhesionAmapRepository extends AdhesionRepository
{
    /**
     * @return AdhesionAmap[]
     */
    public function search(
        SearchAdhesionAmapState $state,
        User $creator
    ) {
        if ($state->isEmpty()) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.creator', 'c')
            ->leftJoin('aa.value', 'value')
            ->leftJoin('aa.amap', 'amap')
            ->leftJoin('amap.livraisonLieux', 'll')
            ->leftJoin('ll.ville', 'v')
            ->leftJoin('v.departement', 'd')
            ->leftJoin('d.region', 'r')
        ;

        if (!$creator->isSuperAdmin()) {
            $qb
                ->andWhere('aa.creator = :creator')
                ->setParameter('creator', $creator)
            ;
        }

        if (true === $state->getAmapSupprimee()) {
            return $qb
                ->andWhere('aa.amap IS NULL')
                ->getQuery()
                ->getResult()
            ;
        }

        if (null !== $state->getRegion()) {
            $qb
                ->andWhere('r = :region')
                ->setParameter('region', $state->getRegion())
            ;
        }

        if (null !== $state->getDepartement()) {
            $qb
                ->andWhere('d = :department')
                ->setParameter('department', $state->getDepartement())
            ;
        }

        if (null !== $state->getAdhesion()) {
            $qb
                ->andWhere('value.year = :year')
                ->setParameter('year', $state->getAdhesion())
            ;
        }

        if (null !== $state->getEtudiante()) {
            $qb
                ->andWhere('amap.amapEtudiante = :student')
                ->setParameter('student', $state->getEtudiante())
            ;
        }

        if (null !== $state->getKeyword()) {
            $qb
                ->andWhere('amap.nom LIKE :keyWord')
                ->setParameter('keyWord', '%'.$state->getKeyword().'%')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return AdhesionAmap[]
     */
    public function findGeneratedByUser(User $user): array
    {
        return $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.amap', 'a')
            ->where(':user MEMBER OF a.admins')
            ->setParameter('user', $user)
            ->andWhere('aa.state = :state')
            ->setParameter('state', AdhesionAmap::STATE_GENERATED)
            ->getQuery()
            ->getResult()
        ;
    }
}
