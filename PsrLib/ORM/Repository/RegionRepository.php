<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;

/**
 * @extends EntityRepository<Region>
 */
class RegionRepository extends EntityRepository
{
    private array $cachedUserRegionMap = [];

    public function qbAllOrdered(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('r')
            ->orderBy('r.nom', 'ASC')
        ;
    }

    /**
     * @return Region[]
     */
    public function findAllOrdered(): array
    {
        return $this
            ->qbAllOrdered()
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Region[]
     */
    public function findWithLogo()
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.logo IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get accessible region for user. Memory cache result to avoid recurring database requests.
     *
     * @return Region[]
     */
    public function getRegionForUserCached(User $user)
    {
        $userId = $user->getId();
        if (isset($this->cachedUserRegionMap[$userId])) {
            return $this->cachedUserRegionMap[$userId];
        }
        if ($user->isSuperAdmin()) {
            $regions = $this->findAllOrdered();
        } elseif ($user->isAdminRegion()) {
            $regions = $user->getAdminRegions()->toArray();
        } elseif ($user->isAdminDepartment()) {
            $regions = $user->getAdminDepartments()->map(fn (Departement $departement) => $departement->getRegion())->toArray();
        } elseif ($user->isAmapAdmin()) {
            $regions = $this->findAllOrdered();
        } else {
            throw new \LogicException('Invalid user : '.$user->getUsername());
        }

        $this->cachedUserRegionMap[$userId] = $regions;

        return $regions;
    }

    public function qbRegionAsAdmin(User $user): QueryBuilder
    {
        if ($user->isSuperAdmin()) {
            return $this->qbAllOrdered();
        }

        return $this
            ->createQueryBuilder('r')
            ->where(':user MEMBER OF r.admins')
            ->setParameter('user', $user)
            ->orderBy('r.nom', 'ASC')
        ;
    }
}
