<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\User;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @extends EntityRepository<Contrat>
 */
class ContratRepository extends EntityRepository
{
    /**
     * @return Contrat[]
     */
    public function search(SearchContratSigneState $state): array
    {
        $mc = $state->getMc();
        if (null === $mc) {
            return [];
        }

        $qb = $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amapien', 'a')
            ->leftJoin('a.user', 'u')
            ->addSelect('a')
            ->where('c.modeleContrat = :mc')
            ->setParameter('mc', $mc)
            ->orderBy('u.name.lastName', 'ASC')
        ;

        if (null !== $state->getContratPaiementStatut()) {
            $qb
                ->andWhere('c.contratPaiementStatut = :paiementStatut')
                ->setParameter('paiementStatut', $state->getContratPaiementStatut())
            ;
        }

        if (null !== $state->getContratStatusWorkflowState()) {
            $qb
                ->andWhere('c.state = :workflowState')
                ->setParameter('workflowState', $state->getContratStatusWorkflowState())
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Contrat[]
     */
    public function getByUserAmapienActivated(User $user): array
    {
        return $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amapien', 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->andWhere('a.etat = :etat')
            ->setParameter('etat', ActivableUserInterface::ETAT_ACTIF)
            ->andWhere('c.state = :c_state')
            ->setParameter('c_state', ContratStatusWorkflow::STATE_VALIDATED)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Contrat[]
     */
    public function getWithDateMc(ModeleContrat $modeleContrat, ModeleContratDate $date): array
    {
        return $this
            ->createQueryBuilder('c')
            ->addSelect('a')
            ->leftJoin('c.amapien', 'a')
            ->leftJoin('c.cellules', 'cc')
            ->where('c.modeleContrat = :mc')
            ->setParameter('mc', $modeleContrat)
            ->andWhere('cc.modeleContratDate = :date')
            ->setParameter('date', $date)
            ->andWhere('cc.quantite > 0')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Contrat[]
     */
    public function getActiveWithFermeDateAmapien(Ferme $ferme, Amapien $amapien, \DateTime $date): array
    {
        return $this
            ->createQueryBuilder('c')
            ->leftJoin('c.modeleContrat', 'mc')
            ->leftJoin('mc.dates', 'd')
            ->where('c.amapien = :amapien')
            ->setParameter('amapien', $amapien)
            ->andWhere('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('d.dateLivraison = :date')
            ->setParameter('date', $date)
            ->andWhere('c.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_VALIDATED)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Contrat[]
     */
    public function getToValidateForUser(User $user): array
    {
        return $this
            ->createQueryBuilder('c')
            ->leftJoin('c.amapien', 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->andWhere('c.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE)
            ->getQuery()
            ->getResult()
        ;
    }
}
