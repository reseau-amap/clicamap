<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Assert\Assertion;
use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @extends EntityRepository<ContratLivraison>
 */
class ContratLivraisonRepository extends EntityRepository
{
    public function countTotalDeliveredByMcpNoRegul(Contrat $contrat, ModeleContratProduit $modeleContratProduit): float
    {
        Assertion::eq($modeleContratProduit->getModeleContrat(), $contrat->getModeleContrat());
        Assertion::false($modeleContratProduit->getRegulPds());

        /** @var ContratCellule[] $cellWithQuantities */
        $cellWithQuantities = $contrat
            ->getCellules()
            ->filter(fn (ContratCellule $cellule) => $cellule->getModeleContratProduit() === $modeleContratProduit
            && $cellule->getQuantite() > 0.0)
            ->toArray()
        ;

        /** @var \DateTime $cellDates */
        $cellDates = array_map(fn (ContratCellule $cellule) => $cellule->getModeleContratDate()->getDateLivraison()->format('Y-m-d'), $cellWithQuantities);

        /** @var ContratLivraison[] $livs */
        $livs = $this
            ->createQueryBuilder('c')
            ->where('c.livre = :livre')
            ->setParameter('livre', true)
            ->andWhere('c.amapien = :amapien')
            ->setParameter('amapien', $contrat->getAmapien())
            ->andWhere('c.ferme = :ferme')
            ->setParameter('ferme', $contrat->getModeleContrat()->getFerme())
            ->andWhere('c.date IN (:dates)')
            ->setParameter('dates', $cellDates)
            ->getQuery()
            ->getResult()
        ;

        $total = 0;
        foreach ($cellWithQuantities as $cel) {
            foreach ($livs as $liv) {
                if ($liv->getDate()->eq($cel->getModeleContratDate()->getDateLivraison())) {
                    $total += $cel->getQuantite();
                }
            }
        }

        return $total;
    }
}
