<?php

declare(strict_types=1);

namespace PsrLib\ORM\Repository;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeProduit;

/**
 * @extends ServiceEntityRepository<FermeProduit>
 */
class FermeProduitRepository extends ServiceEntityRepository
{
    /**
     * @return FermeProduit[]
     */
    public function getFromFermeOrdered(Ferme $ferme)
    {
        $fermeProduits = $this
            ->createQueryBuilder('fp')
            ->where('fp.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('fp.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;

        usort($fermeProduits, function (FermeProduit $p1, FermeProduit $p2) {
            $tpComparaison = strnatcasecmp($p1->getTypeProduction()->getNom(), $p2->getTypeProduction()->getNom());
            if (0 !== $tpComparaison) {
                return $tpComparaison;
            }

            $nameComparison = strnatcasecmp($p1->getNom(), $p2->getNom());
            if (0 !== $nameComparison) {
                return $nameComparison;
            }

            return strnatcasecmp($p1->getConditionnement(), $p2->getConditionnement());
        });

        return $fermeProduits;
    }
}
