<?php

declare(strict_types=1);

namespace PsrLib\ORM\Repository;

use PsrLib\ORM\Entity\EvenementWithRelatedEntity;

interface EvenementWithRelatedEntityRepositoryInterface
{
    /**
     * @return EvenementWithRelatedEntity[]
     */
    public function findByMultipleRelated(array $related): array;

    /**
     * @return EvenementWithRelatedEntity[]
     */
    public function findByMultipleRelatedWithoutDefaultLimited(array $related): array;
}
