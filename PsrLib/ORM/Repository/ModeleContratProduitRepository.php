<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @extends EntityRepository<ModeleContratProduit>
 */
class ModeleContratProduitRepository extends EntityRepository
{
    public function totalProductNotDeliveredFromFermeAmapDate(Ferme $ferme, Amap $amap, \DateTime $dateTime): array
    {
        return $this
            ->createQueryBuilder('p')
            ->leftJoin(ContratCellule::class, 'c', Join::WITH, 'c.modeleContratProduit = p')
            ->leftJoin('c.modeleContratDate', 'd')
            ->leftJoin('p.modeleContrat', 'mc')
            ->leftJoin('mc.livraisonLieu', 'll')
            ->leftJoin('c.contrat', 'ct')
            ->select('p as product, SUM(c.quantite) as qty')
            ->where('mc.ferme = :ferme')
            ->setParameter('ferme', $ferme)
            ->andWhere('d.dateLivraison = :date')
            ->setParameter('date', $dateTime)
            ->andWhere('ll.amap = :amap')
            ->setParameter('amap', $amap)
            ->andWhere('ct.amapien NOT IN (SELECT amapien FROM PsrLib\ORM\Entity\Amapien amapien JOIN PsrLib\ORM\Entity\ContratLivraison cl WITH cl.amapien = amapien WHERE cl.ferme = :ferme AND cl.date = :date)')
            ->groupBy('p')
            ->getQuery()
            ->getResult()
        ;
    }

    public function totalByProductFromMcDateContractValidated(ModeleContrat $modeleContrat, \DateTime $dateTime): array
    {
        return $this
            ->createQueryBuilder('p')
            ->leftJoin(ContratCellule::class, 'c', Join::WITH, 'c.modeleContratProduit = p')
            ->leftJoin('c.contrat', 'ct')
            ->leftJoin('c.modeleContratDate', 'd')
            ->select('p as product, SUM(c.quantite) as qty')
            ->where('d.modeleContrat = :mc')
            ->andWhere('p.modeleContrat = :mc')
            ->setParameter('mc', $modeleContrat)
            ->andWhere('d.dateLivraison = :date')
            ->setParameter('date', $dateTime)
            ->andWhere('ct.state = :state')
            ->setParameter('state', ContratStatusWorkflow::STATE_VALIDATED)
            ->groupBy('p')
            ->getQuery()
            ->getResult()
        ;
    }
}
