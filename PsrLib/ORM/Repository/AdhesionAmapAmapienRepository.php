<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\ORM\Repository;

use Assert\Assertion;
use Doctrine\ORM\EntityRepository;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;

class AdhesionAmapAmapienRepository extends EntityRepository
{
    /**
     * @return AdhesionAmapAmapien[]
     */
    public function search(?int $year, ?string $keyword, Amap $creator)
    {
        $qb = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.amapien', 'amapien')
            ->leftJoin('amapien.user', 'user')
            ->leftJoin('aa.value', 'value')
            ->where('aa.creator = :creator')
            ->setParameter('creator', $creator)
        ;

        if (null !== $year && $year > 0) {
            $qb
                ->andWhere('value.year = :year')
                ->setParameter('year', $year)
            ;
        }

        if ('' !== $keyword) {
            $qb
                ->andWhere('user.name.lastName LIKE :keyWord')
                ->setParameter('keyWord', '%'.$keyword.'%')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Amap $creator
     */
    public function getVoucherNumbers($creator): array|false
    {
        // Override condition as creator is AMAP
        Assertion::isInstanceOf($creator, Amap::class);
        $res = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->select('value.voucherNumber as voucherNumber')
            ->where('aa.creator = :creator')
            ->setParameter('creator', $creator)
            ->getQuery()
            ->getScalarResult()
        ;

        return array_column($res, 'voucherNumber');
    }

    /**
     * @param Amap[] $amaps
     *
     * @return array<int, int>
     */
    public function countAdhesionsByAmapMultiple($amaps)
    {
        $res = $this
            ->createQueryBuilder('a')
            ->select('COUNT(a) as count, amap.id as id')
            ->leftJoin('a.creator', 'amap')
            ->where('amap IN (:amaps)')
            ->setParameter('amaps', $amaps)
            ->groupBy('amap.id')
            ->getQuery()
            ->getResult()
        ;

        return array_column($res, 'count', 'id');
    }

    /**
     * @return AdhesionAmapAmapien[]
     */
    public function findGeneratedByUser(User $user): array
    {
        return $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.amapien', 'amapien')
            ->where('amapien.user = :user')
            ->setParameter('user', $user)
            ->andWhere('aa.state = :state')
            ->setParameter('state', AdhesionAmapAmapien::STATE_GENERATED)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return int[]
     */
    public function getAllYears()
    {
        $res = $this
            ->createQueryBuilder('aa')
            ->leftJoin('aa.value', 'value')
            ->select('value.year')
            ->where('value.year IS NOT NULL')
            ->distinct()
            ->orderBy('value.year', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        return array_column($res, 'year');
    }

    /**
     * @param string[] $ids
     *
     * @return AdhesionAmapAmapien[]
     */
    public function findByMultipleIds(array $ids, string $state = null)
    {
        $qb = $this
            ->createQueryBuilder('aa')
            ->where('aa.id in (:ids)')
            ->setParameter('ids', $ids)
        ;

        if (null !== $state) {
            $qb
                ->andWhere('aa.state = :state')
                ->setParameter('state', $state)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
