<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\ModeleContrat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ModelContratForm6Type extends AbstractType
{
    public function __construct(private readonly Translator $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModeleContrat $data */
        $data = $options['data'];
        Assertion::isInstanceOf($data, ModeleContrat::class);
        if ($data->isCdp()) {
            $builder
                ->add('reglementType', ChoiceType::class, [
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => [ModeleContrat::mode_carte],
                    'data' => [ModeleContrat::mode_carte],
                    'disabled' => true,
                    'choice_label' => fn (string $choice) => $this->translator->trans($choice, [], 'modele_contrat_reglement_type'),
                ])
                ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                    /** @var ModeleContrat $data */
                    $data = $event->getData();
                    $data->setReglementType([ModeleContrat::mode_carte]);
                })
            ;
        } else {
            $builder
                ->add('reglementType', ChoiceType::class, [
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => ModeleContrat::mode_available,
                    'choice_label' => fn (string $choice) => $this->translator->trans($choice, [], 'modele_contrat_reglement_type'),
                ])
            ;
        }

        $builder
            ->add('reglementNbMax', NumberType::class, [
                'html5' => true,
                'required' => true,
            ])
            ->add('dateReglements', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ModeleContratDateReglementType::class,
                'by_reference' => false,
                'error_bubbling' => false,
            ])
            ->add('reglementModalite', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'L\'information inscrite dans ce champ est reprise dans le contrat juridique.',
                    'rows' => 6,
                ],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'data_class' => ModeleContrat::class,
            'validation_groups' => ['form6'],
        ]);
    }
}
