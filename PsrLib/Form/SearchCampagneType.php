<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\DTO\SearchCampagneState;
use PsrLib\ORM\Entity\Amap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchCampagneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = $options['amap_choices'];
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $choices,
                'choice_label' => fn (?Amap $amap) => $amap?->getNom(),
                'choice_value' => fn (?Amap $amap) => $amap?->getId(),
                'placeholder' => '',
                'required' => false,
                'label' => 'AMAP',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap_choices' => [],
            'method' => 'GET',
            'data_class' => SearchCampagneState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'empty_data' => fn (FormInterface $form) => new SearchCampagneState(),
        ]);
    }
}
