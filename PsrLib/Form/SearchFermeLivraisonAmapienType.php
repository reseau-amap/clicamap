<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\SearchFermeLivraisonAmapienState;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapienRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFermeLivraisonAmapienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?SearchFermeLivraisonAmapienState $state */
        $state = $options['data'];
        Assertion::notNull($state);

        $builder
            ->add('amapien', EntityType::class, [
                'class' => Amapien::class,
                'query_builder' => fn (AmapienRepository $amapienRepository) => $amapienRepository->qbActiveAmapienWithContratValidatedDate($state->getAmap(), $state->getFerme(), $state->getDate()),
                'choice_value' => fn (?Amapien $amapien) => null === $amapien ? '' : $amapien->getId(),
                'choice_label' => fn (?Amapien $amapien) => null === $amapien ? '' : $amapien->getUser(),
                'attr' => ['class' => 'js-select2'],
                'required' => false,
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchFermeLivraisonAmapienState::class,
        ]);
    }
}
