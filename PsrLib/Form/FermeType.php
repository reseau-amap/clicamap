<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\CarbonDateType;
use PsrLib\Form\Type\FermeAnneeAdhesionType;
use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRegroupementRepository;
use PsrLib\Services\AuthentificationMockable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FermeType extends AbstractType
{
    public function __construct(private readonly AuthentificationMockable $authentification)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Ferme $ferme */
        $ferme = $options['data'];
        Assertion::isInstanceOf($ferme, Ferme::class);

        /** @var User $currentUser */
        $currentUser = $this->authentification->getCurrentUser();
        $siretDisabled = $currentUser->isPaysan() && $ferme->inRegroupement();

        $builder
            ->add('nom', TextType::class, [
                'required' => false,
                'label' => 'Nom de la ferme :',
            ])
            ->add('ordreCheque', TextType::class, [
                'required' => false,
                'label' => 'Chèque à l\'ordre de (raison sociale) :',
            ])
            ->add('siret', NumberType::class, [
                'required' => false,
                'disabled' => $siretDisabled,
                'attr' => $siretDisabled ? ['readonly' => true] : [],
                'label' => 'Numéro de SIRET :',
                'html5' => true,
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'Description :',
            ])
            ->add('libAdr', TextType::class, [
                'required' => false,
                'label' => 'N°, voie :',
            ])
            ->add('ville', VilleType::class, [
                'attr' => ['readonly' => true],
                'label' => 'Code postal, Ville :',
            ])
            ->add('gpsLatitude', TextType::class, [
                'attr' => ['readonly' => true],
                'label' => 'GPS Latitude :',
            ])
            ->add('gpsLongitude', TextType::class, [
                'attr' => ['readonly' => true],
                'label' => 'GPS Longitude :',
            ])
            ->add('tva', YesNoChoiceType::class, [
                'label' => 'Assujetti à la TVA :',
            ])
            ->add('tvaTaux', TextType::class, [
                'required' => false,
                'label' => 'Taux de TVA applicable :',
            ])
            ->add('tvaDate', CarbonDateType::class, [
                'required' => false,
                'label' => 'Date du passage de non assujetti à assujetti :',
            ])
            ->add('surface', NumberType::class, [
                'required' => false,
                'label' => 'Surface de la ferme (ha) :',
                'html5' => true,
                'attr' => ['step' => .01, 'min' => 0],
            ])
            ->add('uta', NumberType::class, [
                'required' => false,
                'label' => 'Nombre d\'UTA :',
                'html5' => true,
                'attr' => ['step' => .01, 'min' => 0],
            ])
            ->add('certification', ChoiceType::class, [
                'choices' => Ferme::AVAILABLE_CERTIFICATIONS,
                'choice_label' => fn (?string $choice) => array_search($choice, Ferme::AVAILABLE_CERTIFICATIONS, true),
                'label' => 'Certification :',
            ])
            ->add('url', TextType::class, [
                'required' => false,
                'label' => 'Site internet :',
            ])
            ->add('dateInstallation', CarbonDateType::class, [
                'required' => false,
                'label' => 'Date d\'installation',
            ])
            ->add('anneeDebCommercialisationAmap', TextType::class, [
                'required' => false,
                'label' => 'Année de début de commercialisation en AMAP :',
            ])
            ->add('msa', TextType::class, [
                'required' => false,
                'label' => 'Numéro MSA :',
            ])
            ->add('spg', TextType::class, [
                'required' => false,
                'label' => 'Visite de partenariat :',
            ])
        ;

        $permissionEditAdhesions = $options['permission_edit_adhesions'];
        $builder
            ->add('anneeAdhesions', FermeAnneeAdhesionType::class, [
                'ferme' => $options['data'],
                'required' => false,
                'disabled' => !$permissionEditAdhesions,
                'attr' => ['readonly' => !$permissionEditAdhesions],
            ])
        ;

        /** @var User $currentUser */
        $currentUser = $options['current_user'];
        Assertion::isInstanceOf($currentUser, User::class);
        if ($currentUser->isSuperAdmin() || $currentUser->isAdminRegion() || $currentUser->isFermeRegroupementAdmin()) {
            $builder
                ->add('regroupement', EntityType::class, [
                    'query_builder' => fn (FermeRegroupementRepository $repo) => $currentUser->isFermeRegroupementAdmin() ? $repo->qbByAdmin($currentUser) : $repo->qbAll(),
                    'label' => 'Regroupement',
                    'class' => FermeRegroupement::class,
                    'required' => $currentUser->isFermeRegroupementAdmin(),
                    'placeholder' => $currentUser->isFermeRegroupementAdmin() ? false : '',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ferme::class,
            'permission_edit_adhesions' => false,
            'current_user' => null,
        ]);
    }
}
