<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\ModeleContratDeplacement;
use PsrLib\Form\Type\CarbonDateType;
use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleContratDeplacementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModeleContratDeplacement $mcd */
        $mcd = $options['data'];
        Assertion::isInstanceOf($mcd, ModeleContratDeplacement::class);

        $builder
            ->add('src', EntityType::class, [
                'choices' => $mcd->getMc()->getFutureDates(),
                'choice_label' => function (?ModeleContratDate $modeleContratDate) {
                    if (null === $modeleContratDate) {
                        return '';
                    }

                    return $modeleContratDate->getDateLivraison()->format('Y-m-d');
                },
                'class' => ModeleContratDate::class,
            ])
            ->add('dst', CarbonDateType::class, [
                'attr' => [
                    'placeholder' => 'Cliquer pour choisir une nouvelle date...',
                    'class' => 'datepicker',
                    'data-date-format' => 'yyyy-mm-dd',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModeleContratDeplacement::class,
        ]);
    }
}
