<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchContratSignePaysanType extends AbstractType
{
    public function __construct(private readonly ModeleContratRepository $modeleContratRepo, private readonly AmapRepository $amapRepo)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $filterArchived = $options['filter_archived_state'];

        /** @var User $currentUser */
        $currentUser = $options['currentUser'];
        Assertion::isInstanceOf($currentUser, User::class);

        $fermes = $currentUser->getFermesAsRegroupementOrPaysan();
        if ($options['filter_active_fermes']) {
            $fermes = array_filter($fermes, fn (Ferme $ferme) => $ferme->isAdhesionActive());
        }
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $this->amapRepo->findByFermes($fermes),
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Amap $amap) => null === $amap ? '' : $amap->getNom(),
                'choice_value' => fn (?Amap $amap) => null === $amap ? '' : $amap->getId(),
            ])
            ->add('mc', ChoiceType::class, [
                'choices' => [],
                'disabled' => true,
            ])
        ;

        $builder
            ->get('amap')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($fermes, $filterArchived): void {
                $form = $event->getForm();
                $amap = $form->getParent()->get('amap')->getData();

                if (null === $amap) {
                    return;
                }

                $choices = $this->modeleContratRepo->findValidatedByFermesAmap($fermes, $amap);
                if (null !== $filterArchived) {
                    $choices = array_filter($choices, fn (ModeleContrat $mc) => $mc->isArchived() === $filterArchived);
                }

                $form
                    ->getParent()
                    ->add('mc', ChoiceType::class, [
                        'choices' => $choices,
                        'choice_label' => fn (?ModeleContrat $modeleContrat) => null === $modeleContrat ? null : $modeleContrat->getNom(),
                        'choice_value' => fn (?ModeleContrat $modeleContrat) => null === $modeleContrat ? null : $modeleContrat->getId(),
                        'placeholder' => '',
                        'required' => false,
                    ])
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'currentUser' => null,
            'method' => 'GET',
            'data_class' => SearchContratSigneState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch,
            'filter_archived_state' => null,
            'filter_active_fermes' => false,
        ]);
    }
}
