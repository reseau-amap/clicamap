<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\SearchFermeLivraisonAmapState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Repository\AmapRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFermeLivraisonAmapType extends AbstractType
{
    public function __construct(private readonly AmapRepository $amapRepo)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?SearchFermeLivraisonAmapState $data */
        $data = $options['data'];
        Assertion::notNull($data);

        $amaps = $this
            ->amapRepo
            ->getAmapsWithModeleContractValidated($data->getFerme(), $data->getDate())
        ;

        $builder
            ->add('amap', ChoiceType::class, [
                'label' => 'AMAP',
                'choices' => $amaps,
                'empty_data' => 1 === count($amaps) ? (string) $amaps[0]->getId() : null,
                'required' => false,
                'placeholder' => count($amaps) > 1 ? '' : false,
                'choice_label' => function (?Amap $amap) {
                    if (null === $amap) {
                        return null;
                    }

                    return $amap->getNom();
                },
                'choice_value' => function (?Amap $amap) {
                    if (null === $amap) {
                        return null;
                    }

                    return $amap->getId();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchFermeLivraisonAmapState::class,
            'attr' => ['class' => 'js-form-search'],
            'method' => 'GET',
        ]);
    }
}
