<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\DTO\SearchAmapienAmapState;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\ORM\Entity\Amap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchAmapienAmapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = $options['amaps'];
        $isMultipleChoice = count($choices) > 1;
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $choices,
                'choice_label' => fn (Amap $amap) => $amap->getNom(),
                'placeholder' => $isMultipleChoice ? '' : false,
                'disabled' => !$isMultipleChoice,
                'data' => $isMultipleChoice ? null : $choices[0],
                'label' => 'AMAP',
            ])
            ->add('keyword', TextType::class, [
                'required' => false,
                'label' => 'Mot clé',
            ])
            ->add('adhesion', AnneeAdhesionGenericType::class, [
                'label' => 'Années d\'adhésion',
            ])
            ->add('filter', ChoiceType::class, [
                'choices' => [
                    'Voir tous les comptes' => null,
                    'Voir uniquement les comptes activés' => true,
                    'Voir uniquement les comptes désactivés' => false,
                ],
                'label' => 'Filtre',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'currentUser' => null,
            'method' => 'GET',
            'data_class' => SearchAmapienAmapState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'amaps' => [],
            'empty_data' => function (FormInterface $form) {
                $state = new SearchAmapienAmapState();
                $state->setAmap($form->get('amap')->getData());

                return $state;
            },
        ]);
    }
}
