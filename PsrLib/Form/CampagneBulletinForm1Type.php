<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Entity\CampagneBulletin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneBulletinForm1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('amapien', CampagneBulletinForm1AmapienType::class, [
                'label' => false,
                'validation_groups' => ['Default'],
            ])
            ->add('montants', CollectionType::class, [
                'entry_type' => CampagneBulletinForm1MontantType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'entry_options' => ['label' => false],
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CampagneBulletin::class,
            'validation_groups' => ['form1'],
        ]);
    }
}
