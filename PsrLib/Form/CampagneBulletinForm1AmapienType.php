<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amapien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneBulletinForm1AmapienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('user', CampagneBulletinForm1UserType::class, [
            'label' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Amapien::class,
        ]);
    }
}
