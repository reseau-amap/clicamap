<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserUsernameOrEmailSelectorType extends AbstractType implements DataTransformerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer($this)
        ;
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'label' => false,
        ]);
    }

    /**
     * @param ?User $user
     *
     * @return ?string
     */
    public function transform($user)
    {
        if (null === $user) {
            return '';
        }

        return $user->getUsername();
    }

    /**
     * @param ?string $username
     *
     * @return User|null
     */
    public function reverseTransform($username)
    {
        if (null === $username) {
            return null;
        }
        // transform the array to a string
        return $this->userRepository->findByUsernameOrEmail($username);
    }
}
