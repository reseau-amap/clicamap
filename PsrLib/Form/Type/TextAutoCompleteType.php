<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextAutoCompleteType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'data-autocomplete-url' => null,
            ],
            'row_attr' => [
                'class' => 'ui-front',
            ],
        ]);
    }
}
