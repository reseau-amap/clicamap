<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CheckSwitchType extends AbstractType
{
    public function getParent()
    {
        return CheckboxType::class;
    }
}
