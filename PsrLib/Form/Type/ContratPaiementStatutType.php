<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use PsrLib\Enum\ContratPaiementStatut;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ContratPaiementStatutType extends AbstractType
{
    public function __construct(private readonly Translator $translator)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => ContratPaiementStatut::class,
            'choice_label' => fn (ContratPaiementStatut $label) => $this->translator->trans($label->value, [], 'contrat_paiement_statut'),
            'required' => false,
        ]);
    }

    public function getParent()
    {
        return EnumType::class;
    }
}
