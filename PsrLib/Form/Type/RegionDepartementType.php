<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegionDepartementType extends AbstractType
{
    public function __construct(private readonly RegionRepository $regionRepo, private readonly DepartementRepository $departmentRepo)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User|null $currentUser */
        $currentUser = $options['currentUser'];
        if ($currentUser->isSuperAdmin()) {
            $regions = $this->regionRepo->findAllOrdered();
        } elseif ($currentUser->isAdminRegion()) {
            $regions = $currentUser->getAdminRegions()->toArray();
        } elseif ($currentUser->isAdminDepartment()) {
            $regions = $currentUser->getAdminDepartments()->map(fn (Departement $departement) => $departement->getRegion())->toArray();
        } else {
            throw new \LogicException('Invalid current user : '.$currentUser->getId());
        }
        $builder
            ->add('region', ChoiceType::class, [
                'choices' => $regions,
                'disabled' => 1 === count($regions),
                'data' => 1 === count($regions) ? $regions[0] : null,
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Region $region) => null === $region ? '' : $region->getNom(),
                'choice_value' => fn (?Region $region) => null === $region ? '' : $region->getId(),
            ])
        ;

        $builder
            ->get('region')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($currentUser): void {
                /** @var Region|null $region */
                $region = $event->getData();

                $this->addDepartmentField($event->getForm()->getParent(), $region, $currentUser);
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($currentUser): void {
                /** @var Region|null $region */
                $region = $event->getData();

                $this->addDepartmentField($event->getForm()->getParent(), $region, $currentUser);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'currentUser' => null,
        ]);
    }

    private function addDepartmentField(FormInterface $form, ?Region $region, User $currentUser): void
    {
        if ($currentUser->isAdminDepartment()) {
            $departements = $currentUser->getAdminDepartments()->toArray();
        } else {
            $departements = null === $region ? [] : $this->departmentRepo->findBy(['region' => $region]);
        }

        $form
            ->add('departement', ChoiceType::class, [
                'choices' => $departements,
                'disabled' => 1 === count($departements),
                'data' => 1 === count($departements) ? $departements[0] : null,
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Departement $departement) => null === $departement ? '' : $departement->getNom(),
                'choice_value' => fn (?Departement $departement) => null === $departement ? '' : $departement->getId(),
            ])
        ;
    }
}
