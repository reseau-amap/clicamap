<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\Services\TvaLabel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrixTVAType extends AbstractType
{
    public function __construct(private readonly TvaLabel $tvaLabel)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $vatChoices = [null];
        if (true === $options['with_vat']) {
            $vatChoices = PrixTVA::TVA_CHOICES;
        }

        $builder
            ->add('prixTTC', MoneyCustomType::class, [
                'required' => false,
                'label' => 'Prix',
            ])
            ->add('tva', ChoiceType::class, [
                'label' => 'TVA',
                'choices' => $vatChoices,
                'choice_label' => fn (?float $choice) => $this->tvaLabel->getLabel($choice),
                'choice_value' => fn (?float $choice) => $choice,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PrixTVA::class,
            'with_vat' => false,
        ]);
    }
}
