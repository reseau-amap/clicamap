<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use PsrLib\Services\MoneyHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoneyCustomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                function ($moneyAsObject) {
                    if (null === $moneyAsObject) {
                        return null;
                    }

                    return MoneyHelper::toStringDecimal($moneyAsObject);
                },
                function ($moneyAsString) {
                    if ('' === $moneyAsString || null === $moneyAsString) {
                        return null;
                    }

                    return MoneyHelper::fromString($moneyAsString);
                }
            ))
        ;
    }

    public function getParent(): string
    {
        return NumberType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'html5' => true,
            'input' => 'string',
            'scale' => 2,
            'data_class' => null,
            'attr' => ['step' => '0.01'],
        ]);
    }
}
