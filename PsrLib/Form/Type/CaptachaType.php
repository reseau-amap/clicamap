<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaptachaType extends AbstractType
{
    public function __construct(private readonly \Captcha $captcha)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                $data = $event->getData();

                if (!$this->captcha->verifCaptcha($data)) {
                    $event->getForm()->addError(new FormError('Le code de vérification est incorrect'));
                }
            })
        ;
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'Code de vérification',
        ]);
    }
}
