<?php

declare(strict_types=1);

namespace PsrLib\Form\Type;

use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startAt', CarbonDateType::class, [
                'label' => $options['label_start'],
                'required' => false,
            ])
            ->add('endAt', CarbonDateType::class, [
                'label' => $options['label_end'],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Period::class,
            'label_start' => 'Période - date de début',
            'label_end' => 'Période - date de fin',
        ]);
    }
}
