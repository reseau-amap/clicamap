<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Carbon\Carbon;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleContratCopyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mc', EntityType::class, [
                'class' => ModeleContrat::class,
                'query_builder' => function (ModeleContratRepository $modeleContratRepository) use ($options) {
                    return $modeleContratRepository->qbV2ByAmapFermeOrderByForclusion(
                        $options['amap'],
                        $options['ferme']
                    );
                },
                'choice_label' => fn (ModeleContrat $mc) => sprintf(
                    '%s (fin de souscription : %s)',
                    $mc->getNom(),
                    null === $mc->getForclusion() ? 'indéfinie' : Carbon::instance($mc->getForclusion())->format('d/m/Y')
                ),
                'attr' => ['class' => 'js-select2'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap' => null,
            'ferme' => null,
        ]);
    }
}
