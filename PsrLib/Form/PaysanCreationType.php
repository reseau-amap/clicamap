<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Repository\FermeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class PaysanCreationType extends AbstractType
{
    public function __construct(
        private readonly FermeRepository $fermeRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currentUser = $options['current_user'];
        Assertion::notNull($currentUser);

        $builder
            ->add('ferme', EntityType::class, [
                'class' => Ferme::class,
                'query_builder' => fn (FermeRepository $fermeRepository) => $fermeRepository->qbAccessibleForUser($currentUser),
                'mapped' => false,
                'constraints' => [new NotNull(['message' => 'Merci de sélectionner une ferme'])],
                'placeholder' => '',
                'required' => false,
                'attr' => ['class' => 'js-select2'],
            ])
        ;

        $builder
            ->get('ferme')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (PreSetDataEvent $event) use ($options): void {
                if (null !== $options['default_ferme_id']) {
                    $ferme = $this->fermeRepository->find($options['default_ferme_id']);
                    if (null !== $ferme) {
                        $event->setData(
                            $ferme
                        );
                    }
                }
            })
        ;
    }

    public function getParent()
    {
        return PaysanEditType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'current_user' => null,
            'data_class' => Paysan::class,
            'default_ferme_id' => null,
        ]);
    }
}
