<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\DTO\SearchRegionDepartementInterface;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchBaseRegionDepartementType extends AbstractType
{
    public function __construct(protected readonly RegionRepository $regionRepo, protected readonly DepartementRepository $departmentRepo)
    {
    }

    public function getRegionFromRawIdForUser(?string $regionId, User $user): ?Region
    {
        $userRegions = $this->regionRepo->getRegionForUserCached($user);
        foreach ($userRegions as $userRegion) {
            if ($userRegion->getId() === (int) $regionId) {
                return $userRegion;
            }
        }

        return null;
    }

    public function getRegionDepartementFromPreSubmitData(FormInterface $form, ?array $eventData, User $user): array
    {
        $regionId = $eventData['region'] ?? null;
        if (null === $regionId) { // no submitted data. Keep PRE_SET_DATA field
            $region = $form->get('region')->getData();
            if (null === $region) {
                return [null, null];
            }
        } else {
            // Check region accesible for user as we listen before SUBMIT check
            $region = $this
                ->getRegionFromRawIdForUser($regionId, $user)
            ;
        }

        if (null === $region) {
            return [null, null];
        }

        $departementId = $eventData['departement'] ?? null;
        if (null === $departementId) {
            $departement = $form->get('departement')->getData();
            if (null !== $departement) {
                return [$region, $departement];
            }
        }
        $departement = null;
        $userDepartements = $this
            ->departmentRepo
            ->getDepartementsForUserCached($user, $region)
        ;
        foreach ($userDepartements as $userDepartement) {
            if ($userDepartement->getId() === $departementId) {
                $departement = $userDepartement;
            }
        }

        return [$region, $departement];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User|null $currentUser */
        $currentUser = $options['currentUser'];
        if (null === $currentUser) {
            throw new \LogicException('currentUser must be set');
        }

        $restrictToUser = true === $options['restrict_to_user'];
        if ($restrictToUser) {
            $regions = $this->regionRepo->getRegionForUserCached($currentUser);
        } else {
            $regions = $this->regionRepo->findAllOrdered();
        }

        $builder
            ->add('region', ChoiceType::class, [
                'label' => 'Région',
                'choices' => $regions,
                'disabled' => 1 === count($regions),
                'data' => 1 === count($regions) ? $regions[0] : null,
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Region $region) => null === $region ? '' : $region->getNom(),
                'choice_value' => fn (?Region $region) => null === $region ? '' : $region->getId(),
            ])
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($currentUser, $restrictToUser): void {
                $form = $event->getForm();

                /** @var ?SearchRegionDepartementInterface $data */
                $data = $event->getData();
                if (null === $data || null === $data->getRegion()) {
                    $region = $form->get('region')->getData();
                } else {
                    $region = $data->getRegion();
                }

                $this
                    ->addDepartementFromRegion($form, $region, $currentUser, $restrictToUser)
                ;
            })
        ;

        // Bind on pre submit to allow multiple dependant fields.
        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($currentUser, $restrictToUser): void {
                $form = $event->getForm();
                $data = $event->getData();

                $regionId = $data['region'] ?? null;
                if (null === $regionId) { // no submitted data. Keep PRE_SET_DATA field
                    return;
                }

                // Check region accesible for user as we listen before SUBMIT check
                $region = $this->getRegionFromRawIdForUser((string) $regionId, $currentUser);

                $this->addDepartementFromRegion($form, $region, $currentUser, $restrictToUser);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchRegionDepartementInterface::class,
            'currentUser' => null,
            'method' => 'GET',
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'restrict_to_user' => true,
        ]);
    }

    private function addDepartementFromRegion(FormInterface $form, ?Region $region, User $currentUser, bool $restrictToUser): void
    {
        if (null === $region) {
            $form
                ->add('departement', ChoiceType::class, [
                    'label' => 'Département',
                    'disabled' => true,
                ])
            ;

            return;
        }

        if ($restrictToUser) {
            $departements = $this->departmentRepo->getDepartementsForUserCached($currentUser, $region);
        } else {
            $departements = $region->getDepartements()->toArray();
        }

        $form
            ->add('departement', ChoiceType::class, [
                'label' => 'Département',
                'choices' => $departements,
                'disabled' => 1 === count($departements),
                'data' => 1 === count($departements) ? $departements[0] : null,
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Departement $departement) => null === $departement ? '' : $departement->getNom(),
                'choice_value' => fn (?Departement $departement) => null === $departement ? '' : $departement->getId(),
            ])
        ;
    }
}
