<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Repository\FermeProduitRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModelContratForm3Type extends AbstractType
{
    public function __construct(
        private readonly FermeProduitRepository $fermeProduitRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $contrat = $options['data'];
        Assertion::isInstanceOf($contrat, ModeleContrat::class);

        $fermeProduits = $this->fermeProduitRepository->getFromFermeOrdered($contrat->getFerme());
        if (0 === count($fermeProduits)) {
            return;
        }

        $emptyMcp = ModeleContratProduit::createFromFermeProduit($fermeProduits[0]);
        $builder
            ->add('produits', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ModelContratForm3ProduitType::class,
                'by_reference' => false,
                'entry_options' => [
                    'ferme' => $contrat->getFerme(),
                ],
                'prototype_data' => $emptyMcp,
                'label' => false,
            ])
            ->add('empty', HiddenType::class, [ // Force validation trigger on empty request
                'mapped' => false,
                'data' => 'empty',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModeleContrat::class,
        ]);
    }
}
