<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\DocumentUtilisateurFormState;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\Services\AdhesionRangeBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class DocumentUtilisateurType extends AbstractType
{
    public function __construct(
        private readonly AdhesionRangeBuilder $adhesionRangeBuilder,
        private readonly AmapRepository $amapRepository,
        private readonly FermeRepository $fermeRepository,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currentUser = $options['current_user'];
        Assertion::isInstanceOf($currentUser, User::class);

        $nextYear = Carbon::now()->addYear();

        $choices = array_merge(
            $this->amapRepository->qbAmapAccessibleForAmapien($currentUser)->getQuery()->getResult(),
            $this->fermeRepository->qbAccessibleForUser($currentUser)->getQuery()->getResult()
        );

        $builder
            ->add('annee', AnneeAdhesionGenericType::class, [
                'label' => 'Année de référence',
                'placeholder' => '',
                'required' => false,
                'choices' => array_combine($this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear), $this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear)),
            ])
            ->add('nom', TextType::class, [
                'label' => 'Nom du document',
                'required' => false,
            ])
            ->add('target', ChoiceType::class, [
                'label' => 'AMAP ou Ferme',
                'choices' => $choices,
                'choice_label' => function (Amap|Ferme $choice = null) {
                    if (null === $choice) {
                        return '';
                    }
                    if ($choice instanceof Amap) {
                        return 'AMAP : '.$choice->getNom();
                    }

                    return 'Ferme : '.$choice->getNom();
                },
                'choice_value' => function (Amap|Ferme $choice = null) {
                    if (null === $choice) {
                        return '';
                    }

                    if ($choice instanceof Amap) {
                        return 'amap_'.$choice->getId();
                    }

                    return 'ferme_'.$choice->getId();
                },
                'attr' => ['class' => 'js-select2'],
                'placeholder' => '',
                'required' => false,
            ])
            ->add('file', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize' => '10Mi',
                        'maxSizeMessage' => 'Le fichier que vous avez sélectionné est trop volumineux. La taille maximale autorisée est de 10 Mo.',
                    ]),
                ],
                'attr' => ['class' => 'hidden'],
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'current_user' => null,
            'data_class' => DocumentUtilisateurFormState::class,
        ]);
    }
}
