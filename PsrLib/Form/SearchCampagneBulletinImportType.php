<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\SearchCampagneBulletinImportState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Repository\CampagneRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchCampagneBulletinImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('amap', ChoiceType::class, [
                'label' => 'AMAP',
                'choices' => $options['amap_choices'],
                'choice_label' => 'nom',
                'choice_value' => 'id',
                'required' => true,
                'empty_data' => function (FormInterface $form) {
                    $data = $form->getData();

                    // Prevent injection of non accessible amap for user
                    Assertion::inArray($data, $form->getConfig()->getOption('choices'));

                    return $data;
                },
            ])
            ->add('campagne', ChoiceType::class, [
                'choices' => [],
                'required' => false,
                'label' => 'Campagne',
            ])
            ->addEventlistener(FormEvents::PRE_SET_DATA, function (PreSetDataEvent $event): void {
                $this->addFieldCampagne($event->getForm(), $event->getData()->getAmap());
            })
        ;

        $builder->get('amap')->addEventlistener(FormEvents::POST_SUBMIT, function (PostSubmitEvent $event): void {
            $this->addFieldCampagne($event->getForm()->getParent(), $event->getForm()->getData());
        });
    }

    private function addFieldCampagne(FormInterface $builder, ?Amap $amap): void
    {
        if (null === $amap) {
            return;
        }
        $builder->add('campagne', EntityType::class, [
            'class' => Campagne::class,
            'query_builder' => function (CampagneRepository $er) use ($amap) {
                return $er->createQueryBuilder('c')
                    ->where('c.amap = :amap')
                    ->setParameter('amap', $amap)
                    ->orderBy('c.nom', 'ASC')
                ;
            },
            'choice_label' => 'nom',
            'required' => false,
            'label' => 'Campagne',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'amap_choices' => [],
            'method' => 'GET',
            'data_class' => SearchCampagneBulletinImportState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch,
        ]);
    }
}
