<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Carbon\Carbon;
use PsrLib\DTO\SearchDocumentUtilisateurState;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\Services\AdhesionRangeBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDocumentUtilisateurFormType extends AbstractType
{
    public function __construct(private readonly AdhesionRangeBuilder $adhesionRangeBuilder)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $nextYear = Carbon::now()->addYear();

        $builder
            ->add('year', AnneeAdhesionGenericType::class, [
                'required' => false,
                'label' => 'Année',
                'placeholder' => '',
                'choices' => array_combine($this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear), $this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear)),
            ])
            ->add('keyword', TextType::class, [
                'required' => false,
                'label' => 'Mot clé',
            ])
        ;
    }

    public function getParent()
    {
        return SearchBaseRegionDepartementType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDocumentUtilisateurState::class,
            'method' => 'GET',
            'empty_data' => function (FormInterface $form) {
                $state = new SearchDocumentUtilisateurState();
                $state->setRegion($form->get('region')->getData());
                $state->setDepartement($form->get('departement')->getData());

                return $state;
            },
        ]);
    }
}
