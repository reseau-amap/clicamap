<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Enum\SearchUserStatePermissionEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchUserPermissionFieldType extends AbstractType
{
    public function getParent(): string
    {
        return EnumType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => SearchUserStatePermissionEnum::class,
            'choice_label' => fn (SearchUserStatePermissionEnum $choice) => match ($choice) {
                SearchUserStatePermissionEnum::BLANK => '',
                SearchUserStatePermissionEnum::Y => 'O',
                SearchUserStatePermissionEnum::N => 'N',
            },
            'empty_data' => SearchUserStatePermissionEnum::BLANK,
        ]);
    }
}
