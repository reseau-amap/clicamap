<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\AddressType;
use PsrLib\Form\Type\NameType;
use PsrLib\Form\Type\VilleType;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

class UserProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?User $data */
        $data = $builder->getData();

        $builder
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur',
            ])
            ->add('name', NameType::class, [
                'label' => false,
                'disabled' => $options['disable_profil_name_edit'],
            ])
            ->add('address', AddressType::class, [
                'label' => false,
            ])
            ->add('ville', VilleType::class, [
                'label' => 'Ville',
                'constraints' => [new NotBlank()],
            ])
            ->add('emails', CollectionType::class, [
                'entry_type' => UserEmailType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'Emails',
                'entry_options' => ['label' => false, 'attr' => ['class' => 'col-xs-11']],
            ])
            ->add('numTel1', TextType::class, [
                'label' => 'Téléphone 1',
                'required' => false,
            ])
            ->add('numTel2', TextType::class, [
                'label' => 'Téléphone 2',
                'required' => false,
            ])
            ->add('newsletter', CheckboxType::class, [
                'label' => 'J\'autorise Clic\'AMAP à mettre à jour mes coordonnées sur les listes de diffusion email du réseau auquel j\'appartiens',
                'required' => false,
            ])
        ;

        if (null !== $data && ($data->isAdminRegion() || $data->isAdminDepartment())) {
            $builder->add('adhesionInfo', AdhesionInfoType::class, [
                'label' => false,
                'constraints' => [new Valid()],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'disable_profil_name_edit' => false,
        ]);
    }
}
