<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Carbon\Carbon;
use PsrLib\DTO\SearchFermeLivraisonListState;
use PsrLib\Form\Type\PeriodFlatpickrType;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFermeLivraisonListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('period', PeriodFlatpickrType::class, [
                'label' => 'Période',
                'empty_data' => PeriodFlatpickrType::periodToString(
                    Period::buildFromDates(
                        Carbon::now(),
                        Carbon::now()->addMonths(1)
                    )
                ),
                'min_date' => null,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchFermeLivraisonListState::class,
            'attr' => ['class' => 'js-form-search'],
            'method' => 'GET',
        ]);
    }
}
