<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContratPaiementStatutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contratPaiementStatut', \PsrLib\Form\Type\ContratPaiementStatutType::class, [
                'required' => true,
                'label' => 'Nouveau statut',
            ])
        ;
    }
}
