<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amapien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapAdminsAmapienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function ($event): void {
                $form = $event->getForm();
                $data = $event->getData();
                if ($data instanceof Amapien) {
                    $form->add('admin', CheckboxType::class, [
                        'label' => $data->getUser(),
                        'required' => false,
                    ]);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Amapien::class,
        ]);
    }
}
