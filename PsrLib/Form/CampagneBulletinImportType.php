<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CampagneBulletinImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'Fichier',
                'required' => false,
                'constraints' => [new NotBlank(['message' => 'Veuillez sélectionner un fichier.'])],
                'attr' => [
                    'accept' => '.xls',
                    'class' => 'hidden',
                ],
            ])
        ;
    }
}
