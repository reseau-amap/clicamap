<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\SearchDistributionAmapienState;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionAmapienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?User $currentUser */
        $currentUser = $options['current_user'];
        Assertion::notNull($currentUser);
        $amaps = $currentUser->getAmapsAsAmapienActif();

        SearchDistributionBaseType::buildFormForAmaps($builder, $amaps, [
            'min_date' => Carbon::now(),
        ]);

        $builder
            ->add('filter', ChoiceType::class, [
                'choices' => [
                    'Afficher tous les créneaux disponibles' => SearchDistributionAmapienState::FILTER_AVAILABLE,
                    'Afficher uniquement les créneaux auxquels je suis inscrit' => SearchDistributionAmapienState::FILTER_MINE,
                    'Afficher tous les créneaux' => SearchDistributionAmapienState::FILTER_ALL,
                ],
                'label' => 'Filtre',
                'empty_data' => SearchDistributionAmapienState::FILTER_AVAILABLE,
            ])
        ;
    }

    public function getParent()
    {
        return SearchDistributionBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionAmapienState::class,
        ]);
    }
}
