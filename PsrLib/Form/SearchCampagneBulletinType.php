<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\DTO\SearchCampagneBulletinState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchCampagneBulletinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('annee', ChoiceType::class, [
                'choices' => array_combine($options['annee_choices'], $options['annee_choices']),
                'placeholder' => '',
                'required' => false,
                'label' => 'Année d\'adhésion',
            ])
            ->add('keyword', TextType::class, [
                'required' => false,
                'label' => 'Mot clé',
            ])
            ->add('onlyDraft', CheckboxType::class, [
                'required' => false,
                'label' => 'Afficher uniquement les bulletins sans reçus',
            ])
        ;
    }

    public function getParent()
    {
        return SearchCampagneBulletinImportType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'annee_choices' => [],
            'method' => 'GET',
            'data_class' => SearchCampagneBulletinState::class,
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
        ]);
    }
}
