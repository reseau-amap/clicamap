<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\Validator\PasswordStrength;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => true,
            'first_options' => ['label' => 'Mot de passe'],
            'second_options' => ['label' => 'Repeat Password'],
            'constraints' => [
                new NotBlank(message: 'Veuillez saisir un mot de passe.'),
                new Length(min: 8, max: 200, minMessage: 'Le champ "Mot de passe" doit contenir au moins 8 caractères.'),
                new PasswordStrength(),
            ],
            'options' => ['attr' => ['maxlength' => 200]], // Workaround for https://github.com/bjeavons/zxcvbn-php/issues/74
            'invalid_message' => 'Les deux mot de passe ne correspondent pas',
        ]);
    }

        public function configureOptions(OptionsResolver $resolver): void
        {
            $resolver->setDefaults([
            ]);
        }
}
