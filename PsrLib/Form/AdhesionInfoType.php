<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\AdhesionInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdhesionInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nomReseau', TextType::class, [
                'required' => false,
                'label' => 'Nom du réseau',
            ])
            ->add('siret', TextType::class, [
                'required' => false,
                'label' => 'SIRET',
            ])
            ->add('rna', TextType::class, [
                'required' => false,
                'label' => 'RNA',
            ])
            ->add('villeSignature', TextType::class, [
                'required' => false,
                'label' => 'Ville mentionnée dans la signature (Fait à)',
            ])
            ->add('site', TextType::class, [
                'required' => false,
                'label' => 'Site internet',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdhesionInfo::class,
            'validation_groups' => ['Default', 'adhesion_info'],
        ]);
    }
}
