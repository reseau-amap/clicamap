<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\RegionDepartementType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FermeRefProduitType extends AbstractType
{
    public function __construct(private readonly AmapRepository $amapRepo)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User|null $currentUser */
        $currentUser = $options['currentUser'];
        Assertion::notNull($currentUser);

        if ($currentUser->isAmapAdmin()
            || (!$currentUser->isAdmin())) {
            $this->addForAmapAmapienNonAdmin($builder, $currentUser);

            return;
        }

        $builder
            ->add('regionDepartement', RegionDepartementType::class, [
                'currentUser' => $currentUser,
            ])
            ->get('regionDepartement')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                $this->addAmapField($event->getForm()->getParent(), []);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                /** @var Departement|null $departement */
                $departement = $event->getForm()->get('departement')->getData();

                if (null !== $departement) {
                    $this->addAmapField(
                        $event->getForm()->getParent(),
                        $this->amapRepo->findByDepartement($departement)
                    );
                }
            })
        ;
    }

    public function addForAmapAmapienNonAdmin(FormBuilderInterface $builder, User $currentUser): void
    {
        if ($currentUser->isAmapAdmin()) {
            $amaps = $currentUser->getAdminAmaps()->toArray();
        } elseif ($currentUser->isAmapien()) {
            $amaps = $this
                ->amapRepo
                ->qbAmapAccessibleForAmapien($currentUser)
                ->getQuery()
                ->getResult()
            ;
        } else {
            throw new \InvalidArgumentException('Invalid user type for $currentUser');
        }

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($amaps): void {
                $this->addAmapField($event->getForm(), $amaps);
            })
        ;
    }

    /**
     * @param Amap[] $amaps
     */
    public function addAmapField(FormInterface $form, array $amaps): void
    {
        $form
            ->add('amap', ChoiceType::class, [
                'choices' => $amaps,
                'choice_label' => fn (?Amap $amap) => null === $amap ? '' : $amap->getNom(),
                'choice_value' => fn (?Amap $amap) => $amap?->getId(),
                'required' => false,
                'placeholder' => count($amaps) > 1 ? '' : false,
                'empty_data' => 1 === count($amaps) ? (string) $amaps[0]->getId() : null,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
            'currentUser' => null,
        ]);
    }
}
