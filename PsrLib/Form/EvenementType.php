<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\CarbonDateType;
use PsrLib\Form\Type\CheckSwitchType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\EvenementAmap;
use PsrLib\ORM\Entity\EvenementDepartement;
use PsrLib\ORM\Entity\EvenementFerme;
use PsrLib\ORM\Entity\EvenementRegion;
use PsrLib\ORM\Entity\EvenementSuperAdmin;
use PsrLib\ORM\Entity\EvenementWithRelatedEntity;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $data = $options['data'];
        Assertion::isInstanceOf($data, Evenement::class);

        $user = $options['current_user'];
        Assertion::isInstanceOf($user, User::class);

        $builder
            ->add('titre', TextType::class, [
                'required' => false,
                'label' => 'Titre *',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description *',
                'required' => false,
                'attr' => ['class' => 'summernote-lite'],
                'empty_data' => '',
            ])
            ->add('descriptionCourte', TextareaType::class, [
                'label' => 'Description courte *',
                'required' => false,
                'attr' => ['class' => 'summernote-lite'],
                'empty_data' => '',
            ])
            ->add('lieu', TextType::class, [
                'label' => 'Lieu',
                'required' => false,
                'attr' => ['placeholder' => 'Texte libre ou URL'],
            ])
            ->add('dateDeb', CarbonDateType::class, [
                'label' => 'Date de début',
                'required' => false,
            ])
            ->add('dateFin', CarbonDateType::class, [
                'label' => 'Date de fin',
                'required' => false,
            ])
            ->add('img', FileType::class, [
                'mapped' => false,
                'constraints' => new Image([
                    'maxHeight' => 1024,
                    'maxWidth' => 1024,
                    'mimeTypes' => ['image/jpeg', 'image/png'],
                    'maxSize' => '2Mi',
                ]),
                'attr' => ['class' => 'hidden'],
                'label' => 'Image',
                'required' => false,
                'help' => 'Taille max: 2Mo, Formats autorisés: jpeg, png',
            ])
            ->add('pj', FileType::class, [
                'mapped' => false,
                'constraints' => new File([
                    'maxSize' => '2Mi',
                ]),
                'attr' => ['class' => 'hidden'],
                'label' => 'Pièce jointe',
                'required' => false,
                'help' => 'Taille max: 2Mo',
            ])
        ;

        if ($data instanceof EvenementWithRelatedEntity) {
            $builder->add('accesAbonnesDefautUniquement', CheckSwitchType::class, [
                'label' => 'Accès abonnés par défaut uniquement',
                'required' => false,
            ]);
            $this->addFieldRelatedEntity($data, $user, $builder);
        }
        if ($data instanceof EvenementSuperAdmin) {
            $builder
                ->add('related', TextType::class, [
                    'disabled' => true,
                    'mapped' => false,
                    'label' => 'Profil',
                    'data' => 'Super Admin',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
            'current_user' => null,
        ]);
    }

    private function addFieldRelatedEntity(EvenementWithRelatedEntity $evt, User $user, FormBuilderInterface $builder): void
    {
        $relatedFieldDefault = [
            'constraints' => [new NotNull(['message' => 'Ce champ ne doit pas être vide'])],
            'attr' => ['class' => 'js-select2'],
        ];
        if ($evt instanceof EvenementFerme) {
            $builder->add('related', EntityType::class, array_merge($relatedFieldDefault, [
                'label' => 'Ferme',
                'class' => Ferme::class,
                'query_builder' => fn (FermeRepository $fermeRepository) => $fermeRepository->qbEvenementCreationForUser($user),
            ]));
        } elseif ($evt instanceof EvenementAmap) {
            $builder->add('related', EntityType::class, array_merge($relatedFieldDefault, [
                'label' => 'Amap',
                'class' => Amap::class,
                'query_builder' => fn (AmapRepository $amapRepository) => $amapRepository->qbEvenementCreationForUser($user),
            ]));
        } elseif ($evt instanceof EvenementRegion) {
            $builder->add('related', EntityType::class, array_merge($relatedFieldDefault, [
                'label' => 'Région',
                'class' => Region::class,
                'query_builder' => fn (RegionRepository $regionRepository) => $regionRepository->qbRegionAsAdmin($user),
            ]));
        } elseif ($evt instanceof EvenementDepartement) {
            $builder->add('related', EntityType::class, array_merge($relatedFieldDefault, [
                'label' => 'Département',
                'class' => Departement::class,
                'query_builder' => fn (DepartementRepository $departementRepository) => $departementRepository->qbDepartementAsAdmin($user),
            ]));
        } else {
            throw new \LogicException('Invalid related entity');
        }
    }
}
