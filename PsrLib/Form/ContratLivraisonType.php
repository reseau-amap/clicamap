<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\Form\Type\CheckSwitchType;
use PsrLib\ORM\Entity\ContratLivraison;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContratLivraisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('livre', CheckSwitchType::class, [
                'label' => 'Livraison réalisée ?',
                'required' => false,
            ])
            ->add('hidden', HiddenType::class, [ // Add hidden field to trigger form on "livre" false
                'label' => false,
                'mapped' => false,
                'data' => 'hidden',
            ])
            ->add('cellules', CollectionType::class, [
                'entry_type' => ContratLivraisonCelluleType::class,
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContratLivraison::class,
        ]);
    }
}
