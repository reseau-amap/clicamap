<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\DTO\UserLogin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AuthentificationCdpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                /** @var UserLogin $data */
                $data = $event->getData();

                $authenticatedUser = $data->getUser();
                if (null === $authenticatedUser) {
                    return;
                }

                if (!$authenticatedUser->isAmapien()) {
                    $event
                        ->getForm()
                        ->get('user')
                        ->addError(
                            new FormError('Cette interface est uniquement disponible pour les amapiens')
                        )
                    ;
                }
            })
        ;
    }

    public function getParent()
    {
        return UserLoginType::class;
    }
}
