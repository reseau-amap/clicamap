<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\SearchDistributionBaseState;
use PsrLib\Form\Type\PeriodFlatpickrType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionBaseType extends AbstractType
{
    /**
     * @param Amap[]                $amapChoices
     * @param array<string, Carbon> $periodOptionsAdditional
     */
    public static function buildFormForAmaps(FormBuilderInterface $builder, $amapChoices, $periodOptionsAdditional): void
    {
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $amapChoices,
                'choice_label' => fn (?Amap $amap) => null === $amap ? '' : $amap->getNom(),
                'choice_value' => fn (?Amap $amap) => null === $amap ? '' : $amap->getId(),
                'label' => 'AMAP',
                'required' => false,
                'empty_data' => 1 === count($amapChoices) ? (string) $amapChoices[0]->getId() : null,
                'placeholder' => count($amapChoices) > 1 ? '' : false,
            ])
            ->add('livLieu', ChoiceType::class, [
                'label' => 'Lieu de livraison',
                'disabled' => true,
            ])
            ->add('period', TextType::class, [
                'label' => 'Période',
                'disabled' => true,
            ])
        ;

        $builder
            ->get('amap')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($periodOptionsAdditional): void {
                /** @var Amap|null $amap */
                $amap = $event->getForm()->getData();
                if (null === $amap) {
                    return;
                }

                $lls = $amap->getLivraisonLieux()->toArray();

                $periodOptions = array_merge([
                    'label' => 'Période',
                ], $periodOptionsAdditional);
                $event
                    ->getForm()
                    ->getParent()
                    ->add('livLieu', ChoiceType::class, [
                        'label' => 'Lieu de livraison',
                        'empty_data' => 1 === count($lls) ? (string) $lls[0]->getId() : null,
                        'choices' => $lls,
                        'required' => false,
                        'placeholder' => count($lls) > 1 ? '' : false,
                        'choice_label' => fn (?AmapLivraisonLieu $ll) => null === $ll ? '' : $ll->getNom(),
                        'choice_value' => fn (?AmapLivraisonLieu $ll) => null === $ll ? '' : $ll->getId(),
                    ])
                    ->add('period', PeriodFlatpickrType::class, $periodOptions)
                ;
            })
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $currentUser */
        $currentUser = $options['current_user'];
        Assertion::notNull($currentUser);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionBaseState::class,
            'current_user' => null,
            'method' => 'GET',
            'attr' => ['class' => 'js-form-search'],
        ]);
    }
}
