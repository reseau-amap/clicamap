<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\CaptachaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PortailContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'constraints' => [new NotBlank(), new Length(['max' => 20])],
                'required' => false,
                'label' => 'Nom',
            ])
            ->add('prenom', TextType::class, [
                'constraints' => [new NotBlank(), new Length(['max' => 20])],
                'required' => false,
                'label' => 'Prénom',
            ])
            ->add('email', EmailType::class, [
                'constraints' => [new NotBlank(), new Length(['max' => 50]), new Email(['mode' => 'strict'])],
                'required' => false,
                'label' => 'Email',
            ])
            ->add('cp', TextType::class, [
                'constraints' => [new Length(['min' => 5, 'max' => 5])],
                'required' => false,
                'label' => 'Code postal',
            ])
            ->add('contenu', TextareaType::class, [
                'constraints' => [new NotBlank(), new Length(['max' => 1000])],
                'required' => false,
                'label' => 'Contenu',
            ])
            ->add('captcha', CaptachaType::class, [
                'constraints' => [new NotBlank()],
                'required' => false,
                'label' => 'Code de vérification',
            ])
            ->add('confirmation', CheckboxType::class, [
                'required' => false,
                'label' => 'Recevoir un mail de confirmation',
            ])
        ;
    }
}
