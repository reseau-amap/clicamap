<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom du document',
                'required' => false,
            ])
            ->add('permissionAnonyme', CheckboxType::class, [
                'label' => "Page d'accueil (utilisateur non connecté)",
                'required' => false,
            ])
            ->add('permissionAdmin', CheckboxType::class, [
                'label' => 'Administrateur réseau',
                'required' => false,
            ])
            ->add('permissionPaysan', CheckboxType::class, [
                'label' => 'Paysan',
                'required' => false,
            ])
            ->add('permissionAmap', CheckboxType::class, [
                'label' => 'AMAP',
                'required' => false,
            ])
            ->add('permissionAmapienref', CheckboxType::class, [
                'label' => 'AMAPien référent',
                'required' => false,
            ])
            ->add('permissionAmapien', CheckboxType::class, [
                'label' => 'AMAPien',
                'required' => false,
            ])
            ->add('lien', TextType::class, [
                'label' => 'Lien vers le document',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
