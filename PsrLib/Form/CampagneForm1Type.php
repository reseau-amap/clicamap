<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Carbon\Carbon;
use PsrLib\Form\Type\AnneeAdhesionGenericType;
use PsrLib\Form\Type\PeriodType;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneMontant;
use PsrLib\Services\AdhesionRangeBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneForm1Type extends AbstractType
{
    public function __construct(private readonly AdhesionRangeBuilder $adhesionRangeBuilder)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $nextYear = Carbon::now()->addYear();

        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom',
                'attr' => ['placeholder' => sprintf('Adhésion %s', (int) date('Y') + 1)],
                'required' => false,
            ])
            ->add('period', PeriodType::class, [
                'label' => false,
            ])
            ->add('anneeAdhesionAmapien', AnneeAdhesionGenericType::class, [
                'label' => 'Année d\'adhésion à afficher sur le profil de l\'amapien',
                'placeholder' => false,
                'choices' => array_combine($this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear), $this->adhesionRangeBuilder->buildAdhesionAvailiableRange($nextYear)),
            ])
            ->add('montants', CollectionType::class, [
                'entry_type' => CampagneMontantType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'well'],
                ],
                'prototype_data' => new CampagneMontant(),
            ])
        ;

        // Force reordering of montants
        $builder->addeventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
            /** @var Campagne $campagne */
            $campagne = $event->getData();
            $newMontants = [];
            foreach ($campagne->getMontants() as $montant) {
                $newMontants[] = $montant;
                $campagne->removeMontant($montant);
            }
            usort($newMontants, fn (CampagneMontant $a, CampagneMontant $b) => $a->getOrdre() - $b->getOrdre());
            foreach ($newMontants as $montant) {
                $campagne->addMontant($montant);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Campagne::class,
        ]);
    }
}
