<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Campagne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneForm2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('paiementMethode', TextType::class, [
                'label' => 'Titre moyen de paiement',
                'required' => false,
                'attr' => ['placeholder' => 'Chèque ou virement'],
            ])
            ->add('paiementDescription', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'attr' => ['class' => 'summernote-lite', 'placeholder' => 'Description du moyen de paiement. Exemple : ordre du chèque.'],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Campagne::class,
        ]);
    }
}
