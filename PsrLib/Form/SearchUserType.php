<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\DTO\SearchUserState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('superAdmin', SearchUserPermissionFieldType::class, [
                'label' => 'Super admin',
            ])
            ->add('admin', SearchUserPermissionFieldType::class, [
                'label' => 'Admin',
            ])
            ->add('paysan', SearchUserPermissionFieldType::class, [
                'label' => 'Paysan',
            ])
            ->add('amap', SearchUserPermissionFieldType::class, [
                'label' => 'Amap',
            ])
            ->add('amapien', SearchUserPermissionFieldType::class, [
                'label' => 'Amapien',
            ])
        ;
    }

    public function getParent(): string
    {
        return SearchBaseRegionDepartementType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchUserState::class,
            'empty_data' => function (FormInterface $form) {
                $state = new SearchUserState();
                $state->setRegion($form->get('region')->getData());
                $state->setDepartement($form->get('departement')->getData());

                return $state;
            },
        ]);
    }
}
