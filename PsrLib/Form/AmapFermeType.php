<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapFermeType extends AbstractType
{
    /**
     * @var Ferme[]
     */
    private ?array $fermeChoices = null;

    public function __construct(private readonly FermeRepository $fermeRepo)
    {
    }

    /**
     * @return Ferme[]
     */
    public function getFermeFromAmapCached(?Amap $amap)
    {
        if (null !== $this->fermeChoices) {
            return $this->fermeChoices;
        }

        $this->fermeChoices = $this->fermeRepo->getFromAmap($amap);

        return $this->fermeChoices;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User|null $restrictFermesRef */
        $restrictFermesRef = $options['restrict_fermes_ref'];

        /** @var Amap[] $choices */
        $choices = $options['amap_choices'];
        $builder
            ->add('amap', ChoiceType::class, [
                'choices' => $choices,
                'data' => 1 === count($choices) ? $choices[0] : null,
                'disabled' => 1 === count($choices),
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Amap $amap) => null === $amap ? '' : $amap->getNom(),
                'choice_value' => fn (?Amap $amap) => null === $amap ? '' : $amap->getId(),
            ])
            ->add('ferme', ChoiceType::class, [
                'disabled' => true,
            ])
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($restrictFermesRef): void {
                $form = $event->getForm();

                $data = $event->getData();
                if (null === $data) {
                    $amap = $form->get('amap')->getData();
                } else {
                    $amap = $data['amap'] ?? null;
                }

                $this->addFermeField($form, $amap, $restrictFermesRef);
            })
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($choices, $restrictFermesRef): void {
                $form = $event->getForm();
                $data = $event->getData();

                $amapId = $data['amap'] ?? '';
                if ('' === $amapId) {
                    $amap = $form->get('amap')->getData();
                } else {
                    $amap = null;
                    foreach ($choices as $choice) {
                        if ($choice->getId() === (int) $amapId) {
                            $amap = $choice;
                        }
                    }
                }

                $this->addFermeField($form, $amap, $restrictFermesRef);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'amap_choices' => [],
                'restrict_fermes_ref' => null,
            ])
        ;
    }

    private function addFermeField(FormInterface $form, ?Amap $amap, ?User $restrictFermesRef): void
    {
        $choices = null === $amap ? [] : $this->getFermeFromAmapCached($amap);
        if (null !== $restrictFermesRef && null !== $amap) {
            $refAmapien = $restrictFermesRef->getUserAmapienAmapForAmap($amap);
            $choices = array_filter($choices, fn (Ferme $ferme) => $refAmapien->getRefProdFermes()->contains($ferme));
        }

        $form
            ->add('ferme', ChoiceType::class, [
                'choices' => $choices,
                'disabled' => 0 === count($choices),
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (?Ferme $ferme) => null === $ferme ? '' : $ferme->getNom(),
                'choice_value' => fn (?Ferme $ferme) => null === $ferme ? '' : $ferme->getId(),
            ])
        ;
    }
}
