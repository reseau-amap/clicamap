<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ContratSouscriptionForm2DateType extends AbstractType
{
    public function __construct(private readonly PropertyAccessor $pa)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModeleContrat|null $mc */
        $mc = $options['mc'];
        Assertion::isInstanceOf($mc, ModeleContrat::class);

        $dates = $mc->getDatesReglementOrderedAsc()->toArray();

        $formOptions = [
            'label' => false,
            'choice_label' => fn (ModeleContratDatesReglement $d) => $d->getDate()->format('d/m/Y'),
            'choice_value' => fn (?ModeleContratDatesReglement $d) => (string) $d?->getId(),
        ];
        if ('[0]' === $options['property_path']) {
            $builder
                ->add('modeleContratDatesReglement', ChoiceType::class, array_merge($formOptions, [
                    'choices' => [$dates[0]],
                    'placeholder' => false,
                    'row_attr' => ['class' => 'disabled'],
                ]))
            ;

            return;
        }

        if ($options['disable_all']) {
            $builder
                ->add('modeleContratDatesReglement', ChoiceType::class, array_merge($formOptions, [
                    'choices' => [$this->pa->getValue($dates, $options['property_path'])],
                    'placeholder' => false,
                    'row_attr' => ['class' => 'disabled'],
                ]))
            ;

            return;
        }

        $builder
            ->add('modeleContratDatesReglement', ChoiceType::class, array_merge($formOptions, [
                'choices' => $dates,
                'placeholder' => '',
                'required' => false,
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContratDatesReglement::class,
            'mc' => null,
            'disable_all' => false,
        ]);
    }
}
