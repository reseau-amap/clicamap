<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\DTO\ContratCommandeBase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContratSouscriptionForm1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('items', CollectionType::class, [
                'entry_type' => ContratSouscriptionForm1ItemType::class,
                'entry_options' => [
                    'label' => false,
                    'mc' => $options['data']->getMc(),
                ],
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContratCommandeBase::class,
        ]);
    }
}
