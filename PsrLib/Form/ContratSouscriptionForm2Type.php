<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ContratSouscriptionForm2Type extends AbstractType
{
    public function __construct(
        private readonly Translator $translator,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Contrat $contrat */
        $contrat = $options['data'];
        Assertion::isInstanceOf($contrat, Contrat::class);

        $mc = $contrat->getModeleContrat();

        /** @var Money $amountTotal */
        $amountTotal = $options['total'];
        Assertion::isInstanceOf($amountTotal, \Money\Money::class);

        $reglementMax = $mc->getReglementNbMax();
        $reglementMcCount = $mc->getDatesReglementOrderedAsc()->count();

        $contratCount = $contrat->getDatesReglements()->count();
        $builder
            ->add('reglementNb', IntegerType::class, [
                'label' => 'Nombre de règlements',
                'mapped' => false,
                'data' => $contratCount > 0 ? $contratCount : null,
                'attr' => ['min' => 1, 'max' => $reglementMax, 'step' => 1],
                'constraints' => [new NotBlank(), new Range(['min' => 1, 'max' => $reglementMax, 'groups' => ['subscription_2']])],
                'row_attr' => ['class' => 1 === $reglementMax ? 'disabled' : ''],
            ])
            ->add('reglementType', ChoiceType::class, [
                'choices' => $mc->getReglementType(),
                'choice_label' => function ($mode) use ($mc) {
                    $trans = $this->translator->trans($mode, [], 'modele_contrat_reglement_type');
                    if (ModeleContrat::mode_cheque === $mode) {
                        $trans .= sprintf(' (à l\'ordre de %s)', $mc->getFerme()->getOrdreCheque());
                    }

                    return $trans;
                },
                'label' => 'Modes de règlement',
                'required' => false,
                'expanded' => true,
                'multiple' => false,
                'placeholder' => false,
            ])
        ;

        $formModifier = function (FormInterface $form, int $reglementNb) use ($mc, $options, $reglementMcCount): void {
            $form->add('datesReglements', CollectionType::class, [
                'entry_type' => ContratSouscriptionForm2DateType::class,
                'entry_options' => [
                    'mc' => $mc,
                    'disable_all' => $reglementNb === $reglementMcCount,
                ],
                'label' => false,
                'validation_groups' => $options['validation_groups'],
                'error_bubbling' => false,
            ]);
        };

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function ($event) use ($formModifier, $amountTotal): void {
                $form = $event->getForm();
                /** @var Contrat $contrat */
                $contrat = $event->getData();

                $formModifier($form, $contrat->getDatesReglements()->count());

                $this->updateContatAmount($amountTotal, $contrat);
            })
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function ($event) use ($mc, $reglementMax, $reglementMcCount): void {
                $data = $event->getData();

                // User can bypass front contraints with "Enter" key. As we disable constraints, we need to change value in event
                $reglementNb = (int) $data['reglementNb'];
                if ($reglementNb < 1) {
                    $reglementNb = 1;
                }

                if ($reglementNb > $reglementMax) {
                    $reglementNb = $reglementMax;
                }
                $data['reglementNb'] = (string) $reglementNb;

                // Pre fill all dates if regementNb is reglementAvaliableCount
                if ($reglementNb === $reglementMcCount) {
                    $dates = array_map(fn (ModeleContratDatesReglement $mcd) => ['modeleContratDatesReglement' => $mcd->getId()], $mc->getDatesReglementOrderedAsc()->toArray());
                    $data['datesReglements'] = $dates;
                }

                $event->setData($data);
            })
        ;

        $builder->get('reglementNb')->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($mc, $formModifier, $amountTotal): void {
            $form = $event->getForm();
            /** @var Contrat $contrat */
            $contrat = $form->getParent()->getData();

            $reglementNb = $form->getData();
            while ($contrat->getDatesReglements()->count() < $reglementNb) {
                $contrat->addDatesReglement(new ContratDatesReglement());
            }
            while ($contrat->getDatesReglements()->count() > $reglementNb) {
                $contrat->removeDatesReglement($contrat->getDatesReglements()->last());
            }

            // Recalculate amounts
            $this->updateContatAmount($amountTotal, $contrat);

            // Force first date available to be the first selected
            /** @var ContratDatesReglement|false $firstDate */
            $firstDate = $contrat->getDatesReglements()->first();
            if (false !== $firstDate) {
                $firstDate->setModeleContratDatesReglement($mc->getDatesReglementOrderedAsc()->first());
            }

            $formModifier($form->getParent(), $reglementNb);
        });
    }

    private function updateContatAmount(Money $amountTotal, Contrat $contrat): void
    {
        $dateCount = $contrat->getDatesReglements()->count();
        if (0 === $dateCount) {
            return;
        }
        $amounts = $amountTotal->allocateTo($dateCount);
        foreach ($contrat->getDatesReglements() as $datesReglement) {
            $datesReglement->setMontant(array_shift($amounts));
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contrat::class,
            'total' => null,
        ]);
    }
}
