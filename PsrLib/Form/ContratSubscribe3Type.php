<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;

class ContratSubscribe3Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('confirmSign', CheckboxType::class, [
                'label' => '<b>J\'accepte les termes de <a href="'.site_url('/contrat_signe/preview_pdf').'" target="_blank"> mon contrat juridique</a></b>',
                'label_html' => true,
                'required' => false,
                'constraints' => [new IsTrue(['message' => 'Le champ Confirmation de signature est requis.'])],
            ])
            ->add('confirmCharter', CheckboxType::class, [
                'label' => '<b> Je confirme avoir pris connaissance de la <a href="https://amapartage.fr/images/imagesCK/files/ressources/cont3/f4_charte-des-amap-mars-2014.pdf" target="_blank">Charte des AMAP</a></b>',
                'label_html' => true,
                'required' => false,
                'constraints' => [new IsTrue(['message' => 'Le champ Confirmation de la charte est requis.'])],
            ])
            ->add('hidden', HiddenType::class, [ // Force submit when nothing selected
                'mapped' => false,
                'data' => 'hidden',
            ])
        ;
    }
}
