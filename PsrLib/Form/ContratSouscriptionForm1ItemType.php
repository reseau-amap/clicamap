<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\ContratCommandeItem;
use PsrLib\ORM\Entity\ModeleContrat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ContratSouscriptionForm1ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModeleContrat $mc */
        $mc = $options['mc'];
        Assertion::isInstanceOf($mc, ModeleContrat::class);

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function ($event) use ($mc): void {
                $form = $event->getForm();
                /** @var ContratCommandeItem $data */
                $data = $event->getData();

                if (!$data->isDisabled()) {
                    $isModeDemi = 'demi' === $mc->getInformationLivraison()->getAmapienDeplacementMode();

                    $options = [
                        'constraints' => [
                            new NotBlank(),
                            new Range(['min' => 0]),
                        ],
                        'required' => false,
                        'empty_data' => 0,
                        'label' => false,
                        'html5' => true,
                        'scale' => $isModeDemi ? 1 : 0,
                        'attr' => [
                            'min' => 0,
                            'step' => $isModeDemi ? 0.5 : 1,
                            'class' => 'text-center simulation-input',
                        ],
                    ];

                    if ($mc->getPremiereDateLivrableAvecDelais(Carbon::now())->greaterThan($data->getMcd()->getDateLivraison())) {
                        $options['attr']['readonly'] = 'readonly';
                        $options['data'] = $data->getQty();
                        $options['mapped'] = false;
                    }

                    $form
                        ->add('qty', NumberType::class, $options)
                    ;
                }
            })

            // Empty value for qty field is zero
            ->addEventListener(FormEvents::PRE_SUBMIT, function ($event): void {
                $data = $event->getData();

                if (isset($data['qty']) && '' === $data['qty']) {
                    $data['qty'] = 0;
                    $event->setData($data);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContratCommandeItem::class,
            'mc' => null,
        ]);
    }
}
