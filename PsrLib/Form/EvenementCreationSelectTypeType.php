<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\Enum\EvenementType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\NotNull;

class EvenementCreationSelectTypeType extends AbstractType
{
    public function __construct(
        private readonly Translator $translator
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $options['type_choice'],
                'label' => 'Profil',
                'required' => false,
                'choice_label' => fn (?EvenementType $enum) => $enum ? $this->translator->trans($enum->value, [], 'evenement_add_type') : null,
                'choice_value' => fn (?EvenementType $enum) => $enum ? $enum->value : null,
                'placeholder' => '',
                'constraints' => [new NotNull(['message' => 'Merci de choisir une valeur'])],
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'type_choice' => null,
        ]);
    }
}
