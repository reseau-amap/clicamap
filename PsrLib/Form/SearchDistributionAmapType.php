<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\DTO\SearchDistributionAmapState;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDistributionAmapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?User $currentUser */
        $currentUser = $options['current_user'];
        Assertion::notNull($currentUser);
        $amaps = $currentUser->getAdminAmaps()->toArray();

        SearchDistributionBaseType::buildFormForAmaps($builder, $amaps, []);

        $builder
            ->add('complete', CheckboxType::class, [
                'label' => 'Afficher également les distrib\' complètes',
                'required' => false,
            ])
        ;
    }

    public function getParent()
    {
        return SearchDistributionBaseType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchDistributionAmapState::class,
        ]);
    }
}
