<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\PrixTVAType;
use PsrLib\Form\Type\TypeProductionType;
use PsrLib\ORM\Entity\FermeProduit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class FermeProduitType extends AbstractType
{
    public function __construct(private readonly Translator $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var FermeProduit $produit */
        $produit = $options['data'];
        Assertion::isInstanceOf($produit, FermeProduit::class);

        $builder
            ->add('typeProduction', TypeProductionType::class, [
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder' => '',
                'label' => 'Type de production',
                'attr' => [],
            ])
            ->add('nom', TextType::class, [
                'label' => 'Nom',
            ])
            ->add('conditionnement', TextType::class, [
                'attr' => ['placeholder' => 'ex : 1 pot de 500g'],
                'label' => 'Conditionnement',
            ])
            ->add('certification', ChoiceType::class, [
                'choices' => FermeProduit::AVAILABLE_CERTIFICATIONS,
                'multiple' => false,
                'required' => false,
                'placeholder' => '',
                'choice_label' => fn (string $choice) => $this->translator->trans($choice, [], 'ferme_produit_certification'),
                'label' => 'Label',
            ])
            ->add('regulPds', CheckboxType::class, [
                'label' => 'Régularisation de poids ?',
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Actif ?',
                'disabled' => $options['enabled_state_disabled'],
            ])
            ->add('prix', PrixTVAType::class, [
                'label' => false,
                'with_vat' => $produit->getFerme()->isTva(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FermeProduit::class,
            'enabled_state_disabled' => false,
        ]);
    }
}
