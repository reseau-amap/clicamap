<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\Form\Type\MoneyCustomType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\CampagneMontant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneMontantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => 'Titre montant',
                'attr' => ['placeholder' => 'Adhésion à l\'association'],
                'required' => false,
            ])
            ->add('montantLibre', YesNoChoiceType::class, [
                'label' => 'Montant libre ?',
            ])
            ->add('montant', MoneyCustomType::class, [
                'label' => 'Montant',
                'required' => false,
                'row_attr' => ['class' => 'js-amount-row'],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description (texte libre)',
                'required' => false,
                'attr' => ['class' => 'summernote-lite'],
            ])
            ->add('ordre', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CampagneMontant::class,
        ]);
    }
}
