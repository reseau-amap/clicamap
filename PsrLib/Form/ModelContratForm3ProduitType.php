<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\PrixTVASimpleType;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Repository\FermeProduitRepository;
use PsrLib\Services\MoneyHelper;
use PsrLib\Services\RequestHtmxUtils;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class ModelContratForm3ProduitType extends AbstractType
{
    public function __construct(
        private readonly FermeProduitRepository $fermeProduitRepository,
        private readonly Request $request,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $ferme = $options['ferme'];
        Assertion::isInstanceOf($ferme, Ferme::class);

        $fermeProduits = $this->fermeProduitRepository->getFromFermeOrdered($ferme);
        $tps = array_unique(array_map(fn (FermeProduit $fermeProduit) => $fermeProduit->getTypeProduction(), $fermeProduits));

        $builder
            ->add('typeProduction', EntityType::class, [
                'choices' => $tps,
                'class' => TypeProduction::class,
                'choice_label' => fn (TypeProduction $tp) => $tp->getNom(),
                'label' => false,
            ])
            ->add('prix', PrixTVASimpleType::class, [
                'label' => false,
                'constraints' => [new Valid(['groups' => ['Default']])], // Validate here to change validation group
                'validation_groups' => ['Default'],
            ])
            ->add('regulPds', CheckboxType::class, [
                'label' => 'Régulation de poids',
                'required' => false,
            ])
        ;

        $filterFermeProduitsByTpIp = fn (?int $tpId = null): array => array_filter($fermeProduits, fn (FermeProduit $fp) => $fp->getTypeProduction()->getId() === $tpId);

        $addFermeProduitField = function (FormInterface $form, ?int $tp = null) use ($filterFermeProduitsByTpIp): void {
            $form
                ->add('fermeProduit', EntityType::class, [
                    'choices' => $filterFermeProduitsByTpIp($tp),
                    'class' => FermeProduit::class,
                    'choice_label' => fn (FermeProduit $fp) => $fp->getNom().' / '.$fp->getConditionnement(),
                    'label' => false,
                ])
            ;
        };

        $builder
            ->get('typeProduction')
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($addFermeProduitField): void {
                $tp = $event->getData();

                $addFermeProduitField($event->getForm()->getParent(), $tp?->getId());
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($addFermeProduitField): void {
                $tp = $event->getData();

                $addFermeProduitField($event->getForm()->getParent(), (int) $tp);
            })
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($fermeProduits, $filterFermeProduitsByTpIp): void {
                $data = $event->getData();
                $form = $event->getForm();

                $updateDataFermeProduit = function (FermeProduit $fp) use (&$data): void {
                    $data['prix']['prixTTC'] = MoneyHelper::toStringDecimal($fp->getPrix()->getPrixTTC());
                    if ($fp->isRegulPds()) {
                        $data['regulPds'] = '1';
                    } else {
                        unset($data['regulPds']);
                    }
                };

                if (RequestHtmxUtils::isFieldTriggeredHtmx($this->request, $form->get('typeProduction'))) {
                    $fp = $filterFermeProduitsByTpIp((int) $data['typeProduction']);
                    $updateDataFermeProduit(reset($fp));
                    $event->setData($data);
                }

                if (
                    RequestHtmxUtils::isFieldTriggeredHtmx($this->request, $form->get('fermeProduit'))
                ) {
                    /** @var FermeProduit[] $fermeProduitSent */
                    $fermeProduitSent = array_filter($fermeProduits, fn (FermeProduit $fp) => $fp->getId() === (int) $data['fermeProduit']);
                    if (1 !== count($fermeProduitSent)) {
                        return;
                    }

                    $updateDataFermeProduit(reset($fermeProduitSent));
                    $event->setData($data);
                }
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                /** @var ModeleContratProduit $mcp */
                $mcp = $event->getData();
                $mcp->bindPropertiesDataFromFermeProduit();
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModeleContratProduit::class,
            'ferme' => null,
        ]);
    }
}
