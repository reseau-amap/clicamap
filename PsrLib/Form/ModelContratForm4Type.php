<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratInformationLivraison;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ModelContratForm4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModeleContrat $mc */
        $mc = $options['mc'];
        Assertion::isInstanceOf($mc, ModeleContrat::class);

        $contratDateCount = $mc->getDates()->count();

        $builder
            ->add('produitsIdentiquePaysan', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('produitsIdentiqueAmapien', YesNoChoiceType::class, [
                'required' => false,
            ])
            ->add('nblivPlancher', NumberType::class, [
                'html5' => true,
                'required' => false,
                'constraints' => [
                    new Assert\Range(['min' => 1, 'max' => $contratDateCount, 'groups' => ['form4']]),
                ],
            ])
            ->add('amapienGestionDeplacement', YesNoChoiceType::class, [
                'required' => false,
            ])

            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                /** @var ModeleContratInformationLivraison $data */
                $data = $event->getData();

                $this->addReportFields(
                    $event->getForm(),
                    true === $data->getAmapienGestionDeplacement(),
                );
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                $data = $event->getData();

                $this->addReportFields(
                    $event->getForm(),
                    ($data['amapienGestionDeplacement'] ?? false) === '1',
                );
            })

            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($mc): void {
                /** @var ModeleContratInformationLivraison $data */
                $data = $event->getData();
                if ($mc->getNbLivPlafond() === $data->getNblivPlancher()) {
                    $data->setNblivPlancherDepassement(false);
                }
            })

            // Initialize nb deplacement
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($mc): void {
                /** @var ModeleContratInformationLivraison $data */
                $data = $event->getData();
                $data->setAmapienDeplacementNb(
                    $mc->getDates()->count() - $data->getNblivPlancher()
                );
            })
        ;

        $builder
            ->get('nblivPlancher')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($contratDateCount): void {
                $this->addNbLivPlancherDepassementField($event->getForm()->getParent(), (int) $event->getData(), $contratDateCount);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($contratDateCount): void {
                $this->addNbLivPlancherDepassementField($event->getForm()->getParent(), (int) $event->getData(), $contratDateCount);
            })
        ;
    }

    private function addNbLivPlancherDepassementField(FormInterface $form, int $nbLivPlancher, int $contratDateCount): void
    {
        if ($nbLivPlancher < $contratDateCount) {
            $form->add('nblivPlancherDepassement', YesNoChoiceType::class, [
                'required' => false,
            ]);
        } else {
            $form->remove('nblivPlancherDepassement');
        }
    }

    private function addReportFields(FormInterface $form, bool $amapienGestionDeplacement): void
    {
        if ($amapienGestionDeplacement) {
            $form
                ->add('amapienPermissionReportLivraison', YesNoChoiceType::class, [
                    'required' => false,
                ])
                ->add('amapienPermissionDeplacementLivraison', YesNoChoiceType::class, [
                    'required' => false,
                ])
            ;
        } else {
            $form
                ->remove('amapienPermissionReportLivraison')
                ->remove('amapienPermissionDeplacementLivraison')
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mc' => null,
            'data_class' => ModeleContratInformationLivraison::class,
            'validation_groups' => ['form4'],
        ]);
    }
}
