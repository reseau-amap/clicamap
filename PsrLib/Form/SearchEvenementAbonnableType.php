<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\DTO\SearchEvenementAbonnable;
use PsrLib\Enum\EvenementType;
use PsrLib\Form\Type\TextAutoCompleteType;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\ORM\Repository\RegionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Translator;

class SearchEvenementAbonnableType extends AbstractType
{
    public function __construct(
        private readonly Translator $translator,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('region', EntityType::class, [
                'class' => Region::class,
                'query_builder' => fn (RegionRepository $repo) => $repo->qbAllOrdered(),
                'label' => 'Région',
                'required' => false,
                'choice_label' => 'nom',
                'placeholder' => '',
            ])
            ->add('departement', ChoiceType::class, [
                'disabled' => true,
                'label' => 'Département',
            ])
            ->add('type', EnumType::class, [
                'class' => EvenementType::class,
                'label' => 'Profil',
                'required' => false,
                'choice_label' => fn (?EvenementType $enum) => $enum ? $this->translator->trans($enum->value, [], 'evenement_add_type') : null,
                'placeholder' => '',
            ])
            ->add('keyword', TextAutoCompleteType::class, [
                'label' => 'Recherche',
                'required' => false,
                'attr' => [
                    'data-autocomplete-url' => $this->urlGenerator->generate('evenement_abonnement_keyword'),
                ],
            ])
            ->add('subscriptionOnly', CheckboxType::class, [
                'required' => false,
                'label' => 'Uniquement les abonnements existants',
            ])
        ;

        $builder
            ->get('region')
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                $region = $event->getForm()->getData();

                if (null === $region) {
                    return;
                }

                $event
                    ->getForm()
                    ->getParent()
                    ->add('departement', EntityType::class, [
                        'class' => Departement::class,
                        'label' => 'Département',
                        'required' => false,
                        'query_builder' => fn (DepartementRepository $repo) => $repo->qbByRegionOrdered($region),
                        'placeholder' => '',
                    ])
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchEvenementAbonnable::class,
            'empty_data' => fn (FormInterface $form) => new SearchEvenementAbonnable(),
            'method' => 'GET',
            'validation_groups' => false, // Disable validation to avoir error rendering data dependent switch
        ]);
    }
}
