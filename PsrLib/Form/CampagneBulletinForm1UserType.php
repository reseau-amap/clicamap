<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\Form\Type\AddressType;
use PsrLib\Form\Type\NameType;
use PsrLib\Form\Type\VilleType;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

class CampagneBulletinForm1UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', NameType::class, [
                'label' => false,
            ])
            ->add('numTel1', TextType::class, [
                'label' => 'Tél. 1',
                'required' => false,
            ])
            ->add('numTel2', TextType::class, [
                'label' => 'Tél. 2',
                'required' => false,
            ])
            ->add('emails', CollectionType::class, [
                'entry_type' => UserEmailType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'Emails',
                'entry_options' => ['label' => false, 'attr' => ['class' => 'col-xs-11']],
                'constraints' => [new Valid()],
            ])
            ->add('address', AddressType::class, [
                'label' => false,
            ])
            ->add('ville', VilleType::class, [
                'label' => 'Ville',
                'constraints' => [new NotBlank()],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
