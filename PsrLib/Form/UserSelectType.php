<?php

declare(strict_types=1);

namespace PsrLib\Form;

use PsrLib\ORM\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class UserSelectType extends AbstractType
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = $this->userRepository->getUsersNames();

        $builder
            ->add('user', ChoiceType::class, [
                'choices' => $choices,
                'multiple' => false,
                'expanded' => false,
                'attr' => ['class' => 'js-select2'],
                'placeholder' => '',
                'label' => false,
                'constraints' => [new NotNull()],
            ])
        ;
    }
}
