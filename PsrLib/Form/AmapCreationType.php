<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Form;

use PsrLib\Form\Type\AmapAnneeAdhesionType;
use PsrLib\Form\Type\UserUsernameOrEmailSelectorType;
use PsrLib\Form\Type\VilleType;
use PsrLib\Form\Type\YesNoChoiceType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Uploader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class AmapCreationType extends AbstractType
{
    private AuthorizationCheckerInterface $authorizationChecker;
    private Uploader $uploader;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, Uploader $uploader)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->uploader = $uploader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('admin', UserUsernameOrEmailSelectorType::class, [
                'label' => 'Administrateur',
                'required' => false,
                'attr' => ['placeholder' => 'Indiquez ici l\'identifiant ou l\'adresse email de l\'administrateur de l\'AMAP.'],
                'mapped' => false,
                'data' => $options['admin'],
            ])
            ->add('nom', TextType::class, [
                'required' => false,
                'label' => 'Nom :',
            ])
            ->add('adresseAdmin', TextType::class, [
                'required' => false,
                'label' => 'Adresse administrative :',
            ])
            ->add('adresseAdminNom', TextType::class, [
                'required' => false,
                'label' => 'Nom du lieu de l\'adresse administrative :',
            ])
            ->add('etat', ChoiceType::class, [
                'choices' => [
                    'En création' => Amap::ETAT_CREATION,
                    'En fonctionnement' => Amap::ETAT_FONCIONNEMENT,
                    'En attente' => Amap::ETAT_ATTENTE,
                ],
                'label' => 'Etat',
                'expanded' => true,
            ])
            ->add('possedeAssurance', YesNoChoiceType::class, [
                'required' => false,
                'label' => 'Assurance réseau',
            ])
            ->add('email', TextType::class, [
                'required' => false,
                'label' => 'Email de contact public :',
            ])
            ->add('url', TextType::class, [
                'required' => false,
                'label' => 'Adresse site Web ou blog :',
            ])
            ->add('anneeCreation', TextType::class, [
                'required' => false,
                'label' => 'Année de création de l\'AMAP :',
            ])
            ->add('nbAdherents', TextType::class, [
                'required' => false,
                'label' => 'Nombre d\'adhérents :',
            ])
            ->add('adresseAdminVille', VilleType::class, [
                'constraints' => [new NotNull(['message' => 'Le champ "Code postal, Ville administrative" est requis.'])],
                'attr' => ['readonly' => true],
                'label' => 'Code Postal, Ville :',
            ])
            ->add('siret', TextType::class, [
                'required' => false,
                'label' => 'Numéro SIRET :',
            ])
            ->add('rna', TextType::class, [
                'required' => false,
                'label' => 'Numéro RNA :',
            ])
            ->add('logo', FileType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'Logo :',
                'constraints' => [new Image([
                    'maxHeight' => 1024,
                    'maxWidth' => 1024,
                    'mimeTypes' => ['image/jpeg', 'image/png'],
                    'maxSize' => '500Ki',
                ])],
                'attr' => ['class' => 'hidden'],
            ])
        ;

        if ($this->authorizationChecker->isGranted(AmapVoter::ACTION_AMAP_CHANGE_ADHESION, $options['data'])) {
            $builder
                ->add('anneeAdhesions', AmapAnneeAdhesionType::class, [
                    'amap' => $options['data'],
                    'label' => 'Année(s) d\'adhésion (Liste à choix multiple ; maintenez Shift ou Ctrl enfoncés pour sélectionner / désélectionner plusieurs années) :',
                    'required' => false,
                ])
            ;
        }

        if ($options['additionalFields']) {
            $builder
                ->add('amapEtudiante', YesNoChoiceType::class, [
                    'required' => false,
                    'label' => 'AMAP étudiante',
                ])
                ->add('associationDeFait', YesNoChoiceType::class, [
                    'required' => false,
                    'label' => 'Association de fait :',
                ])
            ;
        }

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
            $logoInput = $event->getForm()->get('logo')->getData();
            if (null === $logoInput) {
                return;
            }

            $logo = $this
                ->uploader
                ->uploadFile($logoInput, \PsrLib\ORM\Entity\Files\AmapLogo::class)
            ;
            $event->getData()->setLogo($logo);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Amap::class,
            'additionalFields' => true,
            'admin' => null,
        ]);
    }
}
