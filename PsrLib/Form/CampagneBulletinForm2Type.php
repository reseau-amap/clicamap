<?php

declare(strict_types=1);

namespace PsrLib\Form;

use Assert\Assertion;
use PsrLib\Form\Type\CarbonDateType;
use PsrLib\ORM\Entity\CampagneBulletin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampagneBulletinForm2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ?CampagneBulletin $bulletin */
        $bulletin = $options['data'];
        Assertion::notNull($bulletin);

        $builder
            ->add('paiementDate', CarbonDateType::class, [
                'label' => 'Date de règlement',
                'required' => false,
            ])
            ->add('payeur', TextType::class, [
                'label' => 'Payeur (émetteur du paiement)',
                'required' => false,
            ])
            ->add('chartre', CheckboxType::class, [
                'label' => 'Je m\'engage à respecter <a href="https://amapartage.fr/images/imagesCK/files/ressources/cont3/f4_charte-des-amap-mars-2014.pdf" target="_blank">la chartre des AMAP</a>',
                'label_html' => true,
                'required' => false,
                'validation_groups' => ['Default'],
            ])
            ->add('amapien', CampagneBulletinForm2AmapienType::class, [
                'label' => 'false',
            ])
            ->add('permissionImage', CheckboxType::class, [
                'label' => sprintf(
                    'J’autorise %s à utiliser mon image (vidéo, photo) dans un but ,de promouvoir ses activités et évènements (presse flyers, site internet, facebook).',
                    $bulletin->getCampagne()->getAutheurInfo()->getNom()
                ),
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CampagneBulletin::class,
            'validation_groups' => ['form2'],
        ]);
    }
}
