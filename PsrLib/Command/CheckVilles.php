<?php

declare(strict_types=1);

namespace PsrLib\Command;

use DI\Attribute\Inject;
use PsrLib\ORM\Repository\VilleRepository;
use PsrLib\Services\NominatimClient;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Check ville consistency with OSM.
 * Use custom nominatim server and no public one because this command will use a large number of requests.
 */
class CheckVilles extends BaseCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:check:villes';

    #[Inject]
    private VilleRepository $villeRepository; // @phpstan-ignore-line

    #[Inject]
    private NominatimClient $nominatimClient; // @phpstan-ignore-line

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->write('Loading villes : ');
        $villes = $this->villeRepository->findAll();
        $io->writeln('ok');

        $errorNotFound = [];
        $errorNotmatching = [];
        $io->progressStart(count($villes));
        foreach ($villes as $ville) {
            $io->progressAdvance();
            $geocodes = $this->nominatimClient->geocode(sprintf(
                '%s, %s',
                $ville->getNom(),
                $ville->getCpString()
            ));
            if (0 === count($geocodes)) {
                $errorNotFound[] = $ville;
                continue;
            }

            if ($geocodes[0]->getAddress()->getISO3166lv16DepNumber() !== $ville->getDepartement()->getId()) {
                $errorNotmatching[] = [
                    'ville' => $ville,
                    'geocode' => $geocodes[0],
                ];
            }
        }

        $io->writeln('Villes not found in OSM : ');
        $this->renderTable(
            $io,
            ['Nom', 'Code postal'],
            array_map(fn ($ville) => [$ville->getNom(), $ville->getCpString()], $errorNotFound)
        );

        $io->writeln('Villes not matching OSM departement : ');
        $this
            ->renderTable(
                $io,
                ['ID', 'Nom', 'Code postal', 'Departement', 'OSM departement'],
                array_map(fn ($error) => [
                    $error['ville']->getId(),
                    $error['ville']->getNom(),
                    $error['ville']->getCpString(),
                    $error['ville']->getDepartement()->getId(),
                    $error['geocode']->getAddress()->getISO3166lv16DepNumber(),
                ], $errorNotmatching)
            )
        ;

        return 0;
    }

    private function renderTable(SymfonyStyle $io, array $header, array $rows): void
    {
        $io->createTable()
            ->setHeaders($header)
            ->setRows($rows)
            ->setStyle('box')
            ->render()
        ;
    }
}
