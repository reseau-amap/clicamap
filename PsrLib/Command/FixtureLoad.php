<?php

declare(strict_types=1);

namespace PsrLib\Command;

use DI\Attribute\Inject;
use PsrLib\Services\FixtureLoader;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FixtureLoad extends BaseCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:fixtures:load';

    #[Inject]
    private FixtureLoader $loader; // @phpstan-ignore-line

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->write('Loading fixtures : ');
        $this->loader->loadFixtures();
        $io->writeln('ok');

        return 0;
    }
}
