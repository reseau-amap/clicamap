<?php

declare(strict_types=1);

namespace PsrLib\Command;

use DI\Attribute\Inject;
use PsrLib\ProjectLocation;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Routing\RouteCollection;

/**
 * Small tool to help find invalid routes usage in controllers and templates.
 */
class CheckRouting extends BaseCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:check:routing';

    #[Inject]
    private RouteCollection $routeCollection; // @phpstan-ignore-line

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Checking controllers...');
        $controllers = Finder::create()->files()->in(ProjectLocation::PROJECT_ROOT.'/PsrLib/Controller')->name('*.php');
        foreach ($controllers as $controller) {
            if (str_starts_with($controller->getFilename(), 'Abstract')) {
                continue;
            }
            $content = $this->readAndCleanFile($controller->getRealPath());

            $calls = $this->extractPhpRouteCalls($content);
            $this->checkRoute($calls, $controller, $io);
        }
        $io->success('Controllers checked.');

        $io->writeln('Checking tempates...');
        $templates = Finder::create()->files()->in(ProjectLocation::PROJECT_ROOT.'/templates')->name('*.twig');
        foreach ($templates as $template) {
            $content = $this->readAndCleanFile($template->getRealPath());
            $calls = $this->extractTwigRouteCalls($content);

            $this->checkRoute($calls, $template, $io);
        }

        return 0;
    }

    private function checkRoute(array $calls, \SplFileInfo $file, SymfonyStyle $io): void
    {
        foreach ($calls as $call) {
            $route = $this->routeCollection->get($call['route']);
            if (!$route) {
                $io->error(sprintf('Route "%s" not found in "%s"', $call['route'], $file->getRealPath()));
            }

            foreach ($call['params'] as $param) {
                $paramNames = array_map(fn (\ReflectionParameter $p) => $p->getName(), $route->getDefault('_controller_method_parameters'));
                if (!in_array($param, $paramNames)) {
                    $io->error(sprintf('Parameter "%s" not found in route "%s" in "%s"', $param, $call['route'], $file->getRealPath()));
                }
            }
        }
    }

    private function extractPhpRouteCalls(string $phpCode): array
    {
        $pattern = '/(redirectToRoute|generateUrl)\((.*?)\)(;|,)/';
        preg_match_all($pattern, $phpCode, $matches, PREG_SET_ORDER);
        $calls = [];
        foreach ($matches as $match) {
            preg_match_all('/\'(.*?)\'(,.*){0,1}/', $match[2], $matchName, PREG_SET_ORDER);
            $matchParams = [];
            if (isset($matchName[0][2])) {
                preg_match_all('/\'([^\)]+?)\'=>/', $matchName[0][2], $matchParams, PREG_SET_ORDER);
            }
            $calls[] = [
                'method' => $match[1],
                'route' => $matchName[0][1],
                'params' => array_map(fn ($a) => $a[1], $matchParams),
            ];
        }

        return $calls;
    }

    private function extractTwigRouteCalls(string $twigCode): array
    {
        $pattern = '/(path)\((.*?)\)/';
        preg_match_all($pattern, $twigCode, $matches, PREG_SET_ORDER);
        $calls = [];
        foreach ($matches as $match) {
            preg_match_all('/\'(.*?)\'(,.*){0,1}/', $match[2], $matchName, PREG_SET_ORDER);
            $matchParams = [];
            if (isset($matchName[0][2])) {
                preg_match_all('/([^{,]*?):/', $matchName[0][2], $matchParams, PREG_SET_ORDER);
            }

            $calls[] = [
                'method' => $match[1],
                'route' => $matchName[0][1],
                'params' => array_map(fn ($a) => $a[1], $matchParams),
            ];
        }

        return $calls;
    }

    /**
     * Read a file and remove all the new lines and spaces.
     */
    private function readAndCleanFile(string $path): string
    {
        $content = file_get_contents($path);
        $content = preg_replace('/\s/', ' ', $content);
        $content = str_replace(PHP_EOL, '', $content);

        return str_replace(' ', '', $content);
    }
}
