<?php

declare(strict_types=1);

namespace PsrLib\AliceProvider;

use PsrLib\ORM\Entity\UserEmail;

class UserEmailProvider
{
    public function user_email(string $email): UserEmail
    {
        $res = new UserEmail();
        $res->setEmail($email);

        return $res;
    }
}
