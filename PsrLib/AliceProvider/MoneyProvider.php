<?php

declare(strict_types=1);

namespace PsrLib\AliceProvider;

use Money\Money;

class MoneyProvider
{
    public function money(string $amount): Money
    {
        return Money::EUR($amount);
    }
}
