<?php

declare(strict_types=1);

namespace PsrLib\AliceProvider;

use PsrLib\ORM\Entity\Embeddable\Name;

class NameProvider
{
    public function name(string $prenom, string $nom): Name
    {
        $name = new Name();
        $name->setFirstName($prenom);
        $name->setLastName($nom);

        return $name;
    }
}
