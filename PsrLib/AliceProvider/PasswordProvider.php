<?php

declare(strict_types=1);

namespace PsrLib\AliceProvider;

use PsrLib\Services\Password\PasswordEncoder;

class PasswordProvider
{
    /**
     * Cache to avoid same password encoding multiple time.
     *
     * @var string[]
     */
    private array $encodedPasswordCache = [];

    public function __construct(private readonly PasswordEncoder $passwordEncoder)
    {
    }

    public function password_encode(string $password): string
    {
        if (isset($this->encodedPasswordCache[$password])) {
            return $this->encodedPasswordCache[$password];
        }

        $this->encodedPasswordCache[$password] = $this->passwordEncoder->encodePassword($password);

        return $this->encodedPasswordCache[$password];
    }
}
