<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\EvenementRelatedEntity;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\EvenementAbonnementRepository;
use PsrLib\Services\EntityBuilder\EvenementAbonnementDefaultFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EvenementAbonnementButtonRender
{
    public function __construct(
        private readonly EvenementAbonnementRepository $evenementAbonnementRepository,
        private readonly EvenementAbonnementDefaultFactory $abonnementDefaultFactory,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
    }

    public function renderButton(User $user, EvenementRelatedEntity $relatedEntity): string
    {
        $userAbonnements = $this->evenementAbonnementRepository->findByUser($user);
        if ($userAbonnements->hasAbonnement($user, $relatedEntity)) {
            return $this->renderButtonRemove($userAbonnements->getAbonnement($user, $relatedEntity));
        }

        $abonnements = $this->abonnementDefaultFactory->getDefaultForUser($user);
        if ($abonnements->hasAbonnement($user, $relatedEntity)) {
            return '<button class="btn btn-default" disabled="disabled">Par défaut</button>';
        }

        return $this->renderButtonAdd($relatedEntity);
    }

    private function renderButtonAdd(EvenementRelatedEntity $evenementRelatedEntity): string
    {
        return sprintf(
            '<button 
            hx-trigger="click"
            hx-swap="outerHTML"
            hx-disabled-elt="this"
            hx-post="%s"
            hx-vals=\'%s\'
            class="btn btn-success">Je veux suivre</button>',
            $this->urlGenerator->generate('evenement_abonnement_add'),
            json_encode(['type' => EvenementType::fromRelatedEntity($evenementRelatedEntity)->value, 'id' => $evenementRelatedEntity->getId()])
        );
    }

    private function renderButtonRemove(EvenementAbonnement $evenementAbonnement): string
    {
        return sprintf(
            '<button 
            hx-trigger="click"
            hx-swap="outerHTML"
            hx-disabled-elt="this"
            hx-post="%s"
            class="btn btn-danger">Je ne veux plus suivre</button>',
            $this->urlGenerator->generate('evenement_abonnement_remove', ['id' => $evenementAbonnement->getId()])
        );
    }
}
