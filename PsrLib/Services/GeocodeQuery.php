<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use PsrLib\DTO\GeocodeSuggestion;
use PsrLib\DTO\Nominatim\NominatimResponse;
use PsrLib\ORM\Repository\VilleRepository;
use PsrLib\Services\EntityBuilder\VilleFactory;

class GeocodeQuery
{
    public function __construct(private readonly NominatimClient $nominatimClient, private readonly VilleRepository $villeRepository, private readonly VilleFactory $villeFactory)
    {
    }

    /**
     * Make a geocoding request. Match with database city, and add to it if missing.
     *
     * @return GeocodeSuggestion[]
     */
    public function geocode_suggestions(string $query): array
    {
        $res = [];
        $addresses = $this->nominatimClient->geocode($query);

        $addresses = array_values(array_filter($addresses, fn (NominatimResponse $address) => null !== $address->getLat() && null !== $address->getLon()));

        foreach ($addresses as $address) {
            $ville = $this->villeRepository->getOneFromAddress($address);
            if (null === $ville) {
                $ville = $this->villeFactory->create($address);
                if (null === $ville) {
                    continue;
                }

                $this->villeRepository->add($ville, true);
            }

            $res[] = new GeocodeSuggestion(
                $address,
                $ville
            );
        }

        return $res;
    }
}
