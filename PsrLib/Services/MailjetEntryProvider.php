<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\DTO\Api\Mailjet\Entry;
use PsrLib\DTO\Api\Mailjet\Properties;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\UserRepository;

class MailjetEntryProvider
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly AmapRepository $amapRepository
    ) {
    }

    /**
     * @return Entry[]
     */
    public function getData()
    {
        $users = $this->userRepository->findBy([
            'newsletter' => true,
        ]);

        $entries = [];
        foreach ($users as $user) {
            $entryProperties = $this->generatePropertyFromUser($user);
            foreach ($user->getEmails() as $email) {
                $entries[] = new Entry(
                    $email->getEmail(),
                    $user->getName()->getFullNameInversed(),
                    $user->getUuid(),
                    $entryProperties
                );
            }
        }

        /** @var Amap[] $amaps */
        $amaps = $this->amapRepository->findAll();
        foreach ($amaps as $amap) {
            if (null === $amap->getEmail()) {
                continue;
            }
            $entryProperties = $this->generatePropertyFromAmap($amap);
            $entries[] = new Entry(
                $amap->getEmail(),
                $amap->getNom(),
                $amap->getUuid(),
                $entryProperties
            );
        }

        return $entries;
    }

    private function generatePropertyFromUser(User $user): Properties
    {
        $types = [];
        if ($user->isAmapAdmin()) {
            $types[] = Properties::TYPE_AMAP;
        }
        if ($user->isAmapien()) {
            $types[] = Properties::TYPE_AMAPIEN;
        }
        if ($user->isPaysan()) {
            $types[] = Properties::TYPE_PAYSAN;
        }
        $property = new Properties(implode('|', $types));

        $property->setNom($user->getName()->getLastName());
        $property->setPrenom($user->getName()->getFirstName());
        $property->setRegion((string) $user->getRegion());
        $property->setDepartement((string) $user->getDepartement());

        $property->setAdhesion(implode('|', $this->extractAdhesions($user)));
        $property->setAmapienAmap(implode('|', array_unique(array_merge($user->getAmapsAsAmapien(), $user->getAdminAmaps()->toArray()))));
        $property->setAmapEtudiante(implode('|', $user->getAdminAmaps()->map(fn (Amap $amap) => $amap->getAmapEtudiante() ? 'oui' : 'non')->toArray()));
        $property->setAmapAssurance(implode('|', $user->getAdminAmaps()->map(fn (Amap $amap) => $amap->isPossedeAssurance() ? 'oui' : 'non')->toArray()));
        $property->setTppPropose(implode('|', $this->extractTpPropose($user)));
        $property->setAmapTppRecherche(implode('|', $this->extractTpRecherche($user)));
        $property->setPaysanCertification($this->extractFermeCertification($user));
        $property->setPaysanDateInstallation($this->extractFermeDateInstallation($user));
        $property->setPaysanAdhesion(implode('|', $this->extractPaysanAdhesions($user)));
        $property->setAmapienRole(implode('|', $this->extractAmapienRole($user)));

        return $property;
    }

    private function generatePropertyFromAmap(Amap $amap): Properties
    {
        $property = new Properties(Properties::TYPE_AMAP);

        $property->setNom($amap->getNom());
        $property->setRegion((string) $amap->getRegion());
        $property->setDepartement((string) $amap->getDepartement());

        $property->setAdhesion(implode('|', $this->extractAdhesionsFromAmap($amap)));
        $property->setAmapEtudiante($amap->getAmapEtudiante() ? 'oui' : 'non');
        $property->setAmapAssurance($amap->isPossedeAssurance() ? 'oui' : 'non');
        $property->setTppPropose(implode('|', $this->extractTpProposeFromAmap($amap)));
        $property->setAmapTppRecherche(implode('|', $this->extractTpRechercheFromAmap($amap)));

        return $property;
    }

    private function extractAdhesionsFromAmap(Amap $amap): array
    {
        $adhesions = [];
        foreach ($amap->getAnneeAdhesions() as $anneeAdhesion) {
            $adhesions[] = $anneeAdhesion->getAnnee();
        }
        sort($adhesions);

        return $adhesions;
    }

    private function extractAdhesions(User $user): array
    {
        $adhesions = [];
        foreach ($user->getAdminAmaps() as $amap) {
            $adhesions = array_merge($adhesions, $this->extractAdhesionsFromAmap($amap));
        }
        sort($adhesions);

        return array_unique($adhesions);
    }

    private function extractTpProposeFromAmap(Amap $amap): array
    {
        $tps = [];
        foreach ($amap->getTpPropose() as $tpPropose) {
            $tps[] = $tpPropose->getNom();
        }
        sort($tps);

        return $tps;
    }

    private function extractTpPropose(User $user): array
    {
        $tps = [];
        foreach ($user->getAdminAmaps() as $amap) {
            $tps = array_merge($tps, $this->extractTpProposeFromAmap($amap));
        }
        foreach ($user->getFermesAsPaysan() as $ferme) {
            foreach ($ferme->getTypeProductionProposes() as $tpPropose) {
                $tps[] = $tpPropose->getTypeProduction()->getNom();
            }
        }
        sort($tps);

        return array_unique($tps);
    }

    private function extractTpRechercheFromAmap(Amap $amap): array
    {
        $tps = [];
        foreach ($amap->getTpRecherche() as $tpRecherche) {
            $tps[] = $tpRecherche->getNom();
        }
        sort($tps);

        return array_unique($tps);
    }

    private function extractTpRecherche(User $user): array
    {
        $tps = [];
        foreach ($user->getAdminAmaps() as $amap) {
            $tps = array_merge($tps, $this->extractTpRechercheFromAmap($amap));
        }
        sort($tps);

        return array_unique($tps);
    }

    private function extractFermeCertification(User $user): string
    {
        $certifications = [];
        foreach ($user->getFermesAsPaysan() as $ferme) {
            $certifications[] = $ferme->getCertification();
        }

        return implode('|', $certifications);
    }

    private function extractFermeDateInstallation(User $user): string
    {
        $dates = [];
        foreach ($user->getFermesAsPaysan() as $ferme) {
            if (null !== $ferme->getDateInstallation()) {
                $dates[] = $ferme->getDateInstallation()->format('d/m/Y');
            }
        }

        return implode('|', $dates);
    }

    private function extractPaysanAdhesions(User $user): array
    {
        $adhesions = [];
        foreach ($user->getFermesAsPaysan() as $ferme) {
            foreach ($ferme->getAnneeAdhesions() as $anneeAdhesion) {
                $adhesions[] = $anneeAdhesion->getAnnee();
            }
        }
        sort($adhesions);

        return array_unique($adhesions);
    }

    private function extractAmapienRole(User $user): array
    {
        $roles = [];
        foreach ($user->getAmapienAmaps() as $amapien) {
            if ($amapien === $amapien->getAmap()->getAmapienRefReseau()) {
                $roles[] = 'correspondant_reseau';
            }
            if ($amapien === $amapien->getAmap()->getAmapienRefReseauSecondaire()) {
                $roles[] = 'correspondant_reseau_sec';
            }
            if ($amapien->isRefProduit()) {
                $roles[] = 'ref_produit';
            }
        }
        sort($roles);

        return array_unique($roles);
    }
}
