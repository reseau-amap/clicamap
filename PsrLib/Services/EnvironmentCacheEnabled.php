<?php

declare(strict_types=1);

namespace PsrLib\Services;

class EnvironmentCacheEnabled
{
    public static function isCacheEnabled(): bool
    {
        return 'true' === getenv('CACHE_ENABLE');
    }
}
