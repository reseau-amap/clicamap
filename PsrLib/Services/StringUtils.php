<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Symfony\Component\String\Slugger\AsciiSlugger;

class StringUtils
{
    public function slugToFileName(string $input): string
    {
        $exploded = explode('.', $input);
        $fileName = $exploded[0] ?? '';
        $extension = $exploded[1] ?? '';

        $slugger = new AsciiSlugger('fr');
        $slugged = $slugger->slug($fileName, ' ', 'fr')->lower()->truncate(100)->toString();

        return $slugged.'.'.$extension;
    }
}
