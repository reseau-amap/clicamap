<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\HttpFoundation\Request;

class RequestFactory
{
    public function create(): Request
    {
        return \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    }
}
