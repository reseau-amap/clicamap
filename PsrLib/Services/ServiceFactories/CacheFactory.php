<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\CacheInterface;

class CacheFactory
{
    final public const CACHE_LOCATION = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/sf/';

    public function create(): CacheInterface
    {
        return new FilesystemAdapter(
            'phpdi_cache',
            0,
            self::CACHE_LOCATION
        );
    }
}
