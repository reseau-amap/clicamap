<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class UrlMatcherFactory
{
    public function create(RouteCollection $routeCollection, RequestContext $context): UrlMatcher
    {
        return new UrlMatcher($routeCollection, $context);
    }
}
