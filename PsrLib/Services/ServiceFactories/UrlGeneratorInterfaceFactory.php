<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class UrlGeneratorInterfaceFactory
{
    public function create(RouteCollection $routeCollection, RequestContext $requestContext, Request $request): UrlGeneratorInterface
    {
        return new UrlGenerator($routeCollection, $requestContext->fromRequest($request));
    }
}
