<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\DoctrineDbalStore;

class LockFactoryFactory
{
    public function __construct(
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function create(): LockFactory
    {
        return new LockFactory(
            new DoctrineDbalStore($this->em->getConnection())
        );
    }
}
