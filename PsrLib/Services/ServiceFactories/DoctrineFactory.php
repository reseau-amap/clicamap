<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\ServiceFactories;

use Carbon\Doctrine\CarbonImmutableType;
use Carbon\Doctrine\CarbonType;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AttributeDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use PsrLib\ORM\EventSubscriber\AdhesionAmapienPostGenerateSubscriber;
use PsrLib\ORM\EventSubscriber\AdhesionAmapPostGenerateSubscriber;
use PsrLib\ORM\EventSubscriber\AdhesionFermePostGenerateSubscriber;
use PsrLib\ORM\Type\MoneyType;
use PsrLib\ORM\Type\TimeType;
use PsrLib\ProjectLocation;
use PsrLib\Services\EnvironmentCacheEnabled;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\PhpFilesAdapter;

class DoctrineFactory
{
    public function create(
        AdhesionAmapPostGenerateSubscriber $adhesionAmapPostGenerateSubscriber,
        AdhesionFermePostGenerateSubscriber $adhesionFermePostGenerateSubscriber,
        AdhesionAmapienPostGenerateSubscriber $adhesionAmapienPostGenerateSubscriber
    ): EntityManagerInterface {
        AnnotationReader::addGlobalIgnoredName('mixin');
        AnnotationReader::addGlobalIgnoredName('alias');

        if (!Type::hasType('money')) {
            Type::addType('money', MoneyType::class);
        }
        if (!Type::hasType('carbon')) {
            Type::addType('carbon', CarbonType::class);
        }
        if (!Type::hasType('carbon_immutable')) {
            Type::addType('carbon_immutable', CarbonImmutableType::class);
        }
        if (!Type::hasType('time_custom')) {
            Type::addType('time_custom', TimeType::class);
        }

        // AnnotationRegistry::registerLoader('class_exists');

        $isProductionSetup = EnvironmentCacheEnabled::isCacheEnabled();

        $baseCacheFolder = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/doctrine';
        $queryCache = new ArrayAdapter();
        $metadataCache = new ArrayAdapter();
        if ($isProductionSetup) {
            $queryCache = new PhpFilesAdapter('doctrine_queries', 0, $baseCacheFolder.'/query');
            $metadataCache = new PhpFilesAdapter('doctrine_metadata', 0, $baseCacheFolder.'/metadata');
        }

        $config = new Configuration();
        $config->setMetadataCache($metadataCache);
        $driverImpl = new AttributeDriver([ProjectLocation::PROJECT_ROOT.'/PsrLib/ORM/Entity']);
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCache($queryCache);
        $config->setProxyDir($baseCacheFolder.'/proxy');
        $config->setProxyNamespace('PsrLib\Proxies');
        $namingStrategy = new UnderscoreNamingStrategy(CASE_LOWER);
        $config->setNamingStrategy($namingStrategy);

        $config->addFilter('soft-deleteable', SoftDeleteableFilter::class);

        if ($isProductionSetup) {
            $config->setAutoGenerateProxyClasses(false);
        } else {
            $config->setAutoGenerateProxyClasses(true);
        }

        $connection = DriverManager::getConnection([
            'driver' => 'pdo_mysql',
            'user' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'host' => $_ENV['DB_HOSTNAME'],
            'dbname' => $_ENV['DB_DATABASE'],
            'charset' => 'UTF8',
        ], $config);

        $em = new EntityManager($connection, $config);

        $evm = $em->getEventManager();
        $evm->addEventSubscriber($adhesionAmapPostGenerateSubscriber);
        $evm->addEventSubscriber($adhesionFermePostGenerateSubscriber);
        $evm->addEventSubscriber($adhesionAmapienPostGenerateSubscriber);
        $evm->addEventSubscriber(new SoftDeleteableListener());

        $em->getFilters()->enable('soft-deleteable'); // Auto hide all deleted entities

        return $em;
    }
}
