<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Renderer\TwigRenderer;
use PsrLib\Services\EntityBuilder\KnpMenuFactory;
use PsrLib\Twig\KnpMenuExtension;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;

// Custom factory used to avoid circular reference
class KnpMenuExtensionFactory
{
    public function __construct(
        private readonly KnpMenuFactory $knpMenuFactory,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly Matcher $matcher
    ) {
    }

    public function create(Environment $environment): KnpMenuExtension
    {
        $renderer = new TwigRenderer(
            $environment,
            'menu/template_main.html.twig',
            $this->matcher
        );

        return new KnpMenuExtension(
            $this->knpMenuFactory,
            $renderer,
            $this->tokenStorage
        );
    }
}
