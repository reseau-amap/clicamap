<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;

class RequestContextFactory
{
    public function create(Request $request): RequestContext
    {
        $context = new RequestContext();
        $context->fromRequest($request);

        return $context;
    }
}
