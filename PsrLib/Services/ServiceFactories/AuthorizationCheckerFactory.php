<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AuthorizationCheckerFactory
{
    public function create(
        TokenStorageInterface $tokenStorage,
        AccessDecisionManagerInterface $accessDecisionManager
    ): AuthorizationCheckerInterface {
        return new AuthorizationChecker(
            $tokenStorage,
            $accessDecisionManager,
            false,
            false,
        );
    }
}
