<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use PsrLib\Services\Security\Voters\ActualiteManageVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapAmapienVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapienVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapVoter;
use PsrLib\Services\Security\Voters\AdhesionCreatorUserVoter;
use PsrLib\Services\Security\Voters\AdhesionFermeVoter;
use PsrLib\Services\Security\Voters\AjaxVoter;
use PsrLib\Services\Security\Voters\AmapienVoter;
use PsrLib\Services\Security\Voters\AmapLivraisonLieuVoter;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\CampagneBulletinDownloadVoter;
use PsrLib\Services\Security\Voters\CampagneBulletinImportVoter;
use PsrLib\Services\Security\Voters\CampagneBulletinRecuActionHeaderVoter;
use PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter;
use PsrLib\Services\Security\Voters\CampagneBulletinRecuDownloadVoter;
use PsrLib\Services\Security\Voters\CampagneEditVoter;
use PsrLib\Services\Security\Voters\CampagneListAdminVoter;
use PsrLib\Services\Security\Voters\CampagneListAmapienVoter;
use PsrLib\Services\Security\Voters\CampagneListVoter;
use PsrLib\Services\Security\Voters\CampagneNewVoter;
use PsrLib\Services\Security\Voters\CampagneSubscriptionVoter;
use PsrLib\Services\Security\Voters\CdpAuthVoter;
use PsrLib\Services\Security\Voters\ContactVoter;
use PsrLib\Services\Security\Voters\ContractAmapienValidationVoter;
use PsrLib\Services\Security\Voters\ContractVoter;
use PsrLib\Services\Security\Voters\DistributionVoter;
use PsrLib\Services\Security\Voters\DocumentUtilisateurDownloadVoter;
use PsrLib\Services\Security\Voters\DocumentUtilisateurEditVoter;
use PsrLib\Services\Security\Voters\DocumentUtilisateurManageAdminVoter;
use PsrLib\Services\Security\Voters\DocumentVoter;
use PsrLib\Services\Security\Voters\EvenementAbonnementListVoter;
use PsrLib\Services\Security\Voters\EvenementAddVoter;
use PsrLib\Services\Security\Voters\EvenementDisplayVoter;
use PsrLib\Services\Security\Voters\EvenementEditVoter;
use PsrLib\Services\Security\Voters\EvenementListVoter;
use PsrLib\Services\Security\Voters\EvenementRemoveVoter;
use PsrLib\Services\Security\Voters\FermeProduitVoter;
use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use PsrLib\Services\Security\Voters\FermeVoter;
use PsrLib\Services\Security\Voters\ImportVoter;
use PsrLib\Services\Security\Voters\MailshotVoter;
use PsrLib\Services\Security\Voters\ModelContratCopyVoter;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use PsrLib\Services\Security\Voters\NetworkVoter;
use PsrLib\Services\Security\Voters\PaysanVoter;
use PsrLib\Services\Security\Voters\UserPasswordStrengthVoter;
use PsrLib\Services\Security\Voters\UserTypeVoter;
use PsrLib\Services\Security\Voters\UserVoter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class AccessDecisionManagerFactory
{
    public function create(
        AdhesionAmapAmapienVoter $adhesionAmapAmapienVoter,
        AdhesionAmapienVoter $adhesionAmapienVoter,
        AdhesionAmapVoter $adhesionAmapVoter,
        AdhesionFermeVoter $adhesionFermeVoter,
        AdhesionCreatorUserVoter $adhesionVoter,
        AjaxVoter $ajaxVoter,
        AmapienVoter $amapienVoter,
        AmapLivraisonLieuVoter $amapLivraisonLieuVoter,
        AmapVoter $amapVoter,
        ContactVoter $contactVoter,
        ContractVoter $contractVoter,
        DistributionVoter $distributionVoter,
        DocumentVoter $documentVoter,
        FermeRegroupementVoter $fermeRegroupementVoter,
        FermeVoter $fermeVoter,
        ImportVoter $importVoter,
        ModelContratVoter $modelContratVoter,
        NetworkVoter $networkVoter,
        PaysanVoter $paysanVoter,
        UserTypeVoter $userTypeVoter,
        UserVoter $userVoter,
        MailshotVoter $mailshotVoter,
        UserPasswordStrengthVoter $userPasswordStrengthVoter,
        ModelContratCopyVoter $modelContratCopyVoter,
        CampagneListVoter $campagneListVoter,
        CampagneNewVoter $campagneNewVoter,
        CampagneEditVoter $campagneEditVoter,
        CampagneListAmapienVoter $campagneListAmapienVoter,
        CampagneSubscriptionVoter $campagneSubscriptionVoter,
        CampagneBulletinDownloadVoter $campagneBulletinDownloadVoter,
        CampagneBulletinRecuDownloadVoter $campagneBulletinRecuDownloadVoter,
        CampagneBulletinRecuActionHeaderVoter $campagneBulletinRecuActionheaderVoter,
        CampagneBulletinRecuActionVoter $campagneBulletinRecuActionVoter,
        CampagneListAdminVoter $campagneListAdminVoter,
        DocumentUtilisateurManageAdminVoter $documentUtilisateurManageAdminVoter,
        DocumentUtilisateurEditVoter $documentUtilisateurEditVoter,
        DocumentUtilisateurDownloadVoter $documentUtilisateurDownloadVoter,
        CampagneBulletinImportVoter $campagneBulletinImportVoter,
        FermeProduitVoter $fermeProduitVoter,
        CdpAuthVoter $cdpAuthVoter,
        ActualiteManageVoter $actualiteManageVoter,
        EvenementEditVoter $evenementEditVoter,
        EvenementDisplayVoter $evenementDisplayVoter,
        EvenementListVoter $evenementListVoter,
        EvenementAddVoter $evenementAddVoter,
        EvenementAbonnementListVoter $evenementAbonnementListVoter,
        EvenementRemoveVoter $evenementRemoveVoter,
        ContractAmapienValidationVoter $contractAmapienValidationVoter,
    ): AccessDecisionManagerInterface {
        return new AccessDecisionManager([
            $adhesionAmapAmapienVoter,
            $adhesionAmapienVoter,
            $adhesionAmapVoter,
            $adhesionFermeVoter,
            $adhesionVoter,
            $ajaxVoter,
            $amapienVoter,
            $amapLivraisonLieuVoter,
            $amapVoter,
            $contactVoter,
            $contractVoter,
            $distributionVoter,
            $documentVoter,
            $fermeRegroupementVoter,
            $fermeVoter,
            $importVoter,
            $modelContratVoter,
            $networkVoter,
            $paysanVoter,
            $userTypeVoter,
            $userVoter,
            $mailshotVoter,
            $userPasswordStrengthVoter,
            $modelContratCopyVoter,
            $campagneListVoter,
            $campagneNewVoter,
            $campagneEditVoter,
            $campagneListAmapienVoter,
            $campagneSubscriptionVoter,
            $campagneBulletinDownloadVoter,
            $campagneBulletinRecuDownloadVoter,
            $campagneBulletinRecuActionheaderVoter,
            $campagneBulletinRecuActionVoter,
            $campagneListAdminVoter,
            $documentUtilisateurManageAdminVoter,
            $documentUtilisateurEditVoter,
            $documentUtilisateurDownloadVoter,
            $campagneBulletinImportVoter,
            $fermeProduitVoter,
            $cdpAuthVoter,
            $actualiteManageVoter,
            $evenementEditVoter,
            $evenementDisplayVoter,
            $evenementListVoter,
            $evenementAddVoter,
            $evenementAbonnementListVoter,
            $evenementRemoveVoter,
            $contractAmapienValidationVoter,
        ]);
    }
}
