<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use PsrLib\Services\EnvironmentCacheEnabled;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;

class TranslatorFactory
{
    public function create(): Translator
    {
        $translator = new Translator(
            'fr',
            null,
            ProjectLocation::PROJECT_ROOT.'application/writable/cache/translations',
            !EnvironmentCacheEnabled::isCacheEnabled()
        );
        $translator->addLoader('xlf', new XliffFileLoader());
        $translator->addLoader('yaml', new YamlFileLoader());

        $translator->addResource('xlf', ProjectLocation::PROJECT_ROOT.'vendor/symfony/validator/Resources/translations/validators.fr.xlf', 'fr');

        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/modele_contrat_etat.fr.yaml',
            'fr',
            'modele_contrat_etat'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/modele_contrat_reglement_type.fr.yaml',
            'fr',
            'modele_contrat_reglement_type'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/ferme_produit_certification.fr.yaml',
            'fr',
            'ferme_produit_certification'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/campagne_etat.fr.yaml',
            'fr',
            'campagne_etat'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/amap_livraison_horaire_jour.fr.yaml',
            'fr',
            'amap_livraison_horaire_jour'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/user_role.fr.yaml',
            'fr',
            'user_role'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/adhesion_label.fr.yaml',
            'fr',
            'adhesion_label'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/collectif_profil.fr.yaml',
            'fr',
            'collectif_profil'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/contrat_paiement_statut.fr.yaml',
            'fr',
            'contrat_paiement_statut'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/evenement_add_type.fr.yaml',
            'fr',
            'evenement_add_type'
        );
        $translator->addResource(
            'yaml',
            ProjectLocation::PROJECT_ROOT.'translations/amap_etat.fr.yaml',
            'fr',
            'amap_etat'
        );

        return $translator;
    }
}
