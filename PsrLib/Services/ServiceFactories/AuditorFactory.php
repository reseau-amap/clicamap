<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use DH\Auditor\Auditor;
use DH\Auditor\Configuration;
use DH\Auditor\Provider\Doctrine\DoctrineProvider;
use DH\Auditor\Provider\Doctrine\Service\AuditingService;
use DH\Auditor\Provider\Doctrine\Service\StorageService;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\Services\Authentification;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AuditorFactory
{
    public function create(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher, Authentification $authentification): Auditor
    {
        $auditor = new Auditor(
            new Configuration([
                'enabled' => true,
                'timezone' => 'UTC',
                'role_checker' => null,
                'user_provider' => fn () => $authentification->getCurrentUser(),
                'security_provider' => fn () => [$_SERVER['REMOTE_ADDR'] ?? null, null],
            ]),
            $eventDispatcher
        );

        $doctrineProvider = new DoctrineProvider(
            new \DH\Auditor\Provider\Doctrine\Configuration([
                'table_prefix' => '_audit_',
                'table_suffix' => '',
                'viewer' => false,
            ])
        );
        $doctrineProvider->registerAuditingService(new AuditingService('default', $em));
        $doctrineProvider->registerStorageService(new StorageService('default', $em));
        $auditor->registerProvider($doctrineProvider);

        return $auditor;
    }
}
