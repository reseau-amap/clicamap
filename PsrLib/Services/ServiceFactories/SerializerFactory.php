<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\ServiceFactories;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use PsrLib\Serializer\AmapWizardAmapLogoDenormalizer;
use PsrLib\Serializer\AmapWizardTypeProductionDenormalizer;
use PsrLib\Serializer\AmapWizardUserDenormalizer;
use PsrLib\Serializer\AmapWizardVilleDenormalizer;
use PsrLib\Serializer\CampagneAmapNormalizer;
use PsrLib\Serializer\CampagneBulletinAmapienNormalizer;
use PsrLib\Serializer\CampagneBulletinCampagneNormalizer;
use PsrLib\Serializer\CampagneBulletinImportLineDTONormalizer;
use PsrLib\Serializer\CampagneBulletinMontantLibreCampagneMontantNormalizer;
use PsrLib\Serializer\CampagneBulletinPaiementCarbonNormalizer;
use PsrLib\Serializer\CampagneLogoNormalizer;
use PsrLib\Serializer\CampagnePeriodNormalizer;
use PsrLib\Serializer\CampagneWizardDTONormalizer;
use PsrLib\Serializer\ContratCelluleNormalizer;
use PsrLib\Serializer\ContratDatesReglementNormalizer;
use PsrLib\Serializer\ContratNormalizer;
use PsrLib\Serializer\ContratNormalizerUserAmapienAmap;
use PsrLib\Serializer\EvenementAbonnableDTODatatableNormalizer;
use PsrLib\Serializer\ExcelDecoder;
use PsrLib\Serializer\ExcelEncoder;
use PsrLib\Serializer\GeocodeSuggestionAjaxNormalizer;
use PsrLib\Serializer\ModeleContratDeplacementNormalizer;
use PsrLib\Serializer\ModeleContratNormalizer;
use PsrLib\Serializer\ModeleContratProduitExclureNormalizer;
use PsrLib\Serializer\ModeleContratProduitNormalizer;
use PsrLib\Serializer\MoneyDenormalizer;
use PsrLib\Serializer\MoneyNormalizer;
use PsrLib\Serializer\PrixTVANormalizer;
use PsrLib\Serializer\SimpleCarbonDenormalizer;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as BaseSerializer;

class SerializerFactory
{
    public function create(
        MoneyNormalizer $moneyNormalizer,
        MoneyDenormalizer $moneyDenormalizer,
        SimpleCarbonDenormalizer $simpleCarbonDenormalizer,
        ModeleContratNormalizer $modeleContratNormalizer,
        ModeleContratProduitNormalizer $modeleContratProduitNormalizer,
        ModeleContratProduitExclureNormalizer $modeleContratProduitExclureNormalizer,
        ContratNormalizer $contratNormalizer,
        ContratCelluleNormalizer $contratCelluleNormalizer,
        ContratDatesReglementNormalizer $contratDatesReglementNormalizer,
        ModeleContratDeplacementNormalizer $contratDeplacementNormalizer,
        GeocodeSuggestionAjaxNormalizer $geocodeSuggestionAjaxNormalizer,
        ContratNormalizerUserAmapienAmap $contratNormalizerUserAmapienAmap,
        PrixTVANormalizer $prixTVANormalizer,
        AmapWizardVilleDenormalizer $amapWizardVilleDenormalizer,
        AmapWizardTypeProductionDenormalizer $amapWizardTypeProductionDenormalizer,
        AmapWizardUserDenormalizer $amapWizardUserDenormalizer,
        AmapWizardAmapLogoDenormalizer $amapWizardAmapLogoDenormalizer,
        CampagneAmapNormalizer $campagneAmapNormalizer,
        CampagnePeriodNormalizer $campagnePeriodNormalizer,
        CampagneWizardDTONormalizer $campagneWizardDTONormalizer,
        CampagneBulletinAmapienNormalizer $campagneBulletinAmapienNormalizer,
        CampagneBulletinCampagneNormalizer $campagneBulletinCampagneNormalizer,
        CampagneBulletinMontantLibreCampagneMontantNormalizer $campagneBulletinMontantLibreCampagneMontantNormalizer,
        CampagneLogoNormalizer $campagneLogoNormalizer,
        CampagneBulletinPaiementCarbonNormalizer $campagneBulletinPaiementCarbonNormalizer,
        CampagneBulletinImportLineDTONormalizer $campagneBulletinImportLineDTONormalizer,
        EvenementAbonnableDTODatatableNormalizer $evenemntAbonnableDTODatatableNormalizer,
    ): BaseSerializer {
        AnnotationRegistry::registerLoader('class_exists');
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $listExtractors = [$reflectionExtractor];
        $typeExtractors = [$phpDocExtractor, $reflectionExtractor];
        $descriptionExtractors = [$phpDocExtractor];
        $accessExtractors = [$reflectionExtractor];
        $propertyInitializableExtractors = [$reflectionExtractor];

        $propertyInfo = new PropertyInfoExtractor(
            $listExtractors,
            $typeExtractors,
            $descriptionExtractors,
            $accessExtractors,
            $propertyInitializableExtractors
        );

        // Fix carbon compatibility
        AnnotationReader::addGlobalIgnoredName('alias');

        $dateTimeNormalizer = new DateTimeNormalizer([
            DateTimeNormalizer::TIMEZONE_KEY => date_default_timezone_get(),
        ]);

        return new BaseSerializer(
            [
                $campagneBulletinPaiementCarbonNormalizer,
                $moneyNormalizer,
                $dateTimeNormalizer,
                $moneyDenormalizer,
                $simpleCarbonDenormalizer,
                $modeleContratNormalizer,
                $modeleContratProduitNormalizer,
                $modeleContratProduitExclureNormalizer,
                $contratNormalizer,
                $contratCelluleNormalizer,
                $contratDatesReglementNormalizer,
                $contratDeplacementNormalizer,
                $geocodeSuggestionAjaxNormalizer,
                $contratNormalizerUserAmapienAmap,
                $prixTVANormalizer,
                $amapWizardVilleDenormalizer,
                $amapWizardTypeProductionDenormalizer,
                $amapWizardUserDenormalizer,
                $amapWizardAmapLogoDenormalizer,
                $campagneAmapNormalizer,
                $campagnePeriodNormalizer,
                $campagneWizardDTONormalizer,
                $campagneBulletinAmapienNormalizer,
                $campagneBulletinCampagneNormalizer,
                $campagneBulletinMontantLibreCampagneMontantNormalizer,
                $campagneLogoNormalizer,
                $campagneBulletinImportLineDTONormalizer,
                $evenemntAbonnableDTODatatableNormalizer,
                new BackedEnumNormalizer(),
                new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter, null, $propertyInfo),
                new ArrayDenormalizer(),
            ],
            [new ExcelDecoder(), new ExcelEncoder(), new JsonEncoder()]
        );
    }
}
