<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class PropertyAccessorFactory
{
    public function create(): PropertyAccessor
    {
        return PropertyAccess::createPropertyAccessor();
    }
}
