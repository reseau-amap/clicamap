<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use PsrLib\Services\Routing\CodeIgniterAnnotationClassLoader;
use Symfony\Component\Routing\RouteCollection;

class RouteCollectionFactory
{
    public function create(CodeIgniterAnnotationClassLoader $annotationClassLoader): RouteCollection
    {
        $routeLoader = new \Symfony\Component\Routing\Loader\AnnotationDirectoryLoader(
            new \Symfony\Component\Config\FileLocator([]),
            $annotationClassLoader
        );

        return $routeLoader->load(\PsrLib\ProjectLocation::PROJECT_ROOT.'/PsrLib/Controller');
    }
}
