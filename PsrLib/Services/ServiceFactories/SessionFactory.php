<?php

declare(strict_types=1);

namespace PsrLib\Services\ServiceFactories;

use PsrLib\ProjectLocation;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NullSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class SessionFactory
{
    public function create(): Session
    {
        $fileSessionHandler = new NullSessionHandler();
        if (PHP_SAPI !== 'cli') {
            $fileSessionHandler = new NativeFileSessionHandler(ProjectLocation::PROJECT_ROOT.'/application/writable/session');
        }
        $sessionStorage = new NativeSessionStorage([], $fileSessionHandler);

        $session = new Session($sessionStorage);
        if (PHP_SAPI !== 'cli') {
            $session->start();
        }

        return $session;
    }
}
