<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Psr\Container\ContainerInterface;
use PsrLib\ORM\Entity\User;

/**
 * Mockable authentification used for unit testing.
 */
class AuthentificationMockable
{
    private ?\PsrLib\ORM\Entity\User $mockedUser = null;

    public function __construct(private readonly ContainerInterface $container)
    {
    }

    public function mockUser(?User $user = null): void
    {
        $this->mockedUser = $user;
    }

    /**
     * @return User|null
     */
    public function getCurrentUser()
    {
        if (null !== $this->mockedUser) {
            return $this->mockedUser;
        }

        return $this->container->get(Authentification::class)->getCurrentUser();
    }
}
