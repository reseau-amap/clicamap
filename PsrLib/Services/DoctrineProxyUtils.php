<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Doctrine\Persistence\Proxy;

class DoctrineProxyUtils
{
    /**
     * Get real entity class for entity
     * Remote proxy class if exists.
     */
    public static function getRealClass(object $object): string
    {
        $objectClass = $object::class;
        $reflectionClass = new \ReflectionClass($objectClass);

        // Make sure we are not using a Proxy class
        if ($object instanceof Proxy) {
            $reflectionParentClass = $reflectionClass->getParentClass();
            if (false !== $reflectionParentClass) {
                return $reflectionParentClass->getName();
            }
        }

        return $object::class;
    }
}
