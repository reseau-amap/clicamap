<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Carbon\CarbonImmutable;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token;
use Ramsey\Uuid\Uuid;

class JwtToken
{
    /**
     * @var Configuration
     */
    private $configuration;

    public function __construct()
    {
        $this->configuration = Configuration::forAsymmetricSigner(
            new Sha256(),
            InMemory::file(__DIR__.'/../../application/writable/keys/jwtCdp.key'),
            InMemory::file(__DIR__.'/../../application/writable/keys/jwtCdp.pub')
        );
    }

    public function buildToken(string $sub, array $payload): Token
    {
        $now = CarbonImmutable::now();

        $token = $this
            ->configuration
            ->builder()
            ->issuedBy('https://www.clicamap.org')
            ->identifiedBy(Uuid::uuid4()->toString())
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now)
            ->expiresAt($now->addHour())
            ->relatedTo($sub)
        ;

        foreach ($payload as $key => $value) {
            $token = $token->withClaim($key, $value);
        }

        return $token->getToken($this->configuration->signer(), $this->configuration->signingKey());
    }

    public function parseToken(string $token): Token
    {
        return $this->configuration->parser()->parse($token);
    }
}
