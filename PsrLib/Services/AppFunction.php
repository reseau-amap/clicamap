<?php

declare(strict_types=1);

namespace PsrLib\Services;

class AppFunction
{
    // Nettoyer la chaîne de caractère
    public function caracteres_speciaux(mixed $chaine, string $charset = 'utf-8'): string
    {
        $chaine = htmlentities((string) $chaine, ENT_NOQUOTES, $charset);
        $chaine = trim($chaine);
        $chaine = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $chaine); // Enlève les accents
        $chaine = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $chaine); // pour les ligatures (le e dans le o)
        $chaine = preg_replace('#&[^;]+;#', '', $chaine); // supprime les autres caractères
        $chaine = preg_replace('/[^A-Za-z0-9]+/', '-', $chaine); // On remplace les caracteres non-alphanumériques par le tiret
        $chaine = strtolower($chaine); // On convertit le tout en minuscules

        return trim($chaine, '-'); // Supprime les tirets en début ou en fin de chaine
    }
}
