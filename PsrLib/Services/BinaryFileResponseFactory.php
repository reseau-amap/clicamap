<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\ORM\Entity\Files\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use function Symfony\Component\String\u;

class BinaryFileResponseFactory
{
    public static function cleanFileName(string $in): string
    {
        $out = (string) u($in)->ascii();

        return str_replace(['/', '\\', '%'], '_', $out);
    }

    public static function createFromFile(File $file, ?string $fileName = null): BinaryFileResponse
    {
        $response = new BinaryFileResponse($file->getFileFullPath());

        if (null === $fileName) {
            $fileName = $file->getOriginalFileName() ?? $file->getFileName();
        }
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, self::cleanFileName($fileName));

        return $response;
    }

    /**
     * Warning: the response will remove the file after sending it.
     */
    public static function createFromTempFilePath(string|\SplFileInfo $path, string $fileName, string $disposition = ResponseHeaderBag::DISPOSITION_ATTACHMENT): BinaryFileResponse
    {
        if ($path instanceof \SplFileInfo) {
            $path = $path->getPathname();
        }
        $response = new BinaryFileResponse($path);
        $response->setContentDisposition($disposition, self::cleanFileName($fileName));
        $response->deleteFileAfterSend(true);

        return $response;
    }
}
