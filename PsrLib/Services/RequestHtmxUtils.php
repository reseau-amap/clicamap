<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Various utils to help use of htmx in backend.
 */
class RequestHtmxUtils
{
    public static function isHtmxRequest(Request $request): bool
    {
        return 'true' === $request->headers->get('HX-Request', '');
    }

    public static function isFieldTriggeredHtmx(Request $request, FormInterface $form): bool
    {
        $fullFieldName = [];
        while (null !== $form->getParent()) {
            $fullFieldName[] = sprintf('[%s]', $form->getPropertyPath()->getElement(0));
            $form = $form->getParent();
        }
        $fullFieldName[] = $form->getName();
        $fullFieldName = implode('', array_reverse($fullFieldName));

        return $fullFieldName === $request->headers->get('HX-Trigger-Name', '');
    }
}
