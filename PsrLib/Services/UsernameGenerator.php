<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Symfony\Component\String\Slugger\AsciiSlugger;

class UsernameGenerator
{
    private readonly AsciiSlugger $slugger;

    public function __construct()
    {
        $this->slugger = new AsciiSlugger('fr', ['fr' => ['.' => '.']]);
    }

    public function generate(?string $prenom, ?string $nom): string
    {
        $prenom = null === $prenom ? null : $this->slugger->slug($prenom);
        $nom = null === $nom ? null : $this->slugger->slug($nom);
        $username = mb_strtolower($prenom.'.'.$nom);

        return rtrim($username, '.');
    }
}
