<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use Psr\Log\AbstractLogger;
use PsrLib\DTO\ContratDistributionExport;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;
use PsrLib\ProjectLocation;
use Ramsey\Uuid\Uuid;
use Twig\Environment;

class MPdfGeneration
{
    private readonly \Mpdf\Mpdf $mPdf;

    /**
     * @var \CI_Loader
     */
    private $ciLoader;

    /**
     * @var \CI_Config
     */
    private $ciConfig;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly Environment $twig,
        \CI_Loader $ciLoader,
        \CI_Config $config
    ) {
        $this->ciLoader = $ciLoader;
        $this->ciConfig = $config;

        // Override backtrack setting to avoid rendering errors
        ini_set('pcre.backtrack_limit', '10000000000');

        $this->mPdf = $this->buildMpdf();

        // DEBUG log
        $this->mPdf->setLogger(new class() extends AbstractLogger {
            public function log($level, $message, array $context = []): void
            {
                $date = new \DateTime();
                file_put_contents(
                    ProjectLocation::PROJECT_ROOT.'/application/logs/mpdf.log',
                    $date->format(DATE_ATOM).'-'.$level.': '.$message.PHP_EOL,
                    FILE_APPEND
                );
            }
        });
    }

    public function genererContratVierge(ModeleContrat $mc): \SplFileInfo
    {
        $exclusions = $this->orderExclusionByProduitDate(
            $this
                ->em
                ->getRepository(ModeleContratProduitExclure::class)
                ->getByContrat($mc)
        );

        $html = $this->twig->render(
            'PDF/contrat_preview.html.twig',
            [
                'mc' => $mc,
                'exclusions' => $exclusions,
            ]
        );

        $outputFileName = tempnam(sys_get_temp_dir(), 'contrat_');

        $this->mPdf->WriteHTML($html);
        $this->mPdf->Output(
            $outputFileName,
            Destination::FILE
        );

        return new \SplFileInfo($outputFileName);
    }

    public function genererContratSigneTmp(Contrat $contrat): \SplFileInfo
    {
        $this->genererContratSigne($contrat);

        $outputFileName = tempnam(sys_get_temp_dir(), 'contrat_');

        $this->mPdf->Output($outputFileName, Destination::FILE);

        return new \SplFileInfo($outputFileName);
    }

    /**
     * @throws MpdfException
     */
    public function genererContratSigneFile(Contrat $contrat): string
    {
        $this->genererContratSigne($contrat);

        // Creation destination si existe pas
        $contratPath = ContratPdf::getUploadPath();
        if (!is_dir($contratPath)) {
            mkdir($contratPath, 0o777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $contratPath.'/'.$name;
        $this->mPdf->Output($path, Destination::FILE);

        return $name;
    }

    public function genererAdhesion(Adhesion $adhesion): string
    {
        $mPdf = $this->buildMpdf(); // Use new Mpdf instance on each call as generation made in bash
        $html = $this->ciLoader->view('PDF/adhesion.php', [
            'adhesion' => $adhesion,
        ], true);

        $mPdf->WriteHTML($html);

        // Creation destination si existe pas
        $adhesionPath = $this->ciConfig->item('adhesion_path');
        if (!is_dir($adhesionPath)) {
            mkdir($adhesionPath, 0o777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $adhesionPath.'/'.$name;
        $mPdf->Output($path, Destination::FILE);

        return $name;
    }

    public function genererContratSigneDistribution(ModeleContrat $mc): \SplFileInfo
    {
        $html = $this->twig->render('PDF/contrat_distribution.html.twig', [
            'dto' => new ContratDistributionExport($mc),
        ]);

        $this->mPdf->WriteHTML($html);

        $outputFileName = tempnam(sys_get_temp_dir(), 'distribution_');

        $this->mPdf->Output($outputFileName, Destination::FILE);

        return new \SplFileInfo($outputFileName);
    }

    // Build a new pdf instance for each file generation
    private function buildMpdf(): Mpdf
    {
        return new Mpdf([
            'debug' => true,
            'tempDir' => ProjectLocation::PROJECT_ROOT.'/application/writable/mpdf/',
        ]);
    }

    /**
     * @param ModeleContratProduitExclure[] $exclusions
     *
     * @return ModeleContratProduitExclure[][]
     */
    private function orderExclusionByProduitDate($exclusions)
    {
        $res = [];
        foreach ($exclusions as $exclusion) {
            $produitId = $exclusion->getModeleContratProduit()->getId();
            $dateId = $exclusion->getModeleContratDate()->getId();
            if (!array_key_exists($produitId, $res)) {
                $res[$produitId] = [];
            }

            if (!array_key_exists($dateId, $res)) {
                $res[$produitId][$dateId] = $exclusion;
            }
        }

        return $res;
    }

    private function genererContratSigne(Contrat $contrat): void
    {
        $mc = $contrat->getModeleContrat();
        $exclusions = $this->orderExclusionByProduitDate(
            $this
                ->em
                ->getRepository(ModeleContratProduitExclure::class)
                ->getByContrat($mc)
        );
        $html = $this->twig->render(
            'PDF/contrat_signe.html.twig',
            [
                'mc' => $mc,
                'exclusions' => $exclusions,
                'contrat' => $contrat,
            ]
        );

        $this->mPdf->WriteHTML($html);
    }

    private function genererCampagne(Campagne $campagne): void
    {
        $html = $this->twig->render(
            'PDF/campagne_preview.html.twig',
            [
                'campagne' => $campagne,
            ]
        );

        $this->mPdf->WriteHTML($html);
    }

    /**
     * @throws MpdfException
     */
    public function genererCampagneTmp(Campagne $campagne): string
    {
        $this->genererCampagne($campagne);

        $outputFileName = tempnam(sys_get_temp_dir(), 'campagne_');
        $this->mPdf->Output($outputFileName, Destination::FILE);

        return $outputFileName;
    }

    private function genererCampagneBulletin(CampagneBulletin $bulletin): void
    {
        $html = $this->twig->render(
            'PDF/campagne_bulletin.html.twig',
            [
                'now' => Carbon::now(),
                'campagne' => $bulletin->getCampagne(),
                'bulletin' => $bulletin,
            ]
        );

        $this->mPdf->WriteHTML($html);
    }

    /**
     * @throws MpdfException
     */
    public function genererCampagneBulletinTmp(CampagneBulletin $bulletin): string
    {
        $this->genererCampagneBulletin($bulletin);

        $outputFileName = tempnam(sys_get_temp_dir(), 'campagne_bulletin_');
        $this->mPdf->Output($outputFileName, Destination::FILE);

        return $outputFileName;
    }

    public function genererCampagneBulletinFile(CampagneBulletin $bulletin): string
    {
        $this->genererCampagneBulletin($bulletin);

        // Creation destination si existe pas
        $contratPath = \PsrLib\ORM\Entity\Files\CampagneBulletin::getUploadPath();
        if (!is_dir($contratPath)) {
            mkdir($contratPath, 0o777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $contratPath.'/'.$name;
        $this->mPdf->Output($path, Destination::FILE);

        return $name;
    }

    public function genererCampagneBulletinRecusFile(CampagneBulletin $bulletin, string $numero): string
    {
        $html = $this->twig->render(
            'PDF/campagne_bulletin_recus.html.twig',
            [
                'bulletin' => $bulletin,
                'now' => Carbon::now(),
                'numero' => $numero,
            ]
        );

        $mPdf = $this->buildMpdf(); // Use new Mpdf instance on each call as generation made in bash

        $mPdf->WriteHTML($html);

        // Creation destination si existe pas
        $contratPath = \PsrLib\ORM\Entity\Files\CampagneBulletinRecus::getUploadPath();
        if (!is_dir($contratPath)) {
            mkdir($contratPath, 0o777, true);
        }

        $name = Uuid::uuid4()->toString().'.pdf';
        $path = $contratPath.'/'.$name;
        $mPdf->Output($path, Destination::FILE);

        return $name;
    }
}
