<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\Exception\AdhesionDecodeException;
use PsrLib\Exception\AdhesionImportException;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionCreatorUser;
use PsrLib\ORM\Entity\AdhesionValue;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\AdhesionBuider;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdhesionImport
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly ValidatorInterface $validator, private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param Amap|User $creator
     */
    public function doImport(array $inputFile, string $adhesionValueClass, AdhesionBuider $adhesionBuider, $creator): void
    {
        // Parse file
        try {
            /** @var AdhesionValue[] $adhesionsValues */
            $adhesionsValues = $this->serializer
                ->deserialize(
                    $inputFile['tmp_name'],
                    $adhesionValueClass.'[]',
                    'xls',
                    ['disable_type_enforcement' => true]
                )
            ;
        } catch (\Exception $e) {
            if ($e instanceof AdhesionDecodeException) {
                throw new AdhesionImportException(['Format de fichier invalide : '.$e->getMessage()]);
            }

            throw new AdhesionImportException(['Format de fichier invalide']);
        }

        // Build entity from raw data file
        $adhesions = $this->buildEntitiesFromValues($adhesionsValues, $adhesionBuider, $creator);

        // Validate file

        $errors = [];
        foreach ($adhesions as $key => $adhesion) {
            $violations = $this->validator->validate($adhesion, null, ['Default', 'import']);
            if (count($violations) > 0) {
                $errors = [...$errors, ...array_map(fn (ConstraintViolationInterface $constraintViolation) => sprintf('Ligne %s : %s', $key + 1, $constraintViolation->getMessage()), iterator_to_array($violations))];
            }
        }
        if (count($errors) > 0) {
            throw new AdhesionImportException($errors);
        }

        // Search duplication
        $import = new \PsrLib\DTO\AdhesionImport($adhesions);
        $violations = $this->validator->validate($import, null, 'import');
        if (count($violations) > 0) {
            $errors = array_merge($errors, array_map(fn (ConstraintViolationInterface $constraintViolation) => sprintf('%s', $constraintViolation->getMessage()), iterator_to_array($violations)));

            throw new AdhesionImportException($errors);
        }

        foreach ($adhesions as $adhesion) {
            $this->em->persist($adhesion);
        }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            sentryCapture($e);

            throw new AdhesionImportException(['Erreur interne']);
        }
    }

    /**
     * @param AdhesionValue[] $adhesionsValues
     * @param Amap|User       $creator
     *
     * @return AdhesionCreatorUser[]|AdhesionAmapAmapien[]
     */
    private function buildEntitiesFromValues($adhesionsValues, AdhesionBuider $adhesionBuider, $creator)
    {
        $adhesions = [];
        $buildErrors = [];
        $line = 1; // Pass header
        foreach ($adhesionsValues as $adhesionValue) {
            try {
                ++$line;
                $adhesions[] = $adhesionBuider->buildFromValue($adhesionValue, $creator);
            } catch (\RuntimeException $e) {
                $buildErrors[] = sprintf('Ligne %s : %s', $line, $e->getMessage());
            }
        }
        if (count($buildErrors) > 0) {
            throw new AdhesionImportException($buildErrors);
        }

        return $adhesions;
    }
}
