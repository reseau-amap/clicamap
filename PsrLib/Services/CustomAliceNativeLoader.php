<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Nelmio\Alice\Loader\NativeLoader;
use PsrLib\AliceProvider\CarbonProvider;
use PsrLib\AliceProvider\DoctrineReferenceProvider;
use PsrLib\AliceProvider\MoneyProvider;
use PsrLib\AliceProvider\NameProvider;
use PsrLib\AliceProvider\PasswordProvider;
use PsrLib\AliceProvider\UserEmailProvider;

class CustomAliceNativeLoader extends NativeLoader
{
    public function __construct(
        private readonly DoctrineReferenceProvider $doctrineReferenceProvider,
        private readonly PasswordProvider $passwordProvider,
        private readonly CarbonProvider $carbonProvider,
        private readonly MoneyProvider $moneyProvider,
        private readonly UserEmailProvider $userEmailProvider,
        private readonly NameProvider $nameProvider,
    ) {
        parent::__construct();
    }

    public function createFakerGenerator(): \Faker\Generator
    {
        $generator = parent::createFakerGenerator();
        $generator->addProvider($this->doctrineReferenceProvider);
        $generator->addProvider($this->passwordProvider);
        $generator->addProvider($this->carbonProvider);
        $generator->addProvider($this->moneyProvider);
        $generator->addProvider($this->userEmailProvider);
        $generator->addProvider($this->nameProvider);

        return $generator;
    }
}
