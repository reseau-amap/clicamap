<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Carbon\Carbon;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Repository\AmapDistributionRepository;

class AmapDistributionNotificationSender
{
    final public const AMAPIENS_RAPPELS = [6, 18];
    final public const AMAP_RELANCE = [1, 6, 18];
    final public const AMAP_ALERTE_FIN = [30, 60];

    public function __construct(private readonly Email_Sender $emailSender, private readonly AmapDistributionRepository $amapDistributionRepo)
    {
    }

    public function amapienRappels(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        foreach (self::AMAPIENS_RAPPELS as $delay) {
            $distributions = $this->amapDistributionRepo->getDistributionInDaysFromNow($delay);
            foreach ($distributions as $distribution) {
                foreach ($distribution->getAmapiens() as $amapien) {
                    $messages[] = [
                        'distribution' => $distribution,
                        'amapien' => $amapien,
                    ];
                }
            }
        }

        foreach ($messages as $message) {
            if ($message['amapien']->estActif()) {
                try {
                    $this->emailSender->envoyerDistributionAmapienRappel($message['distribution'], $message['amapien']);
                } catch (\Exception $e) {
                    sentryCapture($e);
                }
            }
            usleep(100000);
        }
    }

    public function amapRelance(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        foreach (self::AMAP_RELANCE as $delay) {
            $distributions = $this->amapDistributionRepo->getDistributionInDaysFromNow($delay);

            /** @var AmapDistribution[][] $amapDistributionsByAmap */
            $amapDistributionsByAmap = [];
            $amaps = [];

            foreach ($distributions as $distribution) {
                if ($distribution->getPlacesRestantes() > 0) {
                    $distributionAmap = $distribution->getAmapLivraisonLieu()->getAmap();
                    $distributionAmapId = $distributionAmap->getId();
                    if (!isset($amapDistributionsByAmap[$distributionAmapId])) {
                        $amapDistributionsByAmap[$distributionAmapId] = [];
                        $amaps[$distributionAmapId] = $distributionAmap;
                    }
                    $amapDistributionsByAmap[$distributionAmapId][] = $distribution;
                }
            }

            foreach ($amapDistributionsByAmap as $amapId => $amapDistributions) {
                usort($amapDistributionsByAmap[$amapId], fn (AmapDistribution $a, AmapDistribution $b) => $a->getDateDebut()->timestamp - $b->getDateDebut()->timestamp);
            }

            foreach ($amapDistributionsByAmap as $amapId => $amapDistributions) {
                $dst = $amaps[$amapId]
                    ->getAmapiens()
                    ->toArray()
                ;
                $dst = array_filter($dst, function (Amapien $amapien) use ($amapDistributions) {
                    if (!$amapien->estActif()) {
                        return false;
                    }
                    foreach ($amapDistributions as $amapDistribution) {
                        if ($amapDistribution->getAmapiens()->contains($amapien)) {
                            return false;
                        }
                    }

                    return true;
                });
                $messages[] = [
                    'distributions' => $amapDistributions,
                    'amapiens' => array_values($dst),
                ];
            }
        }

        foreach ($messages as $message) {
            try {
                $this->emailSender->envoyerDistributionAmapiensRelance(
                    $message['distributions'],
                    $message['amapiens']
                );
            } catch (\Exception $e) {
                sentryCapture($e);
            }
            usleep(100000);
        }
    }

    public function amapAlerteFin(): void
    {
        // Group messages to avoid long delay between database requests
        $messages = [];

        $distributions = $this->amapDistributionRepo->getLastDistributionByAmap();
        $now = Carbon::now()->startOfDay();
        foreach ($distributions as $distribution) {
            foreach (self::AMAP_ALERTE_FIN as $alerte) {
                $limit = $now->clone()->addDays($alerte);
                if ($distribution->getDate()->eq($limit)) {
                    $messages[] = $distribution;
                }
            }
        }

        foreach ($messages as $distribution) {
            try {
                $this->emailSender->envoyerDistributionAmapAlerteFin($distribution);
            } catch (\Exception $e) {
                sentryCapture($e);
            }
            usleep(100000);
        }
    }
}
