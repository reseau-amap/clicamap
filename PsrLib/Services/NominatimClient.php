<?php

declare(strict_types=1);

namespace PsrLib\Services;

use GuzzleHttp\Client;
use PsrLib\DTO\Nominatim\NominatimResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class NominatimClient
{
    final public const NOMINATIM_CACHE_LIFETIME = 60 * 60 * 24; // One day

    public function __construct(
        private readonly Client $client,
        private readonly SerializerInterface $serializer,
        private readonly CacheInterface $cache
    ) {
    }

    /**
     * @return NominatimResponse[]
     */
    public function geocode(string $request): array
    {
        return $this->cache->get(hash('sha256', $request), function (ItemInterface $item) use ($request) {
            $item->expiresAfter(self::NOMINATIM_CACHE_LIFETIME);

            return $this->_do_query($request);
        });
    }

    /**
     * @return NominatimResponse[]
     */
    private function _do_query(string $request)
    {
        $res = $this->client->request('GET', getenv('NOMINATIM_HOST'), [
            'query' => [
                'q' => $request,
                'format' => 'jsonv2',
                'addressdetails' => 1,
                'limit' => 5,
                'countrycodes' => 'fr',
            ],
            'headers' => [
                'User-Agent' => 'Clicamap (https://www.clicamap.org)',
            ],
        ]);

        return $this->serializer->deserialize($res->getBody()->getContents(), NominatimResponse::class.'[]', 'json');
    }
}
