<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\CampagneBulletinImportLineDTO;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\CampagneBulletinMontantLibre;
use PsrLib\ORM\Entity\User;

class CampagneBulletinBuilder
{
    public function createFromCampagne(Campagne $campagne, User $currentUser): CampagneBulletin
    {
        $bulletin = new \PsrLib\ORM\Entity\CampagneBulletin();
        $bulletin->setCampagne($campagne);

        $amapien = $currentUser->getUserAmapienAmapForAmap($campagne->getAmap());
        if (null === $amapien) {
            throw new \RuntimeException('User must be in campaign amap');
        }
        $bulletin->setAmapien($amapien);

        foreach ($campagne->getMontantLibres() as $montant) {
            $bulletinMontant = new CampagneBulletinMontantLibre();
            $bulletinMontant->setCampagneMontant($montant);
            $bulletin->addMontant($bulletinMontant);
        }

        return $bulletin;
    }

    public function createFromImportDTO(CampagneBulletinImportLineDTO $bulletinImportLineDTO): CampagneBulletin
    {
        $bulletin = new CampagneBulletin();
        $bulletin->setCampagne($bulletinImportLineDTO->getCampagne());
        $bulletin->setAmapien($bulletinImportLineDTO->getAmapien());
        $bulletin->setPayeur($bulletinImportLineDTO->getPayeur());
        $bulletin->setPaiementDate($bulletinImportLineDTO->getPaiementDate());
        $bulletin->setPermissionImage('O' === $bulletinImportLineDTO->getPermissionImage());
        $campagneMontantLibres = $bulletinImportLineDTO->getCampagne()->getMontants();
        foreach ($bulletinImportLineDTO->getMontantLibres() as $i => $montantLibre) {
            $bulletinMontantLibre = new CampagneBulletinMontantLibre();
            $bulletinMontantLibre->setCampagneMontant($campagneMontantLibres[$i]);
            $bulletinMontantLibre->setMontant($montantLibre);
            $bulletin->addMontant($bulletinMontantLibre);
        }

        return $bulletin;
    }
}
