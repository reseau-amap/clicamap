<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\CampagneBulletinRecus;
use PsrLib\ORM\Repository\CampagneBulletinRecusRepository;
use PsrLib\Services\MPdfGeneration;

class CampagneBulletinRecusBuilder
{
    public function __construct(private readonly MPdfGeneration $mPdfGeneration, private readonly CampagneBulletinRecusRepository $campagneBulletinRecusRepository)
    {
    }

    public function create(CampagneBulletin $campagneBulletin): CampagneBulletinRecus
    {
        $amap = $campagneBulletin->getCampagne()->getAmap();
        $sequence = $this->campagneBulletinRecusRepository->getNumeroInterneSuivantForAmap($amap);
        $numero = sprintf('%05d%05d', $amap->getId(), $sequence);

        $recusPdf = new \PsrLib\ORM\Entity\Files\CampagneBulletinRecus(
            $this->mPdfGeneration->genererCampagneBulletinRecusFile($campagneBulletin, $numero),
        );

        return new CampagneBulletinRecus(
            $sequence,
            $numero,
            $recusPdf
        );
    }
}
