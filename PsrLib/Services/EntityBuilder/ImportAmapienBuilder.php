<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapienAnneeAdhesion;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\UserRepository;
use PsrLib\ORM\Repository\VilleRepository;
use PsrLib\Services\UsernameGenerator;

class ImportAmapienBuilder
{
    public function __construct(
        private readonly VilleRepository $villeRepo,
        private readonly UserRepository $userRepo,
        private readonly UsernameGenerator $usernameGenerator,
        private readonly AmapienRepository $amapienRepository,
    ) {
    }

    /**
     * @return Amapien[]
     */
    public function buildAmapiensFromImport(array $valeurs, Amap $amap)
    {
        $amapiens = [];

        $usernamesToAdd = [];
        // A->prénom ; B->nom ; C->mail ; D->tel ; E->adresse ; F->code postal
        // G->ville ; H->adhérents années ; I->abonné newsletter
        foreach ($valeurs as $valeur) {
            $user = $this->userRepo->getOneByEmail((string) $valeur['C']);
            if (null === $user) {
                $user = new User();
                $user->getName()->setFirstName(trim((string) $valeur['A']));
                $user->getName()->setLastName(trim((string) $valeur['B']));

                $usernameTemplate = $this->usernameGenerator->generate($user->getName()->getFirstName(), $user->getName()->getLastName());
                $username = $usernameTemplate;
                $i = 2;
                while ($this->userRepo->count(['username' => $username]) > 0 || in_array($username, $usernamesToAdd, true)) {
                    $username = $usernameTemplate.$i++;
                }
                $user->setUsername($username);
                $usernamesToAdd[] = $username;

                $email = (string) $valeur['C'];
                if ('=' === $email[0]) { // =HYPERLINK
                    $email = str_replace('=HYPERLINK("mailto:', '', $email);
                    $email = explode('","', $email);
                    $email = trim($email[0]);
                }
                $userEmail = new UserEmail();
                $userEmail->setEmail($email);
                $user->addEmail($userEmail);

                $tels = explode('|', (string) $valeur['D']);
                // Si il y a plusieurs numéro
                if (count($tels) > 1) {
                    $user->setNumTel1($tels[0]);
                    $user->setNumTel2($tels[1]);
                } else {
                    $user->setNumTel1(trim($tels[0]));
                }

                $user->getAddress()->setAdress(trim((string) $valeur['E']));

                $ville = $this->villeRepo->findOneBy([
                    'cp' => trim((string) $valeur['F']),
                    'nom' => trim((string) $valeur['G']),
                ]);
                $user->setVille($ville);
                $user->setNewsletter('non' !== trim(strtolower((string) $valeur['I'])));
            }

            $amapien = $this->amapienRepository->findOneDeletedByAmapUser($amap, $user);
            if (null !== $amapien) {
                $amapien->setDeletedAt(null);
            } else {
                $amapien = new Amapien($user, $amap);
                $amapien->setEtat(ActivableUserInterface::ETAT_INACTIF);
                $amapien->setAmap($amap);
                $annee = explode('|', (string) $valeur['H']);
                foreach ($annee as $a) {
                    $annne = new AmapienAnneeAdhesion((int) trim($a), $amapien);
                    $amapien->addAnneeAdhesion($annne);
                }
            }

            $amapiens[] = $amapien;
        }

        return $amapiens;
    }
}
