<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use Assert\Assertion;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRepository;

class AdhesionBuilderFerme implements AdhesionBuider
{
    public function __construct(private readonly FermeRepository $fermeRepo)
    {
    }

    /**
     * @param AdhesionValueFerme $value
     * @param User               $creator
     *
     * @return AdhesionFerme
     */
    public function buildFromValue($value, $creator)
    {
        Assertion::isInstanceOf($creator, User::class);
        Assertion::isInstanceOf($value, AdhesionValueFerme::class);

        $ferme = $this->fermeRepo->findOneBy(['siret' => $value->getSiret()]);
        if (null === $ferme) {
            throw new \RuntimeException(sprintf('Le siret %s ne correspond pas à une ferme', $value->getSiret()));
        }

        return new AdhesionFerme($ferme, $value, $creator);
    }
}
