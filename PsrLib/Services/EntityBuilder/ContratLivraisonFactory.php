<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ContratLivraisonCellule;
use PsrLib\ORM\Entity\Ferme;

class ContratLivraisonFactory
{
    /**
     * @param Contrat[] $contrats
     *
     * @return ContratLivraison
     */
    public function create(Amapien $amapien, Ferme $ferme, Carbon $date, $contrats)
    {
        $livraison = new ContratLivraison($amapien, $ferme, $date);

        foreach ($contrats as $contrat) {
            foreach ($contrat->getCellules() as $cellule) {
                if (true === $cellule->getModeleContratProduit()->getRegulPds()
                    && $date->eq($cellule->getModeleContratDate()->getDateLivraison())
                    && $cellule->getQuantite() > 0.0) {
                    $livraison->addCellule(new ContratLivraisonCellule($livraison, $cellule));
                }
            }
        }

        return $livraison;
    }
}
