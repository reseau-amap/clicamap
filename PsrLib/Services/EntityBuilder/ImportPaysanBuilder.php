<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\EntityBuilder;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use PsrLib\ORM\Repository\UserRepository;
use PsrLib\ORM\Repository\VilleRepository;

class ImportPaysanBuilder
{
    public function __construct(
        private readonly VilleRepository $villeRepo,
        private readonly UserRepository $userRepository,
    ) {
    }

    private function getPaysanOrBuild(
        ?string $prenom,
        ?string $nom,
        ?string $email,
        ?string $tel1,
        ?string $tel2,
        ?string $adresse,
        ?int $cp,
        ?string $ville,
        ?string $newsletter,
    ): Paysan {
        $user = $this->userRepository->getOneByEmail($email);
        if (null === $user) {
            $user = new User();
            $user->getName()->setFirstName($prenom);
            $user->getName()->setLastName($nom);
            $userEmail = new UserEmail();
            $userEmail->setEmail($email);
            $user->addEmail($userEmail);

            $user->setNumTel1($tel1);
            $user->setNumTel2($tel2);
            $user->getAddress()->setAdress($adresse);

            $ville = $this->villeRepo->findOneBy([
                'cp' => trim((string) $cp),
                'nom' => trim((string) $ville),
            ]);
            $user->setVille($ville);
            $user->setNewsletter('oui' === mb_strtolower($newsletter));
        }

        $paysan = $user->getPaysan();
        if (null === $paysan) {
            $paysan = new Paysan();
            $user->setPaysan($paysan);
            $paysan->setUser($user);
        }

        return $paysan;
    }

    /**
     * @return Paysan[]
     */
    public function buildPaysansFromImport(array $entete, array $valeurs)
    {
        $paysans = [];
        foreach ($valeurs as $valeur) {
            $paysan = $this
                ->getPaysanOrBuild(
                    $valeur['A'],
                    $valeur['B'],
                    $valeur['C'],
                    $valeur['D'],
                    $valeur['E'],
                    $valeur['G'],
                    $valeur['H'],
                    $valeur['I'],
                    $valeur['M'],
                )
            ;
            $ferme = new Ferme();
            $paysan->addFerme($ferme);

            $fVille = $this->villeRepo->findOneBy([
                'cp' => trim((string) $valeur['Q']),
                'nom' => trim((string) $valeur['R']),
            ]);
            $ferme->setVille($fVille);

            foreach ($valeur as $k => $v) {
                if ($v) {
                    $v = trim((string) $v);

                    // PAYSAN ----------------------------------------------------------
                    // Nom

                    // Adresse Site Web ou Blog
                    if ('F' == $k) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0]);
                        }

                        $ferme->setUrl($v);
                    }

                    // Numéro MSA
                    if ('L' == $k) {
                        $ferme->setMsa($v);
                    }
                    // Année début partenariat AMAP
                    if ('M' == $k) {
                        $ferme->setAnneeDebCommercialisationAmap($v);
                    }

                    // FERME -----------------------------------------------------------
                    // Nom
                    if ('N' == $k) {
                        $ferme->setNom($v);
                        $ferme->setOrdreCheque($v);
                    }
                    // Siret
                    if ('O' == $k) {
                        $ferme->setSiret($v);
                    }
                    // Adresse
                    if ('P' == $k) {
                        $ferme->setLibAdr($v);
                    }
                    // GPS lat
                    if ('lat' == $k) {
                        $ferme->setGpsLatitude($v);
                    }
                    // GPS long
                    if ('long' == $k) {
                        $ferme->setGpsLongitude($v);
                    }
                }
            }

            $paysans[] = $paysan;
        }

        return $paysans;
    }
}
