<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use Carbon\CarbonImmutable;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContratProduit;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ModeleContratCopy
{
    private const PROPERTY_COPY = [
        'description',
        'livraisonLieu',
        'frequence',
        'modalites',
        'choixIdentiques',
        'frequenceArticles',
        'version',
        'filiere',
        'specificite',
        'regulPoid',
        'amapienPermissionIntervenirPlanning',
        'reglementType',
        'reglementModalite',
        'contratAnnexes',
        'ferme',
        'livraisonLieu',
        'delaiModifContrat',
    ];

    public function __construct(private readonly PropertyAccessor $propertyAccessor)
    {
    }

    public function copy(ModeleContrat $mcOrig): ModeleContrat
    {
        $now = CarbonImmutable::now()->startOfDay();
        $mcNew = new ModeleContrat();
        $mcNew->setNom('Copie de '.$mcOrig->getNom());
        $mcNew->setEtat(ModeleContratEtat::ETAT_BROUILLON);

        // Copy base propery
        foreach (self::PROPERTY_COPY as $property) {
            $this->propertyAccessor->setValue($mcNew, $property, $this->propertyAccessor->getValue($mcOrig, $property));
        }

        // Copy info livraison
        $mcNew->setInformationLivraison(clone $mcOrig->getInformationLivraison());

        // Copy products
        foreach ($mcOrig->getProduits() as $produit) {
            $fermeProduit = $produit->getFermeProduit();
            if (null === $fermeProduit) {
                continue;
            }

            $newProduit = new ModeleContratProduit();
            $newProduit->setPrix(clone $fermeProduit->getPrix());
            $newProduit->setFermeProduit($produit->getFermeProduit());
            $newProduit->setNom($produit->getNom());
            $newProduit->setConditionnement($produit->getConditionnement());
            $newProduit->setRegulPds($produit->getRegulPds());
            $newProduit->setTypeProduction($produit->getTypeProduction());

            $mcNew->addProduit($newProduit);
        }

        // Copy dates reglements
        $datesReglement = $mcOrig->getDatesReglementOrderedAsc();
        $datesReglementFirst = $datesReglement->first();
        if (false !== $datesReglementFirst && $now->lt($datesReglementFirst->getDate())) {
            $mcNew->setReglementNbMax($mcOrig->getReglementNbMax());

            foreach ($datesReglement as $dateReglement) {
                $newDateReglement = new ModeleContratDatesReglement();
                $newDateReglement->setDate($dateReglement->getDate());
                $mcNew->addDateReglement($newDateReglement);
            }
        }

        // Copy forclusion
        $forclusion = $mcOrig->getForclusion();
        if (null !== $forclusion && $now->lt($forclusion)) {
            $mcNew->setForclusion($forclusion);
        }

        return $mcNew;
    }
}
