<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\DocumentUtilisateurFormState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Files\DocumentUtilisateur as DocumentUtilisateurFile;
use PsrLib\Services\Uploader;

class DocumentUtilisateurFactory
{
    private Uploader $uploader;

    public function __construct(Uploader $uploader)
    {
        $this->uploader = $uploader;
    }

    // Expect state is validated
    public function create(DocumentUtilisateurFormState $state): DocumentUtilisateurAmap|DocumentUtilisateurFerme
    {
        $fichier = $this->uploader->uploadFile($state->getFile(), DocumentUtilisateurFile::class);
        $target = $state->getTarget();
        if ($target instanceof Amap) {
            return new DocumentUtilisateurAmap(
                $state->getAnnee(),
                $state->getNom(),
                $fichier,
                $target
            );
        } if ($target instanceof Ferme) {
            return new DocumentUtilisateurFerme(
                $state->getAnnee(),
                $state->getNom(),
                $fichier,
                $target
            );
        }

        throw new \LogicException('Amap or Ferme must be set');
    }
}
