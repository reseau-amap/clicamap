<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\CampagneWizardDTO;
use PsrLib\DTO\UserRepositoryReseauAdminFromAmapDTO;
use PsrLib\Exception\CampagneWizardDtoBuilderNoAdminException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneMontant;
use PsrLib\ORM\Entity\Files\CampagneLogo;
use PsrLib\ORM\Entity\Files\File;
use PsrLib\ORM\Repository\UserRepository;
use Symfony\Component\Filesystem\Filesystem;

class CampagneWizardDtoBuilder
{
    public function __construct(private readonly UserRepository $userRepository, private readonly Filesystem $fs)
    {
    }

    public function createNew(Amap $amap): CampagneWizardDTO
    {
        $campagne = Campagne::createFromAmap($amap);
        $campagne->addMontant(new CampagneMontant());
        $this->updateCampagneAuthor($campagne);

        return new CampagneWizardDTO(
            $campagne,
            null
        );
    }

    public function createEdit(Campagne $campagne): CampagneWizardDTO
    {
        $campagneNew = clone $campagne;
        $this->updateCampagneAuthor($campagneNew);

        return new CampagneWizardDTO(
            $campagneNew,
            $campagne
        );
    }

    private function getAdminFromAmap(Amap $amap): ?UserRepositoryReseauAdminFromAmapDTO
    {
        if (!$amap->getAssociationDeFait()) {
            return null;
        }

        $admin = $this->userRepository->getReseauAdminFromAmap($amap);
        if (null === $admin) {
            throw new CampagneWizardDtoBuilderNoAdminException();
        }

        return $admin;
    }

    private function updateCampagneAuthor(Campagne $campagne): void
    {
        $amap = $campagne->getAmap();

        $admin = $this->getAdminFromAmap($amap);
        $autheurInfo = $campagne->getAutheurInfo();
        if (null === $admin) {
            $campagne->setAutheurLogo($this->buildAmapLogo($amap->getLogo()));
            $autheurInfo->setNom($amap->getNom() ?? '');
            $adresse = '';
            $ville = '';
            if (null !== $amap->getAdresseAdminVille()) {
                $adresse = sprintf(
                    '%s %s, %s',
                    $amap->getAdresseAdmin(),
                    $amap->getAdresseAdminVille()->getCpString(),
                    $amap->getAdresseAdminVille()->getNom()
                );
                $ville = $amap->getAdresseAdminVille()->getNom();
            }
            $autheurInfo->setAdresse($adresse);
            $autheurInfo->setVille($ville);
            $autheurInfo->setSite($amap->getUrl() ?? '');
            $autheurInfo->setTelephone('');
            $autheurInfo->setSiret($amap->getSiret() ?? '');
            $autheurInfo->setRna($amap->getRna() ?? '');
            $autheurInfo->setEmails($amap->getEmail() ?? '');
        } else {
            $adminUser = $admin->getUser();
            $adminAdhesionInfo = $adminUser->getAdhesionInfo();

            $campagne->setAutheurLogo($this->buildAmapLogo($admin->getLocation()->getLogo()));

            $adresse = sprintf(
                '%s %s %s, %s',
                $adminUser->getAddress()->getAdress() ?? '',
                $adminUser->getAddress()->getAdressCompl() ?? '',
                $adminUser->getVille()?->getCpString() ?? '',
                $adminUser->getVille()?->getNom() ?? ''
            );
            $autheurInfo->setNom($adminAdhesionInfo->getNomReseau() ?? '');
            $autheurInfo->setAdresse($adresse);
            $autheurInfo->setVille($adminUser->getVille()?->getNom() ?? '');
            $autheurInfo->setSite($adminAdhesionInfo->getSite() ?? '');
            $autheurInfo->setTelephone($adminUser->getNumTel1() ?? '');
            $autheurInfo->setSiret($adminAdhesionInfo->getSiret() ?? '');
            $autheurInfo->setRna($adminAdhesionInfo->getRna() ?? '');

            $email = $adminUser->getEmails()->first();
            $autheurInfo->setEmails(false === $email ? '' : $email->getEmail());
        }
    }

    private function buildAmapLogo(?File $file): ?CampagneLogo
    {
        if (null === $file) {
            return null;
        }

        $fileName = $file->getFileName();
        $logo = new CampagneLogo($fileName, $file->getOriginalFileName());
        $this->fs->touch($logo->getFileFullPath());
        $this->fs->copy($file->getFileFullPath(), $logo->getFileFullPath(), true);

        return $logo;
    }
}
