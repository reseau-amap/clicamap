<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\Nominatim\NominatimResponse;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\DepartementRepository;

class VilleFactory
{
    public function __construct(private readonly DepartementRepository $departementRepository)
    {
    }

    public function create(NominatimResponse $address): ?Ville
    {
        $county = $address->getAddress()->getCounty();

        if (null === $county) {
            return null;
        }

        $placeName = $address->getAddress()->getPlaceName();
        $postCode = $address->getAddress()->getPostCode();
        if (null === $placeName || null === $postCode) {
            return null;
        }

        $dep = $this->departementRepository->findSingleByNameLiked($county);
        if (null === $dep) {
            return null;
        }

        return new Ville(
            (int) $address->getAddress()->getPostCode(),
            $address->getAddress()->getPlaceName(),
            $dep
        );
    }
}
