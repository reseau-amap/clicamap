<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\DTO\EvenementAbonnementContainer;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\EvenementAbonnementDepartement;
use PsrLib\ORM\Entity\EvenementAbonnementFerme;
use PsrLib\ORM\Entity\EvenementAbonnementRegion;
use PsrLib\ORM\Entity\User;

class EvenementAbonnementDefaultFactory
{
    public function getDefaultForUser(User $user): EvenementAbonnementContainer
    {
        $res = [];
        foreach ($user->getAmapsAsAmapienOrAdmin() as $amap) {
            $res[] = EvenementAbonnementAmap::create($user, $amap);
            $res[] = EvenementAbonnementDepartement::create($user, $amap->getDepartement());
            $res[] = EvenementAbonnementRegion::create($user, $amap->getRegion());

            foreach ($amap->getFermesWithRef() as $ferme) {
                $res[] = EvenementAbonnementFerme::create($user, $ferme);
            }
        }

        $fermes = $user->getPaysan()?->getFermes() ?? [];
        foreach ($fermes as $ferme) {
            $res[] = EvenementAbonnementFerme::create($user, $ferme);
            $res[] = EvenementAbonnementDepartement::create($user, $ferme->getDepartement());
            $res[] = EvenementAbonnementRegion::create($user, $ferme->getRegion());

            foreach ($ferme->getAmapienRefs() as $amapienRef) {
                $res[] = EvenementAbonnementAmap::create($user, $amapienRef->getAmap());
            }
        }

        foreach ($user->getAdminRegions() as $region) {
            $res[] = EvenementAbonnementRegion::create($user, $region);
            foreach ($region->getDepartements() as $department) {
                $res[] = EvenementAbonnementDepartement::create($user, $department);
            }
        }
        foreach ($user->getAdminDepartments() as $department) {
            $res[] = EvenementAbonnementDepartement::create($user, $department);
        }

        return new EvenementAbonnementContainer($res);
    }
}
