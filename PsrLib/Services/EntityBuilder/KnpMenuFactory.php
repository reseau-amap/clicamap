<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\MenuFactory;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ActualiteManageVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapVoter;
use PsrLib\Services\Security\Voters\AdhesionCreatorUserVoter;
use PsrLib\Services\Security\Voters\AdhesionFermeVoter;
use PsrLib\Services\Security\Voters\CampagneListAdminVoter;
use PsrLib\Services\Security\Voters\CampagneListAmapienVoter;
use PsrLib\Services\Security\Voters\ContactVoter;
use PsrLib\Services\Security\Voters\ContractVoter;
use PsrLib\Services\Security\Voters\DocumentVoter;
use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use PsrLib\Services\Security\Voters\UserVoter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class KnpMenuFactory
{
    public function __construct(
        private readonly AuthorizationCheckerInterface $authorizationChecker,
        private readonly EntityManagerInterface $em,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function createMenuForUser(User $user): ItemInterface
    {
        $factory = new MenuFactory();

        $menu = $factory->createItem('main');
        $left = $menu->addChild('left');
        $right = $menu->addChild('right');

        if (!$user->hasNoRights()) {
            $clicamap = $left->addChild(getenv('SITE_NAME'));
            $clicamap->addChild('Les événements', ['uri' => $this->urlGenerator->generate('portail_index')]);
            if ($this->authorizationChecker->isGranted(DocumentVoter::ACTION_DOCUMENT_GETOWN)) {
                $clicamap->addChild('Documentation', ['uri' => $this->urlGenerator->generate('documentation_index')]);
            }
        }

        if ($user->isFermeRegroupementAdmin()) {
            foreach ($user->getAdminFermeRegroupements() as $fermeRegroupement) {
                $regroupement = $left->addChild('Gestionnaire regroupement '.$fermeRegroupement->getNom());
                $regroupement->addChild('Gestion des fermes', ['uri' => $this->urlGenerator->generate('ferme_home_regroupement', ['regroupement_id' => $fermeRegroupement->getId()])]);
                $regroupement->addChild('Gestion des paysans', ['uri' => $this->urlGenerator->generate('paysan_home_regroupement', ['regroupement_id' => $fermeRegroupement->getId()])]);
            }
        }

        if ($user->isSuperAdmin()) {
            $network = $left->addChild('Réseaux');
            $network->addChild('Régions & Départements', ['uri' => $this->urlGenerator->generate('region_index')]);
            $network->addChild('Gestion des utilisateurs', ['uri' => $this->urlGenerator->generate('user_index')]);
        }

        if ($user->isAdmin()) {
            $adminAmap = $left->addChild('Gestionnaire Admin');
            $adminAmap->addChild('Gestion des AMAP', ['uri' => $this->urlGenerator->generate('amap_index')]);
            $adminAmap->addChild('Gestion des amapiens', ['uri' => $this->urlGenerator->generate('amapien_search_admin')]);
            $adminAmap->addChild('Import', [
                'uri' => $this->urlGenerator->generate('import_index'),
                'extras' => ['add_separator_after' => true],
            ]);
            $adminAmap->addChild('Gestion des fermes', ['uri' => $this->urlGenerator->generate('ferme_index')]);
            if ($this->authorizationChecker->isGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST)) {
                $adminAmap->addChild('Gestion des regroupements', ['uri' => $this->urlGenerator->generate('ferme_regroupement_index')]);
            }
            $adminAmap->addChild('Gestion des paysans', ['uri' => $this->urlGenerator->generate('paysan_index'), 'extras' => ['add_separator_after' => true]]);
            $adminAmap->addChild('Gestion des contrats vierges', ['uri' => '/contrat_vierge/accueil_admin/']);
            $adminAmap->addChild('Gestion des contrats signés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_admin')]);
            $adminAmap->addChild('Gestion des contrats archivés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_admin_archive')]);
        }

        if ($user->isAmapAdmin()) {
            $amapItem = $left->addChild('Gestionnaire AMAP');
            $amapItem->addChild('Gestion de mon AMAP', ['uri' => $this->urlGenerator->generate('amap_mon_amap')]);
            $amapItem->addChild('Gestion des amapiens', ['uri' => $this->urlGenerator->generate('amapien_search_amap')]);
            $amapItem->addChild('Gestion des adhésions', ['uri' => $this->urlGenerator->generate('campagne_index')]);

            $amapItem->addChild('Gestion des fermes', ['uri' => $this->urlGenerator->generate('ferme_index'), 'extras' => ['add_separator_before' => true]]);
            $amapItem->addChild('Gestion des paysans', ['uri' => $this->urlGenerator->generate('paysan_index'), 'extras' => ['add_separator_after' => true]]);

            $amapItem->addChild('Gestion des contrats vierges', ['uri' => '/contrat_vierge/accueil_amapadmin']);
            $amapItem->addChild('Gestion des contrats signés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_amap')]);
            $amapItem->addChild('Gestion des contrats archivés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_amap_archive')]);

            $agenda = $left->addChild('Agenda AMAP');
            $agenda->addChild('Gestion de la distrib\'AMAP', ['uri' => $this->urlGenerator->generate('distribution_index')]);
            foreach ($user->getAdminAmaps() as $amap) {
                $agenda->addChild('Calendrier des livraisons AMAP '.$amap->getNom(), ['uri' => $this->urlGenerator->generate('amap_calendrier', ['amap_id' => $amap->getId()])]);
            }
        }

        if ($user->isRefProduit()) {
            $refProduit = $left->addChild('Gestionnaire référent');
            $refProduit->addChild('Gestion des fermes', ['uri' => $this->urlGenerator->generate('ferme_home_refprod')]);
            $refProduit->addChild('Gestion des paysans', ['uri' => $this->urlGenerator->generate('paysan_home_refprod'), 'extras' => ['add_separator_after' => true]]);

            $refProduit->addChild('Gestion des contrats vierges', ['uri' => '/contrat_vierge/accueil_refprod']);
            $refProduit->addChild('Gestion des contrats signés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_ref_produit')]);
            $refProduit->addChild('Gestion des contrats archivés', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_ref_produit_archive')]);
        }

        if ($user->isPaysan() && $user->getPaysan()->estActif()) {
            foreach ($user->getPaysan()->getFermes() as $ferme) {
                $fermeItem = $left->addChild('Gestionnaire ferme');
                $fermeItem->addChild('Ma ferme', ['uri' => $this->urlGenerator->generate('ferme_home_paysan', ['ferme_id' => $ferme->getId()])]);
                $fermeItem->addChild('Mes produits', ['uri' => $this->urlGenerator->generate('produit_accueil', ['ferme_id' => $ferme->getId()])]);
                $fermeItem->addChild('Mes livraisons', ['uri' => $this->urlGenerator->generate('ferme_livraison_accueil', ['ferme_id' => $ferme->getId()])]);
            }
        }

        if (($user->isPaysan() && $user->getPaysan()->estActif()) || $user->isFermeRegroupementAdmin()) {
            $contract = $left->addChild('Gestion des contrats');
            $contract->addChild('Contrats à valider', ['uri' => '/contrat_vierge/list_to_validate']);
            $contract->addChild('Contrats en cours', ['uri' => $this->urlGenerator->generate('contrat_signe_accueil_paysan')]);
            if ($this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_PAYSAN)) {
                $contract->addChild('Contrats archivés', ['uri' => $this->urlGenerator->generate('contrat_signe_contrat_paysan_archived')]);
            }
            if ($this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT)) {
                $contract->addChild('Exports comptable', ['uri' => $this->urlGenerator->generate('contrat_signe_contrat_paysan_export')]);
            }
        }

        if ($this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY_OWN)) {
            $contractOwn = $left->addChild('Mes contrats');
            $contractOwn->addChild('Les nouveaux contrats disponibles', ['uri' => $this->urlGenerator->generate('contrat_signe_contrat_own_new')]);
            $contractOwn->addChild('Mes contrats existants', ['uri' => $this->urlGenerator->generate('contrat_signe_contrat_own_existing')]);
            $contractOwn->addChild('Mes contrats archivés', ['uri' => $this->urlGenerator->generate('contrat_signe_contrat_own_archived')]);

            $agendaOwn = $left->addChild('Mon agenda');
            $agendaOwn->addChild('Mes distrib\'AMAP', ['uri' => $this->urlGenerator->generate('distribution_amapien')]);
            foreach ($user->getAmapienAmapsActif() as $amapienAmap) {
                $agendaOwn->addChild('Mes livraisons '.$amapienAmap->getAmap()->getNom(), ['uri' => $this->urlGenerator->generate('amapien_calendrier', ['amapien_id' => $amapienAmap->getId()])]);
            }
        }

        if ($user->isAdmin()) {
            $communication = $left->addChild('Communication');
            $communication->addChild('Événements', ['uri' => $this->urlGenerator->generate('evenements')]);
            $communication->addChild('Publipostage', ['uri' => '/publipostage']);
            $communication->addChild('Gestion des documents', ['uri' => $this->urlGenerator->generate('document_utilisateur_index')]);

            if ($this->authorizationChecker->isGranted(DocumentVoter::ACTION_DOCUMENT_MANAGE)) {
                $communication->addChild('Gestion des guides', ['uri' => $this->urlGenerator->generate('documentation_list_documents')]);
            }
            if ($this->authorizationChecker->isGranted(ActualiteManageVoter::ACTUALITE_MANAGE_VOTER)) {
                $communication->addChild('Gestion des versions', ['uri' => $this->urlGenerator->generate('actualites')]);
            }

            $recus = $left->addChild('Reçus');
            if ($this->authorizationChecker->isGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_IMPORT)) {
                $recus->addChild('Import', ['uri' => $this->urlGenerator->generate('adhesion_import')]);
            }
            if ($this->authorizationChecker->isGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_LIST)) {
                $recus->addChild('AMAP', ['uri' => $this->urlGenerator->generate('adhesion_amap')]);
                $recus->addChild('Paysan', ['uri' => $this->urlGenerator->generate('adhesion_paysan')]);
            }
            if ($this->authorizationChecker->isGranted(CampagneListAdminVoter::ACTION_CAMPAGNE_LIST_ADMIN)) {
                $recus->addChild('Amapien', ['uri' => $this->urlGenerator->generate('campagne_index_admin')]);
            }
        }

        if (!$user->hasNoRights()) {
            $contact = $right->addChild('Contact');
            foreach ($user->getAmapienAmapsActif() as $amapienAmap) {
                $amap = $amapienAmap->getAmap();
                $contact->addChild('Mon AMAP '.$amap->getNom(), ['uri' => $this->urlGenerator->generate('contact_mon_amap', ['amap_id' => $amap->getId()])]);
                $contact->addChild('Un référent produit '.$amap->getNom(), ['uri' => $this->urlGenerator->generate('contact_referent_produit', ['amap_id' => $amap->getId()])]);
            }
            if ($this->authorizationChecker->isGranted(ContactVoter::ACTION_CONTACT_ADMIN)) {
                $contact->addChild('Un administrateur réseau', ['uri' => $this->urlGenerator->generate('contact_admin')]);
            }

            $link = $right->addChild('Lien');
            $this->render_menu_amap_link_amapien($link, $user);
            $this->render_menu_amap_link_paysan($link, $user);
            $this->render_menu_ferme_link_amap($link, $user);
        }

        $myAccount = $right->addChild('Mon compte');
        $myAccount->addChild('Mon profil', ['uri' => $this->urlGenerator->generate('user_profil')]);
        if ($this->authorizationChecker->isGranted(UserVoter::ACTION_USER_CHANGE_PASSWORD, $user)) {
            $myAccount->addChild('Mot de passe', ['uri' => $this->urlGenerator->generate('user_profil_mot_de_passe')]);
        }
        if ($this->authorizationChecker->isGranted(CampagneListAmapienVoter::ACTION_CAMPAGNE_LIST_AMAPIEN)) {
            $myAccount->addChild('Adhésions', ['uri' => $this->urlGenerator->generate('campagne_amapien_liste')]);
        }
        if ($this->authorizationChecker->isGranted(AdhesionAmapVoter::ACTION_ADHESION_AMAP_LIST_SELF)) {
            $myAccount->addChild('Mes reçus AMAP', ['uri' => $this->urlGenerator->generate('adhesion_mes_recus_amap')]);
        }
        $myAccount->addChild('Autres documents', ['uri' => $this->urlGenerator->generate('user_profil_documents')]);
        if ($this->authorizationChecker->isGranted(AdhesionFermeVoter::ACTION_ADHESION_FERME_LIST_SELF)) {
            $myAccount->addChild('Mes reçus Ferme', ['uri' => $this->urlGenerator->generate('adhesion_mes_recus_ferme')]);
        }
        $myAccount->addChild('Déconnexion', ['uri' => $this->urlGenerator->generate('portail_deconnexion'), 'extras' => ['add_separator_before' => true]]);

        return $menu;
    }

    private function render_menu_amap_link_paysan(ItemInterface $link, User $user): void
    {
        if (!$user->isPaysan()) {
            return;
        }
        /** @var Amap[] $amaps */
        $amaps = $this
            ->em
            ->getRepository(Amap::class)
            ->findByFermes($user->getFermesAsPaysan())
        ;

        // Filtre les amaps avec url
        $amaps = array_filter($amaps, fn (Amap $amap) => '' !== $amap->getUrl() && null !== $amap->getUrl());

        // Tri les amaps par ordre alphabétique
        usort($amaps, fn (Amap $a, Amap $b) => strnatcmp(strtolower($a->getNom()), strtolower($b->getNom())));
        foreach ($amaps as $amap) {
            $link->addChild($amap->getNom(), ['uri' => prep_url($amap->getUrl()), 'extras' => ['target' => '_blank']]);
        }
    }

    private function render_menu_amap_link_amapien(ItemInterface $link, User $user): void
    {
        if (!$user->isAmapienActif()) {
            return;
        }

        // Récupère l'amap de l'amapien
        $amapsAmapien = $user->getAmapsAsAmapienActif();

        foreach ($amapsAmapien as $amap) {
            if (null !== $amap->getUrl() && '' !== $amap->getUrl()) {
                $link->addChild($amap->getNom(), ['uri' => prep_url($amap->getUrl()), 'extras' => ['target' => '_blank']]);
            }
        }

        /** @var Ferme[] $fermes */
        $fermes = $this
            ->em
            ->getRepository(Ferme::class)
            ->getFromAmaps($amapsAmapien)
        ;

        // Filtre les fermes avec url
        $fermes = array_filter($fermes, fn (Ferme $ferme) => '' !== $ferme->getUrl() && null !== $ferme->getUrl());

        // Tri les fermes
        uasort($fermes, fn (Ferme $ferme1, Ferme $ferme2) => strnatcmp(strtolower($ferme1->getNom()), strtolower($ferme2->getNom())));
        foreach ($fermes as $ferme) {
            $link->addChild($ferme->getNom(), ['uri' => prep_url($ferme->getUrl()), 'extras' => ['target' => '_blank']]);
        }
    }

    public function render_menu_ferme_link_amap(ItemInterface $link, User $user): void
    {
        if (!$user->isAmapAdmin()) {
            return;
        }

        $fermes = $this
            ->em
            ->getRepository(Ferme::class)
            ->getFromAmaps($user->getAdminAmaps()->toArray())
        ;

        // Filtre les fermes avec url
        $fermes = array_filter($fermes, fn (Ferme $ferme) => '' !== $ferme->getUrl() && null !== $ferme->getUrl());

        uasort($fermes, fn (Ferme $ferme1, Ferme $ferme2) => strnatcmp(strtolower($ferme1->getNom()), strtolower($ferme2->getNom())));
        foreach ($fermes as $ferme) {
            $link->addChild($ferme->getNom(), ['uri' => prep_url($ferme->getUrl()), 'extras' => ['target' => '_blank']]);
        }
    }
}
