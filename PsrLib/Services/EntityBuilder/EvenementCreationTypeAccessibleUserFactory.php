<?php

declare(strict_types=1);

namespace PsrLib\Services\EntityBuilder;

use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\User;

class EvenementCreationTypeAccessibleUserFactory
{
    /**
     * @return EvenementType[]
     */
    public function getTypeAccessibleForUser(User $user): array
    {
        $typeChoices = [];
        if ($user->isSuperAdmin()) {
            $typeChoices[] = EvenementType::SUPER_ADMIN;
            $typeChoices[] = EvenementType::REGION;
            $typeChoices[] = EvenementType::DEPARTEMENT;
        }
        if ($user->isPaysan()) {
            $typeChoices[] = EvenementType::FERME;
        }

        if ($user->isAmapAdmin()) {
            $typeChoices[] = EvenementType::AMAP;
        }
        if ($user->isRefProduit()) {
            $typeChoices[] = EvenementType::AMAP;
        }

        if ($user->isAdminDepartment()) {
            $typeChoices[] = EvenementType::DEPARTEMENT;
        }
        if ($user->isAdminRegion()) {
            $typeChoices[] = EvenementType::DEPARTEMENT;
            $typeChoices[] = EvenementType::REGION;
        }

        return array_unique($typeChoices, SORT_REGULAR);
    }
}
