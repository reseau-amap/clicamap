<?php

declare(strict_types=1);

namespace PsrLib\Services\Password;

class PasswordEncoder
{
    public function encodePassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function testPassword(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
