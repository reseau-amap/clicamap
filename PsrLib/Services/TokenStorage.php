<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Custom token storage implementation used as compatibility layer with SF Security Core.
 */
class TokenStorage implements TokenStorageInterface
{
    public function __construct(
        private readonly Authentification $authentification
    ) {
    }

    public function getToken(): TokenInterface
    {
        $currentUser = $this->authentification->getCurrentUser();
        if (null === $currentUser) {
            return new NullToken();
        }

        return new UsernamePasswordToken(
            $currentUser,
            null,
            ['ROLE_USER'],
        );
    }

    public function setToken(TokenInterface $token = null): void
    {
        throw new \LogicException('Not implemented');
    }
}
