<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContratProduit;

class ContractCalculator
{
    public function totalTTC(Contrat $contrat): Money
    {
        return $this->total($contrat, fn (ModeleContratProduit $mcp) => $mcp->getPrix()->getPrixTTC());
    }

    public function totalHT(Contrat $contrat): Money
    {
        return $this->total($contrat, fn (ModeleContratProduit $mcp) => $mcp->getPrix()->getPrixHT());
    }

    public function totalWithTva(Contrat $contrat, ?float $tva): Money
    {
        return $this->total($contrat, fn (ModeleContratProduit $mcp) => $mcp->getPrix()->getTva() === $tva ? $mcp->getPrix()->getTvaAmount() : Money::EUR(0));
    }

    private function total(Contrat $contrat, \Closure $priceCallback): Money
    {
        $total = Money::EUR(0);
        foreach ($contrat->getCellules() as $commande) {
            $produitPrixUnitaire = $priceCallback($commande->getModeleContratProduit());
            $produitTotal = $produitPrixUnitaire->multiply((string) $commande->getQuantite());
            $total = $total->add($produitTotal);
        }

        return $total;
    }
}
