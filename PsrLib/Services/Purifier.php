<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use PsrLib\ProjectLocation;

/**
 * Wrapper around HTMLPurfier.
 */
class Purifier
{
    final public const CONFIG_NO_HTML = 'CONFIG_NO_HTML';
    final public const CONFIG_SUMMERNOTE = 'CONFIG_SUMMERNOTE';
    final public const CONFIG_SUMMERNOTE_SINGLELINE = 'CONFIG_SUMMERNOTE_SINGLELINE';
    final public const CONFIG_SUMMERNOTE_LITE = 'CONFIG_SUMMERNOTE_LITE';
    final public const CONFIG_MODAL = 'CONFIG_MODAL';

    private readonly \HTMLPurifier $purifier;

    /**
     * @var \HTMLPurifier_Config[]
     */
    private array $CONFIG_MAP = [];

    public function __construct()
    {
        $cacheDir = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/htmlpurifier';
        if (!is_dir($cacheDir) && !mkdir($cacheDir, 0o777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $cacheDir));
        }

        $configSummernote = \HTMLPurifier_Config::createDefault();
        $configSummernote->set('HTML.Allowed', 'p[align|style],br,b,u,ol,ul,li,a[href],pre,blockquote,h1,h2,h3,h4,h5,h6,span[style],font[color],table[class],tbody,tr,td');
        $configSummernote->set('CSS.AllowedProperties', ['background-color', 'margin-left']);
        $configSummernote->set('Core.RemoveProcessingInstructions', true);
        $configSummernote->set('Cache.SerializerPath', $cacheDir);
        $configSummernote->set('Attr.AllowedClasses', ['table', 'table-bordered']);
        $this->CONFIG_MAP[self::CONFIG_SUMMERNOTE] = $configSummernote;

        $configSummernoteLite = \HTMLPurifier_Config::createDefault();
        $configSummernoteLite->set('HTML.Allowed', 'p[align|style],br,b,u,i,a[href],font[color],span[style]');
        $configSummernoteLite->set('CSS.AllowedProperties', ['font-size',  'margin-left', 'text-align']);
        $configSummernoteLite->set('Core.RemoveProcessingInstructions', true);
        $configSummernoteLite->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_SUMMERNOTE_LITE] = $configSummernoteLite;

        $configSummernoteSingleLine = \HTMLPurifier_Config::createDefault();
        $configSummernoteSingleLine->set('HTML.Allowed', 'b,u');
        $configSummernoteSingleLine->set('CSS.AllowedProperties', '');
        $configSummernoteSingleLine->set('Core.RemoveProcessingInstructions', true);
        $configSummernoteSingleLine->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_SUMMERNOTE_SINGLELINE] = $configSummernoteSingleLine;

        $configModal = \HTMLPurifier_Config::createDefault();
        $configModal->set('HTML.Allowed', 'u,strong,br');
        $configModal->set('CSS.AllowedProperties', '');
        $configModal->set('Core.RemoveProcessingInstructions', true);
        $configModal->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_MODAL] = $configModal;

        $configNoHTML = \HTMLPurifier_Config::createDefault();
        $configNoHTML->set('HTML.Allowed', '');
        $configNoHTML->set('CSS.AllowedProperties', '');
        $configNoHTML->set('Core.RemoveProcessingInstructions', true);
        $configNoHTML->set('Cache.SerializerPath', $cacheDir);
        $this->CONFIG_MAP[self::CONFIG_NO_HTML] = $configNoHTML;

        $this->purifier = new \HTMLPurifier();
    }

    public function purify(string $input, string $filter = self::CONFIG_NO_HTML): string
    {
        return $this->purifier->purify($input, $this->CONFIG_MAP[$filter]);
    }
}
