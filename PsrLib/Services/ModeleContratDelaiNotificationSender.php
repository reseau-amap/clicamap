<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;

class ModeleContratDelaiNotificationSender
{
    public function __construct(
        private readonly Email_Sender $emailSender,
        private readonly ModeleContratRepository $modeleContratRepository,
        private readonly AmapienRepository $amapienRepository
    ) {
    }

    public function mcDelai0(): void
    {
        // FERMES ----------------------------------------------------------------
        $mcs = $this
            ->modeleContratRepository
            ->findDelai0NonArchived()
        ;
        $messages = [];

        foreach ($mcs as $mc) {
            $emails = [];
            $fermeReferents = $this->amapienRepository->getRefProduitFromMc($mc);
            foreach ($fermeReferents as $fermeReferent) {
                foreach ($fermeReferent->getUser()->getEmails() as $email) {
                    $emails[] = $email;
                }
            }
            $amapAdmins = $mc->getLivraisonLieu()->getAmap()->getAdmins();
            foreach ($amapAdmins as $amapAdmin) {
                foreach ($amapAdmin->getEmails() as $email) {
                    $emails[] = $email;
                }
            }

            $messages[] = [
                'mc' => $mc,
                'emails' => $emails,
            ];
        }

        foreach ($messages as $message) {
            try {
                $this->emailSender->envoyerMcDelai0Notif(
                    $message['mc'],
                    $message['emails']
                );
            } catch (\Exception $e) {
                sentryCapture($e);
            }
            usleep(100000);
        }
    }
}
