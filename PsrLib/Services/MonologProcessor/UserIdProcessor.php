<?php

declare(strict_types=1);

namespace PsrLib\Services\MonologProcessor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;
use PsrLib\Services\Authentification;

class UserIdProcessor implements ProcessorInterface
{
    private Authentification $authentification;

    public function __construct(Authentification $authentification)
    {
        $this->authentification = $authentification;
    }

    public function __invoke(LogRecord $record): LogRecord
    {
        $currentUser = $this->authentification->getCurrentUser();

        $record->extra['user_id'] = $currentUser?->getId() ?? 'null';

        return $record;
    }
}
