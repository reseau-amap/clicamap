<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class FermeProduitVoter extends Voter
{
    final public const ACTION_FERME_PRODUCT_DELETE = 'ACTION_FERME_PRODUCT_DELETE';
    final public const ACTION_FERME_PRODUCT_EDIT = 'ACTION_FERME_PRODUCT_EDIT';

    public function __construct(private readonly AuthorizationCheckerInterface $securityChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_FERME_PRODUCT_DELETE,
            self::ACTION_FERME_PRODUCT_EDIT,
        ]) && $subject instanceof FermeProduit;
    }

    /**
     * @param ?FermeProduit $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_FERME_PRODUCT_DELETE:
                return $this->isGrantedFermeProductDelete($user, $subject);
            case self::ACTION_FERME_PRODUCT_EDIT:
                return $this->isGrantedFermeProductEdit($user, $subject);
        }

        return false;
    }

    public function isGrantedFermeProductDelete(User $user, FermeProduit $fermeProduit): bool
    {
        // Seul les paysans peuvent supprimer les produits des fermes
        if ($user->isPaysan()) {
            return $this->securityChecker->isGranted(FermeVoter::ACTION_FERME_DISPLAY, $fermeProduit->getFerme());
        }

        return false;
    }

    public function isGrantedFermeProductEdit(User $user, FermeProduit $fermeProduit): bool
    {
        $ferme = $fermeProduit->getFerme();

        if ($user->isAdminOf($fermeProduit->getFerme())) {
            return true;
        }

        if ($user->isPaysan()
            && $this->securityChecker->isGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme)
        ) {
            return true;
        }

        if ($user->isRefProduitOfFerme($ferme)
          && $fermeProduit->isEnabled()
          && $this->securityChecker->isGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme)
        ) {
            return true;
        }

        if ($user->isAmapAdmin()
            && $fermeProduit->isEnabled()
            && $this->securityChecker->isGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme)
        ) {
            return true;
        }

        return false;
    }
}
