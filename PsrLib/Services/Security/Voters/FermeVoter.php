<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class FermeVoter extends Voter
{
    final public const ACTION_FERME_LIST = 'ACTION_FERME_LIST';
    final public const ACTION_FERME_LIST_REFPROD = 'ACTION_FERME_LIST_REFPROD';
    final public const ACTION_FERME_CREATE = 'ACTION_FERME_CREATE';
    final public const ACTION_FERME_DOWNLOAD = 'ACTION_FERME_DOWNLOAD';
    final public const ACTION_FERME_REF_PRODUITS = 'ACTION_FERME_REF_PRODUITS';
    final public const ACTION_FERME_DISPLAY = 'ACTION_FERME_DISPLAY';
    final public const ACTION_FERME_DISPLAY_OWN = 'ACTION_FERME_DISPLAY_OWN';
    final public const ACTION_FERME_REMOVE = 'ACTION_FERME_REMOVE';
    final public const ACTION_FERME_TYPE_PRODUCTION_MANAGE = 'ACTION_FERME_TYPE_PRODUCTION_MANAGE';
    final public const ACTION_FERME_CHANGE_ADHESION = 'ACTION_FERME_CHANGE_ADHESION';
    final public const ACTION_FERME_LIVRAISON_LIST = 'ACTION_FERME_LIVRAISON_LIST';
    final public const ACTION_FERME_EDIT_COMMENT = 'ACTION_FERME_EDIT_COMMENT';

    public function __construct(private readonly AuthorizationCheckerInterface $securityChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_FERME_LIST,
            self::ACTION_FERME_LIST_REFPROD,
            self::ACTION_FERME_CREATE,
            self::ACTION_FERME_DOWNLOAD,
            self::ACTION_FERME_REF_PRODUITS,
            self::ACTION_FERME_DISPLAY,
            self::ACTION_FERME_DISPLAY_OWN,
            self::ACTION_FERME_REMOVE,
            self::ACTION_FERME_TYPE_PRODUCTION_MANAGE,
            self::ACTION_FERME_CHANGE_ADHESION,
            self::ACTION_FERME_LIVRAISON_LIST,
            self::ACTION_FERME_EDIT_COMMENT,
        ]) && ($subject instanceof Ferme || null === $subject);
    }

    /**
     * @param ?Ferme $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_FERME_LIST:
                return $this->isGrantedFermeList($user);
            case self::ACTION_FERME_LIST_REFPROD:
                return $user->isRefProduit();
            case self::ACTION_FERME_CREATE:
                return $this->isGrantedFermeCreate($user);
            case self::ACTION_FERME_DOWNLOAD:
                return $this->isGrantedFermeDownload($user);
            case self::ACTION_FERME_REF_PRODUITS:
                return $this->isGrantedRefProduit($user, $subject);
            case self::ACTION_FERME_DISPLAY:
                return $this->isGrantedFermeDisplay($user, $subject);
            case self::ACTION_FERME_DISPLAY_OWN:
                return $user->isPaysanActifOf($subject);
            case self::ACTION_FERME_REMOVE:
                return $this->isGrantedFermeRemove($user, $subject);
            case self::ACTION_FERME_TYPE_PRODUCTION_MANAGE:
                return $this->isGrantedTypeProductionManage($user, $subject);
            case self::ACTION_FERME_CHANGE_ADHESION:
                return $this->isGrantedPaysanChangeAdhesion($user);
            case self::ACTION_FERME_LIVRAISON_LIST:
                return $this->isGrantedFermeLivraisonList($user, $subject);
            case self::ACTION_FERME_EDIT_COMMENT:
                return $this->isGrantedFermeEditComment($user, $subject);
        }

        return false;
    }

    public function isGrantedFermeList(User $user): bool
    {
        if ($user->isAmapAdmin()) {
            return true;
        }

        return $user->isAdmin();
    }

    public function isGrantedFermeCreate(User $user): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isFermeRegroupementAdmin()) {
            return true;
        }

        if ($user->isAmapAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedFermeDownload(User $user): bool
    {
        return $user->isAdmin();
    }

    public function isGrantedFermeDisplay(User $user, Ferme $ferme): bool
    {
        if ($user->isAmapAdmin()) {
            return true;
        }

        // Les rôles suivant peuvent éditer toutes les fermes
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isRefProduitOfFerme($ferme)) {
            return true;
        }

        // Les paysans ont accès à leur ferme
        if ($user->isPaysanActifOf($ferme)) {
            return true;
        }

        if ($user->isFermeRegroupementAdminOf($ferme)) {
            return true;
        }

        return false;
    }

    public function isGrantedRefProduit(User $user, Ferme $ferme): bool
    {
        if ($user->isFermeRegroupementAdminOf($ferme)) {
            return false;
        }

        return $this->securityChecker->isGranted(self::ACTION_FERME_DISPLAY, $ferme);
    }

    public function isGrantedFermeRemove(User $user, Ferme $ferme): bool
    {
        return $user->isAdmin()
            && $ferme->getModeleContrats()->isEmpty();
    }

    public function isGrantedTypeProductionManage(User $user, Ferme $ferme): bool
    {
        // Les paysans peuvent gérer les types de production
        if ($user->isPaysan()) {
            return $this->isGrantedFermeDisplay($user, $ferme);
        }

        // Les rôles suivant peuvent gérer les types de production
        if ($user->isAdmin()) {
            return $this->isGrantedFermeDisplay($user, $ferme);
        }

        return false;
    }

    public function isGrantedPaysanChangeAdhesion(User $user): bool
    {
        return $user->isAdmin();
    }

    public function isGrantedFermeLivraisonList(User $user, Ferme $ferme): bool
    {
        return $user->isPaysanActifOf($ferme) && $ferme->isAdhesionActive();
    }

    public function isGrantedFermeEditComment(User $user, Ferme $ferme): bool
    {
        return $user->isAdminOf($ferme);
    }
}
