<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AdhesionAmapAmapienVoter extends Voter
{
    final public const ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD = 'ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD,
        ]) && ($subject instanceof AdhesionAmapAmapien || null === $subject);
    }

    /**
     * @param ?AdhesionAmapAmapien $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD:
                return $this->isGrantedDownload($user, $subject);
        }

        return false;
    }

    public function isGrantedDownload(User $user, ?AdhesionAmapAmapien $adhesionAmapAmapien): bool
    {
        if (null === $adhesionAmapAmapien) {
            return false;
        }

        if (null === $adhesionAmapAmapien->getVoucherFileName()) {
            return false;
        }

        return $user->isAmapAdminOfAmap($adhesionAmapAmapien->getCreator())
            || ($user === $adhesionAmapAmapien->getAmapien()?->getUser());
    }
}
