<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserTypeVoter extends Voter
{
    final public const USER_AMAP = 'USER_AMAP';
    final public const USER_AMAPIEN = 'USER_AMAPIEN';
    final public const USER_PAYSAN = 'USER_PAYSAN';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::USER_AMAP,
            self::USER_AMAPIEN,
            self::USER_PAYSAN,
        ])
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::USER_AMAP:
                return $user->isAmapAdmin();
            case self::USER_AMAPIEN:
                return $user->isAmapien();
            case self::USER_PAYSAN:
                return $user->isPaysan();
        }

        return false;
    }
}
