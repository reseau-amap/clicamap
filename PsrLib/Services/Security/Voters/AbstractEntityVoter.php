<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

abstract class AbstractEntityVoter implements CacheableVoterInterface
{
    protected readonly EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return class-string[]
     */
    abstract protected function getEntityClasses(): array;

    /**
     * @param string|class-string $subjectType
     */
    public function supportsType(string $subjectType): bool
    {
        $subjectClass = null;
        try {
            $subjectClass = $this->em->getClassMetadata($subjectType)->name; // @phpstan-ignore-line
        } catch (\Exception $e) {
            return false;
        }

        foreach ($this->getEntityClasses() as $entityClass) {
            if (is_a($subjectClass, $entityClass, true)) {
                return true;
            }
        }

        return false;
    }
}
