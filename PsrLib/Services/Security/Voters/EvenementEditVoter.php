<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\EvenementAmap;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EvenementEditVoter extends AbstractEntityVoter
{
    final public const ACTION_EVENEMENT_EDIT = 'ACTION_EVENEMENT_EDIT';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_EVENEMENT_EDIT === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [Evenement::class];
    }

    /**
     * @param Evenement $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return (
            $this->isGrantedUserAuthor($subject, $user)
            || $this->isGrantedEvtAmapAdmin($subject, $user)
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }

    private function isGrantedUserAuthor(Evenement $evenement, User $user): bool
    {
        return $evenement->getAuteur() === $user;
    }

    private function isGrantedEvtAmapAdmin(Evenement $evenement, User $user): bool
    {
        return $evenement instanceof EvenementAmap && $user->isAmapAdminOfAmap($evenement->getRelated());
    }
}
