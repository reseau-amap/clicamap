<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CampagneBulletinRecuActionVoter extends AbstractEntityVoter
{
    final public const ACTION_CAMPAGNE_BULLETIN_RECU_GENERATE_ACTION = 'ACTION_CAMPAGNE_BULLETIN_RECU_GENERATE_ACTION';
    final public const ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION = 'ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION';
    final public const ACTION_CAMPAGNE_BULLETIN_RECU_ACTION = 'ACTION_CAMPAGNE_BULLETIN_RECU_ACTION';

    public function supportsAttribute(string $attribute): bool
    {
        return in_array($attribute, [
            self::ACTION_CAMPAGNE_BULLETIN_RECU_GENERATE_ACTION,
            self::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION,
            self::ACTION_CAMPAGNE_BULLETIN_RECU_ACTION,
        ], true);
    }

    protected function getEntityClasses(): array
    {
        return [CampagneBulletin::class];
    }

    /**
     * @param CampagneBulletin $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        if (in_array(self::ACTION_CAMPAGNE_BULLETIN_RECU_GENERATE_ACTION, $attributes, true) && null !== $subject->getRecus()) {
            return self::ACCESS_DENIED;
        }

        if (in_array(self::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION, $attributes, true) && null === $subject->getRecus()) {
            return self::ACCESS_DENIED;
        }

        $amap = $subject->getCampagne()->getAmap();

        return (
            (!$amap->getAssociationDeFait() && $user->getAdminAmaps()->contains($amap))
            || ($amap->getAssociationDeFait() && $user->isAdminOf($amap))
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
