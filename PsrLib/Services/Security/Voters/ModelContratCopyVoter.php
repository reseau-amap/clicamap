<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\DTO\ControllerModeleContratCopyArgumentDTO;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ModelContratCopyVoter extends Voter
{
    final public const ACTION_CONTRACT_MODEL_COPY = 'ACTION_CONTRACT_MODEL_COPY';

    public function __construct(private readonly ModeleContratRepository $modelContratRepository)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_CONTRACT_MODEL_COPY,
        ]) && $subject instanceof ControllerModeleContratCopyArgumentDTO;
    }

    /**
     * @param ?ControllerModeleContratCopyArgumentDTO $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_CONTRACT_MODEL_COPY:
                return $this->isGrantedModeleContratCopy($user, $subject);
        }

        return false;
    }

    public function isGrantedModeleContratCopy(User $user, ControllerModeleContratCopyArgumentDTO $argDto): bool
    {
        return $this->modelContratRepository->countV2ByAmapFerme($argDto->getAmap(), $argDto->getFerme()) > 0
            && (
                $user->isAmapAdminOfAmap($argDto->getAmap())
                || ($user->isAmapienOfAmap($argDto->getAmap()) && $user->isRefProduitOfFerme($argDto->getFerme()))
            )
        ;
    }
}
