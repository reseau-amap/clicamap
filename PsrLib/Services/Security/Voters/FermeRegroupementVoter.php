<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class FermeRegroupementVoter extends Voter
{
    final public const ACTION_FERME_REGROUPEMENT_LIST = 'ACTION_FERME_REGROUPEMENT_LIST';
    final public const ACTION_FERME_REGROUPEMENT_MANAGE = 'ACTION_FERME_REGROUPEMENT_MANAGE';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_FERME_REGROUPEMENT_LIST,
            self::ACTION_FERME_REGROUPEMENT_MANAGE,
        ]) && (null === $subject || $subject instanceof FermeRegroupement)
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_FERME_REGROUPEMENT_LIST:
                return $user->isAdminRegion() || $user->isSuperAdmin();
            case self::ACTION_FERME_REGROUPEMENT_MANAGE:
                return $user->getAdminFermeRegroupements()->contains($subject);
        }

        return false;
    }
}
