<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AdhesionAmapienVoter extends Voter
{
    final public const ACTION_ADHESION_AMAPIEN_DOWNLOAD = 'ACTION_ADHESION_AMAPIEN_DOWNLOAD';
    final public const ACTION_ADHESION_AMAPIEN_LIST_SELF = 'ACTION_ADHESION_AMAPIEN_LIST_SELF';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_ADHESION_AMAPIEN_DOWNLOAD,
            self::ACTION_ADHESION_AMAPIEN_LIST_SELF,
        ]) && ($subject instanceof AdhesionAmapien || null === $subject);
    }

    /**
     * @param ?AdhesionAmapien $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_ADHESION_AMAPIEN_LIST_SELF:
                return $user->isAmapien();
            case self::ACTION_ADHESION_AMAPIEN_DOWNLOAD:
                return $this->isGrantedDownload($user, $subject);
        }

        return false;
    }

    public function isGrantedDownload(User $user, ?AdhesionAmapien $adhesionAmapien): bool
    {
        if (null === $adhesionAmapien) {
            return false;
        }

        if (null === $adhesionAmapien->getVoucherFileName()) {
            return false;
        }

        if ($user->isAmapien() && $user === $adhesionAmapien->getAmapien()) {
            return true;
        }
        if ($user->isAdminOf($adhesionAmapien->getAmapien())) {
            return true;
        }

        return false;
    }
}
