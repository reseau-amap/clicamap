<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserPasswordStrengthVoter extends Voter
{
    final public const ACTION_USER_ESTIMATE_PASSWORD_STRENGTH = 'ACTION_USER_ESTIMATE_PASSWORD_STRENGTH';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::ACTION_USER_ESTIMATE_PASSWORD_STRENGTH])
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        return $subject instanceof User
            && null !== $subject->getPasswordResetToken()
            && $subject->getPasswordResetToken()->isValid()
        ;
    }
}
