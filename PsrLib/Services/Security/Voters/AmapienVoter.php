<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AmapienVoter extends Voter
{
    final public const ACTION_AMAPIEN_LIST = 'ACTION_AMAPIEN_LIST';
    final public const ACTION_AMAPIEN_REMOVE = 'ACTION_AMAPIEN_REMOVE';
    final public const ACTION_AMAPIEN_EDIT_EXTERNAL = 'ACTION_AMAPIEN_EDIT_EXTERNAL';
    final public const ACTION_AMAPIEN_CHANGE_NAME = 'ACTION_AMAPIEN_CHANGE_NAME';
    final public const ACTION_AMAPIEN_DISPLAY_PROFIL = 'ACTION_AMAPIEN_DISPLAY_PROFIL';
    final public const ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN = 'ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_AMAPIEN_LIST,
            self::ACTION_AMAPIEN_REMOVE,
            self::ACTION_AMAPIEN_EDIT_EXTERNAL,
            self::ACTION_AMAPIEN_CHANGE_NAME,
            self::ACTION_AMAPIEN_DISPLAY_PROFIL,
            self::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN,
        ]) && ($subject instanceof Amapien || null === $subject);
    }

    /**
     * @param ?Amapien $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_AMAPIEN_DISPLAY_PROFIL:
                return $this->isGrantedAmapienDisplayProfil($user, $subject);
            case self::ACTION_AMAPIEN_REMOVE:
            case self::ACTION_AMAPIEN_EDIT_EXTERNAL:
                return $this->isGrantedAmapienEditExternal($user, $subject);
            case self::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN:
                return $user->isAmapien();
            case self::ACTION_AMAPIEN_LIST:
                return $user->isAdmin();
        }

        return false;
    }

    public function isGrantedAmapienDisplayProfil(User $user, ?Amapien $subject): bool
    {
        if (null === $subject) {
            return false;
        }

        // Autorise les amapiens à éditer leur propre profil
        if ($user === $subject->getUser()) {
            return true;
        }

        // Si on peut éditer le profil d'un amampien, on peut le visualiser
        if (true === $this->isGrantedAmapienEditExternal($user, $subject)) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapienEditExternal(User $user, ?Amapien $subject): bool
    {
        if (null === $subject) {
            return false;
        }

        // Le super admin peut éditer tous les profils
        if ($user->isSuperAdmin()) {
            return true;
        }

        if ($user->isAdminOf($subject)) {
            return true;
        }

        // Autorise les amaps a éditer le profil de leurs membres
        if ($user->isAmapAdminOfAmapien($subject->getUser())) {
            return true;
        }

        return false;
    }
}
