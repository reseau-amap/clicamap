<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class CampagneListVoter implements CacheableVoterInterface
{
    final public const ACTION_CAMPAGNE_LIST = 'ACTION_CAMPAGNE_LIST';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_LIST === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return true;
    }

    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return $user->isAmapAdmin() ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
