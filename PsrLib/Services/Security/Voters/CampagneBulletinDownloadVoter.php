<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class CampagneBulletinDownloadVoter implements CacheableVoterInterface
{
    final public const ACTION_CAMPAGNE_BULLETIN_DOWNLOAD = 'ACTION_CAMPAGNE_BULLETIN_DOWNLOAD';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_BULLETIN_DOWNLOAD === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return CampagneBulletin::class === $subjectType;
    }

    /**
     * @param CampagneBulletin $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        $amap = $subject->getAmapien()->getAmap();

        return ($subject->getAmapien()->getUser() === $user
            || $user->getAdminAmaps()->contains($amap)
            || $user->isAdminOf($amap)
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
