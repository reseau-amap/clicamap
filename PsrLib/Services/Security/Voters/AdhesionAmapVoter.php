<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AdhesionAmapVoter extends Voter
{
    final public const ACTION_ADHESION_AMAP_DOWNLOAD = 'ACTION_ADHESION_AMAP_DOWNLOAD';
    final public const ACTION_ADHESION_AMAP_LIST_SELF = 'ACTION_ADHESION_AMAP_LIST_SELF';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_ADHESION_AMAP_DOWNLOAD,
            self::ACTION_ADHESION_AMAP_LIST_SELF,
        ]) && ($subject instanceof AdhesionAmap || null === $subject);
    }

    /**
     * @param ?AdhesionAmap $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_ADHESION_AMAP_LIST_SELF:
                return $user->isAmapAdmin();
            case self::ACTION_ADHESION_AMAP_DOWNLOAD:
                return $this->isGrantedDownload($user, $subject);
        }

        return false;
    }

    public function isGrantedDownload(User $user, ?AdhesionAmap $adhesionAmap): bool
    {
        if (null === $adhesionAmap) {
            return false;
        }

        if (null === $adhesionAmap->getVoucherFileName()) {
            return false;
        }

        if ($user->isSuperAdmin()) {
            return true;
        }

        if ($adhesionAmap->getCreator() === $user) {
            return true;
        }

        if ($user->isAmapAdminOfAmap($adhesionAmap->getAmap())) {
            return true;
        }

        return false;
    }
}
