<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use Carbon\Carbon;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DistributionVoter extends Voter
{
    final public const ACTION_DISTRIBUTION_DELETE = 'ACTION_DISTRIBUTION_DELETE';
    final public const ACTION_DISTRIBUTION_EDIT = 'ACTION_DISTRIBUTION_EDIT';
    final public const ACTION_DISTRIBUTION_DETAIL = 'ACTION_DISTRIBUTION_DETAIL';
    final public const ACTION_DISTRIBUTION_AMAPIEN_REGISTER = 'ACTION_DISTRIBUTION_AMAPIEN_REGISTER';
    final public const ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER = 'ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_DISTRIBUTION_DELETE,
            self::ACTION_DISTRIBUTION_EDIT,
            self::ACTION_DISTRIBUTION_DETAIL,
            self::ACTION_DISTRIBUTION_AMAPIEN_REGISTER,
            self::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER,
        ]) && ($subject instanceof AmapDistribution || null === $subject);
    }

    /**
     * @param ?AmapDistribution $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_DISTRIBUTION_EDIT:
            case self::ACTION_DISTRIBUTION_DELETE:
            case self::ACTION_DISTRIBUTION_DETAIL:
                return $this->isGrantedDelete($user, $subject);
            case self::ACTION_DISTRIBUTION_AMAPIEN_REGISTER:
                return $this->isGrantedAmapienRegister($user, $subject);
            case self::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER:
                return $this->isGrantedAmapienUnregister($user, $subject);
        }

        return false;
    }

    public function isGrantedDelete(User $user, AmapDistribution $amapDistribution): bool
    {
        return $user->isAmapAdminOfAmap($amapDistribution->getAmapLivraisonLieu()->getAmap());
    }

    public function isGrantedAmapienRegister(User $user, AmapDistribution $amapDistribution): bool
    {
        return $user->isAmapienOfAmap($amapDistribution->getAmapLivraisonLieu()->getAmap())
            && (!$amapDistribution->getAmapiens()->map(fn (Amapien $userAmapienAmap) => $userAmapienAmap->getUser())->contains($user))
            && Carbon::now()->lt($amapDistribution->getDateDebut())
            && $amapDistribution->getAmapiens()->count() < $amapDistribution->getDetail()->getNbPersonnes()
        ;
    }

    public function isGrantedAmapienUnregister(User $user, AmapDistribution $amapDistribution): bool
    {
        return $amapDistribution->getAmapiens()->map(fn (Amapien $userAmapienAmap) => $userAmapienAmap->getUser())->contains($user)
            && Carbon::now()->lt($amapDistribution->getDateDebut())
        ;
    }
}
