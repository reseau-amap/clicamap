<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DocumentVoter extends Voter
{
    final public const ACTION_DOCUMENT_MANAGE = 'ACTION_DOCUMENT_MANAGE';
    final public const ACTION_DOCUMENT_GETOWN = 'ACTION_DOCUMENT_GETOWN';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_DOCUMENT_MANAGE,
            self::ACTION_DOCUMENT_GETOWN,
        ])
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_DOCUMENT_MANAGE:
                return $user->isSuperAdmin();
            case self::ACTION_DOCUMENT_GETOWN:
                return !$user->hasNoRights();
        }

        return false;
    }
}
