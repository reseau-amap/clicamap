<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DocumentUtilisateurEditVoter extends AbstractEntityVoter
{
    final public const ACTION_DOCUMENT_UTILISATEUR_EDIT = 'ACTION_DOCUMENT_UTILISATEUR_EDIT';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_DOCUMENT_UTILISATEUR_EDIT === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [DocumentUtilisateurAmap::class, DocumentUtilisateurFerme::class];
    }

    /**
     * @param DocumentUtilisateurAmap|DocumentUtilisateurFerme $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return $user->isAdminOf($subject) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
