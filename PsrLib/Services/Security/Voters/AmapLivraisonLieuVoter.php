<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class AmapLivraisonLieuVoter extends Voter
{
    final public const ACTION_AMAP_LL_DELETE = 'ACTION_AMAP_LL_DELETE';

    public function __construct(private readonly AuthorizationCheckerInterface $securityChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_AMAP_LL_DELETE,
        ]) && ($subject instanceof AmapLivraisonLieu || null === $subject);
    }

    /**
     * @param ?AmapLivraisonLieu $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_AMAP_LL_DELETE:
                return $this->isGrantedAmapLLRemove($user, $subject);
        }

        return false;
    }

    public function isGrantedAmapLLRemove(User $user, AmapLivraisonLieu $ll): bool
    {
        if (!$this->securityChecker->isGranted(AmapVoter::ACTION_AMAP_DISPLAY, $ll->getAmap())) {
            return false;
        }

        // Une amap doit toujours avoir au moins un lieu de livraison
        if ($ll->getAmap()->getLivraisonLieux()->count() <= 1) {
            return false;
        }

        // Seul les lieux de livraison sans contrat associé sont autorisés
        return $ll->getModeleContrats()->isEmpty();
    }
}
