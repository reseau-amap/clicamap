<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class ModelContratVoter extends Voter
{
    final public const ACTION_CONTRACT_MODEL_LIST_ADMIN = 'ACTION_CONTRACT_MODEL_LIST_ADMIN';
    final public const ACTION_CONTRACT_MODEL_LIST_REFPROD = 'ACTION_CONTRACT_MODEL_LIST_REFPROD';
    final public const ACTION_CONTRACT_MODEL_LIST_AMAP = 'ACTION_CONTRACT_MODEL_LIST_AMAP';
    final public const ACTION_CONTRACT_MODEL_LIST_VALIDATE = 'ACTION_CONTRACT_MODEL_LIST_VALIDATE';
    final public const ACTION_CONTRACT_MODEL_DISPLAY = 'ACTION_CONTRACT_MODEL_DISPLAY';
    final public const ACTION_CONTRACT_MODEL_SUBSCRIBE = 'ACTION_CONTRACT_MODEL_SUBSCRIBE';
    final public const ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE = 'ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE';
    final public const ACTION_CONTRACT_MODEL_EDIT = 'ACTION_CONTRACT_MODEL_EDIT';
    final public const ACTION_CONTRACT_MODEL_SEND = 'ACTION_CONTRACT_MODEL_SEND';
    final public const ACTION_CONTRACT_MODEL_VALIDATE = 'ACTION_CONTRACT_MODEL_VALIDATE';
    final public const ACTION_CONTRACT_MODEL_PREVIEW_PDF = 'ACTION_CONTRACT_MODEL_PREVIEW_PDF';
    final public const ACTION_CONTRACT_MODEL_REMOVE = 'ACTION_CONTRACT_MODEL_REMOVE';
    final public const ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY = 'ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY';
    final public const ACTION_CONTRACT_MODEL_MOVE_DATE = 'ACTION_CONTRACT_MODEL_MOVE_DATE';
    final public const ACTION_CONTRACT_MODEL_CHANGE_DELAY = 'ACTION_CONTRACT_MODEL_CHANGE_DELAY';
    final public const ACTION_CONTRACT_MODEL_MANAGE_PAYMENT = 'ACTION_CONTRACT_MODEL_MANAGE_PAYMENT';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker, private readonly ModeleContratRepository $modelContratRepository, private readonly FermeRepository $fermeRepository)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_CONTRACT_MODEL_LIST_ADMIN,
            self::ACTION_CONTRACT_MODEL_LIST_REFPROD,
            self::ACTION_CONTRACT_MODEL_LIST_AMAP,
            self::ACTION_CONTRACT_MODEL_LIST_VALIDATE,
            self::ACTION_CONTRACT_MODEL_DISPLAY,
            self::ACTION_CONTRACT_MODEL_SUBSCRIBE,
            self::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE,
            self::ACTION_CONTRACT_MODEL_EDIT,
            self::ACTION_CONTRACT_MODEL_SEND,
            self::ACTION_CONTRACT_MODEL_VALIDATE,
            self::ACTION_CONTRACT_MODEL_PREVIEW_PDF,
            self::ACTION_CONTRACT_MODEL_REMOVE,
            self::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY,
            self::ACTION_CONTRACT_MODEL_MOVE_DATE,
            self::ACTION_CONTRACT_MODEL_CHANGE_DELAY,
            self::ACTION_CONTRACT_MODEL_MANAGE_PAYMENT,
        ]) && ($subject instanceof ModeleContrat || null === $subject);
    }

    /**
     * @param ?ModeleContrat $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_CONTRACT_MODEL_LIST_ADMIN:
                return $user->isAdmin();
            case self::ACTION_CONTRACT_MODEL_LIST_REFPROD:
                return $user->isRefProduit();
            case self::ACTION_CONTRACT_MODEL_LIST_AMAP:
                return $user->isAmapAdmin();
            case self::ACTION_CONTRACT_MODEL_LIST_VALIDATE:
                return $this->isGrantedContractModelListValidate($user);
            case self::ACTION_CONTRACT_MODEL_DISPLAY:
                return $this->isGrantedContractModelDisplay($user, $subject);
            case self::ACTION_CONTRACT_MODEL_SUBSCRIBE:
                return $this->isGrantedContractModelSubscribe($user, $subject);
            case self::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE:
                return $this->isGrantedContractModelSubscribeForce($user, $subject);
            case self::ACTION_CONTRACT_MODEL_SEND:
                return $this->isGrantedContractModelSend($user, $subject);
            case self::ACTION_CONTRACT_MODEL_EDIT:
                return $this->isGrantedContractModelEdit($user, $subject);
            case self::ACTION_CONTRACT_MODEL_VALIDATE:
                return $this->isGrantedContractModelValidate($user, $subject);
            case self::ACTION_CONTRACT_MODEL_PREVIEW_PDF:
                return $this->isGrantedContractModelPreviewPdf($user, $subject);
            case self::ACTION_CONTRACT_MODEL_REMOVE:
                return $this->isGrantedContractModelRemove($user, $subject);
            case self::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY:
                return $this->isGrantedContractModelPaymentSummary($user, $subject);
            case self::ACTION_CONTRACT_MODEL_MOVE_DATE:
                return $this->isGrantedContractModelMoveDate($user, $subject);
            case self::ACTION_CONTRACT_MODEL_CHANGE_DELAY:
                return $this->isGrantedChangeDelay($user, $subject);
            case self::ACTION_CONTRACT_MODEL_MANAGE_PAYMENT:
                return $this->isGrantedManagePayment($user, $subject);
        }

        return false;
    }

    public function isGrantedContractModelList(User $user): bool
    {
        // Les AMAPS peuvent lister les modèles de contrat
        if ($user->isAmapAdmin()) {
            return true;
        }

        // Les roles suivants peuvent lister les modèles de contrats
        if ($user->isAdmin() || $user->isRefProduit()) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelListValidate(User $user): bool
    {
        return $user->isPaysan() || $user->isFermeRegroupementAdmin();
    }

    public function isGrantedContractModelDisplay(User $user, ModeleContrat $mc): bool
    {
        if ($this->isGrantedAccessAsAmapOrRelated($user, $mc)) {
            return true;
        }

        if ($this->isGrantedAccessAsPaysan($user, $mc)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelSubscribeForce(User $user, ModeleContrat $mc): bool
    {
        if ($user->isAmapAdminOfAmap($mc->getAmap()) && $mc->isNowBeforeLastDate()) {
            return true;
        }

        // Les rôles suivant peuvent s'inscrire aux contrats vierges si ils ont accès à l'amap correspondante
        if ($user->isAdminRegion()
            || $user->isAdminDepartment()
            || $user->isRefProduitOfFerme($mc->getFerme())) {
            if (!$this->authorizationChecker->isGranted(AmapVoter::ACTION_AMAP_DISPLAY, $mc->getLivraisonLieu()->getAmap())) {
                return false;
            }

            // La date de dernière livraison ne doit pas etre dépassée
            return $mc->isNowBeforeLastDate();
        }

        return false;
    }

    public function isGrantedContractModelSubscribe(User $user, ModeleContrat $mc): bool
    {
        // On ne peut pas s'inscrire à un contrat qui n'est pas en v2
        if (2 !== (int) $mc->getVersion()) {
            return false;
        }

        $amap = $mc->getLivraisonLieu()->getAmap();

        if ($user->isAmapienOfAmap($amap)) {
            // L'amapien ne doit pas etre en liste d'attente
            foreach ($user->getAmapienAmaps() as $amapienAmap) {
                if ($amapienAmap->getAmap() === $amap && $amapienAmap->getFermeAttentes()->contains($mc->getFerme())) {
                    return false;
                }
            }

            // Les amapiens peuvent s'inscrire si la date n'est pas dépassée
            if ($mc->isForclusionPassed()) {
                return false;
            }

            // Verifie que le contrat est dans la liste des contrats que l'on peut souscrire
            $contractsSubscribable = $this
                ->modelContratRepository
                ->getSubscribableContractForAmapien($user)
            ;
            foreach ($contractsSubscribable as $contractSubscribable) {
                if ($contractSubscribable->getId() === $mc->getId()) {
                    return true;
                }
            }

            return false;
        }

        return false;
    }

    public function isGrantedContractModelSend(User $user, ModeleContrat $mc): bool
    {
        return in_array($mc->getEtat(), [ModeleContratEtat::ETAT_CREATION, ModeleContratEtat::ETAT_REFUS], true)
            && $this->isGrantedAccessAsAmapOrRelated($user, $mc)
        ;
    }

    public function isGrantedContractModelEdit(User $user, ModeleContrat $mc): bool
    {
        return in_array($mc->getEtat(), [ModeleContratEtat::ETAT_BROUILLON, ModeleContratEtat::ETAT_CREATION, ModeleContratEtat::ETAT_REFUS], true)
            && $this->isGrantedAccessAsAmapOrRelated($user, $mc)
        ;
    }

    public function isGrantedContractModelValidate(User $user, ModeleContrat $mc): bool
    {
        if (ModeleContratEtat::ETAT_VALIDATION_PAYSAN !== $mc->getEtat()) {
            return false;
        }

        $ferme = $mc->getFerme();

        if ($user->isFermeRegroupementAdminOf($ferme)) {
            return true;
        }

        if ($user->isPaysanActifOf($ferme)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelPreviewPdf(User $user, ModeleContrat $mc): bool
    {
        $mcFerme = $mc->getFerme();
        if (null === $mcFerme) {
            return false;
        }
        // Un paysan ou regroupement peuvent pré visualiser le contrat qu'ils peuvent valider
        if ($this->isGrantedContractModelValidate($user, $mc)) {
            return true;
        }

        // Les autres peuvent pré visualiser les contrats en statut "validation paysan" ou "création" ou "refus"
        if (in_array($mc->getEtat(), [ModeleContratEtat::ETAT_CREATION, ModeleContratEtat::ETAT_REFUS, ModeleContratEtat::ETAT_VALIDATION_PAYSAN], true)
            && $this->isGrantedContractModelDisplay($user, $mc)) {
            return true;
        }

        return false;
    }

    public function isGrantedContractModelRemove(User $user, ModeleContrat $mc): bool
    {
        // Le contrat doit etre visualisable
        if ($this->isGrantedAccessAsAmapOrRelated($user, $mc)) {
            return $mc->getContrats()->isEmpty();
        }

        return false;
    }

    public function isGrantedContractModelPaymentSummary(User $user, ModeleContrat $mc): bool
    {
        // Le contrat doit etre actif et en v2
        if (2 === $mc->getVersion() && ModeleContratEtat::ETAT_VALIDE === $mc->getEtat()) {
            // Le contrat doit etre visualisable
            return $this->isGrantedContractModelDisplay($user, $mc);
        }

        return false;
    }

    public function isGrantedContractModelMoveDate(User $user, ModeleContrat $mc): bool
    {
        return
            ($user->isAmapien() || $user->isAmapAdmin())
            && $this->authorizationChecker->isGranted('ACTION_CONTRACT_MODEL_DISPLAY', $mc)
            && !$mc->isArchived()
        ;
    }

    private function isGrantedAccessAsAmapOrRelated(User $user, ModeleContrat $mc): bool
    {
        $amap = $mc->getAmap();

        // Vérifie que le l'amap & la ferme sont cohérents. Utilisé pour la création du contrat
        $fermesAmap = $this
            ->fermeRepository
            ->getFromAmap($amap)
        ;
        if (!in_array($mc->getFerme(), $fermesAmap, true)) {
            return false;
        }

        // Une AMAP peut afficher les contrats qui lui sont liée
        if ($user->isAmapAdminOfAmap($amap)) {
            return true;
        }

        // Les super admin peuvent modifier tous les contrats vierge
        if ($user->isAdminOf($amap)) {
            return true;
        }

        // Les référents produits ont accès aux contrats vierges de leurs fermes
        if ($user->isAmapienOfAmap($amap) && $user->isRefProduitOfFerme($mc->getFerme())) {
            return true;
        }

        return false;
    }

    private function isGrantedAccessAsPaysan(User $user, ModeleContrat $mc): bool
    {
        return $user->isPaysanActifOf($mc->getFerme());
    }

    private function isGrantedChangeDelay(User $user, ModeleContrat $mc): bool
    {
        return !$mc->isArchived()
            && ($this->isGrantedAccessAsPaysan($user, $mc) || $this->isGrantedAccessAsAmapOrRelated($user, $mc));
    }

    private function isGrantedManagePayment(User $user, ModeleContrat $mc): bool
    {
        return $user->isAmapAdminOfAmap($mc->getAmap())
                || ($user->isRefProduitOfFerme($mc->getFerme()) && $user->isRefProduitOfAmap($mc->getAmap()))
            || $user->isPaysanOf($mc->getFerme())
        ;
    }
}
