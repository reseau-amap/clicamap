<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ImportVoter extends Voter
{
    final public const ACTION_IMPORT = 'ACTION_IMPORT';
    final public const ACTION_IMPORT_AMAP_PAYSAN = 'ACTION_IMPORT_AMAP_PAYSAN';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_IMPORT,
            self::ACTION_IMPORT_AMAP_PAYSAN,
        ])
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_IMPORT:
                return $this->isGrantedImport($user);
            case self::ACTION_IMPORT_AMAP_PAYSAN:
                return $this->isGrantedImportAmapPaysan($user);
        }

        return false;
    }

    public function isGrantedImport(User $user): bool
    {
        return $user->isAmapAdmin()
            || $this->isGrantedImportAmapPaysan($user)
        ;
    }

    public function isGrantedImportAmapPaysan(User $user): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }
}
