<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AmapVoter extends Voter
{
    final public const ACTION_AMAP_LIST = 'ACTION_AMAP_LIST';
    final public const ACTION_AMAP_LIST_OWN = 'ACTION_AMAP_LIST_OWN';
    final public const ACTION_AMAP_DISPLAY = 'ACTION_AMAP_DISPLAY';
    final public const ACTION_AMAP_CREATE = 'ACTION_AMAP_CREATE';
    final public const ACTION_AMAP_CHANGE_STATE = 'ACTION_AMAP_CHANGE_STATE';
    final public const ACTION_AMAP_SEND_PASSWORD = 'ACTION_AMAP_SEND_PASSWORD';
    final public const ACTION_AMAP_COMMENT = 'ACTION_AMAP_COMMENT';
    final public const ACTION_AMAP_SEARCH_PAYSAN = 'ACTION_AMAP_SEARCH_PAYSAN';
    final public const ACTION_AMAP_CHANGE_PASSWORD = 'ACTION_AMAP_CHANGE_PASSWORD';
    final public const ACTION_AMAP_SHOW_MAP = 'ACTION_AMAP_SHOW_MAP';
    final public const ACTION_AMAP_MANAGE_ABS = 'ACTION_AMAP_MANAGE_ABS';
    final public const ACTION_AMAP_CHANGE_ADHESION = 'ACTION_AMAP_CHANGE_ADHESION';
    final public const ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS = 'ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS';
    final public const ACTION_AMAP_CHANGE_REF_PROD = 'ACTION_AMAP_CHANGE_REF_PROD';
    final public const ACTION_AMAP_LIST_AMAPIENS = 'ACTION_AMAP_LIST_AMAPIENS';
    final public const ACTION_AMAP_CREATE_AMAPIENS = 'ACTION_AMAP_CREATE_AMAPIENS';
    final public const ACTION_DISTRIBUTION_LIST = 'ACTION_DISTRIBUTION_LIST';
    final public const ACTION_DISTRIBUTION_ADD = 'ACTION_DISTRIBUTION_ADD';
    final public const ACTION_CONTACT_OWN_AMAP = 'ACTION_CONTACT_OWN_AMAP';
    final public const ACTION_AMAP_EDIT_ADMINS = 'ACTION_AMAP_EDIT_ADMINS';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_AMAP_LIST,
            self::ACTION_AMAP_LIST_OWN,
            self::ACTION_AMAP_DISPLAY,
            self::ACTION_AMAP_CREATE,
            self::ACTION_AMAP_CHANGE_STATE,
            self::ACTION_AMAP_SEND_PASSWORD,
            self::ACTION_AMAP_COMMENT,
            self::ACTION_AMAP_SEARCH_PAYSAN,
            self::ACTION_AMAP_CHANGE_PASSWORD,
            self::ACTION_AMAP_SHOW_MAP,
            self::ACTION_AMAP_MANAGE_ABS,
            self::ACTION_AMAP_CHANGE_ADHESION,
            self::ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS,
            self::ACTION_AMAP_CHANGE_REF_PROD,
            self::ACTION_AMAP_LIST_AMAPIENS,
            self::ACTION_DISTRIBUTION_LIST,
            self::ACTION_DISTRIBUTION_ADD,
            self::ACTION_CONTACT_OWN_AMAP,
            self::ACTION_AMAP_CREATE_AMAPIENS,
            self::ACTION_AMAP_EDIT_ADMINS,
        ]) && ($subject instanceof Amap || null === $subject);
    }

    /**
     * @param ?Amap $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_DISTRIBUTION_LIST:
            case self::ACTION_AMAP_LIST_AMAPIENS:
            case self::ACTION_AMAP_LIST_OWN:
                return $user->isAmapAdmin();
            case self::ACTION_AMAP_LIST:
                return $this->isGrantedAmapList($user);
            case self::ACTION_AMAP_DISPLAY:
                return $this->isGrantedAmapDisplay($user, $subject);
            case self::ACTION_AMAP_CREATE:
                return $this->isGrantedAmapCreate($user);
            case self::ACTION_AMAP_COMMENT:
            case self::ACTION_AMAP_CHANGE_STATE:
            case self::ACTION_AMAP_CHANGE_ADHESION:
            case self::ACTION_AMAP_SEND_PASSWORD:
                return $this->isGrantedMapChangeState($user, $subject);
            case self::ACTION_DISTRIBUTION_ADD:
            case self::ACTION_AMAP_SEARCH_PAYSAN:
            case self::ACTION_AMAP_CHANGE_PASSWORD:
                return $user->isAmapAdminOfAmap($subject);
            case self::ACTION_AMAP_SHOW_MAP:
                return $this->isGrantedAmapShowMap($user, $subject);
            case self::ACTION_AMAP_MANAGE_ABS:
                return $this->isGrantedAmapManageAbs($user, $subject);
            case self::ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS:
                return $this->isGrantedAmapExportActivityIndicators($user);
            case self::ACTION_AMAP_CHANGE_REF_PROD:
                return $this->isGrantedAmapChangeRefProduct($user, $subject);
            case self::ACTION_CONTACT_OWN_AMAP:
                return $user->isAmapienOfAmap($subject);
            case self::ACTION_AMAP_EDIT_ADMINS:
            case self::ACTION_AMAP_CREATE_AMAPIENS:
                return $user->isAmapAdminOfAmap($subject) || $user->isAdminof($subject);
        }

        return false;
    }

    public function isGrantedAmapList(User $user): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedMapChangeState(User $user, Amap $amap): bool
    {
        // Le super admin peut changer l'état de  toutes les AMAPS
        if ($user->isSuperAdmin()) {
            return true;
        }

        // L'admin de région peut changer l'état de  toutes les AMAP de sa région
        if ($user->isAdminOf($amap)) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapChangeRefProduct(User $user, Amap $amap): bool
    {
        if ($user->isAmapAdminOfAmap($amap)) {
            return true;
        }

        if ($user->isAdmin()) {
            return $user->isSuperAdmin() || $user->isAdminOf($amap);
        }

        return false;
    }

    public function isGrantedAmapDisplay(User $user, Amap $amap): bool
    {
        if ($user->isAmapAdminOfAmap($amap)) {
            return true;
        }

        if ($this->isGrantedMapChangeState($user, $amap)) {
            return true;
        }

        if ($user->isRefProduitOfAmap($amap)) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapCreate(User $user): bool
    {
        return $user->isAdmin();
    }

    public function isGrantedAmapSearchPaysan(User $user): bool
    {
        return $user->isAmapAdmin();
    }

    public function isGrantedAmapShowMap(User $user, Amap $amap): bool
    {
        // Les comptent qui peuvent afficher l'amap peuvent afficher la carte
        if ($this->isGrantedAmapDisplay($user, $amap)) {
            return true;
        }

        // Les amapiens associés à l'amap peuvent afficher la carte
        if ($user->isAmapienOfAmap($amap)) {
            return true;
        }

        return false;
    }

    public function isGrantedAmapManageAbs(User $user, ?Amap $amap): bool
    {
        if (null === $amap) {
            return false;
        }

        if (true === $amap->getAmapEtudiante()) {
            return $this->isGrantedAmapDisplay($user, $amap);
        }

        return false;
    }

    public function isGrantedAmapExportActivityIndicators(User $user): bool
    {
        return $user->isAdminRegion() || $user->isAdminDepartment();
    }
}
