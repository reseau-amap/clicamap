<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    final public const ACTION_USER_MANAGE = 'ACTION_USER_MANAGE';
    final public const ACTION_USER_CHANGE_PASSWORD = 'ACTION_USER_CHANGE_PASSWORD';
    final public const ACTION_USER_ESTIMATE_PASSWORD_STRENGTH = 'ACTION_USER_ESTIMATE_PASSWORD_STRENGTH';
    final public const ACTION_USER_IS_AUTHENTICATED = 'ACTION_USER_IS_AUTHENTICATED';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::ACTION_USER_MANAGE, self::ACTION_USER_CHANGE_PASSWORD, self::ACTION_USER_IS_AUTHENTICATED])
        ;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_USER_CHANGE_PASSWORD:
                return $subject === $user;
            case self::ACTION_USER_IS_AUTHENTICATED:
                return true; // Check before switch
            case self::ACTION_USER_MANAGE:
                return $user->isSuperAdmin();
        }

        return false;
    }
}
