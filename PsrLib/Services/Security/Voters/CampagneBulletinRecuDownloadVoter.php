<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\CampagneBulletinRecus;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CampagneBulletinRecuDownloadVoter extends AbstractEntityVoter
{
    final public const ACTION_CAMPAGNE_BULLETIN_RECU_DOWNLOAD = 'ACTION_CAMPAGNE_BULLETIN_RECU_DOWNLOAD';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_BULLETIN_RECU_DOWNLOAD === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [CampagneBulletinRecus::class];
    }

    /**
     * @param CampagneBulletinRecus $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        $amap = $subject->getBulletin()->getAmapien()->getAmap();

        return ($subject->getBulletin()->getAmapien()->getUser() === $user
            || $user->getAdminAmaps()->contains($amap)
            || $user->isAdminOf($amap)
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
