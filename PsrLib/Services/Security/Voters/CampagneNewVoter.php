<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class CampagneNewVoter implements CacheableVoterInterface
{
    final public const ACTION_CAMPAGNE_NEW = 'ACTION_CAMPAGNE_NEW';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_NEW === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return Amap::class === $subjectType;
    }

    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return $user->isAmapAdminOfAmap($subject) || $user->isAdminOf($subject) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
