<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EvenementRemoveVoter extends AbstractEntityVoter
{
    final public const ACTION_EVENEMENT_ABONNEMENT_REMOVE = 'ACTION_EVENEMENT_ABONNEMENT_REMOVE';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_EVENEMENT_ABONNEMENT_REMOVE === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [EvenementAbonnement::class];
    }

    /**
     * @param EvenementAbonnement $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return $subject->getUser() === $user ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
