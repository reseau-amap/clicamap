<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContactVoter extends Voter
{
    final public const ACTION_CONTACT_ADMIN = 'ACTION_CONTACT_ADMIN';
    final public const ACTION_CONTACT_AMAP = 'ACTION_CONTACT_AMAP';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_CONTACT_ADMIN,
            self::ACTION_CONTACT_AMAP,
        ]) && null === $subject;
    }

    /**
     * @param ?Evenement $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_CONTACT_ADMIN:
                return $user->isPaysan() || $user->isAmapAdmin();
            case self::ACTION_CONTACT_AMAP:
                return $this->isGrantedContactAmap($user);
        }

        return false;
    }

    public function isGrantedContactAmap(User $user): bool
    {
        return $user->isPaysan();
    }

    public function isGrantedContactOwnAmap(User $user): bool
    {
        return $user->isAmapien();
    }
}
