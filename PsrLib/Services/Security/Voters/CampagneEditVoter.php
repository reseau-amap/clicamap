<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class CampagneEditVoter implements CacheableVoterInterface
{
    final public const ACTION_CAMPAGNE_EDIT = 'ACTION_CAMPAGNE_EDIT';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_EDIT === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return Campagne::class === $subjectType;
    }

    /**
     * @param Campagne $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        if (null !== $subject->getVersionSuivante()) {
            return self::ACCESS_DENIED;
        }

        return (
            ($subject->getAmap()->getAssociationDeFait() && $user->isAdminOf($subject->getAmap()))
            || $user->isAmapAdminOfAmap($subject->getAmap())
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
