<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CampagneBulletinRecuActionHeaderVoter extends AbstractEntityVoter
{
    final public const ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION_HEADER = 'ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION_HEADER';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION_HEADER === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [Amap::class];
    }

    /**
     * @param Amap $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return (
            (!$subject->getAssociationDeFait() && $user->getAdminAmaps()->contains($subject))
            || ($subject->getAssociationDeFait() && $user->isAdminOf($subject))
        ) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
