<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\EvenementCreationTypeAccessibleUserFactory;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class EvenementAddVoter implements CacheableVoterInterface
{
    final public const ACTION_EVENEMENT_ADD = 'ACTION_EVENEMENT_ADD';

    public function __construct(
        private readonly EvenementCreationTypeAccessibleUserFactory $evenementCreationTypeAccessibleUserFactory,
    ) {
    }

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_EVENEMENT_ADD === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return true;
    }

    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        $accessibleTypes = $this->evenementCreationTypeAccessibleUserFactory->getTypeAccessibleForUser($user);

        return count($accessibleTypes) > 0 ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
