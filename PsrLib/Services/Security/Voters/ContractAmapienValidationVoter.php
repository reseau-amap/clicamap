<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Contrat;
use PsrLib\Workflow\ContratStatusWorkflow;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContractAmapienValidationVoter implements CacheableVoterInterface
{
    final public const ACTION_CONTRACT_SIGNED_AMAPIEN_VALIDATION = 'ACTION_CONTRACT_SIGNED_AMAPIEN_VALIDATION';

    public function __construct(
        private readonly ContratStatusWorkflow $contratStatusWorkflow,
    ) {
    }

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CONTRACT_SIGNED_AMAPIEN_VALIDATION === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return Contrat::class === $subjectType;
    }

    /**
     * @param Contrat $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        if ($subject->getAmapien()->getUser() === $token->getUser()
            && $this->contratStatusWorkflow->can($subject, ContratStatusWorkflow::TRANSITION_AMAPIEN_VALIDATION)
            && $subject->isNowBeforeOrEqualFirstLivraisonDateWithFermeDelai()
        ) {
            return Voter::ACCESS_GRANTED;
        }

        return Voter::ACCESS_DENIED;
    }
}
