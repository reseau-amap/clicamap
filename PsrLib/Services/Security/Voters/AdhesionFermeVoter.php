<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AdhesionFermeVoter extends Voter
{
    final public const ACTION_ADHESION_FERME_DOWNLOAD = 'ACTION_ADHESION_FERME_DOWNLOAD';
    final public const ACTION_ADHESION_FERME_LIST_SELF = 'ACTION_ADHESION_FERME_LIST_SELF';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_ADHESION_FERME_DOWNLOAD,
            self::ACTION_ADHESION_FERME_LIST_SELF,
        ]) && ($subject instanceof AdhesionFerme || null === $subject);
    }

    /**
     * @param ?AdhesionFerme $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_ADHESION_FERME_LIST_SELF:
                return $this->isGrantedList($user);
            case self::ACTION_ADHESION_FERME_DOWNLOAD:
                return $this->isGrantedDownload($user, $subject);
        }

        return false;
    }

    public function isGrantedList(User $user): bool
    {
        return $user->isPaysan();
    }

    public function isGrantedDownload(User $user, ?AdhesionFerme $adhesionFerme): bool
    {
        if (null === $adhesionFerme) {
            return false;
        }

        if (null === $adhesionFerme->getVoucherFileName()) {
            return false;
        }

        if ($user->isAdminOf($adhesionFerme->getFerme())) {
            return true;
        }

        if ($user->isPaysanOf($adhesionFerme->getFerme())) {
            return true;
        }

        return false;
    }
}
