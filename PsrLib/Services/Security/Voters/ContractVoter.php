<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use Carbon\Carbon;
use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class ContractVoter extends Voter
{
    final public const ACTION_CONTRACT_SIGNED_LIST_ADMIN = 'ACTION_CONTRACT_SIGNED_LIST_ADMIN';
    final public const ACTION_CONTRACT_SIGNED_LIST_AMAP = 'ACTION_CONTRACT_SIGNED_LIST_AMAP';
    final public const ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF = 'ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF';
    final public const ACTION_CONTRACT_SIGNED_LIST_PAYSAN = 'ACTION_CONTRACT_SIGNED_LIST_PAYSAN';
    final public const ACTION_CONTRACT_SIGNED_DISPLAY = 'ACTION_CONTRACT_SIGNED_DISPLAY';
    final public const ACTION_CONTRACT_SIGNED_EDIT = 'ACTION_CONTRACT_SIGNED_EDIT';
    final public const ACTION_CONTRACT_SIGNED_REMOVE = 'ACTION_CONTRACT_SIGNED_REMOVE';
    final public const ACTION_CONTRACT_SIGNED_EDIT_FORCE = 'ACTION_CONTRACT_SIGNED_EDIT_FORCE';
    final public const ACTION_CONTRACT_SIGNED_DISPLAY_OWN = 'ACTION_CONTRACT_SIGNED_DISPLAY_OWN';
    final public const ACTION_CONTRACT_SIGNED_EXPORT_PDF = 'ACTION_CONTRACT_SIGNED_EXPORT_PDF';
    final public const ACTION_CONTRACT_SIGNED_EXPORT_XLS = 'ACTION_CONTRACT_SIGNED_EXPORT_XLS';
    final public const ACTION_CONTRACT_SIGNED_MOVE_DATE = 'ACTION_CONTRACT_SIGNED_MOVE_DATE';
    final public const ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT = 'ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT';
    final public const ACTION_CONTRACT_SIGNED_REDIRECT_CDP = 'ACTION_CONTRACT_SIGNED_REDIRECT_CDP';

    public function __construct(private readonly AuthorizationCheckerInterface $securityChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_CONTRACT_SIGNED_LIST_ADMIN,
            self::ACTION_CONTRACT_SIGNED_LIST_AMAP,
            self::ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF,
            self::ACTION_CONTRACT_SIGNED_LIST_PAYSAN,
            self::ACTION_CONTRACT_SIGNED_DISPLAY,
            self::ACTION_CONTRACT_SIGNED_EDIT,
            self::ACTION_CONTRACT_SIGNED_REMOVE,
            self::ACTION_CONTRACT_SIGNED_EDIT_FORCE,
            self::ACTION_CONTRACT_SIGNED_DISPLAY_OWN,
            self::ACTION_CONTRACT_SIGNED_EXPORT_PDF,
            self::ACTION_CONTRACT_SIGNED_EXPORT_XLS,
            self::ACTION_CONTRACT_SIGNED_MOVE_DATE,
            self::ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT,
            self::ACTION_CONTRACT_SIGNED_REDIRECT_CDP,
        ]) && ($subject instanceof Contrat || null === $subject);
    }

    /**
     * @param ?Contrat $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_CONTRACT_SIGNED_LIST_PAYSAN:
                return $user->isPaysan() || $user->isFermeRegroupementAdmin();
            case self::ACTION_CONTRACT_SIGNED_LIST_AMAP:
                return $user->isAmapAdmin();
            case self::ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF:
                return $user->isRefProduit();
            case self::ACTION_CONTRACT_SIGNED_LIST_ADMIN:
                return $user->isAdmin();
            case self::ACTION_CONTRACT_SIGNED_DISPLAY:
                return $this->isGrantedContractSignedDisplay($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_REMOVE:
                return $this->isGrantedContractSignedRemove($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_EDIT:
                return $this->isGrantedContractSignedEdit($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_EDIT_FORCE:
                return $this->isGrantedContractSignedEditForce($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_DISPLAY_OWN:
                return $this->isGrantedContractSignedDisplayOwn($user);
            case self::ACTION_CONTRACT_SIGNED_EXPORT_PDF:
                return $this->isGrantedContractSignedExportPdf($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_EXPORT_XLS:
                return $this->isGrantedContractSignedExportXls($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_MOVE_DATE:
                return $this->isGrantedContractSignedMoveDate($user, $subject);
            case self::ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT:
                return $this->isGrantedContractSignedExportAccount($user);
            case self::ACTION_CONTRACT_SIGNED_REDIRECT_CDP:
                return $this->isGrantedRedirectCdp($user, $subject);
        }

        return false;
    }

    public function isGrantedContractSignedList(User $user): bool
    {
        // Les paysans et les AMAPS peuvent lister les contrats signés

        return $user->isPaysan()
            || $user->isAmapAdmin()
            || $user->isRefProduit()
            || $user->isAdmin()
            || $user->isFermeRegroupementAdmin()
        ;
    }

    public function isGrantedContractSignedDisplay(User $user, Contrat $contrat): bool
    {
        // Les amaps ont accès au contrat signé pour lesquels ils ont accès au contrat vierge
        if ($user->isAmapAdminOfAmap($contrat->getModeleContrat()->getLivraisonLieu()->getAmap())) {
            return true;
        }

        if ($user->isPaysanActifOf($contrat->getModeleContrat()->getFerme())) {
            return true;
        }

        if ($user->isFermeRegroupementAdminOf($contrat->getModeleContrat()->getFerme())) {
            return true;
        }

        // Les amapiens peuvent visualiser leurs contrats
        if ($contrat->getAmapien()->getUser() === $user) {
            return true;
        }

        // Les super admin peuvent visulaliser tous les contrats signés
        if ($user->isSuperAdmin()) {
            return true;
        }

        // Les autres admin aux accès aux contrats pour lesquels ils ont accès au contrat vierge correspondant
        if ($user->isAdmin() || $user->isRefProduit()) { // Référent produit
            return $this
                ->securityChecker
                ->isGranted(
                    ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY,
                    $contrat->getModeleContrat()
                )
            ;
        }

        return false;
    }

    public function isGrantedContractSignedExportPdf(User $user, Contrat $contrat): bool
    {
        if (2 === $contrat->getModeleContrat()->getVersion() && null !== $contrat->getPdf()) {
            return $this
                ->securityChecker
                ->isGranted(
                    ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY,
                    $contrat
                )
            ;
        }

        return false;
    }

    public function isGrantedContractSignedExportXls(User $user, Contrat $contrat): bool
    {
        return !$contrat->getModeleContrat()->isArchived()
            && $this
                ->securityChecker
                ->isGranted(
                    ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY,
                    $contrat
                )
        ;
    }

    public function isGrantedContractSignedRemove(User $user, Contrat $contrat): bool
    {
        $mc = $contrat->getModeleContrat();
        if (2 !== (int) $mc->getVersion()) {
            return false;
        }

        if ($contrat->isCdp() && $contrat->isCdpPaid()) {
            return false;
        }

        if ($user->isAmapAdminOfAmap($mc->getLivraisonLieu()->getAmap()) && $mc->isNowBeforeLastDate()) {
            return true;
        }

        if ($user->isRefProduitOfFerme($mc->getFerme()) && $mc->isNowBeforeLastDate()) {
            return true;
        }

        return false;
    }

    public function isGrantedContractSignedEdit(User $user, Contrat $contrat): bool
    {
        return $contrat->getAmapien()->getUser() === $user && !$contrat->getModeleContrat()->isForclusionPassed();
    }

    public function isGrantedContractSignedEditForce(User $user, Contrat $contrat): bool
    {
        $amapien = $contrat->getAmapien();
        if (null === $amapien || $amapien->isDeleted()) {
            return false;
        }

        return $this->securityChecker->isGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $contrat->getModeleContrat());
    }

    public function isGrantedContractSignedDisplayOwn(User $user): bool
    {
        return $user->isAmapienActif();
    }

    public function isGrantedContractSignedMoveDate(User $user, Contrat $contrat): bool
    {
        $amapien = $contrat->getAmapien();
        if (null === $amapien || $amapien->isDeleted()) {
            return false;
        }
        // Seuls les contats en v2 peuvent etre déplacés
        $mc = $contrat->getModeleContrat();
        if (2 === $mc->getVersion()) {
            // La date de modification doit etre dépassée
            $now = new Carbon();
            if ($now->greaterThan($mc->getForclusion())) {
                // Seuls les référents produits & les amaps peuvent déplacer les livraisons
                if ($user->isAmapAdmin() || $user->isRefProduit()) {
                    if ($this->securityChecker->isGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc)) {
                        return true;
                    }
                }

                // Les amapiens peuvent déplacer les contrats dates dans certains cas
                if ($contrat->getAmapien()->getUser() === $user) {
                    $mcInfoLivraison = $mc->getInformationLivraison();

                    return $mcInfoLivraison->getAmapienGestionDeplacement()
                    && ($mcInfoLivraison->getAmapienPermissionDeplacementLivraison() || $mcInfoLivraison->getAmapienPermissionReportLivraison());
                }
            }
        }

        return false;
    }

    public function isGrantedContractSignedExportAccount(User $currentUser): bool
    {
        return $currentUser->isPaysan()
            && $currentUser->getPaysan()->getFermes()->reduce(
                fn (bool $carry, Ferme $ferme) => $carry || $ferme->isAdhesionActive(),
                false
            )
        ;
    }

    public function isGrantedRedirectCdp(User $user, Contrat $contrat): bool
    {
        return $contrat->isCdp()
            && $contrat->getAmapien()->getUser() === $user
        ;
    }
}
