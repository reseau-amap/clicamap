<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\AdhesionCreatorUser;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class AdhesionCreatorUserVoter extends Voter
{
    final public const ACTION_ADHESION_AMAP_LIST = 'ACTION_ADHESION_AMAP_LIST';
    final public const ACTION_ADHESION_FERME_LIST = 'ACTION_ADHESION_FERME_LIST';
    final public const ACTION_ADHESION_AMAPIEN_LIST = 'ACTION_ADHESION_AMAPIEN_LIST';

    final public const ACTION_ADHESION_AMAP_DELETE = 'ACTION_ADHESION_AMAP_DELETE';
    final public const ACTION_ADHESION_AMAP_GENERATE = 'ACTION_ADHESION_AMAP_GENERATE';
    final public const ACTION_ADHESION_FERME_GENERATE = 'ACTION_ADHESION_FERME_GENERATE';
    final public const ACTION_ADHESION_FERME_DELETE = 'ACTION_ADHESION_FERME_DELETE';
    final public const ACTION_ADHESION_AMAPIEN_GENERATE = 'ACTION_ADHESION_AMAPIEN_GENERATE';
    final public const ACTION_ADHESION_AMAPIEN_DELETE = 'ACTION_ADHESION_AMAPIEN_DELETE';

    final public const ACTION_ADHESION_IMPORT = 'ACTION_ADHESION_IMPORT';

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_ADHESION_AMAP_LIST,
            self::ACTION_ADHESION_FERME_LIST,
            self::ACTION_ADHESION_AMAPIEN_LIST,

            self::ACTION_ADHESION_AMAP_DELETE,
            self::ACTION_ADHESION_AMAP_GENERATE,
            self::ACTION_ADHESION_FERME_GENERATE,
            self::ACTION_ADHESION_FERME_DELETE,
            self::ACTION_ADHESION_AMAPIEN_GENERATE,
            self::ACTION_ADHESION_AMAPIEN_DELETE,

            self::ACTION_ADHESION_IMPORT,
        ]) && ($subject instanceof AdhesionCreatorUser || null === $subject);
    }

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    /**
     * @param ?AdhesionCreatorUser $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_ADHESION_AMAP_LIST:
            case self::ACTION_ADHESION_FERME_LIST:
            case self::ACTION_ADHESION_AMAPIEN_LIST:
                return $this->isGrantedList($user);
            case self::ACTION_ADHESION_AMAP_DELETE:
            case self::ACTION_ADHESION_AMAP_GENERATE:
            case self::ACTION_ADHESION_FERME_GENERATE:
            case self::ACTION_ADHESION_FERME_DELETE:
            case self::ACTION_ADHESION_AMAPIEN_GENERATE:
            case self::ACTION_ADHESION_AMAPIEN_DELETE:
                return $this->isGrantedDelete($user, $subject);

            case self::ACTION_ADHESION_IMPORT:
                return $this->isGrantedImport($user);
        }

        return false;
    }

    public function isGrantedList(User $user): bool
    {
        return $this->authorizationChecker->isGranted(self::ACTION_ADHESION_IMPORT)
            || $user->isSuperAdmin();
    }

    public function isGrantedDelete(User $user, AdhesionCreatorUser $adhesion): bool
    {
        if ($user->isSuperAdmin()) {
            return true;
        }

        return $user === $adhesion->getCreator();
    }

    public function isGrantedImport(User $user): bool
    {
        return $user->isAdminRegion() || $user->isAdminDepartment();
    }
}
