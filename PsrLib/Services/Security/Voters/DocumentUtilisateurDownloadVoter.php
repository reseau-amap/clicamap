<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class DocumentUtilisateurDownloadVoter extends AbstractEntityVoter
{
    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker, EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    final public const ACTION_DOCUMENT_UTILISATEUR_DOWNLOAD = 'ACTION_DOCUMENT_UTILISATEUR_DOWNLOAD';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_DOCUMENT_UTILISATEUR_DOWNLOAD === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [DocumentUtilisateurAmap::class, DocumentUtilisateurFerme::class];
    }

    /**
     * @param DocumentUtilisateurAmap|DocumentUtilisateurFerme $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        if ($this->authorizationChecker->isGranted(DocumentUtilisateurEditVoter::ACTION_DOCUMENT_UTILISATEUR_EDIT, $subject)) {
            return self::ACCESS_GRANTED;
        }

        if ($subject instanceof DocumentUtilisateurAmap && $user->isAmapAdminOfAmap($subject->getAmap())) {
            return self::ACCESS_GRANTED;
        }

        if ($subject instanceof DocumentUtilisateurFerme && $user->isPaysanOf($subject->getFerme())) {
            return self::ACCESS_GRANTED;
        }

        return self::ACCESS_DENIED;
    }
}
