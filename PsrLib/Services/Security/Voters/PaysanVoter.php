<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use DI\Attribute\Injectable;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

#[Injectable(lazy: true)] // Lazy loading to avoid circular references errors
class PaysanVoter extends Voter
{
    final public const ACTION_PAYSAN_DISPLAY_PROFIL = 'ACTION_PAYSAN_DISPLAY_PROFIL';
    final public const ACTION_PAYSAN_CREATE = 'ACTION_PAYSAN_CREATE';
    final public const ACTION_PAYSAN_DOWNLOAD_LIST = 'ACTION_PAYSAN_DOWNLOAD_LIST';
    final public const ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT = 'ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT';
    final public const ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL = 'ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL';
    final public const ACTION_PAYSAN_CHANGE_NAME = 'ACTION_PAYSAN_CHANGE_NAME';
    final public const ACTION_PAYSAN_CHANGE_ADHESION = 'ACTION_PAYSAN_CHANGE_ADHESION';
    final public const ACTION_PAYSAN_CHANGE_CR = 'ACTION_PAYSAN_CHANGE_CR';
    final public const ACTION_PAYSAN_LIST_ADMIN = 'ACTION_PAYSAN_LIST_ADMIN';
    final public const ACTION_PAYSAN_LIST_REFPROD = 'ACTION_PAYSAN_LIST_REFPROD';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::ACTION_PAYSAN_DISPLAY_PROFIL,
            self::ACTION_PAYSAN_CREATE,
            self::ACTION_PAYSAN_DOWNLOAD_LIST,
            self::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT,
            self::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL,
            self::ACTION_PAYSAN_CHANGE_NAME,
            self::ACTION_PAYSAN_CHANGE_ADHESION,
            self::ACTION_PAYSAN_CHANGE_CR,
            self::ACTION_PAYSAN_LIST_ADMIN,
            self::ACTION_PAYSAN_LIST_REFPROD,
        ]) && ($subject instanceof Paysan || null === $subject);
    }

    /**
     * @param ?Paysan $subject
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ACTION_PAYSAN_LIST_ADMIN:
                return $user->isAdmin() || $user->isAmapAdmin();
            case self::ACTION_PAYSAN_LIST_REFPROD:
                return $user->isRefProduit();
            case self::ACTION_PAYSAN_DISPLAY_PROFIL:
                return $this->isGrantedPaysanDisplayProfil($user, $subject);
            case self::ACTION_PAYSAN_CREATE:
                return $this->isGrantedPaysanCreate($user);
            case self::ACTION_PAYSAN_DOWNLOAD_LIST:
                return $this->isGrantedPaysanDownloadList($user);
            case self::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT:
                return $user->isFermeRegroupementAdmin();
            case self::ACTION_PAYSAN_CHANGE_NAME:
            case self::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL:
                return $this->editProfilExternal($user, $subject);
            case self::ACTION_PAYSAN_CHANGE_ADHESION:
                return $this->isGrantedPaysanChangeAdhesion($user, $subject);
            case self::ACTION_PAYSAN_CHANGE_CR:
                return $this->isGrantedPaysanChangeCR($user, $subject);
        }

        return false;
    }

    public function isGrantedPaysanDisplayProfil(User $user, ?Paysan $paysan): bool
    {
        if (null === $paysan) {
            return false;
        }

        // Un paysan peut éditer son propre profil
        if ($user === $paysan->getUser()) {
            return true;
        }

        foreach ($paysan->getFermes() as $paysanFerme) {
            if ($user->isFermeRegroupementAdminOf($paysanFerme)) {
                return true;
            }
        }

        return $this->authorizationChecker->isGranted(self::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan);
    }

    public function editProfilExternal(User $user, ?Paysan $paysan): bool
    {
        if (null === $paysan) {
            return false;
        }

        if ($user->isAdmin()) { // Admin de réseau
            return true;
        }

        // Le référent produit peut éditer les paysans des fermes dont il est référent
        foreach ($paysan->getFermes() as $paysanFerme) {
            if ($user->isRefProduitOfFerme($paysanFerme)) {
                return true;
            }
        }

        // L'amap peut éditer tous les paysans
        if ($user->isAmapAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedPaysanCreate(User $user): bool
    {
        if ($user->isAdmin() || $user->isRefProduit()) {
            return true;
        }

        if ($user->isAmapAdmin()) {
            return true;
        }

        if ($user->isFermeRegroupementAdmin()) {
            return true;
        }

        return false;
    }

    public function isGrantedPaysanDownloadList(User $user): bool
    {
        return $user->isSuperAdmin()
            || $user->isAdminRegion()
            || $user->isAdminDepartment()
        ;
    }

    public function isGrantedPaysanChangeAdhesion(User $user, ?Paysan $paysan): bool
    {
        // Seul les amapiens peuvent changer les années d'adhésion des paysans
        if ($user->isAdmin()) {
            return $this->editProfilExternal($user, $paysan);
        }

        return false;
    }

    public function isGrantedPaysanChangeCr(User $user, ?Paysan $paysan): bool
    {
        return $user->isAdminOf($paysan->getUser());
    }
}
