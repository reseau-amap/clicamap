<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class CampagneSubscriptionVoter implements CacheableVoterInterface
{
    final public const ACTION_CAMPAGNE_SUBSCRIBE = 'ACTION_CAMPAGNE_SUBSCRIBE';

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_CAMPAGNE_SUBSCRIBE === $attribute;
    }

    public function supportsType(string $subjectType): bool
    {
        return Campagne::class === $subjectType;
    }

    /**
     * @param Campagne $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        return $user->isAmapienOfAmap($subject->getAmap())
            && null === $subject->getVersionSuivante()
            && $subject->getBulletins()->filter(fn (CampagneBulletin $bulletin) => $bulletin->getAmapien()->getUser() === $user)->isEmpty()
        ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
