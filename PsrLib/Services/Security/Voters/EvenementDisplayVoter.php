<?php

declare(strict_types=1);

namespace PsrLib\Services\Security\Voters;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EvenementRepositoryUserAbonnement;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EvenementDisplayVoter extends AbstractEntityVoter
{
    final public const ACTION_EVENEMENT_DISPLAY = 'ACTION_EVENEMENT_DISPLAY';

    private readonly EvenementRepositoryUserAbonnement $evenementRepository;

    public function __construct(EntityManagerInterface $em, EvenementRepositoryUserAbonnement $evenementRepository)
    {
        parent::__construct($em);
        $this->evenementRepository = $evenementRepository;
    }

    public function supportsAttribute(string $attribute): bool
    {
        return self::ACTION_EVENEMENT_DISPLAY === $attribute;
    }

    protected function getEntityClasses(): array
    {
        return [Evenement::class];
    }

    /**
     * @param Evenement $subject
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return self::ACCESS_DENIED;
        }

        $userEvts = $this->evenementRepository->getForUser($user);

        return in_array($subject, $userEvts, true) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
