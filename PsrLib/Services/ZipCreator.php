<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\DTO\ZipCreatorFileDTO;

class ZipCreator
{
    /**
     * @param ZipCreatorFileDTO[] $files
     */
    public function createZipFiles($files): string
    {
        $zip = new \ZipArchive();
        $tmpFileName = sprintf('%s/bulk_download_%s.zip', sys_get_temp_dir(), \Ramsey\Uuid\Uuid::uuid4());
        if (true !== $zip->open($tmpFileName, \ZipArchive::CREATE)) {
            throw new \RuntimeException('Unable to create zip file'.$tmpFileName);
        }

        foreach ($files as $file) {
            $zip->addFile($file->getFile()->getFileFullPath(), $file->getFileName());
        }
        $zip->close();

        return $tmpFileName;
    }
}
