<?php

declare(strict_types=1);

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\ContractCalculator;
use PsrLib\Services\TvaLabel;

abstract class ExcelGeneratorExportContratAccountAbstract
{
    public function __construct(protected readonly ContractCalculator $contractCalculator, protected readonly TvaLabel $tvaLabel, protected ModeleContratRepository $modeleContratRepo)
    {
    }

    public function generate(SearchContratSigneState $contratSigneState, Ferme $ferme): string
    {
        $mcSearch = $contratSigneState->getMc();
        if (null === $mcSearch) {
            $mcs = $this
                ->modeleContratRepo
                ->findValidatedByFermeAmap(
                    $ferme,
                    $contratSigneState->getAmap()
                )
            ;
        } else {
            $mcs = [$mcSearch];
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getDefaultRowDimension()->setRowHeight(15);

        $this->generateSheet($sheet, $mcs, $ferme);

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportContractAccountDate_');
        $objWriter = new Xls($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }

    /**
     * @param ModeleContrat[] $mcs
     */
    abstract protected function generateSheet(Worksheet $sheet, $mcs, Ferme $ferme): void;

    protected function generateHeader(Worksheet $sheet, Ferme $ferme): int
    {
        $row = 1;
        $sheet->setCellValueByColumnAndRow(1, $row++, $ferme->getNom());
        $sheet->setCellValueByColumnAndRow(
            1,
            $row++,
            sprintf(
                '%s %s %s',
                $ferme->getLibAdr(),
                $ferme->getVille()->getCpString(),
                $ferme->getVille()->getNom()
            )
        );
        $sheet->setCellValueByColumnAndRow(1, $row++, $ferme->getSiret());
        if ($ferme->isTva()) {
            $sheet->setCellValueByColumnAndRow(1, $row++, null === $ferme->getTvaDate() ? '' : $ferme->getTvaDate()->format('d/m/Y'));
        }
        $sheet->setCellValueByColumnAndRow(1, $row++, sprintf(
            'Extrait le %s',
            Carbon::now()->format('d/m/Y H:i:s')
        ));

        return ++$row;
    }

    protected function getTVAChoices(): array
    {
        return array_filter(PrixTVA::TVA_CHOICES, fn (?float $tva) => null !== $tva);
    }
}
