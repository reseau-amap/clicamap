<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Repository\ContratLivraisonRepository;
use PsrLib\Workflow\ContratStatusWorkflow;

class ExcelGeneratorExportFermeLivraison
{
    /**
     * @var string[]
     *
     * @ref PhpSpreadsheet source code
     */
    private static array $invalidCharacters = ['*', ':', '/', '\\', '?', '[', ']', '\''];

    public function __construct(private readonly ContratLivraisonRepository $contratLivraisonRepo)
    {
    }

    /**
     * @param ModeleContrat[] $mcs
     */
    public function genererFermesLivraisons($mcs, Carbon $date): string
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($mcs as $mc) {
            $sheet->setTitle($this->slugName($mc->getNom()));
            $this->genererFeuille($sheet, $mc, $date);

            $sheet = $spreadsheet->createSheet();
        }
        $spreadsheet->removeSheetByIndex(count($mcs));

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportFermeLivraison_');
        $objWriter = new Xlsx($spreadsheet);
        $objWriter->save($outputFileName);

        return $outputFileName;
    }

    private function genererFeuille(Worksheet $sheet, ModeleContrat $mc, Carbon $date): void
    {
        $sheet->setCellValue('A1', $mc->getLivraisonLieu()->getAmap());
        $sheet->setCellValue('A2', $mc->getNom());
        $sheet->setCellValue('A3', sprintf('Feuille de livraison du %s', $date->format('Y-m-d')));
        $sheet->setCellValue('A4', 'Extrait le '.Carbon::now()->format('d/m/Y H:m:s'));
        $sheet->setCellValue('A6', 'Nom');
        $sheet->setCellValue('B6', 'Prénom');
        $sheet->setCellValue('A10', 'Cumul');

        $sheet->mergeCellsByColumnAndRow(
            1,
            6,
            1,
            8
        );
        $sheet->mergeCellsByColumnAndRow(
            2,
            6,
            2,
            8
        );

        // Add header
        $col = 3;
        foreach ($mc->getProduits() as $produit) {
            $sheet->getCellByColumnAndRow($col, 6)->setValue($produit->getTypeProduction());
            $sheet->getCellByColumnAndRow($col, 7)->setValue($produit->getNom());
            $sheet->getCellByColumnAndRow($col, 8)->setValue($produit->getConditionnement());
            $sheet->getCellByColumnAndRow($col + 1, 8)->setValue('Quantité ou poids livré');
            $sheet->mergeCellsByColumnAndRow(
                $col,
                6,
                $col + 1,
                6
            );
            $sheet->mergeCellsByColumnAndRow(
                $col,
                7,
                $col + 1,
                7
            );

            $col += 2;
        }

        /** @var Contrat[] $contrats */
        $contrats = $mc
            ->getContrats()
            ->filter(fn (Contrat $c) => ContratStatusWorkflow::STATE_VALIDATED === $c->getState())
            ->toArray()
        ;
        usort(
            $contrats,
            fn (Contrat $a, Contrat $b) => strnatcasecmp(
                $a->getAmapien()->getUser()->getName()->getLastName(),
                $b->getAmapien()->getUser()->getName()->getLastName()
            )
        );

        $row = 12;
        foreach ($contrats as $contrat) {
            if (0.0 === $contrat->getCellulesDateTotalQty($date)) {
                continue;
            }
            $amapien = $contrat->getAmapien();

            /** @var ContratLivraison $livraison */
            $livraison = $this->contratLivraisonRepo->findOneBy([
                'ferme' => $mc->getFerme(),
                'amapien' => $amapien,
                'date' => $date,
                'livre' => true,
            ]);

            $sheet->getCellByColumnAndRow(1, $row)->setValue($amapien->getUser()->getName()->getLastName());
            $sheet->getCellByColumnAndRow(2, $row)->setValue($amapien->getUser()->getName()->getFirstName());

            $col = 3;
            foreach ($mc->getProduits() as $produit) {
                $mcd = $mc
                    ->getDates()
                    ->filter(fn (ModeleContratDate $d) => $d->getDateLivraison()->format('Y-m-d') === $date->format('Y-m-d'))
                    ->first()
                ;
                if (false !== $mcd) {
                    $cell = $contrat->getCelluleDateProduit($produit, $mcd);
                    if (null !== $cell && $cell->getQuantite() > 0) {
                        $sheet->getCellByColumnAndRow($col, $row)->setValue($cell->getQuantite());
                        if (null !== $livraison && false === $cell->getModeleContratProduit()->getRegulPds()) {
                            $sheet->getCellByColumnAndRow($col + 1, $row)->setValue($cell->getQuantite());
                        }
                        if (null !== $livraison && true === $cell->getModeleContratProduit()->getRegulPds()) {
                            $livraisonCell = $livraison->getCelluleByContratCellule($cell);
                            if (null !== $livraisonCell) {
                                $sheet->getCellByColumnAndRow($col + 1, $row)->setValue($livraisonCell->getQuantite());
                            }
                        }
                    }
                }
                $col += 2;
            }

            ++$row;
        }

        $colMax = $mc->getProduits()->count() * 2 + 2;
        $rowMax = (12 + count($contrats));
        for ($col = 3; $col <= $colMax; ++$col) {
            $colString = Coordinate::stringFromColumnIndex($col);
            $sumStart = $colString.'12';
            $sumEnd = $colString.$rowMax;
            $sheet->getCellByColumnAndRow($col, 10)->setValue(
                sprintf('=SUM(%s:%s)', $sumStart, $sumEnd)
            );

            $sheet->getColumnDimension($colString)->setWidth(25);
            $sheet->getStyleByColumnAndRow($col, 1, $col, $rowMax)->applyFromArray([
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ],
            ]);
        }
        $sheet->getStyleByColumnAndRow(1, 6, $colMax, $rowMax)->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
        ]);
        $sheet->getStyleByColumnAndRow(1, 1, 2, $rowMax)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);
        $sheet->getStyleByColumnAndRow(1, 6, $colMax, 10)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $sheet->getColumnDimension('A')->setWidth(45);
        $sheet->getColumnDimension('B')->setWidth(35);
    }

    private function slugName(string $mcName): string
    {
        $res = str_replace(self::$invalidCharacters, '', $mcName);
        $res = trim($res, '\'');
        if (strlen($res) > Worksheet::SHEET_TITLE_MAXIMUM_LENGTH) {
            $res = substr($res, 0, Worksheet::SHEET_TITLE_MAXIMUM_LENGTH);
        }

        return $res;
    }
}
