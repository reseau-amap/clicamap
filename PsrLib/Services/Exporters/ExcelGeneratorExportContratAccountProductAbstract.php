<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Money\Money;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Services\MoneyHelper;

abstract class ExcelGeneratorExportContratAccountProductAbstract extends ExcelGeneratorExportContratAccountAbstract
{
    protected function generateSheet(Worksheet $sheet, $mcs, Ferme $ferme): void
    {
        $products = $this->extractProductsFromMultipleMcs($mcs);

        $row = $this->generateHeader($sheet, $ferme);
        $col = 1;

        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Dans le cadre de l’AMAP');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Nom du contrat');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Nom de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Prénom de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Adresse de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Code postal de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Ville de l’amapien');

        foreach ($products as $product) {
            $sheet->getColumnDimensionByColumn($col)->setWidth(22);
            $sheet->setCellValueByColumnAndRow(
                $col++,
                $row,
                sprintf(
                    '%s - %s',
                    $product['name'],
                    $product['hasRegul'] ? 'poids' : 'quantité'
                )
            );
        }

        if ($ferme->isTva()) {
            $sheet->getColumnDimensionByColumn($col)->setWidth(12);
            $sheet->setCellValueByColumnAndRow($col++, $row, 'TOTAL HT');
            foreach ($this->getTVAChoices() as $choice) {
                $sheet->getColumnDimensionByColumn($col)->setWidth(12);
                $sheet->setCellValueByColumnAndRow(
                    $col++,
                    $row,
                    null === $choice
                        ? $this->tvaLabel->getLabel($choice)
                        : 'TVA '.$this->tvaLabel->getLabel($choice)
                );
            }
        }
        $sheet->getColumnDimensionByColumn($col)->setWidth(12);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'TOTAL TTC');

        $sheet->getStyleByColumnAndRow(1, 1, $col, $row)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        ++$row;
        foreach ($mcs as $mc) {
            $mcProducts = $mc->getProduits()->toArray();
            foreach ($mc->getContrats() as $contrat) {
                $this->addRow($sheet, $contrat, $ferme, $mcProducts, $products, $row);
            }
        }
    }

    abstract protected function getProductQuantity(ModeleContratProduit $modeleContratProduit, Contrat $contrat): float;

    abstract protected function calculateTotalHT(Contrat $contrat): Money;

    abstract protected function calculateTotalWithTva(Contrat $contrat, ?float $tva): Money;

    abstract protected function calculateTotalTTC(Contrat $contrat): Money;

    /**
     * @param ModeleContratProduit[] $mcProducts
     */
    private function addRow(Worksheet $sheet, Contrat $contrat, Ferme $ferme, array $mcProducts, array $products, int &$row): void
    {
        $col = 1;
        $amapien = $contrat->getAmapien();
        $amapienUser = $amapien->getUser();
        $sheet->setCellValueByColumnAndRow($col++, $row, $contrat->getModeleContrat()->getLivraisonLieu()->getAmap()->getNom());
        $sheet->setCellValueByColumnAndRow($col++, $row, $contrat->getModeleContrat()->getNom());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getName()->getLastName());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getName()->getFirstName());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getAddress()->getAdress());
        $ville = $amapienUser->getVille();
        if (null !== $ville) {
            $sheet->setCellValueByColumnAndRow($col++, $row, $ville->getCpString());
            $sheet->setCellValueByColumnAndRow($col++, $row, $ville->getNom());
        } else {
            $col += 2;
        }

        foreach ($products as $product) {
            foreach ($mcProducts as $mcProduct) {
                $mcProductFermeProduct = $mcProduct->getFermeProduit();
                $mcProductFermeProductId = null === $mcProductFermeProduct ? null : $mcProductFermeProduct->getId();

                if (
                    (null !== $mcProductFermeProductId && $product['id'] === $mcProductFermeProductId)
                    || (null === $mcProductFermeProductId && $product['name'] === $mcProduct->getNom())
                ) {
                    $qty = $this->getProductQuantity($mcProduct, $contrat);
                    $sheet->setCellValueByColumnAndRow(
                        $col,
                        $row,
                        0.0 === $qty ? null : $qty
                    );
                }
            }
            ++$col;
        }

        if ($ferme->isTva()) {
            $sheet->setCellValueByColumnAndRow(
                $col++,
                $row,
                $this->printAmountOrEmpty($this->calculateTotalHT($contrat))
            );
            foreach ($this->getTVAChoices() as $choice) {
                $amount = $this->calculateTotalWithTva($contrat, $choice);
                $sheet->setCellValueByColumnAndRow(
                    $col++,
                    $row,
                    $this->printAmountOrEmpty($amount)
                );
            }
        }

        $sheet->setCellValueByColumnAndRow(
            $col,
            $row,
            $this->printAmountOrEmpty($this->calculateTotalTTC($contrat))
        );

        ++$row;
    }

    private function printAmountOrEmpty(Money $money): ?string
    {
        return '0' === $money->getAmount() ? null : MoneyHelper::toString($money);
    }

    /**
     * @param ModeleContrat[] $mcs
     */
    private function extractProductsFromMultipleMcs($mcs): array
    {
        $res = [];
        foreach ($mcs as $mc) {
            $mcps = $mc->getProduits();
            foreach ($mcps as $mcp) {
                $product = $mcp->getFermeProduit();

                $res[] = [
                    'id' => null === $product ? null : $product->getId(),
                    'name' => $mcp->getNom(),
                    'hasRegul' => $mcp->getRegulPds(),
                ];
            }
        }

        return array_unique($res, SORT_REGULAR);
    }
}
