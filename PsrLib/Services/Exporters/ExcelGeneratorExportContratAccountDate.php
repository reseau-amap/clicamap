<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\Services\MoneyHelper;

class ExcelGeneratorExportContratAccountDate extends ExcelGeneratorExportContratAccountAbstract
{
    protected function generateSheet(Worksheet $sheet, $mcs, Ferme $ferme): void
    {
        $nbReglements = max(array_map(fn (ModeleContrat $mc) => $mc->getReglementNbMax(), $mcs));

        $row = $this->generateHeader($sheet, $ferme);
        $col = 1;

        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Dans le cadre de l’AMAP');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Nom du contrat');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Nom de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Prénom de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(25);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Adresse de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(20);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Code postal de l’amapien');
        $sheet->getColumnDimensionByColumn($col)->setWidth(20);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'Ville de l’amapien');

        for ($i = 0; $i < $nbReglements; ++$i) {
            $sheet->getColumnDimensionByColumn($col)->setWidth(22);
            $sheet->setCellValueByColumnAndRow($col++, $row, 'Date de paiement '.($i + 1));
            $sheet->getColumnDimensionByColumn($col)->setWidth(22);
            $sheet->setCellValueByColumnAndRow($col++, $row, 'Montant du paiement '.($i + 1));
        }

        if ($ferme->isTva()) {
            $sheet->getColumnDimensionByColumn($col)->setWidth(12);
            $sheet->setCellValueByColumnAndRow($col++, $row, 'TOTAL HT');
            foreach ($this->getTVAChoices() as $choice) {
                $sheet->getColumnDimensionByColumn($col)->setWidth(12);
                $sheet->setCellValueByColumnAndRow(
                    $col++,
                    $row,
                    null === $choice
                        ? $this->tvaLabel->getLabel($choice)
                        : 'TVA '.$this->tvaLabel->getLabel($choice)
                );
            }
        }
        $sheet->getColumnDimensionByColumn($col)->setWidth(12);
        $sheet->setCellValueByColumnAndRow($col++, $row, 'TOTAL TTC');

        $sheet->getStyleByColumnAndRow(1, 1, $col, $row)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        ++$row;
        foreach ($mcs as $mc) {
            foreach ($mc->getContrats() as $contrat) {
                $this->addRow($sheet, $contrat, $ferme, $nbReglements, $row);
            }
        }
    }

    private function addRow(Worksheet $sheet, Contrat $contrat, Ferme $ferme, int $nbReglements, int &$row): void
    {
        $col = 1;
        $amapien = $contrat->getAmapien();
        $amapienUser = $amapien->getUser();
        $sheet->setCellValueByColumnAndRow($col++, $row, $contrat->getModeleContrat()->getLivraisonLieu()->getAmap()->getNom());
        $sheet->setCellValueByColumnAndRow($col++, $row, $contrat->getModeleContrat()->getNom());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getName()->getLastName());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getName()->getFirstName());
        $sheet->setCellValueByColumnAndRow($col++, $row, $amapienUser->getAddress()->getAdress());
        $ville = $amapienUser->getVille();
        if (null !== $ville) {
            $sheet->setCellValueByColumnAndRow($col++, $row, $ville->getCpString());
            $sheet->setCellValueByColumnAndRow($col++, $row, $ville->getNom());
        } else {
            $col += 2;
        }

        $contratDates = $contrat->getDatesReglements()->toArray();
        foreach ($contratDates as $datesReglement) {
            $sheet->setCellValueByColumnAndRow($col++, $row, $datesReglement->getModeleContratDatesReglement()->getDate()->format('Y-m-d'));
            $sheet->setCellValueByColumnAndRow($col++, $row, MoneyHelper::toString($datesReglement->getMontant()));
        }
        $col = $nbReglements * 2 + 8;

        if ($ferme->isTva()) {
            $sheet->setCellValueByColumnAndRow(
                $col++,
                $row,
                MoneyHelper::toString($this->contractCalculator->totalHT($contrat))
            );
            foreach ($this->getTVAChoices() as $choice) {
                $amount = $this->contractCalculator->totalWithTva($contrat, $choice);
                $sheet->setCellValueByColumnAndRow(
                    $col++,
                    $row,
                    '0' === $amount->getAmount() ? null : MoneyHelper::toString($amount)
                );
            }
        }

        $sheet->setCellValueByColumnAndRow(
            $col,
            $row,
            MoneyHelper::toString($this->contractCalculator->totalTTC($contrat))
        );

        ++$row;
    }
}
