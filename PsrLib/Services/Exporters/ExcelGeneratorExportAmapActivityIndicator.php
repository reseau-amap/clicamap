<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PsrLib\DTO\SearchAmapState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAnneeAdhesion;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\AdhesionAmapAmapienRepository;
use PsrLib\ORM\Repository\AmapDistributionRepository;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;

class ExcelGeneratorExportAmapActivityIndicator
{
    /**
     * @var array<int, int>
     */
    private array $activeContractsByAmap = [];

    /**
     * @var array<int, int>
     */
    private array $amapDistributionCountByAmap = [];

    /**
     * @var array<int, int>
     */
    private array $amapAdhesionCountByAmap = [];

    public function __construct(private readonly AmapRepository $amapRepo, private readonly ModeleContratRepository $modeleContratRepository, private readonly AmapDistributionRepository $amapDistributionRepo, private readonly AdhesionAmapAmapienRepository $adhesionAmapAmapienRepository)
    {
    }

    public function genererExportAmap(SearchAmapState $searchAmapState): array
    {
        /** @var Amap[] $amaps */
        $amaps = $this
            ->amapRepo
            ->search($searchAmapState)
        ;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $this->initActiveContractByAmap($amaps);
        $this->amapDistributionCountByAmap = $this
            ->amapDistributionRepo
            ->countDistributionWithAmapienByDateAmapMultiple(
                $amaps,
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            )
        ;
        $this->amapAdhesionCountByAmap = $this
            ->adhesionAmapAmapienRepository
            ->countAdhesionsByAmapMultiple(
                $amaps
            )
        ;

        $this->recordHeader($sheet, $searchAmapState, $amaps);

        $row = 4;
        foreach ($amaps as $amap) {
            $this->recordRow($sheet, $row, $amap);
            ++$row;
        }

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportAmapActivity_');
        $objWriter = new Xls($spreadsheet);
        $objWriter->save($outputFileName);

        return [count($amaps), $outputFileName];
    }

    private function recordRow(Worksheet $sheet, int $row, Amap $amap): void
    {
        $ville = $amap->getAdresseAdminVille();

        $sheet->setCellValueByColumnAndRow(1, $row, $amap->getId());
        $sheet->setCellValueByColumnAndRow(2, $row, $amap->getNom());
        $sheet->setCellValueByColumnAndRow(3, $row, null === $ville ? '' : $ville->getCpString());
        $sheet->setCellValueByColumnAndRow(4, $row, $amap->getEmail());

        $referent = $amap->getAmapienRefReseau();
        if (null !== $referent) {
            $sheet->setCellValueByColumnAndRow(
                5,
                $row,
                $referent->getUser()->getName()
            );
            $sheet->setCellValueByColumnAndRow(
                6,
                $row,
                implode(', ', $referent->getUser()->getEmails()->toArray())
            );
        }

        $sheet->setCellValueByColumnAndRow(7, $row, Amap::ETAT_FONCIONNEMENT === $amap->getEtat() ? 'oui' : 'non');
        $sheet->setCellValueByColumnAndRow(8, $row, $amap->countActiveAmapiens());
        $sheet->setCellValueByColumnAndRow(9, $row, $amap->countAmapiensRef());
        $sheet->setCellValueByColumnAndRow(10, $row, $this->activeContractsByAmap[$amap->getId()]);
        $sheet->setCellValueByColumnAndRow(11, $row, $this->amapDistributionCountByAmap[$amap->getId()] ?? 0);
        $sheet->setCellValueByColumnAndRow(12, $row, $this->amapAdhesionCountByAmap[$amap->getId()] ?? 0);
        $sheet->setCellValueByColumnAndRow(13, $row, $this->isAmapAdhesionThisYearOrPrevious($amap) ? 'oui' : 'non');
    }

    private function isAmapAdhesionThisYearOrPrevious(Amap $amap): bool
    {
        $now = new Carbon();
        $lastYear = new Carbon('-1 year');

        return !$amap
            ->getAnneeAdhesions()
            ->filter(fn (AmapAnneeAdhesion $adhesion) => $adhesion->getAnnee() === $now->year || $adhesion->getAnnee() === $lastYear->year)
            ->isEmpty()
        ;
    }

    /**
     * @param Amap[] $amaps
     */
    private function recordHeader(Worksheet $sheet, SearchAmapState $searchAmapState, $amaps): void
    {
        if (null !== $searchAmapState->getDepartement()) {
            $sheet->getCell('A1')->setValue('Département '.$searchAmapState->getDepartement()->getNom());
        } else {
            $sheet->getCell('A1')->setValue('Région '.$searchAmapState->getRegion()->getNom());
        }

        $sheet->getCell('A2')->setValue(sprintf(
            '%s résultats (Extrait le %s)',
            count($amaps),
            Carbon::now()->format('d/m/Y')
        ));
        $sheet->getStyle('A1:A2')->getFont()->setBold(true);

        $sheet->getCell('A3')->setValue('ID');
        $sheet->getCell('B3')->setValue('Nom de l\'AMAP');
        $sheet->getCell('C3')->setValue('Code postal');
        $sheet->getCell('D3')->setValue('Email public');
        $sheet->getCell('E3')->setValue('Nom & Prénom du correspondant réseau');
        $sheet->getCell('F3')->setValue('adresse mail du correspondant réseau');
        $sheet->getCell('G3')->setValue("AMAP active\u{a0}?");
        $sheet->getCell('H3')->setValue('Nombre d\'amapiens actifs');
        $sheet->getCell('I3')->setValue('Nombre de référents');
        $sheet->getCell('J3')->setValue('Nombre de contrat signés en cours');
        $sheet->getCell('K3')->setValue('Nombre de créneau distrib’ AMAP avec inscrits dans le mois');
        $sheet->getCell('L3')->setValue('Nombre de reçus émis');
        $sheet->getCell('M3')->setValue('Est adhérente cette année ou l\'année précédente ?');

        $sheet->getStyle('A3:M3')->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'alignment' => [
                'wrapText' => true,
            ],
        ]);
        $sheet->getRowDimension(3)->setRowHeight(140);

        $sheet->getColumnDimension('B')->setWidth(15);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->getColumnDimension('I')->setWidth(15);
        $sheet->getColumnDimension('J')->setWidth(15);
    }

    /**
     * @param Amap[] $amaps
     */
    private function initActiveContractByAmap($amaps): void
    {
        $contractsByAmap = $this
            ->modeleContratRepository
            ->getFromAmapMultipleByAmap($amaps)
        ;
        foreach ($amaps as $amap) {
            $amapId = $amap->getId();

            $this->activeContractsByAmap[$amapId] = count(array_filter($contractsByAmap[$amapId] ?? [], fn (ModeleContrat $modeleContrat) => !$modeleContrat->isArchived()));
        }
    }
}
