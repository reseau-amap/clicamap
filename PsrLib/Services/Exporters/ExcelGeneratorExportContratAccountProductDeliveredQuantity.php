<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Repository\ContratLivraisonCelluleRepository;
use PsrLib\ORM\Repository\ContratLivraisonRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\ContractCalculator;
use PsrLib\Services\TvaLabel;

class ExcelGeneratorExportContratAccountProductDeliveredQuantity extends ExcelGeneratorExportContratAccountProductAbstract
{
    public function __construct(ContractCalculator $contractCalculator, TvaLabel $tvaLabel, protected ModeleContratRepository $modeleContratRepository, protected ContratLivraisonRepository $contratLivraisonRepo, protected ContratLivraisonCelluleRepository $contratLivraisonCelluleRepo)
    {
        parent::__construct($contractCalculator, $tvaLabel, $modeleContratRepository);
    }

    protected function calculateTotalHT(Contrat $contrat): Money
    {
        return $this->calculateTotal($contrat, fn (ModeleContratProduit $mcp) => $mcp->getPrix()->getPrixHT());
    }

    protected function calculateTotalWithTva(Contrat $contrat, ?float $tva): Money
    {
        return $this->calculateTotal($contrat, function (ModeleContratProduit $mcp) use ($tva) {
            $prix = $mcp->getPrix();
            if ($prix->getTva() === $tva) {
                return $prix->getTvaAmount();
            }

            return Money::EUR(0);
        });
    }

    protected function calculateTotalTTC(Contrat $contrat): Money
    {
        return $this->calculateTotal($contrat, fn (ModeleContratProduit $mcp) => $mcp->getPrix()->getPrixTTC());
    }

    protected function getProductQuantity(ModeleContratProduit $mcProduct, Contrat $contrat): float
    {
        if ($mcProduct->getRegulPds()) {
            return $this->contratLivraisonCelluleRepo->countTotalDeliveredByMcpRegul(
                $contrat,
                $mcProduct
            );
        }

        return $this->contratLivraisonRepo->countTotalDeliveredByMcpNoRegul(
            $contrat,
            $mcProduct
        );
    }

    private function calculateTotal(Contrat $contrat, \Closure $callback): Money
    {
        $total = Money::EUR(0);

        /** @var ModeleContratProduit $mcp */
        foreach ($contrat->getModeleContrat()->getProduits()->toArray() as $mcp) {
            $qty = $this->getProductQuantity($mcp, $contrat);
            $total = $total->add($callback($mcp)->multiply((string) $qty));
        }

        return $total;
    }
}
