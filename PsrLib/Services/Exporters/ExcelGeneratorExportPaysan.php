<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PsrLib\DTO\SearchPaysanState;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Entity\User;

class ExcelGeneratorExportPaysan
{
    public function __construct(private readonly \PhpOffice\PhpSpreadsheet\Spreadsheet $excel)
    {
    }

    /**
     * @param Paysan[] $paysans
     */
    public function genererExportPaysans($paysans, SearchPaysanState $paysanSearchState = null): string
    {
        $time = 'Extrait le '.date('d/m/Y H:m:s');
        $nb_paysans = count($paysans);
        if (1 == $nb_paysans) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_paysans.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Paysans');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Visite de partenariat');
        $this->excel->getActiveSheet()->setCellValue('K4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('L4', 'Ferme : nom');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Ferme : siret');
        $this->excel->getActiveSheet()->setCellValue('N4', 'Ferme : code postal');
        $this->excel->getActiveSheet()->setCellValue('O4', 'Ferme : ville');
        $this->excel->getActiveSheet()->setCellValue('P4', 'Filières');

        $this->excel->getActiveSheet()->getStyle('A4:P4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:P4')->getFont()->setBold(true);

        $i = 5;
        foreach ($paysans as $paysan) {
            $ferme = $paysan->getFermes()->first();
            $user = $paysan->getUser();

            // ANNÉES D'ADHÉSION -------------------------------------------------
            $annee_adhesion = null;
            if (null !== $ferme) {
                $annee_adhesion = implode(' | ', $ferme->getAnneeAdhesions()->toArray());
            }

            // TYPES DE PRODUCTION -----------------------------------------------
            $type_production = [];
            if (null !== $ferme) {
                /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
                foreach ($ferme->getProduits() as $produit) {
                    $type_production[] = $produit->getTypeProduction();
                }
                $type_production = array_map(fn (TypeProduction $typeProduction) => $typeProduction->getNomComplet(), $type_production);
                $type_production = array_unique($type_production);
            }
            $tp = implode(' | ', $type_production);

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($user->getName()->getLastName() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($user->getName()->getFirstName() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, implode(',', $user->getEmails()->toArray()));
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i, $user->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $user->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('F'.$i, $user->getAddress()->getAdress());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $ville = $user->getVille();
            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, $ville?->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, strtoupper($ville?->getNom() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('J'.$i, $ferme?->getSpg() ?? '', \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('K'.$i, ucfirst($user->getNewsletter() ? 'oui' : 'non'));
            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('L'.$i, ucfirst($ferme->getNom() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('M'.$i, $ferme?->getSiret());
            $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $fVille = $ferme?->getVille();
            $this->excel->getActiveSheet()->setCellValue('N'.$i, $fVille?->getCpString());
            $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('O'.$i, $fVille?->getNom());
            $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('P'.$i, $tp);
            $this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;

        if (null !== $paysanSearchState && null !== $paysanSearchState->getRegion()) {
            $titre = $titre.$paysanSearchState->getRegion().' ; ';
        }

        if (null !== $paysanSearchState && null !== $paysanSearchState->getDepartement()) {
            $titre = $titre.$paysanSearchState->getDepartement().' ; ';
        }

        if (null !== $paysanSearchState && null !== $paysanSearchState->getReseau()) {
            $titre = $titre.$paysanSearchState->getReseau().' ; ';
        }

        if (null !== $paysanSearchState && null !== $paysanSearchState->getTypeProduction()) {
            $titre = $titre.'Produit proposé : '.$paysanSearchState->getTypeProduction()->getNomComplet().' ; ';
        }

        if (null !== $paysanSearchState && true === $paysanSearchState->getActive()) {
            $titre = $titre.'Actifs';
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportPaysan_');

        $nom_fichier = 'liste_des_paysans_'.date('d_m_Y').'.xls';

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save($outputFileName);

        return $outputFileName;
    }
}
