<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services\Exporters;

use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContratProduit;

class ExcelGeneratorExportContratAccountProductOrderedQuantity extends ExcelGeneratorExportContratAccountProductAbstract
{
    protected function getProductQuantity(ModeleContratProduit $mcProduct, Contrat $contrat): float
    {
        return $contrat->totalQtyByProduct($mcProduct);
    }

    protected function calculateTotalHT(Contrat $contrat): Money
    {
        return $this->contractCalculator->totalHT($contrat);
    }

    protected function calculateTotalWithTva(Contrat $contrat, ?float $tva): Money
    {
        return $this->contractCalculator->totalWithTva($contrat, $tva);
    }

    protected function calculateTotalTTC(Contrat $contrat): Money
    {
        return $this->contractCalculator->totalTTC($contrat);
    }
}
