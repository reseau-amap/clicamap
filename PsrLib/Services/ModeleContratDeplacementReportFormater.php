<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use PsrLib\ORM\Entity\ModeleContrat;

class ModeleContratDeplacementReportFormater
{
    public static function formatModeleContratDeplacementReport(ModeleContrat $mc): string
    {
        $mcInfoLivraison = $mc->getInformationLivraison();
        if (true === $mcInfoLivraison->getAmapienPermissionReportLivraison() && true === $mcInfoLivraison->getAmapienPermissionDeplacementLivraison()) {
            return 'Oui sans livraison et sur une date avec livraison';
        }
        if (true === $mcInfoLivraison->getAmapienPermissionDeplacementLivraison()) {
            return 'Oui sur une date sans livraison';
        }
        if (true === $mcInfoLivraison->getAmapienPermissionReportLivraison()) {
            return 'Oui, sur une date avec livraison';
        }

        return 'NON';
    }
}
