<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class Authentification
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly Session $session)
    {
    }

    public function getCurrentUser(): ?User
    {
        if (PHP_SAPI === 'cli') {
            return null;
        }
        $userId = $this->session->get('ID');
        if (null === $userId) {
            return null;
        }

        return $this->em->getReference(User::class, $this->session->get('ID'));
    }
}
