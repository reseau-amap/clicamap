<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use PsrLib\DTO\ContactDTO;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapienInvitation;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class Email_Sender
{
    /**
     * @var \CI_Parser
     */
    private $parser;

    /**
     * @var \CI_Loader
     */
    private $load;

    public function __construct(
        private readonly EntityManagerInterface $em,
        \CI_Parser $parser,
        \CI_Loader $load,
        private readonly Environment $twig,
        private readonly MailerInterface $sfMailer,
        private readonly LoggerInterface $logger,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
        $this->parser = $parser;
        $this->load = $load;
    }

    public function envoyerMailContact(string $nom, string $prenom, string $email, array $destination, string $cp, string $message): void
    {
        $msgTitre = 'Nouveau message du formulaire de contact Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/contact.tpl', [
            'nom' => $nom,
            'prenom' => $prenom,
            'email' => $email,
            'cp' => $cp,
            'message' => $message,
        ], true);

        $this->_doSend($destination, $msgTitre, $msgContenu);
    }

    public function envoyerEnvoiConfirmationMailContact(string $nom, string $prenom, string $email, string $cp, string $message): void
    {
        $msgTitre = 'Accusé de  formulaire de contact Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/contact_confirmation.tpl', [
            'nom' => $nom,
            'prenom' => $prenom,
            'email' => $email,
            'cp' => $cp,
            'message' => $message,
        ], true);

        $this->_doSend([$email], $msgTitre, $msgContenu);
    }

    public function envoyerMdpReinit(User $user): void
    {
        $lien = $this->urlGenerator->generate('portail_mdp_oublie_confirmation', [
            'token' => $user->getPasswordResetToken()->getToken(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $msgTitre = 'Réinitialisation de votre mot de passe Clic\'AMAP';
        $msgContenu = $this->parser->parse('email/mdp_reinit.tpl', [
            'lien' => $lien,
        ], true);

        $this->_doSend($user->getEmails()->toArray(), $msgTitre, $msgContenu);
    }

    public function envoyerModelContratValidationPaysan(ModeleContrat $mc, User $user): void
    {
        $lien = $this->urlGenerator->generate('portail_index', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $ferme = $mc->getFerme();
        $amap = $mc->getLivraisonLieu()->getAmap();
        $paysans = $ferme->getPaysans();

        $msgTitre = "Contrat {$mc->getNom()} de {$amap->getNom()}";
        $msgContenu = $this->parser->parse('email/contrat_vierge_validation_paysan.tpl', [
            'lien' => $lien,
            'amap' => $amap->getNom(),
            'contrat' => $mc->getNom(),
            'nom' => $user->getName()->getFullName(),
            'email' => implode(', ', $user->getEmails()->toArray()),
        ], true);

        foreach ($paysans as $paysan) {
            $this->_doSend($paysan->getUser()->getEmails()->toArray(), $msgTitre, $msgContenu);
        }
    }

    public function envoyerModelContratValide(ModeleContrat $contrat, User $currentUser): void
    {
        $amap = $contrat->getLivraisonLieu()->getAmap();
        $ferme = $contrat->getFerme();
        $paysans = $ferme->getPaysans();

        $refProduits = $this
            ->em
            ->getRepository(Amapien::class)
            ->getRefProduitFromAmapFerme($amap, $ferme)
        ;

        $msgTitre = "Contrat {$contrat->getNom()} validé par le paysan ".$currentUser->getName();
        $msgContenu = $this->load->view('email/contrat_vierge_valide.php', [
            'contrat' => $contrat,
            'currentUser' => $currentUser,
            'paysans' => $paysans,
        ], true);

        $amapEmail = $amap->getEmail();
        if (null !== $amapEmail) {
            $this->_doSend([$amapEmail], $msgTitre, $msgContenu);
        }
        foreach ($refProduits as $refProduit) {
            sleep(5);
            $this->_doSend($refProduit->getUser()->getEmails()->toArray(), $msgTitre, $msgContenu);
        }
    }

    public function envoyerModelContratRefus(ModeleContrat $contrat, User $currentUser, string $motif): void
    {
        $amap = $contrat->getLivraisonLieu()->getAmap();
        $ferme = $contrat->getFerme();
        $paysans = $ferme->getPaysans();

        $refProduits = $this
            ->em
            ->getRepository(Amapien::class)
            ->getRefProduitFromAmapFerme($amap, $ferme)
        ;

        $msgTitre = "Contrat {$contrat->getNom()} refusé par le paysan ".$currentUser->getName();
        $msgContenu = $this->twig->render('email/contrat_vierge_refus.html.twig', [
            'contrat' => $contrat,
            'currentUser' => $currentUser,
            'paysans' => $paysans,
            'motif' => $motif,
        ]);

        $this->_doSend([$amap->getEmail()], $msgTitre, $msgContenu);
        foreach ($refProduits as $refProduit) {
            sleep(5);
            $this->_doSend($refProduit->getUser()->getEmails()->toArray(), $msgTitre, $msgContenu);
        }
    }

    public function envoyerDistributionInscriptionAmapAmapien(AmapDistribution $amapDistribution, User $amapien): void
    {
        $msgTitre = 'Votre inscription à la Distrib\'AMAP du '.$amapDistribution->getDate()->format('d/m/Y');
        $msgContenu = $this->twig->render('email/distribution_amap_amapien_inscription.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getEmails()->toArray();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    public function envoyerDistributionDesinscriptionAmapAmapien(AmapDistribution $amapDistribution, User $amapien): void
    {
        $msgTitre = 'Votre désinscription à la Distrib\'AMAP du '.$amapDistribution->getDate()->format('d/m/Y');
        $msgContenu = $this->twig->render('email/distribution_amap_amapien_deinscription.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getEmails()->toArray();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    public function envoyerDistributionAmapienRappel(AmapDistribution $amapDistribution, Amapien $amapien): void
    {
        $msgTitre = '[Rappel] Vous êtes inscrit pour aider une distribution';
        $msgContenu = $this->twig->render('email/distribution_amapien_rappel.twig', [
            'distribution' => $amapDistribution,
        ]);
        $msgDest = $amapien->getUser()->getEmails()->toArray();

        $this->_doSend($msgDest, $msgTitre, $msgContenu);
    }

    /**
     * @param AmapDistribution[] $amapDistributions
     * @param Amapien[]          $amapiens
     */
    public function envoyerDistributionAmapiensRelance($amapDistributions, $amapiens): void
    {
        $msgTitre = 'Inscrivez-vous, reste des places pour la prochaine distrib\'';
        $msgContenu = $this->twig->render('email/distribution_amapien_relance.twig', [
            'distributions' => $amapDistributions,
        ]);

        foreach ($amapiens as $amapien) {
            $this->_doSend(
                $amapien->getUser()->getEmails()->toArray(),
                $msgTitre,
                $msgContenu
            );
        }
    }

    /**
     * @param UserEmail[] $emails
     */
    public function envoyerMcDelai0Notif(ModeleContrat $mc, array $emails): void
    {
        $msgTitre = 'Message '.getenv('SITE_NAME');
        $msgContenu = $this->twig->render('email/mc_delai_0.twig', [
            'mc' => $mc,
        ]);

        $this->_doSend(
            $emails,
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerDistributionAmapAlerteFin(AmapDistribution $amapDistribution): void
    {
        $msgTitre = 'Mettez à jour vos créneaux de distrib\'AMAP !';
        $msgContenu = $this->twig->render('email/distribution_amap_alerte_fin.twig');

        $this->_doSend(
            [$amapDistribution->getAmapLivraisonLieu()->getAmap()->getEmail()],
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerMailMdp(User $user, string $password): void
    {
        $msgTitre = 'Bienvenue sur '.getenv('SITE_NAME');
        $msgContenu = $this->twig->render('email/user_mdp.twig', [
            'user' => $user,
            'password' => $password,
            'siteName' => getenv('SITE_NAME'),
        ]);

        $this->_doSend(
            $user->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerInvitationAmap(AmapienInvitation $amapienAmapInvitation): void
    {
        $amap = $amapienAmapInvitation->getAmap();
        $msgTitre = $amap->getNom().' souhaite vous ajouter à ses amapiens sur clic\'AMAP';
        $msgContenu = $this->twig->render('email/amapien_invitation.twig', [
            'invitation' => $amapienAmapInvitation,
        ]);

        $this->_doSend(
            $amapienAmapInvitation->getUser()->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerUserDocumentNotification(User $user): void
    {
        $msgTitre = 'Un nouveau document est disponible sur votre profil clic\'AMAP';
        $msgContenu = $this->twig->render('email/user_document_notification_document.twig');

        $this->_doSend(
            $user->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerContratRappelPaiement(Contrat $contrat): void
    {
        $mc = $contrat->getModeleContrat();
        $amap = $mc->getAmap();
        $msgTitre = 'Règlement des contrats avec l\'AMAP de '.$amap->getNom();
        $msgContenu = $this->twig->render('email/contrat_relance_paiement.twig', [
            'mc' => $mc,
            'amap' => $amap,
        ]);

        $this->_doSend(
            $contrat->getAmapien()->getUser()->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerContratAmapienValidationNotification(Contrat $contrat): void
    {
        $mc = $contrat->getModeleContrat();
        $amap = $mc->getAmap();
        $msgTitre = sprintf('Votre contrat "%s" est disponible pour validation', $contrat->getModeleContrat()->getNom());
        $msgContenu = $this->twig->render('email/contrat_amapien_validation.twig', [
            'contrat' => $contrat,
        ]);

        $this->_doSend(
            $contrat->getAmapien()->getUser()->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    /**
     * @param User[] $users
     */
    public function _envoyerAdhesion($users, string $template): void
    {
        $doSend = function (User $user) use ($template): void {
            $msgTitre = 'Un nouveau document est disponible sur votre profil clic\'AMAP';
            $msgContenu = $this->twig->render($template);

            $this->_doSend(
                $user->getEmails()->toArray(),
                $msgTitre,
                $msgContenu
            );
        };

        $users = array_unique($users);
        foreach ($users as $user) {
            $doSend($user);
        }
    }

    /**
     * @param AdhesionAmap[] $adhesions
     */
    public function envoyerAdhesionAmapNotification($adhesions): void
    {
        Assertion::allIsInstanceOf($adhesions, AdhesionAmap::class);

        $users = [];
        foreach ($adhesions as $adhesion) {
            foreach ($adhesion->getAmap()->getAdmins() as $admin) {
                $users[] = $admin;
            }
        }

        $this->_envoyerAdhesion($users, 'email/user_document_notification_adhesion_amap.twig');
    }

    /**
     * @param AdhesionFerme[] $adhesions
     */
    public function envoyerAdhesionFermeNotification($adhesions): void
    {
        Assertion::allIsInstanceOf($adhesions, AdhesionFerme::class);

        $users = [];
        foreach ($adhesions as $adhesion) {
            foreach ($adhesion->getFerme()->getPaysans() as $paysan) {
                $users[] = $paysan->getUser();
            }
        }

        $this->_envoyerAdhesion($users, 'email/user_document_notification_adhesion_ferme.twig');
    }

    public function envoyerMailContactMonAmap(Amap $amap, User $src, ContactDTO $contactDTO): void
    {
        $statut = 'amapien';
        if ($src->isRefProduit()) {
            $statut = 'référent produit';
        }

        $expediteur_prenom = ucfirst(strtolower($src->getName()->getFirstName()));
        $expediteur_nom = mb_strtoupper($src->getName()->getLastName());
        $expediteur_email = implode(';', $src->getEmails()->toArray());

        $msgTitre = 'Message '.getenv('SITE_NAME');
        $msgContenu = sprintf(
            '<p>Bonjour, un %s vous a envoyé un message depuis %s :</p>
            <div style="padding:15px; margin:10px 0px; background-color:#fcf8e3;"><p>Objet : %s</p><p>%s</p><p>--<br/>%s %s<br/>%s<br/>%s</p></div>

            <p><a href="'.site_url().'">Cliquez ici pour accéder à l\'application</a></p>',
            $statut,
            getenv('SITE_NAME'),
            $contactDTO->getTitle(),
            nl2br($contactDTO->getContent()),
            $expediteur_nom,
            $expediteur_prenom,
            $expediteur_email,
            $amap
        );

        $this->_doSend(
            [$amap->getEmail()],
            $msgTitre,
            $msgContenu
        );
    }

    public function envoyerMailContactAdmin(User $target, string $statut, string $title, string $content, string $expediteur_nom, string $expediteur_email): void
    {
        $msgContenu = $this->twig->render('email/contact_admin.html.twig', [
            'statut' => $statut,
            'title' => $title,
            'content' => $content,
            'expediteur_nom' => $expediteur_nom,
            'expediteur_email' => $expediteur_email,
        ]);

        $this->_doSend(
            $target->getEmails()->toArray(),
            'Message '.getenv('SITE_NAME'),
            $msgContenu,
            null,
            $expediteur_email
        );
    }

    public function envoyerMailContactRefProduit(
        array $targets,
        string $statut,
        string $title,
        string $content,
        string $nom,
        string $prenom,
        Amap $amap,
        string $email_from,
        User $currentUser,
    ): void {
        $msgContenu = $this->twig->render('email/contact_refproduit.html.twig', [
            'statut' => $statut,
            'title' => $title,
            'content' => $content,
            'nom' => $nom,
            'prenom' => $prenom,
            'amap' => $amap,
            'email_from' => $email_from,
            'currentUser' => $currentUser,
        ]);

        $this->_doSend(
            $targets,
            'Message '.getenv('SITE_NAME'),
            $msgContenu,
            [$amap->getEmail()],
            $email_from
        );
    }

    public function envoyerMailAmapienActivation(Amapien $amapien): void
    {
        $msgTitre = 'Bienvenue sur Clic\'AMAP';
        $msgContenu = $this->twig->render('email/amapien_activation.twig', [
            'amapien' => $amapien,
        ]);

        $this->_doSend(
            $amapien->getUser()->getEmails()->toArray(),
            $msgTitre,
            $msgContenu
        );
    }

    /**
     * @param string[]|\Stringable[] $destinations
     * @param string|string[]        $copie
     */
    private function _doSend(array $destinations, string $titre, string $contenu, string|array $copie = null, string $replyTo = ''): void
    {
        $this
            ->logger
            ->info('Sending email', [
                'destination' => $destinations,
                'titre' => $titre,
                'copie' => $copie,
            ])
        ;
        $destinationsString = array_map(fn ($d) => (string) $d, $destinations);
        $destinationsStringNoNull = array_filter($destinationsString, fn (string $d) => '' !== $d);
        if (0 === count($destinationsStringNoNull)) {
            return;
        }

        $email = (new Email())
            ->from(new Address(getenv('EMAIL'), getenv('FROM')))
            ->to(...$destinationsStringNoNull)
            ->subject($titre)
            ->html($contenu)
        ;

        if (null !== $copie) {
            if (is_array($copie)) {
                $copieString = array_map(fn ($c) => (string) $c, $copie);
                $email->cc(...$copieString);
            } else {
                $email->cc((string) $copie);
            }
        }

        if ('' !== $replyTo) {
            $email->replyTo($replyTo);
        }

        $errorCount = 0;
        while (true) {
            try {
                $this->sfMailer->send($email);

                return;
            } catch (TransportExceptionInterface $e) {
                ++$errorCount;
                sleep(1);

                if ($errorCount >= 5) {
                    sentryCapture($e);

                    return;
                }
            }
        }
    }
}
