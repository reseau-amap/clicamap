<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\EvenementSuperAdmin;
use PsrLib\ORM\Entity\EvenementWithRelatedEntity;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\EvenementAbonnementRepository;
use PsrLib\ORM\Repository\EvenementSuperAdminRepository;
use PsrLib\ORM\Repository\EvenementWithRelatedEntityRepositoryInterface;
use PsrLib\Services\EntityBuilder\EvenementAbonnementDefaultFactory;

/**
 * Store all the logic to get the events for a user.
 * Take care of the user type, subscriptions, default subscriptions, etc.
 */
class EvenementRepositoryUserAbonnement
{
    public function __construct(
        private readonly EvenementAbonnementRepository $evenementAbonnementRepository,
        private readonly EvenementAbonnementDefaultFactory $abonnementDefaultFactory,
        private readonly EvenementSuperAdminRepository $evenementSuperAdminRepository,
        private readonly EntityManagerInterface $em
    ) {
    }

    /**
     * @return Evenement[]
     */
    public function getForUser(User $user)
    {
        $evts = $this->getEvenements($user);

        // Sort here as we do multiple database requests
        usort($evts, fn (Evenement $a, Evenement $b) => $b->getCreatedAt() <=> $a->getCreatedAt());

        return $evts;
    }

    /**
     * @return Evenement[]
     */
    private function getEvenements(User $user)
    {
        if ($user->isSuperAdmin()) {
            return $this
                ->em
                ->getRepository(Evenement::class)
                ->findBy([
                    'accesAbonnesDefautUniquement' => false,
                ])
            ;
        }

        $default = $this->abonnementDefaultFactory->getDefaultForUser($user);
        $userSubscribed = $this->evenementAbonnementRepository->findByUser($user);

        $evts = [];
        foreach (EvenementType::cases() as $type) {
            $evtClass = $type->getRelatedEvtClass();
            if (is_a($evtClass, EvenementWithRelatedEntity::class, true)) {
                /** @var EvenementWithRelatedEntityRepositoryInterface $evtRepo */
                $evtRepo = $this->em->getRepository($evtClass);

                $defaultRelatedEntities = $default->getRelatedEntitiesFor($type);
                $evts[] = $evtRepo->findByMultipleRelated($defaultRelatedEntities);

                $subscribedRelatedEntities = $userSubscribed->getRelatedEntitiesFor($type);
                $evts[] = $evtRepo->findByMultipleRelatedWithoutDefaultLimited($subscribedRelatedEntities);
            }

            if (EvenementSuperAdmin::class === $evtClass) {
                $evts[] = $this->evenementSuperAdminRepository->findAll();
            }
        }

        return array_unique(
            array_merge(...$evts),
            SORT_REGULAR
        );
    }
}
