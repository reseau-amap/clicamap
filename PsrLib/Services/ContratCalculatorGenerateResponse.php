<?php

declare(strict_types=1);

namespace PsrLib\Services;

use PsrLib\DTO\ContratCommandeBase;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ContratCalculatorGenerateResponse
{
    public function __construct(
        private readonly Environment $twig,
    ) {
    }

    public function renderHtmxResponse(ContratCommandeBase $contractCommandeBase): Response
    {
        return new Response($this->twig->render('contrat_signe/_contrat_commande_form_calculation.html.twig', [
            'commande' => $contractCommandeBase,
        ]));
    }
}
