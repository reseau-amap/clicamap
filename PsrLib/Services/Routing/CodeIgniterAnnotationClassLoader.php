<?php

declare(strict_types=1);

namespace PsrLib\Services\Routing;

use Symfony\Component\Routing\Loader\AnnotationClassLoader;
use Symfony\Component\Routing\Route;

class CodeIgniterAnnotationClassLoader extends AnnotationClassLoader
{
    protected function configureRoute(Route $route, \ReflectionClass $class, \ReflectionMethod $method, object $annot): void
    {
        $route->setDefault('_controller_method', $method->getName());
        $route->setDefault('_controller_method_parameters', $method->getParameters());
        $route->setDefault('_controller_class', $class->getName());
    }
}
