<?php

declare(strict_types=1);

namespace PsrLib\Services;

class TvaLabel
{
    public function getLabel(?float $TVA): string
    {
        if (null === $TVA) {
            return 'Aucune TVA';
        }

        if (0.00 !== fmod($TVA, 1)) {
            $numberString = number_format($TVA, 1, ',', ' ');
        } else {
            $numberString = number_format($TVA, 0, ',', ' ');
        }

        return $numberString.' %';
    }
}
