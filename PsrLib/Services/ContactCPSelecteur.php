<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Services;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;

class ContactCPSelecteur
{
    /**
     * @var string
     */
    private $mailDefaut;

    public function __construct(\CI_Config $config, private readonly EntityManagerInterface $em)
    {
        $this->mailDefaut = $config->item('contact_default_mail');
    }

    /**
     * Choisi le mail de contact depuis le code postal.
     *
     * @return string[]
     */
    public function choisirContactDepuisCP(string $cp): array
    {
        $userRepo = $this
            ->em
            ->getRepository(User::class)
        ;

        $depAmapiens = $userRepo
            ->getAdminDepFromCP($cp)
        ;
        if (!empty($depAmapiens)) {
            return $this->extraireEmailAmapien($depAmapiens);
        }

        $regAmapien = $userRepo
            ->getAdminRegFromCP($cp)
        ;
        if (!empty($regAmapien)) {
            return $this->extraireEmailAmapien($regAmapien);
        }

        return [$this->mailDefaut];
    }

    /**
     * @param User[] $users
     *
     * @return string[]
     */
    private function extraireEmailAmapien($users)
    {
        $res = array_map(fn (User $user) => $user->getEmails()->toArray(), $users);

        $merged = array_merge(...$res);

        return array_map(fn (UserEmail $email) => (string) $email->getEmail(), $merged);
    }
}
