<?php

declare(strict_types=1);

namespace PsrLib\Services;

use Assert\Assertion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMapping;
use PsrLib\DTO\EvenemntAbonnableDTO;
use PsrLib\DTO\SearchEvenementAbonnable;
use PsrLib\Enum\EvenementType;
use PsrLib\Enum\SearchDatatableOrder;
use PsrLib\ORM\Entity\EvenementAbonnementWithRelatedEntity;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\EvenementAbonnementRepository;
use PsrLib\Services\EntityBuilder\EvenementAbonnementDefaultFactory;

class EvenementAbonnableRepository
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EvenementAbonnementRepository $evenementAbonnementRepository,
        private readonly EvenementAbonnementDefaultFactory $abonnementDefaultFactory,
    ) {
    }

    /**
     * Search via Native SQL as we mix a different types.
     *
     * @return EvenemntAbonnableDTO[]
     */
    public function search(User $user, SearchEvenementAbonnable $search)
    {
        $cte = $this->buildCteRequest();

        $request = [$cte, 'SELECT * FROM full WHERE nom IS NOT NULL'];

        $conditions = $this->buildSearchConditions($user, $search);
        $request = array_merge($request, $conditions['conditions']);

        $request[] = $this->buildOrder($search);
        $request[] = 'LIMIT :limit OFFSET :offset';

        $query = $this->em->createNativeQuery(implode(' ', $request), $this->buildRsmSearch());
        $query->setParameters($conditions['parameters']);
        $query->setParameter('limit', $search->getMaxResults());
        $query->setParameter('offset', $search->getOffset());

        $resArray = $query->execute();

        return array_map(function ($item) {
            $type = EvenementType::from($item['type']);
            $id = $item['id'];

            return new EvenemntAbonnableDTO(
                $id,
                $item['nom'],
                $type,
                $this->em->getReference($type->getRelatedEntityClass(), $id)
            );
        }, $resArray);
    }

    public function searchCountResults(User $user, SearchEvenementAbonnable $search): int
    {
        $cte = $this->buildCteRequest();

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('count', 'count', 'integer');

        $request = [$cte, 'SELECT COUNT(*) AS count FROM full WHERE nom IS NOT NULL'];
        $conditions = $this->buildSearchConditions($user, $search);
        $request = array_merge($request, $conditions['conditions']);

        $query = $this->em->createNativeQuery(implode(' ', $request), $rsm);
        $query->setParameters($conditions['parameters']);

        return $query->execute()[0]['count'];
    }

    private function buildOrder(SearchEvenementAbonnable $search): string
    {
        // Native doctrine queries does not support ORDER BY with parameters
        // So we process it here and take care of security
        $dir = SearchDatatableOrder::ASC->valueSQL();
        $column = 'nom';

        $order = $search->getOrder()[0] ?? null;
        if (null !== $order) {
            $dir = $order->getDir()->valueSQL();
            $column = $search->getColumns()[$order->getColumn()]->getData();
        }
        Assertion::inArray($column, ['nom', 'type'], 'Invalid column');

        return "ORDER BY {$column} {$dir}";
    }

    private function buildSearchConditions(User $user, SearchEvenementAbonnable $search): array
    {
        $res = [
            'conditions' => [],
            'parameters' => [],
        ];

        if (null !== $search->getRegion()) {
            $res['conditions'][] = 'AND full.r_id = :region_id';
            $res['parameters']['region_id'] = (string) $search->getRegion()->getId();
        }
        if (null !== $search->getDepartement()) {
            $res['conditions'][] = 'AND full.d_id = :departement_id';
            $res['parameters']['departement_id'] = $search->getDepartement()->getId();
        }
        if (null !== $search->getType()) {
            $res['conditions'][] = 'AND full.type = :type';
            $res['parameters']['type'] = $search->getType()->value;
        }
        if (!empty($search->getKeyword())) {
            $res['conditions'][] = 'AND full.nom LIKE :keyword';
            $res['parameters']['keyword'] = '%'.$search->getKeyword().'%';
        }
        if ($search->isSubscriptionOnly()) {
            $userAbonnements = array_merge(
                $this->abonnementDefaultFactory->getDefaultForUser($user)->getAbonnements(),
                $this->evenementAbonnementRepository->findByUser($user)->getAbonnements()
            );
            if (0 === count($userAbonnements)) {
                $res['conditions'][] = 'AND 1 = 0'; // Hack to avoid SQL syntax error
            } else {
                $expr = new Expr();
                $conditions = array_map(function (EvenementAbonnementWithRelatedEntity $abonnement) use ($expr) {
                    $related = $abonnement->getRelated();

                    return $expr->andX(
                        $expr->eq('full.id', sprintf('\'%s\'', (string) $related->getId())),
                        $expr->eq('full.type', sprintf('\'%s\'', EvenementType::fromRelatedEntity($related)->value))
                    );
                }, $userAbonnements);
                $res['conditions'][] = sprintf('AND (%s)', $expr->orX(...$conditions));
            }
        }

        return $res;
    }

    private function buildRsmSearch(): ResultSetMapping
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('nom', 'nom');
        $rsm->addScalarResult('type', 'type');

        return $rsm;
    }

    private function buildCteRequest(): string
    {
        return sprintf("WITH full AS (SELECT r.reg_id AS id, r.reg_nom as nom, r.reg_id AS r_id, NULL AS d_id, '%s' AS type
              FROM ak_region r
              UNION
              SELECT d.dep_id AS id, d.dep_nom AS nom, d_r.reg_id AS r_id, d.dep_id AS d_id, '%s' AS type
              FROM ak_departement d
                       LEFT JOIN ak_region d_r ON d.dep_fk_reg_id = d_r.reg_id
              UNION
              SELECT f.f_id AS id, f.f_nom AS nom, f_r.reg_id AS r_id, f_d.dep_id AS d_id, '%s' AS type
              FROM ak_ferme f
                       LEFT JOIN ak_ville f_v on f.ville_id = f_v.v_id
                       LEFT JOIN ak_departement f_d ON f_v.v_fk_dep_id = f_d.dep_id
                       LEFT JOIN ak_region f_r ON f_d.dep_fk_reg_id = f_r.reg_id
              UNION
                SELECT a.amap_id AS id, a.amap_nom AS nom, a_r.reg_id AS r_id, a_d.dep_id AS d_id, '%s' AS type 
                FROM ak_amap a
                LEFT JOIN ak_amap_livraison_lieu a_ll ON a.amap_id = a_ll.amap_liv_lieu_fk_amap_id
                LEFT JOIN ak_ville a_v ON a_ll.ville_id = a_v.v_id
                LEFT JOIN ak_departement a_d ON a_v.v_fk_dep_id = a_d.dep_id
                LEFT JOIN ak_region a_r ON a_d.dep_fk_reg_id = a_r.reg_id
) ", EvenementType::REGION->value, EvenementType::DEPARTEMENT->value, EvenementType::FERME->value, EvenementType::AMAP->value);
    }
}
