<?php

declare(strict_types=1);

namespace PsrLib\Services;

use DebugBar\JavascriptRenderer;
use DH\Auditor\Auditor;
use DI\Container;
use DI\ContainerBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use PsrLib\ProjectLocation;
use PsrLib\Services\ServiceFactories\AccessDecisionManagerFactory;
use PsrLib\Services\ServiceFactories\AuditorFactory;
use PsrLib\Services\ServiceFactories\AuthorizationCheckerFactory;
use PsrLib\Services\ServiceFactories\CacheFactory;
use PsrLib\Services\ServiceFactories\DebugBarFactory;
use PsrLib\Services\ServiceFactories\DoctrineFactory;
use PsrLib\Services\ServiceFactories\DoctrineRepositoryFactory;
use PsrLib\Services\ServiceFactories\FormFactoryFactory;
use PsrLib\Services\ServiceFactories\LockFactoryFactory;
use PsrLib\Services\ServiceFactories\LoggerFactory;
use PsrLib\Services\ServiceFactories\MailerFactory;
use PsrLib\Services\ServiceFactories\PropertyAccessorFactory;
use PsrLib\Services\ServiceFactories\RequestContextFactory;
use PsrLib\Services\ServiceFactories\RequestFactory;
use PsrLib\Services\ServiceFactories\RouteCollectionFactory;
use PsrLib\Services\ServiceFactories\SerializerFactory;
use PsrLib\Services\ServiceFactories\SerializerInterfaceFactory;
use PsrLib\Services\ServiceFactories\SessionFactory;
use PsrLib\Services\ServiceFactories\TranslatorFactory;
use PsrLib\Services\ServiceFactories\TwigEnvironmentFactory;
use PsrLib\Services\ServiceFactories\UrlGeneratorInterfaceFactory;
use PsrLib\Services\ServiceFactories\UrlMatcherFactory;
use PsrLib\Services\ServiceFactories\ValidatorFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\ExpressionValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Twig\Environment;

use function DI\create;
use function DI\factory;
use function DI\get;

/**
 * Main container class. Used for instanciation of all libraries in PsrLib subfolder.
 * Use Singleton design pattern as used in multiple part of the code.
 */
class PhpDiContrainerSingleton
{
    private static ?\DI\Container $container = null;

    public static function getContainer(): Container
    {
        if (null === self::$container) {
            $builder = new ContainerBuilder();
            $builder->addDefinitions([
                FormFactoryInterface::class => factory([FormFactoryFactory::class, 'create']),
                EntityManagerInterface::class => factory([DoctrineFactory::class, 'create']),
                ValidatorInterface::class => factory([ValidatorFactory::class, 'create']),
                Serializer::class => factory([SerializerFactory::class, 'create']),
                SerializerInterface::class => factory([SerializerInterfaceFactory::class, 'create']),
                Environment::class => factory([TwigEnvironmentFactory::class, 'create']),
                Translator::class => factory([TranslatorFactory::class, 'create']),
                JavascriptRenderer::class => factory([DebugBarFactory::class, 'create']),
                MailerInterface::class => factory([MailerFactory::class, 'create']),
                Session::class => factory([SessionFactory::class, 'create']),
                CacheInterface::class => factory([CacheFactory::class, 'create']),
                AccessDecisionManagerInterface::class => factory([AccessDecisionManagerFactory::class, 'create']),
                TokenStorageInterface::class => get(\PsrLib\Services\TokenStorage::class),
                AuthorizationCheckerInterface::class => factory([AuthorizationCheckerFactory::class, 'create']),
                Auditor::class => factory([AuditorFactory::class, 'create']),
                EventDispatcherInterface::class => create(\Symfony\Component\EventDispatcher\EventDispatcher::class),
                PropertyAccessor::class => factory([PropertyAccessorFactory::class, 'create']),
                LoggerInterface::class => factory([LoggerFactory::class, 'create']),
                LockFactory::class => factory([LockFactoryFactory::class, 'create']),
                Request::class => factory([RequestFactory::class, 'create']),
                UrlGeneratorInterface::class => factory([UrlGeneratorInterfaceFactory::class, 'create']),
                RouteCollection::class => factory([RouteCollectionFactory::class, 'create']),
                RequestContext::class => factory([RequestContextFactory::class, 'create']),
                UrlMatcher::class => factory([UrlMatcherFactory::class, 'create']),
                \CI_Loader::class => function () {
                    $CI = &get_instance();

                    return $CI->load;
                },
                \CI_Config::class => function () {
                    $CI = &get_instance();

                    return $CI->config; // @phpstan-ignore-line
                },
                \CI_Input::class => function () {
                    $CI = &get_instance();

                    return $CI->input; // @phpstan-ignore-line
                },
                \CI_Parser::class => function () {
                    $CI = &get_instance();
                    $CI->load->library('parser');

                    return $CI->parser; // @phpstan-ignore-line
                },
                \Captcha::class => function () {
                    $CI = &get_instance();
                    $CI->load->library('Captcha');

                    return $CI->captcha; // @phpstan-ignore-line
                },
                \CI_Calendar::class => function () {
                    $CI = &get_instance();
                    $CI->load->library('calendar');

                    return $CI->calendar; // @phpstan-ignore-line
                },
                'validator.expression' => new ExpressionValidator(), // Fix for contraint validator factory usage
                'PsrLib\ORM\Repository\*Repository' => factory([DoctrineRepositoryFactory::class, 'create']),
            ]);
            $builder->useAttributes(true);

            if (EnvironmentCacheEnabled::isCacheEnabled()) {
                $basePath = ProjectLocation::PROJECT_ROOT.'/application/writable/cache/di/';
                $proxiesPath = $basePath.'proxies';
                $cachePath = $basePath.'cache';
                $fs = new Filesystem();
                $fs->mkdir([$proxiesPath, $cachePath]);
                $builder->writeProxiesToFile(true, $proxiesPath);
                $builder->enableCompilation($cachePath);
            }
            $container = $builder->build();

            $container->get(Auditor::class); // Force auditor init

            self::$container = $container;
        }

        return self::$container;
    }

    /**
     * Force container regeneration on next request. Use on unit testing to avoid side effects.
     */
    public static function cleanContainer(): void
    {
        // Force close Doctrine connexion
        self::getContainer()->get(EntityManagerInterface::class)->getConnection()->close();
        self::$container = null;
    }
}
