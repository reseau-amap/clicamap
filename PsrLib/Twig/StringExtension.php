<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class StringExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('ucfirst', $this->ucfirst(...)),
            new TwigFilter('ellipis', $this->ellipis(...)),
        ];
    }

    public function ucfirst(?string $input): string
    {
        if (null === $input) {
            return '';
        }

        return ucfirst($input);
    }

    public function ellipis(?string $input, ?int $length = 65, ?string $separator = '...'): string
    {
        if (null === $input) {
            return '';
        }

        if (strlen($input) > $length) {
            return substr($input, 0, $length).$separator;
        }

        return $input;
    }
}
