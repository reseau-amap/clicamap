<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\ProjectLocation;
use Twig\Extension\AbstractExtension;

class GruntCacheBurstExtension extends AbstractExtension
{
    private array $assetMap;

    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('resolve_asset', [$this, 'resolveAsset']),
        ];
    }

    public function resolveAsset(string $asset): string
    {
        $this->assetMap = json_decode(file_get_contents(ProjectLocation::PROJECT_ROOT.'/public/dist/grunt-cache-bust.json'), true);

        return $this->assetMap[$asset] ?? $asset;
    }
}
