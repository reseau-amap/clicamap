<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use DebugBar\JavascriptRenderer;
use PsrLib\ProjectLocation;
use PsrLib\Services\EnvironmentCacheEnabled;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DebugBarExtension extends AbstractExtension
{
    public function __construct(private readonly JavascriptRenderer $debugBar)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_debug_bar_header', $this->render_debug_bar_header(...), ['is_safe' => ['html']]),
            new TwigFunction('render_debug_bar', $this->render_debug_bar(...), ['is_safe' => ['html']]),
        ];
    }

    public function render_debug_bar_header(): string
    {
        if (EnvironmentCacheEnabled::isCacheEnabled()) {
            return '';
        }

        $this->debugBar->setIncludeVendors(false);

        $cssFile = ProjectLocation::PROJECT_ROOT.'public/debugbar.css';
        if (!file_exists($cssFile)) {
            $this->debugBar->dumpCssAssets($cssFile);
        }

        $jsFile = ProjectLocation::PROJECT_ROOT.'public/debugbar.js';
        if (!file_exists($jsFile)) {
            $this->debugBar->dumpJsAssets($jsFile);
        }

        $out = '<link rel="stylesheet" href="/debugbar.css" />';
        $out .= '<script type="text/javascript" src="/debugbar.js"></script>';

        return $out;
    }

    public function render_debug_bar(): string
    {
        if (EnvironmentCacheEnabled::isCacheEnabled()) {
            return '';
        }

        return $this->debugBar->render();
    }
}
