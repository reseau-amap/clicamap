<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ArrayExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('array_unique', $this->arrayUnique(...)),
            new TwigFilter('array_replace_recursive', $this->arrayReplaceRecursive(...)),
        ];
    }

    public function arrayUnique(array $array): array
    {
        return array_unique($array);
    }

    public function arrayReplaceRecursive(array $a, array $b): array
    {
        return array_replace_recursive($a, $b);
    }
}
