<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Money\Money;
use PsrLib\Services\MoneyHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class MoneyExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('money', $this->money(...)),
            new TwigFilter('money_decimal', $this->moneyDecimal(...)),
        ];
    }

    public function money(?Money $money): string
    {
        if (null === $money) {
            return '';
        }

        return MoneyHelper::toString($money);
    }

    public function moneyDecimal(?Money $money): string
    {
        if (null === $money) {
            return '';
        }

        return MoneyHelper::toStringDecimal($money);
    }
}
