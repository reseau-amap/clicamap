<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RenderPortailBlockExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('renderDocumentPortail', $this->renderDocumentPortail(...), ['is_safe' => ['html']]),
            new TwigFunction('renderSlickLogoReseaux', $this->renderSlickLogoReseaux(...), ['is_safe' => ['html']]),
        ];
    }

    public function renderDocumentPortail(): string
    {
        // Bridge to old CI code
        return render_documents_portail();
    }

    public function renderSlickLogoReseaux(): string
    {
        // Bridge to old CI code
        return render_slick_logo_reseaux();
    }
}
