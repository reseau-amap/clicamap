<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\Services\ModeleContratDeplacementReportFormater;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ModeleContratDeplacementReportFormaterExtension extends AbstractExtension
{
    public function __construct(
        private readonly ModeleContratDeplacementReportFormater $contratDeplacementReportFormater
    ) {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('modele_contrat_deplacement_report_formater', $this->format(...)),
        ];
    }

    public function format(ModeleContrat $mc): string
    {
        return $this->contratDeplacementReportFormater->formatModeleContratDeplacementReport($mc);
    }
}
