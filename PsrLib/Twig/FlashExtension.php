<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashExtension extends AbstractExtension
{
    public function __construct(private readonly Session $session)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_flash_messages', $this->getFlashMessages(...)),
        ];
    }

    public function getFlashMessages(): array
    {
        return $this->session->getFlashBag()->all();
    }
}
