<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;

class ZxcvbnExtension extends AbstractExtension
{
    public function __construct(private readonly Environment $twig)
    {
    }

    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('render_zxcvbn_progress_bar', [$this, 'zxcvbn'], ['is_safe' => ['html']]),
        ];
    }

    public function zxcvbn(string $passwordFieldName, string $url): string
    {
        return $this
            ->twig
            ->render('extensions/zxcvbn_progress_bar.html.twig', [
                'passwordFieldName' => $passwordFieldName,
                'url' => $url,
            ])
        ;
    }
}
