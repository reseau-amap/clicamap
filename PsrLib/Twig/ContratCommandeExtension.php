<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Carbon\Carbon;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Repository\ModeleContratProduitExclureRepository;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ContratCommandeExtension extends AbstractExtension
{
    private readonly Environment $twig; // @phpstan-ignore-line

    // Avoid circular dependencies errors
    public function setTwig(Environment $twig): void
    {
        $this->twig = $twig; // @phpstan-ignore-line
    }

    public function __construct(
        private readonly ModeleContratProduitExclureRepository $contratProduitExclureRepository
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('render_contrat_commande_form', $this->renderContratCommandForm(...), ['is_safe' => ['html']]),
        ];
    }

    public function renderContratCommandForm(
        ModeleContrat $mc,
        string $cancel_url,
        string $validation_url = null,
        string $validation_label = 'Valider',
        array $existingCommand = [],
        string $prevalidationMessage = null
    ): string {
        $livExclusions = $this
            ->contratProduitExclureRepository
            ->getByContrat($mc)
        ;
        $produits = $mc->getProduits()->toArray();
        $dates = $mc->getDates()->toArray();

        $dateInscriptionMin = $mc->getPremiereDateLivrableAvecDelais(Carbon::now());

        $exclusions = [];
        foreach ($produits as $produit) {
            foreach ($dates as $date) {
                $exclusions[$date->getId()][$produit->getId()] = $this->exclusionExiste($livExclusions, $date, $produit);
            }
        }

        return $this->twig->render(
            'includes/contrat_commande.html.twig',
            [
                'existingCommand' => $existingCommand,
                'contrat' => $mc,
                'produits' => $produits,
                'dates' => $dates,
                'cancel_url' => $cancel_url,
                'validation_url' => $validation_url,
                'validation_label' => $validation_label,
                'exclusions' => $exclusions,
                'dateInscriptionMin' => $dateInscriptionMin,
                'prevalidationMessage' => $prevalidationMessage,
            ]
        );
    }

    /**
     * @param \PsrLib\ORM\Entity\ModeleContratProduitExclure[] $exclusions
     */
    private function exclusionExiste($exclusions, ModeleContratDate $date, ModeleContratProduit $produit): bool
    {
        foreach ($exclusions as $exclusion) {
            if ($exclusion->getModeleContratDate()->getId() === $date->getId()
                && $exclusion->getModeleContratProduit()->getId() === $produit->getId()) {
                return true;
            }
        }

        return false;
    }
}
