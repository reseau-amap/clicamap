<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DropdownActionExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('bouton_action', $this->bouton_action(...), ['is_safe' => ['html']]),
        ];
    }

    public function bouton_action(string $name = null, array $items = [], bool $disabled = false): string
    {
        return bouton_action($name, $items, $disabled);
    }
}
