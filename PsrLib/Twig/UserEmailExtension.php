<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Assert\Assertion;
use PsrLib\ORM\Entity\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class UserEmailExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('user_email_links', $this->userEmailLink(...), ['is_safe' => ['html']]),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('users_emails_list', $this->usersEmailList(...)),
        ];
    }

    public function userEmailLink(User $user): string
    {
        $res = [];
        foreach ($user->getEmails() as $email) {
            $res[] = sprintf('<a href="mailto:%s">%s</a>', $email->getEmail(), $email->getEmail());
        }

        return implode(', ', $res);
    }

    public function usersEmailList(array $users): string
    {
        $users = array_filter($users, fn ($user) => null !== $user);
        Assertion::allIsInstanceOf($users, User::class);

        $mails = [];
        foreach ($users as $user) {
            foreach ($user->getEmails() as $email) {
                $mails[] = $email->getEmail();
            }
        }

        return implode(PHP_EOL, $mails);
    }
}
