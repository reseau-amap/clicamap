<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\Services\Purifier;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PurifierExtension extends AbstractExtension
{
    public function __construct(private readonly Purifier $purifier)
    {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('purify_modal', $this->purifyModal(...), ['is_safe' => ['html']]),
            new TwigFilter('purify_summernote_lite', $this->purifySummernoteLite(...), ['is_safe' => ['html']]),
            new TwigFilter('purify_summernote', $this->purifySummernote(...), ['is_safe' => ['html']]),
        ];
    }

    public function purifyModal(?string $input): string
    {
        return $this->purifier->purify($input ?? '', Purifier::CONFIG_MODAL);
    }

    public function purifySummernoteLite(?string $input): string
    {
        return $this->purifier->purify($input ?? '', Purifier::CONFIG_SUMMERNOTE_LITE);
    }

    public function purifySummernote(?string $input): string
    {
        return $this->purifier->purify($input ?? '', Purifier::CONFIG_SUMMERNOTE);
    }
}
