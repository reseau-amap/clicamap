<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UrlExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('base_url', $this->baseUrl(...), ['is_safe' => ['html']]),
            new TwigFunction('site_url', $this->siteUrl(...), ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string|string[] $uri URI string or an array of segments
     */
    public function baseUrl(string|array $uri = '', string $protocol = null): string
    {
        return base_url($uri, $protocol);
    }

    /**
     * @param string|string[] $uri URI string or an array of segments
     */
    public function siteUrl(string|array $uri = '', string $protocol = null): string
    {
        return site_url($uri, $protocol);
    }
}
