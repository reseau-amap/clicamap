<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class DateExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('date_format_french', $this->dateFormatFrench(...)),
            new TwigFilter('date_format_french_hour', $this->dateFormatFrenchHour(...)),
        ];
    }

    public function dateFormatFrench(\DateTimeInterface $input = null): string
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y');
    }

    public function dateFormatFrenchHour(\DateTimeInterface $input = null): string
    {
        if (null === $input) {
            return '';
        }

        return $input->format('d/m/Y H:i:s');
    }
}
