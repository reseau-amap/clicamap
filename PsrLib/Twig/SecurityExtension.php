<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SecurityExtension extends AbstractExtension
{
    public function __construct(private readonly AuthorizationCheckerInterface $securityChecker)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('is_granted', $this->isGranted(...)),
        ];
    }

    /**
     * @param mixed|null $subject
     */
    public function isGranted(string $action, $subject = null): bool
    {
        return $this->securityChecker->isGranted($action, $subject);
    }
}
