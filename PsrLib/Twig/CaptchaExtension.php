<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CaptchaExtension extends AbstractExtension
{
    public function __construct(private readonly \Captcha $captcha)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_nouveau_captcha_img', $this->renderNouveauCaptchaImg(...), ['is_safe' => ['html']]),
        ];
    }

    public function renderNouveauCaptchaImg(): string
    {
        $cap = $this->captcha->nouveauCaptcha();

        return $cap['image'];
    }
}
