<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\ORM\Repository\ActualiteRepository;
use PsrLib\Services\Authentification;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ActualiteExtension extends AbstractExtension
{
    private readonly Environment $twig; // @phpstan-ignore-line

    // Avoid circular dependencies errors
    public function setTwig(Environment $twig): void
    {
        $this->twig = $twig; // @phpstan-ignore-line
    }

    public function __construct(
        private readonly ActualiteRepository $actualiteRepository,
        private readonly Authentification $authentification,
    ) {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_user_actu_modal', $this->renderUserActuModal(...), ['is_safe' => ['html']]),
        ];
    }

    public function renderUserActuModal(): string
    {
        $currentUser = $this->authentification->getCurrentUser();
        if (null === $currentUser) {
            return '';
        }

        $notReadCount = $this->actualiteRepository->countNotReadByUser($currentUser);
        if (0 === $notReadCount) {
            return '';
        }

        $lastNotRead = $this->actualiteRepository->getLastNotReadByUser($currentUser);

        return $this->twig->render('extensions/modal_actualite.html.twig', [
            'actualite' => $lastNotRead,
            'notReadCount' => $notReadCount,
        ]);
    }
}
