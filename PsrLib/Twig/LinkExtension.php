<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class LinkExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('link_or_text', $this->linkOrText(...), ['is_safe' => ['html']]),
        ];
    }

    public function linkOrText(?string $input): string
    {
        if (null === $input) {
            return '';
        }

        if ($this->isLinkValid($input)) {
            return '<a href="'.$input.'" target="_blank">'.$input.'</a>';
        }

        return $input;
    }

    private function isLinkValid(string $link): bool
    {
        return false !== filter_var($link, FILTER_VALIDATE_URL)
            && (
                'http' === parse_url($link, PHP_URL_SCHEME)
                || 'https' === parse_url($link, PHP_URL_SCHEME)
            )
        ;
    }
}
