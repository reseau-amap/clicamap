<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\Services\TvaLabel;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PriceExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('vatLabel', $this->vatLabel(...)),
        ];
    }

    public function vatLabel(?float $vatValue): string
    {
        $tvaLabel = new TvaLabel();

        return $tvaLabel->getLabel($vatValue);
    }
}
