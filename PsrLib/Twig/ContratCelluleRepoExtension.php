<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Repository\ContratCelluleRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ContratCelluleRepoExtension extends AbstractExtension
{
    public function __construct(private readonly ContratCelluleRepository $contratCelluleRepository)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('contrat_cellule_count', $this->findCelluleCount(...)),
            new TwigFunction('contrat_cellule_count_by_date', $this->countNbLivraisonsByDate(...)),
            new TwigFunction('contrat_cellule_count_by_product', $this->countNbLivraisonsByProduct(...)),
        ];
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public function findCelluleCount(array $commandes, ModeleContratDate $date, ModeleContratProduit $produit): float
    {
        return $this
            ->contratCelluleRepository
            ->findCelluleCount($commandes, $date, $produit)
        ;
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public function countNbLivraisonsByDate(array $commandes, ModeleContratDate $date): float
    {
        return $this
            ->contratCelluleRepository
            ->countNbLivraisonsByDate($commandes, $date)
        ;
    }

    /**
     * @param ContratCellule[] $commandes
     */
    public function countNbLivraisonsByProduct(array $commandes, ModeleContratProduit $produit): float
    {
        return $this
            ->contratCelluleRepository
            ->countNbLivraisonsByProduct($commandes, $produit)
        ;
    }
}
