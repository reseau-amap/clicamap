<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Assert\Assertion;
use Knp\Menu\Renderer\RendererInterface;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\KnpMenuFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class KnpMenuExtension extends AbstractExtension
{
    public function __construct(
        private readonly KnpMenuFactory $knpMenuFactory,
        private readonly RendererInterface $renderer,
        private readonly TokenStorageInterface $tokenStorage
    ) {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_menu', $this->renderMenu(...), ['is_safe' => ['html']]),
        ];
    }

    public function renderMenu(): string
    {
        $currentUser = $this->tokenStorage->getToken()?->getUser();
        Assertion::isInstanceOf($currentUser, User::class);

        $menu = $this->knpMenuFactory->createMenuForUser($currentUser);

        return $this->renderer->render($menu);
    }
}
