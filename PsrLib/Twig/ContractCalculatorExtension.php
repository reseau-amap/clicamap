<?php

declare(strict_types=1);

namespace PsrLib\Twig;

use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\Services\ContractCalculator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ContractCalculatorExtension extends AbstractExtension
{
    public function __construct(private readonly ContractCalculator $contractCalculator)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('contrat_total_TTC', $this->contratTotalTTC(...), ['is_safe' => ['html']]),
        ];
    }

    public function contratTotalTTC(Contrat $contrat): Money
    {
        return $this->contractCalculator->totalTTC($contrat);
    }
}
