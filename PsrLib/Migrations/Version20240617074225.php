<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240617074225 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $userIds = $this
            ->connection
            ->executeQuery('SELECT id FROM ak_user')
            ->fetchFirstColumn()
        ;

        $actuIds = $this
            ->connection
            ->executeQuery('SELECT id FROM actualite')
            ->fetchFirstColumn()
        ;

        foreach ($actuIds as $actuId) {
            foreach ($userIds as $userId) {
                $this->addSql('INSERT INTO actualite_user (actualite_id, user_id) VALUES (:actualite_id, :user_id)', [
                    'actualite_id' => $actuId,
                    'user_id' => $userId,
                ]);
            }
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
