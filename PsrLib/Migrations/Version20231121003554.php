<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231121003554 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM ak_campagne_montant');
        $this->addSql('DELETE FROM ak_campagne');
        $this->addSql('CREATE TABLE ak_campagne_bulletin (id INT AUTO_INCREMENT NOT NULL, campagne_id INT DEFAULT NULL, amapien_id INT DEFAULT NULL, pdf_id INT DEFAULT NULL, payeur VARCHAR(255) NOT NULL, paiement_date DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', permission_image TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5CE2613A16227374 (campagne_id), INDEX IDX_5CE2613ABC7A2B78 (amapien_id), UNIQUE INDEX UNIQ_5CE2613A511FC912 (pdf_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_campagne_bulletin (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_95494cf19cee77fc38227e259917a042_idx (type), INDEX object_id_95494cf19cee77fc38227e259917a042_idx (object_id), INDEX discriminator_95494cf19cee77fc38227e259917a042_idx (discriminator), INDEX transaction_hash_95494cf19cee77fc38227e259917a042_idx (transaction_hash), INDEX blame_id_95494cf19cee77fc38227e259917a042_idx (blame_id), INDEX created_at_95494cf19cee77fc38227e259917a042_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_campagne_bulletin_montant_libre (id INT AUTO_INCREMENT NOT NULL, bulletin_id INT DEFAULT NULL, campagne_montant_id INT DEFAULT NULL, montant BIGINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_285B4215D1AAB236 (bulletin_id), INDEX IDX_285B4215FB730A86 (campagne_montant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_campagne_bulletin_montant_libre (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_5d3e2d7ad9873225e700c70654a84323_idx (type), INDEX object_id_5d3e2d7ad9873225e700c70654a84323_idx (object_id), INDEX discriminator_5d3e2d7ad9873225e700c70654a84323_idx (discriminator), INDEX transaction_hash_5d3e2d7ad9873225e700c70654a84323_idx (transaction_hash), INDEX blame_id_5d3e2d7ad9873225e700c70654a84323_idx (blame_id), INDEX created_at_5d3e2d7ad9873225e700c70654a84323_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_campagne_bulletin ADD CONSTRAINT FK_5CE2613A16227374 FOREIGN KEY (campagne_id) REFERENCES ak_campagne (id)');
        $this->addSql('ALTER TABLE ak_campagne_bulletin ADD CONSTRAINT FK_5CE2613ABC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_campagne_bulletin ADD CONSTRAINT FK_5CE2613A511FC912 FOREIGN KEY (pdf_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_campagne_bulletin_montant_libre ADD CONSTRAINT FK_285B4215D1AAB236 FOREIGN KEY (bulletin_id) REFERENCES ak_campagne_bulletin (id)');
        $this->addSql('ALTER TABLE ak_campagne_bulletin_montant_libre ADD CONSTRAINT FK_285B4215FB730A86 FOREIGN KEY (campagne_montant_id) REFERENCES ak_campagne_montant (id)');
        $this->addSql('ALTER TABLE ak_campagne ADD autheur_logo_id INT DEFAULT NULL, ADD auteur_label VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_campagne ADD CONSTRAINT FK_48880B4B119AF98B FOREIGN KEY (autheur_logo_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_48880B4B119AF98B ON ak_campagne (autheur_logo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_campagne_bulletin_montant_libre DROP FOREIGN KEY FK_285B4215D1AAB236');
        $this->addSql('DROP TABLE ak_campagne_bulletin');
        $this->addSql('DROP TABLE _audit_ak_campagne_bulletin');
        $this->addSql('DROP TABLE ak_campagne_bulletin_montant_libre');
        $this->addSql('DROP TABLE _audit_ak_campagne_bulletin_montant_libre');
        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE ak_campagne DROP FOREIGN KEY FK_48880B4B119AF98B');
        $this->addSql('DROP INDEX IDX_48880B4B119AF98B ON ak_campagne');
        $this->addSql('ALTER TABLE ak_campagne DROP autheur_logo_id, DROP auteur_label');
        $this->addSql('ALTER TABLE ak_campagne_montant CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat CHANGE mc_etat mc_etat VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_cp v_cp INT UNSIGNED NOT NULL');
    }
}
