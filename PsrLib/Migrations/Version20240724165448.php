<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\EvenementAmap;
use PsrLib\ORM\Entity\EvenementSuperAdmin;
use PsrLib\ORM\Entity\Files\EvenementImg;
use PsrLib\ORM\Entity\Files\EvenementPj;
use PsrLib\ORM\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240724165448 extends AbstractMigration
{
    private array $evts = [];

    private EntityManagerInterface $em;

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->evts = $this
            ->connection
            ->fetchAll('SELECT * FROM ak_evenement ORDER BY ev_id ASC')
        ;

        $this->addSql('DROP TABLE ak_evenement_info_amap');
        $this->addSql('DROP TABLE ak_evenement_info_departement');
        $this->addSql('DROP TABLE ak_evenement_info_ferme');
        $this->addSql('DROP TABLE ak_evenement_info_reseau');
        $this->addSql('DROP TABLE ak_evenement_info_type_prod');
        $this->addSql('DELETE FROM ak_evenement');

        $this->addSql('CREATE TABLE ak_evenement_abonnement (ev_id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, dtype VARCHAR(255) NOT NULL, INDEX IDX_14BB7E37A76ED395 (user_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_abonnement (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (type), INDEX object_id_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (object_id), INDEX discriminator_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (discriminator), INDEX transaction_hash_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (transaction_hash), INDEX blame_id_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (blame_id), INDEX created_at_fe311aeebf0b62ae01e2d3cf1be0acd8_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_abonnement_amap (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_EC5339554162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_abonnement_amap (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (type), INDEX object_id_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (object_id), INDEX discriminator_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (discriminator), INDEX transaction_hash_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (transaction_hash), INDEX blame_id_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (blame_id), INDEX created_at_4e223e7fdb0b51002bf5d4bfbcd0e261_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_abonnement_departement (ev_id INT NOT NULL, related_id VARCHAR(3) NOT NULL, INDEX IDX_D4142C504162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_abonnement_departement (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_27615914c49e5c880cd52c3bb825dada_idx (type), INDEX object_id_27615914c49e5c880cd52c3bb825dada_idx (object_id), INDEX discriminator_27615914c49e5c880cd52c3bb825dada_idx (discriminator), INDEX transaction_hash_27615914c49e5c880cd52c3bb825dada_idx (transaction_hash), INDEX blame_id_27615914c49e5c880cd52c3bb825dada_idx (blame_id), INDEX created_at_27615914c49e5c880cd52c3bb825dada_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_abonnement_ferme (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_62AF09D24162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_abonnement_ferme (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_994a964b1ac266608dd32dc1da254a54_idx (type), INDEX object_id_994a964b1ac266608dd32dc1da254a54_idx (object_id), INDEX discriminator_994a964b1ac266608dd32dc1da254a54_idx (discriminator), INDEX transaction_hash_994a964b1ac266608dd32dc1da254a54_idx (transaction_hash), INDEX blame_id_994a964b1ac266608dd32dc1da254a54_idx (blame_id), INDEX created_at_994a964b1ac266608dd32dc1da254a54_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_abonnement_region (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_12D118554162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_abonnement_region (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_73141aac8fa1174446c2b7d28738f01e_idx (type), INDEX object_id_73141aac8fa1174446c2b7d28738f01e_idx (object_id), INDEX discriminator_73141aac8fa1174446c2b7d28738f01e_idx (discriminator), INDEX transaction_hash_73141aac8fa1174446c2b7d28738f01e_idx (transaction_hash), INDEX blame_id_73141aac8fa1174446c2b7d28738f01e_idx (blame_id), INDEX created_at_73141aac8fa1174446c2b7d28738f01e_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_amap (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_38FDCCFC4162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_amap (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (type), INDEX object_id_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (object_id), INDEX discriminator_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (discriminator), INDEX transaction_hash_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (transaction_hash), INDEX blame_id_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (blame_id), INDEX created_at_bd7e0d0a4d3fea10390e6e8ecc86558d_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_departement (ev_id INT NOT NULL, related_id VARCHAR(3) NOT NULL, INDEX IDX_25593D7E4162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8  ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_departement (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_83f9901dff7a76cdd6ac3df320ae090a_idx (type), INDEX object_id_83f9901dff7a76cdd6ac3df320ae090a_idx (object_id), INDEX discriminator_83f9901dff7a76cdd6ac3df320ae090a_idx (discriminator), INDEX transaction_hash_83f9901dff7a76cdd6ac3df320ae090a_idx (transaction_hash), INDEX blame_id_83f9901dff7a76cdd6ac3df320ae090a_idx (blame_id), INDEX created_at_83f9901dff7a76cdd6ac3df320ae090a_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_ferme (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_CD71BC6B4162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_ferme (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_4de73c2889015a31353b5a9d9f0c9e97_idx (type), INDEX object_id_4de73c2889015a31353b5a9d9f0c9e97_idx (object_id), INDEX discriminator_4de73c2889015a31353b5a9d9f0c9e97_idx (discriminator), INDEX transaction_hash_4de73c2889015a31353b5a9d9f0c9e97_idx (transaction_hash), INDEX blame_id_4de73c2889015a31353b5a9d9f0c9e97_idx (blame_id), INDEX created_at_4de73c2889015a31353b5a9d9f0c9e97_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_evenement_region (ev_id INT NOT NULL, related_id INT NOT NULL, INDEX IDX_A0C3CDC84162C001 (related_id), PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_region (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_c5ea133ee74894f30b6d939de72d0b64_idx (type), INDEX object_id_c5ea133ee74894f30b6d939de72d0b64_idx (object_id), INDEX discriminator_c5ea133ee74894f30b6d939de72d0b64_idx (discriminator), INDEX transaction_hash_c5ea133ee74894f30b6d939de72d0b64_idx (transaction_hash), INDEX blame_id_c5ea133ee74894f30b6d939de72d0b64_idx (blame_id), INDEX created_at_c5ea133ee74894f30b6d939de72d0b64_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_evenement_abonnement ADD CONSTRAINT FK_14BB7E37A76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id)');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_amap ADD CONSTRAINT FK_EC5339554162C001 FOREIGN KEY (related_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_amap ADD CONSTRAINT FK_EC53395540A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement_abonnement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_departement ADD CONSTRAINT FK_D4142C504162C001 FOREIGN KEY (related_id) REFERENCES ak_departement (dep_id)');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_departement ADD CONSTRAINT FK_D4142C5040A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement_abonnement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_ferme ADD CONSTRAINT FK_62AF09D24162C001 FOREIGN KEY (related_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_ferme ADD CONSTRAINT FK_62AF09D240A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement_abonnement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_region ADD CONSTRAINT FK_12D118554162C001 FOREIGN KEY (related_id) REFERENCES ak_region (reg_id)');
        $this->addSql('ALTER TABLE ak_evenement_abonnement_region ADD CONSTRAINT FK_12D1185540A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement_abonnement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_amap ADD CONSTRAINT FK_38FDCCFC4162C001 FOREIGN KEY (related_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_evenement_amap ADD CONSTRAINT FK_38FDCCFC40A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_departement ADD CONSTRAINT FK_25593D7E4162C001 FOREIGN KEY (related_id) REFERENCES ak_departement (dep_id)');
        $this->addSql('ALTER TABLE ak_evenement_departement ADD CONSTRAINT FK_25593D7E40A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_ferme ADD CONSTRAINT FK_CD71BC6B4162C001 FOREIGN KEY (related_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_evenement_ferme ADD CONSTRAINT FK_CD71BC6B40A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement_region ADD CONSTRAINT FK_A0C3CDC84162C001 FOREIGN KEY (related_id) REFERENCES ak_region (reg_id)');
        $this->addSql('ALTER TABLE ak_evenement_region ADD CONSTRAINT FK_A0C3CDC840A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement (ev_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE756017D96118B');
        $this->addSql('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE75601B3099DAC');
        $this->addSql('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE75601F675F31B');
        $this->addSql('DROP INDEX IDX_1EE75601F675F31B ON ak_evenement');
        $this->addSql('DROP INDEX IDX_1EE75601B3099DAC ON ak_evenement');
        $this->addSql('DROP INDEX IDX_1EE756017D96118B ON ak_evenement');
        $this->addSql('ALTER TABLE ak_evenement ADD auteur_id INT NOT NULL, ADD titre VARCHAR(150) NOT NULL, ADD description LONGTEXT NOT NULL, ADD description_courte LONGTEXT NOT NULL, ADD date_deb DATE DEFAULT NULL, ADD date_fin DATE DEFAULT NULL, ADD acces_abonnes_defaut_uniquement TINYINT(1) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD dtype VARCHAR(255) NOT NULL, DROP ev_info_amap_ferme, DROP ev_fk_amap_id, DROP author_id, DROP ev_date_deb, DROP ev_date_fin, DROP ev_hor_deb, DROP ev_hor_fin, DROP ev_txt, DROP ev_url, DROP ev_info_dep, DROP ev_info_reseaux, DROP ev_info_amap, DROP ev_info_fermes, DROP ev_info_tp, DROP ev_info_amap_amapiens, DROP ev_info_createur_reseau, DROP ev_createur, CHANGE ev_nom lieu VARCHAR(150) DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE7560160BB6FE6 FOREIGN KEY (auteur_id) REFERENCES ak_user (id)');
        $this->addSql('CREATE INDEX IDX_1EE7560160BB6FE6 ON ak_evenement (auteur_id)');
        $this->addSql('CREATE TABLE ak_evenement_super_admin (ev_id INT NOT NULL, PRIMARY KEY(ev_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_evenement_super_admin (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_c5a64dcb8e82ad9c73ac6661c8078672_idx (type), INDEX object_id_c5a64dcb8e82ad9c73ac6661c8078672_idx (object_id), INDEX discriminator_c5a64dcb8e82ad9c73ac6661c8078672_idx (discriminator), INDEX transaction_hash_c5a64dcb8e82ad9c73ac6661c8078672_idx (transaction_hash), INDEX blame_id_c5a64dcb8e82ad9c73ac6661c8078672_idx (blame_id), INDEX created_at_c5a64dcb8e82ad9c73ac6661c8078672_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_evenement_super_admin ADD CONSTRAINT FK_58A3419E40A4EC42 FOREIGN KEY (ev_id) REFERENCES ak_evenement (ev_id) ON DELETE CASCADE');
    }

    public function postUp(Schema $schema): void
    {
        $this->em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);

        foreach ($this->evts as $oldEvt) {
            if ('amap' === $oldEvt['ev_createur']) {
                $newEvt = $this->createEvtAmap($oldEvt);
            } elseif ('sup_adm' === $oldEvt['ev_createur']) {
                $newEvt = $this->createEvtSuperAdmin();
            } else {
                throw new \Exception('Unknown creator');
            }

            $newEvt->setTitre($oldEvt['ev_nom']);
            $description = $this->cleanDescription($oldEvt['ev_txt']);
            $newEvt->setDescription($description);
            $newEvt->setDescriptionCourte($description);
            if (null !== $oldEvt['ev_date_deb']) {
                $newEvt->setDateDeb(new \DateTime($oldEvt['ev_date_deb']));
            }
            if (null !== $oldEvt['ev_date_fin']) {
                $newEvt->setDateFin(new \DateTime($oldEvt['ev_date_fin']));
            }
            $newEvt->setLieu($oldEvt['ev_url']);
            if (null !== $oldEvt['pj_id']) {
                $newEvt->setPj($this->em->getReference(EvenementPj::class, $oldEvt['pj_id']));
            }
            if (null !== $oldEvt['img_id']) {
                $newEvt->setImg($this->em->getReference(EvenementImg::class, $oldEvt['img_id']));
            }
            $this->em->persist($newEvt);
        }

        $this->em->flush();
    }

    private function cleanDescription(string $in): string
    {
        $out = str_replace('<h3>', '<p>', $in);
        $out = str_replace('</h3>', '</p>', $out);
        $out = str_replace('<ul>', '', $out);
        $out = str_replace('</ul>', '', $out);
        $out = str_replace('<li>', '<p>-', $out);
        $out = str_replace('</li>', '</p>', $out);

        return (string) str_replace('<p>-<p>', '<p>-', $out);
    }

    private function createEvtAmap(array $oldEvt): EvenementAmap
    {
        $amap = $this->em->getRepository(Amap::class)->findOneBy(['id' => $oldEvt['ev_fk_amap_id']]);
        $evt = new EvenementAmap($amap->getAdmins()->first());
        $evt->setRelated($amap);
        $evt->setAccesAbonnesDefautUniquement(true);

        return $evt;
    }

    private function createEvtSuperAdmin(): EvenementSuperAdmin
    {
        $superAdmins = $this->em->getRepository(User::class)->findBy(['superAdmin' => true], ['id' => 'ASC']);

        return new EvenementSuperAdmin($superAdmins[0]);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
