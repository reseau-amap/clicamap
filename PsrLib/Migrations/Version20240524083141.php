<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Assert\Assertion;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\ModeleContrat;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240524083141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        $mcSansFerme = $em->getRepository(ModeleContrat::class)->findBy(['ferme' => null]);
        foreach ($mcSansFerme as $mc) {
            Assertion::same(1, $mc->getVersion());
            foreach ($mc->getContrats() as $contrat) {
                $em->remove($contrat);
            }
            $em->remove($mc);
        }
        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
