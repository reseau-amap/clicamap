<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212072651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adhesion_amap DROP FOREIGN KEY FK_3623071452AA66E8');
        $this->addSql('ALTER TABLE adhesion_amap CHANGE amap_id amap_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_3623071452AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE adhesion_amapien DROP FOREIGN KEY FK_FCCE162FBC7A2B78');
        $this->addSql('ALTER TABLE adhesion_amapien CHANGE amapien_id amapien_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162FBC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE adhesion_ferme DROP FOREIGN KEY FK_63AE08EA18981132');
        $this->addSql('ALTER TABLE adhesion_ferme CHANGE ferme_id ferme_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EA18981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_cp v_cp INT(5) UNSIGNED ZEROFILL NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adhesion_amap DROP FOREIGN KEY FK_3623071452AA66E8');
        $this->addSql('ALTER TABLE adhesion_amap CHANGE amap_id amap_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_3623071452AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE adhesion_amapien DROP FOREIGN KEY FK_FCCE162FBC7A2B78');
        $this->addSql('ALTER TABLE adhesion_amapien CHANGE amapien_id amapien_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162FBC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE adhesion_ferme DROP FOREIGN KEY FK_63AE08EA18981132');
        $this->addSql('ALTER TABLE adhesion_ferme CHANGE ferme_id ferme_id INT NOT NULL');
        $this->addSql('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EA18981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_cp v_cp INT UNSIGNED NOT NULL');
    }
}
