<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\ModeleContrat;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230407141228 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        $mcNoFerme = $em
            ->getRepository(ModeleContrat::class)
            ->findBy([
                'ferme' => null,
                'version' => 2,
            ])
        ;
        foreach ($mcNoFerme as $mc) {
            $em->remove($mc);
            foreach ($mc->getContrats() as $c) {
                $em->remove($c);
            }
        }

        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
