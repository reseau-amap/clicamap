<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230630164017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_amapien CHANGE deletedat deleted_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE ak_amapien CHANGE deleted_at deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_cp v_cp INT UNSIGNED NOT NULL');
    }
}
