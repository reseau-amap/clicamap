<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231201154201 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_document_utilisateur (id INT UNSIGNED AUTO_INCREMENT NOT NULL, ficher_id INT NOT NULL, annee INT NOT NULL, nom VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_4F66FE1B51C90057 (ficher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_document_utilisateur (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_b638bca1f7cc122988c19b109b6e06ae_idx (type), INDEX object_id_b638bca1f7cc122988c19b109b6e06ae_idx (object_id), INDEX discriminator_b638bca1f7cc122988c19b109b6e06ae_idx (discriminator), INDEX transaction_hash_b638bca1f7cc122988c19b109b6e06ae_idx (transaction_hash), INDEX blame_id_b638bca1f7cc122988c19b109b6e06ae_idx (blame_id), INDEX created_at_b638bca1f7cc122988c19b109b6e06ae_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_document_utilisateur_amap (id INT UNSIGNED NOT NULL, amap_id INT NOT NULL, INDEX IDX_D9256F9752AA66E8 (amap_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_document_utilisateur_amap (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_6753c926357301981dc8b8fc26f613e0_idx (type), INDEX object_id_6753c926357301981dc8b8fc26f613e0_idx (object_id), INDEX discriminator_6753c926357301981dc8b8fc26f613e0_idx (discriminator), INDEX transaction_hash_6753c926357301981dc8b8fc26f613e0_idx (transaction_hash), INDEX blame_id_6753c926357301981dc8b8fc26f613e0_idx (blame_id), INDEX created_at_6753c926357301981dc8b8fc26f613e0_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_document_utilisateur_ferme (id INT UNSIGNED NOT NULL, ferme_id INT NOT NULL, INDEX IDX_17F0DC1818981132 (ferme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_document_utilisateur_ferme (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_d35c97d9e7e22030abeb06c6cca19423_idx (type), INDEX object_id_d35c97d9e7e22030abeb06c6cca19423_idx (object_id), INDEX discriminator_d35c97d9e7e22030abeb06c6cca19423_idx (discriminator), INDEX transaction_hash_d35c97d9e7e22030abeb06c6cca19423_idx (transaction_hash), INDEX blame_id_d35c97d9e7e22030abeb06c6cca19423_idx (blame_id), INDEX created_at_d35c97d9e7e22030abeb06c6cca19423_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_document_utilisateur ADD CONSTRAINT FK_4F66FE1B51C90057 FOREIGN KEY (ficher_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_document_utilisateur_amap ADD CONSTRAINT FK_D9256F9752AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_document_utilisateur_amap ADD CONSTRAINT FK_D9256F97BF396750 FOREIGN KEY (id) REFERENCES ak_document_utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ak_document_utilisateur_ferme ADD CONSTRAINT FK_17F0DC1818981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_document_utilisateur_ferme ADD CONSTRAINT FK_17F0DC18BF396750 FOREIGN KEY (id) REFERENCES ak_document_utilisateur (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
