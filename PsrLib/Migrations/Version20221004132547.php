<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221004132547 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_ferme_produit ADD prix_tva DOUBLE PRECISION DEFAULT NULL, ADD prix_prix_ttc BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD prix_tva DOUBLE PRECISION DEFAULT NULL, ADD prix_prix_ttc BIGINT NOT NULL');

        $this->addSql('UPDATE ak_ferme_produit SET prix_prix_ttc = cast(f_pro_prix AS DECIMAL(65,2)) * 100 WHERE f_pro_prix != \'\' AND f_pro_prix IS NOT null');
        $this->addSql('UPDATE ak_modele_contrat_produit SET prix_prix_ttc = cast(mc_pro_prix AS DECIMAL(65,2)) * 100 WHERE mc_pro_prix != \'\' AND mc_pro_prix IS NOT null');

        $this->addSql('ALTER TABLE ak_ferme_produit DROP f_pro_prix');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit DROP mc_pro_prix');

        $this->addSql('UPDATE ak_ferme_produit LEFT JOIN ak_ferme af on ak_ferme_produit.f_pro_fk_ferme_id = af.f_id  SET prix_tva=5.5 WHERE af.f_tva=1;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
