<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250109104331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_modele_contrat ADD information_livraison_mc_amapien_gestion_dpcmt TINYINT(1) DEFAULT NULL');
        $this->addSql('UPDATE ak_modele_contrat SET information_livraison_mc_amapien_gestion_dpcmt = 0 WHERE information_livraison_mc_amapien_permission_dpcmt_livraison = 0 OR information_livraison_mc_amapien_permission_report_livraison=0');
        $this->addSql('UPDATE ak_modele_contrat SET information_livraison_mc_amapien_gestion_dpcmt = 1 WHERE information_livraison_mc_amapien_permission_dpcmt_livraison = 1 OR information_livraison_mc_amapien_permission_report_livraison=1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
