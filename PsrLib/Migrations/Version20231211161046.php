<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231211161046 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_contrat ADD uuid VARCHAR(255) NOT NULL, ADD cdp_paid TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_ferme_regroupement ADD cdp TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat ADD uuid VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_paysan ADD pay_uuid VARCHAR(255) DEFAULT NULL');

        $c_ids = $this->connection->executeQuery('SELECT c_id FROM ak_contrat')->fetchAll();
        $mc_ids = $this->connection->executeQuery('SELECT mc_id FROM ak_modele_contrat')->fetchAll();

        foreach (array_column($c_ids, 'c_id') as $c_id) {
            $this->addSql('UPDATE ak_contrat SET uuid=:uuid WHERE c_id=:c_id', [
                'c_id' => $c_id,
                'uuid' => Uuid::uuid4(),
            ]);
        }

        foreach (array_column($mc_ids, 'mc_id') as $mc_id) {
            $this->addSql('UPDATE ak_modele_contrat SET uuid=:uuid WHERE mc_id=:mc_id', [
                'mc_id' => $mc_id,
                'uuid' => Uuid::uuid4(),
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
