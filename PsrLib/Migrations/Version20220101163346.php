<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220101163346 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE ak_amap SET amap_fk_contact_referent_reseau_id=NULL WHERE amap_fk_contact_referent_reseau_id NOT IN (SELECT a_id FROM ak_amapien);');
        $this->addSql('UPDATE ak_amap SET amap_fk_contact_referent_reseau_secondaire_id=NULL WHERE amap_fk_contact_referent_reseau_secondaire_id NOT IN (SELECT a_id FROM ak_amapien);');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63A4B3F446 FOREIGN KEY (amap_fk_contact_referent_reseau_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC633BC43DD2 FOREIGN KEY (amap_fk_contact_referent_reseau_secondaire_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63B3E7E790 FOREIGN KEY (adresse_admin_ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87ABDC63A4B3F446 ON ak_amap (amap_fk_contact_referent_reseau_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87ABDC633BC43DD2 ON ak_amap (amap_fk_contact_referent_reseau_secondaire_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
