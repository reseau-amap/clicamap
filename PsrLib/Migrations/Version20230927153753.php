<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230927153753 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE ak_modele_contrat
            SET mc_amapien_deplacement_mode = 'demi'
            WHERE mc_amapien_deplacement_mode IS NULL
            AND (mc_amapien_permission_report_livraison = 1 || ak_modele_contrat.mc_amapien_permission_deplacement_livraison = 1)
");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
