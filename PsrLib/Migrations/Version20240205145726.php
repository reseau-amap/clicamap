<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\Services\PhpDiContrainerSingleton;
use PsrLib\Services\UsernameGenerator;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240205145726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $usernameGenerator = PhpDiContrainerSingleton::getContainer()->get(UsernameGenerator::class);
        $usersWithoutUsername = $this->connection->fetchAllAssociative('SELECT id,name_first_name,name_last_name FROM ak_user WHERE username IS NULL');
        foreach ($usersWithoutUsername as $user) {
            $usernameTemplate = $usernameGenerator->generate($user['name_first_name'], $user['name_last_name']);
            $username = $usernameTemplate;
            $i = 2;
            while ($this->connection->fetchOne('SELECT COUNT(*) FROM ak_user WHERE username = :username', ['username' => $username]) > 0) {
                $username = $usernameTemplate.$i++;
            }

            $this->addSql('UPDATE ak_user SET username = :username WHERE id = :id', ['username' => $username, 'id' => $user['id']]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
