<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231115182655 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_campagne (id INT AUTO_INCREMENT NOT NULL, amap_id INT NOT NULL, version_suivante_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, annee_adhesion_amapien INT NOT NULL, paiement_methode VARCHAR(255) NOT NULL, paiement_description LONGTEXT NOT NULL, champ_libre LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, period_start_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', period_end_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', INDEX IDX_48880B4B52AA66E8 (amap_id), UNIQUE INDEX UNIQ_48880B4B453848C3 (version_suivante_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_campagne (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_9ec45d817564592b7d442f6c1c13f4a9_idx (type), INDEX object_id_9ec45d817564592b7d442f6c1c13f4a9_idx (object_id), INDEX discriminator_9ec45d817564592b7d442f6c1c13f4a9_idx (discriminator), INDEX transaction_hash_9ec45d817564592b7d442f6c1c13f4a9_idx (transaction_hash), INDEX blame_id_9ec45d817564592b7d442f6c1c13f4a9_idx (blame_id), INDEX created_at_9ec45d817564592b7d442f6c1c13f4a9_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_campagne_montant (id INT AUTO_INCREMENT NOT NULL, campagne_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, montant_libre TINYINT(1) NOT NULL, montant BIGINT NOT NULL, description LONGTEXT DEFAULT NULL, ordre INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_312B0D2916227374 (campagne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_campagne_montant (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_4737bfbe8c1c15ef7707413867416bb5_idx (type), INDEX object_id_4737bfbe8c1c15ef7707413867416bb5_idx (object_id), INDEX discriminator_4737bfbe8c1c15ef7707413867416bb5_idx (discriminator), INDEX transaction_hash_4737bfbe8c1c15ef7707413867416bb5_idx (transaction_hash), INDEX blame_id_4737bfbe8c1c15ef7707413867416bb5_idx (blame_id), INDEX created_at_4737bfbe8c1c15ef7707413867416bb5_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_campagne ADD CONSTRAINT FK_48880B4B52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->addSql('ALTER TABLE ak_campagne ADD CONSTRAINT FK_48880B4B453848C3 FOREIGN KEY (version_suivante_id) REFERENCES ak_campagne (id)');
        $this->addSql('ALTER TABLE ak_campagne_montant ADD CONSTRAINT FK_312B0D2916227374 FOREIGN KEY (campagne_id) REFERENCES ak_campagne (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
