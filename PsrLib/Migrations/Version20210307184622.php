<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $existingFermePaysan = $this
            ->connection
            ->executeQuery(
                'SELECT f_pay_fk_ferme_id, f_pay_fk_paysan_id FROM ak_ferme_paysan
                WHERE f_pay_fk_ferme_id IN (SELECT f_id FROM ak_ferme);'
            )
        ;

        $this->addSql('ALTER TABLE ak_paysan ADD ferme_id BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D86018981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        foreach ($existingFermePaysan as $fp) {
            $this->addSql('UPDATE ak_paysan SET ferme_id = :fermeId WHERE pay_id = :payId', [
                'fermeId' => $fp['f_pay_fk_ferme_id'],
                'payId' => $fp['f_pay_fk_paysan_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
