<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184632 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD attestation_certification_id INT DEFAULT NULL');
        $this->addSql('DELETE FROM ak_ferme_type_production_propose WHERE ferme_tpp_fk_ferme_id NOT IN (SELECT f.f_id FROM ak_ferme f)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD CONSTRAINT FK_DF572660DA1F6AB5 FOREIGN KEY (ferme_tpp_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose ADD CONSTRAINT FK_DF57266059114409 FOREIGN KEY (attestation_certification_id) REFERENCES file (id)');

        $existingFiles = $this
            ->connection
            ->executeQuery('SELECT ferme_tpp_id, ferme_tpp_lien_attestation_certification FROM ak_ferme_type_production_propose')
            ->fetchAll()
        ;

        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        foreach ($existingFiles as $existingFile) {
            $file = new FermeTypeProductionAttestationCertification($existingFile['ferme_tpp_lien_attestation_certification'], $existingFile['ferme_tpp_lien_attestation_certification']);
            $em->persist($file);
            $em->flush();

            $this->addSql('UPDATE ak_ferme_type_production_propose SET attestation_certification_id=:certId WHERE ferme_tpp_id=:tppId', [
                'certId' => $file->getId(),
                'tppId' => $existingFile['ferme_tpp_id'],
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
