<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241213100137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_modele_contrat
    CHANGE COLUMN mc_produits_identique_paysan information_livraison_mc_produits_identique_paysan           TINYINT(1) DEFAULT NULL,
    CHANGE COLUMN mc_nbliv_plancher information_livraison_mc_nbliv_plancher                      INT        DEFAULT NULL,
    CHANGE COLUMN mc_produits_identique_amapien information_livraison_mc_produits_identique_amapien          TINYINT(1) DEFAULT NULL,
    CHANGE COLUMN mc_amapien_permission_deplacement_livraison information_livraison_mc_amapien_permission_dpcmt_livraison  TINYINT(1) DEFAULT NULL,
    CHANGE COLUMN mc_amapien_deplacement_nb information_livraison_mc_amapien_deplacement_nb              INT        DEFAULT NULL,
    CHANGE COLUMN mc_amapien_permission_report_livraison information_livraison_mc_amapien_permission_report_livraison TINYINT(1) DEFAULT NULL,
    CHANGE COLUMN mc_amapien_report_nb information_livraison_mc_amapien_report_nb                   INT        DEFAULT NULL,
    CHANGE COLUMN mc_nbliv_plancher_depassement information_livraison_mc_nbliv_plancher_depassement          TINYINT(1) DEFAULT NULL,
    CHANGE COLUMN mc_amapien_deplacement_mode information_livraison_mc_amapien_deplacement_mode VARCHAR(255) DEFAULT NULL
;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
