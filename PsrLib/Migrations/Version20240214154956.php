<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240214154956 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $delais = $this->connection->fetchAll('SELECT f_id, f_delai_modif_contrat FROM ak_ferme');

        $this->addSql('ALTER TABLE ak_ferme DROP f_delai_modif_contrat');
        $this->addSql('ALTER TABLE ak_modele_contrat ADD delai_modif_contrat INT DEFAULT NULL');

        foreach ($delais as $delai) {
            $this->addSql('UPDATE ak_modele_contrat SET delai_modif_contrat = :delai WHERE mc_fk_ferme_id = :id', ['delai' => $delai['f_delai_modif_contrat'], 'id' => $delai['f_id']]);
        }
        $this->addSql('UPDATE ak_modele_contrat SET delai_modif_contrat = 2 WHERE delai_modif_contrat IS NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
