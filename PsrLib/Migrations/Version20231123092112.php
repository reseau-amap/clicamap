<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231123092112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_campagne_bulletin_recus (id INT AUTO_INCREMENT NOT NULL, pdf_id INT DEFAULT NULL, numero_interne INT NOT NULL, numero VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_A465302511FC912 (pdf_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _audit_ak_campagne_bulletin_recus (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_1e09685ec2bad47c98ed2b1983e8619c_idx (type), INDEX object_id_1e09685ec2bad47c98ed2b1983e8619c_idx (object_id), INDEX discriminator_1e09685ec2bad47c98ed2b1983e8619c_idx (discriminator), INDEX transaction_hash_1e09685ec2bad47c98ed2b1983e8619c_idx (transaction_hash), INDEX blame_id_1e09685ec2bad47c98ed2b1983e8619c_idx (blame_id), INDEX created_at_1e09685ec2bad47c98ed2b1983e8619c_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_campagne_bulletin_recus ADD CONSTRAINT FK_A465302511FC912 FOREIGN KEY (pdf_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_campagne ADD autheur_info_adresse VARCHAR(255) NOT NULL, ADD autheur_info_ville VARCHAR(255) NOT NULL, ADD autheur_info_site VARCHAR(255) NOT NULL, ADD autheur_info_telephone VARCHAR(255) NOT NULL, ADD autheur_info_siret VARCHAR(255) NOT NULL, ADD autheur_info_rna VARCHAR(255) NOT NULL, ADD autheur_info_emails VARCHAR(255) NOT NULL, CHANGE auteur_label autheur_info_nom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_campagne_bulletin ADD recus_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_campagne_bulletin ADD CONSTRAINT FK_5CE2613AD012BDD FOREIGN KEY (recus_id) REFERENCES ak_campagne_bulletin_recus (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5CE2613AD012BDD ON ak_campagne_bulletin (recus_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
