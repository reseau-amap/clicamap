<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220101170625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DELETE FROM ak_amap_collectif WHERE amap_coll_fk_amapien_id NOT IN (SELECT a_id FROM ak_amapien);');
        $this->addSql('DELETE FROM ak_amap_collectif WHERE amap_coll_fk_amap_id NOT IN (SELECT amap_id FROM ak_amap);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
