<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220422151836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_contrat_livraison (id INT AUTO_INCREMENT NOT NULL, amapien_id INT NOT NULL, ferme_id INT NOT NULL, date DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', livre TINYINT(1) NOT NULL, INDEX IDX_278C084BC7A2B78 (amapien_id), INDEX IDX_278C08418981132 (ferme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ak_contrat_livraison_cellule (id INT AUTO_INCREMENT NOT NULL, contrat_livraison_id INT NOT NULL, contrat_cellule_id INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, INDEX IDX_606F7C5C58AF2EC (contrat_livraison_id), UNIQUE INDEX UNIQ_606F7C52B1F68CD (contrat_cellule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_contrat_livraison ADD CONSTRAINT FK_278C084BC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->addSql('ALTER TABLE ak_contrat_livraison ADD CONSTRAINT FK_278C08418981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->addSql('ALTER TABLE ak_contrat_livraison_cellule ADD CONSTRAINT FK_606F7C5C58AF2EC FOREIGN KEY (contrat_livraison_id) REFERENCES ak_contrat_livraison (id)');
        $this->addSql('ALTER TABLE ak_contrat_livraison_cellule ADD CONSTRAINT FK_606F7C52B1F68CD FOREIGN KEY (contrat_cellule_id) REFERENCES ak_contrat_cellule (c_c_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
