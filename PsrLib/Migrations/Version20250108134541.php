<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250108134541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'49\' WHERE v_id=\'44060\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69004\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69020\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69047\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69048\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69049\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69050\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69052\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69055\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69056\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69059\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69076\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69080\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69094\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69118\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69121\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69125\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69133\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69136\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69154\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69179\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69190\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69212\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69213\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69236\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69268\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69272\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69280\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69285\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69289\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69291\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69294\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69295\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69297\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'69298\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'69\' WHERE v_id=\'1000388\'');
        $this->addSql('UPDATE ak_ville SET v_fk_dep_id=\'61\' WHERE v_id=\'1000594\';');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
