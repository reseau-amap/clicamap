<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220223085112 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_ferme_regroupement (id INT AUTO_INCREMENT NOT NULL, password_reset_token_id INT DEFAULT NULL, ville_id VARCHAR(5) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, adresse_admin VARCHAR(255) DEFAULT NULL, email_invalid TINYINT(1) NOT NULL, nom VARCHAR(255) DEFAULT NULL, siret VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_D16BD542AA2E96A0 (password_reset_token_id), INDEX IDX_D16BD542A73F0036 (ville_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_ferme_regroupement ADD CONSTRAINT FK_D16BD542AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('ALTER TABLE ak_ferme_regroupement ADD CONSTRAINT FK_D16BD542A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');

        $this->addSql('ALTER TABLE ak_ferme ADD regroupement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_ferme ADD CONSTRAINT FK_AD7E64AE98655AD2 FOREIGN KEY (regroupement_id) REFERENCES ak_ferme_regroupement (id)');
        $this->addSql('CREATE INDEX IDX_AD7E64AE98655AD2 ON ak_ferme (regroupement_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
