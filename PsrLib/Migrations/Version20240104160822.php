<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240104160822 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE ak_ferme_produit SET f_pro_nom = REPLACE(f_pro_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_ferme_produit SET f_pro_conditionnement = REPLACE(f_pro_conditionnement, '&amp;', '&');");
        $this->addSql("UPDATE ak_paysan_commentaire SET pay_com_com = REPLACE(pay_com_com, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_nom = REPLACE(mc_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_filiere = REPLACE(mc_filiere, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_description = REPLACE(mc_description, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_reglement_modalite = REPLACE(mc_reglement_modalite, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_contrat_annexes = REPLACE(mc_contrat_annexes, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat SET mc_specificite = REPLACE(mc_specificite, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat_produit SET mc_pro_conditionnement = REPLACE(mc_pro_conditionnement, '&amp;', '&');");
        $this->addSql("UPDATE ak_amap_livraison_lieu SET amap_liv_lieu = REPLACE(amap_liv_lieu, '&amp;', '&');");
        $this->addSql("UPDATE ak_amap SET amap_nom = REPLACE(amap_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_ferme SET f_nom = REPLACE(f_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_ferme SET f_description = REPLACE(f_description, '&amp;', '&');");
        $this->addSql("UPDATE ak_user SET username = REPLACE(username, '&amp;', '&');");
        $this->addSql("UPDATE ak_user SET name_first_name = REPLACE(name_first_name, '&amp;', '&');");
        $this->addSql("UPDATE ak_user SET name_last_name = REPLACE(name_last_name, '&amp;', '&');");
        $this->addSql("UPDATE ak_amap_collectif SET amap_coll_precision = REPLACE(amap_coll_precision, '&amp;', '&');");
        $this->addSql("UPDATE ak_modele_contrat_produit SET mc_pro_nom = REPLACE(mc_pro_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_amap_distribution SET detail_tache = REPLACE(detail_tache, '&amp;', '&');");
        $this->addSql("UPDATE adhesion_value SET payer = REPLACE(payer, '&amp;', '&');");
        $this->addSql("UPDATE adhesion_value SET payer = REPLACE(payer, '&AMP;', '&');");
        $this->addSql("UPDATE ak_evenement SET ev_nom = REPLACE(ev_nom, '&amp;', '&');");
        $this->addSql("UPDATE ak_evenement SET ev_txt = REPLACE(ev_txt, '&amp;', '&');");
        $this->addSql("UPDATE ak_campagne SET autheur_info_nom = REPLACE(autheur_info_nom, '&amp;', '&');");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
