<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231102144635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE _audit_file (id INT UNSIGNED AUTO_INCREMENT NOT NULL, type VARCHAR(10) NOT NULL, object_id VARCHAR(255) NOT NULL, discriminator VARCHAR(255) DEFAULT NULL, transaction_hash VARCHAR(40) DEFAULT NULL, diffs LONGTEXT DEFAULT NULL, blame_id VARCHAR(255) DEFAULT NULL, blame_user VARCHAR(255) DEFAULT NULL, blame_user_fqdn VARCHAR(255) DEFAULT NULL, blame_user_firewall VARCHAR(100) DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX type_5848afb23c03cca0db3a014178bd44f6_idx (type), INDEX object_id_5848afb23c03cca0db3a014178bd44f6_idx (object_id), INDEX discriminator_5848afb23c03cca0db3a014178bd44f6_idx (discriminator), INDEX transaction_hash_5848afb23c03cca0db3a014178bd44f6_idx (transaction_hash), INDEX blame_id_5848afb23c03cca0db3a014178bd44f6_idx (blame_id), INDEX created_at_5848afb23c03cca0db3a014178bd44f6_idx (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_amap ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63F98F144A FOREIGN KEY (logo_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87ABDC63F98F144A ON ak_amap (logo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
