<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220630081256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8DA73F0036;');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu DROP FOREIGN KEY FK_65938E81A73F0036;');
        $this->addSql('ALTER TABLE ak_reseau_villes DROP FOREIGN KEY FK_7E474F12F6AFA406;');
        $this->addSql('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63B3E7E790;');
        $this->addSql('ALTER TABLE ak_ferme DROP FOREIGN KEY FK_AD7E64AEA73F0036;');
        $this->addSql('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D860A73F0036;');
        $this->addSql('ALTER TABLE ak_ferme_regroupement DROP FOREIGN KEY FK_D16BD542A73F0036;');

        $this->addSql('ALTER TABLE ak_ville MODIFY COLUMN v_id VARCHAR(10);');
        $this->addSql('ALTER TABLE ak_ferme MODIFY COLUMN ville_id VARCHAR(10);');
        $this->addSql('ALTER TABLE ak_paysan MODIFY COLUMN ville_id VARCHAR(10);');

        $this->addSql("UPDATE ak_ville SET v_id = REPLACE(v_id, 'A', '90');");
        $this->addSql("UPDATE ak_ville SET v_id = REPLACE(v_id, 'B', '91');");
        $this->addSql("UPDATE ak_amapien SET ville_id = REPLACE(ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_amap_livraison_lieu SET ville_id = REPLACE(ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_reseau_villes SET res_v_fk_ville_id = REPLACE(res_v_fk_ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_amap SET adresse_admin_ville_id = REPLACE(adresse_admin_ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_ferme SET ville_id = REPLACE(ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_paysan SET ville_id = REPLACE(ville_id, 'A', '90');");
        $this->addSql("UPDATE ak_ferme_regroupement SET ville_id = REPLACE(ville_id, 'A', '90');");

        $this->addSql('ALTER TABLE ak_ville MODIFY COLUMN v_id  INT AUTO_INCREMENT;');

        $this->addSql('ALTER TABLE ak_amap_livraison_lieu CHANGE ville_id ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu ADD CONSTRAINT FK_65938E81A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_amapien CHANGE ville_id ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8DA73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_paysan CHANGE ville_id ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_amap CHANGE adresse_admin_ville_id adresse_admin_ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63B3E7E790 FOREIGN KEY (adresse_admin_ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_reseau_villes CHANGE res_v_fk_ville_id res_v_fk_ville_id INT NOT NULL;');
        $this->addSql('ALTER TABLE ak_reseau_villes ADD CONSTRAINT FK_7E474F12F6AFA406 FOREIGN KEY (res_v_fk_ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_ferme CHANGE ville_id ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_ferme ADD CONSTRAINT FK_AD7E64AEA73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id);');
        $this->addSql('ALTER TABLE ak_ferme_regroupement CHANGE ville_id ville_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE ak_ferme_regroupement ADD CONSTRAINT FK_D16BD542A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id);');

        $this->addSql('ALTER TABLE ak_ville AUTO_INCREMENT=1000000;');
        $this->addSql('ALTER TABLE ak_ville CHANGE v_nom v_nom VARCHAR(255) NOT NULL COLLATE utf8_general_ci');

        $this->addSql('DELETE FROM ak_ville WHERE v_nom = \'SAINT-LOUIS\' AND v_cp=97134;');
        $this->addSql('DELETE FROM ak_ville WHERE v_nom = \'THIÈVRES\' AND v_cp=62760;');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_88EBF3AF16250089B0F17121 ON ak_ville (v_cp, v_nom)');

        $this->addSql('ALTER TABLE ak_departement ADD osm_id VARCHAR(255) NOT NULL');

        $osmIdMap = [
            'AIN' => 7387,
            'AISNE' => 7411,
            'ALLIER' => 1_450_201,
            'ALPES-DE-HAUTE-PROVENCE' => 7380,
            'HAUTES-ALPES' => 7436,
            'ALPES-MARITIMES' => 7385,
            'ARDÈCHE' => 7430,
            'ARDENNES' => 7395,
            'ARIÈGE' => 7439,
            'AUBE' => 7441,
            'AUDE' => 7446,
            'AVEYRON' => 7451,
            'BOUCHES-DU-RHÔNE' => 7393,
            'CALVADOS' => 7453,
            'CANTAL' => 7381,
            'CHARENTE' => 7428,
            'CHARENTE-MARITIME' => 7431,
            'CHER' => 7456,
            'CORRÈZE' => 7464,
            'CÔTE-D\'OR' => 7424,
            'CÔTES-D\'ARMOR' => 7398,
            'CREUSE' => 7459,
            'DORDOGNE' => 7375,
            'DOUBS' => 7462,
            'DRÔME' => 7434,
            'EURE' => 7435,
            'EURE-ET-LOIR' => 7374,
            'FINISTÈRE' => 102430,
            'CORSE-DU-SUD' => 76932,
            'HAUTE-CORSE' => 76931,
            'GARD' => 7461,
            'HAUTE-GARONNE' => 7413,
            'HAUTE-LOIRE' => 7452,
            'GERS' => 7422,
            'GIRONDE' => 7405,
            'HÉRAULT' => 7429,
            'ILLE-ET-VILAINE' => 7465,
            'INDRE' => 7417,
            'INDRE-ET-LOIRE' => 7408,
            'ISÈRE' => 7437,
            'JURA' => 7460,
            'LANDES' => 7376,
            'LOIR-ET-CHER' => 7399,
            'LOIRE' => 7420,
            'LOIRE-ATLANTIQUE' => 7432,
            'LOIRET' => 7440,
            'LOT' => 7454,
            'LOT-ET-GARONNE' => 1_284_995,
            'LOZÈRE' => 7421,
            'MAINE-ET-LOIRE' => 7409,
            'MANCHE' => 7404,
            'MARNE' => 7379,
            'HAUTE-MARNE' => 7396,
            'MAYENNE' => 7438,
            'MEURTHE-ET-MOSELLE' => 51856,
            'MEUSE' => 7382,
            'MORBIHAN' => 7447,
            'MOSELLE' => 51854,
            'NIÈVRE' => 7448,
            'NORD' => 7400,
            'OISE' => 7427,
            'ORNE' => 7419,
            'PAS-DE-CALAIS' => 7394,
            'PUY-DE-DÔME' => 7406,
            'PYRÉNÉES-ATLANTIQUES' => 7450,
            'HAUTES-PYRÉNÉES' => 7467,
            'PYRÉNÉES-ORIENTALES' => 7466,
            'BAS-RHIN' => 7415,
            'HAUT-RHIN' => 7403,
            'RHÔNE' => 4_850_451,
            'HAUTE-SAÔNE' => 7423,
            'SAÔNE-ET-LOIRE' => 7397,
            'SARTHE' => 7443,
            'SAVOIE' => 7425,
            'HAUTE-SAVOIE' => 7407,
            'PARIS' => 71525,
            'SEINE-MARITIME' => 7426,
            'SEINE-ET-MARNE' => 7383,
            'YVELINES' => 7457,
            'DEUX-SÈVRES' => 7455,
            'SOMME' => 7463,
            'TARN' => 7442,
            'TARN-ET-GARONNE' => 7388,
            'VAR' => 7390,
            'VAUCLUSE' => 7445,
            'VENDÉE' => 7402,
            'VIENNE' => 7377,
            'HAUTE-VIENNE' => 7418,
            'VOSGES' => 7384,
            'YONNE' => 7392,
            'TERRITOIRE DE BELFORT' => 7410,
            'ESSONNE' => 7401,
            'HAUTS-DE-SEINE' => 7449,
            'SEINE-SAINT-DENIS' => 7389,
            'VAL-DE-MARNE' => 7458,
            'VAL-D\'OISE' => 7433,
            'GUADELOUPE' => 1_047_206,
            'MARTINIQUE' => 2_473_088,
            'GUYANE' => 2_202_121,
            'LA RÉUNION' => 77601,
        ];
        foreach ($osmIdMap as $depName => $osmId) {
            $this->addSql('UPDATE ak_departement SET osm_id=:id WHERE dep_nom=:nom', [
                'id' => $osmId,
                'nom' => $depName,
            ]);
        }

        // Add lyon metropole department
        $this->addSql('INSERT INTO ak_departement (dep_id, dep_nom, dep_fk_reg_id, osm_id) VALUE (\'69M\', \'MÉTROPOLE DE LYON\', \'84\', \'4850450\')');
        $this->addSql(
            "UPDATE ak_ville SET v_fk_dep_id='69M' WHERE v_cp IN (
            '69000',
            '69006',
            '69002',
            '69007',
            '69008',
            '69005',
            '69001',
            '69004',
            '69270',
            '69300',
            '69660',
            '69120',
            '69270',
            '69270',
            '69250',
            '69270',
            '69250',
            '69730',
            '69250',
            '69650',
            '69650',
            '69580',
            '69580',
            '69009',
            '69003',
            '69100',
            '69140',
            '69150',
            '69330',
            '69330',
            '69960',
            '69700',
            '69270',
            '69270',
            '69250',
            '69250',
            '69680',
            '69800',
            '69200',
            '69500',
            '69780',
            '69190',
            '69320',
            '69360',
            '69450',
            '69370',
            '69520',
            '69390',
            '69540',
            '69310',
            '69600',
            '69350',
            '69110',
            '69230',
            '69390',
            '69380',
            '69290',
            '69260',
            '69280',
            '69410',
            '69130',
            '69890',
            '69570',
            '69290',
            '69760',
            '69250',
            '69160',
            '69340'
            )"
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
