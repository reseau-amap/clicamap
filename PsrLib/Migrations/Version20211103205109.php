<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211103205109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ak_amap_distribution (id INT AUTO_INCREMENT NOT NULL, amap_livraison_lieu_id BIGINT NOT NULL, date DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon)\', detail_heure_debut VARCHAR(255) NOT NULL, detail_heure_fin VARCHAR(255) NOT NULL, detail_nb_personnes INT NOT NULL, detail_tache VARCHAR(255) NOT NULL, INDEX IDX_C219B4A1857FEC30 (amap_livraison_lieu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_amap_distribution ADD CONSTRAINT FK_C219B4A1857FEC30 FOREIGN KEY (amap_livraison_lieu_id) REFERENCES ak_amap_livraison_lieu (amap_liv_lieu_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
