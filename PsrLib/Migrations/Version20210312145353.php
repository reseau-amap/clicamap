<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\ORM\Entity\Files\EvenementImg;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312145353 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ak_evenement ADD img_id INT DEFAULT NULL, ADD pj_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601C06A9F55 FOREIGN KEY (img_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601F26B1C97 FOREIGN KEY (pj_id) REFERENCES file (id)');

        $existingFiles = $this
            ->connection
            ->executeQuery('SELECT ev_id, ev_img FROM ak_evenement WHERE ev_img IS NOT NULL')
            ->fetchAll()
        ;

        $em = \PsrLib\Services\PhpDiContrainerSingleton::getContainer()->get(EntityManagerInterface::class);
        foreach ($existingFiles as $existingFile) {
            $file = new EvenementImg($existingFile['ev_img']);
            $em->persist($file);
            $em->flush();
            $em->clear(); // optimisation

            $this->addSql('UPDATE ak_evenement SET img_id=:imgId WHERE ev_id=:evId', [
                'imgId' => $file->getId(),
                'evId' => $existingFile['ev_id'],
            ]);
        }

        $this->addSql('ALTER TABLE ak_evenement DROP ev_img, DROP ev_pj');
        $this->addSql('UPDATE ak_evenement set ev_date_fin = NULL WHERE ev_date_fin = \'\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
