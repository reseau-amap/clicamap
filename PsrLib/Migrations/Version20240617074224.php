<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240617074224 extends AbstractMigration
{
    public const ACTUS = [['titre' => 'Version 4.4',
        'date' => '2024-04-01',
        'description' => '
<p><u>AMAP</u></p>
<ul>
    <li><u></u>Déplacement
        de la notion "Délai de la ferme" (Gestion de la ferme ==&gt; Gestion des contrats)
    </li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>Déplacement de la notion "Délai de la ferme" (Gestion de la ferme ==&gt; Gestion des contrats)</li>
    <li>Activation ou désactivation d\'un produit dans la liste des produits proposés par la ferme</li>
</ul><p><u>Réseau</u></p>
<ul>
    <li>Evolution du fichier de téléchargement pour les départements</li>
</ul><p>Général </p>
<ul>
    <li>Correction de bug sur le jour de livraison de l\'AMAP sur l\'étape 2 du contrat vierge</li>
    <li>Correction du message d\'erreur sur l\'amapien.ne déjà en AMAP et/ou dans l\'AMAP</li>
    <li>Correction d\'erreur de plume sur le contrat juridique</li>
    <li>Correction de bugs sur la génération des reçus d\'adhésion en lot</li>
</ul>
', ], ['titre' => 'Version 4.3',
            'date' => '2023-12-01',
            'description' => '
<p><u>Global </u></p>
<ul>
    <li><u></u>Gestion
        des campagnes d\'adhésions et de dons (V2) pour les AMAP
    </li>
    <li>Dépôt de documents en PDF des réseaux sur le compte de l\'AMAP</li>
</ul>
', ], ['titre' => 'Version 4.2',
                'date' => '2023-07-01',
                'description' => "
<p><u>Global </u></p>
<ul>
    <li><u></u>Création
        d'un contrat vierge à partir d'un contrat passé (vierge, signé ou archivé)
    </li>
    <li>Sauvegarde à chaque étape de la création du contrat vierge</li>
    <li>Création
        d'un onglet \"Gestion des contrats archivés\" pour les contrats signés
        après la dernière livraison du contrat. Les fonctionnalités de la gestion\u{a0}
        des contrats signés sont reprises dans la Gestion des contrats archivés
        hormis le déplacement de livraison sous tous les profils sauf celui des paysans
    </li>
    <li>Fonctionnalité \"Copie la
        première ligne partout\" est possible sur un contrat signé ayant des
        livraisons déjà réalisées, à partir de la première date active
    </li>
    <li>Suppression
        d'un amapien Les amapien.nes qui ont un contrat actif
        ne peuvent pas être supprimé sauf en mettant à mettre à zéro les dates
        de livraison à venir.La suppression de l'amapien n'a pas d'incidence dans la gestion
        des contrats passés/archivés, ils sont conservés sous les profils AMAP, référent, paysan.
    </li>
</ul>
", ], ['titre' => 'Version 4',
                    'date' => '2023-05-01',
                    'description' => '
<p><u>Global </u></p>
<ul>
    <li><u></u>évolution
        des droits d\'accès
    </li>
    <li>fiche identique de l\'amapien et du paysan</li>
    <li>création du compte utilisateur [prénom.nom], générer automatiquement pour les comptes existants avant la mise en
        ligne
    </li>
    <li>l\'élément unique des personnes physiques devient le "Nom d’utilisateur unique clic\'AMAP"</li>
</ul><p><u>Amapien :</u></p>
<ul>
    <li>un amapien peut "passer" d\'une AMAP à une autre et également être dans plusieurs AMAP</li>
    <li>un amapien peut avoir plusieurs adresses mails sous son profil</li>
</ul><p><u>Paysan :</u></p>
<ul>
    <li>un paysan peut également être amapien, les fonctionnalités sont fusionnées sous le même profil</li>
</ul><p><u>AMAP</u></p>
<ul>
    <li>Nommer un ou des administrateurs amapien.nes, ces derniers ont alors sous leur profil les fonctionnalités AMAP
    </li>
</ul>
', ], ['titre' => 'Version 3.8',
                        'date' => '2022-06-01',
                        'description' => '
<p><u>Paysan</u></p>
<ul>
    <li>Nouvelle fonctionnalité introduite : le suivi des
        livraisons<u></u></li>
</ul><p><u>Réseau</u></p>
<ul>
    <li>Les admins peuvent consulter la liste des amapiens par AMAP</li>
    <li>Nouveau fichier de téléchargement pour les départements</li>
</ul>
', ], ['titre' => 'Version 3.7',
                            'date' => '2022-04-01',
                            'description' => '
<p><u>Global</u></p>
<ul>
    <li><u><span
    ></u>Remplacer les icônes "outils" par un bouton déroulant "action"</li>
    <li>L\'onglet "www.AMAP" du menu est renommé "Lien"</li>
    <li>Correction des mails distrib\'AMAP envoyé en copie à l\'AMAP</li>
</ul><p><u>Amapien</u></p>
<ul>
    <li><p>Permettre aux amapiens de connaitre les personnes inscrites aux distrib\'AMAP</p><span
    ><u><span
    ></u></li>
</ul><p></p>
', ], ['titre' => 'Version 3.6',
                                'date' => '2022-04-01',
                                'description' => '
<p><u>Global</u></p>
<ul>
    <li><u><span
    ></u>Correction des mails distrib\'AMAP</li>
</ul><p><u>Admin</u></p>
<ul>
    <li><p><u></u>Nouveau fichier d\'export AMAP pour les admins région
    </p></li>
</ul>
', ], ['titre' => 'Version 3.5',
                                    'date' => '2022-03-01',
                                    'description' => '
<p><u>Global</u></p>
<ul>
    <li><p><u></u>Création d\'un nouveau profil "Regroupement de fermes"</p></li>
</ul>
', ], ['titre' => 'Version 3.4',
                                        'date' => '2022-02-01',
                                        'description' => '
<p><u>Global</u></p>
<ul>
    <li><u><span
    ></u>Mise en ligne de la nouvelle version
        de la distrib\'AMAP</li>
    <li>Corrections de bug</li>
</ul><p><u>Amapien</u></p>
<ul>
    <li>Menu: nouvel onglet "Mon agenda" regroupant "Mes livraisons" et "Mes ditrib\'AMAP"
    </li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>L\'icône de validation d\'un contrat est remplacé par un bouton plus visible
    </li>
</ul><p></p><u></u>
', ], ['titre' => 'Version 3.3',
                                            'date' => '2021-09-01',
                                            'description' => '
<p><u>Global</u></p>
<ul>
    <li><u><span
    ></u>Modification de la prise en compte du
        délai de la ferme (0 = jour de livraison)</li>
</ul><u></u><p><u>Référent</u>
</p>
<ul>
    <li>Souscription à la place d\'un amapien, la liste déroulante est trié par ordre alphabétique
    </li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>Lors de la validation d\'un contrat, affichage des règles définies pour le contrat
    </li>
</ul>
', ], ['titre' => 'Version 3.2',
                                                'date' => '2021-07-01',
                                                'description' => '<p><u>Global</u></p>
<ul>
    <li>Le tableau des événements est désormais triable par colonne<u><span
    ></u></li>
</ul><u></u><p><u>Amapien</u></p>
<ul>
    <li>Seul les produits souscrits (quantité non nulle) sont affichés dans le calendrier "Mes livraisons" de chaque amapien
    </li>
    <li>Lors de l\'édition de son profil le bouton "Annuler" renvoie sur la bonne page<u><span
    ></u></li>
</ul><p><u>AMAP</u></p>
<ul>
    <li>Le module de gestion des adhésions amapien est disponible
    </li>
    <li>Création d\'un événement :
        <ul>
            <li>Les champs date et heure ne sont plus obligatoires
            </li>
            <li>La taille maximale des pièces jointes passe à 1M</li>
        </ul>
    </li>
</ul><p><u>Réseau</u></p>
<ul>
    <li>Modification du profil désormais possible</li>
    <li>Nouveaux champs lors de la création d\'une AMAP</li>
    <li>Corrections diverses</li>
</ul>
', ], ['titre' => 'Version 3.1.1',
                                                    'date' => '2021-06-01',
                                                    'description' => '<p><u>Global</u></p>
<ul>
    <li>Il est possible de mettre en forme les événements grâce à l\'éditeur graphique <a
            href="https://summernote.org/">summernote</a></li>
    <li>Correction de bugs</li>
</ul>
', ], ['titre' => 'Version 3.1',
                                                        'date' => '2021-06-01',
                                                        'description' => '<p><u>Global</u></p>
<ul>
    <li>Le menu se condense en une seule icône en affichage mobile (téléphone, tablette)
    </li>
    <li>L\'affichage des calendriers a été amélioré</li>
    <li>La présentation de nombreuses pages a été modifiée/améliorée
    </li>
    <li>Corrections diverses</li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>Un
 onglet "Gestionnaire ferme" a été rajouté au menu du profil paysan,
permettant par exemple d\'accéder directement aux produits proposés </li>
</ul>
', ], ['titre' => 'Version 3.0',
                                                            'date' => '2021-05-01',
                                                            'description' => '<p><u>Global</u></p>
<ul>
    <li>Migration sous <a
            href="https://fr.wikipedia.org/wiki/Doctrine_(ORM)">Doctrine</a> (énorme travail technique qui va faciliter le travail des développeurs et permettre des améliorations/corrections plus rapide)
    </li>
    <li>Changement d\'hébergeur, le site tourne désormais chez <a
            href="https://www.infomaniak.com/fr">infomaniak</a></li>
</ul>
', ], ['titre' => 'Version 2.2.10',
                                                                'date' => '2020-11-01',
                                                                'description' => '<p><u>Global</u></p>
<ul>
    <li>La bannière clic\'amap disparait une fois connecté
    </li>
    <li>Les
 événements : les noms sont cliquables, les dates et les heures ont été
regroupés dans une même colonne et un bouton "Retour" permet de revenir
en arrière</li>
    <li><span
    ><font color="#FF0000">Attention</font> les calendriers "Mes livraisons" et "Mes distib\'AMAP" sont désormais affichés avec lundi comme premier jour de la semaine
    </li>
    <li>Les icônes contrats xls et pdf ont été changées pour rester dans le thème
    </li>
    <li>Le menu s\'affichera mieux en version mobile</li>
    <li>Le site se dote d\'une favicon
    </li>
    <li>Corrections textuelles et cosmétiques diverses</li>
</ul><p><u>AMAP</u></p>
<ul>
    <li>Nouveau menu : <font color="#FF0000">attention</font> les onglets "Gestionnaire" et "Gestion des contrats" ont été regroupés dans un seul onglet "Gestionnaire AMAP"
    </li>
    <li>Lors de la création d\'un amapien ou paysan, on remplit désormais d\'abord son "Nom" puis ensuite son "Prénom"
    </li>
</ul><p><u>Référent</u></p>
<ul>
    <li>Nouveau menu : <span
    ><font color="#FF0000">attention</font> les onglets "Gestionnaire" et "Gestion des contrats" ont été regroupés dans un seul onglet "Gestionnaire référent"
    </li>
    <li>Contrat vierge :
        <ul>
            <li>le calendrier pour choisir les dates est désormais en français avec, <span
            ><font color="#FF0000">attention</font>, lundi comme premier jour de la semaine
            </li>
            <li>modifications cosmétiques de l\'étape 1 et 4 pour améliorer la lisibilité
            </li>
            <li>le nombre de règlement ne peut plus être égal à 0
            </li>
            <li>l\'icône "Valider le contrat" change pour une nouvelle icône "Envoyer en validation paysan"
            </li>
            <li>ajout du bouton "Les produits proposés par la ferme" pour un accès plus rapide
            </li>
        </ul>
    </li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>Modification des libellés lors de la validation du contrat vierge
    </li>
    <li>Correction de la visualisation des autres paysans d\'une ferme
    </li>
</ul>
', ], ['titre' => 'Version 2.2.9',
                                                                    'date' => '2020-09-01',
                                                                    'description' => '<p><u>Global</u></p>
<ul>
    <li>Modification du système d’envoi d’email
    </li>
    <li>Nouveau domaine : <a
            href="https://clicamap.amap-aura.org">clicamap.amap-aura.org</a> devient <a href="https://www.clicamap.org">www.clicamap.org</a>
    </li>
</ul><p><u>Paysan</u></p>
<ul>
    <li>Les années d\'adhésion paysan sont désormais à définir dans la gestion ferme
    </li>
</ul>
', ], ['titre' => 'Version 2.2.8',
                                                                        'date' => '2020-06-01',
                                                                        'description' => '<p><u>Amapien</u></p>
<ul>
    <li>Message d\'info en cas de déconnexion</li>
    <li>Un amapien sur liste d\'attente ne peut plus souscrire à un contrat
    </li>
    <li>Problème d\'arrondi résolu sur le montant de certains contrats
    </li>
</ul><p><u>Référent</u></p>
<ul>
    <li>Les produits sont désormais classés par ordre alphabétique
    </li>
    <li>Le référent ou supérieur, peut observer le PDF du contrat vierge avant la validation par le paysan
    </li>
</ul>
', ], ['titre' => 'Version 2.2.7',
                                                                            'date' => '2020-04-01',
                                                                            'description' => '<p><u>Amapien</u></p>
<ul>
    <li>Un amapien peut désormais voir son contrat jusqu\'au jour de la dernière livraison
    </li>
    <li>Un amapien peut désormais sélectionner plusieurs méthodes de paiements lors de la souscription à un contrat
    </li>
    <li>Modification textuel du contrat juridique</li>
</ul><p><u>Référent</u></p>
<ul>
    <li>Un référent ou un paysan peut désormais télécharger une synthèse des règlements du contrat
    </li>
    <li>Gestion des produits :
        <ul>
            <li>un référent ou un paysan peut désormais rajouter un label (AB, N&P, En conversion AB, Non labellisé, vide)
            </li>
            <li>des
 nouveaux type de production sont disponible (Céréales et légumineuses,
Huiles, Fruits transformés, Plantes aromatiques, Viandes - Cabri,
Viandes - Veau, Vin et bières, Pains et farines)</li>
        </ul>
    </li>
</ul><p><u>Administrateur</u></p>
<ul>
    <li>Les contrats définis inutiles ont été supprimés
    </li>
</ul>
', ], ['titre' => 'Version 2.2.6',
                                                                                'date' => '2020-02-01',
                                                                                'description' => '<p><u>Amapien</u></p>
<ul>
    <li>Un amapien peut désormais supprimer son contrat avant la date de fin de souscription
    </li>
</ul><p><u>Référent</u></p>
<ul>
    <li>Un référent doit répondre à une question supplémentaire à l\'étape 4 de création d\'un contrat vierge
    </li>
    <li>Corrections textuels diverses
    </li>
</ul>
', ], ['titre' => 'Version 2.2.5',
                                                                                    'date' => '2020-01-01',
                                                                                    'description' => '
<ul>
    <li>Correction de bugs divers</li>
</ul>
', ], ['titre' => 'Version 2.2.4',
                                                                                        'date' => '2019-12-01',
                                                                                        'description' => '
<ul>
    <li>Version destinée aux AMAP étudiantes</li>
</ul>
', ], ['titre' => 'Version 2.2.3',
                                                                                            'date' => '2019-11-01',
                                                                                            'description' => '
<ul>
    <li>Le module “type de production” est déplacé dans la gestion des fermes
    </li>
</ul>
', ], ['titre' => 'Version 2.2.2',
                                                                                                'date' => '2019-10-01',
                                                                                                'description' => '
<ul>
    <li>Correction de bugs liés au contrat</li>
</ul>
', ], ['titre' => 'Version 2.2.1',
                                                                                                    'date' => '2019-10-01',
                                                                                                    'description' => '
<ul>
    <li>Correction de bugs</li>
</ul>
', ], ['titre' => 'Version 2.2',
                                                                                                        'date' => '2019-09-01',
                                                                                                        'description' => '
<ul>
    <li>Mise en place du contrat juridique dématérialisé
    </li>
</ul><p></p>', ]];

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        foreach (self::ACTUS as $actu) {
            $this->addSql('INSERT INTO actualite (titre, description,date, created_at, updated_at) VALUES (:titre, :description, :date, :created_at, :updated_at)', [
                'titre' => $actu['titre'],
                'description' => $actu['description'],
                'date' => $actu['date'],
                'created_at' => '1900-01-01',
                'updated_at' => '1900-01-01',
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
