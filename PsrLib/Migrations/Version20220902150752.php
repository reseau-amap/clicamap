<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220902150752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // fix name according to OSM
        $villes = $this
            ->connection
            ->executeQuery('SELECT v_id, v_nom FROM ak_ville')
            ->fetchAll()
        ;
        foreach ($villes as $ville) {
            $newName = $this->transformName($ville['v_nom']);
            $this->addSql('UPDATE ak_ville SET v_nom=:nom WHERE v_id=:id', [
                'id' => $ville['v_id'],
                'nom' => $newName,
            ]);
        }

        $this->addSql('UPDATE ak_departement SET dep_nom=\'TERRITOIRE-DE-BELFORT\' WHERE dep_nom = \'TERRITOIRE DE BELFORT\'');

        $this->addSql('UPDATE ak_ville SET v_nom = \'Paris\' WHERE v_nom LIKE \'Paris%Arrondissement\'');
        $this->addSql('UPDATE ak_ville SET v_nom = \'Marseille\' WHERE v_nom LIKE \'MARSEILLE%Arrondissement\'');
        $this->addSql('UPDATE ak_ville SET v_nom = \'Lyon\' WHERE v_nom LIKE \'LYON%Arrondissement\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    private function transformName(string $name)
    {
        $res = mb_convert_case(mb_strtolower($name), MB_CASE_TITLE, 'UTF-8');
        $res = ucwords($res, '\'');

        $res = str_replace('-De-', '-de-', $res);
        $res = str_replace('-Sur-', '-sur-', $res);
        $res = str_replace('-La-', '-la-', $res);
        $res = str_replace('-Et-', '-et-', $res);
        $res = str_replace('-Au-', '-au-', $res);
        $res = str_replace('-Des-', '-des-', $res);
        $res = str_replace('-Du-', '-du-', $res);
        $res = str_replace('-Sous-', '-sous-', $res);
        $res = str_replace('-En-', '-en-', $res);
        $res = str_replace('-Le-', '-le-', $res);
        $res = str_replace('-Les-', '-les-', $res);
        $res = str_replace('-D\'', '-d\'', $res);
        $res = str_replace('-L\'', '-l\'', $res);
        $res = str_replace('-Lès-', '-lès-', $res);
        $res = str_replace('-Aux-', '-aux-', $res);
        $res = str_replace('-Devant-', '-devant-', $res);
        $res = str_replace('-Près-', '-près-', $res);
        $res = str_replace('-Del-', '-del-', $res);
        $res = str_replace('-À-', '-à-', $res);
        $res = str_replace('-Ès-', '-ès-', $res);
        $res = str_replace('\'é', '\'É', $res);
        $res = str_replace('-Lez', '-lez-', $res);
        $res = str_replace('-Las-', '-las-', $res);
        $res = str_replace('-Di-', '-di-', $res);
        $res = str_replace('-Ez-', '-ez-', $res);
        $res = str_replace('-Ô-', '-ô-', $res);
        $res = str_replace('île-', 'Île-', $res);
        $res = str_replace('-Deux-', '-deux-', $res);
        $res = str_replace('-les-deux-', '-les-Deux-', $res);
        $res = str_replace('-Es-', '-es-', $res);
        $res = str_replace('-Dit-', '-dit-', $res);
        $res = str_replace('-Sans-', '-sans-', $res);
        $res = str_replace('-Derrière-', '-derrière-', $res);
        $res = str_replace('-Dels-', '-dels-', $res);

        // Remaining cases
        $res = str_replace('Neuvy-deux-Clochers', 'Neuvy-Deux-Clochers', $res);
        $res = str_replace("Gommenec'H", "Gommenec'h", $res);
        $res = str_replace("Kermoroc'H", "Kermoroc'h", $res);
        $res = str_replace("Ploulec'H", "Ploulec'h", $res);
        $res = str_replace("Plourac'H", "Plourac'h", $res);
        $res = str_replace("Guilligomarc'H", "Guilligomarc'h", $res);
        $res = str_replace("Plouezoc'H", "Plouezoc'h", $res);
        $res = str_replace("Berd'Huis", "Berd'huis", $res);
        $res = str_replace("Guitalens-l'Albarède", "Guitalens-L'Albarède", $res);

        return str_replace("Noirmoutier-en-l'île", "Noirmoutier-en-l'Île", $res);
    }
}
