<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211110151831 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE amap_distribution_amapien (amap_distribution_id INT NOT NULL, amapien_a_id BIGINT NOT NULL, INDEX IDX_F041D0408AEA9B1B (amap_distribution_id), INDEX IDX_F041D04019E9F30A (amapien_a_id), PRIMARY KEY(amap_distribution_id, amapien_a_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE amap_distribution_amapien ADD CONSTRAINT FK_F041D0408AEA9B1B FOREIGN KEY (amap_distribution_id) REFERENCES ak_amap_distribution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE amap_distribution_amapien ADD CONSTRAINT FK_F041D04019E9F30A FOREIGN KEY (amapien_a_id) REFERENCES ak_amapien (a_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
