<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103182937 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // Cleanup data
        $this->addSql('DELETE FROM ak_ville WHERE v_fk_dep_id=\'\';');

        // Fix engines
        $this->addSql('ALTER TABLE ak_departement ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE ak_region ENGINE = InnoDB;');

        // Fix character encoding
        $this->addSql('ALTER TABLE adhesion_amap CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE adhesion_amap_amapien CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE adhesion_amapien CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE adhesion_ferme CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE adhesion_value CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_absence CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_annee_adhesion CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_collectif CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_commentaire CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_distribution CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_livraison_horaire CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_recherche_paysan CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_type_production_propose CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amap_type_production_recherche CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amapien CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_amapien_annee_adhesion CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_contrat CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_contrat_cellule CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_departement CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_departement_administrateur CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_document CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement_info_amap CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement_info_departement CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement_info_ferme CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement_info_reseau CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_evenement_info_type_prod CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ferme CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ferme_amapien_liste_attente CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ferme_annee_adhesion CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ferme_produit CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ferme_type_production_propose CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_modele_contrat CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_modele_contrat_date CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_modele_contrat_dates_reglement CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit_exclure CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_paysan CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_paysan_cadre_reseau CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_paysan_commentaire CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_region CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_region_administrateur CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_reseau CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_reseau_administrateur CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_reseau_villes CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_type_production CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ak_ville CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE amap_distribution_amapien CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE doctrine_migration_versions CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE ferme_amapien CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE file CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE token CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');

        // Fix remainig keys
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63B3E7E790 FOREIGN KEY (adresse_admin_ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_amap_livraison_lieu ADD CONSTRAINT FK_65938E81A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8DA73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_departement ADD CONSTRAINT FK_CD4D5D97985E3896 FOREIGN KEY (dep_fk_reg_id) REFERENCES ak_region (reg_id)');
        $this->addSql('ALTER TABLE ak_departement ADD CONSTRAINT FK_CD4D5D97F98F144A FOREIGN KEY (logo_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_departement_administrateur ADD CONSTRAINT FK_DB2DFC6443D2066E FOREIGN KEY (dep_adm_fk_departement_id) REFERENCES ak_departement (dep_id)');
        $this->addSql('ALTER TABLE ak_evenement_info_departement ADD CONSTRAINT FK_9170BD4556A13F61 FOREIGN KEY (ev_info_dep_fk_dep_id) REFERENCES ak_departement (dep_id)');
        $this->addSql('ALTER TABLE ak_ferme ADD CONSTRAINT FK_AD7E64AEA73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_region ADD CONSTRAINT FK_4BADF42FF98F144A FOREIGN KEY (logo_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE ak_region_administrateur ADD CONSTRAINT FK_57A63CBA1DB39DDD FOREIGN KEY (reg_adm_fk_region_id) REFERENCES ak_region (reg_id)');
        $this->addSql('ALTER TABLE ak_reseau_villes ADD CONSTRAINT FK_7E474F12F6AFA406 FOREIGN KEY (res_v_fk_ville_id) REFERENCES ak_ville (v_id)');
        $this->addSql('ALTER TABLE ak_ville ADD CONSTRAINT FK_88EBF3AF72E220CB FOREIGN KEY (v_fk_dep_id) REFERENCES ak_departement (dep_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
