<?php

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210307184611 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE token (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon_immutable)\', token VARCHAR(255) NOT NULL, validity_in_hours INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ak_amap ADD password_reset_token_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87ABDC63AA2E96A0 ON ak_amap (password_reset_token_id)');
        $this->addSql('ALTER TABLE ak_amapien ADD password_reset_token_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8DAA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2CDF7F8DAA2E96A0 ON ak_amapien (password_reset_token_id)');
        $this->addSql('ALTER TABLE ak_paysan ADD password_reset_token_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CC11D860AA2E96A0 ON ak_paysan (password_reset_token_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
