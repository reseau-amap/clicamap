<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use PsrLib\Services\PhpDiContrainerSingleton;
use PsrLib\Services\UsernameGenerator;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230323105723 extends AbstractMigration
{
    private UsernameGenerator $usernameGenerator;

    private array $emailMap = [];
    private array $usernameMap = [];
    private array $amapienUserMap = [];
    private array $paysanFermeMap = [];

    public function getDescription(): string
    {
        return '';
    }

    private function executeQueryDisplay(string $query, array $params = [])
    {
        $this->write($query);

        return $this->connection->executeQuery($query, $params);
    }

    private function insertDisplay(string $table, array $data): void
    {
        $this->write(sprintf('INJECT %s : %s', $table, json_encode($data)));
        $this->connection->insert($table, $data);
    }

    private function generateUsername(?string $prenom, ?string $nom)
    {
        $username = $this->usernameGenerator->generate($prenom, $nom);
        if (!isset($this->usernameMap[$username])) {
            $this->usernameMap[$username] = 0;
        }

        ++$this->usernameMap[$username];
        if ($this->usernameMap[$username] > 1) {
            $username .= $this->usernameMap[$username];
        }

        return $username;
    }

    private function injectUserIfEmailNotExist(
        ?string $email,
        ?int $villeId,
        ?int $passwordResetTokenId,
        ?string $username,
        ?string $password,
        ?string $numTel1,
        ?string $numTel2,
        ?int $newsletter,
        ?string $uuid,
        ?int $superAdmin,
        ?string $nameFirstName,
        ?string $nameLastName,
        ?string $addressAdress,
        ?string $addressAdressCompl,
        ?string $adhesionInfoSite,
        ?string $adhesionInfoNomReseau,
        ?string $adhesionInfoSiret,
        ?string $adhesionInfoRna,
        ?string $adhesionInfoVilleSignature,
    ) {
        $emailLower = null === $email ? null : mb_strtolower($email);
        if (isset($this->emailMap[$emailLower])) {
            return $this->emailMap[$emailLower];
        }
        $this->insertDisplay('ak_user', [
            'id' => null,
            'ville_id' => $villeId,
            'password_reset_token_id' => $passwordResetTokenId,
            'username' => $username,
            'password' => $password,
            'num_tel1' => $numTel1,
            'num_tel2' => $numTel2,
            'newsletter' => $newsletter,
            'uuid' => $uuid,
            'super_admin' => $superAdmin,
            'name_first_name' => $nameFirstName,
            'name_last_name' => $nameLastName,
            'address_adress' => $addressAdress,
            'address_adress_compl' => $addressAdressCompl,
            'adhesion_info_nom_reseau' => $adhesionInfoNomReseau,
            'adhesion_info_siret' => $adhesionInfoSiret,
            'adhesion_info_rna' => $adhesionInfoRna,
            'adhesion_info_ville_signature' => $adhesionInfoVilleSignature,
            'adhesion_info_site' => $adhesionInfoSite,
        ]);
        $userId = $this->connection->lastInsertId();
        if (null !== $emailLower) {
            $this->insertDisplay('user_email', [
                'user_id' => $userId,
                'email' => $emailLower,
            ]);
            $this->emailMap[$emailLower] = $userId;
        }

        return $userId;
    }

    private function injectAmapiens(): void
    {
        $amapiens = $this
            ->connection
            ->executeQuery('SELECT * FROM ak_amapien')
            ->fetchAllAssociative()
        ;
        foreach ($amapiens as $amapien) {
            $userId = $this->injectUserIfEmailNotExist(
                $amapien['a_email'],
                $amapien['ville_id'],
                $amapien['password_reset_token_id'],
                $this->generateUsername($amapien['a_prenom'], $amapien['a_nom']),
                $amapien['a_password'],
                $amapien['a_num_tel_1'],
                $amapien['a_num_tel_2'],
                $amapien['newsletter'] ?? 0,
                $amapien['a_uuid'],
                $amapien['super_admin'],
                $amapien['a_prenom'],
                $amapien['a_nom'],
                $amapien['a_lib_adr_1'],
                $amapien['a_lib_adr_2'],
                $amapien['adhesion_info_site'],
                $amapien['adhesion_info_nom_reseau'],
                $amapien['adhesion_info_siret'],
                $amapien['adhesion_info_rna'],
                $amapien['adhesion_info_ville_signature'],
            );
            $this->executeQueryDisplay('UPDATE ak_amapien SET user_id=:user_id WHERE a_id=:a_id', [
                'user_id' => $userId,
                'a_id' => $amapien['a_id'],
            ]);
            $this->amapienUserMap[$amapien['a_id']] = $userId;
            $depAdmins = $this
                ->connection
                ->executeQuery('SELECT dep_adm_fk_departement_id FROM ak_departement_administrateur WHERE dep_adm_fk_amapien_id=:a_id', [
                    'a_id' => $amapien['a_id'],
                ])
                ->fetchFirstColumn()
            ;
            foreach ($depAdmins as $depAdmin) {
                $this->insertDisplay('user_departement', [
                    'user_id' => $userId,
                    'departement_id' => $depAdmin,
                ]);
            }
            $regAdmins = $this
                ->connection
                ->executeQuery('SELECT reg_adm_fk_region_id FROM ak_region_administrateur WHERE reg_adm_fk_amapien_id=:a_id', [
                    'a_id' => $amapien['a_id'],
                ])
                ->fetchFirstColumn()
            ;
            foreach ($regAdmins as $regAdmin) {
                $this->insertDisplay('user_region', [
                    'user_id' => $userId,
                    'region_id' => $regAdmin,
                ]);
            }
        }
    }

    private function injectAmaps(): void
    {
        $amaps = $this
            ->connection
            ->executeQuery('SELECT a.*,
       (SELECT ll.ville_id
        FROM ak_amap_livraison_lieu ll
        WHERE ll.amap_liv_lieu_fk_amap_id = a.amap_id
        ORDER BY ll.amap_liv_lieu_id
        LIMIT 1) ll_ville_id
FROM ak_amap a')
            ->fetchAllAssociative()
        ;

        foreach ($amaps as $amap) {
            if (null === $amap['amap_email_public']) {
                continue;
            }
            $userId = $this->injectUserIfEmailNotExist(
                $amap['amap_email_public'],
                $amap['ll_ville_id'],
                $amap['password_reset_token_id'],
                $this->generateUsername($amap['amap_nom'], null),
                $amap['amap_password'],
                '',
                '',
                0,
                $amap['amap_uuid'],
                0,
                $amap['amap_nom'],
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            );
            $this->executeQueryDisplay('INSERT INTO user_amap (amap_id, user_id) VALUES (:amap_id, :user_id)', [
                'user_id' => $userId,
                'amap_id' => $amap['amap_id'],
            ]);
        }
    }

    private function injectPaysans(): void
    {
        $paysans = $this
            ->connection
            ->executeQuery('SELECT p.* FROM ak_paysan p')
            ->fetchAllAssociative()
        ;

        foreach ($paysans as $paysan) {
            $userId = $this->injectUserIfEmailNotExist(
                $paysan['pay_email'],
                $paysan['ville_id'],
                null,
                $this->generateUsername($paysan['pay_prenom'], $paysan['pay_nom']),
                $paysan['pay_password'],
                $paysan['pay_num_tel_1'],
                $paysan['pay_num_tel_2'],
                $paysan['newsletter'],
                $paysan['pay_uuid'],
                0,
                $paysan['pay_prenom'],
                $paysan['pay_nom'],
                $paysan['pay_lib_adr'],
                null,
                null,
                null,
                null,
                null,
                null,
            );
            $this->executeQueryDisplay('UPDATE ak_paysan SET user_id=:user_id WHERE pay_id=:pay_id', [
                'user_id' => $userId,
                'pay_id' => $paysan['pay_id'],
            ]);
            if (null !== $paysan['ferme_id']) {
                $this->executeQueryDisplay('INSERT INTO paysan_ferme (fp_paysan_id, fp_ferme_id) VALUES (:fp_paysan_id, :fp_ferme_id)', [
                    'fp_paysan_id' => $paysan['pay_id'],
                    'fp_ferme_id' => $paysan['ferme_id'],
                ]);
                $this->paysanFermeMap[$paysan['pay_id']] = $paysan['ferme_id'];
            }
        }
    }

    private function injectRegroupements(): void
    {
        $regroupements = $this
            ->connection
            ->executeQuery('SELECT fr.* FROM ak_ferme_regroupement fr')
            ->fetchAllAssociative()
        ;

        foreach ($regroupements as $regroupement) {
            $userId = $this->injectUserIfEmailNotExist(
                $regroupement['email'],
                $regroupement['ville_id'],
                null,
                $this->generateUsername($regroupement['nom'], null),
                $regroupement['password'],
                null,
                null,
                0,
                Uuid::uuid4()->toString(),
                0,
                null,
                $regroupement['nom'],
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            );
            $this->executeQueryDisplay('INSERT INTO ferme_regroupement_user (ferme_regroupement_id, user_id) VALUE (:ferme_regroupement_id, :user_id)', [
                'ferme_regroupement_id' => $regroupement['id'],
                'user_id' => $userId,
            ]);
        }
    }

    public function updateAdhesions(): void
    {
        foreach ($this->amapienUserMap as $amapienId => $userId) {
            $this->executeQueryDisplay('UPDATE adhesion_amap SET creator_id=:user_id WHERE creator_id=:amapien_id', [
                'user_id' => $userId,
                'amapien_id' => $amapienId,
            ]);
            $this->executeQueryDisplay('UPDATE adhesion_amapien SET creator_id=:user_id WHERE creator_id=:amapien_id', [
                'user_id' => $userId,
                'amapien_id' => $amapienId,
            ]);
            $this->executeQueryDisplay('UPDATE adhesion_amapien SET amapien_id=:user_id WHERE amapien_id=:amapien_id', [
                'user_id' => $userId,
                'amapien_id' => $amapienId,
            ]);
            $this->executeQueryDisplay('UPDATE adhesion_ferme SET creator_id=:user_id WHERE creator_id=:amapien_id', [
                'user_id' => $userId,
                'amapien_id' => $amapienId,
            ]);
        }
    }

    public function updatePaysanCommentaire(): void
    {
        $paysanCommentaires = $this
            ->executeQueryDisplay('SELECT * FROM ak_paysan_commentaire')
            ->fetchAllAssociative()
        ;
        foreach ($paysanCommentaires as $paysanCommentaire) {
            $this
                ->executeQueryDisplay('UPDATE ak_paysan_commentaire SET pay_com_fk_pay_id=:user_id WHERE pay_com_fk_pay_id=:paysan_id', [
                    'user_id' => $this->paysanFermeMap[$paysanCommentaire['pay_com_fk_pay_id']],
                    'paysan_id' => $paysanCommentaire['pay_com_fk_pay_id'],
                ])
            ;
        }
    }

    public function updateEvenementAuthor(): void
    {
        foreach ($this->amapienUserMap as $amapienId => $userId) {
            $this->executeQueryDisplay('UPDATE ak_evenement SET author_id=:user_id WHERE author_id=:amapien_id', [
                'user_id' => $userId,
                'amapien_id' => $amapienId,
            ]);
        }
    }

    // Force import password from amap or paysan if not defined for amapien
    public function updatePasswords(): void
    {
        $this->executeQueryDisplay(
            'UPDATE ak_user u
            LEFT JOIN user_email ue ON u.id = ue.user_id
            LEFT JOIN ak_amap a ON a.amap_email_public = ue.email COLLATE \'utf8_unicode_ci\'
            SET u.password = a.amap_password WHERE u.password IS NULL AND a.amap_password IS NOT NULL;'
        );
        $this->executeQueryDisplay(
            'UPDATE ak_user u
            LEFT JOIN user_email ue ON u.id = ue.user_id
            LEFT JOIN ak_paysan p ON p.pay_email = ue.email COLLATE \'utf8_unicode_ci\'
            SET u.password = p.pay_password WHERE u.password IS NULL AND p.pay_password IS NOT NULL;'
        );
        $this->executeQueryDisplay(
            'UPDATE ak_user u
            LEFT JOIN user_email ue ON u.id = ue.user_id
            LEFT JOIN ak_ferme_regroupement fr ON fr.email = ue.email COLLATE \'utf8_unicode_ci\'
            SET u.password = fr.email WHERE u.password IS NULL AND fr.email IS NOT NULL;'
        );
    }

    public function up(Schema $schema): void
    {
        $this->usernameGenerator = PhpDiContrainerSingleton::getContainer()->get(UsernameGenerator::class);

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->executeQueryDisplay('CREATE TABLE ferme_regroupement_user (ferme_regroupement_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_E6B7A1BC46CA65F3 (ferme_regroupement_id), INDEX IDX_E6B7A1BCA76ED395 (user_id), PRIMARY KEY(ferme_regroupement_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE paysan_ferme (fp_paysan_id INT NOT NULL, fp_ferme_id INT NOT NULL, INDEX IDX_97D3CB41F488CB39 (fp_paysan_id), INDEX IDX_97D3CB41F5FE28F3 (fp_ferme_id), PRIMARY KEY(fp_paysan_id, fp_ferme_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE ak_user (id INT AUTO_INCREMENT NOT NULL, ville_id INT DEFAULT NULL, password_reset_token_id INT DEFAULT NULL, username VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, num_tel1 VARCHAR(255) DEFAULT NULL, num_tel2 VARCHAR(255) DEFAULT NULL, newsletter TINYINT(1) NOT NULL, uuid VARCHAR(255) DEFAULT NULL, super_admin TINYINT(1) NOT NULL, name_first_name VARCHAR(255) DEFAULT NULL, name_last_name VARCHAR(255) DEFAULT NULL, address_adress VARCHAR(255) DEFAULT NULL, address_adress_compl VARCHAR(255) DEFAULT NULL, adhesion_info_nom_reseau VARCHAR(255) DEFAULT NULL, adhesion_info_siret VARCHAR(255) DEFAULT NULL, adhesion_info_rna VARCHAR(255) DEFAULT NULL, adhesion_info_ville_signature VARCHAR(255) DEFAULT NULL, adhesion_info_site VARCHAR(255) DEFAULT NULL, INDEX IDX_C40A36F9A73F0036 (ville_id), UNIQUE INDEX UNIQ_C40A36F9AA2E96A0 (password_reset_token_id), UNIQUE INDEX UNIQ_C40A36F9F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE user_region (user_id INT NOT NULL, region_id INT NOT NULL, INDEX IDX_6A30EA4BA76ED395 (user_id), INDEX IDX_6A30EA4B98260155 (region_id), PRIMARY KEY(user_id, region_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE user_departement (user_id INT NOT NULL, departement_id VARCHAR(3) NOT NULL, INDEX IDX_686D92F6A76ED395 (user_id), INDEX IDX_686D92F6CCF9E01E (departement_id), PRIMARY KEY(user_id, departement_id)) DEFAULT CHARACTER SET utf8 ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE amapien_invitation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, amap_id INT NOT NULL, token_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9649055EA76ED395 (user_id), INDEX IDX_9649055E52AA66E8 (amap_id), UNIQUE INDEX UNIQ_9649055E41DEE7B9 (token_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('CREATE TABLE user_email (v_id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_550872CA76ED395 (user_id), UNIQUE INDEX UNIQ_550872CE7927C74 (email), PRIMARY KEY(v_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8DA73F0036');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8DAA2E96A0');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien DROP FOREIGN KEY FK_2CDF7F8D52AA66E8');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien ADD user_id INT NOT NULL, ADD admin TINYINT(1) NOT NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan ADD user_id INT DEFAULT NULL');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amap DROP FOREIGN KEY FK_3623071461220EA6');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amap_amapien DROP FOREIGN KEY FK_D86B8344BC7A2B78');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amapien DROP FOREIGN KEY FK_FCCE162F61220EA6');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amapien DROP FOREIGN KEY FK_FCCE162FBC7A2B78');
        $this->executeQueryDisplay('ALTER TABLE adhesion_ferme DROP FOREIGN KEY FK_63AE08EA61220EA6');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme ADD f_annee_deb_commercialisation_amap VARCHAR(4) DEFAULT NULL, ADD f_date_installation DATE DEFAULT NULL, ADD f_msa VARCHAR(255) DEFAULT NULL, ADD f_spg VARCHAR(4) DEFAULT NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan_commentaire DROP FOREIGN KEY FK_321CA53E5641AFF6');
        $this->executeQueryDisplay('DROP INDEX IDX_321CA53E5641AFF6 ON ak_paysan_commentaire');
        $this->executeQueryDisplay('ALTER TABLE ak_evenement DROP FOREIGN KEY FK_1EE75601B9FACD98');
        $this->executeQueryDisplay('DROP INDEX IDX_1EE75601B9FACD98 ON ak_evenement');
        $this->executeQueryDisplay('ALTER TABLE ak_evenement CHANGE ev_fk_amapien_id author_id INT DEFAULT NULL');
        $this->executeQueryDisplay('CREATE TABLE user_amap (user_id INT NOT NULL, amap_id INT NOT NULL, INDEX IDX_B4B3701AA76ED395 (user_id), INDEX IDX_B4B3701A52AA66E8 (amap_id), PRIMARY KEY(user_id, amap_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->executeQueryDisplay('ALTER TABLE user_amap ADD CONSTRAINT FK_B4B3701AA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE user_amap ADD CONSTRAINT FK_B4B3701A52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');

        $this->injectAmapiens();
        $this->injectAmaps();

        // Fix user ville when user already exist
        $this->executeQueryDisplay('UPDATE ak_user
            JOIN user_email ue on ak_user.id = ue.user_id
            SET ville_id = (SELECT DISTINCT ll.ville_id FROM ak_amap_livraison_lieu ll JOIN ak_amap a ON ll.amap_liv_lieu_fk_amap_id = a.amap_id WHERE a.amap_email_public = ue.email COLLATE utf8_unicode_ci ORDER BY ll.amap_liv_lieu_id LIMIT 1)
            WHERE ville_id IS NULL');

        $this->injectPaysans();

        $this->executeQueryDisplay('
                UPDATE ak_ferme f
            JOIN ak_paysan ap on f.f_id = ap.ferme_id
        SET f.f_annee_deb_commercialisation_amap = ap.pay_annee_deb_commercialisation_amap
        ,f.f_date_installation = ap.pay_date_installation
        ,f.f_msa = ap.pay_msa
        ,f.f_spg = ap.pay_spg
        ;
    ');

        // Fix user ville when user already exist
        $this->executeQueryDisplay('UPDATE ak_user
    JOIN user_email ue on ak_user.id = ue.user_id
    SET ville_id = (SELECT DISTINCT pay.ville_id FROM ak_paysan pay WHERE pay.pay_email = ue.email COLLATE utf8_unicode_ci LIMIT 1)
    WHERE ville_id IS NULL');

        $this->injectRegroupements();
        $this->updateAdhesions();
        $this->updatePaysanCommentaire();
        $this->updateEvenementAuthor();
        $this->updatePasswords();

        $this->executeQueryDisplay('ALTER TABLE ferme_regroupement_user ADD CONSTRAINT FK_E6B7A1BC46CA65F3 FOREIGN KEY (ferme_regroupement_id) REFERENCES ak_ferme_regroupement (id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE ferme_regroupement_user ADD CONSTRAINT FK_E6B7A1BCA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE paysan_ferme ADD CONSTRAINT FK_97D3CB41F488CB39 FOREIGN KEY (fp_paysan_id) REFERENCES ak_paysan (pay_id)');
        $this->executeQueryDisplay('ALTER TABLE paysan_ferme ADD CONSTRAINT FK_97D3CB41F5FE28F3 FOREIGN KEY (fp_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_user ADD CONSTRAINT FK_C40A36F9A73F0036 FOREIGN KEY (ville_id) REFERENCES ak_ville (v_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_user ADD CONSTRAINT FK_C40A36F9AA2E96A0 FOREIGN KEY (password_reset_token_id) REFERENCES token (id)');
        $this->executeQueryDisplay('ALTER TABLE user_region ADD CONSTRAINT FK_6A30EA4BA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE user_region ADD CONSTRAINT FK_6A30EA4B98260155 FOREIGN KEY (region_id) REFERENCES ak_region (reg_id)');
        $this->executeQueryDisplay('ALTER TABLE user_departement ADD CONSTRAINT FK_686D92F6A76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE user_departement ADD CONSTRAINT FK_686D92F6CCF9E01E FOREIGN KEY (departement_id) REFERENCES ak_departement (dep_id)');
        $this->executeQueryDisplay('ALTER TABLE amapien_invitation ADD CONSTRAINT FK_9649055EA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE amapien_invitation ADD CONSTRAINT FK_9649055E52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id)');
        $this->executeQueryDisplay('ALTER TABLE amapien_invitation ADD CONSTRAINT FK_9649055E41DEE7B9 FOREIGN KEY (token_id) REFERENCES token (id)');
        $this->executeQueryDisplay('ALTER TABLE user_email ADD CONSTRAINT FK_550872CA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('DROP TABLE ak_departement_administrateur');
        $this->executeQueryDisplay('DROP TABLE ak_paysan_cadre_reseau');
        $this->executeQueryDisplay('DROP TABLE ak_region_administrateur');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amap ADD CONSTRAINT FK_3623071461220EA6 FOREIGN KEY (creator_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amap_amapien ADD CONSTRAINT FK_D86B8344BC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_amapien (a_id)');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162F61220EA6 FOREIGN KEY (creator_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE adhesion_amapien ADD CONSTRAINT FK_FCCE162FBC7A2B78 FOREIGN KEY (amapien_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE adhesion_ferme ADD CONSTRAINT FK_63AE08EA61220EA6 FOREIGN KEY (creator_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC633BC43DD2');
        $this->executeQueryDisplay('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63A4B3F446');
        $this->executeQueryDisplay('ALTER TABLE ak_amap DROP FOREIGN KEY FK_87ABDC63AA2E96A0');
        $this->executeQueryDisplay('DROP INDEX UNIQ_87ABDC63AA2E96A0 ON ak_amap');
        $this->executeQueryDisplay('DROP INDEX UNIQ_87ABDC63A4B3F446 ON ak_amap');
        $this->executeQueryDisplay('DROP INDEX UNIQ_87ABDC633BC43DD2 ON ak_amap');
        $this->executeQueryDisplay('DROP TABLE ak_reseau_administrateur');
        $this->executeQueryDisplay('ALTER TABLE ak_amap DROP amap_email_invalid, DROP password_reset_token_id');
        $this->executeQueryDisplay('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC63A4B3F446 FOREIGN KEY (amap_fk_contact_referent_reseau_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_amap ADD CONSTRAINT FK_87ABDC633BC43DD2 FOREIGN KEY (amap_fk_contact_referent_reseau_secondaire_id) REFERENCES ak_amapien (a_id) ON DELETE SET NULL');
        $this->executeQueryDisplay('CREATE INDEX IDX_87ABDC63A4B3F446 ON ak_amap (amap_fk_contact_referent_reseau_id)');
        $this->executeQueryDisplay('CREATE INDEX IDX_87ABDC633BC43DD2 ON ak_amap (amap_fk_contact_referent_reseau_secondaire_id)');
        $this->executeQueryDisplay('DROP INDEX a_email ON ak_amapien');
        $this->executeQueryDisplay('DROP INDEX IDX_2CDF7F8DA73F0036 ON ak_amapien');
        $this->executeQueryDisplay('DROP INDEX a_mdpreinit_token ON ak_amapien');
        $this->executeQueryDisplay('DROP INDEX UNIQ_2CDF7F8DAA2E96A0 ON ak_amapien');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien DROP a_prenom, DROP a_nom, DROP a_email, DROP a_password, DROP a_num_tel_1, DROP a_num_tel_2, DROP a_lib_adr_1, DROP a_lib_adr_2, DROP a_mdpreinit_token, DROP a_mdpreinit_creationdate, DROP a_uuid, DROP a_email_invalid, DROP password_reset_token_id, DROP super_admin, DROP newsletter, DROP ville_id, DROP a_lib_adr_1_complement, DROP adhesion_info_site, DROP adhesion_info_nom_reseau, DROP adhesion_info_siret, DROP adhesion_info_rna, DROP adhesion_info_ville_signature');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8DA76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien ADD CONSTRAINT FK_2CDF7F8D52AA66E8 FOREIGN KEY (amap_id) REFERENCES ak_amap (amap_id) ON DELETE CASCADE');
        $this->executeQueryDisplay('CREATE INDEX IDX_2CDF7F8DA76ED395 ON ak_amapien (user_id)');
        $this->executeQueryDisplay('CREATE UNIQUE INDEX UNIQ_2CDF7F8DA76ED39552AA66E8 ON ak_amapien (user_id, amap_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_amapien_liste_attente DROP FOREIGN KEY FK_29F9892359474DF');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_amapien_liste_attente DROP FOREIGN KEY FK_29F98926A721E02');
        $this->executeQueryDisplay('DROP INDEX IDX_29F98926A721E02 ON ak_ferme_amapien_liste_attente');
        $this->executeQueryDisplay('DROP INDEX IDX_29F9892359474DF ON ak_ferme_amapien_liste_attente');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_amapien_liste_attente ADD CONSTRAINT FK_29F98926A721E02 FOREIGN KEY (f_a_la_fk_amapien_id) REFERENCES ak_amapien (a_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_amapien_liste_attente ADD CONSTRAINT FK_29F9892359474DF FOREIGN KEY (f_a_la_fk_ferme_id) REFERENCES ak_ferme (f_id)');
        $this->executeQueryDisplay('CREATE INDEX IDX_29F98926A721E02 ON ak_ferme_amapien_liste_attente (f_a_la_fk_amapien_id)');
        $this->executeQueryDisplay('CREATE INDEX IDX_29F9892359474DF ON ak_ferme_amapien_liste_attente (f_a_la_fk_ferme_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_amapien_annee_adhesion CHANGE a_aa_fk_amapien_id a_aa_fk_amapien_id INT NOT NULL');
        $this->executeQueryDisplay('ALTER TABLE ferme_amapien DROP FOREIGN KEY FK_AA769FCC19E9F30A');
        $this->executeQueryDisplay('ALTER TABLE ferme_amapien DROP FOREIGN KEY FK_AA769FCC48A4116C');
        $this->executeQueryDisplay('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC19E9F30A FOREIGN KEY (amapien_a_id) REFERENCES ak_amapien (a_id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE ferme_amapien ADD CONSTRAINT FK_AA769FCC48A4116C FOREIGN KEY (ferme_f_id) REFERENCES ak_ferme (f_id) ON DELETE CASCADE');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_regroupement DROP FOREIGN KEY FK_D16BD542AA2E96A0');
        $this->executeQueryDisplay('DROP INDEX UNIQ_D16BD542AA2E96A0 ON ak_ferme_regroupement');
        $this->executeQueryDisplay('ALTER TABLE ak_ferme_regroupement DROP password_reset_token_id, DROP email, DROP password, DROP email_invalid');
        $this->executeQueryDisplay('ALTER TABLE ak_modele_contrat_produit CHANGE prix_prix_ttc prix_prix_ttc BIGINT NOT NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D86018981132');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D860A73F0036');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan DROP FOREIGN KEY FK_CC11D860AA2E96A0');
        $this->executeQueryDisplay('DROP INDEX IDX_CC11D86018981132 ON ak_paysan');
        $this->executeQueryDisplay('DROP INDEX pay_mdpreinit_token ON ak_paysan');
        $this->executeQueryDisplay('DROP INDEX UNIQ_CC11D860AA2E96A0 ON ak_paysan');
        $this->executeQueryDisplay('DROP INDEX IDX_CC11D860A73F0036 ON ak_paysan');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan DROP password_reset_token_id, DROP ferme_id, DROP ville_id, DROP pay_prenom, DROP pay_nom, DROP pay_conjoint_prenom, DROP pay_conjoint_nom, DROP pay_email, DROP pay_password, DROP pay_num_tel_1, DROP pay_num_tel_2, DROP pay_lib_adr, DROP pay_annee_deb_commercialisation_amap, DROP pay_date_installation, DROP pay_msa, DROP pay_statut, DROP pay_spg, DROP pay_mdpreinit_token, DROP pay_mdpreinit_creationdate, DROP pay_uuid, DROP pay_email_invalid, DROP newsletter');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan ADD CONSTRAINT FK_CC11D860A76ED395 FOREIGN KEY (user_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('CREATE UNIQUE INDEX UNIQ_CC11D860A76ED395 ON ak_paysan (user_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan_commentaire CHANGE pay_com_fk_pay_id ferme_id INT NOT NULL');
        $this->executeQueryDisplay('ALTER TABLE ak_paysan_commentaire ADD CONSTRAINT FK_321CA53E18981132 FOREIGN KEY (ferme_id) REFERENCES ak_ferme (f_id)');
        $this->executeQueryDisplay('CREATE INDEX IDX_321CA53E18981132 ON ak_paysan_commentaire (ferme_id)');
        $this->executeQueryDisplay('ALTER TABLE ak_evenement ADD CONSTRAINT FK_1EE75601F675F31B FOREIGN KEY (author_id) REFERENCES ak_user (id)');
        $this->executeQueryDisplay('CREATE INDEX IDX_1EE75601F675F31B ON ak_evenement (author_id)');

        $this->executeQueryDisplay('DELETE aa
            FROM ak_amapien_annee_adhesion aa
                     LEFT JOIN ak_amapien a on aa.a_aa_fk_amapien_id = a.a_id
            WHERE a.amap_id IS NULL;');
        $this->executeQueryDisplay('DELETE FROM ak_amapien WHERE amap_id IS NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
