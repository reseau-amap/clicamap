<?php

declare(strict_types=1);

namespace PsrLib\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220111155726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit DROP FOREIGN KEY FK_A15C0C3833DEB7AF');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD CONSTRAINT FK_A15C0C3833DEB7AF FOREIGN KEY (mc_pro_fk_ferme_produit_id) REFERENCES ak_ferme_produit (f_pro_id) ON DELETE SET NULL');
        $this->addSql('DROP INDEX word ON captcha');
        $this->addSql('ALTER TABLE captcha CHANGE captcha_time captcha_time INT UNSIGNED DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adhesion_value CHANGE amount amount BIGINT NOT NULL, CHANGE amount_membership amount_membership BIGINT DEFAULT NULL, CHANGE amount_donation amount_donation BIGINT DEFAULT NULL, CHANGE amount_insurance amount_insurance BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ak_amap_distribution CHANGE detail_heure_debut detail_heure_debut VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE detail_heure_fin detail_heure_fin VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE ak_contrat_dates_reglement CHANGE montant montant BIGINT NOT NULL');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit DROP FOREIGN KEY FK_A15C0C3833DEB7AF');
        $this->addSql('ALTER TABLE ak_modele_contrat_produit ADD CONSTRAINT FK_A15C0C3833DEB7AF FOREIGN KEY (mc_pro_fk_ferme_produit_id) REFERENCES ak_ferme_produit (f_pro_id)');
        $this->addSql('ALTER TABLE captcha CHANGE captcha_time captcha_time INT UNSIGNED NOT NULL');
        $this->addSql('CREATE INDEX word ON captcha (word)');
    }
}
