<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Validator;

use PsrLib\DTO\ContratCommandeDeplacement;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ContratCommandeDeplacementValidateDeplacementValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (null === $value) {
            return;
        }

        if (!$value instanceof ContratCommandeDeplacement) {
            throw new UnexpectedTypeException($value, ContratCommandeDeplacement::class);
        }
        if (false !== $value->getMc()->getInformationLivraison()->getAmapienPermissionDeplacementLivraison()) {
            return;
        }

        $contrat = $value->getContrat();
        foreach ($value->getItems() as $item) {
            $cellule = $contrat->getCelluleDateProduit($item->getMcp(), $item->getMcd());
            if (null !== $cellule && $item->getQty() > $cellule->getQuantite()
                && 0.0 === $cellule->getQuantite()) {
                $this
                    ->context
                    ->buildViolation('Impossible de déplacer une livraison sur une date sans livraison.')
                    ->addViolation()
                ;

                return;
            }
        }
    }
}
