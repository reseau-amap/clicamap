<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Validator;

use PsrLib\ORM\Entity\AmapAbsence;
use PsrLib\ORM\Entity\Document;
use PsrLib\ORM\Repository\DocumentRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DocumentAnonymeMaxValidator extends ConstraintValidator
{
    public function __construct(private readonly DocumentRepository $documentRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (null === $value) {
            return;
        }

        if (!$value instanceof Document) {
            throw new UnexpectedTypeException($value, AmapAbsence::class);
        }

        $nbAnonymes = $this
            ->documentRepository
            ->countDocumentAnonyme($value->getId())
        ;

        if ($nbAnonymes > 2) {
            $this->context->buildViolation('Trois documents maximum sont autorisés en page d\'accueil')
                ->addViolation()
            ;
        }
    }
}
