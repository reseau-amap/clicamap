<?php

declare(strict_types=1);

namespace PsrLib\Validator;

use Symfony\Component\Validator\Constraints\AbstractComparison;

// TODO: test
#[\Attribute]
class MoneyGreaterThanOrEqual extends AbstractComparison
{
    final public const TOO_LOW_ERROR = 'a1de8258-9b42-46ff-930f-7a7cb8320402';

    /**
     * @var string
     */
    public $message = 'Cette valeur doit être supérieure ou égale à {{ compared_value }}.';

    /**
     * @var string[]
     */
    protected static $errorNames = [
        self::TOO_LOW_ERROR => 'TOO_LOW_ERROR',
    ];
}
