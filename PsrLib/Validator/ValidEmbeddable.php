<?php

declare(strict_types=1);

namespace PsrLib\Validator;

use Symfony\Component\Validator\Constraints\Valid;

/**
 * @ref https://github.com/symfony/symfony/issues/40741
 */
#[\Attribute]
class ValidEmbeddable extends Valid
{
    public array $embeddedGroups = ['Default'];

    /**
     * {@inheritDoc}
     */
    public function getTargets(): array
    {
        return [
            self::PROPERTY_CONSTRAINT,
        ];
    }
}
