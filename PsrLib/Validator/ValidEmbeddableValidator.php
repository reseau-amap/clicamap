<?php

declare(strict_types=1);

namespace PsrLib\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidEmbeddableValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidEmbeddable) {
            throw new UnexpectedTypeException($constraint, ValidEmbeddable::class);
        }

        if (null === $value) {
            return;
        }

        $this->context
            ->getValidator()
            ->inContext($this->context)
            // This is the important line here – more or less the only one that differs from the original `ValidValidator`.
            ->validate($value, null, $constraint->embeddedGroups)
        ;
    }
}
