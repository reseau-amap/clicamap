<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Validator;

use PsrLib\DTO\ContratCommandeSouscription;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ContratCommandeSouscriptionValidateNbLivraisonsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (null === $value) {
            return;
        }

        if (!$value instanceof ContratCommandeSouscription) {
            throw new UnexpectedTypeException($value, ContratCommandeSouscription::class);
        }

        $mcNblivBypass = $value->getMc()->getInformationLivraison()->getNblivPlancherDepassement();
        $nbLivAmapienMin = $value->getMc()->getInformationLivraison()->getNblivPlancher();

        if (true === $mcNblivBypass) {
            if ($value->countNbLivraisons() < $nbLivAmapienMin) {
                $this
                    ->context
                    ->buildViolation('Vous devez sélectionner au moins <b>%nb%</b> livraisons')
                    ->setParameter('%nb%', (string) $nbLivAmapienMin)
                    ->addViolation()
                ;
            }

            return;
        }

        if ($value->countNbLivraisons() !== $nbLivAmapienMin) {
            $this
                ->context
                ->buildViolation('Vous devez sélectionner exactement <b>%nb%</b> livraisons')
                ->setParameter('%nb%', (string) $nbLivAmapienMin)
                ->addViolation()
            ;
        }
    }
}
