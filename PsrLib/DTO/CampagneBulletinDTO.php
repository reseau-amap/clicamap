<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;

class CampagneBulletinDTO
{
    private User $user;

    private CampagneBulletin $bulletin;

    public function __construct(User $user, CampagneBulletin $bulletin)
    {
        $this->user = $user;
        $this->bulletin = $bulletin;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getBulletin(): CampagneBulletin
    {
        return $this->bulletin;
    }
}
