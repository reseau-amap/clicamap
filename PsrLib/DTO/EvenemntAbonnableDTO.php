<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\EvenementRelatedEntity;

class EvenemntAbonnableDTO
{
    private string $id;

    private string $nom;

    private EvenementType $type;

    private EvenementRelatedEntity $relatedEntity;

    public function __construct(string $id, string $nom, EvenementType $type, EvenementRelatedEntity $relatedEntity)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->type = $type;
        $this->relatedEntity = $relatedEntity;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getType(): EvenementType
    {
        return $this->type;
    }

    public function getRelatedEntity(): EvenementRelatedEntity
    {
        return $this->relatedEntity;
    }
}
