<?php

declare(strict_types=1);

namespace PsrLib\DTO\Api\Mailjet;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Properties
{
    final public const TYPE_AMAP = 'AMAP';
    final public const TYPE_AMAPIEN = 'amapien';
    final public const TYPE_PAYSAN = 'paysan';

    #[Groups('public')]
    private ?string $prenom = null;

    #[Groups('public')]
    private ?string $nom = null;

    #[Groups('public')]
    private ?string $region = null;

    #[Groups('public')]
    private ?string $departement = null;

    #[SerializedName('amapien_amap')]
    #[Groups('public')]
    private ?string $amapienAmap = null;

    #[Groups('public')]
    private ?string $adhesion = null;

    #[SerializedName('amap_etudiante')]
    #[Groups('public')]
    private ?string $amapEtudiante = null;

    #[SerializedName('amap_assurance')]
    #[Groups('public')]
    private ?string $amapAssurance = null;

    #[SerializedName('tpp_propose')]
    #[Groups('public')]
    private ?string $tppPropose = null;

    #[SerializedName('amap_tpp_recherche')]
    #[Groups('public')]
    private ?string $amapTppRecherche = null;

    #[SerializedName('paysan_certification')]
    #[Groups('public')]
    private ?string $paysanCertification = null;

    #[SerializedName('paysan_date_installation')]
    #[Groups('public')]
    private ?string $paysanDateInstallation = null;

    #[SerializedName('paysan_adhesion')]
    #[Groups('public')]
    private ?string $paysanAdhesion = null;

    #[SerializedName('amapien_role')]
    #[Groups('public')]
    private ?string $amapienRole = null;

    /**
     * Properties constructor.
     */
    public function __construct(
        #[Groups('public')]
        private readonly string $type
    ) {
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): void
    {
        $this->nom = $nom;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): void
    {
        $this->region = $region;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): void
    {
        $this->departement = $departement;
    }

    public function getAmapienAmap(): ?string
    {
        return $this->amapienAmap;
    }

    public function setAmapienAmap(?string $amapienAmap): void
    {
        $this->amapienAmap = $amapienAmap;
    }

    public function getAdhesion(): ?string
    {
        return $this->adhesion;
    }

    public function setAdhesion(?string $adhesion): void
    {
        $this->adhesion = $adhesion;
    }

    public function getAmapEtudiante(): ?string
    {
        return $this->amapEtudiante;
    }

    public function setAmapEtudiante(?string $amapEtudiante): void
    {
        $this->amapEtudiante = $amapEtudiante;
    }

    public function getAmapAssurance(): ?string
    {
        return $this->amapAssurance;
    }

    public function setAmapAssurance(?string $amapAssurance): void
    {
        $this->amapAssurance = $amapAssurance;
    }

    public function getTppPropose(): ?string
    {
        return $this->tppPropose;
    }

    public function setTppPropose(?string $tppPropose): void
    {
        $this->tppPropose = $tppPropose;
    }

    public function getAmapTppRecherche(): ?string
    {
        return $this->amapTppRecherche;
    }

    public function setAmapTppRecherche(?string $amapTppRecherche): void
    {
        $this->amapTppRecherche = $amapTppRecherche;
    }

    public function getPaysanCertification(): ?string
    {
        return $this->paysanCertification;
    }

    public function setPaysanCertification(?string $paysanCertification): void
    {
        $this->paysanCertification = $paysanCertification;
    }

    public function getPaysanDateInstallation(): ?string
    {
        return $this->paysanDateInstallation;
    }

    public function setPaysanDateInstallation(?string $paysanDateInstallation): void
    {
        $this->paysanDateInstallation = $paysanDateInstallation;
    }

    public function getPaysanAdhesion(): ?string
    {
        return $this->paysanAdhesion;
    }

    public function setPaysanAdhesion(?string $paysanAdhesion): void
    {
        $this->paysanAdhesion = $paysanAdhesion;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAmapienRole(): ?string
    {
        return $this->amapienRole;
    }

    public function setAmapienRole(?string $amapienRole): void
    {
        $this->amapienRole = $amapienRole;
    }
}
