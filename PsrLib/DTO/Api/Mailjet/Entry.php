<?php

declare(strict_types=1);

namespace PsrLib\DTO\Api\Mailjet;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Entry
{
    #[SerializedName('Email')]
    private readonly string $email;

    #[SerializedName('Name')]
    private readonly string $name;

    #[Groups('public')]
    private readonly string $uuid;

    #[SerializedName('Properties')]
    #[Groups('public')]
    private readonly Properties $properties;

    public function __construct(
        string $email,
        string $name,
        string $uuid,
        Properties $properties
    ) {
        $this->email = $email;
        $this->name = $name;
        $this->uuid = $uuid;
        $this->properties = $properties;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getProperties(): Properties
    {
        return $this->properties;
    }
}
