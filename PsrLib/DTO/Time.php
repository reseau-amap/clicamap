<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class Time implements \Stringable
{
    #[Assert\Range(min: 0, max: 23, minMessage: "L'heure doit etre comprise entre 0 et 23", maxMessage: "L'heure doit etre comprise entre 0 et 23")]
    private int $hour;

    #[Assert\Range(min: 0, max: 59, minMessage: 'Les minutes doivent être comprises entre 0 et 59', maxMessage: 'Les minutes doivent être comprises entre 0 et 59')]
    private int $minute;

    public function __construct(int $hour, int $minute)
    {
        $this->hour = $hour;
        $this->minute = $minute;
    }

    public function __toString(): string
    {
        return sprintf(
            '%02s:%02s',
            $this->getHour(),
            $this->getMinute()
        );
    }

    public function getMinutesTotal(): int
    {
        return $this->hour * 60 + $this->minute;
    }

    public function getHour(): int
    {
        return $this->hour;
    }

    public function setHour(int $hour): Time
    {
        $this->hour = $hour;

        return $this;
    }

    public function getMinute(): int
    {
        return $this->minute;
    }

    public function setMinute(int $minute): Time
    {
        $this->minute = $minute;

        return $this;
    }
}
