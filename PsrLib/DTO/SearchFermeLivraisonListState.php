<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Embeddable\Period;
use PsrLib\ORM\Entity\Ferme;
use Symfony\Component\Validator\Constraints as Assert;

class SearchFermeLivraisonListState
{
    #[Assert\NotNull]
    #[Assert\Valid]
    private ?\PsrLib\ORM\Entity\Embeddable\Period $period = null;

    public function __construct(private readonly Ferme $ferme)
    {
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): SearchFermeLivraisonListState
    {
        $this->period = $period;

        return $this;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }
}
