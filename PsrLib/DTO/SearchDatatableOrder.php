<?php

declare(strict_types=1);

namespace PsrLib\DTO;

class SearchDatatableOrder
{
    private int $column;
    private \PsrLib\Enum\SearchDatatableOrder $dir;

    public function getColumn(): int
    {
        return $this->column;
    }

    public function setColumn(string $column): SearchDatatableOrder
    {
        $this->column = (int) $column;

        return $this;
    }

    public function getDir(): \PsrLib\Enum\SearchDatatableOrder
    {
        return $this->dir;
    }

    public function setDir(string $dir): SearchDatatableOrder
    {
        $this->dir = \PsrLib\Enum\SearchDatatableOrder::from($dir);

        return $this;
    }
}
