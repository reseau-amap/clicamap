<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class DocumentUtilisateurFormState
{
    #[Assert\NotNull]
    #[Assert\GreaterThan(0)]
    private ?int $annee = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 1, max: 100)]
    private ?string $nom = null;

    #[Assert\NotNull(message: 'Vous devez sélectionner un fichier.')]
    private ?UploadedFile $file = null;

    #[Assert\NotNull(message: 'Vous devez sélectionner une AMAP ou une ferme.')]
    private Amap|Ferme|null $target = null;

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(?int $annee): DocumentUtilisateurFormState
    {
        $this->annee = $annee;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): DocumentUtilisateurFormState
    {
        $this->nom = $nom;

        return $this;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    public function setFile(?UploadedFile $file): DocumentUtilisateurFormState
    {
        $this->file = $file;

        return $this;
    }

    public function getTarget(): Amap|Ferme|null
    {
        return $this->target;
    }

    public function setTarget(Amap|Ferme|null $target): DocumentUtilisateurFormState
    {
        $this->target = $target;

        return $this;
    }
}
