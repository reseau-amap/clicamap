<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Reseau;
use PsrLib\ORM\Entity\TypeProduction;

class SearchPaysanState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    private ?\PsrLib\ORM\Entity\Reseau $reseau = null;

    private ?string $adhesion = null;

    private ?\PsrLib\ORM\Entity\TypeProduction $typeProduction = null;

    private ?string $commercialisation = null;

    private ?string $keyword = null;

    private ?bool $active = null;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getAdhesion()
            && null === $this->getTypeProduction()
            && null === $this->getCommercialisation()
            && null === $this->getKeyword()
            && false === $this->getActive()
        ;
    }

    public function getReseau(): ?Reseau
    {
        return $this->reseau;
    }

    public function setReseau(?Reseau $reseau): SearchPaysanState
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getAdhesion(): ?string
    {
        return $this->adhesion;
    }

    public function setAdhesion(?string $adhesion): SearchPaysanState
    {
        $this->adhesion = $adhesion;

        return $this;
    }

    public function getTypeProduction(): ?TypeProduction
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?TypeProduction $typeProduction): SearchPaysanState
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }

    public function getCommercialisation(): ?string
    {
        return $this->commercialisation;
    }

    public function setCommercialisation(?string $commercialisation): SearchPaysanState
    {
        $this->commercialisation = $commercialisation;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchPaysanState
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): SearchPaysanState
    {
        $this->active = $active;

        return $this;
    }
}
