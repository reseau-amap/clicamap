<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\Validator\ContratCommandeSouscriptionValidateNbLivraisons;
use PsrLib\Validator\ContratCommandeValidateLivSame;

#[ContratCommandeSouscriptionValidateNbLivraisons(groups: ['subscription'])]
#[ContratCommandeValidateLivSame(groups: ['subscription'])]
class ContratCommandeSouscription extends ContratCommandeBase
{
    public function getItems(): array
    {
        return $this->items;
    }

    public function getSubscribableMcp(): array
    {
        return $this->mc->getProduits()->toArray();
    }
}
