<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;

class ControllerModeleContratCopyArgumentDTO
{
    private Amap $amap;
    private Ferme $ferme;

    public function __construct(Amap $amap, Ferme $ferme)
    {
        $this->amap = $amap;
        $this->ferme = $ferme;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }
}
