<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

class SearchAdhesionAmapState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    private ?int $adhesion = null;

    private ?bool $etudiante = null;

    private ?string $keyword = null;

    private ?bool $amapSupprimee = null;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getAdhesion()
            && null === $this->getEtudiante()
            && null === $this->getKeyword()
            && null === $this->getAmapSupprimee()
        ;
    }

    public function getAdhesion(): ?int
    {
        return $this->adhesion;
    }

    public function setAdhesion(?int $adhesion): void
    {
        $this->adhesion = $adhesion;
    }

    public function getEtudiante(): ?bool
    {
        return $this->etudiante;
    }

    public function setEtudiante(?bool $etudiante): void
    {
        $this->etudiante = $etudiante;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): void
    {
        $this->keyword = $keyword;
    }

    public function getAmapSupprimee(): ?bool
    {
        return $this->amapSupprimee;
    }

    public function setAmapSupprimee(?bool $amapSupprimee): void
    {
        $this->amapSupprimee = $amapSupprimee;
    }
}
