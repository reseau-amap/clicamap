<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class UserLogin
{
    #[Assert\NotNull(message: 'Utilisateur non trouvé')]
    private ?User $user = null;

    #[Assert\NotBlank]
    private ?string $plainPassword = null;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }
}
