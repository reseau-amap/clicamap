<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Assert\Assertion;
use PsrLib\ORM\Entity\User;

class SearchDistributionAmapienState extends SearchDistributionBaseState
{
    public const FILTER_AVAILABLE = 'available';
    public const FILTER_MINE = 'mine';
    public const FILTER_ALL = 'all';
    public const FILTERS = [self::FILTER_AVAILABLE, self::FILTER_MINE, self::FILTER_ALL];

    /**
     * @var string
     */
    private $filter = self::FILTER_AVAILABLE;

    public function __construct(private readonly User $user)
    {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getFilter(): string
    {
        return $this->filter;
    }

    public function setFilter(string $filter): self
    {
        Assertion::inArray($filter, self::FILTERS);
        $this->filter = $filter;

        return $this;
    }
}
