<?php

declare(strict_types=1);

namespace PsrLib\DTO;

class CityOSMDto implements \Stringable
{
    /**
     * @param string[] $postalCode
     */
    public function __construct(private readonly string $name, private readonly string $osmId, private readonly array $postalCode, private readonly string $depId)
    {
    }

    /**
     * used for deduplication.
     */
    public function __toString(): string
    {
        return hash(
            'sha256',
            sprintf(
                '%s%s',
                implode(',', $this->getPostalCode()),
                $this->getName()
            )
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOsmId(): string
    {
        return $this->osmId;
    }

    /**
     * @return string[]
     */
    public function getPostalCode(): array
    {
        return $this->postalCode;
    }

    public function getDepId(): string
    {
        return $this->depId;
    }
}
