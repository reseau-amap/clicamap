<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Files\File;

class ZipCreatorFileDTO
{
    private File $file;
    private string $fileName;

    public function __construct(File $file, string $fileName)
    {
        $this->file = $file;
        $this->fileName = $fileName;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }
}
