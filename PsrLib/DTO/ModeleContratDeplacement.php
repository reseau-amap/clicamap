<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ModeleContratDeplacement
{
    private \PsrLib\ORM\Entity\ModeleContratDate $src;

    #[Assert\NotNull(message: "Merci d'indiquer une date de destination.")]
    private ?\DateTime $dst = null;

    private readonly \PsrLib\ORM\Entity\ModeleContrat $mc;

    public function __construct(ModeleContrat $mc)
    {
        Assertion::minCount($mc->getDates(), 1);
        $this->mc = $mc;
        $this->src = $mc->getDates()->first();
    }

    #[Assert\Callback]
    public function validateDstNotInMcDates(ExecutionContextInterface $context): void
    {
        $dst = $this->getDst();
        if (null === $dst) {
            return;
        }

        $dstCarbon = Carbon::instance($dst);
        foreach ($this->getMc()->getDates() as $date) {
            if ($dstCarbon->isSameAs('Y-m-d', $date->getDateLivraison())) {
                $context
                    ->buildViolation('La date sélectionnée est déjà une date de livraison.')
                    ->atPath('dst')
                    ->addViolation()
                ;
            }
        }
    }

    /**
     * @return ModeleContratDate
     */
    public function getSrc()
    {
        return $this->src;
    }

    public function setSrc(ModeleContratDate $src): void
    {
        $this->src = $src;
    }

    public function getMc(): ModeleContrat
    {
        return $this->mc;
    }

    public function getDst(): ?\DateTime
    {
        return $this->dst;
    }

    public function setDst(?\DateTime $dst): void
    {
        $this->dst = $dst;
    }
}
