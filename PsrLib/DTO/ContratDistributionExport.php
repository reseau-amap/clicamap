<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;

class ContratDistributionExport
{
    private ModeleContrat $mc;

    public function __construct(
        ModeleContrat $mc
    ) {
        $this->mc = $mc;
    }

    public function getMc(): ModeleContrat
    {
        return $this->mc;
    }

    /**
     * @return Contrat[]
     */
    public function getContratsValidatedOrderedByAmapien()
    {
        $contrats = $this->mc->getContratsValidated()->toArray();
        usort($contrats, fn (Contrat $c1, Contrat $c2) => strcasecmp((string) $c1->getAmapien()->getUser()->getName()->getLastName(), (string) $c2->getAmapien()->getUser()->getName()->getLastName()));

        return $contrats;
    }

    public function isContratAllFutureDateLivSame(): bool
    {
        foreach ($this->getContratsValidatedOrderedByAmapien() as $contrat) {
            $ref = null;

            foreach ($contrat->getModeleContrat()->getFutureDates() as $date) {
                if (null === $ref) {
                    $ref = $this->parseContratDate($contrat, $date);
                }

                $dateParsed = $this->parseContratDate($contrat, $date);
                if ($ref !== $dateParsed) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return array<int, float|null>
     */
    private function parseContratDate(Contrat $contrat, ModeleContratDate $date): array
    {
        $res = [];
        foreach ($this->mc->getProduits() as $produit) {
            $cellule = $contrat->getCelluleDateProduit($produit, $date);
            if (null !== $cellule) {
                $res[$produit->getId()] = $cellule->getQuantite();
            }
        }

        return $res;
    }
}
