<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

class SearchAdhesionFermeState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    private ?int $adhesion = null;
    private ?string $keyword = null;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && null === $this->getAdhesion()
            && null === $this->getKeyword()
        ;
    }

    public function getAdhesion(): ?int
    {
        return $this->adhesion;
    }

    public function setAdhesion(?int $adhesion): SearchAdhesionFermeState
    {
        $this->adhesion = $adhesion;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchAdhesionFermeState
    {
        $this->keyword = $keyword;

        return $this;
    }
}
