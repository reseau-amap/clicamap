<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Campagne;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CampagneBulletinImportLineDTO
{
    #[Assert\NotBlank(message: 'Amapien introuvable')]
    private ?Amapien $amapien;

    #[Assert\NotBlank(message: 'Le nom du payeur est obligatoire')]
    private ?string $payeur;

    #[Assert\NotBlank(message: 'La date de paiement est obligatoire')]
    private ?Carbon $paiementDate;

    #[Assert\NotBlank(message: 'La permission d\'image est obligatoire')]
    #[Assert\Choice(choices: ['O', 'N'])]
    private ?string $permissionImage;

    private array $montantLibres = [];

    private Campagne $campagne;

    public function __construct(Campagne $campagne)
    {
        $this->campagne = $campagne;
    }

    #[Assert\Callback]
    public function validateNbMontantLibres(ExecutionContextInterface $context): void
    {
        if (count($this->montantLibres) !== $this->getCampagne()->countMontantLibres()) {
            $context->buildViolation('Le nombre de montants libres ne correspond pas au nombre de montants libres attendus')
                ->atPath('montantLibres')
                ->addViolation()
            ;
        }
    }

    #[Assert\Callback]
    public function validateMontantLibresNull(ExecutionContextInterface $context): void
    {
        $i = 1;
        foreach ($this->montantLibres as $montantLibre) {
            if (null === $montantLibre || Money::EUR(0)->equals($montantLibre)) {
                $context->buildViolation(sprintf('Le montant libre %s n\'est pas valide', $i))
                    ->atPath('montantLibres')
                    ->addViolation()
                ;
            }
            ++$i;
        }
    }

    public function getPayeur(): ?string
    {
        return $this->payeur;
    }

    public function setPayeur(?string $payeur): CampagneBulletinImportLineDTO
    {
        $this->payeur = $payeur;

        return $this;
    }

    public function getPaiementDate(): ?Carbon
    {
        return $this->paiementDate;
    }

    public function setPaiementDate(?Carbon $paiementDate): CampagneBulletinImportLineDTO
    {
        $this->paiementDate = $paiementDate;

        return $this;
    }

    public function getPermissionImage(): ?string
    {
        return $this->permissionImage;
    }

    public function setPermissionImage(?string $permissionImage): CampagneBulletinImportLineDTO
    {
        $this->permissionImage = $permissionImage;

        return $this;
    }

    public function getMontantLibres(): array
    {
        return $this->montantLibres;
    }

    public function setMontantLibres(array $montantLibres): CampagneBulletinImportLineDTO
    {
        $this->montantLibres = $montantLibres;

        return $this;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): CampagneBulletinImportLineDTO
    {
        $this->amapien = $amapien;

        return $this;
    }

    public function getCampagne(): Campagne
    {
        return $this->campagne;
    }

    public function setCampagne(Campagne $campagne): CampagneBulletinImportLineDTO
    {
        $this->campagne = $campagne;

        return $this;
    }
}
