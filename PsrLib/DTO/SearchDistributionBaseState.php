<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\Period;
use Symfony\Component\Validator\Constraints as Assert;

class SearchDistributionBaseState
{
    private ?\PsrLib\ORM\Entity\AmapLivraisonLieu $livLieu = null;

    #[Assert\Valid]
    private ?\PsrLib\ORM\Entity\Embeddable\Period $period = null;

    private ?\PsrLib\ORM\Entity\Amap $amap = null;

    public function getLivLieu(): ?AmapLivraisonLieu
    {
        return $this->livLieu;
    }

    public function setLivLieu(?AmapLivraisonLieu $livLieu): SearchDistributionBaseState
    {
        $this->livLieu = $livLieu;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): SearchDistributionBaseState
    {
        $this->period = $period;

        return $this;
    }

    public function getAmap(): ?\PsrLib\ORM\Entity\Amap
    {
        return $this->amap;
    }

    public function setAmap(?\PsrLib\ORM\Entity\Amap $amap): void
    {
        $this->amap = $amap;
    }
}
