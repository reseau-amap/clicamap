<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

class DatatableAjaxResponseDTO
{
    #[Groups(['datatable'])]
    private int $draw;

    #[Groups(['datatable'])]
    private int $recordsTotal;

    #[Groups(['datatable'])]
    private int $recordsFiltered;

    #[Groups(['datatable'])]
    private array $data;

    public function __construct(int $draw, int $recordsTotal, array $data)
    {
        $this->draw = $draw;
        $this->recordsTotal = $recordsTotal;
        $this->recordsFiltered = $recordsTotal;
        $this->data = $data;
    }

    public function getDraw(): int
    {
        return $this->draw;
    }

    public function getRecordsTotal(): int
    {
        return $this->recordsTotal;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getRecordsFiltered(): int
    {
        return $this->recordsFiltered;
    }
}
