<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;

class SearchEvenementAbonnable extends SearchDatatable
{
    private ?Region $region = null;
    private ?Departement $departement = null;
    private ?EvenementType $type = null;
    private ?string $keyword = null;
    private bool $subscriptionOnly = false;

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): SearchEvenementAbonnable
    {
        $this->region = $region;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): SearchEvenementAbonnable
    {
        $this->departement = $departement;

        return $this;
    }

    public function getType(): ?EvenementType
    {
        return $this->type;
    }

    public function setType(?EvenementType $type): SearchEvenementAbonnable
    {
        $this->type = $type;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchEvenementAbonnable
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function isSubscriptionOnly(): bool
    {
        return $this->subscriptionOnly;
    }

    public function setSubscriptionOnly(bool $subscriptionOnly): SearchEvenementAbonnable
    {
        $this->subscriptionOnly = $subscriptionOnly;

        return $this;
    }
}
