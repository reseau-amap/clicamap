<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;

class UserRepositoryReseauAdminFromAmapDTO
{
    #[Groups(['wizardCampagne'])]
    private User $user;

    #[Groups(['wizardCampagne'])]
    private Region|Departement $location;

    public function __construct(User $user, Departement|Region $location)
    {
        $this->user = $user;
        $this->location = $location;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getLocation(): Departement|Region
    {
        return $this->location;
    }
}
