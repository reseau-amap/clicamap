<?php

declare(strict_types=1);

namespace PsrLib\DTO;

class SearchDistributionAmapState extends SearchDistributionBaseState
{
    private bool $complete = false;

    public function isComplete(): bool
    {
        return $this->complete;
    }

    public function setComplete(bool $complete): SearchDistributionAmapState
    {
        $this->complete = $complete;

        return $this;
    }
}
