<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * Helper for calendar rendering.
 */
class ContratCelluleContainer
{
    private array $cellules;

    /**
     * @param ContratCellule[] $cellules
     */
    public function __construct(array $cellules)
    {
        $this->cellules = $cellules;
    }

    public function length(): int
    {
        return count($this->cellules);
    }

    public function sumByModeleContratProduit(ModeleContratProduit $mcp): float
    {
        $sum = 0.0;
        foreach ($this->cellules as $cellule) {
            if ($cellule->getModeleContratProduit() === $mcp) {
                $sum += $cellule->getQuantite();
            }
        }

        return $sum;
    }

    public function getMcDateFromMcCellules(ModeleContrat $mc): ?ModeleContratDate
    {
        foreach ($this->cellules as $cellule) {
            if ($cellule->getContrat()->getModeleContrat() === $mc) {
                return $cellule->getModeleContratDate();
            }
        }

        return null;
    }

    /**
     * @return ContratCellule[]
     */
    public function getByMcAmapien(ModeleContrat $mc, Amapien $amapien): array
    {
        return array_filter($this->cellules, fn (ContratCellule $cellule) => $cellule->getContrat()->getModeleContrat() === $mc && $cellule->getContrat()->getAmapien() === $amapien);
    }
}
