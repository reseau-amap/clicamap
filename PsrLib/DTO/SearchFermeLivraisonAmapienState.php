<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Ferme;
use Symfony\Component\Validator\Constraints as Assert;

class SearchFermeLivraisonAmapienState
{
    #[Assert\NotNull(message: 'Merci de sélectionner un amapien.')]
    private ?\PsrLib\ORM\Entity\Amapien $amapien = null;

    public function __construct(private readonly Ferme $ferme, private readonly Amap $amap, private readonly Carbon $date)
    {
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }

    public function getAmap(): Amap
    {
        return $this->amap;
    }

    public function getDate(): Carbon
    {
        return $this->date;
    }

    public function getAmapien(): ?Amapien
    {
        return $this->amapien;
    }

    public function setAmapien(?Amapien $amapien): SearchFermeLivraisonAmapienState
    {
        $this->amapien = $amapien;

        return $this;
    }
}
