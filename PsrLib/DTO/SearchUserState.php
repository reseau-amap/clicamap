<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\Enum\SearchUserStatePermissionEnum;

class SearchUserState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    private SearchUserStatePermissionEnum $superAdmin = SearchUserStatePermissionEnum::BLANK;
    private SearchUserStatePermissionEnum $admin = SearchUserStatePermissionEnum::BLANK;
    private SearchUserStatePermissionEnum $paysan = SearchUserStatePermissionEnum::BLANK;
    private SearchUserStatePermissionEnum $amap = SearchUserStatePermissionEnum::BLANK;
    private SearchUserStatePermissionEnum $amapien = SearchUserStatePermissionEnum::BLANK;

    public function isEmpty(): bool
    {
        return null === $this->getRegion()
            && null === $this->getDepartement()
            && SearchUserStatePermissionEnum::BLANK === $this->getSuperAdmin()
            && SearchUserStatePermissionEnum::BLANK === $this->getAdmin()
            && SearchUserStatePermissionEnum::BLANK === $this->getPaysan()
            && SearchUserStatePermissionEnum::BLANK === $this->getAmap()
            && SearchUserStatePermissionEnum::BLANK === $this->getAmapien()
        ;
    }

    public function getSuperAdmin(): SearchUserStatePermissionEnum
    {
        return $this->superAdmin;
    }

    public function setSuperAdmin(SearchUserStatePermissionEnum $superAdmin): void
    {
        $this->superAdmin = $superAdmin;
    }

    public function getAdmin(): SearchUserStatePermissionEnum
    {
        return $this->admin;
    }

    public function setAdmin(SearchUserStatePermissionEnum $admin): void
    {
        $this->admin = $admin;
    }

    public function getPaysan(): SearchUserStatePermissionEnum
    {
        return $this->paysan;
    }

    public function setPaysan(SearchUserStatePermissionEnum $paysan): void
    {
        $this->paysan = $paysan;
    }

    public function getAmap(): SearchUserStatePermissionEnum
    {
        return $this->amap;
    }

    public function setAmap(SearchUserStatePermissionEnum $amap): void
    {
        $this->amap = $amap;
    }

    public function getAmapien(): SearchUserStatePermissionEnum
    {
        return $this->amapien;
    }

    public function setAmapien(SearchUserStatePermissionEnum $amapien): void
    {
        $this->amapien = $amapien;
    }
}
