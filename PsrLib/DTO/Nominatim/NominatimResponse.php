<?php

declare(strict_types=1);

namespace PsrLib\DTO\Nominatim;

class NominatimResponse
{
    private ?string $lat = null;

    private ?string $lon = null;

    private ?\PsrLib\DTO\Nominatim\NominatimResponseAddress $address = null;

    private ?string $displayName = null;

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(?string $lat): void
    {
        $this->lat = $lat;
    }

    public function getLon(): ?string
    {
        return $this->lon;
    }

    public function setLon(?string $lon): void
    {
        $this->lon = $lon;
    }

    public function getAddress(): NominatimResponseAddress
    {
        return $this->address;
    }

    public function setAddress(NominatimResponseAddress $address): void
    {
        $this->address = $address;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(?string $displayName): void
    {
        $this->displayName = $displayName;
    }
}
