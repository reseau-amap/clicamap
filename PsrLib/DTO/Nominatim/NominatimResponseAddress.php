<?php

declare(strict_types=1);

namespace PsrLib\DTO\Nominatim;

use Symfony\Component\Serializer\Annotation\SerializedName;

class NominatimResponseAddress
{
    private ?string $house_number = null;

    private ?string $road = null;

    private ?string $hamlet = null;

    private ?string $village = null;

    private ?string $city = null;

    private ?string $municipality = null;

    private ?string $town = null;

    private ?string $county = null;

    private ?string $region = null;

    private ?string $postCode = null;

    #[SerializedName('ISO3166-2-lvl6')]
    private ?string $ISO3166lv16 = null;

    /**
     * Get the place of the address. Return the more specific.
     */
    public function getPlaceName(): ?string
    {
        $places = ['hamlet', 'village', 'town', 'city'];
        foreach ($places as $place) {
            if (null !== $this->{$place}) {
                return $this->{$place};
            }
        }

        return null;
    }

    public function getHouseNumber(): ?string
    {
        return $this->house_number;
    }

    public function setHouseNumber(?string $house_number): void
    {
        $this->house_number = $house_number;
    }

    public function getRoad(): ?string
    {
        return $this->road;
    }

    public function setRoad(?string $road): void
    {
        $this->road = $road;
    }

    public function getHamlet(): ?string
    {
        return $this->hamlet;
    }

    public function setHamlet(?string $hamlet): void
    {
        $this->hamlet = $hamlet;
    }

    public function getVillage(): ?string
    {
        return $this->village;
    }

    public function setVillage(?string $village): void
    {
        $this->village = $village;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getMunicipality(): ?string
    {
        return $this->municipality;
    }

    public function setMunicipality(?string $municipality): void
    {
        $this->municipality = $municipality;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): void
    {
        $this->county = $county;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): void
    {
        $this->region = $region;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function setPostCode(?string $postCode): void
    {
        $this->postCode = $postCode;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): void
    {
        $this->town = $town;
    }

    public function getISO3166lv16(): ?string
    {
        return $this->ISO3166lv16;
    }

    public function setISO3166lv16(?string $ISO3166lv16): NominatimResponseAddress
    {
        $this->ISO3166lv16 = $ISO3166lv16;

        return $this;
    }

    public function getISO3166lv16DepNumber(): ?string
    {
        if (null === $this->ISO3166lv16) {
            return null;
        }

        return explode('-', $this->ISO3166lv16)[1] ?? null;
    }
}
