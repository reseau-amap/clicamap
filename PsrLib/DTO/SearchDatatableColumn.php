<?php

declare(strict_types=1);

namespace PsrLib\DTO;

class SearchDatatableColumn
{
    private ?string $data = null;
    private ?string $name = null;
    private bool $searchable = false;
    private bool $orderable = false;

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): SearchDatatableColumn
    {
        $this->data = $data;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): SearchDatatableColumn
    {
        $this->name = $name;

        return $this;
    }

    public function isSearchable(): bool
    {
        return $this->searchable;
    }

    public function setSearchable(string $searchable): SearchDatatableColumn
    {
        $this->searchable = 'true' === $searchable;

        return $this;
    }

    public function isOrderable(): bool
    {
        return $this->orderable;
    }

    public function setOrderable(string $orderable): SearchDatatableColumn
    {
        $this->orderable = 'true' === $orderable;

        return $this;
    }
}
