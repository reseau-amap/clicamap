<?php

declare(strict_types=1);

namespace PsrLib\DTO;

class SearchDocumentUtilisateurState implements SearchRegionDepartementInterface
{
    use SearchRegionDepartementTrait;

    private ?int $year = null;

    private ?string $keyword = null;

    public function isEmpty(): bool
    {
        return null === $this->getYear()
            && null === $this->getKeyword()
            && null === $this->getRegion()
            && null === $this->getDepartement()
        ;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): SearchDocumentUtilisateurState
    {
        $this->year = $year;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): SearchDocumentUtilisateurState
    {
        $this->keyword = $keyword;

        return $this;
    }
}
