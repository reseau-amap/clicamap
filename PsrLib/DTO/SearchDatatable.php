<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class SearchDatatable
{
    #[SerializedName('length')]
    private int $maxResults = 25;

    #[SerializedName('start')]
    private int $offset = 0;

    private int $draw = 0;

    /**
     * @var SearchDatatableColumn[]
     */
    private array $columns = [];

    /**
     * @var SearchDatatableOrder[]
     */
    private array $order = [];

    public function getMaxResults(): int
    {
        return $this->maxResults;
    }

    public function setMaxResults(string $maxResults): self
    {
        $this->maxResults = (int) $maxResults;

        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(string $offset): self
    {
        $this->offset = (int) $offset;

        return $this;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param SearchDatatableColumn[] $columns
     *
     * @return $this
     */
    public function setColumns(array $columns): SearchDatatable
    {
        $this->columns = $columns;

        return $this;
    }

    public function getDraw(): int
    {
        return $this->draw;
    }

    public function setDraw(string $draw): SearchDatatable
    {
        $this->draw = (int) $draw;

        return $this;
    }

    public function getOrder(): array
    {
        return $this->order;
    }

    /**
     * @param SearchDatatableOrder[] $order
     *
     * @return $this
     */
    public function setOrder(array $order): SearchDatatable
    {
        $this->order = $order;

        return $this;
    }
}
