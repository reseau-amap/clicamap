<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\EvenementAbonnementWithRelatedEntity;
use PsrLib\ORM\Entity\EvenementRelatedEntity;
use PsrLib\ORM\Entity\User;

class EvenementAbonnementContainer
{
    /**
     * @var EvenementAbonnementWithRelatedEntity[]
     */
    private array $abonnements;

    /**
     * @param EvenementAbonnementWithRelatedEntity[] $abonnements
     */
    public function __construct(array $abonnements)
    {
        $this->abonnements = $abonnements;
    }

    /**
     * @return EvenementRelatedEntity[]
     */
    public function getRelatedEntitiesFor(EvenementType $type)
    {
        $res = [];
        foreach ($this->abonnements as $abonnement) {
            $relatedClass = $type->getRelatedEntityClass();
            if ($abonnement->getRelated() instanceof $relatedClass) {
                $res[] = $abonnement->getRelated();
            }
        }

        return array_unique($res, SORT_REGULAR);
    }

    public function getAbonnement(User $user, EvenementRelatedEntity $entity): ?EvenementAbonnement
    {
        foreach ($this->abonnements as $abonnement) {
            if ($abonnement->getRelated() === $entity && $abonnement->getUser() === $user) {
                return $abonnement;
            }
        }

        return null;
    }

    public function hasAbonnement(User $user, EvenementRelatedEntity $entity): bool
    {
        return null !== $this->getAbonnement($user, $entity);
    }

    /**
     * @return EvenementAbonnementWithRelatedEntity[]
     */
    public function getAbonnements(): array
    {
        return $this->abonnements;
    }
}
