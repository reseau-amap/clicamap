<?php

declare(strict_types=1);

namespace PsrLib\DTO\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ZxcvbnResponse extends JsonResponse
{
    public function __construct(int $strength)
    {
        parent::__construct(['strength' => $strength]);
    }
}
