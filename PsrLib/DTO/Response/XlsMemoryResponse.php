<?php

declare(strict_types=1);

namespace PsrLib\DTO\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Response to allow user to download dynamic generated file.
 */
class XlsMemoryResponse extends Response
{
    public function __construct(string $content, string $fileName)
    {
        parent::__construct($content);

        $this->headers->set('Content-Type', 'application/vnd.ms-excel');
        $this->headers->set('Content-Disposition', 'attachment; filename="'.$fileName.'"');
    }
}
