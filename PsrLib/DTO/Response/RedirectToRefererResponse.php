<?php

declare(strict_types=1);

namespace PsrLib\DTO\Response;

use Symfony\Component\HttpFoundation\RedirectResponse;

class RedirectToRefererResponse extends RedirectResponse
{
    public function __construct(string $default)
    {
        $referer = $this->getReferer();

        parent::__construct($referer ?: $default);
    }

    private function getReferer(): string
    {
        return empty($_SERVER['HTTP_REFERER']) ? '' : trim($_SERVER['HTTP_REFERER']);
    }
}
