<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Campagne;

class CampagneWizardDTO
{
    private Campagne $campagne;

    private ?Campagne $previous;

    public function __construct(Campagne $campagne, ?Campagne $previous)
    {
        $this->campagne = $campagne;
        $this->previous = $previous;
    }

    public function getCampagne(): Campagne
    {
        return $this->campagne;
    }

    public function getPrevious(): ?Campagne
    {
        return $this->previous;
    }
}
