<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Money\Money;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

class ContratCommandeItem
{
    private float $qty = 0.0;

    private bool $disabled;

    private ModeleContratProduit $mcp;

    private ModeleContratDate $mcd;

    public function __construct(ModeleContratProduit $mcp, ModeleContratDate $mcd)
    {
        $this->mcp = $mcp;
        $this->mcd = $mcd;
        $this->disabled = $this->mcp->hasExclusionForModeleContratDate($this->mcd);
    }

    public function total(): Money
    {
        return $this->mcp->getPrix()->getPrixTTC()->multiply((string) $this->qty);
    }

    public function getMcp(): ModeleContratProduit
    {
        return $this->mcp;
    }

    public function getMcd(): ModeleContratDate
    {
        return $this->mcd;
    }

    public function getQty(): float
    {
        return $this->qty;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setQty(float $qty): ContratCommandeItem
    {
        if ($this->isDisabled()) {
            throw new \RuntimeException('Cannot set qty for disabled item');
        }
        $this->qty = $qty;

        return $this;
    }
}
