<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonHoraire;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;

class AmapCreationWizard
{
    #[Groups(['wizardAmap'])]
    private ?\PsrLib\ORM\Entity\Amap $amap = null;

    #[Groups(['wizardAmap'])]
    private ?\PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu = null;

    #[Groups(['wizardAmap'])]
    private ?\PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire = null;

    #[Groups(['wizardAmap'])]
    private ?\PsrLib\ORM\Entity\User $admin = null;

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): AmapCreationWizard
    {
        $this->amap = $amap;

        return $this;
    }

    public function getLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }

    public function setLivraisonLieu(?AmapLivraisonLieu $livraisonLieu): AmapCreationWizard
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }

    public function getLivraisonHoraire(): ?AmapLivraisonHoraire
    {
        return $this->livraisonHoraire;
    }

    public function setLivraisonHoraire(?AmapLivraisonHoraire $livraisonHoraire): AmapCreationWizard
    {
        $this->livraisonHoraire = $livraisonHoraire;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): void
    {
        $this->admin = $admin;
    }
}
