<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;

class AmapDistributionDetailMassEdit
{
    public function __construct(private AmapDistributionDetail $detail, private ?AmapLivraisonLieu $livraisonLieu)
    {
    }

    public function getDetail(): AmapDistributionDetail
    {
        return $this->detail;
    }

    public function setDetail(AmapDistributionDetail $detail): AmapDistributionDetailMassEdit
    {
        $this->detail = $detail;

        return $this;
    }

    public function getLivraisonLieu(): ?AmapLivraisonLieu
    {
        return $this->livraisonLieu;
    }

    public function setLivraisonLieu(?AmapLivraisonLieu $livraisonLieu): AmapDistributionDetailMassEdit
    {
        $this->livraisonLieu = $livraisonLieu;

        return $this;
    }
}
