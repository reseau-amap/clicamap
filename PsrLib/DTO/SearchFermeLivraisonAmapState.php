<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use Symfony\Component\Validator\Constraints as Assert;

class SearchFermeLivraisonAmapState
{
    #[Assert\NotNull]
    private ?\PsrLib\ORM\Entity\Amap $amap = null;

    public function __construct(private readonly Ferme $ferme, private readonly Carbon $date)
    {
    }

    public function getDate(): Carbon
    {
        return $this->date;
    }

    public function getFerme(): Ferme
    {
        return $this->ferme;
    }

    public function getAmap(): ?Amap
    {
        return $this->amap;
    }

    public function setAmap(?Amap $amap): SearchFermeLivraisonAmapState
    {
        $this->amap = $amap;

        return $this;
    }
}
