<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\Contrat;
use PsrLib\Validator\ContratCommandeDeplacementValidateDeplacement;
use PsrLib\Validator\ContratCommandeDeplacementValidateReport;
use PsrLib\Validator\ContratCommandeDeplacementValidateTotalSame;

#[ContratCommandeDeplacementValidateTotalSame(groups: ['move'])]
#[ContratCommandeDeplacementValidateDeplacement(groups: ['move'])]
#[ContratCommandeDeplacementValidateReport(groups: ['move'])]
class ContratCommandeDeplacement extends ContratCommandeBase
{
    private Contrat $contrat;

    public static function createFromContrat(Contrat $contrat): static
    {
        $order = parent::createFromContrat($contrat);
        $order->contrat = $contrat;

        return $order;
    }

    public function getSubscribableMcp(): array
    {
        $mcps = $this->mc->getProduits()->toArray();

        return array_filter($mcps, fn ($mcp) => $this->contrat->totalQtyByProduct($mcp) > 0);
    }

    public function getItems(): array
    {
        $subscribableMcp = $this->getSubscribableMcp();

        return array_filter($this->items, fn ($item) => in_array($item->getMcp(), $subscribableMcp));
    }

    public function getContrat(): Contrat
    {
        return $this->contrat;
    }
}
