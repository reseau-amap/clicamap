<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use Money\Money;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * Base classe for order
 * Used for subscription and move
 * Do not use it directly.
 */
/** @phpstan-consistent-constructor */
abstract class ContratCommandeBase
{
    /**
     * @var ContratCommandeItem[]
     */
    protected $items;

    protected ModeleContrat $mc;

    private function __construct() // Use createFromMc or createFromContrat
    {
    }

    abstract public function getSubscribableMcp(): array;

    abstract public function getItems(): array;

    public function getItem(ModeleContratProduit $mcp, ModeleContratDate $mcd): ContratCommandeItem
    {
        return $this->items[self::getKey($mcp, $mcd)];
    }

    public static function createFromMc(ModeleContrat $mc): static
    {
        $order = new static();
        $order->items = [];
        $order->mc = $mc;

        foreach ($mc->getDatesOrderedAsc() as $date) {
            foreach ($mc->getProduits() as $produit) {
                $order->items[self::getKey($produit, $date)] = new ContratCommandeItem($produit, $date);
            }
        }

        return $order;
    }

    public static function createFromContrat(Contrat $contrat): static
    {
        $order = self::createFromMc($contrat->getModeleContrat());
        foreach ($contrat->getCellules() as $cellule) {
            $item = $order->getItem($cellule->getModeleContratProduit(), $cellule->getModeleContratDate());
            if (!$item->isDisabled()) {
                $item->setQty($cellule->getQuantite() ?? 0.0);
            }
        }

        return $order;
    }

    public static function getKey(ModeleContratProduit $mcp, ModeleContratDate $mcd): string
    {
        return sprintf('%s_%s', $mcd->getId(), $mcp->getId());
    }

    public function applyToContract(Contrat $contrat): void
    {
        foreach ($contrat->getCellules() as $cellule) {
            $item = $this->getItem($cellule->getModeleContratProduit(), $cellule->getModeleContratDate());
            if (!$item->isDisabled()) {
                $cellule->setQuantite($item->getQty());
            }
        }
    }

    public function total(): Money
    {
        $total = Money::EUR(0);
        foreach ($this->items as $item) {
            $total = $total->add($item->total());
        }

        return $total;
    }

    public function totalQtyByMcp(ModeleContratProduit $modeleContratProduit): float
    {
        $total = 0.0;
        foreach ($this->items as $item) {
            if ($item->getMcp() === $modeleContratProduit) {
                $total += $item->getQty();
            }
        }

        return $total;
    }

    public function totalByMcp(ModeleContratProduit $modeleContratProduit): Money
    {
        $total = Money::EUR(0);
        foreach ($this->items as $item) {
            if ($item->getMcp() === $modeleContratProduit) {
                $total = $total->add($item->total());
            }
        }

        return $total;
    }

    public function totalByMcd(ModeleContratDate $modeleContratDate): Money
    {
        $total = Money::EUR(0);
        foreach ($this->items as $item) {
            if ($item->getMcd() === $modeleContratDate) {
                $total = $total->add($item->total());
            }
        }

        return $total;
    }

    public function countNbLivraisons(): int
    {
        $datesWithLiv = 0;
        foreach ($this->getMc()->getDatesOrderedAsc() as $date) {
            if ($this->totalByMcd($date)->isPositive()) {
                ++$datesWithLiv;
            }
        }

        return $datesWithLiv;
    }

    public function areLivSame(): bool // TODO: test
    {
        // Groupe les commandes par dates
        $orderDates = [];
        foreach ($this->items as $item) {
            $dateId = $item->getMcd()->getId();
            if (!array_key_exists($dateId, $orderDates)) {
                $orderDates[$dateId] = [];
            }
            $orderDates[$dateId][] = $item;
        }

        // TODO: force sort ?

        // Référence de la première ligne contenant des commandes
        $orderRefDateId = null;
        foreach ($orderDates as $dateId => $orders) {
            $nbLiv = array_reduce($orders, fn ($count, ContratCommandeItem $c) => $count + (float) $c->getQty(), 0);

            // La ligne ne contiens pas de livraisons. Elle est ignorée
            if (0.0 === $nbLiv) {
                continue;
            }

            // La ligne est la première qui contiens des livraison. Elle est sauvegardée comme référence
            if (null === $orderRefDateId) {
                $orderRefDateId = $dateId;

                continue;
            }

            // Compare la ligne courante avec la ligne de référence
            foreach ($orders as $i => $order) {
                if (!isset($orderDates[$orderRefDateId][$i])) { // Pour les contrats avec des nombres de produits différents
                    if (0.0 !== $order->getQty()) {
                        return false;
                    }
                } elseif ($orderDates[$orderRefDateId][$i]->getQty() !== $order->getQty()) {
                    return false;
                }
            }
        }

        return true;
    }

    public function getMc(): ModeleContrat
    {
        return $this->mc;
    }
}
