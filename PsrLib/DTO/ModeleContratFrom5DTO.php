<?php

declare(strict_types=1);

namespace PsrLib\DTO;

use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;

class ModeleContratFrom5DTO
{
    /**
     * @var array<string, array<int, bool>>
     */
    private array $exclusions = [];

    /**
     * @var ModeleContratDate[]
     */
    private readonly array $dates;

    /**
     * @var ModeleContratProduit[]
     */
    private readonly array $produits;

    public function __construct(ModeleContrat $mc)
    {
        $this->dates = $mc->getDatesOrderedAsc();
        $this->produits = $mc->getProduits()->toArray();

        $this->exclusions = [];
        foreach ($this->dates as $date) {
            foreach ($this->produits as $produit) {
                $this->exclusions[
                    $this->getKeyForProduitDate($produit, $date)
                    ] = false === $produit
                        ->getExclusions()
                        ->filter(fn (ModeleContratProduitExclure $exclure) => $exclure->getModeleContratDate() === $date)
                        ->first()
                ;
            }
        }
    }

    public function getKeyForProduitDate(ModeleContratProduit $p, ModeleContratDate $d): string
    {
        return sprintf('%s_%s', $d->getDateLivraison()->format('Y-m-d'), $p->getFermeProduit()->getId());
    }

    public function getExclusions(): array
    {
        return $this->exclusions;
    }

    public function setExclusions(array $exclusions): ModeleContratFrom5DTO
    {
        $this->exclusions = $exclusions;

        return $this;
    }

    public function getDates(): array
    {
        return $this->dates;
    }

    public function getProduits(): array
    {
        return $this->produits;
    }
}
