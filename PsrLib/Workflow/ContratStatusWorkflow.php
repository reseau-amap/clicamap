<?php

declare(strict_types=1);

namespace PsrLib\Workflow;

use Symfony\Component\Workflow\Definition;
use Symfony\Component\Workflow\MarkingStore\MethodMarkingStore;
use Symfony\Component\Workflow\StateMachine;
use Symfony\Component\Workflow\Transition;

/**
 * Use custom type to allow autowiring
 * With PHPDI.
 */
class ContratStatusWorkflow extends StateMachine
{
    public const STATE_VALIDATED = 'STATE_VALIDATED';
    public const STATE_AMAPIEN_TO_VALIDATE = 'STATE_AMAPIEN_TO_VALIDATE';
    public const TRANSITION_AMAPIEN_VALIDATION = 'TRANSITION_AMAPIEN_VALIDATION';

    public function __construct()
    {
        parent::__construct(
            new Definition(
                [self::STATE_AMAPIEN_TO_VALIDATE, self::STATE_VALIDATED],
                [
                    new Transition(
                        self::TRANSITION_AMAPIEN_VALIDATION,
                        [self::STATE_AMAPIEN_TO_VALIDATE],
                        [self::STATE_VALIDATED]
                    ),
                ],
            ),
            new MethodMarkingStore(true, 'state'),
        );
    }
}
