<?php

declare(strict_types=1);

namespace PsrLib\TwigcsRuleset;

use FriendsOfTwig\Twigcs\Rule\RuleInterface;
use FriendsOfTwig\Twigcs\Rule\UnusedVariable;
use FriendsOfTwig\Twigcs\Ruleset\Official;

class OfficialMinusUnusedVariables extends Official
{
    public function getRules()
    {
        $rules = parent::getRules();

        return array_filter($rules, fn (RuleInterface $rule) => !$rule instanceof UnusedVariable);
    }
}
