<?php

declare(strict_types=1);

namespace PsrLib\Enum;

enum ContratPaiementStatut: string
{
    case NON_PAYE = 'NON_PAYE';
    case PAIEMENT_PARTIEL = 'PAIEMENT_PARTIEL';
    case PAYE = 'PAYE';
}
