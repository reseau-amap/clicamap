<?php

declare(strict_types=1);

namespace PsrLib\Enum;

enum ModeleContratEtat: string
{
    case ETAT_BROUILLON = 'brouillon';
    case ETAT_CREATION = 'creation';
    case ETAT_VALIDATION_PAYSAN = 'validation';
    case ETAT_VALIDE = 'valide';
    case ETAT_REFUS = 'refus';
    case ETAT_ACTIF = 'actif'; // Old contract state not used for new one
}
