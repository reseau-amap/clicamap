<?php

declare(strict_types=1);

namespace PsrLib\Enum;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\EvenementAbonnementDepartement;
use PsrLib\ORM\Entity\EvenementAbonnementFerme;
use PsrLib\ORM\Entity\EvenementAbonnementRegion;
use PsrLib\ORM\Entity\EvenementAbonnementWithRelatedEntity;
use PsrLib\ORM\Entity\EvenementAmap;
use PsrLib\ORM\Entity\EvenementDepartement;
use PsrLib\ORM\Entity\EvenementFerme;
use PsrLib\ORM\Entity\EvenementRegion;
use PsrLib\ORM\Entity\EvenementRelatedEntity;
use PsrLib\ORM\Entity\EvenementSuperAdmin;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\Services\DoctrineProxyUtils;

enum EvenementType: string
{
    private const RELATED_ENTITY_CLASS_MAP = [
        Ferme::class => self::FERME,
        Amap::class => self::AMAP,
        Region::class => self::REGION,
        Departement::class => self::DEPARTEMENT,
    ];

    case FERME = 'FERME';
    case AMAP = 'AMAP';
    case REGION = 'REGION';
    case DEPARTEMENT = 'DEPARTEMENT';
    case SUPER_ADMIN = 'SUPER_ADMIN';

    /**
     * @return class-string<Evenement>
     */
    public function getRelatedEvtClass(): string
    {
        return match ($this) {
            self::FERME => EvenementFerme::class,
            self::AMAP => EvenementAmap::class,
            self::REGION => EvenementRegion::class,
            self::DEPARTEMENT => EvenementDepartement::class,
            self::SUPER_ADMIN => EvenementSuperAdmin::class,
        };
    }

    /**
     * @return class-string<EvenementRelatedEntity>|false
     */
    public function getRelatedEntityClass(): string|false
    {
        return array_search($this, self::RELATED_ENTITY_CLASS_MAP, true);
    }

    public static function fromRelatedEntity(EvenementRelatedEntity $relatedEntity): self
    {
        return self::RELATED_ENTITY_CLASS_MAP[DoctrineProxyUtils::getRealClass($relatedEntity)];
    }

    /**
     * @return class-string<EvenementAbonnementWithRelatedEntity>|null
     */
    public function getRelatedAbonnementEntityClass(): ?string
    {
        return match ($this) {
            self::FERME => EvenementAbonnementFerme::class,
            self::AMAP => EvenementAbonnementAmap::class,
            self::REGION => EvenementAbonnementRegion::class,
            self::DEPARTEMENT => EvenementAbonnementDepartement::class,
            self::SUPER_ADMIN => null,
        };
    }
}
