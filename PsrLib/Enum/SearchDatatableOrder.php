<?php

declare(strict_types=1);

namespace PsrLib\Enum;

enum SearchDatatableOrder: string
{
    case ASC = 'asc';
    case DESC = 'desc';

    public function valueSQL(): string
    {
        return strtoupper($this->value);
    }
}
