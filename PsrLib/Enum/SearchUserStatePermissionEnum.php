<?php

declare(strict_types=1);

namespace PsrLib\Enum;

enum SearchUserStatePermissionEnum: string
{
    case BLANK = 'BLANK';
    case Y = 'Y';
    case N = 'N';
}
