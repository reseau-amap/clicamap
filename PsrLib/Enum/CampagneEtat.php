<?php

declare(strict_types=1);

namespace PsrLib\Enum;

use Symfony\Contracts\Translation\TranslatableInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

enum CampagneEtat: string implements TranslatableInterface
{
    case ETAT_BROUILLON = 'brouillon';
    case ETAT_ENCOURS = 'encours';
    case ETAT_TERMINE = 'termine';

    public function trans(TranslatorInterface $translator, string $locale = null): string
    {
        return $translator->trans($this->value, [], 'campagne_etat');
    }
}
