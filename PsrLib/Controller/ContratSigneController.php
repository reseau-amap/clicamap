<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Assert\Assertion;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Exception;
use Money\Money;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PsrLib\DTO\ContratCommandeDeplacement;
use PsrLib\DTO\ContratCommandeSouscription;
use PsrLib\DTO\ContratWizard;
use PsrLib\DTO\ModeleContratDeplacement;
use PsrLib\DTO\Response\RedirectToRefererResponse;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\Form\ContratPaiementStatutType;
use PsrLib\Form\ContratSigneNewForce;
use PsrLib\Form\ContratSouscriptionForm1Type;
use PsrLib\Form\ContratSouscriptionForm2Type;
use PsrLib\Form\ContratSubscribe3Type;
use PsrLib\Form\ModeleContratDelaiType;
use PsrLib\Form\ModeleContratDeplacementType;
use PsrLib\Form\SearchContratSigneAmapType;
use PsrLib\Form\SearchContratSignePaysanType;
use PsrLib\Form\SearchContratSignePaysanWithPaiementStatusType;
use PsrLib\Form\SearchContratSigneType;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\ContratRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\AppFunction;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\ContractCalculator;
use PsrLib\Services\ContratCalculatorGenerateResponse;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Exporters\ExcelGenerator;
use PsrLib\Services\Exporters\ExcelGeneratorExportContratAccountDate;
use PsrLib\Services\Exporters\ExcelGeneratorExportContratAccountProductDeliveredQuantity;
use PsrLib\Services\Exporters\ExcelGeneratorExportContratAccountProductOrderedQuantity;
use PsrLib\Services\JwtToken;
use PsrLib\Services\MoneyHelper;
use PsrLib\Services\MPdfGeneration;
use PsrLib\Services\RequestHtmxUtils;
use PsrLib\Services\Security\Voters\ContractAmapienValidationVoter;
use PsrLib\Services\Security\Voters\ContractVoter;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use PsrLib\Workflow\ContratStatusWorkflow;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use function Symfony\Component\String\u;

class ContratSigneController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_contrat_signe';

    #[Route('/contrat_signe/counter/{mc_id}', name: 'contrat_signe_counter')]
    public function counter(int $mc_id): Response
    {
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        return $this->render('contrat_signe/counter_step_1.html.twig', [
            'mc' => $mc,
            'retPath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/move/{mc_id}', name: 'contrat_signe_move')]
    public function move(int $mc_id): Response
    {
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_MOVE_DATE, $mc);

        $mcd = new ModeleContratDeplacement($mc);
        $this->session_store_contrat_move($mcd);

        return $this->redirectToRoute('contrat_signe_move_step_1');
    }

    #[Route('/contrat_signe/move_step_1', name: 'contrat_signe_move_step_1')]
    public function move_step_1(): Response
    {
        $mcd = $this->session_get_contrat_move();
        if (false === $mcd) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $form = $this->formFactory->create(ModeleContratDeplacementType::class, $mcd);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session_store_contrat_move($mcd);

            return $this->redirectToRoute('contrat_signe_move_step_2');
        }

        return $this->render('contrat_signe/move_step_1.html.twig', [
            'form' => $form->createView(),
            'mc' => $mcd->getMc(),
            'retPath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/move_step_2', name: 'contrat_signe_move_step_2')]
    public function move_step_2(ValidatorInterface $validator): Response
    {
        $mcd = $this->session_get_contrat_move();
        if (false === $mcd) {
            redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $violations = $validator->validate($mcd);
        if (count($violations) > 0) {
            redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $src = $mcd->getSrc();
        $dst = $mcd->getDst();
        $mc = $mcd->getMc();

        if ($this->request->isMethod('POST')) {
            $src->setDateLivraison($dst);
            $this->em->flush();
            $this->addFlash(
                'notice_success',
                'Les contrats ont été modifiés avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $contratDates = $this
            ->em
            ->getRepository(Contrat::class)
            ->getWithDateMc($mcd->getMc(), $mcd->getSrc())
        ;
        $mc_amapiens_deplacement = [];
        foreach ($contratDates as $contrat) {
            $mc_amapiens_deplacement[] = $contrat->getAmapien();
        }

        return $this->render('contrat_signe/move_step_2.html.twig', [
            'mc_amapiens_deplacement' => $mc_amapiens_deplacement,
            'date_a_deplacer' => $src,
            'nouvelle_date' => $dst,
            'mc' => $mc,
            'retPath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/telecharger_souscripteurs/{mc_id}', name: 'contrat_signe_telecharger_souscripteurs')]
    public function telecharger_souscripteurs(int $mc_id, Spreadsheet $excel, AppFunction $appfunction): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $mc_nom = $mc->getNom();

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        /** @var Contrat[] $contrats */
        $contrats = $mc->getContratsValidated()->toArray();
        usort($contrats, fn (Contrat $contrat1, Contrat $contrat2) => strcasecmp(
            $contrat1->getAmapien()->getUser()->getName()->getFullNameInversed(),
            $contrat2->getAmapien()->getUser()->getName()->getFullNameInversed()
        ));

        // worksheet 1
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Liste des souscripteurs');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]], ];

        $excel->getActiveSheet()->setCellValue('A3', 'Nom');
        $excel->getActiveSheet()->setCellValue('B3', 'Prénom');
        $excel->getActiveSheet()->setCellValue('C3', 'Email');
        $excel->getActiveSheet()->setCellValue('D3', 'Tél 1');
        $excel->getActiveSheet()->setCellValue('E3', 'Adr');
        $excel->getActiveSheet()->setCellValue('F3', 'Code Postal');
        $excel->getActiveSheet()->setCellValue('G3', 'Ville');

        $excel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A3:G3')->getFont()->setBold(true);

        $i = 4;
        foreach ($contrats as $contrat) {
            $amapien = $contrat->getAmapien();
            $excel->getActiveSheet()->setCellValue('A'.$i, $appfunction->caracteres_speciaux($amapien->getUser()->getName()->getLastName()));
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, $appfunction->caracteres_speciaux($amapien->getUser()->getName()->getFirstName()));
            $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, implode(';', $amapien->getUser()->getEmails()->toArray()));
            $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('D'.$i, $appfunction->caracteres_speciaux($amapien->getUser()->getNumTel1()));
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('E'.$i, $appfunction->caracteres_speciaux($amapien->getUser()->getAddress()->getAdress()));
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $ville = $amapien->getUser()->getVille();
            $excel->getActiveSheet()->setCellValue('F'.$i, null === $ville ? null : $ville->getCpString());
            $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('G'.$i, $appfunction->caracteres_speciaux(null === $ville ? null : $ville->getNom()));
            $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->applyFromArray($style);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A1', $time);

        $nom_fichier = 'liste-des-souscripteurs-de-'.$appfunction->caracteres_speciaux($mc_nom).'.xls'; // save our workbook as this file name

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = IOFactory::createWriter($excel, IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportTelechargerSouscripteur_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outputFileName,
            $nom_fichier
        );
    }

    #[Route('/contrat_signe/telecharger_liasse_contrats/{mc_id}', name: 'contrat_signe_telecharger_liasse_contrats')]
    public function telecharger_liasse_contrats(int $mc_id, Spreadsheet $excel, AppFunction $appfunction): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $mc_nom = $mc->getNom();

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        /** @var Contrat[] $contrats */
        $contrats = $mc->getContratsValidated()->toArray();
        $nbr_contrats = count($contrats);

        /** @var ModeleContratProduit[] $mc_produits */
        $mc_produits = $mc->getProduits()->toArray();

        /** @var ModeleContratDate[] $mc_dates */
        $mc_dates = $mc->getDates()->toArray();

        $style = [
            'font' => ['bold' => true, 'color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER], ];

        $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

        // Un onglet par contrat
        $i = 0;
        foreach ($contrats as $contrat) {
            /** @var ContratCellule[] $cellules */
            $cellules = $contrat->getCellules()->toArray();

            $amapien = $contrat->getAmapien();
            $excel->setActiveSheetIndex($i);
            $titre = $amapien->getUser()->getName()->getLastName().' '.substr($amapien->getUser()->getName()->getFirstName(), 0, 1).'.';
            $excel->getActiveSheet()->setTitle(
                u($appfunction->caracteres_speciaux($titre))->truncate(Worksheet::SHEET_TITLE_MAXIMUM_LENGTH, '...')->toString()
            );

            // ENTÊTE TABLEAU PRODUITS
            $excel->getActiveSheet()->mergeCells('A4:A7', $time);
            $excel->getActiveSheet()->setCellValue('A4', 'Dates');
            $excel->getActiveSheet()->getStyle('A4:A7')->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            $excel->getActiveSheet()->mergeCells('B4:B7', $time);
            $excel->getActiveSheet()->setCellValue('B4', 'Total');
            $excel->getActiveSheet()->getStyle('B4:B7')->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            $j = 'C';
            foreach ($mc_produits as $pro) {
                $k = 4;
                // Type de produit
                $excel->getActiveSheet()->setCellValue($j.$k, $appfunction->caracteres_speciaux($pro->getTypeProduction()->getSlug()));
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Nom du produit
                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, $appfunction->caracteres_speciaux($pro->getNom()));
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, MoneyHelper::toStringDecimal($pro->getPrix()->getPrixTTC()).' €');
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);

                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, $appfunction->caracteres_speciaux($pro->getConditionnement()));
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

                ++$j;
            }

            // DATES ET QUANTITÉS
            $excel->getActiveSheet()->setCellValue('A9', 'Cumul');
            $excel->getActiveSheet()->getStyle('A9')->applyFromArray($style);

            $k = 11;
            foreach ($mc_dates as $d_l) {
                $excel->getActiveSheet()->setCellValue('A'.$k, $d_l->getDateLivraison()->format('Y-m-d'));
                $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);

                ++$k;
            }

            // CELLULES
            $k = 11; // Incrémentation des lignes
            $montant_total = Money::EUR(0);
            $produit_quantite_total = [];
            // Initialisation à 0 des valeurs du tableau $produit_quantite_total
            foreach ($mc_produits as $pro) {
                array_push($produit_quantite_total, 0);
            }

            foreach ($mc_dates as $mc_d) {
                $j = 'C'; // Incrémentation des colonnes
                $montant_total_date = Money::EUR(0);
                $index = 0; // Pour $produit_quantite_total

                foreach ($mc_produits as $mc_pro) {
                    $x = 0;

                    foreach ($cellules as $c_c) {
                        if ($mc_pro->getId() == $c_c->getModeleContratProduit()->getId() && $mc_d->getId() == $c_c->getModeleContratDate()->getId()) {
                            $excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                            $excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);

                            $produit_quantite_total[$index] += $c_c->getQuantite();
                            $montant_total_date = $montant_total_date->add(
                                $mc_pro->getPrix()->getPrixTTC()->multiply((string) $c_c->getQuantite())
                            );
                            ++$x;

                            break;
                        }
                    }

                    if (0 == $x) {
                        $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                    }

                    ++$j;
                    ++$index;
                }

                $excel->getActiveSheet()->setCellValue('B'.$k, MoneyHelper::toStringDecimal($montant_total_date).' €');
                $excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($stylePrix);

                $montant_total = $montant_total->add($montant_total_date);
                ++$k;
            }

            $excel->getActiveSheet()->setCellValue('B9', MoneyHelper::toStringDecimal($montant_total).' €');
            $excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('B9')->applyFromArray($stylePrix);

            $j = 'C';
            foreach ($produit_quantite_total as $p_q_t) {
                $excel->getActiveSheet()->setCellValue($j.'9', $p_q_t);
                $excel->getActiveSheet()->getStyle($j.'9')->applyFromArray($style);
                ++$j;
            }

            // ENTÊTE
            $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
            $excel->getActiveSheet()->setCellValue('A2', $time);
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $excel->getActiveSheet()->setCellValue('A1', 'Contrat de '.$appfunction->caracteres_speciaux($titre));

            // On ne crée pas de nouvel onglet s'il ne reste plus de produit
            if ($i < ($nbr_contrats - 1)) {
                $excel->createSheet();
            }

            ++$i;
        }

        // On replace le curseur sur le premier onglet
        $excel->setActiveSheetIndex(0);

        $nom_fichier = 'liste-des-souscripteurs-de-'.$appfunction->caracteres_speciaux($mc_nom).'.xls'; // Nom du .xls

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = IOFactory::createWriter($excel, IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportTelechargerLiasseContrat_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outputFileName,
            $nom_fichier
        );
    }

    #[Route('/contrat_signe/telecharger_fiche_distribution/{mc_id}', name: 'contrat_signe_telecharger_fiche_distribution')]
    public function telecharger_fiche_distribution(int $mc_id, MPdfGeneration $mpdf): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $file = $mpdf->genererContratSigneDistribution($mc);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $file,
            'distribution-'.$mc->getNom().'.pdf'
        );
    }

    #[Route('/contrat_signe/telecharger_feuilles_distribution/{mc_id}', name: 'contrat_signe_telecharger_feuilles_distribution')]
    public function telecharger_feuilles_distribution(int $mc_id, Spreadsheet $excel, AppFunction $appFunction): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mc);

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        $nbr_dates = $mc->getDates()->count();

        // Un onglet par contrat
        $i = 0;

        /** @var ModeleContratDate $mc_d */
        foreach ($mc->getDates() as $mc_d) {
            $excel->setActiveSheetIndex($i);

            $style = [
                'font' => ['bold' => true, 'color' => ['rgb' => '000000']],
                'borders' => ['outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000'], ]],
                'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER], ];

            $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

            // ENTÊTE TABLEAU PRODUITS
            // Nom de l'amapien
            $excel->getActiveSheet()->mergeCells('A4:A7');
            $excel->getActiveSheet()->setCellValue('A4', 'Nom');
            $excel->getActiveSheet()->getStyle('A4:A7')->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            // Prénom de l'amapien
            $excel->getActiveSheet()->mergeCells('B4:B7');
            $excel->getActiveSheet()->getStyle('B4:B7')->applyFromArray($style);
            $excel->getActiveSheet()->setCellValue('B4', 'Prénom');
            $excel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            /** @var Contrat[] $contrats */
            $contrats = $mc->getContratsValidated()->toArray();
            usort($contrats, fn (Contrat $c1, Contrat $c2) => strcasecmp(
                $c1->getAmapien()->getUser()->getName()->getFullNameInversed(),
                $c2->getAmapien()->getUser()->getName()->getFullNameInversed()
            ));

            $j = 'C';

            /** @var ModeleContratProduit $pro */
            foreach ($mc->getProduits() as $pro) {
                $prix = $pro->getPrix().' €';

                $k = 4;
                // Type de produit
                $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getTypeProduction()->getSlug()));
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Nom du produit
                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getNom()));
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Prix du produit
                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($prix));
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                // Conditionnement du produit
                ++$k;
                $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getConditionnement()));
                $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);

                ++$j;
            }

            // NOM, PRÉNOM ET QUANTITÉS
            $excel->getActiveSheet()->setCellValue('A9', 'Cumul');
            $excel->getActiveSheet()->getStyle('A9')->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);

            $k = 11;

            foreach ($contrats as $contrat) {
                $amapien = $contrat->getAmapien();
                $excel->getActiveSheet()->setCellValue('A'.$k, $appFunction->caracteres_speciaux($amapien->getUser()->getName()->getLastName()));
                $excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

                $excel->getActiveSheet()->setCellValue('B'.$k, $appFunction->caracteres_speciaux($amapien->getUser()->getName()->getFirstName()));
                $excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
                $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

                ++$k;
            }

            // CELLULES
            $k = 11; // Incrémentation des lignes
            $produit_quantite_total = [];
            // Initialisation à 0 des valeurs du tableau $produit_quantite_total
            foreach ($mc->getProduits() as $pro) {
                array_push($produit_quantite_total, 0);
            }

            foreach ($contrats as $contrat) {
                $j = 'C'; // Incrémentation des colonnes
                $index = 0; // Pour $produit_quantite_total

                /** @var ModeleContratProduit $mc_pro */
                foreach ($mc->getProduits() as $mc_pro) {
                    $x = 0;

                    /** @var ContratCellule $c_c */
                    foreach ($contrat->getCellules() as $c_c) {
                        if (
                            $mc_pro->getId() === $c_c->getModeleContratProduit()->getId()
                            && $mc_d->getId() === $c_c->getModeleContratDate()->getId()
                        ) {
                            if ($c_c->getQuantite() > 0.0) {
                                $excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                            }
                            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                            $excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);
                            $produit_quantite_total[$index] += $c_c->getQuantite();

                            ++$x;

                            break;
                        }
                    }

                    if (0 == $x) {
                        $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                    }
                    ++$j;
                    ++$index;
                }
                ++$k;
            }

            $j = 'C';
            foreach ($produit_quantite_total as $p_q_t) {
                $excel->getActiveSheet()->setCellValue($j.'9', $p_q_t);
                $excel->getActiveSheet()->getStyle($j.'9')->applyFromArray($style);
                ++$j;
            }

            // On ne crée pas de nouvel onglet s'il ne reste plus de produit
            if ($i < ($nbr_dates - 1)) {
                $excel->createSheet();
            }

            // ENTÊTE (à la fin pour placer le curseur sur la cellule 'A1')
            $titre = $mc_d->getDateLivraison()->format('Y-m-d');
            $excel->getActiveSheet()->setTitle($appFunction->caracteres_speciaux($titre));

            $excel->getActiveSheet()->getStyle('A2')->applyFromArray($style);
            $excel->getActiveSheet()->setCellValue('A2', $time);
            $excel->getActiveSheet()->getStyle('A1')->applyFromArray($style);
            $excel->getActiveSheet()->setCellValue('A1', 'Feuille de distribution du '.$titre);

            ++$i;
        }

        // On replace le curseur sur le premier onglet
        $excel->setActiveSheetIndex(0);

        $nom_fichier = 'feuille-distribution-'.$appFunction->caracteres_speciaux($mc->getNom()).'.xls'; // Nom du .xls

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = IOFactory::createWriter($excel, IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportTelechargerFeuilleDistribution_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outputFileName,
            $nom_fichier
        );
    }

    #[Route('/contrat_signe/telecharger_synthese_contrat/{mc_id}', name: 'contrat_signe_telecharger_synthese_contrat')]
    public function telecharger_synthese_contrat(int $mc_id, ExcelGenerator $excelGenerator): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_PAYMENT_SUMMARY, $mc);

        $slugify = new Slugify();
        $fileName = sprintf(
            'synthese_paiement_%s.xls',
            $slugify->slugify($mc->getNom(), '_')
        );

        $outFileName = $excelGenerator->generer_synthese_contrat_reglements($mc);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outFileName,
            $fileName
        );
    }

    #[Route('/contrat_signe/telecharger_contrat/{c_id}', name: 'contrat_signe_telecharger_contrat')]
    public function telecharger_contrat(int $c_id, Spreadsheet $excel, AppFunction $appFunction): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);

        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $c_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_XLS, $contrat);

        $time = 'Extrait le '.date('d/m/Y H:m:s');
        $amapien_nom = strtoupper($contrat->getAmapien()->getUser()->getName()->getLastName());
        $amapien_prenom = ucfirst($contrat->getAmapien()->getUser()->getName()->getFirstName());
        $mc = $contrat->getModeleContrat();
        $mc_nom = $mc->getNom();

        /** @var ModeleContratProduit[] $mc_produits */
        $mc_produits = $mc->getProduits()->toArray();

        /** @var ModeleContratDate[] $mc_dates_livraison */
        $mc_dates_livraison = $mc->getDates()->toArray();

        /** @var ContratCellule[] $contrat_cellules */
        $contrat_cellules = $contrat->getCellules()->toArray();

        $style = [
            'font' => ['bold' => true, 'color' => ['rgb' => '000']],
            'borders' => ['outline' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER], ];
        $stylePrix = ['font' => ['color' => ['rgb' => '0000FF']]];

        // --

        $excel->setActiveSheetIndex(0);
        $titre = $appFunction->caracteres_speciaux($amapien_nom).' '.$appFunction->caracteres_speciaux($amapien_prenom);
        $titre = substr($titre, 0, 31);
        $excel->getActiveSheet()->setTitle($appFunction->caracteres_speciaux($titre));

        // ENTÊTE TABLEAU PRODUITS
        $excel->getActiveSheet()->mergeCells('A5:A8', $time);
        $excel->getActiveSheet()->setCellValue('A5', 'Dates');
        $excel->getActiveSheet()->getStyle('A5:A8')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $excel->getActiveSheet()->mergeCells('B5:B8', $time);
        $excel->getActiveSheet()->setCellValue('B5', 'Total');
        $excel->getActiveSheet()->getStyle('B5:B8')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('B5')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

        $j = 'C';
        foreach ($mc_produits as $pro) {
            $k = 5;
            // Type de produit
            $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getTypeProduction()->getNom()));
            $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            // Nom du produit
            ++$k;
            $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getNom()));
            $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            ++$k;
            $excel->getActiveSheet()->setCellValue($j.$k, MoneyHelper::toStringDecimal($pro->getPrix()->getPrixTTC()).' €');
            $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($stylePrix);

            ++$k;
            $excel->getActiveSheet()->setCellValue($j.$k, $appFunction->caracteres_speciaux($pro->getConditionnement()));
            $excel->getActiveSheet()->getColumnDimension($j)->setAutoSize(true);
            $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);

            ++$j;
        }

        // DATES ET QUANTITÉS
        $excel->getActiveSheet()->setCellValue('A10', 'Cumul');
        $excel->getActiveSheet()->getStyle('A10')->applyFromArray($style);

        $k = 12;
        foreach ($mc_dates_livraison as $d_l) {
            $excel->getActiveSheet()->setCellValue('A'.$k, $d_l->getDateLivraison()->format('Y-m-d'));
            $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $excel->getActiveSheet()->getStyle('A'.$k)->applyFromArray($style);

            ++$k;
        }

        // CELLULES
        $k = 12; // Incrémentation des lignes
        $montant_total = Money::EUR(0);
        $produit_quantite_total = [];
        // Initialisation à 0 des valeurs du tableau $produit_quantite_total
        foreach ($mc_produits as $pro) {
            array_push($produit_quantite_total, 0);
        }

        foreach ($mc_dates_livraison as $mc_d) {
            $j = 'C'; // Incrémentation des colonnes
            $montant_total_date = Money::EUR(0);
            $index = 0; // Pour $produit_quantite_total

            foreach ($mc_produits as $mc_pro) {
                $x = 0;
                foreach ($contrat_cellules as $c_c) {
                    if ($mc_pro->getId() == $c_c->getModeleContratProduit()->getId() && $mc_d->getId() == $c_c->getModeleContratDate()->getId()) {
                        $excel->getActiveSheet()->setCellValue($j.$k, $c_c->getQuantite());
                        $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                        $excel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(false);
                        $produit_quantite_total[$index] += $c_c->getQuantite();
                        $montant_total_date = $montant_total_date->add(
                            $mc_pro->getPrix()->getPrixTTC()->multiply((string) $c_c->getQuantite())
                        );
                        ++$x;

                        break;
                    }
                }
                if (0 == $x) {
                    $excel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
                }

                ++$j;
                ++$index;
            }

            $excel->getActiveSheet()->setCellValue('B'.$k, MoneyHelper::toStringDecimal($montant_total_date).' €');
            $excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($style);
            $excel->getActiveSheet()->getStyle('B'.$k)->applyFromArray($stylePrix);

            $montant_total = $montant_total->add($montant_total_date);
            ++$k;
        }

        $excel->getActiveSheet()->setCellValue('B10', MoneyHelper::toStringDecimal($montant_total).' €');
        $excel->getActiveSheet()->getStyle('B10')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('B10')->applyFromArray($stylePrix);

        $j = 'C';
        foreach ($produit_quantite_total as $p_q_t) {
            $excel->getActiveSheet()->setCellValue($j.'10', $p_q_t);
            $excel->getActiveSheet()->getStyle($j.'10')->applyFromArray($style);
            ++$j;
        }

        $excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A3', $time);
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', 'Contrat de '.$appFunction->caracteres_speciaux($titre));
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A1', $mc_nom);

        // On replace le curseur sur le premier onglet
        $excel->setActiveSheetIndex(0);

        $nom_fichier = 'contrat-'.$appFunction->caracteres_speciaux($mc_nom).'-'.$appFunction->caracteres_speciaux($amapien_nom).'-'.$appFunction->caracteres_speciaux($amapien_prenom).'.xls'; // Nom du .xls

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = IOFactory::createWriter($excel, IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportContrat_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outputFileName,
            $nom_fichier
        );
    }

    #[Route('/contrat_signe/remove/{c_id}', name: 'contrat_signe_remove')]
    public function remove(int $c_id): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $c_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_REMOVE, $contrat);
        $this->em->remove($contrat);

        $this->em->flush();

        $this->addFlash(
            'flash_contract_remove_confirmation',
            'Le contrat a été correctement supprimé.'
        );

        return new RedirectToRefererResponse($this->generateUrl('contrat_signe_contrat_own_existing'));
    }

    #[Route('/contrat_signe/mc_remove/{mc_id}', name: 'contrat_signe_mc_remove')]
    public function mc_remove(int $mc_id): Response
    {
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_REMOVE, $mc);

        $this->em->remove($mc);
        $this->em->flush();

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('/contrat_signe/contrat_own_new', name: 'contrat_signe_contrat_own_new')]
    public function contrat_own_new(ModeleContratRepository $modeleContratRepository, ContratRepository $contratRepository): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var User $currentUser Type granted by permission */
        $currentUser = $this->getUser();

        /** @var ModeleContrat[] $mc_all */
        $mc_all = $modeleContratRepository
            ->getSubscribableContractForAmapien($currentUser)
        ;

        $contratValidation = $contratRepository->getToValidateForUser($currentUser);

        // Filtre les contrats sont la dernière date de livraison est passée
        $now = new Carbon();
        $mc_all = array_filter($mc_all, function (ModeleContrat $mc) use ($now) {
            $lastDate = $mc->getLastDate();
            if (null === $lastDate) {
                return false;
            }

            return $now->lessThanOrEqualTo($lastDate->getDateLivraison());
        });

        $attente = [];
        foreach ($mc_all as $mc) {
            $attente[$mc->getId()] = $mc
                ->getFerme()
                ->getAmapienAttentes()
                ->map(fn (Amapien $attente) => $attente->getUser())
                ->contains($currentUser)
            ;
        }

        // Nettoie les données de session du wizard
        $this->session_clear_contrat_wizard();

        return $this->render('contrat_signe/contrat_own_new.html.twig', [
            'mc_all' => $mc_all,
            'contratValidation' => $contratValidation,
            'attente' => $attente,
            'currentUser' => $currentUser,
        ]);
    }

    #[Route('/contrat_signe/contrat_edit/{contrat_id}', name: 'contrat_signe_contrat_edit')]
    public function contrat_edit(int $contrat_id): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);

        $contractForced = $this->request->query->has('force')
            && $this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EDIT_FORCE, $contrat)
        ;

        if (!(
            $contractForced
            || $this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EDIT, $contrat)
        )) {
            $this->exit_error();
        }

        // Met à jour la date de modification
        $contrat->setDateModification(Carbon::now());

        // Reset anciennes dates si le contrat est encore en cours de validation pour ne pas bloquer l'amapien
        if (ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE === $contrat->getState()) {
            $firstDate = $contrat->getModeleContrat()->getPremiereDateLivrableAvecDelais(Carbon::now());
            $datesLivraisons = $contrat->getModeleContrat()->getDatesOrderedAsc();
            foreach ($datesLivraisons as $datesLivraison) {
                $dateLivraisonCarbon = Carbon::instance($datesLivraison->getDateLivraison());
                if ($firstDate->gte($dateLivraisonCarbon)) {
                    foreach ($contrat->getCellulesDate($dateLivraisonCarbon) as $cellule) {
                        $cellule->setQuantite(0);
                    }
                }
            }
        }

        $referrer = $this->request->headers->get('referer', $this->generateUrl('contrat_signe_contrat_own_existing'));
        $wizard = new ContratWizard();
        $wizard->setContrat($contrat);
        $wizard->setUrlRetour($referrer);
        $wizard->setCreationFromAMAP(
            $contractForced
        );

        $this->session_store_contrat_wizard($wizard);

        return $this->redirectToRoute('contrat_signe_contrat_subscribe_1', ['modele_contrat_id' => 0]);
    }

    #[Route('/contrat_signe/contrat_new_force/{modele_contrat_id}', name: 'contrat_signe_contrat_new_force')]
    public function contrat_new_force(int $modele_contrat_id): Response
    {
        /** @var ModeleContrat $mc */
        $mc = $this->findOrExit(ModeleContrat::class, $modele_contrat_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $mc);

        /** @var Amapien[] $amapiens */
        $amapiens = $this
            ->em
            ->getRepository(Amapien::class)
            ->getAllForceableFromModeleContract($mc)
        ;

        $form = $this->createForm(ContratSigneNewForce::class, null, [
            'amapiens_choices' => $amapiens,
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contrat = Contrat::create($mc, $form->get('amapien')->getData(), Carbon::now());
            if (!$contrat->isCdp() && $contrat->getAmapien()->getUser()->getEmails()->count() > 0) {
                $contrat->setState(ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE);
            }

            $wizardDto = new ContratWizard();
            $wizardDto->setContrat($contrat);
            $wizardDto->setUrlRetour($this->session_get_home_path(self::HOME_PATH_ID));
            $wizardDto->setCreationFromAMAP(true);
            $this->session_store_contrat_wizard($wizardDto);

            return $this->redirectToRoute('contrat_signe_contrat_subscribe_1', ['modele_contrat_id' => 0]);
        }

        return $this->render('contrat_signe/new_force.html.twig', [
            'form' => $form->createView(),
            'amapiens' => $amapiens,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/contrat_subscribe_1/{modele_contrat_id}', name: 'contrat_signe_contrat_subscribe_1')]
    public function contrat_subscribe_1(int $modele_contrat_id, ContratCalculatorGenerateResponse $calculatorGenerateResponse): Response
    {
        if (0 === $modele_contrat_id) {
            $wizard = $this->session_get_contrat_wizard();
            if (false === $wizard) {
                return $this->redirectToRoute('portail_index');
            }
            $contrat = $wizard->getContrat();
            $mc = $contrat->getModeleContrat();
        } else {
            /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
            $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $modele_contrat_id);
            $wizard = new \PsrLib\DTO\ContratWizard();
            $wizard->setUrlRetour($this->generateUrl('contrat_signe_contrat_own_new'));

            $currentUser = $this->getUser();
            if (null === $currentUser) {
                $this->exit_error();
            }

            $userAmapienAmap = $currentUser
                ->getAmapienAmaps()
                ->filter(fn (Amapien $userAmapienAmap) => $userAmapienAmap->getAmap() === $mc->getLivraisonLieu()->getAmap())
                ->first()
            ;
            if (!$userAmapienAmap) {
                $this->exit_error();
            }
            $contrat = Contrat::create($mc, $userAmapienAmap, Carbon::now());
        }

        $this->checkWizardPermission($wizard, $mc);

        $dto = ContratCommandeSouscription::createFromContrat($contrat);
        $validationGroups = ['Default', 'subscription'];
        if ($wizard->isCreationFromAMAP()) {
            $validationGroups = ['Default'];
        }
        $form = $this->formFactory->create(ContratSouscriptionForm1Type::class, $dto, [
            'validation_groups' => $validationGroups,
        ]);
        $form->handleRequest($this->request);

        if (RequestHtmxUtils::isHtmxRequest($this->request)) {
            return $calculatorGenerateResponse->renderHtmxResponse($dto);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $dto->applyToContract($contrat);
            $wizard->setContrat($contrat);

            $this->session_store_contrat_wizard($wizard);

            return $this->redirectToRoute('contrat_signe_contrat_subscribe_2');
        }

        $ferme = $mc->getFerme();
        $livraison_lieu = $mc->getLivraisonLieu();

        return $this->render('contrat_signe/contrat_subscribe_1.html.twig', [
            'form' => $form->createView(),
            'contrat' => $mc,
            'ferme' => $ferme,
            'livraison_lieu' => $livraison_lieu,
            'contrat_signe' => $contrat,
            'commande' => isset($contrat) ? $contrat->getCellules()->toArray() : [],
            'url_retour' => $wizard->getUrlRetour(),
            'bypassNbLivCheck' => $wizard->isCreationFromAMAP(),
        ]);
    }

    #[Route('/contrat_signe/contrat_subscribe_2', name: 'contrat_signe_contrat_subscribe_2')]
    public function contrat_subscribe_2(ContractCalculator $contractCalculator): Response
    {
        $wizard = $this->session_get_contrat_wizard();
        if (false === $wizard) {
            return $this->redirectToRoute('contrat_signe_contrat_own_new');
        }

        $contrat = $wizard->getContrat();
        $mc = $contrat->getModeleContrat();

        $total = $contractCalculator->totalTTC($contrat);

        // Pre fill first date if no one exist
        if (0 === $contrat->getDatesReglements()->count() && 1 === $mc->getReglementNbMax()) {
            $contrat->addDatesReglement(ContratDatesReglement::build($mc->getDatesReglementOrderedAsc()->first(), $total));
        }
        // Prefill reglement type
        if (null === $contrat->getReglementType() && 1 === count($mc->getReglementType())) {
            $contrat->setReglementType($mc->getReglementType()[0]);
        }

        $isAjaxRequest = RequestHtmxUtils::isHtmxRequest($this->request);
        $form = $this->formFactory->create(ContratSouscriptionForm2Type::class, $contrat, [
            'total' => $total,
            'validation_groups' => $isAjaxRequest ? false : ['subscription_2'],
        ]);
        $form->handleRequest($this->request);
        if ($isAjaxRequest) {
            return $this->render('contrat_signe/contrat_subscribe_2_form.html.twig', [
                'form' => $form->createView(),
                'mc' => $mc,
                'total' => $total,
            ]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setContrat($contrat);
            $this->session_store_contrat_wizard($wizard);

            return $this->redirectToRoute('contrat_signe_contrat_subscribe_3');
        }

        return $this->render('contrat_signe/contrat_subscribe_2.html.twig', [
            'contrat' => $contrat,
            'mc' => $mc,
            'form' => $form->createView(),
            'url_retour' => $wizard->getUrlRetour(),
            'total' => $total,
        ]);
    }

    #[Route('/contrat_signe/contrat_subscribe_3', name: 'contrat_signe_contrat_subscribe_3')]
    public function contrat_subscribe_3(MPdfGeneration $MPdfGeneration, Email_Sender $emailSender): Response
    {
        $wizard = $this->session_get_contrat_wizard();
        if (false === $wizard) {
            return $this->redirectToRoute('contrat_signe_contrat_own_new');
        }

        $form = $this->formFactory->create(ContratSubscribe3Type::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contrat = $wizard->getContrat();
            $contractIsNew = null === $contrat->getId();

            // Vérifie que le contrat n'existe pas en BDD, par ex si on a ouvert le wizard sur deux onglets en meme temps
            // et qu'on en a complété un en premier
            if ($contractIsNew) {
                $existingContract = $this
                    ->em
                    ->getRepository(Contrat::class)
                    ->findOneBy([
                        'amapien' => $contrat->getAmapien(),
                        'modeleContrat' => $contrat->getModeleContrat(),
                    ])
                ;
                if (null !== $existingContract) {
                    $this->addFlash(
                        'notice_error',
                        'Une erreur est survenue ! Merci de recommencer.'
                    );

                    return $this->redirect($wizard->getUrlRetour());
                }
            }

            // remplace le contrat en BDD. Permis parce que pas de lien en BDD sur les contrats
            if (null !== $contrat->getId()) {
                $this->em->remove($this->em->getReference(Contrat::class, $contrat->getId()));
            }

            // Generation du fichier de contrat definitif
            $fileName = $MPdfGeneration->genererContratSigneFile($contrat);
            $pdf = new ContratPdf($fileName);
            $contrat->setPdf($pdf);

            $this->em->persist($contrat);
            $this->em->flush();

            $this->session_clear_contrat_wizard();

            if ($contrat->isCdp()) {
                if ($wizard->isCreationFromAMAP()) {
                    $this->addFlash(
                        'notice_success',
                        'Le contrat a bien été créé mais n\'est pas réglé. L\'Amapien doit se rendre dans son interface "Mes contrats existants" pour effectuer le paiement.'
                    );

                    return $this->redirect($wizard->getUrlRetour());
                }

                if ($contractIsNew) {
                    return $this->redirectToRoute('contrat_signe_redirection_cdp', ['contrat_id' => $contrat->getId()]);
                }
                $token = (new JwtToken())->buildToken('contract_edit', [
                    'contract_uuid' => $contrat->getUuid(),
                    'amapien_uuid' => $contrat->getAmapien()->getUser()->getUuid(),
                ]);

                return $this->redirect(getenv('CDP_URL_CONTRACT_NEW').'?token='.$token->toString());
            }

            if (ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE === $contrat->getState()) {
                $emailSender->envoyerContratAmapienValidationNotification($contrat);
            }

            return $this->redirect($wizard->getUrlRetour());
        }

        return $this->render('contrat_signe/contrat_subscribe_3.html.twig', [
            'url_retour' => $wizard->getUrlRetour(),
            'form' => $form->createView(),
            'wizard' => $wizard,
        ]);
    }

    #[Route('/contrat_signe/redirection_cdp/{contrat_id}', name: 'contrat_signe_redirection_cdp')]
    public function redirection_cdp(int $contrat_id): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_REDIRECT_CDP, $contrat);

        $form = $this
            ->formFactory
            ->createBuilder()
            ->add('submit', SubmitType::class, [
                'label' => 'Valider',
            ])
            ->getForm()
        ;
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = (new JwtToken())->buildToken('contract_new', [
                'contract_uuid' => $contrat->getUuid(),
                'amapien_uuid' => $contrat->getAmapien()->getUser()->getUuid(),
            ]);

            return $this->redirect(getenv('CDP_URL_CONTRACT_NEW').'?token='.$token->toString());
        }

        return $this->render('contrat_signe/redirection_cdp.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/contrat_signe/redirection_cdp_echeances/{contrat_id}', name: 'contrat_signe_redirection_cdp_echeances')]
    public function redirection_cdp_echeances(int $contrat_id): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_REDIRECT_CDP, $contrat);

        $token = (new JwtToken())->buildToken('contract_show', [
            'contract_uuid' => $contrat->getUuid(),
            'amapien_uuid' => $contrat->getAmapien()->getUser()->getUuid(),
        ]);

        return $this->redirect(getenv('CDP_URL_CONTRACT_NEW').'?token='.$token->toString());
    }

    #[Route('/contrat_signe/redirection_cdp_reglement/{contrat_id}', name: 'contrat_signe_redirection_cdp_reglement')]
    public function redirection_cdp_reglement(int $contrat_id): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_REDIRECT_CDP, $contrat);

        $token = (new JwtToken())->buildToken('contract_payment', [
            'contract_uuid' => $contrat->getUuid(),
            'amapien_uuid' => $contrat->getAmapien()->getUser()->getUuid(),
        ]);

        return $this->redirect(getenv('CDP_URL_CONTRACT_NEW').'?token='.$token->toString());
    }

    #[Route('/contrat_signe/preview_pdf', name: 'contrat_signe_preview_pdf')]
    public function preview_pdf(MPdfGeneration $MPdfGeneration): Response
    {
        $wizard = $this->session_get_contrat_wizard();
        if (false === $wizard) {
            return $this->redirectToRoute('contrat_signe_contrat_own_new');
        }

        $file = $MPdfGeneration->genererContratSigneTmp(
            $wizard->getContrat()
        );

        return BinaryFileResponseFactory::createFromTempFilePath(
            $file,
            'contrat.pdf',
            ResponseHeaderBag::DISPOSITION_INLINE
        );
    }

    #[Route('/contrat_signe/contrat_own_existing', name: 'contrat_signe_contrat_own_existing')]
    public function contrat_own_existing(): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var User $currentUser Granted by permission */
        $currentUser = $this->getUser();

        /** @var Contrat[] $contrats */
        $contrats = $this
            ->em
            ->getRepository(Contrat::class)
            ->getByUserAmapienActivated($currentUser)
        ;

        // Filtre les contrats sont la dernière date de livraison est passée
        $contrats = array_filter($contrats, fn (Contrat $contrat) => !$contrat->getModeleContrat()->isArchived());

        $contratCdp = array_reduce($contrats, fn (bool $cdp, Contrat $contrat) => $cdp || $contrat->isCdp(), false);

        return $this->render('contrat_signe/contrat_own_existing.html.twig', [
            'contrats' => $contrats,
            'currentUser' => $currentUser,
            'contratCdp' => $contratCdp,
        ]);
    }

    #[Route('/contrat_signe/contrat_own_archived', name: 'contrat_signe_contrat_own_archived')]
    public function contrat_own_archived(): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY_OWN);

        /** @var User $currentUser Granted by permission */
        $currentUser = $this->getUser();

        /** @var Contrat[] $contrats */
        $contrats = $this
            ->em
            ->getRepository(Contrat::class)
            ->getByUserAmapienActivated($currentUser)
        ;

        // Filtre les contrats sont la dernière date de livraison est passée
        $contrats = array_filter($contrats, fn (Contrat $contrat) => $contrat->getModeleContrat()->isArchived());

        return $this->render('contrat_signe/contrat_own_archived.html.twig', [
            'contrats' => $contrats,
            'currentUser' => $currentUser,
        ]);
    }

    #[Route('/contrat_signe/contrat_pdf/{contrat_id}', name: 'contrat_signe_contrat_pdf')]
    public function contrat_pdf(int $contrat_id): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);

        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_PDF, $contrat);

        $mc = $contrat->getModeleContrat();
        $amap = $mc->getLivraisonLieu()->getAmap();
        $ferme = $mc->getFerme();
        $creationDate = $contrat->getDateCreation();
        $fileName = $mc->getNom()
            .'-'
            .$contrat->getAmapien()->getUser()->getName()->getFullNameInversed()
            .'-'
            .$amap->getNom()
            .'-'
            .$ferme->getNom()
            .'-'
            .$creationDate->format('d.m.Y')
            .'.pdf'
        ;

        return BinaryFileResponseFactory::createFromFile(
            $contrat->getPdf(),
            $fileName,
        );
    }

    #[Route('/contrat_signe/contrat_paysan_archived', name: 'contrat_signe_contrat_paysan_archived')]
    public function contrat_paysan_archived(): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_PAYSAN);
        $this->disableSoftDeleteFor(Amapien::class);
        $this->session_store_home_path(self::HOME_PATH_ID);

        $currentUser = $this->getUser();

        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = SearchContratSigneState::createWithWorkflowState(ContratStatusWorkflow::STATE_VALIDATED);
        $searchForm = $this->formFactory->create(SearchContratSignePaysanType::class, $searchContratState, [
            'currentUser' => $currentUser,
            'filter_archived_state' => true,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(Contrat::class)
                ->search($searchContratState)
            ;
        }

        return $this->render('contrat_signe/display_all_paysan_archived.html.twig', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    #[Route('/contrat_signe/contrat_paysan_export', name: 'contrat_signe_contrat_paysan_export')]
    public function contrat_paysan_export(): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_ACCOUNT);

        $currentUser = $this->getUser();

        $this->redirectFromStoredGetParamIfExist('contrat_signe_paysan_export_accound');

        $searchContratState = SearchContratSigneState::createWithWorkflowState(ContratStatusWorkflow::STATE_VALIDATED);
        $searchForm = $this->formFactory->create(SearchContratSignePaysanType::class, $searchContratState, [
            'currentUser' => $currentUser,
            'filter_archived_state' => null,
            'filter_active_fermes' => true,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(Contrat::class)
                ->search($searchContratState)
            ;
        }

        return $this->render('contrat_signe/contrat_paysan_export_account.html.twig', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
        ]);
    }

    #[Route('/contrat_signe/contrat_paysan_export_date', name: 'contrat_signe_contrat_paysan_export_date')]
    public function contrat_paysan_export_date(ExcelGeneratorExportContratAccountDate $excelGeneratorExportContratAccountDate): Response
    {
        return $this->_contrat_paysan_export(
            fn (SearchContratSigneState $searchContratState, Ferme $ferme) => $excelGeneratorExportContratAccountDate->generate(
                $searchContratState,
                $ferme
            ),
            'Export_comptable_%s_%s_versiondatedepaiement.xls'
        );
    }

    #[Route('/contrat_signe/contrat_paysan_export_product_delivred', name: 'contrat_signe_contrat_paysan_export_product_delivred')]
    public function contrat_paysan_export_product_delivred(ExcelGeneratorExportContratAccountProductDeliveredQuantity $excelGeneratorExportContratAccountProductDelivered): Response
    {
        return $this->_contrat_paysan_export(
            fn (SearchContratSigneState $searchContratState, Ferme $ferme) => $excelGeneratorExportContratAccountProductDelivered->generate(
                $searchContratState,
                $ferme
            ),
            'Export_comptable_%s_%s_versionproduitslivres.xls'
        );
    }

    #[Route('/contrat_signe/contrat_paysan_export_product_ordered', name: 'contrat_signe_contrat_paysan_export_product_ordered')]
    public function contrat_paysan_export_product_ordered(ExcelGeneratorExportContratAccountProductOrderedQuantity $excelGeneratorExportContratAccountProductOrdered): Response
    {
        return $this->_contrat_paysan_export(
            fn (SearchContratSigneState $searchContratState, Ferme $ferme) => $excelGeneratorExportContratAccountProductOrdered->generate(
                $searchContratState,
                $ferme
            ),
            'Export_comptable_%s_%s_versionproduitscommandes.xls'
        );
    }

    private function _contrat_paysan_export(\Closure $fileGenerationCallback, string $exportFileNameTemplate): Response
    {
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_PAYSAN);
        $this->disableSoftDeleteFor(Amapien::class);

        $searchContratState = SearchContratSigneState::createWithWorkflowState(ContratStatusWorkflow::STATE_VALIDATED);
        $searchForm = $this->formFactory->create(SearchContratSignePaysanType::class, $searchContratState, [
            'currentUser' => $this->getUser(),
            'filter_archived_state' => null,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        /** @var User $currentUser Granted by permission */
        $currentUser = $this->getUser();
        Assertion::true($currentUser->isPaysan());

        $ferme = $currentUser->getFermesAsPaysan()[0];
        $fileName = $fileGenerationCallback(
            $searchContratState,
            $ferme
        );

        return BinaryFileResponseFactory::createFromTempFilePath(
            $fileName,
            sprintf(
                $exportFileNameTemplate,
                $ferme->getNom(),
                Carbon::now()->format('Y_m_d')
            )
        );
    }

    #[Route('/contrat_signe/accueil_admin', name: 'contrat_signe_accueil_admin')]
    public function accueil_admin(): Response
    {
        return $this->accueil_admin_internal(false);
    }

    #[Route('/contrat_signe/accueil_admin_archive', name: 'contrat_signe_accueil_admin_archive')]
    public function accueil_admin_archive(): Response
    {
        return $this->accueil_admin_internal(true);
    }

    private function accueil_admin_internal(bool $displayArchive): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_ADMIN);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = new SearchContratSigneState();
        $searchForm = $this->formFactory->create(SearchContratSigneType::class, $searchContratState, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $contrats = $this
            ->em
            ->getRepository(Contrat::class)
            ->search($searchContratState)
        ;
        $contrats = array_filter(
            $contrats,
            fn (Contrat $contrat) => $contrat->getModeleContrat()->isArchived() === $displayArchive
        );

        return $this->render('contrat_signe/display_all.html.twig', [
            'pageTitle' => $displayArchive ? 'Gestion des contrats archivés' : 'Gestion des contrats signés',
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
            'showOtherButton' => !$displayArchive,
            'homePath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/accueil_amap', name: 'contrat_signe_accueil_amap')]
    public function accueil_amap(): Response
    {
        return $this->accueil_amap_internal(false);
    }

    #[Route('/contrat_signe/accueil_amap_archive', name: 'contrat_signe_accueil_amap_archive')]
    public function accueil_amap_archive(): Response
    {
        return $this->accueil_amap_internal(true);
    }

    private function accueil_amap_internal(bool $displayArchive): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_AMAP);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(SearchContratSigneAmapType::class, null, [
            'amap_choices' => $currentUser->getAdminAmaps(),
            'filter_archived_state' => $displayArchive,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $searchContratState = $searchForm->getData();
        $contrats = $this
            ->em
            ->getRepository(Contrat::class)
            ->search($searchContratState)
        ;
        $contrats = array_filter(
            $contrats,
            fn (Contrat $contrat) => $contrat->getModeleContrat()->isArchived() === $displayArchive
        );

        return $this->render('contrat_signe/display_all_amap.html.twig', [
            'pageTitle' => $displayArchive ? 'Gestion des contrats archivés' : 'Gestion des contrats signés',
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
            'showOtherButton' => !$displayArchive,
            'homePath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/accueil_ref_produit', name: 'contrat_signe_accueil_ref_produit')]
    public function accueil_ref_produit(): Response
    {
        return $this->accueil_ref_produit_internal(false);
    }

    #[Route('/contrat_signe/accueil_ref_produit_archive', name: 'contrat_signe_accueil_ref_produit_archive')]
    public function accueil_ref_produit_archive(): Response
    {
        return $this->accueil_ref_produit_internal(true);
    }

    private function accueil_ref_produit_internal(bool $displayArchive): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(SearchContratSigneAmapType::class, null, [
            'amap_choices' => $currentUser->getAmapsAsRefProduit(),
            'restrict_fermes_ref' => $currentUser,
            'filter_archived_state' => $displayArchive,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $searchContratState = $searchForm->getData();

        $contrats = $this
            ->em
            ->getRepository(Contrat::class)
            ->search($searchContratState)
        ;
        $contrats = array_filter(
            $contrats,
            fn (Contrat $contrat) => $contrat->getModeleContrat()->isArchived() === $displayArchive
        );

        return $this->render('contrat_signe/display_all_amap.html.twig', [
            'pageTitle' => $displayArchive ? 'Gestion des contrats archivés' : 'Gestion des contrats signés',
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
            'showOtherButton' => !$displayArchive,
            'homePath' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/accueil_paysan', name: 'contrat_signe_accueil_paysan')]
    public function accueil_paysan(): Response
    {
        $this->disableSoftDeleteFor(Amapien::class);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_LIST_PAYSAN);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('contrat_signe');

        $searchContratState = SearchContratSigneState::createWithWorkflowState(ContratStatusWorkflow::STATE_VALIDATED);
        $searchForm = $this->formFactory->create(SearchContratSignePaysanWithPaiementStatusType::class, $searchContratState, [
            'currentUser' => $this->getUser(),
            'filter_archived_state' => false,
        ]);
        $searchForm->handleRequest($this->request);

        $contrats = [];
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $contrats = $this
                ->em
                ->getRepository(Contrat::class)
                ->search($searchContratState)
            ;
        }

        return $this->render('contrat_signe/display_all_paysan.html.twig', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'searchContratState' => $searchContratState,
            'showOtherButton' => true,
        ]);
    }

    #[Route('/contrat_signe/delai/{mc_id}', name: 'contrat_signe_delai')]
    public function delai(int $mc_id): Response
    {
        $mc = $this->findOrExit(ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_CHANGE_DELAY, $mc);

        $retUrl = $this->session_get_home_path(self::HOME_PATH_ID);

        $form = $this->formFactory->create(ModeleContratDelaiType::class, $mc);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('notice_success', 'Le délai a bien été modifié');

            return $this->redirect($retUrl);
        }

        return $this->render('contrat_signe/delai.html.twig', [
            'form' => $form->createView(),
            'retUrl' => $retUrl,
            'mc' => $mc,
        ]);
    }

    #[Route('/contrat_signe/paiement_rappel', name: 'contrat_signe_paiement_rappel')]
    public function paiement_rappel(Email_Sender $emailSender): Response
    {
        $contrats = $this->parseContratPaiement();

        foreach ($contrats as $contrat) {
            if ($contrat->getAmapien()->isDeleted()) {
                $this->addFlash(
                    'notice_error',
                    sprintf("Impossible d'envoyer les rappels car l'amapien \"%s\" car il a été supprimé", $contrat->getAmapien()->getUser()->getName())
                );

                return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
            }
        }

        foreach ($contrats as $contrat) {
            $emailSender->envoyerContratRappelPaiement($contrat);
        }

        $this->addFlash('notice_success', 'Les rappels de paiement ont bien été envoyés');

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('/contrat_signe/paiement_statut', name: 'contrat_signe_paiement_statut')]
    public function paiement_statut(): Response
    {
        $contrats = $this->parseContratPaiement();
        $form = $this->formFactory->create(ContratPaiementStatutType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $statut = $form->get('contratPaiementStatut')->getData();
            foreach ($contrats as $contrat) {
                $contrat->setContratPaiementStatut($statut);
            }
            $this->em->flush();
            $this->addFlash('notice_success', 'Les statuts de paiement ont bien été modifiés');

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('contrat_signe/paiement_statut.html.twig', [
            'form' => $form->createView(),
            'mc' => $contrats[0]->getModeleContrat(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_signe/amapien_validation/{contrat_id}', name: 'contrat_signe_amapien_validation')]
    public function amapien_validation(int $contrat_id, ContratStatusWorkflow $contratStatusWorkflow): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractAmapienValidationVoter::ACTION_CONTRACT_SIGNED_AMAPIEN_VALIDATION, $contrat);

        $form = $this->formFactory->create(ContratSubscribe3Type::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contratStatusWorkflow->apply($contrat, ContratStatusWorkflow::TRANSITION_AMAPIEN_VALIDATION);
            $this->em->flush();

            $this->addFlash('success', 'Le contrat a bien été validé');

            return $this->redirectToRoute('contrat_signe_contrat_own_new');
        }

        return $this->render('contrat_signe/contrat_amapien_validation.html.twig', [
            'form' => $form->createView(),
            'contrat' => $contrat,
        ]);
    }

    #[Route('/contrat_signe/contrat_deplacement/{contrat_id}', name: 'contrat_signe_contrat_deplacement')]
    public function contrat_deplacement(int $contrat_id, ContratCalculatorGenerateResponse $calculatorGenerateResponse): Response
    {
        /** @var Contrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\Contrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_MOVE_DATE, $contrat);

        $returnUrl = $this->request->query->get('ret');
        if (null === $returnUrl) {
            $returnUrl = $this->session_get_home_path(self::HOME_PATH_ID);
        }

        $dto = ContratCommandeDeplacement::createFromContrat($contrat);
        $form = $this->formFactory->create(ContratSouscriptionForm1Type::class, $dto, [
            'validation_groups' => ['Default', 'move'],
        ]);
        $form->handleRequest($this->request);

        if (RequestHtmxUtils::isHtmxRequest($this->request)) {
            return $calculatorGenerateResponse->renderHtmxResponse($dto);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $dto->applyToContract($contrat);
            $this->em->flush();

            $this->addFlash('notice_success', 'Le déplacement a bien été effectué');

            return $this->redirect($returnUrl);
        }

        return $this->render('contrat_signe/contrat_deplacement.html.twig', [
            'form' => $form->createView(),
            'mc' => $contrat->getModeleContrat(),
            'contrat_signe' => $contrat,
            'url_retour' => $returnUrl,
        ]);
    }

    /**
     * @return Contrat[]
     */
    private function parseContratPaiement(): array
    {
        $this->disableSoftDeleteFor(Amapien::class);
        $contrats = $this->parseTableSelectQueryParams(Contrat::class);

        foreach ($contrats as $contrat) {
            $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_MANAGE_PAYMENT, $contrat->getModeleContrat());
        }

        return $contrats;
    }

    private function session_store_contrat_wizard(ContratWizard $wizard): void
    {
        $this->sfSession->set(
            'c_wizard',
            $this->serializer->serialize($wizard, 'json', [
                'groups' => 'wizardContract',
                'json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION,
            ])
        );
    }

    private function session_get_contrat_wizard(): false|ContratWizard
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('c_wizard', ''), ContratWizard::class, 'json', [
                    'groups' => 'wizardContract',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function session_clear_contrat_wizard(): void
    {
        $this->sfSession->set('c_wizard', '');
    }

    private function session_get_contrat_move(): false|ModeleContratDeplacement
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('c_move', ''), ModeleContratDeplacement::class, 'json')
            ;
        } catch (Exception) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function session_store_contrat_move(ModeleContratDeplacement $mcd): void
    {
        $this->sfSession->set(
            'c_move',
            $this->serializer->serialize($mcd, 'json')
        );
    }

    private function checkWizardPermission(?ContratWizard $wizard, ModeleContrat $modeleContrat): void
    {
        if (null === $wizard) {
            $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_SUBSCRIBE, $modeleContrat);

            return;
        }
        $contrat = $wizard->getContrat();
        if (null === $contrat) {
            $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_SUBSCRIBE, $modeleContrat);

            return;
        }

        if ($wizard->isCreationFromAMAP()) {
            $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_SUBSCRIBE_FORCE, $contrat->getModeleContrat());

            return;
        }

        $this->denyAccessUnlessGranted(ContractVoter::ACTION_CONTRACT_SIGNED_EDIT, $contrat);
    }
}
