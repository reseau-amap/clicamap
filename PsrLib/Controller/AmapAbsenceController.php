<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\Security\Voters\AmapVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AmapAbsenceController extends AbstractController
{
    #[Route('amap_absence/liste/{amapId}', name: 'amap_absence_liste')]
    public function liste(int $amapId): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_MANAGE_ABS, $amap);
        $absences = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapAbsence::class)
            ->findBy([
                'amap' => $amap,
            ])
        ;

        return $this->render('amap_absence/liste.html.twig', [
            'absences' => $absences,
            'amap' => $amap,
        ]);
    }

    /**
     * @param null $absenceId
     */
    #[Route('amap_absence/form/{amapId}/{absenceId}', name: 'amap_absence_form')]
    public function form(int $amapId, int $absenceId = null): Response
    {
        if (null === $absenceId) {
            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);
            $absence = new \PsrLib\ORM\Entity\AmapAbsence($amap);
        } else {
            /** @var \PsrLib\ORM\Entity\AmapAbsence $absence */
            $absence = $this->findOrExit(\PsrLib\ORM\Entity\AmapAbsence::class, $absenceId);
        }

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_MANAGE_ABS, $absence->getAmap());

        $form = $this
            ->formFactory
            ->create(\PsrLib\Form\AmapAbsenceType::class, $absence)
        ;

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($absence);
            $this->em->flush();

            return $this->redirectToRoute('amap_absence_liste', [
                'amapId' => $absence->getAmap()->getId(),
            ]);
        }

        return $this->render('amap_absence/form.html.twig', [
            'absence' => $absence,
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap_absence/supprimer/{absenceId}', name: 'amap_absence_supprimer')]
    public function supprimer(int $absenceId): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapAbsence $absence */
        $absence = $this->findOrExit(\PsrLib\ORM\Entity\AmapAbsence::class, $absenceId);
        $amap = $absence->getAmap();

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_MANAGE_ABS, $amap);

        $this->em->remove($absence);
        $this->em->flush();

        return $this->redirectToRoute('amap_absence_liste', [
            'amapId' => $amap->getId(),
        ]);
    }
}
