<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Controller;

use PsrLib\DTO\EvenemntAbonnableDTO;
use PsrLib\DTO\SearchEvenementAbonnable;
use PsrLib\ORM\Entity\EvenementAbonnementWithRelatedEntity;
use PsrLib\Services\EvenementAbonnableRepository;
use PsrLib\Services\EvenementAbonnementButtonRender;
use PsrLib\Services\Security\Voters\EvenementRemoveVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class EvenementAbonnementsController extends AbstractController
{
    #[Route('evenement_abonnement', name: 'evenement_abonnement')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAbonnementListVoter::ACTION_EVENEMENT_ABONNEMENT_LIST);

        $this->redirectFromStoredGetParamIfExist('evenement_abonnement');
        $searchForm = $this->createForm(\PsrLib\Form\SearchEvenementAbonnableType::class);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        return $this->render('evenement_abonnements/index.html.twig', [
            'searchForm' => $searchForm->createView(),
        ]);
    }

    #[Route('evenement_abonnement/search', name: 'evenement_abonnement_search')]
    public function search(EvenementAbonnableRepository $evenementAbonnableRepository, SerializerInterface $serializer): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAbonnementListVoter::ACTION_EVENEMENT_ABONNEMENT_LIST);
        $this->denyUnlessXmlHttpRequest();

        $search = $this->parseQuerySearchDatatable(SearchEvenementAbonnable::class);
        $searchForm = $this->createForm(\PsrLib\Form\SearchEvenementAbonnableType::class, $search);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $currentUser = $this->getUser();

        return new Response($serializer->serialize(new \PsrLib\DTO\DatatableAjaxResponseDTO(
            (int) $this->request->query->get('draw', 0),
            $evenementAbonnableRepository->searchCountResults($currentUser, $search),
            $evenementAbonnableRepository->search($currentUser, $search)
        ), 'json', ['groups' => ['datatable'], 'current_user' => $this->getUser()]));
    }

    #[Route('evenement_abonnement/add', name: 'evenement_abonnement_add', methods: 'POST')]
    public function add(EvenementAbonnementButtonRender $evenementAbonnementButtonRender): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAbonnementListVoter::ACTION_EVENEMENT_ABONNEMENT_LIST);

        $type = \PsrLib\Enum\EvenementType::tryFrom($this->request->request->get('type'));
        if (null === $type) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }

        $related = $this->em->find($type->getRelatedEntityClass(), $this->request->request->get('id'));
        if (null === $related) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }

        $abonnementEntityClass = $type->getRelatedAbonnementEntityClass();
        if (null === $abonnementEntityClass) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }
        $abonnement = $abonnementEntityClass::create($this->getUser(), $related);
        $this->em->persist($abonnement);
        $this->em->flush();

        return new Response($evenementAbonnementButtonRender->renderButton($this->getUser(), $related));
    }

    #[Route('evenement_abonnement/remove/{id}', name: 'evenement_abonnement_remove', methods: 'POST')]
    public function remove(int $id, EvenementAbonnementButtonRender $evenementAbonnementButtonRender): ?Response
    {
        $abonnement = $this->em->find(\PsrLib\ORM\Entity\EvenementAbonnement::class, $id);
        $this->denyAccessUnlessGranted(EvenementRemoveVoter::ACTION_EVENEMENT_ABONNEMENT_REMOVE, $abonnement);

        if (!($abonnement instanceof EvenementAbonnementWithRelatedEntity)) {
            $this->exit_error();

            return null;
        }

        $related = $abonnement->getRelated();

        $this->em->remove($abonnement);
        $this->em->flush();

        return new Response($evenementAbonnementButtonRender->renderButton($this->getUser(), $related));
    }

    #[Route('evenement_abonnement/keyword_suggestion', name: 'evenement_abonnement_keyword')]
    public function keyword_suggestion(SerializerInterface $serializer, EvenementAbonnableRepository $evenementAbonnableRepository): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAbonnementListVoter::ACTION_EVENEMENT_ABONNEMENT_LIST);
        $search = new SearchEvenementAbonnable();
        $search->setKeyword($this->request->query->get('term', ''));
        $search->setMaxResults('10');

        $searchResponse = $evenementAbonnableRepository->search($this->getUser(), $search);

        $dtoResponse = array_map(fn (EvenemntAbonnableDTO $abonnable) => new \PsrLib\DTO\AutocompleteResponseItemDTO($abonnable->getNom(), $abonnable->getNom()), $searchResponse);

        return new Response($serializer->serialize($dtoResponse, 'json'));
    }
}
