<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PsrLib\Services\AppFunction;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Security\Voters\FermeProduitVoter;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    /**
     * Afficher les produits d'une ferme sélectionnée.
     */
    #[Route('/produit/accueil/{ferme_id}', name: 'produit_accueil')]
    public function accueil(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        return $this->render('product/index.html.twig', [
            'ferme' => $ferme,
        ]);
    }

    /**
     * Créer / éditer un produit.
     */
    #[Route('/produit/informations/{ferme_id}/{produit_id}', name: 'produit_informations')]
    public function informations(int $ferme_id, int $produit_id = null): Response
    {
        if (null === $produit_id) {
            /** @var \PsrLib\ORM\Entity\Ferme $ferme */
            $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
            $produit = new \PsrLib\ORM\Entity\FermeProduit();
            $produit->setFerme($ferme);
        } else {
            /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
            $produit = $this->findOrExit(\PsrLib\ORM\Entity\FermeProduit::class, $produit_id);
            $ferme = $produit->getFerme();
        }

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $user = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\FermeProduitType::class, $produit, [
            'enabled_state_disabled' => ($user->isRefProduitOfFerme($ferme) || $user->isAmapAdmin()) && !$user->isPaysanOf($ferme),
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isAdd = null === $produit->getId();

            if ($isAdd) {
                $this->em->persist($produit);
            }
            $this->em->flush();

            if ($isAdd) {
                $this->addFlash(
                    'notice_success',
                    'Produit ajouté avec succès.'
                );
            } else {
                $this->addFlash(
                    'notice_success',
                    'Produit modifié avec succès.'
                );
            }

            return $this->redirectToRoute('produit_accueil', ['ferme_id' => $ferme->getId()]);
        }

        return $this->render('product/informations.html.twig', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Télécharger les produits d'une ferme.
     */
    #[Route('/produit/download/{ferme_id}', name: 'produit_download')]
    public function download(int $ferme_id, Spreadsheet $excel, AppFunction $appFunction): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $time = 'Extrait le '.date('d/m/Y H:m:s');

        // Onglet 1
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT], ];

        $excel->getActiveSheet()->setCellValue('A3', 'Type de production');
        $excel->getActiveSheet()->setCellValue('B3', 'Nom');
        $excel->getActiveSheet()->setCellValue('C3', 'Conditionnement');
        $excel->getActiveSheet()->setCellValue('D3', 'Prix');
        $excel->getActiveSheet()->setCellValue('E3', 'Régularisation de poids ?');

        $excel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);

        $i = 4;

        /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
        foreach ($ferme->getProduits() as $produit) {
            $excel->getActiveSheet()->setCellValue('A'.$i, ucfirst($produit->getTypeProduction()->getNom()));
            $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($produit->getNom()));
            $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, $produit->getConditionnement());
            $excel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('D'.$i, $produit->getPrix());
            $excel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('E'.$i, $produit->isRegulPds() ? 'Oui' : 'Non');
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A1', $time);

        $nom_fichier = $appFunction->caracteres_speciaux($ferme->getNom()).'_produits.xls'; // save our workbook as this file name

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportProduit_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    /**
     * Supprimer un produit.
     */
    #[Route('/produit/remove/{produit_id}', name: 'produit_remove')]
    public function remove(int $produit_id): Response
    {
        /** @var \PsrLib\ORM\Entity\FermeProduit $produit */
        $produit = $this->findOrExit(\PsrLib\ORM\Entity\FermeProduit::class, $produit_id);
        $ferme = $produit->getFerme();
        $this->denyAccessUnlessGranted(FermeProduitVoter::ACTION_FERME_PRODUCT_DELETE, $produit);

        $this->em->remove($produit);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Produit supprimé avec succès.'
        );

        return $this->redirectToRoute('produit_accueil', ['ferme_id' => $ferme->getId()]);
    }
}
