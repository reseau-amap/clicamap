<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Controller;

use PsrLib\Form\EvenementCreationSelectTypeType;
use PsrLib\ORM\Entity\Evenement;
use PsrLib\Services\EntityBuilder\EvenementCreationTypeAccessibleUserFactory;
use PsrLib\Services\EvenementRepositoryUserAbonnement;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EvenementController extends AbstractController
{
    #[Route('evenement', name: 'evenements')]
    public function index(EvenementRepositoryUserAbonnement $evenementRepository): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementListVoter::ACTION_EVENEMENT_LIST);

        $userEvts = $evenementRepository->getForUser($this->getUser());

        return $this->render('evenement/index.html.twig', [
            'evts' => $userEvts,
        ]);
    }

    #[Route('evenement/display/{id}', name: 'evenements_display')]
    public function display(int $id): Response
    {
        $evt = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementDisplayVoter::ACTION_EVENEMENT_DISPLAY, $evt);

        return $this->render('evenement/display.html.twig', [
            'evt' => $evt,
        ]);
    }

    #[Route('evenement/add_1', name: 'evenements_add_1')]
    public function add_1(EvenementCreationTypeAccessibleUserFactory $evenementCreationTypeAccessibleUserFactory): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAddVoter::ACTION_EVENEMENT_ADD);

        $redirectToStep2WithChoice = function (\PsrLib\Enum\EvenementType $type): RedirectResponse {
            return new RedirectResponse($this->generateUrl('evenements_add_2', [
                'type' => $type->value,
            ]));
        };

        $typeChoices = $evenementCreationTypeAccessibleUserFactory->getTypeAccessibleForUser($this->getUser());
        if (1 === count($typeChoices)) {
            return $redirectToStep2WithChoice($typeChoices[0]);
        }

        $form = $this
            ->createForm(EvenementCreationSelectTypeType::class, null, [
                'type_choice' => $typeChoices,
            ])
        ;

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $redirectToStep2WithChoice($form->get('type')->getData());
        }

        return $this->render('evenement/add_1.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('evenement/add_2', name: 'evenements_add_2')]
    public function add_2(EvenementCreationTypeAccessibleUserFactory $evenementCreationTypeAccessibleUserFactory): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementAddVoter::ACTION_EVENEMENT_ADD);

        $type = \PsrLib\Enum\EvenementType::tryFrom($this->request->get('type'));
        if (!in_array($type, $evenementCreationTypeAccessibleUserFactory->getTypeAccessibleForUser($this->getUser()))) {
            return $this->redirectToRoute('evenements');
        }

        $currentUser = $this->getUser();
        $evtClass = $type->getRelatedEvtClass();
        $evt = new $evtClass($currentUser);

        $form = $this
            ->createForm(\PsrLib\Form\EvenementType::class, $evt, [
                'current_user' => $currentUser,
            ])
        ;

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($evt);
            $this->handleUploads($form, $evt);
            $this->em->flush();

            $this->addFlash('success', "L'actualité a bien été ajoutée");

            return $this->redirectToRoute('evenements');
        }

        return $this->render('evenement/add_2.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('evenement/remove/{id}', name: 'evenements_remove', methods: 'POST')]
    public function remove(int $id): Response
    {
        $evt = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementEditVoter::ACTION_EVENEMENT_EDIT, $evt);

        $this->em->remove($evt);
        $this->em->flush();

        $this->addFlash('success', 'Actualité supprimée');

        return $this->redirectToRoute('evenements');
    }

    #[Route('evenement/edit/{id}', name: 'evenements_edit')]
    public function edit(int $id): Response
    {
        $evt = $this->findOrExit(\PsrLib\ORM\Entity\Evenement::class, $id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\EvenementEditVoter::ACTION_EVENEMENT_EDIT, $evt);

        $form = $this
            ->createForm(\PsrLib\Form\EvenementType::class, $evt, [
                'current_user' => $this->getUser(),
            ])
        ;

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($evt);
            $this->handleUploads($form, $evt);
            $this->em->flush();

            $this->addFlash('success', 'L\'actualité a bien été modifiée');

            return $this->redirectToRoute('evenements');
        }

        return $this->render('evenement/edit.html.twig', [
            'form' => $form->createView(),
            'evt' => $evt,
        ]);
    }

    private function handleUploads(\Symfony\Component\Form\FormInterface $form, Evenement $evenement): void
    {
        $uploadedImg = $form->get('img')->getData();
        if (null !== $uploadedImg) {
            $logo = $this
                ->uploader
                ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\EvenementImg::class)
            ;
            $evenement->setImg($logo);
        }

        $uploadedPj = $form->get('pj')->getData();
        if (null !== $uploadedPj) {
            $logo = $this
                ->uploader
                ->uploadFile($uploadedPj, \PsrLib\ORM\Entity\Files\EvenementPj::class)
            ;
            $evenement->setPj($logo);
        }
    }
}
