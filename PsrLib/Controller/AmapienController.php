<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PsrLib\DTO\ContratCelluleContainer;
use PsrLib\DTO\SearchAmapienAmapState;
use PsrLib\DTO\SearchAmapienState;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapienInvitation;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Password\PasswordEncoder;
use PsrLib\Services\Password\PasswordGenerator;
use PsrLib\Services\Security\Voters\AmapienVoter;
use PsrLib\Services\Security\Voters\AmapVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AmapienController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_amapien';

    public function __construct(
        private readonly Spreadsheet $excel
    ) {
    }

    /**
     * Accueil / Moteur de recherche.
     */
    #[Route('amapien/search_amap', name: 'amapien_search_amap')]
    public function search_amap(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_LIST_AMAPIENS);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('amapien');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienAmapType::class, null, [
            'currentUser' => $this->getUser(),
            'amaps' => $this->getUser()->getAdminAmaps(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));
        $amapienSearchState = $searchForm->getData();

        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->search($amapienSearchState)
        ;

        return $this->render('amapien/display_all.html.twig', [
            'amapiens' => $amapiens,
            'searchForm' => $searchForm->createView(),
            'amapienSearchState' => $amapienSearchState,
            'downloadPath' => 'amapien/telecharger_amapiens_amap?'.$this->request->getQueryString(),
        ]);
    }

    /**
     * Accueil / Moteur de recherche.
     */
    #[Route('amapien/search_admin', name: 'amapien_search_admin')]
    public function search_admin(): Response
    {
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_LIST);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('amapien');
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $amapienSearchState = $searchForm->getData();
        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->search($amapienSearchState)
        ;

        return $this->render('amapien/display_all.html.twig', [
            'amapiens' => $amapiens,
            'searchForm' => $searchForm->createView(),
            'amapienSearchState' => $amapienSearchState,
            'downloadPath' => 'amapien/telecharger_amapiens?'.$this->request->getQueryString(),
        ]);
    }

    /**
     * Afficher le détail de la fiche amapien.
     */
    #[Route('amapien/display/{amapien_id}', name: 'amapien_display')]
    public function display(int $amapien_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);

        // Security
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_DISPLAY_PROFIL, $amapien);

        return $this->render('amapien/display.html.twig', [
            'amapien' => $amapien,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Afficher le détail de la fiche amapien.
     */
    #[Route('amapien/liste_attente/{amapien_id}', name: 'amapien_liste_attente')]
    public function liste_attente(int $amapien_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $form = $this->formFactory->create(\PsrLib\Form\AmapienListeAttente::class, $amapien);

        // Force submit on empty list
        if ('POST' === $this->request->getMethod()) {
            $form->submit($this->request->request->get('amapien_liste_attente', null));
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La liste d\'attente de l\'amapien a été mise à jour avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        /** @var \PsrLib\ORM\Entity\Ferme[] $amapFermes */
        $amapFermes = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class)->getFromAmap($amapien->getAmap());

        return $this->render('amapien/liste_attente.html.twig', [
            'form' => $form->createView(),
            'amapien' => $amapien,
            'amapFermes' => $amapFermes,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Calendrier de livraisons.
     */
    #[Route('amapien/calendrier/{amapien_id}/{an}/{mois}', name: 'amapien_calendrier')]
    public function calendrier(\CI_Calendar $calendar, int $amapien_id = null, string $an = null, string $mois = null): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);

        // Sécurité
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_DISPLAY_PROFIL, $amapien);

        // PRÉFÉRENCES DU CALENDRIER -------------------------------------------
        $prefs = ['start_day' => 'monday',
            'day_type' => 'long',
            'show_next_prev' => true,
            'next_prev_url' => site_url('amapien/calendrier/'.$amapien_id), ]; // Le zéro est pour $date_id

        $prefs['template'] = ' 
      {table_open}<table class="table"  border="0" cellpadding="0" cellspacing="0">{/table_open}

      {heading_row_start}<tr>{/heading_row_start}

      {heading_previous_cell}<th><a href="{previous_url}"><i class="glyphicon glyphicon-menu-left"></i></a></th>{/heading_previous_cell}
      {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
      {heading_next_cell}<th><a href="{next_url}"><i class="glyphicon glyphicon-menu-right"></i></a></th>{/heading_next_cell}

      {heading_row_end}</tr>{/heading_row_end}

      {week_row_start}<tr>{/week_row_start}
      {week_day_cell}<td>{week_day}</td>{/week_day_cell}
      {week_row_end}</tr>{/week_row_end}

      {cal_row_start}<tr>{/cal_row_start}
      {cal_cell_start}<td>{/cal_cell_start}
      {cal_cell_start_today}<td>{/cal_cell_start_today}
      {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

      {cal_cell_content}<a href="{content}" data-toggle="modal" data-target="#modal_{day}"><button type=button class="btn btn-primary btn-xs">{day}</button></a>{/cal_cell_content}

      {cal_cell_no_content}{day}{/cal_cell_no_content}

      {cal_cell_blank}&nbsp;{/cal_cell_blank}

      {cal_cell_other}{day}{/cal_cel_other}

      {cal_cell_end}</td>{/cal_cell_end}
      {cal_cell_end_today}</td>{/cal_cell_end_today}
      {cal_cell_end_other}</td>{/cal_cell_end_other}
      {cal_row_end}</tr>{/cal_row_end}

      {table_close}</table>{/table_close}
      ';

        $calendar->initialize($prefs);
        $data = [];

        if (!$an) {
            $data['an'] = date('Y');
        } else {
            $data['an'] = $an;
        }
        if (!$mois) {
            $data['mois'] = date('m');
        } else {
            $data['mois'] = $mois;
        }

        $data['liens'] = null;

        // ---------------------------------------------------------------------
        // TOUTES LES DATES DE CONTRATS VIERGES (MC) LIÉS À UNE AMAPIEN --------
        $amap = $amapien->getAmap();

        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $date_all */
        $date_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->getByAmap($amap)
        ;
        if (count($date_all) > 0) {
            // On dédoublonne les dates ------------------------------------------
            $amapien_date_all = [];
            foreach ($date_all as $date) {
                $amapien_date_all[] = $date->getDateLivraison()->format('Y-m-d');
            }
            $amapien_date_all = array_unique($amapien_date_all);

            // Liens du calendrier et infos des Modals ---------------------------
            $data['liens'] = [];

            /** @var \PsrLib\ORM\Repository\ModeleContratRepository $modeleContratRepo */
            $modeleContratRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ;

            /** @var \PsrLib\ORM\Repository\ContratCelluleRepository $contratCelluleRepo */
            $contratCelluleRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)
            ;

            foreach ($amapien_date_all as $a_d) {
                // Année
                $a_d_an = substr($a_d, 0, 4);
                // Mois
                $a_d_mois = substr($a_d, 5, 2);

                // L'année et le mois du contrat correspondent avec l'année et le mois courant du calendrier
                if ($data['an'] == $a_d_an
                    && $data['mois'] == $a_d_mois
                ) {
                    $jour = substr($a_d, -2);
                    // On enlève le premier zéro du jour s'il y en a un --------------
                    if (0 == substr($jour, 0, 1)) {
                        $jour = substr($jour, 1, 2);
                    }

                    // On place un lien sur le jour du calendrier --------------------
                    $data['livraisons'][$jour] = [];

                    // Tableaux en fonction de l'amapien et de la date ---------------
                    // Tableau des contrats vierges : mc_id , mc_nom , lieu (nom ; adr ; cp ; ville)
                    $data['livraisons'][$jour]['mc'] = $modeleContratRepo
                        ->getFromAmapienDate($amapien, $a_d)
                    ;
                    $data['livraisons'][$jour]['cellules'] = new ContratCelluleContainer($contratCelluleRepo->getFromAmapienDateForValidatedContract($amapien, $a_d));

                    if (count($data['livraisons'][$jour]['mc']) > 0
                        && $data['livraisons'][$jour]['cellules']->length() > 0) {
                        $data['liens'][$jour] = '#';
                    }
                }
            }
        }

        $data['calendrier'] = $calendar
            ->generate(
                $data['an'],
                $data['mois'],
                $data['liens']
            )
        ;
        $data['amapien'] = $amapien;

        return $this->render('amapien/calendrier.html.twig', $data);
    }

    #[Route('amapien/creation_verification/{amap_id}', name: 'amapien_creation_verification')]
    public function creation_verification(int $amap_id, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE_AMAPIENS, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\UserVerificationType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ?\PsrLib\ORM\Entity\User $user */
            $user = $form->get('user')->getData();
            if (null === $user) {
                return $this->render('amapien/creation_verification.twig', [
                    'form' => $form->createView(),
                    'amap' => $amap,
                    'warning_not_found' => true,
                    'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
                ]);
            }

            $amapien = $user->getUserAmapienAmapForAmap($amap);
            if (null === $amapien) {
                $invitation = new \PsrLib\ORM\Entity\AmapienInvitation($user, $amap);
                $this->em->persist($invitation);
                $this->em->flush();
                $this->addFlash(
                    'notice_success',
                    'L\'amapien existe déjà sur clic\'amap : un email lui a été envoyé pour qu\'il rejoigne votre AMAP'
                );
                $emailSender->envoyerInvitationAmap($invitation);

                return $this->redirectToRoute('amapien_creation_verification', ['amap_id' => $amap->getId()]);
            }

            $this->addFlash('notice_error', 'L\'amapien est déjà présent dans votre AMAP.');

            return $this->redirectToRoute('amapien_creation_verification', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amapien/creation_verification.twig', [
            'form' => $form->createView(),
            'amap' => $amap,
            'warning_not_found' => false,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amapien/verification_validation/{token_id}', name: 'amapien_verification_validation')]
    public function verification_validation(string $token_id): Response
    {
        $flashSuccess = fn (AmapienInvitation $invitation) => $this->addFlash(
            'notice_success',
            sprintf("Vous avez bien été ajouté à l'AMAP %s.", $invitation->getAmap()->getNom())
        );

        $invitation = $this->em->getRepository(\PsrLib\ORM\Entity\AmapienInvitation::class)->getByToken($token_id);
        if (null === $invitation || !$invitation->getToken()->isValid()) {
            $this->addFlash('notice_error', 'Le lien de validation est invalide ou expiré.');

            return $this->redirectToRoute('index');
        }

        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        $amapien = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->findOneBy(['user' => $invitation->getUser(), 'amap' => $invitation->getAmap()])
        ;
        if (null !== $amapien) { // L'amapien est déjà présent dans l'AMAP
            if ($amapien->isDeleted()) {
                $amapien->setDeletedAt(null);
                $this->em->remove($invitation);
                $this->em->flush();

                $flashSuccess($invitation);

                return $this->redirectToRoute('index');
            }

            $this->addFlash('notice_error', 'Le lien de validation est invalide ou expiré.');

            return $this->redirectToRoute('index');
        }

        $amapien = new \PsrLib\ORM\Entity\Amapien($invitation->getUser(), $invitation->getAmap());
        $this->em->persist($amapien);
        $this->em->remove($invitation);
        $this->em->flush();

        $flashSuccess($invitation);

        return $this->redirectToRoute('index');
    }

    /**
     * Edition d'un amapien.
     */
    #[Route('amapien/informations_generales_edition/{amapien_id}', name: 'amapien_informations_generales_edition')]
    public function informations_generales_edition(int $amapien_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $form = $this->formFactory->create(\PsrLib\Form\UserAmapienAmapType::class, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'amapien a été mis à jour avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amapien/informations_generales.html.twig', [
            'form' => $form->createView(),
            'amapien' => $amapien,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amapien/informations_generales_creation/{amap_id}', name: 'amapien_informations_generales_creation')]
    public function informations_generales_creation(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE_AMAPIENS, $amap);

        $user = new User();
        $amapien = new \PsrLib\ORM\Entity\Amapien($user, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\UserAmapienAmapCreationType::class, $amapien);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($amapien);
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'amapien a été créé avec succès.'
            );

            if (true === $form->get('needWaiting')->getData()) {
                return $this->redirectToRoute('amapien_liste_attente', ['amapien_id' => $amapien->getId()]);
            }

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amapien/informations_generales.html.twig', [
            'form' => $form->createView(),
            'amapien' => $amapien,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amapien/telecharger_amapiens_amap', name: 'amapien_telecharger_amapiens_amap')]
    public function telecharger_amapiens_amap(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_LIST_AMAPIENS);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienAmapType::class, null, [
            'currentUser' => $this->getUser(),
            'amaps' => $this->getUser()->getAdminAmaps(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));
        $amapienSearchState = $searchForm->getData();

        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->search($amapienSearchState)
        ;

        if (0 === count($amapiens)) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->_telecharger_amapien($amapiens, $searchForm->getData());
    }

    /**
     * Télécharger l'ensemble des amapiens de la session recherche.
     */
    #[Route('amapien/telecharger_amapiens', name: 'amapien_telecharger_amapiens')]
    public function telecharger_amapiens(): Response
    {
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_LIST);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapienType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        /** @var \PsrLib\DTO\SearchAmapienState $searchState */
        $searchState = $searchForm->getData();

        /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
        $amapiens = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->search($searchState);
        if (0 === count($amapiens)) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->_telecharger_amapien($amapiens, $searchForm->getData());
    }

    /**
     * Créer un mot de passe pour un amapien et lui envoyer par email.
     */
    #[Route('amapien/password/{amapien_id}', name: 'amapien_password')]
    public function password(int $amapien_id, PasswordEncoder $passwordEncoder, PasswordGenerator $passwordGenerator, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);

        $password = $passwordGenerator->generatePassword();
        $pass_hash = $passwordEncoder->encodePassword($password);

        // EMAIL
        $user = $amapien->getUser();
        $user->setPassword($pass_hash);
        $amapien->setEtat(ActivableUserInterface::ETAT_ACTIF);
        $this->em->flush();

        $emailSender->envoyerMailMdp($user, $password);

        $this->addFlash(
            'notice_success',
            'Le mot de passe a été envoyé avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    /**
     * @param Amapien[] $amapiens
     */
    private function _telecharger_amapien(
        array $amapiens,
        SearchAmapienAmapState $searchAmapienAmapState
    ): Response {
        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = count($amapiens);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Amapiens');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $this->excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Email');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Abonné newsletter');
        $this->excel->getActiveSheet()->setCellValue('K4', 'liste d\'attente = oui/non');
        $this->excel->getActiveSheet()->setCellValue('L4', 'État');

        $this->excel->getActiveSheet()->getStyle('A4:L4')->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle('A4:L4')->getFont()->setBold(true);

        $i = 5;
        foreach ($amapiens as $amapien) {
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());
            $user = $amapien->getUser();

            $this->excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($user->getName()->getLastName() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $this->excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($user->getName()->getFirstName() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('C'.$i, implode(';', $user->getEmails()->toArray()));
            $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i, $user->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$i, $user->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('F'.$i, $user->getAddress()->getAdress());
            $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValueExplicit('G'.$i, $user->getVille()?->getCpString() ?? null, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('H'.$i, strtoupper($user->getVille()?->getNom() ?? ''));
            $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('J'.$i, ucfirst(true === $user->getNewsletter() ? 'Oui' : 'Non'));
            $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            if (!$amapien->getFermeAttentes()->isEmpty()) {
                $this->excel->getActiveSheet()->setCellValue('K'.$i, 'OUI');
            } else {
                $this->excel->getActiveSheet()->setCellValue('K'.$i, 'NON');
            }

            $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $this->excel->getActiveSheet()->setCellValue('L'.$i, ucfirst($amapien->getEtat()));
            $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->applyFromArray($style);

            ++$i;
        }

        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;

        if ($searchAmapienAmapState instanceof SearchAmapienState) {
            if (null !== $searchAmapienAmapState->getRegion()) {
                $titre = $searchAmapienAmapState->getRegion().' ; ';
            }

            if (null !== $searchAmapienAmapState->getDepartement()) {
                $titre .= $searchAmapienAmapState->getDepartement().' ; ';
            }

            if (null !== $searchAmapienAmapState->getReseau()) {
                $titre .= $searchAmapienAmapState->getReseau().' ; ';
            }
        }

        if (null !== $searchAmapienAmapState) {
            if (null !== $searchAmapienAmapState->getAmap()) {
                $titre .= $searchAmapienAmapState->getAmap().' ; ';
            }

            if (null !== $searchAmapienAmapState->getAdhesion()) {
                $titre .= 'Années d\'adhésion : '.$searchAmapienAmapState->getAdhesion().' ; ';
            }

            if (true === $searchAmapienAmapState->getNewsletter()) {
                $titre = $titre.'Abonnés à la newsletter ; ';
            }

            if (true === $searchAmapienAmapState->getFilter()) {
                $titre .= 'Actifs';
            }
        }

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_amapiens_'.date('d_m_Y').'.xls';

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportAmapiens_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    /**
     * Activer / désactiver un amapien.
     */
    #[Route('amapien/de_activate/{amapien_id}/{etat}', name: 'amapien_de_activate')]
    public function de_activate(int $amapien_id, string $etat, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapien);
        if (!in_array($etat, ActivableUserInterface::ETAT_AVALIABLE, true)) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $amapien->setEtat(ActivableUserInterface::ETAT_ACTIF === $etat ? ActivableUserInterface::ETAT_INACTIF : ActivableUserInterface::ETAT_ACTIF);
        $this->em->flush();

        if (ActivableUserInterface::ETAT_ACTIF === $amapien->getEtat()) {
            $emailSender->envoyerMailAmapienActivation($amapien);
        }

        $this->addFlash(
            'notice_success',
            'L\'état de l\'amapien a été modifié avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('amapien/suppression/{amapien_id}', name: 'amapien_suppression', methods: ['POST'])]
    public function suppression(int $amapien_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_AMAPIEN_REMOVE, $amapien);

        if ($this->em->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)->countTotalForAmapienInFutureWithQtyPositive($amapien) > 0) {
            $this->addFlash(
                'notice_error',
                'Impossible de supprimer un amapien qui a encore des livraisons prévues'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->remove($amapien, true);

        $this->addFlash(
            'notice_success',
            'L\'amapien a été supprimé avec succès'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('amapien/activation_multiple', name: 'amapien_activation_multiple', methods: ['POST'])]
    public function activation_multiple(Email_Sender $emailSender): Response
    {
        $amapiens = $this->parseTableSelectQueryParams(Amapien::class);
        $this->denyAccessUnlessGrantedMultiple(AmapienVoter::ACTION_AMAPIEN_EDIT_EXTERNAL, $amapiens);

        $amapiensNonActivated = array_filter($amapiens, fn (Amapien $amapien) => ActivableUserInterface::ETAT_INACTIF === $amapien->getEtat());
        foreach ($amapiensNonActivated as $amapien) {
            $amapien->setEtat(ActivableUserInterface::ETAT_ACTIF);
        }
        $this->em->flush();
        foreach ($amapiensNonActivated as $amapien) {
            $emailSender->envoyerMailAmapienActivation($amapien);
        }

        $this->addFlash(
            'notice_success',
            sprintf('%s amapien(s) sélectionné(s) activé(s).', count($amapiensNonActivated))
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }
}
