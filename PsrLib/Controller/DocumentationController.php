<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\Security\Voters\DocumentVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DocumentationController extends AbstractController
{
    #[Route('documentation', name: 'documentation_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoter::ACTION_DOCUMENT_GETOWN);

        $currentUser = $this->getUser();

        $documents = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Document::class)
            ->findForUser($currentUser)
        ;

        return $this->render(
            'documentation/index.html.twig',
            ['documents' => $documents]
        );
    }

    #[Route('documentation/contact', name: 'documentation_contact')]
    public function contact(): Response
    {
        return $this->index();
    }

    #[Route('documentation/list_documents', name: 'documentation_list_documents')]
    public function list_documents(): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoter::ACTION_DOCUMENT_MANAGE);
        $documents = $this->em->getRepository(\PsrLib\ORM\Entity\Document::class)->findAll();

        return $this->render(
            'documentation/list.html.twig',
            ['documents' => $documents]
        );
    }

    #[Route('documentation/form/{document_id}', name: 'documentation_form')]
    public function form(int $document_id = null): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoter::ACTION_DOCUMENT_MANAGE);

        if (null === $document_id) {
            $document = new \PsrLib\ORM\Entity\Document();
        } else {
            $document = $this->findOrExit(\PsrLib\ORM\Entity\Document::class, $document_id);
        }

        $form = $this->formFactory->create(\PsrLib\Form\DocumentationType::class, $document);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($document);
            $this->em->flush();
            if (null === $document_id) {
                $this->addFlash(
                    'flash_document_form_confirm',
                    'Document correctement ajouté'
                );
            } else {
                $this->addFlash(
                    'flash_document_form_confirm',
                    'Document correctement modifié'
                );
            }

            return $this->redirectToRoute('documentation_list_documents');
        }

        return $this->render(
            'documentation/form.html.twig',
            [
                'document' => $document,
                'form' => $form->createView(),
            ]
        );
    }

    #[Route('documentation/supprimer/{document_id}', name: 'documentation_supprimer')]
    public function supprimer(int $document_id): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoter::ACTION_DOCUMENT_MANAGE);

        $document = $this->findOrExit(\PsrLib\ORM\Entity\Document::class, $document_id);
        $this->em->remove($document);
        $this->em->flush();

        return $this->redirectToRoute('documentation_list_documents');
    }
}
