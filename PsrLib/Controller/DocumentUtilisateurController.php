<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Form\DocumentUtilisateurType;
use PsrLib\ORM\Entity\DocumentUtilisateur;
use PsrLib\ORM\Repository\DocumentUtilisateurRepository;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\EntityBuilder\DocumentUtilisateurFactory;
use PsrLib\Services\Security\Voters\DocumentUtilisateurEditVoter;
use PsrLib\Services\Security\Voters\DocumentUtilisateurManageAdminVoter;
use PsrLib\Services\ZipCreator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DocumentUtilisateurController extends AbstractController
{
    public function __construct(
        private readonly DocumentUtilisateurFactory $documentUtilisateurFactory,
        private readonly Email_Sender $emailSender,
        private readonly ZipCreator $zipCreator
    ) {
    }

    #[Route('document_utilisateur', name: 'document_utilisateur_index')]
    public function index(DocumentUtilisateurRepository $documentUtilisateurRepository): Response
    {
        $this->denyAccessUnlessGranted(DocumentUtilisateurManageAdminVoter::ACTION_DOCUMENT_UTILISATEUR_MANAGE);

        $this->redirectFromStoredGetParamIfExist('document_utilisateur');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchDocumentUtilisateurFormType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $documents = $documentUtilisateurRepository->search($searchForm->getData());

        $emails = [];
        /**
         * @param \PsrLib\ORM\Entity\User[] $users
         *
         * @return string[]
         */
        $parseUsersEmails = function ($users) {
            $emails = [];
            foreach ($users as $user) {
                foreach ($user->getEmails() as $email) {
                    $emails[] = $email->getEmail();
                }
            }

            return $emails;
        };
        foreach ($documents as $document) {
            $emails[$document->getId()] = [];
            if ($document instanceof \PsrLib\ORM\Entity\DocumentUtilisateurAmap) {
                $emails[$document->getId()] = $parseUsersEmails($document->getAmap()->getAdmins()->toArray());
            }
            if ($document instanceof \PsrLib\ORM\Entity\DocumentUtilisateurFerme) {
                $emails[$document->getId()] = $parseUsersEmails(
                    $document->getFerme()->getPaysans()->map(fn (\PsrLib\ORM\Entity\Paysan $paysan) => $paysan->getUser())->toArray()
                );
            }
        }

        return $this->render('document_utilisateur/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'documents' => $documents,
            'emails' => $emails,
        ]);
    }

    #[Route('document_utilisateur/document_action', name: 'document_utilisateur_document_action', methods: ['POST'])]
    public function document_action(): Response
    {
        $documents = $this->document_utilisateur_action_get_selected();
        if (0 === count($documents)) {
            $this->addFlash('warning', 'Aucun document sélectionné.');

            return $this->redirectToRoute('document_utilisateur_index');
        }

        $this->denyAccessUnlessGrantedMultiple(DocumentUtilisateurEditVoter::ACTION_DOCUMENT_UTILISATEUR_EDIT, $documents);
        if ($this->request->request->has('action-delete')) {
            return $this->document_action_delete($documents);
        }

        if ($this->request->request->has('action-download')) {
            return $this->document_action_download($documents);
        }

        if ($this->request->request->has('action-notify')) {
            return $this->document_action_notify($documents);
        }

        return $this->redirectToRoute('document_utilisateur_index');
    }

    /**
     * @param \PsrLib\ORM\Entity\DocumentUtilisateur[] $documents
     */
    private function document_action_delete($documents): Response
    {
        foreach ($documents as $document) {
            $this->em->remove($document);
        }
        $this->em->flush();
        $this->addFlash('success', 'Les documents ont bien été supprimé.');

        return $this->redirectToRoute('document_utilisateur_index');
    }

    /**
     * @param \PsrLib\ORM\Entity\DocumentUtilisateur[] $documents
     */
    private function document_action_download($documents): Response
    {
        $files = array_map(
            fn (\PsrLib\ORM\Entity\DocumentUtilisateur $du) => new \PsrLib\DTO\ZipCreatorFileDTO($du->getFicher(), $du->getFicher()->getOriginalFileName()),
            $documents
        );
        $fileName = $this->zipCreator->createZipFiles($files);

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'documents.zip');
    }

    /**
     * @param \PsrLib\ORM\Entity\DocumentUtilisateur[] $documents
     */
    private function document_action_notify($documents): Response
    {
        $users = array_map(function (DocumentUtilisateur $du) {
            if ($du instanceof \PsrLib\ORM\Entity\DocumentUtilisateurAmap) {
                return $du->getAmap()->getAdmins()->toArray();
            }
            if ($du instanceof \PsrLib\ORM\Entity\DocumentUtilisateurFerme) {
                return $du->getFerme()->getPaysans()->map(fn (\PsrLib\ORM\Entity\Paysan $paysan) => $paysan->getUser())->toArray();
            }
        }, $documents);
        $users = array_merge(...$users);
        foreach ($users as $user) {
            $this->emailSender->envoyerUserDocumentNotification($user);
        }

        $this->addFlash('success', 'Les notifications ont bien été envoyées.');

        return $this->redirectToRoute('document_utilisateur_index');
    }

    #[Route('document_utilisateur/creation', name: 'document_utilisateur_creation')]
    public function creation(): Response
    {
        $this->denyAccessUnlessGranted(DocumentUtilisateurManageAdminVoter::ACTION_DOCUMENT_UTILISATEUR_MANAGE);

        $form = $this->formFactory->create(DocumentUtilisateurType::class, null, [
            'current_user' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentUtilisateur = $this->documentUtilisateurFactory->create($form->getData());
            $this->em->persist($documentUtilisateur);
            $this->em->flush();

            $this->addFlash('success', 'Le document a bien été créé.');

            return $this->redirectToRoute('document_utilisateur_index');
        }

        return $this->render('document_utilisateur/creation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('document_utilisateur/supprimer/{du_id}', name: 'document_utilisateur_supprimer', methods: ['POST'])]
    public function supprimer(int $du_id): Response
    {
        $du = $this->findOrExit(\PsrLib\ORM\Entity\DocumentUtilisateur::class, $du_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\DocumentUtilisateurEditVoter::ACTION_DOCUMENT_UTILISATEUR_EDIT, $du);

        $this->em->remove($du);
        $this->em->flush();

        $this->addFlash('success', 'Le document a bien été supprimé.');

        return $this->redirectToRoute('document_utilisateur_index');
    }

    #[Route('document_utilisateur/telecharger/{du_id}', name: 'document_utilisateur_telecharger')]
    public function telecharger(int $du_id): Response
    {
        /** @var DocumentUtilisateur $du */
        $du = $this->findOrExit(\PsrLib\ORM\Entity\DocumentUtilisateur::class, $du_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\DocumentUtilisateurDownloadVoter::ACTION_DOCUMENT_UTILISATEUR_DOWNLOAD, $du);

        return BinaryFileResponseFactory::createFromFile($du->getFicher());
    }

    /**
     * @return \PsrLib\ORM\Entity\DocumentUtilisateur[]
     */
    private function document_utilisateur_action_get_selected()
    {
        /** @var string[] $selectedIds */
        $selectedIds = $this->request->request->get('selected');

        return array_map(fn ($id) => $this->findOrExit(\PsrLib\ORM\Entity\DocumentUtilisateur::class, $id), array_keys($selectedIds));
    }
}
