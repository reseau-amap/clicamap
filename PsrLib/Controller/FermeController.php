<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Carbon\Carbon;
use DI\Attribute\Inject;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Exporters\ExcelGeneratorExportFerme;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FermeController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_ferme';

    #[Inject]
    public \PsrLib\Services\Leaflet $leaflet;

    #[Route('ferme', name: 'ferme_index')]
    public function index(): Response
    {
        return $this->accueil();
    }

    /**
     * Accueil / Moteur de recherche.
     */
    #[Route('ferme/accueil', name: 'ferme_accueil')]
    public function accueil(): Response
    {
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIST);
        $this->session_store_home_path(self::HOME_PATH_ID);

        $this->redirectFromStoredGetParamIfExist('ferme');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $gps = 0;
        $map = null;
        $fermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->search($searchForm->getData())
        ;

        if ($searchForm->get('carto')->getData()) {
            [$gps, $map] = $this->cartographie($fermes);
        }

        return $this->render('ferme/display_all.html.twig', [
            'fermes' => $fermes,
            'searchForm' => $searchForm->createView(),
            'gps' => $gps,
            'map' => $map,
        ]);
    }

    #[Route('ferme/home_paysan/{ferme_id}', name: 'ferme_home_paysan')]
    public function home_paysan(int $ferme_id = null): Response
    {
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY_OWN, $ferme);

        $this->session_store_home_path(self::HOME_PATH_ID);

        return $this->render('ferme/home_paysan.html.twig', [
            'paysan' => $this->getUser(),
            'ferme' => $ferme,
        ]);
    }

    /**
     *  Ajout / Édition des informations générales.
     */
    #[Route('ferme/informations_generales/{ferme_id}', name: 'ferme_informations_generales')]
    public function informations_generales(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\FermeType::class, $ferme, [
            'permission_edit_adhesions' => $currentUser->isAdmin(),
            'current_user' => $this->getUser(),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La ferme a été modifiée avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('ferme/informations_generales.html.twig', [
            'form' => $form->createView(),
            'ferme' => $ferme,
        ]);
    }

    #[Route('ferme/informations_generales_creation', name: 'ferme_informations_generales_creation')]
    public function informations_generales_creation(): Response
    {
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_CREATE);

        $ferme = new \PsrLib\ORM\Entity\Ferme();

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\FermeType::class, $ferme, [
            'permission_edit_adhesions' => $currentUser->isAdmin(),
            'current_user' => $this->getUser(),
        ]);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($ferme);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Vous venez de créer une ferme. Vous pouvez d\'ores et déjà lui ajouter un ou plusieurs paysans.'
            );

            // Bind home path to allow cancel button working
            $this->session_store_home_path(PaysanController::HOME_PATH_ID, $this->session_get_home_path(self::HOME_PATH_ID));

            return $this->redirectToRoute('paysan_informations_generales_creation', [
                'ferme_id' => $ferme->getId(),
            ]);
        }

        return $this->render('ferme/informations_generales.html.twig', [
            'form' => $form->createView(),
            'ferme' => $ferme,
        ]);
    }

    /**
     * Afficher les détails de la ferme.
     */
    #[Route('ferme/display/{ferme_id}', name: 'ferme_display')]
    public function display(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $fermeCommentaire = new \PsrLib\ORM\Entity\FermeCommentaire($ferme);
        $formComment = $this->formFactory->create(\PsrLib\Form\FermeCommentaireType::class, $fermeCommentaire);
        $formComment->handleRequest($this->request);
        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $comment = $formComment->getData();
            $comment->setFerme($ferme);
            $this->em->persist($comment);
            $this->em->flush();

            return $this->redirectToRoute('ferme_display', ['ferme_id' => $ferme->getId()]);
        }

        return $this->render('ferme/display.html.twig', [
            'ferme' => $ferme,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            'request' => $this->request,
            'formComment' => $formComment->createView(),
            'formCommentHasErrors' => $formComment->getErrors(true)->count() > 0,
        ]);
    }

    /**
     * Ajouter un commentaire daté.
     */
    #[Route('ferme/ferme_commentaire_remove/{commentaire_id}', name: 'ferme_ferme_commentaire_remove', methods: ['POST'])]
    public function ferme_commentaire_remove(int $commentaire_id): Response
    {
        /** @var \PsrLib\ORM\Entity\FermeCommentaire $commentaire */
        $commentaire = $this->findOrExit(\PsrLib\ORM\Entity\FermeCommentaire::class, $commentaire_id);

        $ferme = $commentaire->getFerme();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\FermeVoter::ACTION_FERME_EDIT_COMMENT, $ferme);

        $this->em->remove($commentaire);
        $this->em->flush();

        return $this->redirectToRoute('ferme_display', ['ferme_id' => $ferme->getId()]);
    }

    /**
     * Supprimer la ferme.
     */
    #[Route('ferme/remove_ferme/{ferme_id}', name: 'ferme_remove_ferme')]
    public function remove_ferme(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_REMOVE, $ferme);

        $this->em->remove($ferme);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'La ferme a été supprimée avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    /**
     * Afficher les paysans de la ferme.
     */
    #[Route('ferme/paysan_display/{ferme_id}', name: 'ferme_paysan_display')]
    public function paysan_display(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $form = $this->formFactory->create(\PsrLib\Form\UserVerificationType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\User|null $user */
            $user = $form->get('user')->getData();
            if (null === $user) {
                $this->addFlash(
                    'notice_error',
                    'Aucun utilisateur trouvé avec cet email ou ce nom d\'utilisateur.'
                );

                return $this->redirectToRoute('ferme_paysan_display', ['ferme_id' => $ferme->getId()]);
            }

            $paysan = $user->getPaysan();
            if (null === $paysan) {
                $paysan = new \PsrLib\ORM\Entity\Paysan();
                $paysan->setUser($user);
                $this->em->persist($paysan);
            }

            if ($paysan->getFermes()->contains($ferme)) {
                $this->addFlash(
                    'notice_error',
                    'Le paysan est déjà lié à cette ferme.'
                );

                return $this->redirectToRoute('ferme_paysan_display', ['ferme_id' => $ferme->getId()]);
            }
            $paysan->addFerme($ferme);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le paysan a été ajouté avec succès.'
            );

            return $this->redirectToRoute('ferme_paysan_display', ['ferme_id' => $ferme->getId()]);
        }

        return $this->render('ferme/paysan_display.html.twig', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer un paysan de la ferme.
     */
    #[Route('ferme/remove_paysan/{pay_id}/{f_id}', name: 'ferme_remove_paysan')]
    public function remove_paysan(int $pay_id, int $f_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $pay_id);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $f_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $paysan->removeFerme($ferme);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le paysan a été supprimé avec succès.'
        );

        return $this->redirectToRoute('ferme_paysan_display', ['ferme_id' => $ferme->getId()]);
    }

    #[Route('ferme/referent_display/{ferme_id}', name: 'ferme_referent_display')]
    public function referent_display(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $this->redirectFromStoredGetParamIfExist('ferme_referent');
        $formSearch = $this->formFactory->create(\PsrLib\Form\FermeRefProduitType::class, null, [
            'currentUser' => $this->getUser(),
        ]);

        // Force submit when nothing selected
        $formSearch->submit($this->request->query->get($formSearch->getName()));

        /** @var \Symfony\Component\Form\FormInterface $amapField */
        $amapField = $formSearch->get('amap');

        /** @var \PsrLib\ORM\Entity\Amap|null $formAmap */
        $formAmap = $amapField->getData();
        if (null === $formAmap) {
            $amapAccessibles = $amapField->getConfig()->getOption('choices');
        } else {
            $amapAccessibles = [$formAmap];
        }

        /** @var \PsrLib\ORM\Entity\Amap|null $selectedAmap */
        $selectedAmap = $amapField->getData();
        $formAdd = null;
        if (null !== $selectedAmap) {
            $formAdd = $this
                ->formFactory
                ->create(\PsrLib\Form\FermeRefProduitAmapienType::class, null, [
                    'amap' => $selectedAmap,
                    'ferme' => $ferme,
                ])
            ;
        }

        $referents = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getRefProduitFromMultipleAmapsFerme($amapAccessibles, $ferme)
        ;

        return $this->render('ferme/referent_display.html.twig', [
            'ferme' => $ferme,
            'referents' => $referents,
            'formSearch' => $formSearch->createView(),
            'formAdd' => null === $formAdd ? null : $formAdd->createView(),
            'amapAccessibles' => $amapAccessibles,
            'selectedAmap' => $selectedAmap,
        ]);
    }

    #[Route('ferme/add_referent_produit/{f_id}/{a_id}', name: 'ferme_add_referent_produit')]
    public function add_referent_produit(int $f_id, int $a_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $f_id);

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $a_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CHANGE_REF_PROD, $amap);

        $formAdd = $this
            ->formFactory
            ->create(\PsrLib\Form\FermeRefProduitAmapienType::class, null, [
                'amap' => $amap,
                'ferme' => $ferme,
            ])
        ;
        $formAdd->handleRequest($this->request);
        if ($formAdd->isSubmitted() && $formAdd->isValid()) {
            $ferme->addAmapienRef($formAdd->get('amapien')->getData());
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le référent a été ajouté avec succès.'
            );
        }

        return $this->redirectToRoute('ferme_referent_display', ['ferme_id' => $ferme->getId()]);
    }

    #[Route('ferme/remove_referent_produit/{f_id}/{f_ref_id}', name: 'ferme_remove_referent_produit')]
    public function remove_referent_produit(int $f_id, int $f_ref_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $refProd */
        $refProd = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $f_ref_id);

        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $f_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $ferme->removeAmapienRef($refProd);

        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le référent a été supprimé avec succès.'
        );

        return $this->redirectToRoute('ferme_referent_display', ['ferme_id' => $ferme->getId()]);
    }

    /**
     * Ajouter / Éditer les produits proposés.
     */
    #[Route('ferme/type_production_creation/{ferme_id}', name: 'ferme_type_production_creation')]
    public function type_production_creation(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        $tpp = new \PsrLib\ORM\Entity\FermeTypeProductionPropose($ferme);
        $form = $this->formFactory->create(\PsrLib\Form\FermeTypeProductionProposeType::class, $tpp);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $certificate granted by form */
            $certificate = $form->get('certification')->getData();
            $file = $this
                ->uploader
                ->uploadFile($certificate, \PsrLib\ORM\Entity\Files\FermeTypeProductionAttestationCertification::class)
            ;
            $tpp->setAttestationCertification($file);

            $this->em->persist($tpp);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le type de production a été ajouté avec succès.'
            );

            return $this->redirectToRoute('ferme_type_production_display', ['ferme_id' => $ferme->getId()]);
        }

        return $this->render('ferme/type_production_creation.html.twig', [
            'ferme' => $ferme,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Afficher les produits proposés par le paysan.
     */
    #[Route('ferme/type_production_display/{ferme_id}', name: 'ferme_type_production_display')]
    public function type_production_display(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        return $this->render('ferme/type_production_display.html.twig', [
            'ferme' => $ferme,
        ]);
    }

    /**
     * Supprimer un produit proposé par le paysan.
     */
    #[Route('ferme/type_production_remove/{type_production_id}', name: 'ferme_type_production_remove')]
    public function type_production_remove(int $type_production_id): Response
    {
        /** @var \PsrLib\ORM\Entity\FermeTypeProductionPropose $tpp */
        $tpp = $this->findOrExit(\PsrLib\ORM\Entity\FermeTypeProductionPropose::class, $type_production_id);
        $ferme = $tpp->getFerme();
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_TYPE_PRODUCTION_MANAGE, $ferme);

        $this->em->remove($tpp);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le produit n\'est plus proposé par le paysan'
        );

        return $this->redirectToRoute('ferme_type_production_display', ['ferme_id' => $ferme->getId()]);
    }

    #[Route('ferme/telecharger_ferme_complet', name: 'ferme_telecharger_ferme_complet')]
    public function telecharger_ferme_complet(ExcelGeneratorExportFerme $excelgeneratorexportferme): Response
    {
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DOWNLOAD);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $outFileName = $excelgeneratorexportferme->genererExportFermeComplet($searchForm->getData());

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outFileName,
            sprintf('liste_des_fermes_%s.xlsx', Carbon::now()->format('Y_m_d'))
        );
    }

    #[Route('ferme/home_refprod', name: 'ferme_home_refprod')]
    public function home_refprod(): Response
    {
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIST_REFPROD);

        $this->session_store_home_path(self::HOME_PATH_ID);

        return $this->render('ferme/display_all_refprod.html.twig', [
            'fermes' => $this->getUser()->getFermesAsRefproduit(),
        ]);
    }

    #[Route('ferme/home_regroupement/{regroupement_id}', name: 'ferme_home_regroupement')]
    public function home_regroupement(int $regroupement_id): Response
    {
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $regroupement_id);
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_MANAGE, $regroupement);

        $this->session_store_home_path(self::HOME_PATH_ID);

        return $this->render('ferme/display_all_regroupement.html.twig', [
            'fermes' => $regroupement->getFermes()->toArray(),
        ]);
    }

    /**
     * Générer la CARTOGRAPHIE.
     *
     * @param $fermes \PsrLib\ORM\Entity\Ferme[]
     */
    private function cartographie(array $fermes): array
    {
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIST);

        $gps = 0;
        $map = null;

        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        foreach ($fermes as $ferme) {
            if ($ferme->getGpsLatitude()) {
                ++$gps;
                $config = [
                    'center' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(),
                    'zoom' => 7,
                ];

                $this->leaflet->initialize($config);

                // INFOS DU POPUP
                $infos = '<strong>'.addslashes($ferme->getNom()).'</strong>';
                if ($ferme->getLibAdr()) {
                    $infos = $infos.'<br/>'.addslashes($ferme->getLibAdr());
                }
                $ville = $ferme->getVille();
                $infos = $infos.'<br/>'.$ville->getCpString();
                $infos = $infos.', '.$ville->getNom();

                // Emplacement du marker
                $marker = [
                    'latlng' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(), // Marker Location
                    // POPUP
                    'popupContent' => $infos, // Popup Content
                ];

                $this->leaflet->add_marker($marker);
            }
        }

        if ($gps > 0) {
            $map = $this->leaflet->create_map();
        }

        return [$gps, $map];
    }
}
