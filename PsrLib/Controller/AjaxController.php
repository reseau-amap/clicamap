<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\GeocodeQuery;
use PsrLib\Services\Security\Voters\AjaxVoter;
use PsrLib\Services\UsernameGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * Autocomplétion CP / Ville.
     */
    #[Route('ajax/get_cp_ville_suggestion', name: 'ajax_get_cp_ville_suggestion')]
    public function get_cp_ville_suggestion(GeocodeQuery $geocoder): Response
    {
        $row_set = [];
        $this->denyAccessUnlessGranted(AjaxVoter::ACTION_AJAX);

        if (!isset($_GET['term'])) {
            return new Response();
        }
        $suggestions = $geocoder->geocode_suggestions($_GET['term']);

        foreach ($suggestions as $suggestion) {
            $ville = $suggestion->getVille();

            $new_row['label'] = $ville->getCpString().', '.$ville->getNom();
            $new_row['value'] = $ville->getCpString().', '.$ville->getNom();
            $row_set[] = $new_row; // construit un tableau
        }

        return new JsonResponse($row_set);
    }

    /**
     * Autocomplétition des adresses.
     */
    #[Route('ajax/geocode_get_suggestion', name: 'ajax_geocode_get_suggestion')]
    public function geocode_get_suggestion(GeocodeQuery $geocoder): Response
    {
        $this->denyAccessUnlessGranted(AjaxVoter::ACTION_AJAX);

        $query = $this->request->get('term');
        if (null === $query) {
            return new JsonResponse([]);
        }

        $suggestions = $geocoder->geocode_suggestions($query);

        return JsonResponse::fromJsonString($this->serializer->serialize($suggestions, 'json', ['groups' => 'ajax']));
    }

    #[Route('ajax/username_generate', name: 'ajax_username_generate', methods: ['POST'])]
    public function username_generate(UsernameGenerator $usernameGenerator): Response
    {
        $this->denyAccessUnlessGranted(AjaxVoter::ACTION_AJAX);

        $prenom = $this->request->request->get('firstName');
        $nom = $this->request->request->get('lastName');

        $username = $usernameGenerator->generate($prenom, $nom);

        return JsonResponse::fromJsonString($this->serializer->serialize(['username' => $username], 'json'));
    }
}
