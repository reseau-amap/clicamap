<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\ORM\Repository\UserRepository;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Password\PasswordEncoder;
use PsrLib\Services\Password\PasswordGenerator;
use PsrLib\Services\Security\Voters\UserVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user', name: 'user_index')]
    public function index(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::ACTION_USER_MANAGE);
        $this->redirectFromStoredGetParamIfExist('user');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchUserType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));
        $userSearchState = $searchForm->getData();

        $users = $userRepository
            ->search($userSearchState)
        ;

        return $this->render('user/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'users' => $users,
        ]);
    }

    #[Route('/user/creation', name: 'user_creation')]
    public function creation(): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::ACTION_USER_MANAGE);

        $user = new \PsrLib\ORM\Entity\User();
        $form = $this->formFactory->create(\PsrLib\Form\UserProfilType::class, $user);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('notice_success', 'Utilisateur ajouté.');

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/creation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/user/modification/{userId}', name: 'user_modification')]
    public function modification(int $userId = null): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::ACTION_USER_MANAGE);
        $user = $this->findOrExit(\PsrLib\ORM\Entity\User::class, $userId);

        $form = $this->formFactory->create(\PsrLib\Form\UserProfilType::class, $user);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('notice_success', 'Utilisateur modifié.');

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/modification.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/user/mdp/{userId}', name: 'user_mdp')]
    public function mdp(PasswordGenerator $passwordGenerator, PasswordEncoder $passwordEncoder, Email_Sender $emailSender, int $userId = null): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::ACTION_USER_MANAGE);
        /** @var \PsrLib\ORM\Entity\User $user */
        $user = $this->findOrExit(\PsrLib\ORM\Entity\User::class, $userId);

        $password = $passwordGenerator->generatePassword();
        $pass_hash = $passwordEncoder->encodePassword($password);

        // EMAIL
        $user->setPassword($pass_hash);
        $this->em->flush();

        $emailSender->envoyerMailMdp($user, $password);

        $this->addFlash(
            'notice_success',
            'Le mot de passe a été envoyé avec succès.'
        );

        return $this->redirectToRoute('user_index');
    }
}
