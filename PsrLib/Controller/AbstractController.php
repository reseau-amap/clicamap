<?php

declare(strict_types=1);

namespace PsrLib\Controller;

abstract class AbstractController
{
    use AbstractControllerTrait;
}
