<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\Leaflet;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    public function __construct(
        private readonly Leaflet $leaflet
    ) {
    }

    #[Route('map/amap_display/{amap_id}', name: 'map_amap_display')]
    public function amap_display(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_SHOW_MAP, $amap);

        // CARTOGRAPHIE
        $gps = 0;
        $map = null;

        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
        foreach ($amap->getLivraisonLieux() as $liv_lieu) {
            if ($liv_lieu->getGpsLatitude()) {
                ++$gps;
                $config = [
                    'center' => $liv_lieu->getGpsLatitude().','.$liv_lieu->getGpsLongitude(),
                    'zoom' => 12,
                ];

                $this->leaflet->initialize($config);

                // INFOS DU POPUP
                $infos = '<strong>'.$amap->getNom().'</strong>';
                $infos = $infos.'<br/>'.$liv_lieu->getNom();
                if ($liv_lieu->getAdresse()) {
                    $infos = $infos.'<br/>'.$liv_lieu->getAdresse();
                }
                $ville = $liv_lieu->getVille();
                if (null !== $ville) {
                    $infos = $infos.'<br/>'.$ville->getCpString();
                }
                if (null !== $ville) {
                    $infos = $infos.' '.$ville->getNom();
                }
                if ($amap->getEmail()) {
                    $infos = $infos.'<br/>'.$amap->getEmail();
                }
                if ($amap->getUrl()) {
                    $infos = $infos.'<br/>'.$amap->getUrl();
                }

                // Emplacement du marker
                $marker = [
                    'latlng' => $liv_lieu->getGpsLatitude().','.$liv_lieu->getGpsLongitude(), // Marker Location
                    // POPUP
                    'popupContent' => addslashes($infos), // Popup Content
                ];

                $this->leaflet->add_marker($marker);
            }
        }

        if ($gps > 0) {
            $map = $this->leaflet->create_map();
        } else {
            $gps = 'aucun';
        }

        return $this->render('map/amap.html.twig', [
            'gps' => $gps,
            'map' => $map,
            'amap' => $amap,
        ]);
    }

    #[Route('map/ferme_display/{ferme_id}', name: 'map_ferme_display')]
    public function ferme_display(int $ferme_id): Response
    {
        $data = [];
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        $data['ferme'] = $ferme;

        $config = [
            'center' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(),
            'zoom' => 16,
        ];

        $this->leaflet->initialize($config);

        // INFOS DU POPUP
        $infos = '<strong>'.$ferme->getNom().'</strong>';
        if ($ferme->getLibAdr()) {
            $infos = $infos.'<br/>'.$ferme->getLibAdr();
        }
        $ville = $ferme->getVille();
        if ($ville->getCpString()) {
            $infos = $infos.'<br/>'.$ville->getCpString();
        }
        if ($ville->getNom()) {
            $infos = $infos.', '.$ville->getNom();
        }

        // Emplacement du marker
        $marker = [
            'latlng' => $ferme->getGpsLatitude().','.$ferme->getGpsLongitude(), // Marker Location

            // POPUP
            'popupContent' => $infos, // Popup Content
        ];

        $this->leaflet->add_marker($marker);
        $data['map'] = $this->leaflet->create_map();

        return $this->render('map/ferme.html.twig', $data);
    }
}
