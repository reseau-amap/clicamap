<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Controller;

use PsrLib;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\ContactVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * Le tableau est-il vide de valeurs ?
     *
     * @return bool
     */
    public function _array_vide(mixed $a)
    {
        foreach ($a as $k => $v) {
            if (!empty($v)) {
                return false;
            }
        }

        return true;
    }

    /**
     * ADMIN
     * JE SUIS une FERME / PAY ou une AMAP.
     */
    #[Route('contact/admin', name: 'contact_admin')]
    public function admin(): Response
    {
        // Sécurité
        $this->denyAccessUnlessGranted(ContactVoter::ACTION_CONTACT_ADMIN);

        $admins = $this->getAdminsForUser($this->getUser());

        return $this->render('contact/admin.html.twig', $admins);
    }

    #[Route('contact/admin_form/{id}', name: 'contact_admin_form')]
    public function admin_form(int $id, Email_Sender $emailSender): Response
    {
        $currentUser = $this->getUser();
        $admins = $this->getAdminsForUser($currentUser);

        $this->denyAccessUnlessGranted(ContactVoter::ACTION_CONTACT_ADMIN);
        $targetIds = $this->extractTargetIds(array_merge($admins['adminRegions'], $admins['adminDepartements']));
        if (!in_array($id, $targetIds)) {
            $this->exit_error();
        }

        $form = $this->formFactory->create(\PsrLib\Form\ContactTargetType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var \PsrLib\ORM\Entity\User $target */
                $target = $this
                    ->em
                    ->getRepository(\PsrLib\ORM\Entity\User::class)
                    ->findOneBy([
                        'id' => $id,
                    ])
                ;
                if ($currentUser->isAmapAdmin()) {
                    $statut = 'une AMAP';
                    $expediteur_nom = (string) $currentUser;
                    $expediteur_email = implode(';', $currentUser->getEmails()->toArray());
                } else {
                    $statut = 'un paysan';
                    $expediteur_nom = (string) $currentUser;
                    $expediteur_email = implode(';', $currentUser->getEmails()->toArray());
                }

                $emailSender->envoyerMailContactAdmin(
                    $target,
                    $statut,
                    (string) $form->get('title')->getData(),
                    (string) $form->get('content')->getData(),
                    $expediteur_nom,
                    $expediteur_email
                );

                $this->addFlash(
                    'notice_success',
                    'Le message a été envoyé avec succès.'
                );

                return $this->redirectHtmx('/contact/admin');
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé ! Une adresse email n\'est sans doute pas valide.'
            );

            return $this->redirectHtmx('/contact/admin');
        }

        return $this->render('contact/admin_form.html.twig', [
            'form' => $form->createView(),
            'currentPath' => $this->request->getPathInfo(),
        ]);
    }

    private function getAdminsForUser(PsrLib\ORM\Entity\User $user): array
    {
        return [
            'adminRegions' => $this->getUser()?->getRegion()?->getAdmins()->toArray() ?? [],
            'adminDepartements' => $this->getUser()?->getDepartement()?->getAdmins()->toArray() ?? [],
        ];
    }

    /**
     * MON AMAP
     * JE SUIS un AMAPIEN ou un RÉFÉRENT PRODUIT.
     */
    #[Route('contact/mon_amap/{amap_id}', name: 'contact_mon_amap')]
    public function mon_amap(int $amap_id = null, Email_Sender $emailSender): Response
    {
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_CONTACT_OWN_AMAP, $amap);

        $currentUser = $this->getUser();

        $form = $this->formFactory->create(\PsrLib\Form\ContactType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $emailSender->envoyerMailContactMonAmap($amap, $currentUser, $form->getData());
                $this->addFlash(
                    'notice_success',
                    'Le message a été envoyé avec succès.'
                );

                return $this->redirectToRoute('contact_mon_amap', [
                    'amap_id' => $amap->getId(),
                ]);
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé ! Une adresse email n\'est sans doute pas valide.'
            );
        }

        return $this->render('contact/mon_amap.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    /**
     * RÉFÉRENT PRODUIT
     * JE SUIS un AMAPIEN ou un RÉFÉRENT PRODUIT.
     */
    #[Route('contact/referent_produit/{amap_id}', name: 'contact_referent_produit')]
    public function referent_produit(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_CONTACT_OWN_AMAP, $amap);

        $params = $this->getFermesReferentsForAmap($amap);
        $params['amap'] = $amap;

        return $this->render('contact/referent.html.twig', $params);
    }

    #[Route('contact/referent_produit_form/{amap_id}/{ferme_id}', name: 'contact_referent_produit_form')]
    public function referent_produit_form(int $amap_id, int $ferme_id, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_CONTACT_OWN_AMAP, $amap);

        /** @var \PsrLib\ORM\Entity\User $currentUser Granted by permission */
        $currentUser = $this->getUser();

        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $fermes = $this->getFermesReferentsForAmap($amap);
        if (!in_array($ferme, $fermes['fermes'], true)) {
            $this->exit_error();
        }

        $form = $this->formFactory->create(\PsrLib\Form\ContactTargetType::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $statut = 'amapien';
                if ($currentUser->isRefProduit()) {
                    $statut = 'référent produit';
                }

                $prenom = $currentUser->getName()->getFirstName() ?? '';
                $nom = $currentUser->getName()->getLastName() ?? '';
                $email_from = (string) $currentUser->getEmails()->first();

                $refProduits = $this
                    ->em
                    ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                    ->getRefProduitFromAmapFerme($amap, $ferme)
                ;
                $emails = [];
                foreach ($refProduits as $refProduit) {
                    foreach ($refProduit->getUser()->getEmails() as $email) {
                        $emails[] = $email;
                    }
                }

                $emailSender->envoyerMailContactRefProduit(
                    $emails,
                    $statut,
                    (string) $form->get('title')->getData(),
                    (string) $form->get('content')->getData(),
                    $nom,
                    $prenom,
                    $amap,
                    $email_from,
                    $currentUser
                );

                $this->addFlash(
                    'notice_success',
                    'Le message a été envoyé avec succès.'
                );

                return $this->redirectHtmx('/contact/referent_produit/'.$amap->getId());
            }

            $this->addFlash(
                'notice_error',
                'Le message n\'a pas pu être envoyé ! Une adresse email n\'est sans doute pas valide.'
            );

            return $this->redirectHtmx('/contact/referent_produit/'.$amap->getId());
        }

        return $this->render('contact/referent_form.html.twig', [
            'form' => $form->createView(),
            'currentPath' => $this->request->getPathInfo(),
        ]);
    }

    private function getFermesReferentsForAmap(PsrLib\ORM\Entity\Amap $amap): array
    {
        $fermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amap)
        ;

        $amapienRepo = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class);
        $refProduits = [];
        foreach ($fermes as $ferme) {
            $refProduits[$ferme->getId()] = $amapienRepo
                ->getRefProduitFromAmapFerme($amap, $ferme)
            ;
        }

        return [
            'fermes' => $fermes,
            'refProduits' => $refProduits,
        ];
    }

    /**
     * Mentions légales.
     */
    #[Route('contact/mentions_legales', name: 'contact_mentions_legales')]
    public function mentions_legales(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        return $this->render('contact/mentions-legales.html.twig');
    }

    /**
     * @param \PsrLib\ORM\Entity\User[] $users
     */
    private function extractTargetIds(array $users): array
    {
        return array_map(fn (PsrLib\ORM\Entity\User $user) => (string) $user->getId(), $users);
    }
}
