<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\MailjetEntryProvider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api/mailjet', name: 'api_mailjet', methods: ['GET'])]
    public function mailjet(MailjetEntryProvider $mailjetProvider): Response
    {
        if ($this->request->headers->get('X-Authorization') !== getenv('API_MAILJET_TOKEN')) {
            $this->exit_error();
        }

        $data = $mailjetProvider->getData();

        $serialized = $this->serializer->serialize($data, 'json');

        $response = new Response(
            $serialized,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        $response->prepare($this->request);

        return $response;
    }
}
