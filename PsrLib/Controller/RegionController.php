<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\Security\Voters\NetworkVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegionController extends AbstractController
{
    #[Route('/region', name: 'region_index')]
    public function index(): Response
    {
        return $this->accueil();
    }

    /**
     * Accueil | Affiche toutes les régions.
     */
    #[Route('/region/accueil', name: 'region_accueil')]
    public function accueil(): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        $regions = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Region::class)
            ->findAllOrdered()
        ;

        return $this->render('region/index.html.twig', [
            'region_all' => $regions,
        ]);
    }

    /**
     * Affiche tous les départements d'une région.
     */
    #[Route('/region/departement/{region_id}', name: 'region_departement')]
    public function departement(int $region_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $departements = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Departement::class)
            ->findBy([
                'region' => $region,
            ])
        ;

        return $this->render('departement/index.html.twig', [
            'region' => $region,
            'departement_all' => $departements,
        ]);
    }

    /**
     * Afficher les Admin d'une région.
     */
    #[Route('/region/reg_admin/{region_id}', name: 'region_reg_admin')]
    public function reg_admin(int $region_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);
        $form = $this->formFactory->create(\PsrLib\Form\UserSelectType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var int $userId */
            $userId = $form->get('user')->getData();
            $user = $this->em->find(\PsrLib\ORM\Entity\User::class, $userId);
            if ($region->getAdmins()->contains($user)) {
                $this->addFlash(
                    'notice_error',
                    'L\'administrateur "'.$user->getName().'" existe déjà dans la région !'
                );

                return $this->redirectToRoute('region_reg_admin', [
                    'region_id' => $region->getId(),
                ]);
            }

            $this->addFlash(
                'notice_success',
                'L\'administrateur a été ajouté avec succès.'
            );
            $user->addAdminRegion($region);
            $this->em->flush();

            return $this->redirectToRoute('region_reg_admin', [
                'region_id' => $region->getId(),
            ]);
        }

        return $this->render('region/admin.html.twig', [
            'region' => $region,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Suppression d'un Admin d'un réseau.
     */
    #[Route('/region/remove_reg_admin/{region_id}/{reg_adm_id}', name: 'region_remove_reg_admin')]
    public function remove_reg_admin(int $region_id, int $reg_adm_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        /** @var \PsrLib\ORM\Entity\User $admin */
        $admin = $this->em->getReference(\PsrLib\ORM\Entity\User::class, $reg_adm_id);
        $admin->removeAdminRegion($region);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'administrateur a été retiré avec succès.'
        );

        return $this->redirectToRoute('region_reg_admin', [
            'region_id' => $region->getId(),
        ]);
    }

    /**
     * Afficher les Admin du département.
     */
    #[Route('/region/dep_admin/{dep_id}', name: 'region_dep_admin')]
    public function dep_admin(int $dep_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $dep_id);

        $form = $this->formFactory->create(\PsrLib\Form\UserSelectType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var int $userId */
            $userId = $form->get('user')->getData();
            $user = $this->em->find(\PsrLib\ORM\Entity\User::class, $userId);
            if ($departement->getAdmins()->contains($user)) {
                $this->addFlash(
                    'notice_error',
                    'L\'administrateur "'.$user->getName().'" existe déjà dans le département !'
                );

                return $this->redirectToRoute('region_dep_admin', [
                    'dep_id' => $departement->getId(),
                ]);
            }

            $this->addFlash(
                'notice_success',
                'L\'administrateur a été ajouté avec succès.'
            );
            $user->addAdminDepartment($departement);
            $this->em->flush();

            return $this->redirectToRoute('region_dep_admin', [
                'dep_id' => $departement->getId(),
            ]);
        }

        return $this->render('departement/admin.html.twig', [
            'departement' => $departement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Suppression d'un Admin d'un département.
     */
    #[Route('/region/remove_dep_admin/{dep_id}/{dep_adm_id}', name: 'region_remove_dep_admin')]
    public function remove_dep_admin(int $dep_id, int $dep_adm_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $dep_id);

        /** @var \PsrLib\ORM\Entity\User $admin */
        $admin = $this->em->getReference(\PsrLib\ORM\Entity\User::class, $dep_adm_id);
        $admin->removeAdminDepartment($departement);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'administrateur a été retiré avec succès.'
        );

        return $this->redirectToRoute('region_dep_admin', [
            'dep_id' => $departement->getId(),
        ]);
    }

    #[Route('/region/region_logo/{region_id}', name: 'region_region_logo')]
    public function region_logo(int $region_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Region $region */
        $region = $this->findOrExit(\PsrLib\ORM\Entity\Region::class, $region_id);

        $form = $this->formFactory->create(\PsrLib\Form\EntityWithLogoType::class, $region);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedImg = $form->get('img')->getData();
            if (null !== $uploadedImg) {
                $logo = $this
                    ->uploader
                    ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\Logo::class)
                ;
                $region->setLogo($logo);
            }
            $this->em->flush();

            return $this->redirectToRoute('region_index');
        }

        return $this->render('region/logo.html.twig', [
            'region' => $region,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/region/departement_logo/{departement_id}', name: 'region_departement_logo')]
    public function departement_logo(int $departement_id): Response
    {
        $this->denyAccessUnlessGranted(NetworkVoter::ACTION_NETWORK_MANAGE);

        /** @var \PsrLib\ORM\Entity\Departement $departement */
        $departement = $this->findOrExit(\PsrLib\ORM\Entity\Departement::class, $departement_id);

        $form = $this->formFactory->create(\PsrLib\Form\EntityWithLogoType::class, $departement);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedImg = $form->get('img')->getData();
            if (null !== $uploadedImg) {
                $logo = $this
                    ->uploader
                    ->uploadFile($uploadedImg, \PsrLib\ORM\Entity\Files\Logo::class)
                ;
                $departement->setLogo($logo);
            }
            $this->em->flush();

            return $this->
                redirectToRoute('region_departement', [
                    'region_id' => $departement->getRegion()->getId(),
                ]);
        }

        return $this->render('departement/logo.html.twig', [
            'departement' => $departement,
            'form' => $form->createView(),
        ]);
    }
}
