<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\Response\RedirectToRefererResponse;
use PsrLib\Exception\AdhesionImportException;
use PsrLib\ORM\Entity\Adhesion;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use PsrLib\ORM\Repository\AdhesionRepository;
use PsrLib\Services\AdhesionImport;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\EntityBuilder\AdhesionBuilderAmap;
use PsrLib\Services\EntityBuilder\AdhesionBuilderFerme;
use PsrLib\Services\MPdfGeneration;
use PsrLib\Services\Security\Voters\AdhesionAmapAmapienVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapienVoter;
use PsrLib\Services\Security\Voters\AdhesionAmapVoter;
use PsrLib\Services\Security\Voters\AdhesionCreatorUserVoter;
use PsrLib\Services\Security\Voters\AdhesionFermeVoter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class AdhesionController extends AbstractController
{
    public function __construct(
        private readonly MPdfGeneration $mpdfgeneration,
        private readonly Email_Sender $emailSender
    ) {
    }

    #[Route('adhesion/import', name: 'adhesion_import')]
    public function import(
        AdhesionImport $adhesionImport,
        AdhesionBuilderAmap $adhesionBuilderAmap,
        AdhesionBuilderFerme $adhesionBuilderFerme
    ): Response {
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_IMPORT);
        $errors = [];

        $action = null;
        if (isset($_FILES['import_amap'])) {
            $action = 'amap';

            try {
                $adhesionImport->doImport(
                    $_FILES['import_amap'],
                    AdhesionValueAmap::class,
                    $adhesionBuilderAmap,
                    $this->getUser()
                );
            } catch (AdhesionImportException $e) {
                return $this->render('adhesion/import.html.twig', [
                    'errors' => $e->getErrors(),
                    'action' => $action,
                ]);
            }

            $this->addFlash(
                'notice_success',
                'Import validé.'
            );

            return $this->redirectToRoute('adhesion_import');
        }

        if (isset($_FILES['import_paysan'])) {
            $action = 'paysan';

            try {
                $adhesionImport->doImport(
                    $_FILES['import_paysan'],
                    AdhesionValueFerme::class,
                    $adhesionBuilderFerme,
                    $this->getUser()
                );
            } catch (AdhesionImportException $e) {
                return $this->render('adhesion/import.html.twig', [
                    'errors' => $e->getErrors(),
                    'action' => $action,
                ]);
            }

            $this->addFlash(
                'notice_success',
                'Import validé.'
            );

            return $this->redirectToRoute('adhesion_import');
        }

        return $this->render('adhesion/import.html.twig', [
            'errors' => $errors,
            'action' => $action,
        ]);
    }

    #[Route('adhesion/amap', name: 'adhesion_amap')]
    public function amap(): Response
    {
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_LIST);

        $this->redirectFromStoredGetParamIfExist('adhesion_amap');

        /** @var \PsrLib\ORM\Entity\User $currentUser Granted by permission */
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAdhesionAmapType::class, null, [
            'currentUser' => $currentUser,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmap::class)
            ->search(
                $searchForm->getData(),
                $currentUser
            )
        ;

        return $this->render('adhesion/amap.html.twig', [
            'searchForm' => $searchForm->createView(),
            'adhesions' => $adhesions,
        ]);
    }

    #[Route('adhesion/amap_download/{id}', name: 'adhesion_amap_download')]
    public function amap_download(int $id): Response
    {
        /** @var AdhesionAmap|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmap::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionAmapVoter::ACTION_ADHESION_AMAP_DOWNLOAD, $adhesion);

        $response = new BinaryFileResponse(
            Adhesion::ADHESION_PATH.$adhesion->getVoucherFileName()
        );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf(
                'recus_amap_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            )
        );

        return $response;
    }

    #[Route('adhesion/amap_delete/{id}', name: 'adhesion_amap_delete', methods: ['POST'])]
    public function amap_delete(int $id): Response
    {
        /** @var AdhesionAmap|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmap::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée.'
        );

        return new RedirectToRefererResponse('/adhesion/amap');
    }

    #[Route('adhesion/amap_action', name: 'adhesion_amap_action')]
    public function amap_action(Request $request): Response
    {
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_LIST);

        $response = null;
        if ($request->request->has('action-delete')) {
            $this->_bulk_delete(AdhesionAmap::class, AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_DELETE);
        } elseif ($request->request->has('action-generate')) {
            $this->_bulk_generate(AdhesionAmap::class, AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_GENERATE);
        } elseif ($request->request->has('action-download')) {
            $response = $this->_bulk_download(AdhesionAmap::class, AdhesionAmapVoter::ACTION_ADHESION_AMAP_DOWNLOAD);
        } elseif ($request->request->has('action-notify')) {
            $this->_bulk_notify(AdhesionAmap::class, AdhesionCreatorUserVoter::ACTION_ADHESION_AMAP_DELETE);
        }

        if (null === $response) {
            $response = new RedirectToRefererResponse('/adhesion/amap');
        }

        return $response;
    }

    #[Route('adhesion/paysan', name: 'adhesion_paysan')]
    public function paysan(): Response
    {
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_LIST);
        $this->redirectFromStoredGetParamIfExist('adhesion_ferme');

        /** @var \PsrLib\ORM\Entity\User $currentUser Granted by permission */
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAdhesionPaysanType::class, null, [
            'currentUser' => $currentUser,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $adhesions = $this
            ->em
            ->getRepository(AdhesionFerme::class)
            ->search(
                $searchForm->getData(),
                $currentUser
            )
        ;

        return $this->render('adhesion/ferme.html.twig', [
            'searchForm' => $searchForm->createView(),
            'adhesions' => $adhesions,
        ]);
    }

    #[Route('adhesion/paysan_action', name: 'adhesion_paysan_action')]
    public function paysan_action(Request $request): Response
    {
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_LIST);

        $response = null;
        if ($request->request->has('action-delete')) {
            $this->_bulk_delete(AdhesionFerme::class, AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_DELETE);
        } elseif ($request->request->has('action-generate')) {
            $this->_bulk_generate(AdhesionFerme::class, AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_GENERATE);
        } elseif ($request->request->has('action-download')) {
            $response = $this->_bulk_download(AdhesionFerme::class, AdhesionFermeVoter::ACTION_ADHESION_FERME_DOWNLOAD);
        } elseif ($request->request->has('action-notify')) {
            $this->_bulk_notify(AdhesionFerme::class, AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_DELETE);
        }

        if (null === $response) {
            $response = new RedirectToRefererResponse('/adhesion/paysan');
        }

        return $response;
    }

    #[Route('adhesion/paysan_download/{id}', name: 'adhesion_paysan_download')]
    public function paysan_download(int $id): Response
    {
        /** @var AdhesionFerme|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionFerme::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionFermeVoter::ACTION_ADHESION_FERME_DOWNLOAD, $adhesion);

        $response = new BinaryFileResponse(
            Adhesion::ADHESION_PATH.$adhesion->getVoucherFileName()
        );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf(
                'recus_ferme_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            )
        );

        return $response;
    }

    #[Route('adhesion/paysan_delete/{id}', name: 'adhesion_paysan_delete', methods: ['POST'])]
    public function paysan_delete(int $id): Response
    {
        /** @var AdhesionAmap|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionFerme::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionCreatorUserVoter::ACTION_ADHESION_FERME_DELETE, $adhesion);

        $this->em->remove($adhesion);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Adhésion supprimée.'
        );

        return new RedirectToRefererResponse('/adhesion/paysan');
    }

    #[Route('adhesion/amapien_download/{id}', name: 'adhesion_amapien_download')]
    public function amapien_download(int $id): Response
    {
        /** @var AdhesionAmapien|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapien::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionAmapienVoter::ACTION_ADHESION_AMAPIEN_DOWNLOAD, $adhesion);

        $response = new BinaryFileResponse(
            Adhesion::ADHESION_PATH.$adhesion->getVoucherFileName()
        );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf(
                'recus_amapien_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            )
        );

        return $response;
    }

    #[Route('adhesion/amap_amapien_download/{id}', name: 'adhesion_amap_amapien_download')]
    public function amap_amapien_download(int $id): Response
    {
        /** @var AdhesionAmapAmapien|null $adhesion */
        $adhesion = $this->em->getRepository(AdhesionAmapAmapien::class)->findOneBy(['id' => $id]);
        $this->denyAccessUnlessGranted(AdhesionAmapAmapienVoter::ACTION_ADHESION_AMAP_AMAPIEN_DOWNLOAD, $adhesion);

        $response = new BinaryFileResponse(
            Adhesion::ADHESION_PATH.$adhesion->getVoucherFileName()
        );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf(
                'recus_amapien_%s_%s.pdf',
                $adhesion->getValue()->getVoucherNumber(),
                $adhesion->getValue()->getProcessingDate()->format('d_m_Y')
            )
        );

        return $response;
    }

    #[Route('adhesion/mes_recus_amap', name: 'adhesion_mes_recus_amap')]
    public function mes_recus_amap(): Response
    {
        $this->denyAccessUnlessGranted(AdhesionAmapVoter::ACTION_ADHESION_AMAP_LIST_SELF);

        /** @var \PsrLib\ORM\Entity\User $currentUser */
        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmap::class)
            ->findGeneratedByUser($currentUser)
        ;

        return $this->render('adhesion/amap_self.html.twig', [
            'adhesions' => $adhesions,
        ]);
    }

    #[Route('adhesion/mes_recus_ferme', name: 'adhesion_mes_recus_ferme')]
    public function mes_recus_ferme(): Response
    {
        $this->denyAccessUnlessGranted(AdhesionFermeVoter::ACTION_ADHESION_FERME_LIST_SELF);

        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionFerme::class)
            ->findGeneratedByPaysan($currentUser->getPaysan())
        ;

        return $this->render('adhesion/ferme_self.html.twig', [
            'adhesions' => $adhesions,
        ]);
    }

    /**
     * @return int[]
     */
    private function _get_selected_ids(): array
    {
        /** @var array<int, string> $selectedIds */
        $selectedIds = $this->request->request->get('selected') ?? [];

        return array_keys($selectedIds);
    }

    /**
     * @param class-string<\PsrLib\ORM\Entity\Adhesion> $adhesionClass
     */
    private function _bulk_delete(string $adhesionClass, string $permission): void
    {
        Assertion::subclassOf($adhesionClass, \PsrLib\ORM\Entity\Adhesion::class);
        $selectedIds = $this->_get_selected_ids();

        /** @var AdhesionRepository $repo */
        $repo = $this->em->getRepository($adhesionClass);
        /** @var AdhesionController[] $adhesions */
        $adhesions = $repo->findByMultipleIds($selectedIds);
        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_error',
                'Aucune adhésion sélectionnée !'
            );

            return;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        foreach ($adhesions as $adhesion) {
            $this->em->remove($adhesion);
        }

        $this->em->flush();
        $this->addFlash(
            'notice_success',
            sprintf('%d adhésions supprimées avec succès.', count($adhesions))
        );
    }

    /**
     * @param class-string<\PsrLib\ORM\Entity\Adhesion> $adhesionClass
     */
    private function _bulk_generate(string $adhesionClass, string $permission): void
    {
        $evm = $this->em->getEventManager();

        $selectedIds = $this->_get_selected_ids();

        /** @var AdhesionRepository $repo */
        $repo = $this->em->getRepository($adhesionClass);

        /** @var \PsrLib\ORM\Entity\Adhesion[] $adhesions */
        $adhesions = $repo
            ->findByMultipleIds($selectedIds, \PsrLib\ORM\Entity\Adhesion::STATE_DRAFT)
        ;

        $adhesions = array_filter($adhesions, function (Adhesion $adhesion) {
            if ($adhesion instanceof AdhesionAmap) {
                return null !== $adhesion->getAmap();
            }

            return $adhesion;
        });

        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_success',
                'Aucune adhésion en attente : aucun reçu ne sera généré.'
            );

            return;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        foreach ($adhesions as $adhesion) {
            $adhesion->setGenerationDate(Carbon::now());

            try {
                $filename = $this->mpdfgeneration->genererAdhesion($adhesion);
            } catch (\PsrLib\Exception\AdhesionGenerationCreatorNoCityExecption $e) {
                $this->addFlash(
                    'notice_error',
                    $e->getMessage()
                );

                return;
            }

            $adhesion->setVoucherFileName($filename);
            $adhesion->setState(\PsrLib\ORM\Entity\Adhesion::STATE_GENERATED);

            $evm->dispatchEvent(
                \PsrLib\ORM\EventSubscriber\Events::ADHESION_POST_GENERATE,
                new \PsrLib\ORM\EventSubscriber\AdhesionPostGenerateEventArgs($adhesion)
            );
        }

        $this->em->flush();

        $this->addFlash(
            'notice_success',
            sprintf('%d adhésions générées.', count($adhesions))
        );
    }

    /**
     * @param class-string<\PsrLib\ORM\Entity\Adhesion> $adhesionClass
     */
    private function _bulk_download(string $adhesionClass, string $permission): ?Response
    {
        $selectedIds = $this->_get_selected_ids();

        /** @var AdhesionRepository $repo */
        $repo = $this->em->getRepository($adhesionClass);

        /** @var AdhesionAmap[] $adhesions */
        $adhesions = $repo->findByMultipleIds($selectedIds, \PsrLib\ORM\Entity\Adhesion::STATE_GENERATED);
        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_error',
                'Aucune adhésion sélectionnée !'
            );

            return null;
        }

        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        $zip = new \ZipArchive();
        $tmpFileName = sprintf('%s/bulk_download_%s.zip', sys_get_temp_dir(), \Ramsey\Uuid\Uuid::uuid4());
        if (true !== $zip->open($tmpFileName, \ZipArchive::CREATE)) {
            throw new \RuntimeException('Unable to create zip file'.$tmpFileName);
        }

        foreach ($adhesions as $adhesion) {
            $zip->addFile(Adhesion::ADHESION_PATH.$adhesion->getVoucherFileName(), $adhesion->getVoucherFileName());
        }
        $zip->close();

        return BinaryFileResponseFactory::createFromTempFilePath($tmpFileName, 'recus.zip');
    }

    /**
     * @param class-string<\PsrLib\ORM\Entity\Adhesion> $adhesionClass
     */
    private function _bulk_notify(string $adhesionClass, string $permission): void
    {
        $selectedIds = $this->_get_selected_ids();
        /** @var AdhesionRepository $repo */
        $repo = $this->em->getRepository($adhesionClass);

        $adhesions = $repo->findByMultipleIds($selectedIds, \PsrLib\ORM\Entity\Adhesion::STATE_GENERATED);

        if (0 === count($adhesions)) {
            $this->addFlash(
                'notice_error',
                'Aucune adhésion générée sélectionnée !'
            );

            return;
        }
        $this->denyAccessUnlessGrantedMultiple($permission, $adhesions);

        if (AdhesionAmap::class === $adhesionClass) {
            $this->emailSender->envoyerAdhesionAmapNotification($adhesions); // @phpstan-ignore-line
        } elseif (AdhesionFerme::class === $adhesionClass) {
            $this->emailSender->envoyerAdhesionFermeNotification($adhesions); // @phpstan-ignore-line
        } else {
            throw new \LogicException('Invalid adhesion class');
        }

        $this->addFlash(
            'notice_success',
            'Notifications envoyées avec succès.'
        );
    }
}
