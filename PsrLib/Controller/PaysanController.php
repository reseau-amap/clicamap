<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Exporters\ExcelGeneratorExportPaysan;
use PsrLib\Services\Password\PasswordEncoder;
use PsrLib\Services\Password\PasswordGenerator;
use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use PsrLib\Services\Security\Voters\FermeVoter;
use PsrLib\Services\Security\Voters\PaysanVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaysanController extends AbstractController
{
    public const HOME_PATH_ID = 'home_paysan';

    /**
     * Index.
     */
    #[Route('paysan', name: 'paysan_index')]
    public function index(): Response
    {
        return $this->accueil();
    }

    #[Route('paysan/accueil', name: 'paysan_accueil')]
    public function accueil(): Response
    {
        // Sécurité
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_LIST_ADMIN);
        $this->session_store_home_path(self::HOME_PATH_ID);

        $this->redirectFromStoredGetParamIfExist('paysan');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchPaysanType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->search($searchForm->getData())
        ;

        return $this->render('paysan/display_all.html.twig', [
            'paysans' => $paysans,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * Afficher le détail d'une fiche paysan.
     */
    #[Route('paysan/display/{paysan_id}', name: 'paysan_display')]
    public function display(int $paysan_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan);

        return $this->render('paysan/display.html.twig', [
            'paysan' => $paysan,
        ]);
    }

    /**
     * Ajout / Édition des informations générales.
     */
    #[Route('paysan/informations_generales/{paysan_id}', name: 'paysan_informations_generales')]
    public function informations_generales(int $paysan_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL, $paysan);

        $form = $this->formFactory->create(\PsrLib\Form\PaysanEditType::class, $paysan);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($paysan);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le paysan a été mis à jour avec succès.'
            );

            return $this->redirectToRoute('paysan_index');
        }

        return $this->render('paysan/informations_generales.html.twig', [
            'paysan' => $paysan,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('paysan/informations_generales_creation', name: 'paysan_informations_generales_creation')]
    public function informations_generales_creation(): Response
    {
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_CREATE);

        $user = new \PsrLib\ORM\Entity\User();
        $paysan = new \PsrLib\ORM\Entity\Paysan();
        $paysan->setUser($user);
        $form = $this->formFactory->create(\PsrLib\Form\PaysanCreationType::class, $paysan, [
            'current_user' => $this->getUser(),
            'default_ferme_id' => $this->request->query->get('ferme_id'),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ferme = $form->get('ferme')->getData();
            $paysan->addFerme($ferme);
            $this->em->persist($paysan);
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le paysan a été créé avec succès. Vous pouvez en rajouter un autre.'
            );

            return $this->redirectToRoute('paysan_informations_generales_creation', [
                'ferme_id' => $ferme->getId(),
            ]);
        }

        return $this->render('paysan/informations_generales.html.twig', [
            'paysan' => $paysan,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('paysan/recherche_amap', name: 'paysan_recherche_amap')]
    public function recherche_amap(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        return $this->render('paysan/recherche_amap.html.twig');
    }

    /**
     * Télécharger l'ensemble des paysans de la session recherche.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    #[Route('paysan/telecharger_paysans', name: 'paysan_telecharger_paysans')]
    public function telecharger_paysans(ExcelGeneratorExportPaysan $excelGeneratorExportPaysan): Response
    {
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_DOWNLOAD_LIST);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchPaysanType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        /** @var \PsrLib\DTO\SearchPaysanState $paysanSearchState */
        $paysanSearchState = $searchForm->getData();

        /** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->search($paysanSearchState)
        ;

        if (0 === count($paysans)) {
            return $this->redirectToRoute('paysan_index');
        }

        $fileName = $excelGeneratorExportPaysan->genererExportPaysans($paysans, $paysanSearchState);

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'liste_des_paysans_'.date('d_m_Y').'.xls');
    }

    #[Route('paysan/telecharger_paysans_regroupement', name: 'paysan_telecharger_paysans_regroupement')]
    public function telecharger_paysans_regroupement(ExcelGeneratorExportPaysan $excelGeneratorExportPaysan): Response
    {
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT);

        /** @var \PsrLib\ORM\Entity\User $currentUser */
        $currentUser = $this->getUser();

        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($currentUser->getFermesAsRegroupement())
        ;

        $fileName = $excelGeneratorExportPaysan->genererExportPaysans($paysans);

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'liste_des_paysans_'.date('d_m_Y').'.xls');
    }

    /**
     * Télécharger.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    #[Route('paysan/telecharger_ferme_referents_produit/{ferme_id}', name: 'paysan_telecharger_ferme_referents_produit')]
    public function telecharger_ferme_referents_produit(int $ferme_id, Spreadsheet $excel): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme);

        /** @var \PsrLib\ORM\Entity\Amapien[] $ferme_referent_produit_all */
        $ferme_referent_produit_all = $ferme->getAmapienRefs()->toArray();
        $time = 'extrait le '.date('d/m/Y');
        $nb_amapiens = count($ferme_referent_produit_all);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $excel->setActiveSheetIndex(0);

        $excel->getActiveSheet()->setTitle('Référents produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $excel->getActiveSheet()->setCellValue('A4', 'AMAP');
        $excel->getActiveSheet()->setCellValue('B4', 'Nom');
        $excel->getActiveSheet()->setCellValue('C4', 'Prénom');
        $excel->getActiveSheet()->setCellValue('D4', 'Email');
        $excel->getActiveSheet()->setCellValue('E4', 'Téléphone 1');
        $excel->getActiveSheet()->setCellValue('F4', 'Téléphone 2');
        $excel->getActiveSheet()->setCellValue('G4', 'Adresse');
        $excel->getActiveSheet()->setCellValue('H4', 'Code Postal');
        $excel->getActiveSheet()->setCellValue('I4', 'Ville');

        $excel->getActiveSheet()->getStyle('A4:I4')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A4:I4')->getFont()->setBold(true);

        $i = 5;
        foreach ($ferme_referent_produit_all as $amapien) {
            $user = $amapien->getUser();
            $excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($amapien->getAmap()->getNom()));
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, strtoupper($user->getName()->getFirstName()));
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, ucfirst($user->getName()->getLastName()));
            $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('D'.$i, implode(', ', $user->getEmails()->toArray()));
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('E'.$i, $user->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('F'.$i, $user->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('G'.$i, $user->getAddress()->getAdress());
            $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $ville = $user->getVille();
            $excel->getActiveSheet()->setCellValueExplicit('H'.$i, null === $ville ? null : $ville->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('I'.$i, strtoupper($ville?->getNom() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $excel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($style);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = 'Référents produit';
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A1', $titre);

        $outputFileName = tempnam(sys_get_temp_dir(), 'exportFermeReferent_');

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, 'Excel5');
        // force user to download the Excel file without writing it to server's HD
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, 'liste_des_referents_produit_'.date('d_m_Y').'.xls');
    }

    /**
     * Créer un mot de passe pour un paysan et lui envoyer par email.
     */
    #[Route('paysan/password/{paysan_id}', name: 'paysan_password')]
    public function password(int $paysan_id, PasswordGenerator $passwordGenerator, PasswordEncoder $passwordEncoder, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan);

        $password = $passwordGenerator->generatePassword();
        $pass_hash = $passwordEncoder->encodePassword($password);

        // EMAIL
        $user = $paysan->getUser();
        $user->setPassword($pass_hash);
        $paysan->setEtat(ActivableUserInterface::ETAT_ACTIF);
        $this->em->flush();

        $emailSender->envoyerMailMdp($user, $password);

        $this->addFlash(
            'notice_success',
            'Le mot de passe a été envoyé avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    /**
     * Supprimer une fiche paysan.
     */
    #[Route('paysan/remove_paysan/{paysan_id}', name: 'paysan_remove_paysan')]
    public function remove_paysan(int $paysan_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_CHANGE_CR, $paysan);

        $this->em->remove($paysan);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le paysan a été supprimé avec succès.'
        );

        return $this->redirectToRoute('paysan_index');
    }

    /**
     * Désactiver une fiche paysan.
     */
    #[Route('paysan/de_activate_paysan/{paysan_id}', name: 'paysan_de_activate_paysan')]
    public function de_activate_paysan(int $paysan_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        $paysan = $this->findOrExit(\PsrLib\ORM\Entity\Paysan::class, $paysan_id);
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysan);

        $paysan->setEtat(
            \PsrLib\ORM\Entity\ActivableUserInterface::ETAT_ACTIF === $paysan->getEtat()
                ? \PsrLib\ORM\Entity\ActivableUserInterface::ETAT_INACTIF
                : \PsrLib\ORM\Entity\ActivableUserInterface::ETAT_ACTIF
        );
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'état du paysan a été modifié avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('paysan/home_refprod', name: 'paysan_home_refprod')]
    public function home_refprod(): Response
    {
        $this->denyAccessUnlessGranted(PaysanVoter::ACTION_PAYSAN_LIST_REFPROD);
        $this->session_store_home_path(self::HOME_PATH_ID);
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($this->getUser()->getFermesAsRefproduit())
        ;

        return $this->render('paysan/display_all_refprod.html.twig', [
            'paysans' => $paysans,
        ]);
    }

    #[Route('paysan/home_regroupement/{regroupement_id}', name: 'paysan_home_regroupement')]
    public function home_regroupement(int $regroupement_id): Response
    {
        $fermeRegroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $regroupement_id);
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_MANAGE, $fermeRegroupement);
        $this->session_store_home_path(self::HOME_PATH_ID);
        $paysans = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Paysan::class)
            ->findByFermeMultiple($fermeRegroupement->getFermes()->toArray())
        ;

        return $this->render('paysan/display_all_regroupement.html.twig', [
            'paysans' => $paysans,
        ]);
    }
}
