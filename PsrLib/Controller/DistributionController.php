<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Carbon\Carbon;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\EntityBuilder\AmapDistributionBuilder;
use PsrLib\Services\EntityBuilder\AmapDistributionDetailMassEditDtoBuilder;
use PsrLib\Services\Exporters\ExcelGeneratorDistributionAmap;
use PsrLib\Services\Security\Voters\AmapienVoter;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\DistributionVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class DistributionController extends AbstractController
{
    public function __construct(
        private readonly Email_Sender $email_sender,
    ) {
    }

    #[Route('distribution/index', name: 'distribution_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_DISTRIBUTION_LIST);

        $this->redirectFromStoredGetParamIfExist('distribution_amap');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchDistributionAmapType::class, null, [
            'current_user' => $this->getUser(),
        ]);

        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $distributions = [];
        if ($searchForm->isValid()) {
            $distributions = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
                ->search($searchForm->getData())
            ;
        }

        $amap = $searchForm->get('amap')->getData();

        return $this->render('distribution/index.html.twig', [
            'distributions' => $distributions,
            'searchForm' => $searchForm->createView(),
            'amap' => $amap,
        ])
        ;
    }

    #[Route('distribution/detail/{d_id}', name: 'distribution_detail')]
    public function detail(int $d_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapDistribution $distribution */
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_DETAIL, $distribution);

        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionAmapAmapienInscription::class, null, [
            'distribution' => $distribution,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\Amapien[] $amapiens */
            $amapiens = $form->get('amapiens')->getData();
            foreach ($amapiens as $amapien) {
                $distribution->addAmapien($amapien);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Amapiens ajoutés avec succès.'
            );
            foreach ($amapiens as $amapien) {
                if ($amapien->estActif()) {
                    $this->email_sender->envoyerDistributionInscriptionAmapAmapien($distribution, $amapien->getUser());
                }
            }

            return $this->redirectToRoute('distribution_detail', ['d_id' => $distribution->getId()]);
        }

        return $this->render('distribution/detail.html.twig', [
            'distribution' => $distribution,
            'form' => $form->createView(),
        ]);
    }

    #[Route('distribution/amap_desinscription/{d_id}', name: 'distribution_amap_desinscription', methods: ['POST'])]
    public function amap_desinscription(int $d_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapDistribution $distribution */
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_DETAIL, $distribution);

        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $this->request->request->get('amapien'));

        if (!$distribution->getAmapiens()->contains($amapien)) {
            $this->exit_error();
        }

        $distribution->removeAmapien($amapien);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Amapiens désinscrits avec succès.'
        );

        if ($amapien->estActif()) {
            $this->email_sender->envoyerDistributionDesinscriptionAmapAmapien($distribution, $amapien->getUser());
        }

        return $this->redirectToRoute('distribution_detail', ['d_id' => $distribution->getId()]);
    }

    #[Route('distribution/amapien', name: 'distribution_amapien')]
    public function amapien(): Response
    {
        $this->denyAccessUnlessGranted(AmapienVoter::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN);

        $this->redirectFromStoredGetParamIfExist('distribution_amapien');

        $currentUser = $this->getUser();
        $searchState = new \PsrLib\DTO\SearchDistributionAmapienState($currentUser);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchDistributionAmapienType::class, $searchState, [
            'current_user' => $currentUser,
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $distributions = [];
        if ($searchForm->isValid()) {
            $distributions = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
                ->searchAmapienState($searchState)
            ;
        }

        return $this->render('distribution/liste.twig', [
            'distributions' => $distributions,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    #[Route('distribution/ajout/{amap_id}', name: 'distribution_ajout')]
    public function ajout(int $amap_id, AmapDistributionBuilder $amapDistributionPeriodBuilder): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_DISTRIBUTION_ADD, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionAjoutType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\DTO\AmapDistributionAjout $dto */
            $dto = $form->getData();

            $distributions = $amapDistributionPeriodBuilder->buildFromAjoutDTO($dto);
            foreach ($distributions as $distribution) {
                $this->em->persist($distribution);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été ajoutées avec succès.'
            );

            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('distribution/ajout.html.twig', [
            'form' => $form->createView(),
            'amap' => $amap,
        ]);
    }

    #[Route('distribution/edit/{id}', name: 'distribution_edit')]
    public function edit(int $id): Response
    {
        /** @var AmapDistribution $distribution */
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_EDIT, $distribution);

        $amap = $distribution->getAmapLivraisonLieu()->getAmap();
        $form = $this->formFactory->create(\PsrLib\Form\AmapDistributionEditType::class, $distribution, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'La distribution a été modifiée avec succès.'
            );

            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('distribution/edit.html.twig', [
            'form' => $form->createView(),
            'distribution' => $distribution,
        ]);
    }

    #[Route('distribution/supprimer/{id}', name: 'distribution_supprimer', methods: ['POST'])]
    public function supprimer(int $id): Response
    {
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_DELETE, $distribution);

        $this->distributionMarkToRemove($distribution);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'La distribution a été supprimée avec succès.'
        );

        return $this->redirectToRoute('distribution_index');
    }

    #[Route('distribution/supprimer_masse', name: 'distribution_supprimer_masse')]
    public function supprimer_masse(): Response
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(DistributionVoter::ACTION_DISTRIBUTION_DELETE, $distributions);

        $form = $this
            ->formFactory
            ->createBuilder()
            ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                'label' => 'Valider',
            ])
            ->getForm()
        ;
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $amap = $distributions[0]->getAmapLivraisonLieu()->getAmap();
            foreach ($distributions as $distribution) {
                $this->distributionMarkToRemove($distribution);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été supprimées avec succès.'
            );

            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('distribution/suppression_masse.html.twig', [
            'form' => $form->createView(),
            'distributions' => $distributions,
        ]);
    }

    #[Route('distribution/edit_masse', name: 'distribution_edit_masse')]
    public function edit_masse(AmapDistributionDetailMassEditDtoBuilder $amapDistributionDetailBuilder): Response
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(DistributionVoter::ACTION_DISTRIBUTION_EDIT, $distributions);

        $dto = $amapDistributionDetailBuilder
            ->buildFromMultipleDistributions($distributions)
        ;
        $form = $this
            ->formFactory
            ->create(\PsrLib\Form\AmapDistributionEditMasseType::class, $dto, [
                'amap' => $distributions[0]->getAmapLivraisonLieu()->getAmap(), // expect all distributions to be in the same amap
            ])
        ;
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $detail = $dto->getDetail();
            $ll = $dto->getLivraisonLieu();

            foreach ($distributions as $distribution) {
                $distribution->setDetail($detail);
                $distribution->setAmapLivraisonLieu($ll);
            }
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Les distributions ont été modifiées avec succès.'
            );

            $amap = $distributions[0]->getAmapLivraisonLieu()->getAmap();

            return $this->redirectToRoute('distribution_index');
        }

        return $this->render('distribution/edition_masse.html.twig', [
            'form' => $form->createView(),
            'distributions' => $distributions,
        ]);
    }

    #[Route('distribution/telecharger_masse', name: 'distribution_telecharger_masse')]
    public function telecharger_masse(ExcelGeneratorDistributionAmap $excelGeneratorDistributionAmap): Response
    {
        $distributions = $this->parseDistributionIdsFromRequest();
        $this->denyAccessUnlessGrantedMultiple(DistributionVoter::ACTION_DISTRIBUTION_DETAIL, $distributions);

        \Assert\Assertion::notEmpty($distributions);
        $amap = $distributions[0]->getAmapLivraisonLieu()->getAmap(); // Assert all distributions are from the same amap

        $outFileName = $excelGeneratorDistributionAmap->genererExportDistributionInscrits($distributions);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outFileName,
            sprintf('Distribution_%s_%s.xls', $amap->getNom(), Carbon::now()->format('Y_m_d')),
            ResponseHeaderBag::DISPOSITION_INLINE
        );
    }

    #[Route('distribution/inscription/{d_id}', name: 'distribution_inscription', methods: ['POST'])]
    public function inscription(string $d_id): Response
    {
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER, $distribution);

        $user = $this->getUser();
        $amapien = $user->getUserAmapienAmapForAmap($distribution->getAmapLivraisonLieu()->getAmap());
        $distribution->addAmapien($amapien);
        $this->em->flush();

        return $this->redirectToRoute('distribution_amapien');
    }

    #[Route('distribution/desinscription/{d_id}', name: 'distribution_desinscription', methods: ['POST'])]
    public function desinscription(string $d_id): Response
    {
        $distribution = $this->findOrExit(\PsrLib\ORM\Entity\AmapDistribution::class, $d_id);
        $this->denyAccessUnlessGranted(DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER, $distribution);

        $user = $this->getUser();
        $amapien = $user->getUserAmapienAmapForAmap($distribution->getAmapLivraisonLieu()->getAmap());
        $distribution->removeAmapien($amapien);
        $this->em->flush();

        return $this->redirectToRoute('distribution_amapien');
    }

    private function distributionMarkToRemove(AmapDistribution $distribution): void
    {
        foreach ($distribution->getAmapiens() as $amapien) {
            $this->email_sender->envoyerDistributionDesinscriptionAmapAmapien($distribution, $amapien->getUser());
        }
        $this->em->remove($distribution);
    }

    /**
     * @return \PsrLib\ORM\Entity\AmapDistribution[]
     */
    private function parseDistributionIdsFromRequest(): array
    {
        /** @var \PsrLib\ORM\Entity\AmapDistribution[] $distributions */
        $distributions = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapDistribution::class)
            ->findByMultipleIds(
                explode(',', (string) $this->request->query->get('ids', ''))
            )
        ;
        if (0 === count($distributions)) {
            $this->exit_error();
        }

        return $distributions;
    }
}
