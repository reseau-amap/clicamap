<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaintenanceController extends AbstractController
{
    #[Route('/maintenance', name: 'maintenance')]
    public function index(): Response
    {
        // Disable maintenance page if maintenance not enable to avoid used blocked on page
        if (($_ENV['MAINTENANCE_ENABLED'] ?? null) === 'false') {
            return $this->redirect('/');
        }

        return $this->render('maintenance/index.html.twig');
    }
}
