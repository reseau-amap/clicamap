<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */
class FermeRegroupementController extends AbstractController
{
    #[Route('ferme_regroupement', name: 'ferme_regroupement_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST);

        $regroupements = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\FermeRegroupement::class)
            ->findAll()
        ;

        return $this->render('ferme_regroupement/index.html.twig', [
            'regroupements' => $regroupements,
        ]);
    }

    #[Route('ferme_regroupement/ajout', name: 'ferme_regroupement_ajout')]
    public function ajout(): Response
    {
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST);

        $form = $this->formFactory->create(\PsrLib\Form\FermeRegroupementType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
            $regroupement = $form->getData();
            $this->em->persist($regroupement);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Regroupement ajouté'
            );

            return $this->redirectToRoute('ferme_regroupement_index');
        }

        return $this->render('ferme_regroupement/ajout.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('ferme_regroupement/supprimer/{r_id}', name: 'ferme_regroupement_supprimer', methods: ['POST'])]
    public function supprimer(int $r_id): Response
    {
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST);

        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $r_id);

        $this->em->remove($regroupement);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Regroupement supprimé'
        );

        return $this->redirectToRoute('ferme_regroupement_index');
    }

    #[Route('ferme_regroupement/admins/{r_id}', name: 'ferme_regroupement_admins')]
    public function admins(int $r_id): Response
    {
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST);

        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $r_id);

        $form = $this->formFactory->create(\PsrLib\Form\UserSelectType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var int $userId */
            $userId = $form->get('user')->getData();
            /** @var \PsrLib\ORM\Entity\User $user */
            $user = $this->em->find(\PsrLib\ORM\Entity\User::class, $userId);
            if ($regroupement->getAdmins()->contains($user)) {
                $this->addFlash(
                    'notice_error',
                    'L\'administrateur "'.$user->getName().'" existe déjà dans le regroupement !'
                );

                return $this
                    ->redirectToRoute('ferme_regroupement_admins', ['r_id' => $regroupement->getId()])
                ;
            }

            $this->addFlash(
                'notice_success',
                'L\'administrateur a été ajouté avec succès.'
            );
            $regroupement->addAdmin($user);
            $this->em->flush();

            return $this
                ->redirectToRoute('ferme_regroupement_admins', ['r_id' => $regroupement->getId()])
            ;
        }

        return $this->render('ferme_regroupement/admins.html.twig', [
            'regroupement' => $regroupement,
            'form' => $form->createView(),
        ]);
    }

    #[Route('ferme_regroupement/admins_remove/{r_id}/{u_id}', name: 'ferme_regroupement_admins_remove')]
    public function admins_remove(int $r_id, int $u_id): Response
    {
        $this->denyAccessUnlessGranted(FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST);

        /** @var \PsrLib\ORM\Entity\FermeRegroupement $regroupement */
        $regroupement = $this->findOrExit(\PsrLib\ORM\Entity\FermeRegroupement::class, $r_id);

        $user = $this->findOrExit(\PsrLib\ORM\Entity\User::class, $u_id);
        $regroupement->removeAdmin($user);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'L\'administrateur a été retiré avec succès.'
        );

        return $this->redirectToRoute('ferme_regroupement_admins', ['r_id' => $regroupement->getId()]);
    }
}
