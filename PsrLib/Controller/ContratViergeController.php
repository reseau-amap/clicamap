<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Carbon\Carbon;
use PsrLib;
use PsrLib\DTO\ModeleContratFrom5DTO;
use PsrLib\Services\ContratCalculatorGenerateResponse;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\EntityBuilder\ModeleContratCopy;
use PsrLib\Services\MPdfGeneration;
use PsrLib\Services\RequestHtmxUtils;
use PsrLib\Services\Security\Voters\AmapVoter;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContratViergeController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_contrat_vierge';

    #[Route('/contrat_vierge/accueil_admin', name: 'contrat_vierge_accueil_admin')]
    public function accueil_admin(): Response
    {
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_LIST_ADMIN);
        $this->session_store_home_path(self::HOME_PATH_ID);
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeType::class, null, [
            'currentUser' => $currentUser,
        ]);

        return $this->accueil($currentUser, $searchForm);
    }

    #[Route('/contrat_vierge/accueil_refprod', name: 'contrat_vierge_accueil_refprod')]
    public function accueil_refprod(): Response
    {
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_LIST_REFPROD);
        $this->session_store_home_path(self::HOME_PATH_ID);
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeAmapType::class, null, [
            'amap_choices' => $currentUser->getAmapsAsRefProduit(),
            'restrict_fermes_ref' => $currentUser,
        ]);

        return $this->accueil($currentUser, $searchForm, true);
    }

    #[Route('/contrat_vierge/accueil_amapadmin', name: 'contrat_vierge_accueil_amapadmin')]
    public function accueil_amapadmin(): Response
    {
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_LIST_AMAP);
        $this->session_store_home_path(self::HOME_PATH_ID);
        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchContratViergeAmapType::class, null, [
            'amap_choices' => $currentUser->getAdminAmaps(),
        ]);

        return $this->accueil($currentUser, $searchForm);
    }

    /**
     * Accueil.
     */
    private function accueil(PsrLib\ORM\Entity\User $currentUser, FormInterface $searchForm, bool $filterRefProd = false): Response
    {
        $this->redirectFromStoredGetParamIfExist('contrat_vierge');

        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        if ($searchForm->has('amapFerme')) {
            $amapFermeField = $searchForm->get('amapFerme');
            $fermeSelected = $amapFermeField->get('ferme')->getData();
            $amapSelected = $amapFermeField->get('amap')->getData();
        } else {
            $fermeSelected = $searchForm->get('ferme')->getData();
            $amapSelected = $searchForm->get('amap')->getData();
        }

        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->search($searchForm->getData())
        ;

        // filter ref prod contracts
        if ($filterRefProd) {
            $contrats = array_filter($contrats, fn (PsrLib\ORM\Entity\ModeleContrat $mc) => $currentUser->isRefProduitOfFerme($mc->getFerme()));
        }

        $mcCopyArgumentDTO = null;
        if (null !== $amapSelected && null !== $fermeSelected) {
            $mcCopyArgumentDTO = new \PsrLib\DTO\ControllerModeleContratCopyArgumentDTO($amapSelected, $fermeSelected);
        }

        return $this->render('contrat_vierge/display_all.html.twig', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
            'fermeSelected' => $fermeSelected,
            'amapSelected' => $amapSelected,
            'mcCopyArgumentDTO' => $mcCopyArgumentDTO,
        ]);
    }

    /**
     * Supprimer le contrat vierge.
     */
    #[Route('/contrat_vierge/remove/{mc_id}', name: 'contrat_vierge_remove')]
    public function remove(int $mc_id): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_REMOVE, $mc);

        $this->em->remove($mc);
        $this->em->flush();

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('/contrat_vierge/v2_contrat_copy/{ferme_id}/{amap_id}', name: 'contrat_vierge_v2_contrat_copy')]
    public function v2_contrat_copy(int $ferme_id, int $amap_id, ModeleContratCopy $modeleContratCopy): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $this->denyAccessUnlessGranted(
            \PsrLib\Services\Security\Voters\ModelContratCopyVoter::ACTION_CONTRACT_MODEL_COPY,
            new \PsrLib\DTO\ControllerModeleContratCopyArgumentDTO($amap, $ferme)
        );
        $form = $this->formFactory->create(\PsrLib\Form\ModeleContratCopyFormType::class, null, [
            'amap' => $amap,
            'ferme' => $ferme,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newMc = $modeleContratCopy->copy($form->get('mc')->getData());

            $this->em->persist($newMc);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Contrat vierge copié avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('contrat_vierge/copy.html.twig', [
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/contrat_vierge/v2_state_to_validate/{mc_id}', name: 'contrat_vierge_v2_state_to_validate')]
    public function v2_state_to_validate(int $mc_id, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $mc_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_EDIT, $mc);

        $mc->setEtat(\PsrLib\Enum\ModeleContratEtat::ETAT_VALIDATION_PAYSAN);
        $this->em->flush();

        $emailSender->envoyerModelContratValidationPaysan($mc, $this->getUser());

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('/contrat_vierge/v2_state_paysan_approval/{contrat_id}', name: 'contrat_vierge_v2_state_paysan_approval')]
    public function v2_state_paysan_approval(int $contrat_id, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_VALIDATE, $mc);

        $form = $this->createForm(\PsrLib\Form\ModeleContratPaysanApprovalType::class, null, [
            'contrat_id' => $mc->getId(),
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mc->setEtat(\PsrLib\Enum\ModeleContratEtat::ETAT_VALIDE);

            $this->em->flush();

            $emailSender->envoyerModelContratValide($mc, $this->getUser());

            return $this->redirect('/contrat_vierge/list_to_validate');
        }

        return $this->render('contrat_vierge/paysan_approval.html.twig', [
            'contrat' => $mc,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/contrat_vierge/v2_state_paysan_refuse/{contrat_id}', name: 'contrat_vierge_v2_state_paysan_refuse')]
    public function v2_state_paysan_refuse(int $contrat_id, Email_Sender $emailSender): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_VALIDATE, $mc);

        $mc->setEtat(\PsrLib\Enum\ModeleContratEtat::ETAT_REFUS);
        $this->em->flush();

        $motif = $this->request->request->get('refus', '');
        $emailSender->envoyerModelContratRefus($mc, $this->getUser(), $motif);

        return $this->redirect('/contrat_vierge/list_to_validate');
    }

    #[Route('/contrat_vierge/v2_preview_pdf/{contrat_id}', name: 'contrat_vierge_v2_preview_pdf')]
    public function v2_preview_pdf(int $contrat_id, MPdfGeneration $pdfGeneration): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_PREVIEW_PDF, $mc);

        $file = $pdfGeneration->genererContratVierge($mc);

        return PsrLib\Services\BinaryFileResponseFactory::createFromTempFilePath(
            $file,
            sprintf('%s.pdf', $mc->getNom())
        );
    }

    #[Route('/contrat_vierge/v2_contrat_edit/{contrat_id}', name: 'contrat_vierge_v2_contrat_edit')]
    public function v2_contrat_edit(int $contrat_id): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_EDIT, $contrat);

        /** @var \PsrLib\ORM\Entity\ModeleContratProduit[] $productsToDelete */
        $productsToDelete = array_filter(
            $contrat->getProduits()->toArray(),
            fn (PsrLib\ORM\Entity\ModeleContratProduit $produit) => null === $produit->getFermeProduit() || !$produit->getFermeProduit()->isEnabled()
        );
        if (!empty($productsToDelete)) {
            foreach ($productsToDelete as $productToDelete) {
                $contrat->removeProduit($productToDelete);
            }
            $this->addFlash(
                'notice_error',
                sprintf(
                    'Attention : les produits suivants ont été supprimés car ils n\'existent plus dans la ferme : %s',
                    implode(
                        ',',
                        array_map(fn (PsrLib\ORM\Entity\ModeleContratProduit $produit) => $produit->getNom(), $productsToDelete)
                    )
                )
            );
        }

        $this->v2_store_data_to_session($contrat);

        return $this->redirect('/contrat_vierge/v2_contrat_form_1');
    }

    /**
     * Première étape de la création d'un nouveau contrat.
     */
    #[Route('/contrat_vierge/v2_contrat_form_1/{ferme_id}/{amap_id}', name: 'contrat_vierge_v2_contrat_form_1')]
    public function v2_contrat_form_1(int $ferme_id = null, int $amap_id = null): Response
    {
        // Get data from argument or from session
        if (null === $ferme_id || null === $amap_id) {
            $contrat = $this->v2_get_data_from_session();
            if (false === $contrat) {
                return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
            }
            $amap = $contrat->getLivraisonLieu()->getAmap();
            $defaultll = $contrat->getLivraisonLieu();
        } else {
            $contrat = new \PsrLib\ORM\Entity\ModeleContrat();

            /** @var \PsrLib\ORM\Entity\Amap $amap */
            $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

            /** @var \PsrLib\ORM\Entity\Ferme $ferme */
            $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

            $contrat->setFerme($ferme);
            $contrat->setLivraisonLieu($amap->getLivraisonLieux()->first());
            $defaultll = 1 === $amap->getLivraisonLieux()->count() ? $amap->getLivraisonLieux()->first() : null;
        }

        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_EDIT, $contrat);

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm1Type::class, $contrat, [
            'amap' => $amap,
            'defaultLl' => $defaultll,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_2');
        }

        return $this->render('contrat_vierge/v2_form_1.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Seconde étape de la création d'un nouveau contrat.
     *
     * @param $ferme_id
     * @param $amap_id
     * @param $contrat_id
     */
    #[Route('/contrat_vierge/v2_contrat_form_2', name: 'contrat_vierge_v2_contrat_form_2')]
    public function v2_contrat_form_2(): Response
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm2Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contratDatesString = $contrat
                ->getDates()
                ->map(fn (\PsrLib\ORM\Entity\ModeleContratDate $date) => $date->getDateLivraison()->format('Y-m-d'))
                ->toArray()
            ;
            // Update exclusions
            foreach ($contrat->getProduits() as $produit) {
                foreach ($produit->getExclusions() as $exclusion) {
                    // Associate to new dates
                    foreach ($contrat->getDates() as $date) {
                        if ($date->getDateLivraison()->format('Y-m-d') === $exclusion->getModeleContratDate()->getDateLivraison()->format('Y-m-d')) {
                            $exclusion->setModeleContratDate($date);
                        }
                    }

                    // Remove old exclusions
                    if (!in_array($exclusion->getModeleContratDate()->getDateLivraison()->format('Y-m-d'), $contratDatesString)) {
                        $exclusion->setModeleContratDate(null);
                        $produit->removeExclusion($exclusion);
                    }
                }
            }

            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_3');
        }

        $contraintesLivraisonsString = [];
        $contraintesLivraisons = [];
        foreach ($contrat->getLivraisonLieu()->getLivraisonHoraires()->toArray() as $horaire) {
            $contraintesLivraisonsString[] = trans($horaire->getJour(), 'amap_livraison_horaire_jour').' en saison '.$horaire->getSaison().'';
            $contraintesLivraisons[] = array_search(
                $horaire->getJour(),
                \PsrLib\ORM\Entity\AmapLivraisonHoraire::JOURS,
                true
            );
        }
        $contraintesLivraisonsString = 'Pour rappel l\'AMAP livre le '.implode(' OU ', $contraintesLivraisonsString);

        $contratDates = $contrat
            ->getDates()
            ->map(function (PsrLib\ORM\Entity\ModeleContratDate $date) {
                if (null === $date->getDateLivraison()) {
                    return null;
                }

                return $date->getDateLivraison()->format('Y-m-d');
            })
            ->toArray()
        ;

        $formView = $form->createView();

        // Disable auto form rendering as done by front-end js
        $formView['dates']->setRendered();

        return $this->render('contrat_vierge/v2_form_2.html.twig', [
            'contrat' => $contrat,
            'form' => $formView,
            'contraintesLivraisonsString' => $contraintesLivraisonsString,
            'contraintesLivraisons' => $contraintesLivraisons,
            'contratDates' => $contratDates,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Troisième étape de la création d'un nouveau contrat.
     */
    #[Route('/contrat_vierge/v2_contrat_form_3', name: 'contrat_vierge_v2_contrat_form_3')]
    public function v2_contrat_form_3(): Response
    {
        $fermeProduitRepo = $this->em->getRepository(\PsrLib\ORM\Entity\FermeProduit::class);

        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        // Ajoute tous les produites par defaut
        if ($this->request->isMethod('GET') && $contrat->getProduits()->isEmpty()) {
            $fermeProduits = $fermeProduitRepo->getFromFermeOrdered($contrat->getFerme());
            foreach ($fermeProduits as $fermeProduit) {
                $contrat->addProduit(\PsrLib\ORM\Entity\ModeleContratProduit::createFromFermeProduit($fermeProduit));
            }
        }
        $isAjaxRequest = RequestHtmxUtils::isHtmxRequest($this->request);
        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm3Type::class, $contrat, [
            'validation_groups' => $isAjaxRequest ? false : ['form3'],
        ]);
        $form->handleRequest($this->request);

        if ($isAjaxRequest) {
            return $this->render('contrat_vierge/v2_form_3_form.html.twig', [
                'contrat' => $contrat,
                'form' => $form->createView(),
                'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_4');
        }

        return $this->render('contrat_vierge/v2_form_3.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Quatrième étape de la création d'un nouveau contrat.
     */
    #[Route('/contrat_vierge/v2_contrat_form_4', name: 'contrat_vierge_v2_contrat_form_4')]
    public function v2_contrat_form_4(): Response
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $isHtmxRequest = RequestHtmxUtils::isHtmxRequest($this->request);
        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm4Type::class, $contrat->getInformationLivraison(), [
            'validation_groups' => $isHtmxRequest ? false : ['form4'],
            'mc' => $contrat,
        ]);
        $form->handleRequest($this->request);
        if ($isHtmxRequest) {
            return $this->render('contrat_vierge/v2_form_4_form.html.twig', [
                'contrat' => $contrat,
                'form' => $form->createView(),
                'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_5');
        }

        return $this->render('contrat_vierge/v2_form_4.html.twig', [
            'nbDatesContract' => $contrat->getDates()->count(),
            'ferme' => $contrat->getFerme(),
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Cinquième étape de la création d'un nouveau contrat.
     */
    #[Route('/contrat_vierge/v2_contrat_form_5', name: 'contrat_vierge_v2_contrat_form_5')]
    public function v2_contrat_form_5(): Response
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        // Ne pas afficher la fenetre si les livraisons sont identiques
        if (true === $contrat->getInformationLivraison()->getProduitsIdentiquePaysan()) {
            return $this->redirect('/contrat_vierge/v2_contrat_form_6');
        }

        $dto = new ModeleContratFrom5DTO($contrat);
        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm5Type::class, $dto);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($dto->getProduits() as $produit) {
                $produit->getExclusions()->clear();
            }
            foreach ($dto->getDates() as $date) {
                $date->getExclusions()->clear();
            }

            foreach ($dto->getDates() as $date) {
                foreach ($dto->getProduits() as $produit) {
                    if (false === $dto->getExclusions()[$dto->getKeyForProduitDate($produit, $date)]) {
                        $exclusion = new \PsrLib\ORM\Entity\ModeleContratProduitExclure();
                        $date->addExclusion($exclusion);
                        $produit->addExclusion($exclusion);
                    }
                }
            }

            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_6');
        }

        return $this->render('contrat_vierge/v2_form_5.html.twig', [
            'dto' => $dto,
            'form' => $form->createView(),
            'contrat' => $contrat,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_vierge/v2_contrat_form_6', name: 'contrat_vierge_v2_contrat_form_6')]
    public function v2_contrat_form_6(): Response
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm6Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $this->v2_store_data_to_session($contrat);

            return $this->redirect('/contrat_vierge/v2_contrat_form_7');
        }

        return $this->render('contrat_vierge/v2_form_6.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('/contrat_vierge/v2_contrat_form_7', name: 'contrat_vierge_v2_contrat_form_7')]
    public function v2_contrat_form_7(): Response
    {
        $contrat = $this->v2_get_data_from_session();
        if (false === $contrat) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $form = $this->formFactory->create(\PsrLib\Form\ModelContratForm7Type::class, $contrat);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->v2_is_draft_button_clicked_and_valid($contrat)) {
                return $this->v2_save_draft($contrat);
            }

            $contrat->setEtat(\PsrLib\Enum\ModeleContratEtat::ETAT_CREATION);
            // remplace le contrat en BDD. Permis parce que seuls les contrats en cours de création peuvent etre modifiés
            if (null !== $contrat->getId()) {
                $this->em->remove($this->em->getReference(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat->getId()));
            }
            $this->em->persist($contrat);
            $this->em->flush();

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('contrat_vierge/v2_form_7.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    private function v2_is_draft_button_clicked_and_valid(PsrLib\ORM\Entity\ModeleContrat $mc): bool
    {
        return \PsrLib\Enum\ModeleContratEtat::ETAT_BROUILLON === $mc->getEtat() && $this->request->request->has('draft');
    }

    private function v2_save_draft(PsrLib\ORM\Entity\ModeleContrat $mc): Response
    {
        if (null !== $mc->getId()) {
            $this->em->remove($this->em->getReference(\PsrLib\ORM\Entity\ModeleContrat::class, $mc->getId()));
        }

        $this->em->persist($mc);
        $this->em->flush();

        $this->addFlash('notice_success', 'Le contrat a bien été sauvegardé en brouillon.');

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('/contrat_vierge/ajax_test_date_absence', name: 'contrat_vierge_ajax_test_date_absence')]
    public function ajax_test_date_absence(Request $request): Response
    {
        $amapId = (int) $request->query->get('amap_id');

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amapId);

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        try {
            $date = Carbon::createFromFormat('Y-m-d', $request->query->get('date'));
        } catch (\Exception) {
            return new JsonResponse(['result' => false]);
        }

        if (!$amap->getAmapEtudiante()) {
            return new JsonResponse(['result' => false]);
        }

        /** @var \PsrLib\ORM\Entity\AmapAbsence[] $absences */
        $absences = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapAbsence::class)
            ->findBy([
                'amap' => $amap,
            ])
        ;

        foreach ($absences as $absence) {
            if ($date->gte($absence->getAbsenceDebut()) && $date->lte($absence->getAbsenceFin())) {
                return new JsonResponse([
                    'result' => true,
                    'title' => $absence->getTitre(),
                ]);
            }
        }

        return new JsonResponse(['result' => false]);
    }

    #[Route('/contrat_vierge/list_to_validate', name: 'contrat_vierge_list_to_validate')]
    public function list_to_validate(): Response
    {
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_LIST_VALIDATE);

        $currentUser = $this->getUser();
        $searchForm = $this->formFactory->create(\PsrLib\Form\ContratViergeChooseAmapPaysan::class, null, [
            'current_user' => $currentUser,
        ]);
        $searchForm->handleRequest($this->request);

        $amap = null;
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $amap = $searchForm->get('amap')->getData();
        }

        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->findToValidateForUser($currentUser, $amap)
        ;

        return $this->render('contrat_vierge/list_to_validate.html.twig', [
            'contrats' => $contrats,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    #[Route('/contrat_vierge/v2_simulation/{contrat_id}', name: 'contrat_vierge_v2_simulation')]
    public function v2_simulation(int $contrat_id, ContratCalculatorGenerateResponse $calculatorGenerateResponse): Response
    {
        /** @var \PsrLib\ORM\Entity\ModeleContrat $contrat */
        $contrat = $this->findOrExit(\PsrLib\ORM\Entity\ModeleContrat::class, $contrat_id);
        $this->denyAccessUnlessGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_EDIT, $contrat);

        $dto = PsrLib\DTO\ContratCommandeSouscription::createFromMc($contrat);
        $form = $this->formFactory->create(PsrLib\Form\ContratSouscriptionForm1Type::class, $dto, [
            'validation_groups' => ['Default', 'subscription'],
        ]);
        $form->handleRequest($this->request);

        if (RequestHtmxUtils::isHtmxRequest($this->request)) {
            return $calculatorGenerateResponse->renderHtmxResponse($dto);
        }

        return $this->render('contrat_vierge/simulation.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    private function v2_get_data_from_session(): false|PsrLib\ORM\Entity\ModeleContrat
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('mc_wizard', ''), \PsrLib\ORM\Entity\ModeleContrat::class, 'json', [
                    'groups' => 'wizard',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function v2_store_data_to_session(PsrLib\ORM\Entity\ModeleContrat $modeleContrat): void
    {
        $this->sfSession->set(
            'mc_wizard',
            $this->serializer->serialize($modeleContrat, 'json', [
                'groups' => 'wizard',
            ])
        );
    }
}
