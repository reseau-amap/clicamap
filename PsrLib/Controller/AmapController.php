<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Carbon\Carbon;
use DI\Attribute\Inject;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PsrLib\DTO\AmapCreationWizard;
use PsrLib\DTO\ContratCelluleContainer;
use PsrLib\DTO\Response\RedirectToRefererResponse;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapCollectif;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\Exporters\ExcelGeneratorExportAmap;
use PsrLib\Services\Exporters\ExcelGeneratorExportAmapActivityIndicator;
use PsrLib\Services\Security\Voters\AmapLivraisonLieuVoter;
use PsrLib\Services\Security\Voters\AmapVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AmapController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_amap';

    public function __construct(
        private readonly AmapRepository $amapRepository
    ) {
    }

    #[Inject]
    public \PsrLib\Services\Password\PasswordEncoder $passwordEncoder;

    #[Inject]
    public \PsrLib\Services\Password\PasswordGenerator $passwordGenerator;

    #[Inject]
    public ExcelGeneratorExportAmapActivityIndicator $excelGeneratorExportAmapActivityIndicator;

    #[Inject]
    public \PsrLib\Services\Email_Sender $emailSender;

    #[Inject]
    public \Symfony\Component\Translation\Translator $translator;

    #[Inject]
    public \PsrLib\Services\Leaflet $leaflet;

    /**
     * Première fonction.
     */
    #[Route('amap', name: 'amap_index')]
    public function index(): Response
    {
        return $this->amap_mdr();
    }

    /**
     * Accueil / Moteur de recherche.
     */
    #[Route('amap/amap_mdr', name: 'amap_amap_mdr')]
    public function amap_mdr(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_LIST);

        $this->session_store_home_path(self::HOME_PATH_ID);
        $this->redirectFromStoredGetParamIfExist('amap');

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $data = $searchForm->getData();
        $amaps = $this->amapRepository->search($data);

        $gps = 0;
        $map = null;
        if ($searchForm->get('carto')->getData()) {
            [$gps, $map] = $this->cartographie($amaps);
        }

        return $this->render('amap/display_all.html.twig', [
            'amaps' => $amaps,
            'gps' => $gps,
            'map' => $map,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    #[Route('amap/mon_amap', name: 'amap_mon_amap')]
    public function mon_amap(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_LIST_OWN);
        $this->session_store_home_path(self::HOME_PATH_ID);

        $amaps = $this->getUser()->getAdminAmaps();

        return $this->render('amap/display_own.html.twig', [
            'amaps' => $amaps,
        ]);
    }

    /**
     * Éditer l'état de l'AMAP.
     */
    #[Route('amap/edit_etat/{etat}/{amap_id}', name: 'amap_edit_etat')]
    public function edit_etat(string $etat, int $amap_id): Response
    {
        // Sécurité
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CHANGE_STATE, $amap);

        if (in_array($etat, \PsrLib\ORM\Entity\Amap::ETATS)) {
            $amap->setEtat($etat);
        }
        $this->em->flush();

        return new RedirectToRefererResponse($this->generateUrl('amap_index'));
    }

    /**
     * Afficher les détails d'une AMAP.
     */
    #[Route('amap/display/{amap_id}', name: 'amap_display')]
    public function display(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $amapFermes = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ferme::class)
            ->getFromAmap($amap)
        ;

        return $this->render('amap/display.html.twig', [
            'amap' => $amap,
            'amapFermes' => $amapFermes,
        ]);
    }

    /**
     * Calendrier de livraisons.
     *
     * @param null $an
     * @param null $mois
     */
    #[Route('amap/calendrier/{amap_id}/{an}/{mois}', name: 'amap_calendrier')]
    public function calendrier(\CI_Calendar $calendar, int $amap_id, string $an = null, string $mois = null): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        // Sécurité ------------------------------------------------------------
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $data = [];
        // PRÉFÉRENCES DU CALENDRIER -------------------------------------------
        if ($amap_id) {
            $prefs = ['start_day' => 'monday',
                'day_type' => 'long',
                'show_next_prev' => true,
                'next_prev_url' => $this->generateUrl('amap_calendrier', ['amap_id' => $amap_id])]; // Le zéro est pour $date_id

            $prefs['template'] = '
          {table_open}<table class="table"  border="0" cellpadding="0" cellspacing="0">{/table_open}

          {heading_row_start}<tr>{/heading_row_start}

          {heading_previous_cell}<th><a href="{previous_url}"><i class="glyphicon glyphicon-menu-left"></i></a></th>{/heading_previous_cell}
          {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
          {heading_next_cell}<th><a href="{next_url}"><i class="glyphicon glyphicon-menu-right"></i></a></th>{/heading_next_cell}

          {heading_row_end}</tr>{/heading_row_end}

          {week_row_start}<tr>{/week_row_start}
          {week_day_cell}<td>{week_day}</td>{/week_day_cell}
          {week_row_end}</tr>{/week_row_end}

          {cal_row_start}<tr>{/cal_row_start}
          {cal_cell_start}<td>{/cal_cell_start}
          {cal_cell_start_today}<td>{/cal_cell_start_today}
          {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

          {cal_cell_content}<a href="{content}" data-toggle="modal" data-target="#modal_{day}"><button type=button class="btn btn-primary btn-xs">{day}</button></a>{/cal_cell_content}

          {cal_cell_no_content}{day}{/cal_cell_no_content}

          {cal_cell_blank}&nbsp;{/cal_cell_blank}

          {cal_cell_other}{day}{/cal_cel_other}

          {cal_cell_end}</td>{/cal_cell_end}
          {cal_cell_end_today}</td>{/cal_cell_end_today}
          {cal_cell_end_other}</td>{/cal_cell_end_other}
          {cal_row_end}</tr>{/cal_row_end}

          {table_close}</table>{/table_close}
          ';

            $calendar->initialize($prefs);
        }

        if (!$an) {
            $data['an'] = date('Y');
        } else {
            $data['an'] = $an;
        }
        if (!$mois) {
            $data['mois'] = date('m');
        } else {
            $data['mois'] = $mois;
        }

        $data['liens'] = null;

        $data['amap'] = $amap;
        $data['livraison_lieu'] = $amap->getLivraisonLieux()->toArray();

        // TOUTES LES DATES DE CONTRATS VIERGES (MC) LIÉS À UNE AMAPIEN --------
        /** @var \PsrLib\ORM\Entity\ModeleContratDate[] $date_all */
        $date_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->getByAmap($amap)
        ;
        if (count($date_all) > 0) {
            // On dédoublonne les dates ------------------------------------------
            $amap_date_all = [];
            foreach ($date_all as $date) {
                $amap_date_all[] = $date->getDateLivraison()->format('Y-m-d');
            }
            $amap_date_all = array_unique($amap_date_all);

            // Liens du calendrier et infos des Modals ---------------------------
            $data['liens'] = [];

            /** @var \PsrLib\ORM\Repository\ModeleContratRepository $modeleContratRepo */
            $modeleContratRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ;

            /** @var \PsrLib\ORM\Repository\ContratCelluleRepository $contratCelluleRepo */
            $contratCelluleRepo = $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\ContratCellule::class)
            ;

            foreach ($amap_date_all as $a_d) {
                // Année
                $a_d_an = substr($a_d, 0, 4);
                // Mois
                $a_d_mois = substr($a_d, 5, 2);

                // L'année et le mois du contrat correspondent avec l'année et le mois courant du calendrier
                if ($data['an'] == $a_d_an
                    && $data['mois'] == $a_d_mois
                ) {
                    $jour = substr($a_d, -2);
                    // On enlève le premier zéro du jour s'il y en a un --------------
                    if (0 == substr($jour, 0, 1)) {
                        $jour = substr($jour, 1, 2);
                    }

                    // On place un lien sur le jour du calendrier --------------------
                    $data['livraisons'][$jour] = [];

                    $data['livraisons'][$jour]['mc'] = $modeleContratRepo
                        ->getFromAmapDate($amap, $a_d)
                    ;
                    $data['livraisons'][$jour]['cellules'] = new ContratCelluleContainer($contratCelluleRepo->getFromAmapDateForValidatedContract($amap, $a_d));

                    if ($data['livraisons'][$jour]['cellules']->length() > 0) {
                        $data['liens'][$jour] = '#';
                    }
                }
            }
        }

        $data['calendrier'] = $calendar
            ->generate($data['an'], $data['mois'], $data['liens'])
        ;

        return $this->render('amap/calendrier.html.twig', $data);
    }

    /**
     * Télécharger infos du collectif.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    #[Route('amap/telecharger_collectif/{amap_id}', name: 'amap_telecharger_collectif')]
    public function telecharger_collectif(int $amap_id, Spreadsheet $excel): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $collectifs = $amap->getCollectifs();

        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = $collectifs->count();

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $excel->setActiveSheetIndex(0);

        $excel->getActiveSheet()->setTitle('Collectif');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $excel->getActiveSheet()->setCellValue('A4', 'Profil');
        $excel->getActiveSheet()->setCellValue('B4', 'Précision');
        $excel->getActiveSheet()->setCellValue('C4', 'Nom');
        $excel->getActiveSheet()->setCellValue('D4', 'Prénom');
        $excel->getActiveSheet()->setCellValue('E4', 'Email');
        $excel->getActiveSheet()->setCellValue('F4', 'Téléphone 1');
        $excel->getActiveSheet()->setCellValue('G4', 'Téléphone 2');
        $excel->getActiveSheet()->setCellValue('H4', 'Adresse');
        $excel->getActiveSheet()->setCellValue('I4', 'Code Postal');
        $excel->getActiveSheet()->setCellValue('J4', 'Ville');
        $excel->getActiveSheet()->setCellValue('K4', 'Adhérent année');
        $excel->getActiveSheet()->setCellValue('L4', 'Abonné newsletter');
        $excel->getActiveSheet()->setCellValue('M4', 'État');

        $excel->getActiveSheet()->getStyle('A4:M4')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A4:M4')->getFont()->setBold(true);

        $i = 5;

        /** @var \PsrLib\ORM\Entity\AmapCollectif $collectif */
        foreach ($collectifs as $collectif) {
            $amapien = $collectif->getAmapien();
            $amapienUser = $amapien->getUser();
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());

            $excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($collectif->getProfil() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, strtoupper($collectif->getPrecision() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, strtoupper($amapienUser->getName()->getLastName() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $excel->getActiveSheet()->getStyle('C'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('D'.$i, ucfirst($amapienUser->getName()->getFirstName() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('E'.$i, implode(';', $amapienUser->getEmails()->toArray()));
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('F'.$i, $amapienUser->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('G'.$i, $amapienUser->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('H'.$i, $amapienUser->getAddress()->getAdress());
            $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $amapienVille = $amapienUser->getVille();
            $excel->getActiveSheet()->setCellValueExplicit('I'.$i, null === $amapienVille ? '' : $amapienVille->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('J'.$i, strtoupper(null === $amapienVille ? '' : $amapienVille->getNom()));
            $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('K'.$i, $annee_adhesion);
            $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('L'.$i, ucfirst(true === $amapienUser->getNewsletter() ? 'oui' : ''));
            $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('M'.$i, ucfirst($amapien->getEtat()));
            $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $excel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray($style);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;
        $titre = $titre.$amap->getNom().' -> collectif';

        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_du collectif_'.date('d_m_Y').'.xls';

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportCollectif_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    /**
     * Ajouter un commentaire daté.
     */
    #[Route('amap/amap_commentaire_add/{amap_id}', name: 'amap_amap_commentaire_add')]
    public function amap_commentaire_add(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CHANGE_STATE, $amap);

        $commentaire = new \PsrLib\ORM\Entity\AmapCommentaire(
            $amap,
            Carbon::now(),
            $this->request->request->get('amap_commentaire')
        );
        $this->em->persist($commentaire);
        $this->em->flush();

        return $this->redirectToRoute('amap_display', ['amap_id' => $amap_id]);
    }

    /**
     *  Ajouter un commentaire daté.
     */
    #[Route('amap/amap_commentaire_remove/{commentaire_id}', name: 'amap_amap_commentaire_remove')]
    public function amap_commentaire_remove(int $commentaire_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapCommentaire $commentaire */
        $commentaire = $this->findOrExit(\PsrLib\ORM\Entity\AmapCommentaire::class, $commentaire_id);

        $amap = $commentaire->getAmap();

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CHANGE_STATE, $amap);

        $this->em->remove($commentaire);
        $this->em->flush();

        return $this->redirectToRoute('amap_display', ['amap_id' => $amap->getId()]);
    }

    /**
     * Télécharger l'ensemble des AMAP.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    #[Route('amap/telecharger_amap_synthese', name: 'amap_telecharger_amap_synthese')]
    public function telecharger_amap_synthese(Spreadsheet $excel): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $amaps = $this->amapRepository->search($searchForm->getData());
        if (0 === count($amaps)) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        // Tous les types de production ------------------------------------------
        /** @var \PsrLib\ORM\Entity\TypeProduction[] $type_production */
        $type_production = $this->em->getRepository(\PsrLib\ORM\Entity\TypeProduction::class)->findAll();
        $time = 'Extrait le '.date('d/m/Y');
        $nb_amap = count($amaps);

        if (1 == $nb_amap) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amap.' résultats ('.$time.')';
        }

        // worksheet 1
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Amap');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $excel->getActiveSheet()->setCellValue('A4', 'ID');
        $excel->getActiveSheet()->setCellValue('B4', 'Nom');
        $excel->getActiveSheet()->setCellValue('C4', 'Nombre d\'adhérents');
        $excel->getActiveSheet()->setCellValue('D4', 'Email public');
        $excel->getActiveSheet()->setCellValue('E4', 'Email correspondant');
        $excel->getActiveSheet()->setCellValue('F4', 'Lieu de livraison');
        $excel->getActiveSheet()->setCellValue('G4', 'Adresse de livraison');
        $excel->getActiveSheet()->setCellValue('H4', 'CP de livraison');
        $excel->getActiveSheet()->setCellValue('I4', 'Ville de livraison');
        $excel->getActiveSheet()->setCellValue('J4', 'Saison de livraison');
        $excel->getActiveSheet()->setCellValue('K4', 'Jours de livraison');
        $excel->getActiveSheet()->setCellValue('L4', 'Heure de début de livraison');
        $excel->getActiveSheet()->setCellValue('M4', 'Heure de fin de livraison');

        $excel->getActiveSheet()->getStyle('A4:CB4')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A4:CB4')->getFont()->setBold(true);

        $i = 5;
        foreach ($amaps as $amap) {
            $excel->getActiveSheet()->setCellValue('A'.$i, mb_strtoupper((string) $amap->getId()));
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            // NOM DE L'AMAP -------------------------------------------------------
            $excel->getActiveSheet()->setCellValue('B'.$i, mb_strtoupper($amap->getNom()));
            $excel->getActiveSheet()->getStyle('B'.$i)->getFont()->setBold(true);

            // NOMBRE D'ADHÉRENTS --------------------------------------------------
            $excel->getActiveSheet()->setCellValue('C'.$i, mb_strtolower($amap->getNbAdherents() ?? ''));

            // CONTACTS ------------------------------------------------------------
            // EMAIL de l'AMAP -----------------------------------------------------
            $excel->getActiveSheet()->setCellValue('D'.$i, mb_strtolower($amap->getEmail() ?? ''));

            // EMAIL des CORRESPONDANTS CLIC'AMAP ----------------------------------
            $correspondant_clicamap = $amap->getAmapiens()->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire());
            if ($correspondant_clicamap->count() > 0) {
                $correspondants = null;
                $nb_c = $correspondant_clicamap->count();
                $n = 1;
                $retour = ' ; ';

                foreach ($correspondant_clicamap as $c) {
                    $correspondants .= implode(' ; ', $c->getUser()->getEmails()->toArray());
                    if ($n < $nb_c) {
                        $correspondants = $correspondants.$retour;
                    }
                    ++$n;
                }
                $excel->getActiveSheet()->setCellValue('D'.$i, mb_strtolower($correspondants));
            } else { // ou celui des CORRESPONDANT RÉSEAUX
                $correspondant_reseau = $amap->getAmapienRefReseau();
                if (null !== $correspondant_reseau) {
                    $excel->getActiveSheet()->setCellValue(
                        'E'.$i,
                        mb_strtolower(
                            implode(';', $correspondant_reseau->getUser()->getEmails()->toArray())
                        )
                    );
                } else { // ou celui du PRÉSIDENT
                    $collectif = $amap->getCollectifs();
                    foreach ($collectif as $coll) {
                        if (\PsrLib\ORM\Entity\AmapCollectif::PROFIL_PRESIDENT == $coll->getProfil()) {
                            $excel->getActiveSheet()->setCellValue(
                                'E'.$i,
                                mb_strtolower(
                                    implode(';', $coll->getAmapien()->getUser()->getEmails()->toArray())
                                )
                            );
                        }
                    }
                }
            }

            // LIEUX DE LIVRAISON
            $livraison_lieu = $amap->getLivraisonLieux();
            $livraison_lieu_nbr = count($livraison_lieu);

            $j = $i; // 1er ligne de la cellule de l'AMAP
            $n = 1; // Incrémentation

            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $l_l */
            foreach ($livraison_lieu as $l_l) {
                $excel->getActiveSheet()->setCellValue('F'.$i, $l_l->getNom());
                $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

                $excel->getActiveSheet()->setCellValue('G'.$i, $l_l->getAdresse());
                $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

                $excel->getActiveSheet()->setCellValueExplicit('H'.$i, $l_l->getVille()->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

                $excel->getActiveSheet()->setCellValue('I'.$i, $l_l->getVille()->getNom());
                $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

                $livraison_horaire = $l_l->getLivraisonHoraires();
                $livraison_horaire_nbr = count($livraison_horaire);

                $k = $i; // 1er ligne de la cellule du lieu de livraison
                $nn = 1; // Incrémentation

                /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $l_h */
                foreach ($livraison_horaire as $l_h) {
                    if (\PsrLib\ORM\Entity\AmapLivraisonHoraire::SAISON_ETE == $l_h->getSaison()) {
                        $saison = 'Eté';
                    } else {
                        $saison = 'Hiver';
                    }

                    $excel->getActiveSheet()->setCellValue('J'.$i, $saison);
                    $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

                    $excel->getActiveSheet()->setCellValue('K'.$i, $this->translator->trans($l_h->getJour(), [], 'amap_livraison_horaire_jour'));
                    $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

                    $excel->getActiveSheet()->setCellValue('L'.$i, $l_h->getHeureDebut());
                    $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

                    $excel->getActiveSheet()->setCellValue('M'.$i, $l_h->getHeureFin());
                    $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

                    // Cellules lieu de livraison de l'AMAP ----------------------------
                    if ($nn == $livraison_horaire_nbr) {
                        $m = 'E'.$k.':E'.$i;
                        $excel->getActiveSheet()->mergeCells($m);
                        $m = 'F'.$k.':F'.$i;
                        $excel->getActiveSheet()->mergeCells($m);
                        $m = 'G'.$k.':G'.$i;
                        $excel->getActiveSheet()->mergeCells($m);
                        $m = 'H'.$k.':H'.$i;
                        $excel->getActiveSheet()->mergeCells($m);
                    } else {
                        ++$i;
                        ++$nn;
                    }
                }

                // Cellule nom / emails de l'AMAP ------------------------------------
                if ($n == $livraison_lieu_nbr) {
                    $m = 'A'.$j.':A'.$i;
                    $excel->getActiveSheet()->mergeCells($m);
                    $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $m = 'B'.$j.':B'.$i;
                    $excel->getActiveSheet()->mergeCells($m);
                    $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $m = 'C'.$j.':C'.$i;
                    $excel->getActiveSheet()->mergeCells($m);
                    $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $m = 'D'.$j.':D'.$i;
                    $excel->getActiveSheet()->mergeCells($m);
                    $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $m = 'E'.$j.':E'.$i;
                    $excel->getActiveSheet()->mergeCells($m);
                    $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                } else {
                    ++$i;
                    ++$n;
                }
            }

            // FILIÈRES ------------------------------------------------------------

            $y = 'N'; // Position du pointeur pour l'entête
            $z = $y; // Entête
            // Pour chaque filière -------------------------------------------------
            // On affiche que les catégories filles --------------------------------
            foreach ($type_production as $tp) {
                if (!$tp->hasChilds()) {
                    $tp_nom = $tp->getNomComplet();

                    // Quatres colonnes par filière
                    $excel->getActiveSheet()->setCellValue($y.'4', $tp_nom.' (Référents / Paysans)');
                    ++$z;
                    ++$z;
                    ++$z; // soit $y + 3
                    $m = $y.'4:'.$z.'4';
                    // Merge de l'entête
                    $excel->getActiveSheet()->mergeCells($m);

                    // RÉFÉRENTS / PAYSANS ---------------------------------------------
                    $referents_nom_prenom = [];
                    $referents_email = [];
                    $referents_nom_prenom_cell = null;
                    $referents_email_cell = null;

                    $paysans_nom_prenom = [];
                    $paysans_email = [];
                    $paysans_nom_prenom_cell = null;
                    $paysans_email_cell = null;

                    /** @var \PsrLib\ORM\Entity\Amapien[] $ferme_referents */
                    $ferme_referents = $this
                        ->em
                        ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
                        ->getRefProduitFromAmapTp($amap, $tp)
                    ;

                    // On fait le tour de toutes les fermes reliées à l'AMAP -----------
                    foreach ($ferme_referents as $amapien) {
                        $referents_nom_prenom[] = $amapien->getUser()->getName()->getFullNameInversed();
                        $referents_email = array_merge($referents_email, $amapien->getUser()->getEmails()->toArray());

                        $referents_nom_prenom = array_unique($referents_nom_prenom);
                        $referents_email = array_unique($referents_email);

                        // On liste les paysans dans des tableaux ----------------------
                        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
                        foreach ($amapien->getRefProdFermes() as $ferme) {
                            foreach ($ferme->getPaysans() as $paysan) {
                                $paysans_nom_prenom[] = $paysan->getUser()->getName()->getFullNameInversed();
                                $paysans_email = array_merge($paysans_email, $paysan->getUser()->getEmails()->toArray());
                            }
                        }
                        $paysans_nom_prenom = array_unique($paysans_nom_prenom);
                        $paysans_email = array_unique($paysans_email);
                    }

                    // RÉFÉRENTS -------------------------------------------------------
                    if ($referents_email) {
                        // On place les valeurs des tableaux dans une variable string ----
                        $nb_r = count($referents_nom_prenom);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($referents_nom_prenom as $referent_nom_prenom) {
                            $referents_nom_prenom_cell .= $referent_nom_prenom;
                            if ($n < $nb_r) {
                                $referents_nom_prenom_cell = $referents_nom_prenom_cell.$retour;
                            }
                            ++$n;
                        }
                        $nb_r = count($referents_email);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($referents_email as $referent_email) {
                            $referents_email_cell .= $referent_email;
                            if ($n < $nb_r) {
                                $referents_email_cell = $referents_email_cell.$retour;
                            }
                            ++$n;
                        }
                    }
                    // Cellules des référents ------------------------------------------
                    $excel->getActiveSheet()->setCellValue($y.$j, $referents_nom_prenom_cell);
                    ++$y;

                    $excel->getActiveSheet()->setCellValue($y.$j, $referents_email_cell);
                    ++$y;

                    // PAYSANS ---------------------------------------------------------
                    if ($paysans_email) {
                        // On place les valeurs des tableaux dans une variable string ----
                        $nb_r = count($paysans_nom_prenom);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($paysans_nom_prenom as $paysan_nom_prenom) {
                            $paysans_nom_prenom_cell .= $paysan_nom_prenom;
                            if ($n < $nb_r) {
                                $paysans_nom_prenom_cell = $paysans_nom_prenom_cell.$retour;
                            }
                            ++$n;
                        }
                        $nb_r = count($paysans_email);
                        $n = 1;
                        $retour = ' ; ';
                        foreach ($paysans_email as $paysan_email) {
                            $paysans_email_cell .= $paysan_email;
                            if ($n < $nb_r) {
                                $paysans_email_cell = $paysans_email_cell.$retour;
                            }
                            ++$n;
                        }
                    }

                    // Cellules des paysans --------------------------------------------
                    $excel->getActiveSheet()->setCellValue($y.$j, $paysans_nom_prenom_cell);
                    ++$y;

                    $excel->getActiveSheet()->setCellValue($y.$j, $paysans_email_cell);
                    ++$y;
                    // Positionnement de l'entête --------------------------------------
                    $z = $y;
                }
            }

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = 'Amap';
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_amap_'.date('d_m_Y').'.xls'; // save our workbook as this file name

        // save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportAmapSynthese_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    #[Route('amap/telecharger_amap_complet', name: 'amap_telecharger_amap_complet')]
    public function telecharger_amap_complet(ExcelGeneratorExportAmap $excelgeneratorexportamap): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $outFileName = $excelgeneratorexportamap->genererExportAmapComplet($searchForm->getData());

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outFileName,
            sprintf('liste_des_amaps_%s.xls', Carbon::now()->format('Y_m_d'))
        );
    }

    /**
     * Télécharger.
     */
    #[Route('amap/telecharger_referents_produit/{amap_id}', name: 'amap_telecharger_referents_produit')]
    public function telecharger_referents_produit(int $amap_id, Spreadsheet $excel): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        /** @var \PsrLib\ORM\Entity\Amapien[] $amap_referent_produit_all */
        $amap_referent_produit_all = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getRefProduitFromAmap($amap)
        ;

        $time = 'extrait le '.date('d/m/Y');

        $nb_amapiens = count($amap_referent_produit_all);

        if (1 == $nb_amapiens) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_amapiens.' résultats ('.$time.')';
        }

        // worksheet 1
        $excel->setActiveSheetIndex(0);

        $excel->getActiveSheet()->setTitle('Référents produits');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $excel->getActiveSheet()->setCellValue('C4', 'Email');
        $excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $excel->getActiveSheet()->setCellValue('J4', 'Abonné newsletter');
        $excel->getActiveSheet()->setCellValue('K4', 'État');

        $excel->getActiveSheet()->getStyle('A4:K4')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A4:K4')->getFont()->setBold(true);

        $i = 5;

        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        foreach ($amap_referent_produit_all as $amapien) {
            $user = $amapien->getUser();
            $annee_adhesion = implode(' | ', $amapien->getAnneeAdhesions()->toArray());

            $excel->getActiveSheet()->setCellValue('A'.$i, strtoupper($user->getName()->getLastName() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, ucfirst($user->getName()->getFirstName() ?? ''));
            $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, implode(';', $user->getEmails()->toArray()));
            $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('D'.$i, $user->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('E'.$i, $user->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('F'.$i, $user->getAddress()->getAdress());
            $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $villeAmapien = $user->getVille();
            $excel->getActiveSheet()->setCellValueExplicit('G'.$i, null === $villeAmapien ? '' : $villeAmapien->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('H'.$i, strtoupper(null === $villeAmapien ? '' : $villeAmapien->getNom()));
            $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('J'.$i, ucfirst(true === $user->getNewsletter() ? 'oui' : ''));
            $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('K'.$i, ucfirst($amapien->getEtat()));
            $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $excel->getActiveSheet()->getStyle('A'.$i.':K'.$i)->applyFromArray($style);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = null;
        $titre = $titre.$amap->getNom().' -> référents produit';

        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_referents_produit_'.date('d_m_Y').'.xls';

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportReferentProduit_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    /**
     * Télécharger l'ensemble des paysans de la session recherche.
     */
    #[Route('amap/telecharger_paysans/{amap_id}', name: 'amap_telecharger_paysans')]
    public function telecharger_paysans(int $amap_id, Spreadsheet $excel): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        // fermes reliées à l'AMAP par un référent
        /** @var \PsrLib\ORM\Entity\Ferme[] $fermes */
        $fermes = $this->em->getRepository(\PsrLib\ORM\Entity\Ferme::class)->getFromAmap($amap);

        /** @var \PsrLib\ORM\Entity\Paysan[] $paysans */
        $paysans = [];
        foreach ($fermes as $ferme) {
            $paysans = array_merge($paysans, $ferme->getPaysans()->toArray());
        }

        $time = 'Extrait le '.date('d/m/Y');

        $nb_paysans = count($paysans);

        if (1 == $nb_paysans) {
            $time = '1 résultat ('.$time.')';
        } else {
            $time = $nb_paysans.' résultats ('.$time.')';
        }

        // worksheet 1
        $excel->setActiveSheetIndex(0);

        $excel->getActiveSheet()->setTitle('Paysans');

        $style = [
            'font' => ['color' => ['rgb' => '000000']],
            'borders' => ['outline' => [
                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['rgb' => '000'], ]],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, ], ];

        $excel->getActiveSheet()->setCellValue('A4', 'Nom');
        $excel->getActiveSheet()->setCellValue('B4', 'Prénom');
        $excel->getActiveSheet()->setCellValue('C4', 'Email');
        $excel->getActiveSheet()->setCellValue('D4', 'Téléphone 1');
        $excel->getActiveSheet()->setCellValue('E4', 'Téléphone 2');
        $excel->getActiveSheet()->setCellValue('F4', 'Adresse');
        $excel->getActiveSheet()->setCellValue('G4', 'Code Postal');
        $excel->getActiveSheet()->setCellValue('H4', 'Ville');
        $excel->getActiveSheet()->setCellValue('I4', 'Adhérent année');
        $excel->getActiveSheet()->setCellValue('J4', 'Suivi SPG');
        $excel->getActiveSheet()->setCellValue('K4', 'Abonné newsletter');
        $excel->getActiveSheet()->setCellValue('L4', 'Ferme : nom');
        $excel->getActiveSheet()->setCellValue('M4', 'Ferme : siret');
        $excel->getActiveSheet()->setCellValue('N4', 'Ferme : code postal');
        $excel->getActiveSheet()->setCellValue('O4', 'Ferme : ville');

        $excel->getActiveSheet()->getStyle('A4:O4')->applyFromArray($style);
        $excel->getActiveSheet()->getStyle('A4:O4')->getFont()->setBold(true);

        $i = 5;
        /** @var \PsrLib\ORM\Entity\Paysan $paysan */
        foreach ($paysans as $paysan) {
            $paysanFerme = $paysan->getFermes()->first();
            $paysanUser = $paysan->getUser();
            $annee_adhesion = implode(
                ' | ',
                $paysanFerme->getAnneeAdhesions()->toArray()
            );

            $excel->getActiveSheet()->setCellValue('A'.$i, strtoupper((string) $paysanUser->getName()->getLastName()));
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);

            $excel->getActiveSheet()->setCellValue('B'.$i, ucfirst((string) $paysanUser->getName()->getFirstName()));
            $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('C'.$i, implode(';', $paysanUser->getEmails()->toArray()));
            $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('D'.$i, $paysanUser->getNumTel1(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('E'.$i, $paysanUser->getNumTel2(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('F'.$i, $paysanUser->getAddress()->getAdress());
            $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

            $villePaysan = $paysanUser->getVille();
            $excel->getActiveSheet()->setCellValueExplicit('G'.$i, null === $villePaysan ? '' : $villePaysan->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('H'.$i, strtoupper(null === $villePaysan ? '' : $villePaysan->getNom()));
            $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('I'.$i, $annee_adhesion);
            $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValueExplicit('J'.$i, null === $villePaysan ? '' : $villePaysan->getCpString(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('K'.$i, ucfirst($paysanUser->getNewsletter() ? 'oui' : 'non'));
            $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('L'.$i, ucfirst((string) $paysanFerme->getNom()));
            $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('M'.$i, $paysanFerme->getSiret() ?? '');
            $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('N'.$i, $paysanFerme->getVille()->getCpString() ?? '');
            $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $excel->getActiveSheet()->setCellValue('O'.$i, $paysanFerme->getVille()->getNom() ?? '');
            $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

            $excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($style);

            ++$i;
        }

        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $excel->getActiveSheet()->setCellValue('A2', $time);

        $titre = $amap->getNom().' -> paysans';

        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        $excel->getActiveSheet()->setCellValue('A1', $titre);

        $nom_fichier = 'liste_des_paysans_'.date('d_m_Y').'.xls';

        // Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        // if you want to save it as .XLSX Excel 2007 format
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($excel, \PhpOffice\PhpSpreadsheet\IOFactory::WRITER_XLS);
        // force user to download the Excel file without writing it to server's HD
        $outputFileName = tempnam(sys_get_temp_dir(), 'exportPaysans_');
        $objWriter->save($outputFileName);

        return BinaryFileResponseFactory::createFromTempFilePath($outputFileName, $nom_fichier);
    }

    #[Route('amap/telecharger_indicateurs_activite', name: 'amap_telecharger_indicateurs_activite')]
    public function telecharger_indicateurs_activite(ExcelGeneratorExportAmapActivityIndicator $excelGeneratorExportAmapActivityIndicator): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchAmapType::class, null, [
            'currentUser' => $this->getUser(),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $searchAmapState = $searchForm->getData();

        [$amapCount, $outFileName] = $excelGeneratorExportAmapActivityIndicator->genererExportAmap($searchAmapState);

        return BinaryFileResponseFactory::createFromTempFilePath(
            $outFileName,
            sprintf(
                '%s_clicamap_%s.xls',
                $amapCount,
                Carbon::now()->format('Y_m_d')
            )
        );
    }

    #[Route('amap/informations_generales_edition/{amap_id}', name: 'amap_informations_generales_edition')]
    public function informations_generales_edition(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapEditionType::class, $amap, [
            'additionalFields' => $this->authorizationChecker->isGranted(AmapVoter::ACTION_AMAP_CREATE),
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'L\'AMAP a été modifiée avec succès.'
            );

            if ($this->getUser()->isAmapAdminOfAmap($amap)) {
                return $this->redirectToRoute('amap_mon_amap');
            }

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amap/informations_generales_edition.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/informations_generales_creation', name: 'amap_informations_generales_creation')]
    public function informations_generales_creation(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);

        $wizard = $this->wizard_get_session_data();
        if (false === $wizard) {
            $wizard = new \PsrLib\DTO\AmapCreationWizard();
            $amap = new \PsrLib\ORM\Entity\Amap();
        } else {
            $amap = $wizard->getAmap();
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapCreationType::class, $amap, [
            'admin' => $wizard->getAdmin(),
        ]);

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setAmap($amap);
            $wizard->setAdmin($form->get('admin')->getData());
            $this->wizard_store_session_data($wizard);

            return $this->redirectToRoute('amap_livraison_lieu_creation');
        }

        return $this->render('amap/informations_generales_creation.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * LIEUX DE LIVRAISON.
     */
    #[Route('amap/livraison_lieu/{amap_id}', name: 'amap_livraison_lieu')]
    public function livraison_lieu(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $ll */
            $ll = $form->getData();
            $ll->setAmap($amap);
            $this->em->persist($ll);
            $this->em->flush();

            return $this->redirectToRoute('amap_livraison_lieu', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/livraison_lieu.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/livraison_lieu_creation', name: 'amap_livraison_lieu_creation')]
    public function livraison_lieu_creation(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);

        $wizard = $this->wizard_get_session_data();
        $ll = $wizard->getLivraisonLieu();
        if (null === $ll) {
            $ll = new \PsrLib\ORM\Entity\AmapLivraisonLieu();
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class, $ll);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setLivraisonLieu($ll);
            $this->wizard_store_session_data($wizard);

            return $this->redirectToRoute('amap_livraison_horaire_creation');
        }

        return $this->render('amap/livraison_lieu_creation.html.twig', [
            'amap' => $wizard->getAmap(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap/livraison_lieu_edit/{liv_lieu_id}', name: 'amap_livraison_lieu_edit')]
    public function livraison_lieu_edit(int $liv_lieu_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);

        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonLieuType::class, $livraisonLieu);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('amap_livraison_lieu', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/livraison_lieu_edition.html.twig', [
            'amap' => $amap,
            'livraisonLieu' => $livraisonLieu,
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap/livraison_lieu_remove/{liv_lieu_id}', name: 'amap_livraison_lieu_remove')]
    public function livraison_lieu_remove(int $liv_lieu_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);
        $this->denyAccessUnlessGranted(AmapLivraisonLieuVoter::ACTION_AMAP_LL_DELETE, $livraisonLieu);

        $amap = $livraisonLieu->getAmap();

        $this->em->remove($livraisonLieu);
        $this->em->flush();

        return $this->redirectToRoute('amap_livraison_lieu', ['amap_id' => $amap->getId()]);
    }

    #[Route('amap/livraison_horaire_creation', name: 'amap_livraison_horaire_creation')]
    public function livraison_horaire_creation(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        $llh = $wizard->getLivraisonHoraire();
        if (null === $llh) {
            $llh = new \PsrLib\ORM\Entity\AmapLivraisonHoraire();
        }

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $llh);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wizard->setLivraisonHoraire($llh);
            $this->wizard_store_session_data($wizard);

            return $this->redirectToRoute('amap_produits_creation');
        }

        return $this->render('amap/livraison_horaires_creation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * HORAIRES DE LIVRAISON.
     */
    #[Route('amap/livraison_horaire/{liv_lieu_id}', name: 'amap_livraison_horaire')]
    public function livraison_horaire(int $liv_lieu_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $livraisonLieu */
        $livraisonLieu = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonLieu::class, $liv_lieu_id);
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $llh = new \PsrLib\ORM\Entity\AmapLivraisonHoraire();
        $llh->setLivraisonLieu($livraisonLieu);
        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $llh);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($llh);
            $this->em->flush();

            return $this->redirectToRoute('amap_livraison_horaire', ['liv_lieu_id' => $livraisonLieu->getId()]);
        }

        $livraisonHoraires = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class)
            ->getByLivraisonLieuOrderedByStartHour($livraisonLieu)
        ;

        return $this->render('amap/livraison_horaire.html.twig', [
            'amap' => $amap,
            'livraisonHoraires' => $livraisonHoraires,
            'livraisonLieu' => $livraisonLieu,
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap/livraison_horaire_edit/{liv_hor_id}', name: 'amap_livraison_horaire_edit')]
    public function livraison_horaire_edit(int $liv_hor_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire */
        $livraisonHoraire = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class, $liv_hor_id);
        $livraisonLieu = $livraisonHoraire->getLivraisonLieu();
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapLivraisonHoraireType::class, $livraisonHoraire);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('amap_livraison_horaire', ['liv_lieu_id' => $livraisonLieu->getId()]);
        }

        return $this->render('amap/livraison_horaire_edition.html.twig', [
            'amap' => $amap,
            'livraisonLieu' => $livraisonLieu,
            'livraisonHoraire' => $livraisonHoraire,
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap/livraison_horaire_remove/{liv_hor_id}', name: 'amap_livraison_horaire_remove')]
    public function livraison_horaire_remove(int $liv_hor_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapLivraisonHoraire $livraisonHoraire */
        $livraisonHoraire = $this->findOrExit(\PsrLib\ORM\Entity\AmapLivraisonHoraire::class, $liv_hor_id);
        $livraisonLieu = $livraisonHoraire->getLivraisonLieu();
        $amap = $livraisonLieu->getAmap();
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $this->em->remove($livraisonHoraire);
        $this->em->flush();

        return $this->redirectToRoute('amap_livraison_horaire', ['liv_lieu_id' => $livraisonLieu->getId()]);
    }

    #[Route('amap/produits_creation', name: 'amap_produits_creation')]
    public function produits_creation(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        $amap = $wizard->getAmap();
        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitType::class, $amap);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->wizard_store_session_data($wizard);

            return $this->redirectToRoute('amap_confirmation');
        }

        return $this->render('amap/produits_creation.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    #[Route('amap/produits/{amap_id}', name: 'amap_produits')]
    public function produits(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitType::class, $amap);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash('notice_success', 'L\'AMAP a été modifiée avec succès.');

            return $this->redirectToRoute('amap_produits', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/produits_edition.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/recherche_paysan/{amap_id}', name: 'amap_recherche_paysan')]
    public function recherche_paysan(int $amap_id): Response
    {
        /** @var Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_SEARCH_PAYSAN, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapProduitRechercheType::class, $amap);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recherche = new \PsrLib\ORM\Entity\AmapRecherchePaysan($amap);
            $this->em->persist($recherche);
            $this->em->flush();

            $this->addFlash('notice_success', 'L\'AMAP a été modifiée avec succès.');

            return $this->redirectToRoute('amap_mon_amap');
        }

        return $this->render('amap/produits_edition.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    /**
     * Confirmation avant sauvegarde des données.
     */
    #[Route('amap/confirmation', name: 'amap_confirmation')]
    public function confirmation(): Response
    {
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CREATE);
        $wizard = $this->wizard_get_session_data();

        // Rebuild amap entity
        $amap = $wizard->getAmap();
        $ll = $wizard->getLivraisonLieu();
        $llh = $wizard->getLivraisonHoraire();

        $llh->setLivraisonLieu($ll);
        $ll->addLivraisonHoraire($llh);

        $ll->setAmap($amap);
        $amap->addLivraisonLieux($ll);

        $admin = $wizard->getAdmin();
        if (null !== $admin) {
            $admin->addAdminAmap($amap);
        }

        if (\Symfony\Component\HttpFoundation\Request::METHOD_POST === $this->request->getMethod()) {
            $this->em->persist($amap);
            $this->em->flush();

            // clean cache data
            $this->wizard_clear_session_data();

            $this->addFlash(
                'notice_success',
                'L\'AMAP a été crée avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amap/confirmation.html.twig', [
            'amap' => $amap,
            'admin' => $admin,
        ]);
    }

    /**
     * Afficher les gestionnaires de l'AMAP.
     */
    #[Route('amap/collectif/{amap_id}', name: 'amap_collectif')]
    public function collectif(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $collectif = new \PsrLib\ORM\Entity\AmapCollectif($amap);
        $form = $this->formFactory->create(\PsrLib\Form\AmapCollectifType::class, $collectif);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($collectif);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le rôle a été ajouté avec succès.'
            );

            return $this->redirectToRoute('amap_collectif', ['amap_id' => $amap->getId()]);
        }

        $collectif = $amap->getCollectifs()->toArray();
        usort($collectif, function (AmapCollectif $a, AmapCollectif $b) {
            $profileToOrder = fn (\PsrLib\ORM\Entity\AmapCollectif $a) => match ($a->getProfil()) {
                \PsrLib\ORM\Entity\AmapCollectif::PROFIL_PRESIDENT => 1,
                \PsrLib\ORM\Entity\AmapCollectif::PROFIL_TRESORIER => 2,
                \PsrLib\ORM\Entity\AmapCollectif::PROFIL_SECRETAIRE => 3,
                \PsrLib\ORM\Entity\AmapCollectif::PROFIL_AUTRE => 4,
                default => 5,
            };

            return $profileToOrder($a) - $profileToOrder($b);
        });

        return $this->render('amap/collectif_display.html.twig', [
            'collectif' => $collectif,
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/collectif_remove/{coll_id}', name: 'amap_collectif_remove')]
    public function collectif_remove(int $coll_id): Response
    {
        /** @var \PsrLib\ORM\Entity\AmapCollectif $collectif */
        $collectif = $this->findOrExit(\PsrLib\ORM\Entity\AmapCollectif::class, $coll_id);
        $amap = $collectif->getAmap();

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $this->em->remove($collectif);
        $this->em->flush();

        return $this->redirectToRoute('amap_collectif', ['amap_id' => $amap->getId()]);
    }

    /**
     * Changer le référent réseau.
     */
    #[Route('amap/referent_edition/{amap_id}', name: 'amap_referent_edition')]
    public function referent_edition(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ref = $form->get('referent')->getData();
            $amap->setAmapienRefReseau($ref);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le référent réseau a été mis à jour avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amap/referent_edition.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/referent_suppression/{amap_id}', name: 'amap_referent_suppression', methods: ['POST'])]
    public function referent_suppression(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $amap->setAmapienRefReseau(null);
        $this->em->flush();

        return $this->redirectToRoute('amap_referent_edition', ['amap_id' => $amap->getId()]);
    }

    /**
     * Changer le référent réseau.
     */
    #[Route('amap/referent_second_edition/{amap_id}', name: 'amap_referent_second_edition')]
    public function referent_second_edition(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ref = $form->get('referent')->getData();
            $amap->setAmapienRefReseauSecondaire($ref);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le référent réseau secondaire a été mis à jour avec succès.'
            );

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('amap/referent_second_edition.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('amap/referent_second_suppression/{amap_id}', name: 'amap_referent_second_suppression', methods: ['POST'])]
    public function referent_second_suppression(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $amap->setAmapienRefReseauSecondaire(null);
        $this->em->flush();

        return $this->redirectToRoute('amap_referent_second_edition', ['amap_id' => $amap->getId()]);
    }

    /*
     *
     * Afficher les gestionnaires de l'AMAP.
     *
     * @param $amap_id
     */
    #[Route('amap/gestionnaire_display/{amap_id}', name: 'amap_gestionnaire_display')]
    public function gestionnaire_display(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\AmapReferentType::class, null, [
            'amap' => $amap,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $amapien = $form->get('referent')->getData();

            if ($amapien->isGestionnaire()) {
                $this->addFlash(
                    'notice_error',
                    'L\'amapien est déjà gestionnaire de l\'AMAP !'
                );

                return $this->redirectToRoute('amap_gestionnaire_display', ['amap_id' => $amap->getId()]);
            }

            $amapien->setGestionnaire(true);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Le correspondant Clic\'AMAP a été ajouté avec succès.'
            );

            return $this->redirectToRoute('amap_gestionnaire_display', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/gestionnaire_display.html.twig', [
            'amap' => $amap,
            'form' => $form->createView(),
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            'gestionnaires' => $amap->getAmapiens()->matching(\PsrLib\ORM\Repository\AmapienRepository::criteriaGestionaire()),
        ]);
    }

    /**
     * Supprimer le droit gestionnaire à un amapien.
     *
     * @param $amap_id
     */
    #[Route('amap/gestionnaire_remove/{amapien_id}', name: 'amap_gestionnaire_remove')]
    public function gestionnaire_remove(int $amapien_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $amap = $amapien->getAmap();
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_DISPLAY, $amap);

        $amapien->setGestionnaire(false);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Le correspondant Clic\'AMAP a été supprimé avec succès.'
        );

        return $this->redirectToRoute('amap_gestionnaire_display', ['amap_id' => $amap->getId()]);
    }

    /**
     * Créer un mot de passe pour une AMAP et lui envoyer par email.
     */
    #[Route('amap/password/{amap_id}', name: 'amap_password')]
    public function password(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_SEND_PASSWORD, $amap);

        foreach ($amap->getAdmins() as $admin) {
            $password = $this->passwordGenerator->generatePassword();
            $pass_hash = $this->passwordEncoder->encodePassword($password);

            $admin->setPassword($pass_hash);
            $this->em->flush();

            $this->emailSender->envoyerMailMdp($admin, $password);
        }

        $this->addFlash(
            'notice_success',
            'Le mot de passe a été envoyé avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    /**
     * Supprimer une AMAP.
     */
    #[Route('amap/remove/{amap_id}', name: 'amap_remove')]
    public function remove(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        // Sécurité
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_CHANGE_STATE, $amap);

        $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->remove($amap);

        $this->addFlash(
            'notice_success',
            'L\'AMAP a été supprimée avec succès.'
        );

        return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
    }

    #[Route('amap/admins/{amap_id}', name: 'amap_admins')]
    public function admins(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_EDIT_ADMINS, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\UserVerificationType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ?\PsrLib\ORM\Entity\User $newAdmin */
            $newAdmin = $form->get('user')->getData();
            if (null === $newAdmin) {
                return $this->render('amap/admin.html.twig', [
                    'form' => $form->createView(),
                    'amap' => $amap,
                    'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
                    'alertNotFound' => true,
                ]);
            }
            if ($newAdmin->isAmapAdminOfAmap($amap)) {
                $this->addFlash(
                    'notice_error',
                    'L\'utilisateur est déjà administrateur de l\'AMAP !'
                );

                return $this->redirectToRoute('amap_admins', ['amap_id' => $amap->getId()]);
            }

            $newAdmin->addAdminAmap($amap);
            $this->em->flush();
            $this->addFlash(
                'notice_success',
                'Les administrateurs ont été mis à jour avec succès.'
            );

            return $this->redirectToRoute('amap_admins', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/admin.html.twig', [
            'form' => $form->createView(),
            'amap' => $amap,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
            'alertNotFound' => false,
        ]);
    }

    #[Route('amap/admins_remove/{amap_id}/{user_id}', name: 'amap_admins_remove', methods: ['POST'])]
    public function admins_remove(int $amap_id, int $user_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        /** @var \PsrLib\ORM\Entity\User $user */
        $user = $this->findOrExit(\PsrLib\ORM\Entity\User::class, $user_id);

        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_EDIT_ADMINS, $amap);

        if ($user === $this->getUser()) {
            $this->addFlash(
                'notice_error',
                'Impossible de se supprimer soit-même des administrateurs de l\'AMAP.'
            );

            return $this->redirectToRoute('amap_admins', ['amap_id' => $amap->getId()]);
        }

        $user->removeAdminAmap($amap);
        $this->em->flush();

        $this->addFlash(
            'notice_success',
            'Les administrateurs ont été mis à jour avec succès.'
        );

        return $this->redirectToRoute('amap_admins', ['amap_id' => $amap->getId()]);
    }

    #[Route('amap/admins_creation/{amap_id}', name: 'amap_admins_creation')]
    public function admins_creation(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(AmapVoter::ACTION_AMAP_EDIT_ADMINS, $amap);

        $form = $this->formFactory->create(\PsrLib\Form\UserProfilType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->addAdminAmap($amap);
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash(
                'notice_success',
                'Administrateur ajouté avec succès.'
            );

            return $this->redirectToRoute('amap_admins', ['amap_id' => $amap->getId()]);
        }

        return $this->render('amap/admin_creation.html.twig', [
            'form' => $form->createView(),
            'amap' => $amap,
        ]);
    }

    private function wizard_store_session_data(AmapCreationWizard $wizard): void
    {
        $this->sfSession->set(
            'amap_wizard',
            $this->serializer->serialize($wizard, 'json', [
                'json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION,
                'groups' => 'wizardAmap',
            ])
        );
    }

    private function wizard_get_session_data(): false|AmapCreationWizard
    {
        try {
            $wizard = $this
                ->serializer
                ->deserialize($this->sfSession->get('amap_wizard', ''), \PsrLib\DTO\AmapCreationWizard::class, 'json', [
                    'groups' => 'wizardAmap',
                ])
            ;
        } catch (\Exception $e) {
            return false;
        }

        return $wizard;
    }

    private function wizard_clear_session_data(): void
    {
        $this->sfSession->set('amap_wizard', '');
    }

    /**
     * Générer la CARTOGRAPHIE.
     *
     * @param $amaps \PsrLib\ORM\Entity\Amap[]
     */
    private function cartographie(array $amaps): array
    {
        $gps = 0;
        $map = null;

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        foreach ($amaps as $amap) {
            /** @var \PsrLib\ORM\Entity\AmapLivraisonLieu $liv_lieu */
            foreach ($amap->getLivraisonLieux() as $liv_lieu) {
                if ($liv_lieu->getGpsLatitude()) {
                    ++$gps;

                    $config = [
                        'center' => $liv_lieu->getGpsLatitude()
                            .','
                            .$liv_lieu->getGpsLongitude(),
                        'zoom' => 7,
                    ];

                    $this->leaflet->initialize($config);

                    // infos du popup
                    $infos = '<strong>'.$amap->getNom().'</strong>';
                    if ($liv_lieu->getNom()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getNom();
                    }
                    if ($liv_lieu->getAdresse()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getAdresse();
                    }
                    if ($liv_lieu->getVille()->getCpString()) {
                        $infos = $infos.'<br/>'.$liv_lieu->getVille()->getCpString();
                    }
                    if ($liv_lieu->getVille()->getNom()) {
                        $infos = $infos.', '.$liv_lieu->getVille()->getNom();
                    }
                    if ($amap->getEmail()) {
                        $infos = $infos.'<br/>'.$amap->getEmail();
                    }
                    if ($amap->getUrl()) {
                        $infos = $infos.'<br/>'.$amap->getUrl();
                    }

                    // Emplacement du marker
                    $marker = [
                        'latlng' => $liv_lieu->getGpsLatitude().
                            ','.
                            $liv_lieu->getGpsLongitude(), // Marker Location
                        // popup
                        'popupContent' => addslashes($infos), // Popup Content
                    ];

                    $this->leaflet->add_marker($marker);
                }
            }

            if ($gps > 0) {
                $map = $this->leaflet->create_map();
            }
        }

        return [$gps, $map];
    }
}
