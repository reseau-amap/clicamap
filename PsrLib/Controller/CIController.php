<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use DI\Container;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\Services\FixtureLoader;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CIController extends AbstractController
{
    public function __construct(
        private readonly Container $container,
    ) {
    }

    // As loader is not defined in production, lazy load it to avoid crash
    private function getLoader(): FixtureLoader
    {
        return $this->container->get(FixtureLoader::class);
    }

    #[Route('/CI/resetdb', name: 'ci_resetDb')]
    public function resetDb(): Response
    {
        $this->assertTestEnv();
        $this->getLoader()->loadFixtures();

        return $this->printSuccess();
    }

    #[Route('/CI/login', name: 'ci_login')]
    public function login(): Response
    {
        $this->assertTestEnv();

        $user = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\User::class)
            ->findByUsernameOrEmail($this->request->get('username'))
        ;

        if (!$user) {
            return $this->redirectToRoute('portail_connexion');
        }

        $this->sfSession->set('ID', $user->getId());

        return $this->redirectToRoute('evenements');
    }

    #[Route('/CI/addLlToAmap', name: 'ci_addLlToAmap')]
    public function addLlToAmap(): Response
    {
        $this->assertTestEnv();

        $amap = $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findOneBy([
            'email' => $this->request->get('amap'),
        ]);
        $ll = new \PsrLib\ORM\Entity\AmapLivraisonLieu();
        $ll->setNom($this->request->get('ll'));
        $ll->setAmap($amap);
        $this->em->persist($ll);
        $this->em->flush();

        return $this->printSuccess();
    }

    #[Route('/CI/forceContratDepassement', name: 'ci_forceContratDepassement')]
    public function forceContratDepassement(): Response
    {
        $this->assertTestEnv();

        /** @var \PsrLib\ORM\Entity\ModeleContrat $mc */
        $mc = $this->em->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)->findOneBy([
            'nom' => $this->request->get('mc'),
        ]);
        $mc->getInformationLivraison()->setNblivPlancherDepassement(true);
        $this->em->flush();

        return $this->printSuccess();
    }

    #[Route('/CI/forceContratProduitRegul', name: 'ci_forceContratProduitRegul')]
    public function forceContratProduitRegul(): Response
    {
        $this->assertTestEnv();

        /** @var \PsrLib\ORM\Entity\ModeleContratProduit $mcp */
        $mcp = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratProduit::class)
            ->createQueryBuilder('mcp')
            ->leftJoin('mcp.modeleContrat', 'mc')
            ->where('mc.nom = :mc')
            ->setParameter('mc', $this->request->get('mc'))
            ->andWhere('mcp.nom = :mcp')
            ->setParameter('mcp', $this->request->get('produit'))
            ->getQuery()
            ->getSingleResult()
        ;
        $mcp->setRegulPds(true);
        $this->em->flush();

        return $this->printSuccess();
    }

    #[Route('/CI/forceContratPaye', name: 'ci_forceContratPaye')]
    public function forceContratPaye(): Response
    {
        $this->assertTestEnv();

        /** @var \PsrLib\ORM\Entity\Contrat $contrat */
        $contrat = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->createQueryBuilder('c')
            ->leftJoin('c.amapien', 'a')
            ->leftJoin('a.user', 'u')
            ->leftJoin('c.modeleContrat', 'mc')
            ->where('u.username = :amapien')
            ->setParameter('amapien', $this->request->get('amapien'))
            ->andWhere('mc.nom = :mc')
            ->setParameter('mc', $this->request->get('mc'))
            ->getQuery()
            ->getSingleResult()
        ;
        $contrat->setCdpPaid(true);
        $this->em->flush();

        return $this->printSuccess();
    }

    private function assertTestEnv(): void
    {
        if (!in_array(getenv('CI_ENV'), ['testing', 'development'], true)) {
            $this->exit_error();
        }
    }

    #[Route('/CI/addUserToAmap', name: 'ci_addUserToAmap')]
    public function addUserToAmap(): Response
    {
        $this->assertTestEnv();

        /** @var \PsrLib\ORM\Entity\User $user */
        $user = $this->em->getRepository(\PsrLib\ORM\Entity\User::class)->findOneBy([
            'username' => $this->request->get('user_name'),
        ]);
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findOneBy([
            'nom' => $this->request->get('amap_name'),
        ]);

        $amapien = new \PsrLib\ORM\Entity\Amapien(
            $user,
            $amap
        );
        $amapien->setEtat(ActivableUserInterface::ETAT_ACTIF);
        $this->em->persist($amapien);
        $this->em->flush();

        return $this->printSuccess();
    }

    #[Route('/CI/empty', name: 'ci_empty')]
    public function empty(): Response
    {
        $this->assertTestEnv();

        return $this->printSuccess();
    }

    private function printSuccess(): Response
    {
        return new Response('ok');
    }
}
