<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\DTO\Response\ZxcvbnResponse;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\Services\Password\PasswordEncoder;
use PsrLib\Services\Zxcvbn;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserProfilController extends AbstractController
{
    #[Route('user_profil', name: 'user_profil')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        return $this->render('user_profil/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    #[Route('user_profil/edit', name: 'user_profil_edit')]
    public function edit(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);
        $form = $this->formFactory->create(\PsrLib\Form\UserProfilType::class, $this->getUser(), [
            'disable_profil_name_edit' => true,
        ]);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Votre profil a bien été mis à jour.');

            return $this->redirectToRoute('user_profil');
        }

        return $this->render('user_profil/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Page de modification du mot de passe.
     */
    #[Route('user_profil/mot_de_passe', name: 'user_profil_mot_de_passe')]
    public function mot_de_passe(PasswordEncoder $passwordEncoder): Response
    {
        $currentUser = $this->getUser();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_CHANGE_PASSWORD, $currentUser);

        $form = $this->formFactory->create(\PsrLib\Form\UserPasswordType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Re initialise le mot de passe et supprime le token
            $currentUser->setPassword($passwordEncoder->encodePassword($form->get('password')->getData()));
            $this->em->flush();

            $this->addFlash('success', 'Mot de passe modifié avec succès.');

            return $this->redirectToRoute('evenements');
        }

        return $this->render('user_profil/mdp.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Test la sécurité du mot de passe dans la page de protection des AMAPS.
     */
    #[Route('user_profil/mot_de_passe_test', name: 'user_profil_mot_de_passe_test')]
    public function mot_de_passe_test(Request $request, SerializerInterface $serializer, Zxcvbn $zxcvbn): Response
    {
        $currentUser = $this->getUser();
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_CHANGE_PASSWORD, $currentUser);

        $strength = $zxcvbn->estimateScore($request->request->get('zxcvbn_password'));

        return new ZxcvbnResponse($strength);
    }

    #[Route('user_profil/documents', name: 'user_profil_documents')]
    public function documents(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        /** @var \PsrLib\ORM\Entity\User $currentUser */
        $currentUser = $this->getUser();

        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmapien::class)
            ->findBy([
                'amapien' => $currentUser,
                'state' => AdhesionAmapien::STATE_GENERATED,
            ])
        ;
        $adhesions_amap_amapien = $this
            ->em
            ->getRepository(AdhesionAmapAmapien::class)
            ->findGeneratedByUser($currentUser)
        ;
        $campagne_bulletins_amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\CampagneBulletin::class)
            ->findByUser($currentUser)
        ;

        $documentUtilisateur = array_merge(
            $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\DocumentUtilisateurAmap::class)
                ->findByUser($currentUser),
            $this
                ->em
                ->getRepository(\PsrLib\ORM\Entity\DocumentUtilisateurFerme::class)
                ->findByUser($currentUser)
        );

        return $this->render('user_profil/documents.html.twig', [
            'adhesions' => array_merge($adhesions, $adhesions_amap_amapien),
            'bulletins' => $campagne_bulletins_amapiens,
            'documentUtilisateur' => $documentUtilisateur,
        ]);
    }
}
