<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Assert\Assertion;
use DI\Attribute\Inject;
use Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter;
use PsrLib\Exception\PermissionErrorException;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Authentification;
use PsrLib\Services\Uploader;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Common features between codeigniter base controller
 * And symfony base controller.
 */
trait AbstractControllerTrait
{
    #[Inject]
    public Authentification $authentification;

    #[Inject]
    protected \Doctrine\ORM\EntityManagerInterface $em;

    #[Inject]
    protected \Symfony\Component\Form\FormFactoryInterface $formFactory;

    #[Inject]
    public Uploader  $uploader;

    #[Inject]
    protected \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker;

    #[Inject]
    protected \Twig\Environment $twig;

    #[Inject]
    protected \Psr\Log\LoggerInterface $logger;

    #[Inject]
    protected \Symfony\Component\HttpFoundation\Session\Session  $sfSession;

    #[Inject]
    protected \Symfony\Component\HttpFoundation\Request $request;

    #[Inject]
    protected UrlGeneratorInterface $urlGenerator;

    #[Inject]
    protected Serializer $serializer;

    /**
     * Get current connected user.
     */
    public function getUser(): ?User
    {
        return $this->authentification->getCurrentUser();
    }

    /**
     * @throws PermissionErrorException
     */
    public function exit_error(): void
    {
        $this->logger->notice('Exit error');
        $this->sfSession->invalidate();
        $this->sfSession->getFlashBag()->set(
            'error_permission',
            'Vous n\'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter.'
        );

        throw new PermissionErrorException();
    }

    /**
     * find existing entity on doctrine or exit.
     *
     * @template T of object
     *
     * @param class-string<T> $class
     * @param string|int|null $id
     *
     * @return object
     */
    public function findOrExit(string $class, $id)
    {
        if (null === $id) {
            $this->exit_error();
        }
        $elt = $this->em->find($class, $id);
        if (null === $elt) {
            $this->exit_error();
        }

        return $elt;
    }

    public function denyUnlessRequestMethod(string $method): void
    {
        if ($this->request->getMethod() !== $method) {
            $this->exit_error();
        }
    }

    /**
     * This method is not a strong security check
     * Only use it to ensure that the request is an ajax request and avoid users to accidentally access a page.
     * Use it in combinaison with other security checks.
     */
    public function denyUnlessXmlHttpRequest(): void
    {
        if (!$this->request->isXmlHttpRequest()) {
            $this->exit_error();
        }
    }

    /**
     * @param string     $action
     * @param mixed|null $subject
     */
    public function denyAccessUnlessGranted($action, $subject = null): void
    {
        $access = $this->authorizationChecker->isGranted($action, $subject);

        if (false === $access) {
            $this->logger->notice(sprintf('Access denied for %s', $action));
            $this->exit_error();
        }
    }

    /**
     * @param string $action
     */
    public function denyAccessUnlessGrantedMultiple($action, array $subjects): void
    {
        if (0 === count($subjects)) {
            $this->exit_error();
        }
        foreach ($subjects as $subject) {
            $access = $this->authorizationChecker->isGranted($action, $subject);

            if (false === $access) {
                $this->logger->notice(sprintf('Access denied for %s', $action));
                $this->exit_error();
            }
        }
    }

    /**
     * Redirect user to stored get param. Store in other case.
     */
    protected function redirectFromStoredGetParamIfExist(string $key): void
    {
        $sessionKey = sprintf('stored_get_params_%s', $key);

        if (0 === $this->request->query->count()) {
            if ($this->sfSession->has($sessionKey)) {
                $lastParams = $this->sfSession->get($sessionKey, []);
                if (count($lastParams) > 0) {
                    // bind flash data
                    foreach ($this->sfSession->getFlashBag()->all() as $flashKey => $flashMessage) {
                        $this->sfSession->getFlashBag()->set($flashKey, $flashMessage);
                    }

                    redirect( // Keep codeigniter function to avoid side effects
                        sprintf(
                            '%s?%s',
                            $this->request->getPathInfo(),
                            http_build_query($lastParams)
                        )
                    );
                }

                return;
            }
        }

        $this->sfSession->set($sessionKey, $this->request->query->all());
    }

    protected function render(string $view, array $parameters = []): Response
    {
        // Render on variable to avoid header already send error
        $rendered = $this
            ->twig
            ->render($view, $parameters)
        ;

        return new Response($rendered);
    }

    /**
     * @param ?object $data
     */
    protected function createForm(string $type, $data = null, array $options = []): FormInterface
    {
        return $this->formFactory->create($type, $data, $options);
    }

    protected function generateUrl(string $route, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->urlGenerator->generate($route, $parameters, $referenceType);
    }

    /**
     * Returns a RedirectResponse to the given URL.
     */
    protected function redirect(string $url, int $status = 302): RedirectResponse
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Returns a RedirectResponse to the given route with the given parameters.
     */
    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
    {
        return $this->redirect($this->generateUrl($route, $parameters), $status);
    }

    protected function redirectHtmx(string $url): Response
    {
        $res = new Response();
        $res->headers->set('HX-Redirect', $url);

        return $res;
    }

    /**
     * Add flash message on current session.
     *
     * @param string       $type
     * @param array|string $messages
     */
    protected function addFlash($type, $messages): void
    {
        $this->sfSession->getFlashBag()->set($type, $messages);
    }

    protected function session_store_home_path(string $id, ?string $pathInfo = null): void
    {
        $this->sfSession->set(
            $id,
            null === $pathInfo ? $this->request->getPathInfo() : $pathInfo
        );
    }

    protected function session_get_home_path(string $id): string
    {
        $homePath = $this->sfSession->get($id);
        if (null === $homePath) {
            return $this->generateUrl('index');
        }

        return $homePath;
    }

    /**
     * @param class-string $class
     */
    protected function disableSoftDeleteFor(string $class): void
    {
        /**
         * @var SoftDeleteableFilter $filter
         */
        $filter = $this->em->getFilters()->getFilter('soft-deleteable');
        $filter->disableForEntity($class);
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    protected function parseQuerySearchDatatable(string $class): object
    {
        Assertion::subclassOf($class, \PsrLib\DTO\SearchDatatable::class);

        return $this
            ->serializer
            ->denormalize(
                $this->request->query->all(),
                $class,
            )
        ;
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $entityClass
     *
     * @return array<int, T>
     */
    protected function parseTableSelectQueryParams(string $entityClass): array
    {
        $selected = $this->request->get('selected', '');
        $ids = explode(',', $selected);

        return array_map(fn ($id) => $this->findOrExit($entityClass, $id), $ids);
    }
}
