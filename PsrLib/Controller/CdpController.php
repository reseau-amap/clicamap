<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\DTO\UserLogin;
use PsrLib\ORM\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CdpController extends AbstractController
{
    public function __construct(
        private readonly \PsrLib\Services\JwtToken $jwtTokenBuilder,
    ) {
    }

    #[Route('/cdp/authentification', name: 'cdp_authentification')]
    public function authentification(): Response
    {
        $afterAuthRedirectUrl = $this->request->query->get('redirectUrl');
        if (null === $afterAuthRedirectUrl) {
            return $this->redirectToRoute('index');
        }

        $currentUser = $this->getUser();
        if (null !== $currentUser) {
            $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CdpAuthVoter::ACTION_CDP_AUTH);

            return $this->redirectUser($currentUser, $afterAuthRedirectUrl);
        }

        $form = $this->formFactory->create(\PsrLib\Form\AuthentificationCdpType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserLogin $data */
            $data = $form->getData();

            return $this->redirectUser($data->getUser(), $afterAuthRedirectUrl);
        }

        return $this->render('cdp/authentification.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function redirectUser(User $user, string $redirectUrl): RedirectResponse
    {
        $token = $this->jwtTokenBuilder->buildToken('authentification', [
            'amapien_uuid' => $user->getUuid(),
        ]);

        return new RedirectResponse($redirectUrl.'?token='.$token->toString());
    }
}
