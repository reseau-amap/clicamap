<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Controller;

use DI\Attribute\Inject;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PsrLib;
use PsrLib\ORM\Entity\Ville;
use PsrLib\Services\AdhesionImport;
use PsrLib\Services\Security\Voters\ImportVoter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ImportController extends AbstractController
{
    final public const UPLOAD_PATH = \PsrLib\ProjectLocation::PROJECT_ROOT.'/application/writable/import/';

    #[Inject]
    public AdhesionImport $adhesionimport;

    #[Inject]
    public \PsrLib\Services\NominatimClient $nominatimClient;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\ImportAmapBuilder $importamapbuilder;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\ImportAmapienBuilder $importamapienbuilder;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\ImportPaysanBuilder $importpaysanbuilder;

    #[Inject]
    public \Symfony\Component\Validator\Validator\ValidatorInterface $validation;

    protected array $data = [
        'entete' => [],
        'valeurs' => [],
        'amap_id' => null,
        'amap_erreur' => null,
        'amapiens_erreur' => null,
        'paysans_erreur' => null,
        'amap_succes' => null,
        'amapiens_succes' => null,
        'paysans_succes' => null,
    ];

    /**
     * Import constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        // Pour la vérification EMAIL
        $this->data['amapien_emails'] = [];
        $this->data['amap_emails'] = $em->getRepository(\PsrLib\ORM\Entity\Amap::class)->getAllEmails();
        $this->data['paysan_emails'] = $em->getRepository(\PsrLib\ORM\Entity\Paysan::class)->getAllEmails();
        $this->data['ferme_siret'] = $em->getRepository(\PsrLib\ORM\Entity\Ferme::class)->getAllSIRET();
    }

    /**
     * Lecture du fichier.
     *
     * @return bool
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function file_to_php(mixed $fichier)
    {
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT);

        // Lecture du fichier
        $objPHPExcel = IOFactory::load($fichier);

        // Cell Collection
        $sheet = $objPHPExcel->getActiveSheet();

        $entete = [];
        $arr_data = [];
        // Extraction vers des tableaux PHP
        foreach ($sheet->getRowIterator(1, $sheet->getHighestDataRow()) as $row) {
            $cellIterator = $row->getCellIterator();

            foreach ($cellIterator as $cell) {
                $data_value = $cell->getValue();
                if (1 === $row->getRowIndex()) {
                    $entete[$row->getRowIndex()][$cell->getColumn()] = $data_value;
                } else {
                    $arr_data[$row->getRowIndex()][$cell->getColumn()] = $data_value;
                }
            }
        }
        // Tableaux PHP
        $this->data['entete'] = $entete;
        $this->data['valeurs'] = $arr_data;

        return true;
    }

    /**
     * L'email existe-il en doublon dans le fichier ?
     *
     * @return array
     */
    public function _email_doublon_check(mixed $colonne)
    {
        $email_array = [];
        $email_doublon_array = [];
        foreach ($this->data['valeurs'] as $valeur) {
            foreach ($valeur as $k => $v) {
                $v = trim((string) $v);
                // Le champ ne doit pas être vide
                if ($k == $colonne && !empty($v)) {
                    if ('=' == $v[0]) { // =HYPERLINK
                        $v = str_replace('=HYPERLINK("mailto:', '', $v);
                        $v = explode('","', $v);
                        $v = trim($v[0] ?? '');
                    }
                    array_push($email_array, $v);
                }
            }
        }
        // Email
        if (!empty($email_array)) {
            // Compte le nombre d'occurence pour un email
            $email_nb = array_count_values($email_array);
            // Si l'email apparaît plusieurs fois -> erreur
            foreach ($email_nb as $email => $e_nb) {
                if (1 != $e_nb) {
                    array_push($email_doublon_array, $email);
                }
            }
        }

        return $email_doublon_array;
    }

    /**
     * Le siret existe-il en doublon dans le fichier ?
     *
     * @return array
     */
    public function _siret_doublon_check(mixed $colonne)
    {
        $siret_array = [];
        $siret_doublon_array = [];

        foreach ($this->data['valeurs'] as $valeur) {
            foreach ($valeur as $k => $v) {
                $v = trim((string) $v);
                // Le champ ne doit pas être vide
                if ($k == $colonne && !empty($v)) {
                    array_push($siret_array, $v);
                }
            }
        }
        // Siret
        if (!empty($siret_array)) {
            // Compte le nombre d'occurence pour un siret
            $siret_nb = array_count_values($siret_array);

            // Si le siret apparaît plusieurs fois -> erreur
            foreach ($siret_nb as $siret => $s_nb) {
                if (1 != $s_nb) {
                    array_push($siret_doublon_array, $siret);
                }
            }
        }

        return $siret_doublon_array;
    }

    /**
     * L'email existe-il déjà dans la BDD ?
     *
     * @return bool
     */
    public function _amapien_email_check(mixed $email, PsrLib\ORM\Entity\Amap $amap)
    {
        if (!isset($this->data['amapien_emails'][$amap->getId()])) {
            $this->data['amapien_emails'][$amap->getId()] = $this->em->getRepository(\PsrLib\ORM\Entity\Amapien::class)->getAllEmails($amap);
        }

        return !in_array($email, $this->data['amapien_emails'][$amap->getId()], true);
    }

    /**
     * @return bool
     */
    public function _amap_email_check(mixed $email_public)
    {
        return !in_array($email_public, $this->data['amap_emails'], true);
    }

    /**
     * @return bool
     */
    public function _paysan_email_check(mixed $email)
    {
        return !in_array($email, $this->data['paysan_emails'], true);
    }

    /**
     * Le siret existe-il déjà dans la BDD ?
     *
     * @return bool
     */
    public function _ferme_siret_exist_check(mixed $siret)
    {
        return !in_array($siret, $this->data['ferme_siret'], true);
    }

    /**
     * Le couple CP / Ville existe-il dans la BDD ?
     *
     * @return bool
     */
    public function _cp_ville_check(mixed $cp, mixed $ville)
    {
        $ville = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ville::class)
            ->findOneBy([
                'cp' => $cp,
                'nom' => $ville,
            ])
        ;

        return null !== $ville;
    }

    /**
     * Sélectionner toutes les villes d'un CP et le retourne sous forme de chaîne.
     *
     * @return Ville[]
     */
    public function cp_ville(mixed $cp)
    {
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT);

        return $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Ville::class)
            ->findBy([
                'cp' => $cp,
            ])
        ;
    }

    /**
     * Index.
     */
    #[Route('import', name: 'import_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT);

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\ImportChooseAmapType::class, null, [
            'current_user' => $currentUser,
        ]);

        $this->data['form'] = $form->createView();

        return $this
            ->render('import/index.html.twig', $this->data)
        ;
    }

    #[Route('import/amap', name: 'import_amap')]
    public function amap(): Response
    {
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT_AMAP_PAYSAN);

        $file = $this->getUploadedFile('amap');
        // FICHIER MAL CONFIGURÉ ---------------------------------------------------
        if (null === $file) {
            $this->data['amap_erreur'] = 'Vous n\'avez pas sélectionné de fichier à envoyer.';

            return $this->index();
        } // CONFIG OK ---------------------------------------------------------------

        try {
            $this->file_to_php($file->getFileInfo()->getPathname());
        } catch (\Exception $e) {
            $this->data['amap_erreur'] = 'Impossible de décoder le fichier';

            return $this->index();
        }

        // VÉRIFICATION DE L'ENTÊTE (À VÉRIFIER / COMPLÉTER) ---------------------
        foreach ($this->data['entete'] as $entete) {
            if ('nom*' != trim((string) $entete['A'])) {
                $this->data['amap_erreur'] = 'Le fichier téléchargé n\'a pas d\'entête ou ce dernier est mal formaté. L\'entête doit être le même que celui du fichier exemple "amap.xls".<br/>';

                break;
            }
        }

        // VÉRIFICATION GÉNÉRALE -------------------------------------------------
        // Email public en doublon dans le fichier ?
        $doublons = $this->_email_doublon_check('I');

        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['amap_erreur'] = $this->data['amap_erreur'].'L\'email de contact public <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        }

        // Email Référent Réseau en doublon dans le fichier ?
        $doublons = $this->_email_doublon_check('AY');

        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['amap_erreur'] = $this->data['amap_erreur'].'L\'email du contact référent réseau <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        }

        // Email Second Référent Réseau en doublon dans le fichier ?
        $doublons = $this->_email_doublon_check('BF');

        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['amap_erreur'] = $this->data['amap_erreur'].'L\'email du second contact référent réseau <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        } // VÉRIFICATION DES CELLULES ---------------------------------------------

        if ($this->data['amap_erreur']) {
            $this->data['amap_erreur'] = '<strong>FICHIER</strong><br/>'.$this->data['amap_erreur'].'<br/>';
        }

        $ligne = 2;

        foreach ($this->data['valeurs'] as $valeur) {
            $cp = null;
            $ville = null;
            $ref_cp = null;
            $ref_ville = null;
            $admin_cp = null;
            $admin_ville = null;
            $alertes = [];

            foreach ($valeur as $k => $v) {
                $v = trim((string) $v);
                // NOM
                if ('A' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "nom" est requis');
                }

                // ADRESSE
                if ('C' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "Adresse de livraison" est requis');
                }

                // CODE POSTAL
                if ('D' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Code Postal" est requis');
                    }

                    $cp = $v;
                }

                // VILLE
                if ('E' == $k) {
                    if ($cp) {
                        if (!$this->cp_ville($cp)) {
                            array_push($alertes, 'Le code postal <strong>'.$cp.'</strong> ne correspond à aucun code postal existant.');
                        } else {
                            if (empty($v)) {
                                array_push($alertes, 'Vous avez renseigné le champ "Code postal", le champ "Ville" est donc requis');
                            } else {
                                $ville = mb_strtoupper($v);
                                if (!$this->_cp_ville_check($cp, $ville)) {
                                    $alertes[] = 'Le couple <strong>'.$cp.'</strong> / <strong>'.$ville.'</strong> ne correspond à aucun lieu. Le Code postal <strong>'.$cp."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                                    $villes = implode(',', $this->cp_ville($cp));
                                    $alertes[] = $villes;
                                    $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                                }
                            }
                        }
                    }
                }

                if ('F' == $k) {
                    if (!in_array($v, \PsrLib\ORM\Entity\AmapLivraisonHoraire::JOURS, true)) {
                        array_push($alertes, 'Le jour de livraison doit être dans la liste : '.implode(',', \PsrLib\ORM\Entity\AmapLivraisonHoraire::JOURS));
                    }
                }

                if ('G' == $k) {
                    $heure = explode('-', $v);
                    $violationDeb = $this->validation->validate($heure[0] ?? '', new \PsrLib\Validator\Heure());
                    $violationFin = $this->validation->validate($heure[1] ?? '', new \PsrLib\Validator\Heure());
                    if (count($violationDeb) > 0 || count($violationFin) > 0) {
                        array_push($alertes, "L'heure de livraison été doit être sous la forme : HH:mm-HH:mm");
                    }
                }

                if ('H' == $k) {
                    $heure = explode('-', $v);
                    $violationDeb = $this->validation->validate($heure[0] ?? '', new \PsrLib\Validator\Heure());
                    $violationFin = $this->validation->validate($heure[1] ?? '', new \PsrLib\Validator\Heure());
                    if (count($violationDeb) > 0 || count($violationFin) > 0) {
                        array_push($alertes, "L'heure de livraison hiver doit être sous la forme : HH:mm-HH:mm");
                    }
                }

                // EMAIL
                if ('I' == $k) {
                    if (!empty($v)) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0] ?? '');
                        }

                        if (!$this->_email_validate($v)) {
                            array_push($alertes, "Le champ \"email contact public\" n'est pas valide : ".$v);
                        }

                        if (!$this->_amap_email_check($v)) {
                            array_push($alertes, "L'email contact public existe déjà dans la base de donnée : ".$v);
                        }
                    }
                }

                // PRODUITS PROPOSÉS
                if ('K' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : légumes" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : légumes" est mal formaté.');
                    }
                }
                if ('L' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : fruits" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : fruits" est mal formaté.');
                    }
                }
                if ('M' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : vache" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : vache" est mal formaté.');
                    }
                }
                if ('N' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : chèvre" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : chèvre" est mal formaté.');
                    }
                }
                if ('O' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : brebis" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : brebis" est mal formaté.');
                    }
                }
                if ('P' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : vin" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : vin" est mal formaté.');
                    }
                }
                if ('Q' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : œufs" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : œufs" est mal formaté.');
                    }
                }
                if ('R' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : pain" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : pain" est mal formaté.');
                    }
                }
                if ('S' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : pisciculture" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : pisciculture" est mal formaté.');
                    }
                }
                if ('T' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : produits de la mer" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : produits de la mer" est mal formaté.');
                    }
                }
                if ('U' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : viande porc" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : viande porc" est mal formaté.');
                    }
                }
                if ('V' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : viande volaille" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : viande volaille" est mal formaté.');
                    }
                }
                if ('W' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : viande agneau" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : viande agneau" est mal formaté.');
                    }
                }
                if ('X' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : viande bœuf" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : viande bœuf" est mal formaté.');
                    }
                }
                if ('Y' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : viande autre" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : viande autre" est mal formaté.');
                    }
                }
                if ('Z' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : miel" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : miel" est mal formaté.');
                    }
                }
                if ('AA' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits proposés : autre / divers" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits proposés : autre / divers" est mal formaté.');
                    }
                }

                // PRODUITS RECHERCHÉS
                if ('AB' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : légumes" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : légumes" est mal formaté.');
                    }
                }
                if ('AC' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : fruits" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : fruits" est mal formaté.');
                    }
                }
                if ('AD' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : vache" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : vache" est mal formaté.');
                    }
                }
                if ('AE' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : chèvre" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : chèvre" est mal formaté.');
                    }
                }
                if ('AF' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : brebis" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : brebis" est mal formaté.');
                    }
                }
                if ('AG' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : vin" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : vin" est mal formaté.');
                    }
                }
                if ('AH' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : œufs" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : œufs" est mal formaté.');
                    }
                }
                if ('AI' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : pain" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : pain" est mal formaté.');
                    }
                }
                if ('AJ' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : pisciculture" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : pisciculture" est mal formaté.');
                    }
                }
                if ('AK' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : produits de la mer" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : produits de la mer" est mal formaté.');
                    }
                }
                if ('AL' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande porc" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande porc" est mal formaté.');
                    }
                }
                if ('AM' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande volaille" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande volaille" est mal formaté.');
                    }
                }
                if ('AN' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande agneau" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande agneau" est mal formaté.');
                    }
                }
                if ('AO' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande bœuf" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : mer" est mal formaté.');
                    }
                }
                if ('AP' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande autre" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : viande autre" est mal formaté.');
                    }
                }
                if ('AQ' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : miel" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : miel" est mal formaté.');
                    }
                }
                if ('AR' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Produits recherchés : autre / divers" est requis');
                    } elseif ('o' != $v && 'O' != $v && 'n' != $v && 'N' != $v) {
                        array_push($alertes, 'Le champ "Produits recherchés : autre / divers" est mal formaté.');
                    }
                }

                // PRÉNOM RÉFÉRENT
                if ('AV' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "correspondant réseau AMAP : prénom" est requis');
                }

                // NOM RÉFÉRENT
                if ('AW' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "correspondant réseau AMAP : nom" est requis');
                }

                // EMAIL RÉFÉRENT
                if ('AY' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "correspondant réseau AMAP : email" est requis');
                    } else {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0] ?? '');
                        }

                        if (false === filter_var($v, FILTER_VALIDATE_EMAIL)) {
                            array_push($alertes, "L'email du correspondant réseau n'est pas valide : ".$v);
                        }
                    }
                }

                // CODE POSTAL RÉFÉRENT
                if ('BA' == $k && !empty($v)) {
                    $ref_cp = $v;
                }

                // VILLE RÉFÉRENT
                if ('BB' == $k) {
                    // Si le CP est renseigné...
                    if ($ref_cp) {
                        if (!$this->cp_ville($ref_cp)) {
                            array_push($alertes, 'Le code postal <strong>'.$ref_cp.'</strong> du correspondant réseau AMAP ne correspond à aucun code postal existant.');
                        } else {
                            if (empty($v)) {
                                array_push($alertes, 'Vous avez renseigné le champ "Code postal" du correspondant réseau AMAP, le champ "Ville" est donc requis');
                            } else {
                                $ref_ville = mb_strtoupper($v);
                                if (!$this->_cp_ville_check($ref_cp, $ref_ville)) {
                                    $alertes[] = 'Le couple <strong>'.$ref_cp.'</strong> et <strong>'.$ref_ville.'</strong> du correspondant réseau AMAP ne correspond à aucun lieu. Le Code postal <strong>'.$ref_cp."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                                    $villes = implode(',', $this->cp_ville($ref_cp));
                                    $alertes[] = $villes;
                                    $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                                }
                            }
                        }
                    }
                }

                // EMAIL RÉFÉRENT 2
                if ('BF' == $k) {
                    if (!empty($v)) {
                        if ('=' == $v[0]) { // =HYPERLINK
                            $v = str_replace('=HYPERLINK("mailto:', '', $v);
                            $v = explode('","', $v);
                            $v = trim($v[0] ?? '');
                        }

                        if (false === filter_var($v, FILTER_VALIDATE_EMAIL)) {
                            array_push($alertes, "L'email du second correspondant réseau AMAP n'est pas valide : ".$v);
                        }
                    }
                }

                // CODE POSTAL ADRESSE ADMINISTRATIVE
                if ('BH' == $k && !empty($v)) {
                    $admin_cp = $v;
                }

                // VILLE RÉFÉRENT ADRESSE ADMINISTRATIVE
                if ('BI' == $k) {
                    // Si le CP est renseigné...
                    if ($admin_cp) {
                        if (!$this->cp_ville($admin_cp)) {
                            array_push($alertes, 'Le code postal <strong>'.$admin_cp."</strong> de l\\'adresse administrative ne correspond à aucun code postal existant.");
                        } else {
                            if (empty($v)) {
                                array_push($alertes, "Vous avez renseigné le champ \"Code postal\" de l\\'adresse administrative, le champ \"Ville\" est donc requis");
                            } else {
                                $admin_ville = mb_strtoupper($v);
                                if (!$this->_cp_ville_check($admin_cp, $admin_ville)) {
                                    $alertes[] = 'Le couple <strong>'.$admin_cp.'</strong> et <strong>'.$admin_ville."</strong> de l\\'adresse administrative ne correspond à aucun lieu. Le Code postal <strong>".$admin_cp."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                                    $villes = implode(',', $this->cp_ville($admin_cp));
                                    $alertes[] = $villes;
                                    $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($alertes)) {
                $this->data['amap_erreur'] = $this->data['amap_erreur'].'<strong>Ligne '.$ligne.'</strong><br/> ';
                $i = 0;
                $retour_ligne = null;

                foreach ($alertes as $alerte) {
                    if (0 != $i) {
                        $retour_ligne = '<br/>';
                    }

                    $this->data['amap_erreur'] = $this->data['amap_erreur'].$retour_ligne.$alerte;
                    ++$i;
                }
                $this->data['amap_erreur'] = $this->data['amap_erreur'].'<br/><br/>';
            }
            ++$ligne;
        }

        // Geo coding des adresses d'amap
        if (!$this->data['amap_erreur']) {
            foreach ($this->data['valeurs'] as $index => $valeur) {
                $address = ($valeur['C'] ?? '').' '.($valeur['D'] ?? '').' '.($valeur['E'] ?? '');

                $suggestions = $this->nominatimClient->geocode($address);
                if (1 !== count($suggestions)) {
                    $this->data['amap_erreur'] .= 'Impossible de trouver les coordonnées GPS pour '.$address.'<br />';

                    continue;
                }

                $this->data['valeurs'][$index]['lat'] = $suggestions[0]->getLat();
                $this->data['valeurs'][$index]['long'] = $suggestions[0]->getLon();
            }
        }

        // ERREURS
        if ($this->data['amap_erreur']) {
            return $this->index();
        } // SAUVEGARDE

        $amaps = $this->importamapbuilder->buildAmapsFromImport($this->data['entete'], $this->data['valeurs']);
        foreach ($amaps as $amap) {
            $this->em->persist($amap);
            foreach ($amap->getAmapiens() as $amapien) {
                $this->em->persist($amapien);
            }
        }
        $this->em->flush();
        $this->data['amap_succes'] = 1;

        return $this->index();
    }

    #[Route('import/amapiens', name: 'import_amapiens')]
    public function amapiens(): Response
    {
        $config = [];
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT);

        $currentUser = $this->getUser();
        $form = $this->formFactory->create(\PsrLib\Form\ImportChooseAmapType::class, null, [
            'current_user' => $currentUser,
        ]);
        $form->handleRequest($this->request);

        $amap = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $amap = $form->get('amap')->getData();
        }

        if (null === $amap) {
            $this->data['amapiens_erreur'] = 'Vous n\'avez choisi aucune AMAP.';

            return $this->index();
        }

        $file = $this->getUploadedFile('amapiens');

        // FICHIER MAL CONFIGURÉ -----------------------------------------------
        if (null === $file) {
            $this->data['amapiens_erreur'] = 'Vous n\'avez pas sélectionné de fichier à envoyer.';

            return $this->index();
        } // CONFIG OK -----------------------------------------------------------

        try {
            $this->file_to_php($file->getFileInfo()->getPathname());
        } catch (\Exception $e) {
            $this->data['amap_erreur'] = 'Impossible de décoder le fichier';

            return $this->index();
        }

        // VÉRIFICATION DE L'ENTÊTE (À VÉRIFIER / COMPLÉTER) -----------------
        foreach ($this->data['entete'] as $entete) {
            // trim() : supprime les espaces (ou d'autres caractères) en début et fin de chaîne
            $prenom = trim((string) $entete['A']);
            $nom = trim((string) $entete['B']);
            $email = trim((string) $entete['C']);
            $tel = trim((string) $entete['D']);
            $adresse = trim((string) $entete['E']);
            $code_postal = trim((string) $entete['F']);
            $ville = trim((string) $entete['G']);
            $adherent_annee = trim((string) $entete['H']);
            $abonne_newsletter = trim((string) $entete['I']);

            if ('Prénom*' != $prenom || 'Nom*' != $nom || 'Email*' != $email || 'Téléphone (2 max.)' != $tel || 'Adresse' != $adresse || 'Code postal' != $code_postal || 'Ville' != $ville || "Années d'adhésion" != $adherent_annee || 'Abonné newsletter' != $abonne_newsletter) {
                $this->data['amapiens_erreur'] = 'Le fichier téléchargé n\'a pas d\'entête ou ce dernier est mal formaté. L\'entête doit être le même que celui du fichier exemple "amapiens.xls".<br/>';
            }
        }

        // VÉRIFICATION GÉNÉRALE ---------------------------------------------
        // Email en doublon dans le fichier ?
        $doublons = $this->_email_doublon_check('C');

        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['amapiens_erreur'] = $this->data['amapiens_erreur'].'L\'email <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        }

        // VÉRIFICATION DES CELLULES -----------------------------------------
        if ($this->data['amapiens_erreur']) {
            $this->data['amapiens_erreur'] = '<strong>FICHIER</strong><br/>'.$this->data['amapiens_erreur'].'<br/>';
        }

        $ligne = 2;
        foreach ($this->data['valeurs'] as $valeur) {
            $alertes = [];
            foreach ($valeur as $k => $v) {
                // PRÉNOM
                if ('A' == $k && empty($v)) {
                    array_push($alertes, 'le champ "Prénom" est requis');
                }
                // NOM
                if ('B' == $k && empty($v)) {
                    array_push($alertes, 'le champ "Nom" est requis');
                }
                // EMAIL
                if ('C' == $k) {
                    $v = (string) $v; // Force conversion to avoid error from excel formating
                    if ('=' == ($v[0] ?? '')) { // =HYPERLINK
                        $v = str_replace('=HYPERLINK("mailto:', '', $v);
                        $v = explode('","', $v);
                        $v = trim((string) $v[0]);
                    }

                    if (empty($v)) {
                        array_push($alertes, 'le champ "Email" est requis');
                    } elseif (!$this->_email_validate($v)) {
                        array_push($alertes, "le champ \"Email\" n'est pas valide : ".$v);
                    } elseif (!$this->_amapien_email_check($v, $amap)) {
                        array_push($alertes, "L'email <strong>".$v.'</strong> existe déjà dans la base de donnée');
                    }
                }

                // CODE POSTAL
                if ('F' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Code Postal" est requis');
                    }

                    $cp = $v;
                }

                // VILLE
                if ('G' == $k) {
                    if (isset($cp)) {
                        if (!$this->cp_ville($cp)) {
                            $alertes[] = 'Le code postal <strong>'.$cp.'</strong> ne correspond à aucun code postal existant.';
                        } else {
                            if (empty($v)) {
                                $alertes[] = 'Vous avez renseigné le champ "Code postal", le champ "Ville" est donc requis';
                            } else {
                                $ville = mb_strtoupper((string) $v);
                                if (!$this->_cp_ville_check($cp, $ville)) {
                                    $alertes[] = 'Le couple <strong>'.$cp.'</strong> / <strong>'.$ville.'</strong> ne correspond à aucun lieu. Le Code postal <strong>'.$cp."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                                    $villes = implode(',', $this->cp_ville($cp));
                                    $alertes[] = $villes;
                                    $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($alertes)) {
                $this->data['amapiens_erreur'] = $this->data['amapiens_erreur'].'<strong>Ligne '.$ligne.'</strong><br/>';
                $i = 0;
                $retour_ligne = null;
                foreach ($alertes as $alerte) {
                    if (0 != $i) {
                        $retour_ligne = '<br/>';
                    }

                    $this->data['amapiens_erreur'] = $this->data['amapiens_erreur'].$retour_ligne.$alerte;
                    ++$i;
                }
                $this->data['amapiens_erreur'] = $this->data['amapiens_erreur'].'<br/><br/>';
            }
            ++$ligne;
        }

        // ERREURS
        if ($this->data['amapiens_erreur']) {
            return $this->index();
        } // SAUVEGARDE

        $amapiens = $this->importamapienbuilder->buildAmapiensFromImport($this->data['valeurs'], $amap);
        foreach ($amapiens as $amapien) {
            $this->em->persist($amapien);
        }
        $this->em->flush();
        $this->data['amapiens_succes'] = 1;

        return $this->index();
    }

    #[Route('import/paysans', name: 'import_paysans')]
    public function paysans(): Response
    {
        $config = [];
        $this->denyAccessUnlessGranted(ImportVoter::ACTION_IMPORT);

        // CONFIGURATION DU FICHIER ----------------------------------------------
        $file = $this->getUploadedFile('paysans');

        // FICHIER MAL CONFIGURÉ
        if (null === $file) {
            $this->data['paysans_erreur'] = 'Vous n\'avez pas sélectionné de fichier à envoyer.';

            return $this->index();
        } // CONFIG OK

        try {
            $this->file_to_php($file->getFileInfo()->getPathname());
        } catch (\Exception $e) {
            $this->data['amap_erreur'] = 'Impossible de décoder le fichier';

            return $this->index();
        }

        // VÉRIFICATION DE L'ENTÊTE (À VÉRIFIER / COMPLÉTER) -------------------
        foreach ($this->data['entete'] as $entete) {
            // mb_strtolower() : Mise en minuscule
            $prenom = mb_strtolower(trim((string) $entete['A']));
            if ('nom*' != $prenom) {
                $this->data['paysans_erreur'] = 'Le fichier téléchargé n\'a pas d\'entête ou ce dernier est mal formaté. L\'entête doit être le même que celui du fichier exemple "paysans.xls".<br/>';
            }
        }

        // VÉRIFICATION GÉNÉRALE -----------------------------------------------
        // Email en doublon dans le fichier ?
        $doublons = $this->_email_doublon_check('C');
        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['paysans_erreur'] = $this->data['paysans_erreur'].'L\'email <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        }

        // SIRET en doublon dans le fichier ?
        $doublons = $this->_siret_doublon_check('O');
        if ($doublons) {
            foreach ($doublons as $d) {
                $this->data['paysans_erreur'] = $this->data['paysans_erreur'].'Le SIRET <strong>'.$d.'</strong> n\'est pas unique dans le fichier XLS.<br/>';
            }
        }

        // VÉRIFICATION DES CELLULES -------------------------------------------
        if ($this->data['paysans_erreur']) {
            $this->data['paysans_erreur'] = '<strong>FICHIER</strong><br/>'.$this->data['paysans_erreur'].'<br/>';
        }

        $ligne = 2;
        foreach ($this->data['valeurs'] as $valeur) {
            $alertes = [];
            $ferme_nom = null;
            $cp_pay = null;
            $ville_pay = null;
            $cp_ferme = null;
            $ville_ferme = null;

            foreach ($valeur as $k => $v) {
                $v = trim((string) $v);

                // NOM
                if ('A' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "Nom" est requis');
                }
                // PRÉNOM
                if ('B' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "Prénom" est requis');
                }
                // EMAIL
                if ('C' == $k) {
                    if ('=' == $v[0]) { // =HYPERLINK
                        $v = str_replace('=HYPERLINK("mailto:', '', $v);
                        $v = explode('","', $v);
                        $v = trim($v[0]);
                    }

                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Email" est requis');
                    } elseif (!$this->_email_validate($v)) {
                        array_push($alertes, "Le champ \"Email\" n'est pas valide : ".$v);
                    } elseif (!$this->_paysan_email_check($v)) {
                        array_push($alertes, "L'email existe déjà dans la base de donnée : ".$v);
                    }
                }
                // CODE POSTAL DOMICILE
                if ('H' == $k) {
                    $cp_pay = $v;
                }
                // VILLE DOMICILE
                if ('I' == $k) {
                    if ($cp_pay) {
                        if (!$this->cp_ville($cp_pay)) {
                            $alertes[] = 'Le code postal <strong>'.$cp_pay.'</strong> ne correspond à aucun code postal existant.';
                        } else {
                            if (empty($v)) {
                                $alertes[] = "Vous avez renseigné le champ \"Domicile\u{a0}: CP\", le champ \"Domicile\u{a0}: Ville\" est donc requis";
                            } else {
                                $ville_pay = mb_strtoupper((string) $v);
                                if (!$this->_cp_ville_check($cp_pay, $ville_pay)) {
                                    $alertes[] = 'Le couple <strong>'.$cp_pay.'</strong> / <strong>'.$ville_pay.'</strong> ne correspond à aucun lieu. Le Code postal <strong>'.$cp_pay."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                                    $villes = implode(',', $this->cp_ville($cp_pay));
                                    $alertes[] = $villes;
                                    $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                                }
                            }
                        }
                    }
                }

                if ('N' == $k) {
                    $ferme_nom = $v;
                }

                // ... le SIRET est obligatoire...
                if ('O' == $k) {
                    if (empty($v)) {
                        array_push($alertes, 'Le champ "Ferme : SIRET" est requis');
                    } elseif (preg_match('#[^0-9]+# ', $v)) {
                        array_push($alertes, 'Le numéro SIRET doit être constitué de chiffres');
                    } elseif (14 != strlen($v)) {
                        array_push($alertes, 'Le numéro SIRET doit être constitué de <strong>14 chiffres</strong>');
                    } elseif (!$this->_ferme_siret_exist_check($v)) {
                        array_push($alertes, 'Le numéro SIRET existe déjà dans la base de donnée : '.$v);
                    }
                }

                // ADRESSE de la FERME
                if ('P' == $k && empty($v)) {
                    array_push($alertes, 'Le champ "Adresse de la Ferme" est requis');
                }

                // ... Le CODE POSTAL de la FERME aussi...
                if ($ferme_nom && 'Q' == $k) {
                    settype($v, 'string');
                    $cp_ferme = $v;

                    if (empty($cp_ferme)) {
                        array_push($alertes, 'Le champ "Ferme : Nom" est renseigné, le champ "Ferme : CP" est donc requis');
                    } elseif (!$this->cp_ville($cp_ferme)) {
                        array_push($alertes, 'Le code postal <strong>'.$cp_ferme.'</strong> ne correspond à aucun code postal existant.');
                    }
                }

                // ... et la VILLE de la FERME aussi.
                if ($ferme_nom && 'R' == $k) {
                    $ville_ferme = mb_strtoupper((string) $v);
                    if (empty($ville_ferme)) {
                        $alertes[] = "Le champ \"Ferme : Nom\" est renseigné, le champ \"Ferme\u{a0}: Ville\" est donc requis";
                    } else {
                        if (!$this->_cp_ville_check($cp_ferme, $ville_ferme)) {
                            $alertes[] = 'Le couple <strong>'.$cp_ferme.'</strong> / <strong>'.$ville_ferme.'</strong> ne correspond à aucun lieu. Le Code postal <strong>'.$cp_ferme."</strong> est associé aux villes suivantes (respectez l'orthographe) : ";
                            $villes = $this->cp_ville($cp_ferme);
                            $alertes[] = implode(',', $villes);
                            $alertes[] = 'Si aucun couple cp/ville n\'est valide merci de passer par l\'ajout individuel.';
                        }
                    }
                }
            }

            if (!empty($alertes)) {
                $this->data['paysans_erreur'] = $this->data['paysans_erreur'].'<strong>Ligne '.$ligne.'</strong><br/> ';
                $i = 0;
                $retour_ligne = '';
                foreach ($alertes as $alerte) {
                    if (0 != $i) {
                        $retour_ligne = '<br/>';
                    }

                    $this->data['paysans_erreur'] = $this->data['paysans_erreur'].$retour_ligne.$alerte;
                    ++$i;
                }
                $this->data['paysans_erreur'] = $this->data['paysans_erreur'].'<br/><br/>';
            }
            ++$ligne;
        }

        // Geo coding des adresses de ferme
        if (!$this->data['paysans_erreur']) {
            foreach ($this->data['valeurs'] as $index => $valeur) {
                $address = $valeur['P'].' '.$valeur['Q'].' '.$valeur['R'];

                $suggestions = $this->nominatimClient->geocode($address);
                if (1 !== count($suggestions)) {
                    $this->data['paysans_erreur'] .= 'Impossible de trouver les coordonnées GPS pour '.$address.'<br />';

                    continue;
                }

                $this->data['valeurs'][$index]['lat'] = $suggestions[0]->getLat();
                $this->data['valeurs'][$index]['long'] = $suggestions[0]->getLon();
            }
        }

        // ERREURS
        if ($this->data['paysans_erreur']) {
            return $this->index();
        } // SAUVEGARDE

        $paysans = $this->importpaysanbuilder->buildPaysansFromImport($this->data['entete'], $this->data['valeurs']);
        foreach ($paysans as $paysan) {
            $this->em->persist($paysan);
            $this->em->persist($paysan->getUser());
            $this->em->persist($paysan->getFermes()->first());
        }
        $this->em->flush();
        $this->data['paysans_succes'] = 1;

        return $this->index();
    }

    private function _email_validate(?string $email): bool
    {
        $violations = $this->validation->validate(
            $email,
            [new NotBlank(), new Email(['mode' => 'strict'])]
        );

        return 0 === count($violations);
    }

    private function getUploadedFile(string $name): ?UploadedFile
    {
        return $this->request->files->get($name);
    }
}
