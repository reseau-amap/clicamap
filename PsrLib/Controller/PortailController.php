<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Assert\Assertion;
use PsrLib\DTO\Response\ZxcvbnResponse;
use PsrLib\Form\UserMdpOublieType;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\UserRepository;
use PsrLib\Services\ContactCPSelecteur;
use PsrLib\Services\Email_Sender;
use PsrLib\Services\Password\PasswordEncoder;
use PsrLib\Services\Security\Voters\UserVoter;
use PsrLib\Services\Zxcvbn;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PortailController extends AbstractController
{
    #[Route('/', name: 'index')]
    #[Route('/portail', name: 'portail_index')]
    public function index(): Response
    {
        $currentUser = $this->getUser();
        if (null !== $currentUser) {
            return $this->redirectToRoute('evenements');
        }

        return $this->redirectToRoute('portail_connexion');
    }

    /**
     * Se déconnecter du site.
     */
    #[Route('portail/deconnexion', name: 'portail_deconnexion')]
    public function deconnexion(): Response
    {
        $this->sfSession->invalidate();

        return $this->redirectToRoute('portail_index');
    }

    /**
     * Se connecter au site.
     */
    #[Route('portail/connexion', name: 'portail_connexion')]
    public function connexion(): Response
    {
        $form = $this->formFactory->create(\PsrLib\Form\UserLoginType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->sfSession->set('ID', $form->getData()->getUser()->getId());

            return $this->redirectToRoute('evenements');
        }

        return $this->render('portail/login.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('portail/contact', name: 'portail_contact')]
    public function contact(Email_Sender $emailSender, ContactCPSelecteur $contactCPSelecteur): Response
    {
        $loginForm = $this->formFactory->create(\PsrLib\Form\UserLoginType::class);
        $contactForm = $this->formFactory->create(\PsrLib\Form\PortailContactType::class);

        $contactForm->handleRequest($this->request);
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $cp = $contactForm->get('cp')->getData();
            $destMails = $contactCPSelecteur->choisirContactDepuisCP($cp);

            // Envoi de l'email
            $emailSender->envoyerMailContact(
                $contactForm->get('nom')->getData(),
                $contactForm->get('prenom')->getData(),
                $contactForm->get('email')->getData(),
                $destMails,
                $cp,
                $contactForm->get('contenu')->getData()
            );

            // Envoi de l'accusé de reception
            if (false !== $contactForm->get('confirmation')->getData()) {
                $emailSender->envoyerEnvoiConfirmationMailContact(
                    $contactForm->get('nom')->getData(),
                    $contactForm->get('prenom')->getData(),
                    $contactForm->get('email')->getData(),
                    $cp,
                    $contactForm->get('contenu')->getData()
                );
            }

            // Message de confirmation pour l'utilisateur
            $this->addFlash(
                'flash_contact_confirmation',
                'Votre message est bien envoyé. Merci et bonne journée'
            );

            return $this->redirectToRoute('portail_connexion');
        }

        return $this->render('portail/contact.html.twig', [
            'form' => $loginForm->createView(),
            'contactForm' => $contactForm->createView(),
        ]);
    }

    #[Route('portail/mdp_oublie', name: 'portail_mdp_oublie')]
    public function mdp_oublie(Email_Sender $emailSender): Response
    {
        $loginForm = $this->formFactory->create(\PsrLib\Form\UserLoginType::class);
        $mdpForm = $this->formFactory->create(UserMdpOublieType::class);

        $mdpForm->handleRequest($this->request);
        if ($mdpForm->isSubmitted() && $mdpForm->isValid()) {
            $user = $mdpForm->get('user')->getData();
            Assertion::isInstanceOf($user, User::class);

            $token = new \PsrLib\ORM\Entity\Token();
            $user->setPasswordResetToken($token);
            $this->em->flush();

            // Envoyer l'email avec le lien de réintialisation
            $emailSender->envoyerMdpReinit($user);

            // Message de confirmation pour l'utilisateur
            $this->addFlash(
                'flash_mdpreinit_mail_envoye',
                ' Un email contenant un lien vous permettant de générer un nouveau mot de passe vient de vous être envoyé, veuillez consulter votre boite mail'
            );

            return $this->redirectToRoute('portail_connexion');
        }

        return $this->render('portail/mdp_oublie.html.twig', [
            'form' => $loginForm->createView(),
            'mdpForm' => $mdpForm->createView(),
        ]);
    }

    #[Route('portail/mdp_oublie_confirmation/{token}', name: 'portail_mdp_oublie_confirmation')]
    public function mdp_oublie_confirmation(string $token, UserRepository $userRepository, PasswordEncoder $passwordEncoder): Response
    {
        /** @var \PsrLib\ORM\Entity\BaseUser|null $user */
        $user = $userRepository->getUserFromTokenMdpReinit($token);

        // Test si le lien est encore valide
        if (null === $user) {
            $this->addFlash(
                'flash_mdpreinit_confirmatio',
                'Le lien que vous avez suivi est invalide ou expiré. Merci de faire une nouvelle demande de reintialisation de mot de passe'
            );

            return $this->redirectToRoute('portail_connexion');
        }

        $form = $this->formFactory->create(\PsrLib\Form\UserPasswordType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Re initialise le mot de passe et supprime le token
            $user->setPassword($passwordEncoder->encodePassword($form->get('password')->getData()));
            $user->setPasswordResetToken(null);
            $this->em->flush();

            $this->addFlash(
                'flash_mdpreinit_confirmatio',
                'Le mot de passe a été correctement mis à jour'
            );

            return $this->redirectToRoute('portail_connexion');
        }

        return $this->render('portail/mdp_oublie_form.html.twig', [
            'form' => $this->formFactory->create(\PsrLib\Form\UserLoginType::class)->createView(),
            'mdpForm' => $form->createView(),
            'token' => $token,
        ]);
    }

    /**
     * Test la force du mot du mot de passe donné.
     */
    #[Route('portail/mdp_oublie_test_password/{tokenMdpOuli}', name: 'portail_mdp_oublie_test_password')]
    public function mdp_oublie_test_password(string $tokenMdpOuli, Request $request, UserRepository $userRepository, Zxcvbn $zxcvbn): Response
    {
        /** @var \PsrLib\ORM\Entity\BaseUser $user */
        $user = $userRepository->getUserFromTokenMdpReinit($tokenMdpOuli);
        $this->denyAccessUnlessGranted(UserVoter::ACTION_USER_ESTIMATE_PASSWORD_STRENGTH, $user);

        $strength = $zxcvbn->estimateScore($request->request->get('zxcvbn_password'));

        return new ZxcvbnResponse($strength);
    }
}
