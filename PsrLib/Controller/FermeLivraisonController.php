<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use Carbon\Carbon;
use PsrLib\Services\BinaryFileResponseFactory;
use PsrLib\Services\EntityBuilder\ContratLivraisonFactory;
use PsrLib\Services\Exporters\ExcelGeneratorExportFermeLivraison;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FermeLivraisonController extends AbstractController
{
    #[Route('ferme_livraison/accueil/{ferme_id}', name: 'ferme_livraison_accueil')]
    public function accueil(int $ferme_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);

        // Pre check to avoid rendering error for non authenticated user
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        if (!$this->authorizationChecker->isGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme)) {
            return $this->render('ferme_livraison/error.html.twig');
        }

        $this->redirectFromStoredGetParamIfExist('Ferme_livraisonController');

        $searchState = new \PsrLib\DTO\SearchFermeLivraisonListState($ferme);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeLivraisonListType::class, $searchState);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        if (!$searchForm->isValid()) {
            return $this->render('ferme_livraison/accueil.html.twig', [
                'searchForm' => $searchForm->createView(),
                'dates' => [],
                'ferme' => $ferme,
            ]);
        }
        $dates = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratDate::class)
            ->searchDate($searchState)
        ;

        return $this->render('ferme_livraison/accueil.html.twig', [
            'searchForm' => $searchForm->createView(),
            'dates' => $dates,
            'ferme' => $ferme,
        ]);
    }

    #[Route('ferme_livraison/date/{ferme_id}/{date}', name: 'ferme_livraison_date')]
    public function date(int $ferme_id, string $date): Response
    {
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        $date = $this->parseDateRaw($date);
        if (null === $date) {
            $this->exit_error();
        }
        $searchState = new \PsrLib\DTO\SearchFermeLivraisonAmapState($ferme, $date);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeLivraisonAmapType::class, $searchState);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $mcs = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->searchLivraison($searchState)
        ;

        $mcProRepo = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratProduit::class)
        ;
        $livs = [];
        foreach ($mcs as $mc) {
            $livs[$mc->getId()] = $mcProRepo->totalByProductFromMcDateContractValidated(
                $mc,
                $date
            );
        }

        return $this->render('ferme_livraison/date.html.twig', [
            'ferme' => $ferme,
            'date' => $date,
            'searchForm' => $searchForm->createView(),
            'mcs' => $mcs,
            'livs' => $livs,
            'amap' => $searchState->getAmap(),
        ]);
    }

    #[Route('ferme_livraison/recherche_amapien/{ferme_id}/{amap_id}/{dateRaw}', name: 'ferme_livraison_recherche_amapien')]
    public function recherche_amapien(int $ferme_id, int $amap_id, string $dateRaw): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $date = $this->parseDateRaw($dateRaw);
        if (null === $date) {
            $this->exit_error();
        }

        $state = new \PsrLib\DTO\SearchFermeLivraisonAmapienState($ferme, $amap, $date);
        $form = $this->formFactory->create(\PsrLib\Form\SearchFermeLivraisonAmapienType::class, $state);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this
                ->redirectToRoute(
                    'ferme_livraison_detail_amapien',
                    [
                        'ferme_id' => $ferme->getId(),
                        'amapien_id' => $state->getAmapien()->getId(),
                        'dateRaw' => $date->format('Y-m-d'),
                    ]
                )
            ;
        }

        return $this->render('ferme_livraison/recherche_amapien.html.twig', [
            'ferme' => $ferme,
            'date' => $date,
            'amap' => $amap,
            'form' => $form->createView(),
        ]);
    }

    #[Route('ferme_livraison/detail_amapien/{ferme_id}/{amapien_id}/{dateRaw}', name: 'ferme_livraison_detail_amapien')]
    public function detail_amapien(int $ferme_id, int $amapien_id, string $dateRaw, ContratLivraisonFactory $contratLivraisonFactory): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        /** @var \PsrLib\ORM\Entity\Amapien $amapien */
        $amapien = $this->findOrExit(\PsrLib\ORM\Entity\Amapien::class, $amapien_id);
        $date = $this->parseDateRaw($dateRaw);
        if (null === $date) {
            $this->exit_error();
        }

        /** @var \PsrLib\ORM\Entity\Contrat[] $contrats */
        $contrats = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Contrat::class)
            ->getActiveWithFermeDateAmapien($ferme, $amapien, $date)
        ;
        if (0 === count($contrats)) { // Prevent invalid amapien or date injection
            $this->exit_error();
        }

        $livraison = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ContratLivraison::class)
            ->findOneBy([
                'amapien' => $amapien,
                'ferme' => $ferme,
                'date' => $date,
            ])
        ;
        if (null === $livraison) {
            $livraison = $contratLivraisonFactory->create($amapien, $ferme, $date, $contrats);
        }

        $form = $this->formFactory->create(\PsrLib\Form\ContratLivraisonType::class, $livraison);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($livraison);
            $this->em->flush();

            $this->addFlash('success', 'Sauvegarde effectuée');

            return $this->
                redirectToRoute(
                    'ferme_livraison_detail_amapien',
                    [
                        'ferme_id' => $ferme->getId(),
                        'amapien_id' => $amapien->getId(),
                        'dateRaw' => $date->format('Y-m-d'),
                    ]
                );
        }

        return $this->render('ferme_livraison/detail_amapien.html.twig', [
            'livraison' => $livraison,
            'contrats' => $contrats,
            'ferme' => $ferme,
            'date' => $date,
            'amapien' => $amapien,
            'form' => $form->createView(),
        ]);
    }

    #[Route('ferme_livraison/livraisons_restantes/{ferme_id}/{amap_id}/{dateRaw}', name: 'ferme_livraison_livraisons_restantes')]
    public function livraisons_restantes(int $ferme_id, int $amap_id, string $dateRaw): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $date = $this->parseDateRaw($dateRaw);
        if (null === $date) {
            $this->exit_error();
        }

        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getActiveAmapienWithContractValidatedDate($amap, $ferme, $date)
        ;
        $livraisons = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ContratLivraison::class)
            ->findBy([
                'ferme' => $ferme,
                'date' => $date,
                'livre' => true,
            ])
        ;

        $livraisonsProduits = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContratProduit::class)
            ->totalProductNotDeliveredFromFermeAmapDate($ferme, $amap, $date)
        ;

        return $this->render('ferme_livraison/livraisons_restantes.html.twig', [
            'ferme' => $ferme,
            'amap' => $amap,
            'date' => $date,
            'amapiens' => $amapiens,
            'livraisons' => $livraisons,
            'livraisonsProduits' => $livraisonsProduits,
        ]);
    }

    #[Route('ferme_livraison/livraisons_restantes_amapiens/{ferme_id}/{amap_id}/{dateRaw}', name: 'ferme_livraison_livraisons_restantes_amapiens')]
    public function livraisons_restantes_amapiens(int $ferme_id, int $amap_id, string $dateRaw): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);

        $date = $this->parseDateRaw($dateRaw);
        if (null === $date) {
            $this->exit_error();
        }

        $amapiens = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\Amapien::class)
            ->getActiveAmapienWithContractDateNotDelivered($amap, $ferme, $date)
        ;

        return $this->render('ferme_livraison/livraisons_restantes_amapiens.html.twig', [
            'ferme' => $ferme,
            'amap' => $amap,
            'date' => $date,
            'amapiens' => $amapiens,
        ]);
    }

    #[Route('ferme_livraison/export/{ferme_id}/{amap_id}/{dateRaw}', name: 'ferme_livraison_export')]
    public function export(int $ferme_id, int $amap_id, string $dateRaw, ExcelGeneratorExportFermeLivraison $excelGeneratorExportFermeLivraison): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);

        /** @var \PsrLib\ORM\Entity\Ferme $ferme */
        $ferme = $this->findOrExit(\PsrLib\ORM\Entity\Ferme::class, $ferme_id);
        $this->denyAccessUnlessGranted(FermeVoter::ACTION_FERME_LIVRAISON_LIST, $ferme);

        $date = $this->parseDateRaw($dateRaw);
        if (null === $date) {
            $this->exit_error();
        }

        $searchState = new \PsrLib\DTO\SearchFermeLivraisonAmapState($ferme, $date);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchFermeLivraisonAmapType::class, $searchState);
        $searchForm->submit(['amap' => $amap_id]);

        $mcs = $this
            ->em
            ->getRepository(\PsrLib\ORM\Entity\ModeleContrat::class)
            ->searchLivraison($searchState)
        ;

        $outFileName = $excelGeneratorExportFermeLivraison->genererFermesLivraisons($mcs, $date);

        return BinaryFileResponseFactory::createFromTempFilePath($outFileName, sprintf(
            'feuille-livraison-contrat-%s-amapiens-produit.xlsx',
            Carbon::now()->format('d_m_Y')
        ));
    }

    private function parseDateRaw(string $dateRaw): ?Carbon
    {
        $date = Carbon::createFromFormat('Y-m-d', $dateRaw);
        if (false === $date) {
            return null;
        }

        return $date->startOfDay();
    }
}
