<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use DI\Attribute\Inject;
use PsrLib\DTO\CampagneWizardDTO;
use PsrLib\DTO\Response\XlsMemoryResponse;
use PsrLib\Exception\CampagneWizardDtoBuilderNoAdminException;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\CampagneBulletinRecus;
use PsrLib\Services\BinaryFileResponseFactory;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Routing\Annotation\Route;

class CampagneController extends AbstractController
{
    protected const HOME_PATH_ID = 'home_campagne';
    protected const HOME_BULLETIN_PATH_ID = 'home_bulletin';

    #[Inject]
    public \PsrLib\ORM\Repository\CampagneRepository $campagneRepository;

    #[Inject]
    public \PsrLib\ORM\Repository\CampagneBulletinRepository $campagneBulletinRepository;

    #[Inject]
    public \PsrLib\Services\MPdfGeneration $mpdfgeneration;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\CampagneWizardDtoBuilder $campagneWizardDtoBuilder;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\CampagneBulletinBuilder $campagneBulletinBuilder;

    #[Inject]
    public \PsrLib\Services\EntityBuilder\CampagneBulletinRecusBuilder $campagneBulletinRecusBuilder;

    #[Inject]
    public LockFactory $lockFactory;

    #[Inject]
    public \PsrLib\Services\ZipCreator $zipCreator;

    #[Inject]
    protected \PsrLib\Services\Email_Sender $email_Sender;

    #[Inject]
    protected \PsrLib\Services\StringUtils $stringUtils;

    #[Inject]
    protected \Symfony\Component\Validator\Validator\ValidatorInterface $validator;

    #[Route('campagne', name: 'campagne_index')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneListVoter::ACTION_CAMPAGNE_LIST);
        $this->redirectFromStoredGetParamIfExist('campagne');

        $this->session_store_home_path(self::HOME_PATH_ID);
        $searchForm = null;
        $userAdminAmaps = $this->getUser()->getAdminAmaps()->toArray();
        $searchFormData = new \PsrLib\DTO\SearchCampagneState();
        if (count($userAdminAmaps) > 1) {
            $searchForm = $this->formFactory->create(\PsrLib\Form\SearchCampagneType::class, null, [
                'amap_choices' => $this->getUser()->getAdminAmaps()->toArray(),
            ]);
            // Force submit when nothing selected
            $searchForm->submit($this->request->query->get($searchForm->getName()));

            $searchFormData = $searchForm->getData();
        } elseif (1 === count($userAdminAmaps)) {
            $searchFormData->setAmap($userAdminAmaps[0]);
        }

        $campagnes = $this->campagneRepository->search($searchFormData);

        $amap = $searchFormData->getAmap();

        return $this->render('campagne/index.html.twig', [
            'bulletinListUrl' => null === $amap ? '' : $this->generateUrl('campagne_bulletin_liste', ['amap_id' => $amap->getId()]),
            'searchForm' => $searchForm?->createView(),
            'campagnes' => $campagnes,
            'amap' => $amap,
        ]);
    }

    #[Route('campagne/index_admin', name: 'campagne_index_admin')]
    public function index_admin(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneListAdminVoter::ACTION_CAMPAGNE_LIST_ADMIN);
        $this->redirectFromStoredGetParamIfExist('campagne_admin');

        $this->session_store_home_path(self::HOME_PATH_ID);

        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchCampagneType::class, null, [
            'amap_choices' => $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findAllAmapDeFaitForAdmin($this->getUser()),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $searchFormData = $searchForm->getData();

        $campagnes = $this->campagneRepository->search($searchFormData);

        $amap = $searchFormData->getAmap();

        return $this->render('campagne/index.html.twig', [
            'bulletinListUrl' => null === $amap ? '' : $this->generateUrl('campagne_bulletin_admin_liste', ['amap_id' => $amap->getId()]),
            'searchForm' => $searchForm->createView(),
            'campagnes' => $campagnes,
            'amap' => $amap,
        ]);
    }

    #[Route('campagne/amapien_liste', name: 'campagne_amapien_liste')]
    public function amapien_liste(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneListAmapienVoter::ACTION_CAMPAGNE_LIST_AMAPIEN);

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($this->getUser());

        return $this->render('campagne/index_amapien.html.twig', [
            'campagnes' => $campagnes,
        ]);
    }

    #[Route('campagne/amapien_souscription/{campagne_id}', name: 'campagne_amapien_souscription')]
    public function amapien_souscription(int $campagne_id): Response
    {
        $campagne = $this->findOrExit(\PsrLib\ORM\Entity\Campagne::class, $campagne_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneSubscriptionVoter::ACTION_CAMPAGNE_SUBSCRIBE, $campagne);
        $bulletin = $this->campagneBulletinBuilder->createFromCampagne($campagne, $this->getUser());

        $this->session_store_campagne_subscription_wizard($bulletin);

        return $this->redirectToRoute('campagne_amapien_souscription_1');
    }

    #[Route('campagne/amapien_souscription_1', name: 'campagne_amapien_souscription_1')]
    public function amapien_souscription_1(): Response
    {
        $bulletin = $this->session_get_campagne_subscription_wizard();
        if (false === $bulletin) {
            return $this->redirectToRoute('campagne_amapien_liste');
        }
        $form = $this->formFactory->create(\PsrLib\Form\CampagneBulletinForm1Type::class, $bulletin);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush(); // Save user data
            $this->session_store_campagne_subscription_wizard($bulletin);

            return $this->redirectToRoute('campagne_amapien_souscription_2');
        }

        return $this->render('campagne/subscription_form_1.html.twig', [
            'campagne' => $bulletin->getCampagne(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('campagne/amapien_souscription_2', name: 'campagne_amapien_souscription_2')]
    public function amapien_souscription_2(): Response
    {
        $bulletin = $this->session_get_campagne_subscription_wizard();
        if (false === $bulletin) {
            return $this->redirectToRoute('campagne_amapien_liste');
        }
        $form = $this->formFactory->create(\PsrLib\Form\CampagneBulletinForm2Type::class, $bulletin);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush(); // Save user data

            $this->session_store_campagne_subscription_wizard($bulletin);

            return $this->redirectToRoute('campagne_amapien_souscription_3');
        }

        return $this->render('campagne/subscription_form_2.html.twig', [
            'campagne' => $bulletin->getCampagne(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('campagne/amapien_souscription_3', name: 'campagne_amapien_souscription_3')]
    public function amapien_souscription_3(): Response
    {
        $bulletin = $this->session_get_campagne_subscription_wizard();
        if (false === $bulletin) {
            return $this->redirectToRoute('campagne_amapien_liste');
        }

        if ($this->request->isMethod('POST')) {
            $fileName = $this->mpdfgeneration->genererCampagneBulletinFile($bulletin);
            $pdf = new \PsrLib\ORM\Entity\Files\CampagneBulletin($fileName);
            $bulletin->setPdf($pdf);
            $this->em->persist($bulletin);

            $this->em->flush();

            $this->addFlash('success', 'La campagne a été souscrite.');

            $this->session_clear_campagne_subscription_wizard();

            return $this->redirectToRoute('user_profil_documents');
        }

        return $this->render('campagne/subscription_form_3.html.twig', [
            'campagne' => $bulletin->getCampagne(),
        ]);
    }

    #[Route('campagne/preview_bulletin_pdf', name: 'campagne_preview_bulletin_pdf')]
    public function preview_bulletin_pdf(): Response
    {
        $bulletin = $this->session_get_campagne_subscription_wizard();
        if (false === $bulletin) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $file = $this->mpdfgeneration->genererCampagneBulletinTmp($bulletin);

        return BinaryFileResponseFactory::createFromTempFilePath($file, 'bulletin.pdf', ResponseHeaderBag::DISPOSITION_INLINE);
    }

    #[Route('campagne/download_bulletin_pdf/{bulletin_id}', name: 'campagne_download_bulletin_pdf')]
    public function download_bulletin_pdf(int $bulletin_id): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        $bulletin = $this->findOrExit(\PsrLib\ORM\Entity\CampagneBulletin::class, $bulletin_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinDownloadVoter::ACTION_CAMPAGNE_BULLETIN_DOWNLOAD, $bulletin);

        $response = new BinaryFileResponse($bulletin->getPdf()->getFileFullPath());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->bulletinFileName($bulletin)
        );

        return $response;
    }

    #[Route('campagne/bulletin_liste/{amap_id}', name: 'campagne_bulletin_liste')]
    public function bulletin_liste(int $amap_id): Response
    {
        $this->session_store_home_path(self::HOME_BULLETIN_PATH_ID);
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneNewVoter::ACTION_CAMPAGNE_NEW, $amap);
        $this->redirectFromStoredGetParamIfExist('campagne_amap');

        return $this->bulletin_liste_internal($amap, $this->getUser()->getAdminAmaps()->toArray());
    }

    #[Route('campagne/bulletin_admin_liste/{amap_id}', name: 'campagne_bulletin_admin_liste')]
    public function bulletin_admin_liste(int $amap_id): Response
    {
        $this->session_store_home_path(self::HOME_BULLETIN_PATH_ID);
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneNewVoter::ACTION_CAMPAGNE_NEW, $amap);
        $this->redirectFromStoredGetParamIfExist('campagne_amap');

        return $this->bulletin_liste_internal($amap, $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findAllAmapDeFaitForAdmin($this->getUser()));
    }

    /**
     * @param Amap[] $amaps
     */
    private function bulletin_liste_internal(Amap $amap, $amaps): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        $searchFormData = new \PsrLib\DTO\SearchCampagneBulletinState($amap);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchCampagneBulletinType::class, $searchFormData, [
            'annee_choices' => $this->campagneRepository->findAnneeByAmap($amap),
            'amap_choices' => $amaps,
        ]);

        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $bulletins = $this->campagneBulletinRepository->search($searchFormData);

        return $this->render('campagne/bulletin_liste.html.twig', [
            'searchForm' => $searchForm->createView(),
            'bulletins' => $bulletins,
            'state' => $searchFormData,
        ]);
    }

    #[Route('campagne/bulletin_action/{amap_id}', name: 'campagne_bulletin_action', methods: ['POST'])]
    public function bulletin_action(int $amap_id): Response
    {
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionHeaderVoter::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION_HEADER, $amap);

        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        if ($this->request->request->has('action-generate')) {
            return $this->bulletin_action_generate($amap);
        }

        if ($this->request->request->has('action-delete')) {
            return $this->bulletin_action_delete();
        }

        if ($this->request->request->has('action-download-bulletin')) {
            return $this->bulletin_action_download_bulletin();
        }

        if ($this->request->request->has('action-download-recu')) {
            return $this->bulletin_action_download_recu();
        }

        if ($this->request->request->has('action-notification')) {
            return $this->bulletin_action_notification();
        }

        throw new \RuntimeException('Invalid action');
    }

    private function bulletin_action_generate(Amap $amap): Response
    {
        $bulletins = $this->bulletin_action_get_selected_not_generated();
        if (0 === count($bulletins)) {
            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        }
        // Check all bulletin from amap to avoid locking issue
        \Assert\Assertion::eq(
            0,
            count(array_filter($bulletins, fn (\PsrLib\ORM\Entity\CampagneBulletin $bulletin) => $bulletin->getCampagne()->getAmap() !== $amap)),
        );

        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_GENERATE_ACTION, $bulletins);

        // As recus counter increment is not atomic (eg: done on database side), we need to lock
        // To ensure that only one generation is done at a time for a given AMAP
        $lock = $this->lockFactory->createLock('bulletin_action_generate_'.$amap->getId(), 180, false);
        if (!$lock->acquire()) {
            $this->addFlash('notice_error', 'Une génération est déjà en cours pour cette AMAP. Merci de réesayer dans quelques minutes.');

            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        }
        foreach ($bulletins as $bulletin) {
            $bulletin->setRecus($this->campagneBulletinRecusBuilder->create($bulletin));

            $amapien = $bulletin->getAmapien();
            $campagneAnneeAdhesion = $bulletin->getCampagne()->getAnneeAdhesionAmapien();
            if ($amapien->getAnneeAdhesions()->filter(fn (\PsrLib\ORM\Entity\AmapienAnneeAdhesion $aa) => $aa->getAnnee() === $campagneAnneeAdhesion)->isEmpty()) {
                $amapienAnneeAdhesion = new \PsrLib\ORM\Entity\AmapienAnneeAdhesion($campagneAnneeAdhesion, $amapien);
                $this->em->persist($amapienAnneeAdhesion);
            }
            $this->em->flush(); // Flush in loop for proper recus counter increment
        }

        $this->em->flush();

        $this->addFlash('success', 'Les reçus ont été générés.');

        $lock->release();

        return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
    }

    private function bulletin_action_delete(): Response
    {
        $exit = function (): Response {
            $this->addFlash('success', 'Les reçus ont été supprimés.');

            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        };
        $bulletins = $this->bulletin_action_get_selected_generated();
        if (0 === count($bulletins)) {
            return $exit();
        }

        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION, $bulletins);

        foreach ($bulletins as $bulletin) {
            $this->remove_recu($bulletin->getRecus());
        }
        $this->em->flush();

        return $exit();
    }

    private function bulletin_action_download_bulletin(): Response
    {
        $bulletins = $this->bulletin_action_get_selected();
        if (0 === count($bulletins)) {
            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        }
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_ACTION, $bulletins);

        $files = array_map(
            fn (\PsrLib\ORM\Entity\CampagneBulletin $bulletin) => new \PsrLib\DTO\ZipCreatorFileDTO($bulletin->getPdf(), $this->bulletinFileName($bulletin)),
            $bulletins
        );
        $fileName = $this->zipCreator->createZipFiles($files);

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'bulletins.zip');
    }

    private function bulletin_action_download_recu(): Response
    {
        $bulletins = $this->bulletin_action_get_selected_generated();
        if (0 === count($bulletins)) {
            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        }
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION, $bulletins);

        $files = array_map(
            fn (\PsrLib\ORM\Entity\CampagneBulletin $bulletin) => new \PsrLib\DTO\ZipCreatorFileDTO($bulletin->getRecus()->getPdf(), $this->recuFileName($bulletin->getRecus())),
            $bulletins
        );
        $fileName = $this->zipCreator->createZipFiles($files);

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'recus.zip');
    }

    private function bulletin_action_notification(): Response
    {
        $bulletins = $this->bulletin_action_get_selected();
        if (0 === count($bulletins)) {
            return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
        }
        $this->denyAccessUnlessGrantedMultiple(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_ACTION, $bulletins);

        foreach ($bulletins as $bulletin) {
            $this->email_Sender->envoyerUserDocumentNotification($bulletin->getAmapien()->getUser());
        }

        $this->addFlash('success', 'Les notifications ont été envoyées.');

        return $this->redirect(site_url($this->session_get_home_path(self::HOME_BULLETIN_PATH_ID)));
    }

    #[Route('campagne/bulletin_recus_supprimer/{bulletin_id}', name: 'campagne_bulletin_recus_supprimer', methods: ['POST'])]
    public function bulletin_recus_supprimer(int $bulletin_id): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);

        $bulletin = $this->findOrExit(\PsrLib\ORM\Entity\CampagneBulletin::class, $bulletin_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_GENERE_ACTION, $bulletin);

        $this->remove_recu($bulletin->getRecus());
        $this->em->flush();

        $this->addFlash('success', 'Le reçu a été supprimé.');

        return $this->redirectToRoute('campagne_bulletin_liste', ['amap_id' => $bulletin->getCampagne()->getAmap()->getId()]);
    }

    #[Route('campagne/bulletin_notification/{bulletin_id}', name: 'campagne_bulletin_notification', methods: ['POST'])]
    public function bulletin_notification(int $bulletin_id): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);

        $bulletin = $this->findOrExit(\PsrLib\ORM\Entity\CampagneBulletin::class, $bulletin_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinRecuActionVoter::ACTION_CAMPAGNE_BULLETIN_RECU_ACTION, $bulletin);

        $this->email_Sender->envoyerUserDocumentNotification($bulletin->getAmapien()->getUser());

        $this->addFlash('success', 'La notification a été envoyée.');

        return $this->redirectToRoute('campagne_bulletin_liste', ['amap_id' => $bulletin->getCampagne()->getAmap()->getId()]);
    }

    /**
     * @return CampagneBulletin[]
     */
    private function bulletin_action_get_selected_generated(): array
    {
        $bulletins = $this->bulletin_action_get_selected();

        return array_filter($bulletins, fn (\PsrLib\ORM\Entity\CampagneBulletin $bulletin) => null !== $bulletin->getRecus());
    }

    /**
     * @return CampagneBulletin[]
     */
    private function bulletin_action_get_selected_not_generated(): array
    {
        $bulletins = $this->bulletin_action_get_selected();

        return array_filter($bulletins, fn (\PsrLib\ORM\Entity\CampagneBulletin $bulletin) => null === $bulletin->getRecus());
    }

    /**
     * @return \PsrLib\ORM\Entity\CampagneBulletin[]
     */
    private function bulletin_action_get_selected()
    {
        /** @var int[] $selectedIds */
        $selectedIds = $this->request->request->get('selected');

        return array_map(fn ($id) => $this->findOrExit(\PsrLib\ORM\Entity\CampagneBulletin::class, $id), array_keys($selectedIds));
    }

    #[Route('campagne/download_bulletin_recu_pdf/{recu_id}', name: 'campagne_download_bulletin_recu_pdf')]
    public function download_bulletin_recu_pdf(int $recu_id): Response
    {
        $this->disableSoftDeleteFor(\PsrLib\ORM\Entity\Amapien::class);
        $recu = $this->findOrExit(\PsrLib\ORM\Entity\CampagneBulletinRecus::class, $recu_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinRecuDownloadVoter::ACTION_CAMPAGNE_BULLETIN_RECU_DOWNLOAD, $recu);

        $response = new BinaryFileResponse($recu->getPdf()->getFileFullPath());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->recuFileName($recu)
        );

        return $response;
    }

    #[Route('campagne/creation/{amap_id}', name: 'campagne_creation')]
    public function creation(int $amap_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Amap $amap */
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneNewVoter::ACTION_CAMPAGNE_NEW, $amap);

        try {
            $wizard = $this->campagneWizardDtoBuilder->createNew($amap);
        } catch (CampagneWizardDtoBuilderNoAdminException $e) {
            $this->addFlash('danger', 'L\'amap est une amap de fait et aucun admin réseau n\'a été trouvé pour cette AMAP.');

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $this->session_store_campagne_wizard($wizard);

        return $this->redirectToRoute('campagne_form_1');
    }

    #[Route('campagne/edition/{campagne_id}', name: 'campagne_edition')]
    public function edition(int $campagne_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Campagne $campagne */
        $campagne = $this->findOrExit(\PsrLib\ORM\Entity\Campagne::class, $campagne_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneEditVoter::ACTION_CAMPAGNE_EDIT, $campagne);

        try {
            $wizard = $this->campagneWizardDtoBuilder->createEdit($campagne);
        } catch (CampagneWizardDtoBuilderNoAdminException $e) {
            $this->addFlash('danger', 'L\'amap est une amap de fait et aucun admin réseau n\'a été trouvé pour cette AMAP.');

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $this->session_store_campagne_wizard($wizard);

        return $this->redirectToRoute('campagne_form_1');
    }

    #[Route('campagne/telecharger/{campagne_id}', name: 'campagne_telecharger')]
    public function telecharger(int $campagne_id): Response
    {
        /** @var \PsrLib\ORM\Entity\Campagne $campagne */
        $campagne = $this->findOrExit(\PsrLib\ORM\Entity\Campagne::class, $campagne_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneEditVoter::ACTION_CAMPAGNE_EDIT, $campagne);

        $fileName = $this->mpdfgeneration->genererCampagneTmp(
            $campagne
        );

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'bulletin vierge.pdf');
    }

    #[Route('campagne/form_1', name: 'campagne_form_1')]
    public function form_1(): Response
    {
        $wizard = $this->session_get_campagne_wizard();
        if (false === $wizard) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $campagne = $wizard->getCampagne();

        $form = $this->formFactory->create(\PsrLib\Form\CampagneForm1Type::class, $campagne, [
            'validation_groups' => ['form1'],
        ]);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session_store_campagne_wizard($wizard);

            return $this->redirectToRoute('campagne_form_2');
        }

        return $this->render('campagne/form_1.html.twig', [
            'form' => $form->createView(),
            'campagne' => $campagne,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('campagne/form_2', name: 'campagne_form_2')]
    public function form_2(): Response
    {
        $wizard = $this->session_get_campagne_wizard();
        if (false === $wizard) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $campagne = $wizard->getCampagne();

        $form = $this->formFactory->create(\PsrLib\Form\CampagneForm2Type::class, $campagne, [
            'validation_groups' => ['form2'],
        ]);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session_store_campagne_wizard($wizard);

            return $this->redirectToRoute('campagne_form_3');
        }

        return $this->render('campagne/form_2.html.twig', [
            'form' => $form->createView(),
            'campagne' => $campagne,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('campagne/form_3', name: 'campagne_form_3')]
    public function form_3(): Response
    {
        $wizard = $this->session_get_campagne_wizard();
        if (false === $wizard) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $campagne = $wizard->getCampagne();

        $form = $this->formFactory->create(\PsrLib\Form\CampagneForm3Type::class, $campagne, [
            'validation_groups' => ['form3'],
        ]);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->session_store_campagne_wizard($wizard);

            return $this->redirectToRoute('campagne_form_4');
        }

        return $this->render('campagne/form_3.html.twig', [
            'form' => $form->createView(),
            'campagne' => $campagne,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('campagne/form_4', name: 'campagne_form_4')]
    public function form_4(): Response
    {
        $wizard = $this->session_get_campagne_wizard();
        if (false === $wizard) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }
        $campagne = $wizard->getCampagne();

        if ($this->request->isMethod('POST')) {
            $campagnePrevious = $wizard->getPrevious();
            if (null !== $campagnePrevious) {
                // Race condition. Can append if campagne edited in another tab
                if (null !== $campagnePrevious->getVersionSuivante()) {
                    $this->addFlash('danger', 'La campagne a été modifiée par un autre utilisateur. Veuillez recommencer.');

                    return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
                }

                $campagnePrevious->setVersionSuivante($campagne);
            }

            $this->em->persist($campagne);
            $this->em->flush();

            $this->session_clear_campagne_wizard();

            if (null !== $campagnePrevious) {
                $this->addFlash('success', 'La campagne a été modifiée avec succès.');
            } else {
                $this->addFlash('success', 'La campagne a été créée avec succès.');
            }

            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        return $this->render('campagne/form_4.html.twig', [
            'campagne' => $campagne,
            'retUrl' => $this->session_get_home_path(self::HOME_PATH_ID),
        ]);
    }

    #[Route('campagne/preview_pdf', name: 'campagne_preview_pdf')]
    public function preview_pdf(): Response
    {
        $wizard = $this->session_get_campagne_wizard();
        if (false === $wizard) {
            return $this->redirect($this->session_get_home_path(self::HOME_PATH_ID));
        }

        $fileName = $this->mpdfgeneration->genererCampagneTmp(
            $wizard->getCampagne()
        );

        return BinaryFileResponseFactory::createFromTempFilePath($fileName, 'bulletin vierge.pdf', ResponseHeaderBag::DISPOSITION_INLINE);
    }

    #[Route('campagne/bulletin_import/{amap_id}/{campagne_id}', name: 'campagne_bulletin_import')]
    public function bulletin_import(int $amap_id, int $campagne_id = null): Response
    {
        $amap = $this->findOrExit(\PsrLib\ORM\Entity\Amap::class, $amap_id);
        $campagne = null;
        if (null !== $campagne_id) {
            $campagne = $this->findOrExit(\PsrLib\ORM\Entity\Campagne::class, $campagne_id);
        }
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneListAdminVoter::ACTION_CAMPAGNE_LIST_ADMIN);
        $this->redirectFromStoredGetParamIfExist('campagne_import_admin');

        $state = new \PsrLib\DTO\SearchCampagneBulletinImportState($amap, $campagne);
        $searchForm = $this->formFactory->create(\PsrLib\Form\SearchCampagneBulletinImportType::class, $state, [
            'amap_choices' => $this->em->getRepository(\PsrLib\ORM\Entity\Amap::class)->findAllAmapDeFaitForAdmin($this->getUser()),
        ]);
        // Force submit when nothing selected
        $searchForm->submit($this->request->query->get($searchForm->getName()));

        $form = $this->formFactory->create(\PsrLib\Form\CampagneBulletinImportType::class);
        $form->handleRequest($this->request);

        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $this->serializer->deserialize(
                $form->get('file')->getData()->getRealPath(),
                \PsrLib\DTO\CampagneBulletinImportLineDTO::class.'[]',
                'xls',
                [
                    'disable_type_enforcement' => true,
                    'campagne' => $state->getCampagne(),
                ]
            );

            $i = 2;
            foreach ($data as $line) {
                $lineErrors = $this->validator->validate($line);
                foreach ($lineErrors as $lineError) {
                    $errors[] = sprintf('Ligne %d : %s', $i, $lineError->getMessage());
                }
                ++$i;
            }

            if (0 === count($errors)) {
                foreach ($data as $line) {
                    $bulletin = $this->campagneBulletinBuilder->createFromImportDTO($line);
                    $this->em->persist($bulletin);
                }
                $this->em->flush();
                $this->addFlash('success', sprintf('%s bulletins ont été importés avec succès.', count($data)));

                return $this->redirectToRoute('campagne_bulletin_admin_liste', ['amap_id' => $state->getAmap()->getId()]);
            }
        }

        return $this->render('campagne/bulletin_import.html.twig', [
            'amap' => $amap,
            'searchForm' => $searchForm->createView(),
            'form' => $form->createView(),
            'state' => $state,
            'errors' => $errors,
        ]);
    }

    #[Route('campagne/bulletin_import_generate_file/{campagne_id}', name: 'campagne_bulletin_import_generate_file')]
    public function bulletin_import_generate_file(int $campagne_id): Response
    {
        $campagne = $this->findOrExit(\PsrLib\ORM\Entity\Campagne::class, $campagne_id);
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\CampagneBulletinImportVoter::ACTION_CAMPAGNE_BULLETIN_IMPORT, $campagne);

        $userEmail = new \PsrLib\ORM\Entity\UserEmail();
        $userEmail->setEmail('amapien@test.com');
        $user = new \PsrLib\ORM\Entity\User();
        $user->addEmail($userEmail);
        $amapien = new \PsrLib\ORM\Entity\Amapien($user, $campagne->getAmap());

        $line = new \PsrLib\DTO\CampagneBulletinImportLineDTO($campagne);
        $line->setAmapien($amapien);
        $line->setPayeur('Payeur');
        $line->setPaiementDate(new \Carbon\Carbon('2000-01-01'));
        $line->setPermissionImage('O');

        $line->setMontantLibres($campagne->getMontantLibres()->map(fn () => \Money\Money::EUR(100))->toArray());

        $file = $this->serializer
            ->serialize(
                [$line],
                'xls',
                [
                    'disable_type_enforcement' => true,
                    'campagne' => $campagne,
                ]
            )
        ;

        return new XlsMemoryResponse($file, 'bulletin_import.xls');
    }

    private function session_get_campagne_wizard(): false|CampagneWizardDTO
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('campagne_wizard', ''), CampagneWizardDTO::class, 'json', [
                    'groups' => 'wizardCampagne',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function session_store_campagne_wizard(CampagneWizardDTO $campagne): void
    {
        $this->sfSession->set(
            'campagne_wizard',
            $this->serializer->serialize($campagne, 'json', [
                'groups' => 'wizardCampagne',
                'json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION,
            ])
        );
    }

    private function session_clear_campagne_wizard(): void
    {
        $this->sfSession->set('campagne_wizard', '');
    }

    private function session_get_campagne_subscription_wizard(): false|CampagneBulletin
    {
        try {
            $deserialized = $this
                ->serializer
                ->deserialize($this->sfSession->get('campagne_subscription_wizard', ''), \PsrLib\ORM\Entity\CampagneBulletin::class, 'json', [
                    'groups' => 'wizardCampagneSubscription',
                ])
            ;
        } catch (\Exception $e) {
            $deserialized = false;
        }

        return $deserialized;
    }

    private function session_store_campagne_subscription_wizard(CampagneBulletin $campagne): void
    {
        $this->sfSession->set(
            'campagne_subscription_wizard',
            $this->serializer->serialize($campagne, 'json', [
                'groups' => 'wizardCampagneSubscription',
                'json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION,
            ])
        );
    }

    private function session_clear_campagne_subscription_wizard(): void
    {
        $this->sfSession->set('campagne_subscription_wizard', '');
    }

    private function remove_recu(CampagneBulletinRecus $recu): void
    {
        $bulletin = $recu->getBulletin();
        $campagne = $bulletin->getCampagne();
        $amapienAA = $bulletin->getAmapien()->getAnneeAdhesions()->filter(fn (\PsrLib\ORM\Entity\AmapienAnneeAdhesion $aa) => $aa->getAnnee() === $campagne->getAnneeAdhesionAmapien())->first();

        if (false !== $amapienAA) {
            $this->em->remove($amapienAA);
        }
        $this->em->remove($recu);
        $recu->getBulletin()->setRecus(null);
    }

    private function bulletinFileName(CampagneBulletin $bulletin): string
    {
        return $this->stringUtils->slugToFileName(
            sprintf(
                'bulletin %s %s.pdf',
                $bulletin->getAmapien()->getAmap()->getNom(),
                $bulletin->getAmapien()->getUser()->getName()->getFullNameInversed()
            )
        );
    }

    private function recuFileName(CampagneBulletinRecus $recus): string
    {
        return $this->stringUtils->slugToFileName(
            sprintf(
                'recu %s %s.pdf',
                $recus->getBulletin()->getAmapien()->getAmap()->getNom(),
                $recus->getBulletin()->getAmapien()->getUser()->getName()->getFullNameInversed(),
            )
        );
    }
}
