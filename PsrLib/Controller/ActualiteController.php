<?php

declare(strict_types=1);

namespace PsrLib\Controller;

use PsrLib\ORM\Entity\Actualite;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActualiteController extends AbstractController
{
    #[Route('actualites', name: 'actualites')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\ActualiteManageVoter::ACTUALITE_MANAGE_VOTER);
        $actualites = $this->em->getRepository(\PsrLib\ORM\Entity\Actualite::class)->findBy([], ['date' => 'DESC']);

        return $this->render('actualites/index.html.twig', [
            'actualites' => $actualites,
        ]);
    }

    #[Route('actualites/user', name: 'actualites_user')]
    public function user(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);
        $actualites = $this->em->getRepository(\PsrLib\ORM\Entity\Actualite::class)->findBy([], ['date' => 'DESC']);

        return $this->render('actualites/user.html.twig', [
            'actualites' => $actualites,
        ]);
    }

    #[Route('actualites/user_read_last', name: 'actualites_user_read_last', methods: 'POST')]
    public function user_read_last(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        $currentUser = $this->getUser();
        if (null === $currentUser) {
            $this->exit_error();
        }

        $actualite = $this->em->getRepository(\PsrLib\ORM\Entity\Actualite::class)->getLastNotReadByUser($currentUser);
        if (null === $actualite) {
            return new Response();
        }

        $actualite->addUtilsateursLus($currentUser);
        $this->em->flush();

        return new Response();
    }

    #[Route('actualites/user_read_all', name: 'actualites_user_read_all', methods: 'POST')]
    public function user_read_all(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\UserVoter::ACTION_USER_IS_AUTHENTICATED);

        $currentUser = $this->getUser();

        $actualites = $this->em->getRepository(\PsrLib\ORM\Entity\Actualite::class)->getNotReadAllByUser($currentUser);
        foreach ($actualites as $actualite) {
            $actualite->addUtilsateursLus($currentUser);
        }
        $this->em->flush();

        return new Response();
    }

    #[Route('actualites/form/{id}', name: 'actualites_form')]
    public function form(int $id = null): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\ActualiteManageVoter::ACTUALITE_MANAGE_VOTER);
        $actu = new \PsrLib\ORM\Entity\Actualite();
        if (null !== $id) {
            $actu = $this->findOrExit(\PsrLib\ORM\Entity\Actualite::class, $id);
        }

        $form = $this->formFactory->create(\PsrLib\Form\ActualiteType::class, $actu);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($actu);
            $this->em->flush();

            $this->addFlash('success', 'L\'actualité a bien été enregistrée');

            return $this->redirectToRoute('actualites');
        }

        return $this->render('actualites/form.html.twig', [
            'form' => $form->createView(),
            'actu' => $actu,
        ]);
    }

    #[Route('actualites/supprimer', name: 'actualites_supprimer', methods: 'POST')]
    public function supprimer(): Response
    {
        $this->denyAccessUnlessGranted(\PsrLib\Services\Security\Voters\ActualiteManageVoter::ACTUALITE_MANAGE_VOTER);

        /** @var \PsrLib\ORM\Entity\Actualite[] $actualites */
        $actualites = $this->parseTableSelectQueryParams(Actualite::class);

        foreach ($actualites as $actualite) {
            $this->em->remove($actualite);
        }
        $this->em->flush();

        $this->addFlash('success', 'Les actualités ont bien été supprimées');

        return $this->redirectToRoute('actualites');
    }
}
