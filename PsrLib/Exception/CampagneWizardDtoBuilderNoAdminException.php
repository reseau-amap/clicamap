<?php

declare(strict_types=1);

namespace PsrLib\Exception;

class CampagneWizardDtoBuilderNoAdminException extends \RuntimeException
{
}
