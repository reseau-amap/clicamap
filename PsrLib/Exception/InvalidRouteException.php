<?php

declare(strict_types=1);

namespace PsrLib\Exception;

class InvalidRouteException extends \LogicException
{
}
