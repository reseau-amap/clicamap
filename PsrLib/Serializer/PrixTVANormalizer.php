<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Money\Money;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PrixTVANormalizer implements NormalizerInterface, DenormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    private ?\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer = null;
    private ?\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer = null;

    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $prix = new PrixTVA();
        $prix->setTva((float) $data['tva']);
        $prix->setPrixTTC($this->denormalizer->denormalize($data['prixTTC'], Money::class));

        return $prix;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return PrixTVA::class === $type && is_array($data) && 'xls' !== $format;
    }

    /**
     * @param PrixTVA    $object
     * @param mixed|null $format
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'tva' => $object->getTva(),
            'prixTTC' => $this->normalizer->normalize($object->getPrixTTC()),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof PrixTVA;
    }
}
