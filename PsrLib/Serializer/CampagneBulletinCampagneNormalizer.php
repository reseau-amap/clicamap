<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Campagne;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneBulletinCampagneNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Campagne
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    /**
     * @param Campagne $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getId();
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return Campagne::class === $type
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->em->find(Campagne::class, $data);
    }
}
