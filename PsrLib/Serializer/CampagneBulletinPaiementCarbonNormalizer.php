<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Carbon\Carbon;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneBulletinPaiementCarbonNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Carbon
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    /**
     * @param ?Carbon $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object?->format('Y-m-d');
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return Carbon::class === $type
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if (null === $data) {
            return null;
        }

        return Carbon::createFromFormat('Y-m-d', $data);
    }
}
