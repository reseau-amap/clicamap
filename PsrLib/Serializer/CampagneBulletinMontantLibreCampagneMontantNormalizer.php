<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\CampagneMontant;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneBulletinMontantLibreCampagneMontantNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof CampagneMontant
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    /**
     * @param CampagneMontant $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getId();
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return CampagneMontant::class === $type
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->em->find(CampagneMontant::class, $data);
    }
}
