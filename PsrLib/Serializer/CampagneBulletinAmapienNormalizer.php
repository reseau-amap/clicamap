<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amapien;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneBulletinAmapienNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Amapien
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    /**
     * @param Amapien $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getId();
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return Amapien::class === $type
            && isset($context['groups'])
            && 'wizardCampagneSubscription' === $context['groups']
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->em->find(Amapien::class, $data);
    }
}
