<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use PsrLib\DTO\GeocodeSuggestion;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class GeocodeSuggestionAjaxNormalizer implements ContextAwareNormalizerInterface
{
    /**
     * @param GeocodeSuggestion $object
     * @param mixed|null        $format
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $address = $object->getAddress();
        $ville = $object->getVille();

        $addressString = sprintf(
            '%s %s %s %s',
            $address->getAddress()->getHouseNumber(),
            $address->getAddress()->getRoad(),
            $address->getAddress()->getPostCode(),
            $address->getAddress()->getPlaceName()
        );

        return [
            'label' => $addressString,
            'value' => [
                'cp' => $ville->getCpString(),
                'ville' => $ville->getNom(),
                'lat' => $address->getLat(),
                'long' => $address->getLon(),
                'address' => $addressString,
            ],
        ];
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof GeocodeSuggestion
            && 'json' === $format
            && ($context['groups'] ?? null) === 'ajax'
        ;
    }
}
