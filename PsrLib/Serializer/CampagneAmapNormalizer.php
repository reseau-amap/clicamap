<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\Amap;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneAmapNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Amap
            && isset($context['groups'])
            && 'wizardCampagne' === $context['groups']
        ;
    }

    /**
     * @param Amap $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getId();
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return Amap::class === $type
            && isset($context['groups'])
            && 'wizardCampagne' === $context['groups']
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->em->find(Amap::class, $data);
    }
}
