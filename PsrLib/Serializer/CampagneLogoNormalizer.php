<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use PsrLib\ORM\Entity\Files\CampagneLogo;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CampagneLogoNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return CampagneLogo::class === $type
            && 'json' === $format
            && ($context['groups'] ?? null) === 'wizardCampagne'
        ;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof CampagneLogo
            && 'json' === $format
            && ($context['groups'] ?? null) === 'wizardCampagne'
        ;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return new CampagneLogo($data['fileName'], $data['originalFileName']);
    }

    /**
     * @param CampagneLogo $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return [
            'fileName' => $object->getFileName(),
            'originalFileName' => $object->getOriginalFileName(),
        ];
    }
}
