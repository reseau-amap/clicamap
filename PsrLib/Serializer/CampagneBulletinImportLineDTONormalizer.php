<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Assert\Assertion;
use Carbon\Carbon;
use PsrLib\DTO\CampagneBulletinImportLineDTO;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\Services\MoneyHelper;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CampagneBulletinImportLineDTONormalizer implements NormalizerInterface, DenormalizerInterface
{
    private const COLUMN_EMAIL = 'Email';
    private const COLUMN_PAYEUR = 'Payeur';
    private const COLUMN_PAIEMENT_DATE = 'Date de paiement';
    private const COLUMN_PERMISSION_IMAGE = 'Autorisation image (O/N)';
    private const DATE_FORMAT = 'd/m/Y';

    private AmapienRepository $amapienRepository;

    public function __construct(AmapienRepository $amapienRepository)
    {
        $this->amapienRepository = $amapienRepository;
    }

    /**
     * @param mixed $data
     *
     * @return CampagneBulletinImportLineDTO
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $campagne = $context['campagne'] ?? null;
        Assertion::isInstanceOf($campagne, \PsrLib\ORM\Entity\Campagne::class);
        $line = new CampagneBulletinImportLineDTO($campagne);

        $amapien = $this->amapienRepository->getByAmapEmail($campagne->getAmap(), $data[self::COLUMN_EMAIL] ?? '');
        $line->setAmapien($amapien);
        $line->setPayeur($data[self::COLUMN_PAYEUR] ?? null);

        $dateRaw = $data[self::COLUMN_PAIEMENT_DATE] ?? null;
        if (Carbon::canBeCreatedFromFormat($dateRaw, self::DATE_FORMAT)) {
            $line->setPaiementDate(Carbon::createFromFormat(self::DATE_FORMAT, $dateRaw));
        } else {
            $line->setPaiementDate(null);
        }
        $line->setPermissionImage($data[self::COLUMN_PERMISSION_IMAGE] ?? null);

        $montants = [];
        foreach ($data as $key => $value) {
            if (str_starts_with($key, 'Montant libre')) {
                try {
                    $parsed = MoneyHelper::fromString($value);
                } catch (\Exception $e) {
                    $parsed = null;
                }
                $montants[] = $parsed;
            }
        }
        $line->setMontantLibres($montants);

        return $line;
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return CampagneBulletinImportLineDTO::class === $type && 'xls' === $format;
    }

    /**
     * @param CampagneBulletinImportLineDTO $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $res = [
            self::COLUMN_EMAIL => $object->getAmapien()->getUser()->getEmails()[0]?->getEmail() ?? '',
            self::COLUMN_PAYEUR => $object->getPayeur(),
            self::COLUMN_PAIEMENT_DATE => $object->getPaiementDate()?->format(self::DATE_FORMAT),
            self::COLUMN_PERMISSION_IMAGE => $object->getPermissionImage(),
        ];

        $i = 1;
        foreach ($object->getMontantLibres() as $montantLibre) {
            $res[sprintf('Montant libre %s', $i)] = MoneyHelper::toStringDecimal($montantLibre);
            ++$i;
        }

        return $res;
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof CampagneBulletinImportLineDTO && 'xls' === $format;
    }
}
