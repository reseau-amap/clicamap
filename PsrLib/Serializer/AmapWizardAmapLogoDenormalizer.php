<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use PsrLib\ORM\Entity\Files\AmapLogo;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class AmapWizardAmapLogoDenormalizer implements ContextAwareDenormalizerInterface, ContextAwareNormalizerInterface
{
    public function denormalize($data, $type, $format = null, array $context = [])
    {
        return new AmapLogo($data['fileName'], $data['originalFileName']);
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return AmapLogo::class === $type
            && isset($context['groups'])
            && 'wizardAmap' === $context['groups']
        ;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof AmapLogo
            && isset($context['groups'])
            && 'wizardAmap' === $context['groups']
        ;
    }

    /**
     * @param AmapLogo $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return [
            'fileName' => $object->getFileName(),
            'originalFileName' => $object->getOriginalFileName(),
        ];
    }
}
