<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\CampagneWizardDTO;
use PsrLib\ORM\Entity\Campagne;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CampagneWizardDTONormalizer implements DenormalizerAwareInterface, NormalizerAwareInterface, NormalizerInterface, DenormalizerInterface
{
    private ?\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer = null;
    private ?\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer = null;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $campagne = $this->denormalizer->denormalize($data['campagne'], Campagne::class, $format, $context);
        $previous = null;
        if (null !== $data['previous']) {
            $previous = $this->em->getReference(Campagne::class, $data['previous']);
        }

        return new CampagneWizardDTO($campagne, $previous);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return CampagneWizardDTO::class === $type;
    }

    /**
     * @param CampagneWizardDTO $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return [
            'campagne' => $this->normalizer->normalize($object->getCampagne(), $format, $context),
            'previous' => $object->getPrevious()?->getId(),
        ];
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof CampagneWizardDTO;
    }
}
