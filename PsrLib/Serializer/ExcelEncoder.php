<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

class ExcelEncoder implements EncoderInterface
{
    public function encode($data, string $format, array $context = [])
    {
        if ('xls' !== $format) {
            throw new \LogicException('Unsupported format');
        }

        Settings::setLocale('fr');

        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();

        if (count($data) > 0) {
            $activeWorksheet->fromArray(array_keys($data[0]), null, 'A1');

            $rowId = 2;
            foreach ($data as $row) {
                $activeWorksheet->fromArray([$row], null, 'A'.$rowId);
                ++$rowId;
            }
        }

        foreach ($activeWorksheet->getColumnIterator() as $column) {
            $activeWorksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $activeWorksheet->getStyle('A1:'.$activeWorksheet->getHighestColumn().'1')->getFont()->setBold(true);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        ob_start();
        $writer->save('php://output');

        return ob_get_clean();
    }

    public function supportsEncoding(string $format)
    {
        return 'xls' === $format;
    }
}
