<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use PsrLib\DTO\EvenemntAbonnableDTO;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EvenementAbonnementButtonRender;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Translation\Translator;

class EvenementAbonnableDTODatatableNormalizer implements ContextAwareNormalizerInterface
{
    public function __construct(
        private readonly Translator $translator,
        private readonly EvenementAbonnementButtonRender $evenementAbonnementButtonRender
    ) {
    }

    /**
     * @param EvenemntAbonnableDTO $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return [
            'nom' => $object->getNom(),
            'type' => $this->translator->trans($object->getType()->value, [], 'evenement_add_type'),
            'action' => $this->evenementAbonnementButtonRender->renderButton($context['current_user'], $object->getRelatedEntity()),
        ];
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof EvenemntAbonnableDTO
            && in_array('datatable', $context['groups'] ?? [])
            && ($context['current_user'] ?? '') instanceof User
        ;
    }
}
