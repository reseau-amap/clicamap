<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class AmapWizardUserDenormalizer implements ContextAwareDenormalizerInterface, ContextAwareNormalizerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        return $this->em->getReference(User::class, $data);
    }

    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        return User::class === $type
            && isset($context['groups'])
            && 'wizardAmap' === $context['groups']
        ;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof User
            && isset($context['groups'])
            && 'wizardAmap' === $context['groups']
        ;
    }

    /**
     * @param User $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getId();
    }
}
