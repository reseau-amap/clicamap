<?php

declare(strict_types=1);

namespace PsrLib\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use PsrLib\DTO\ModeleContratDeplacement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ModeleContratDeplacementNormalizer implements DenormalizerInterface, NormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface
{
    private ?\Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer = null;

    private ?\Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer = null;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param ModeleContratDeplacement $object
     * @param mixed|null               $format
     *
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'mcId' => $object->getMc()->getId(),
            'srcId' => $object->getSrc()->getId(),
            'dst' => $this->normalizer->normalize($object->getDst()),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ModeleContratDeplacement && 'json' === $format;
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $mc = $this->em->getReference(ModeleContrat::class, $data['mcId']);
        $mcd = new ModeleContratDeplacement($mc);
        $mcd->setDst(
            null === $data['dst']
                ? null
                : $this->denormalizer->denormalize($data['dst'], \DateTime::class)
        );
        $mcd->setSrc($this->em->getReference(ModeleContratDate::class, $data['srcId']));

        return $mcd;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return ModeleContratDeplacement::class === $type && 'json' === $format;
    }
}
