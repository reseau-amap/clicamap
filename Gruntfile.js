module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        concat: {
            options: {
            },
            css: {
                src: [
                    './assets/css/jquery-ui.css',
                    './assets/bootstrap/css/bootstrap.css',
                    './assets/bootstrap/css/datepicker.css',
                    './assets/css/fontawesome/css/all.min.css',
                    './assets/plugins/select2/css/select2.min.css',
                    './assets/plugins/flatpickr/flatpickr.min.css',
                    './assets/plugins/slick/slick.css',
                    './assets/plugins/slick/slick-theme-custom.css',
                    './assets/plugins/leaflet/leaflet.css',
                    './assets/plugins/summernote/summernote.min.css',
                    './assets/plugins/datatables/datatables.min.css',
                    './assets/css/style.css',
                ],
                dest: 'public/dist/app.css',
            },

            js: {
                src: [
                    './assets/plugins/leaflet/leaflet.js',
                    './assets/plugins/htmx/htmx.min.js',
                    './assets/plugins/htmx/idiomorph-ext.min.js',
                    './assets/jquery/jquery-2.2.3.min.js',
                    './assets/jquery/jquery-ui.js',
                    './assets/bootstrap/js/bootstrap-datepicker.js',
                    './assets/bootstrap/js/bootstrap.js',
                    './assets/plugins/jquery-debounce/jquery.ba-throttle-debounce.min.js',
                    './assets/plugins/jquery-mask/jquery.mask.min.js',
                    './assets/plugins/bootbox/bootbox.all.min.js',
                    './assets/plugins/select2/js/select2.min.js',
                    './assets/plugins/slick/slick.min.js',
                    './assets/plugins/summernote/summernote.min.js',
                    './assets/plugins/moment/moment.min.js',
                    './assets/plugins/datatables/datatables.min.js',
                    './assets/plugins/datatables/datetime-moment.js',
                    './assets/plugins/datatables/datatables.init.js',
                    './assets/plugins/uri/URI.js',
                    './assets/plugins/flatpickr/flatpickr.js',
                    './assets/plugins/flatpickr/fr.js',
                    './assets/plugins/lodash/lodash.min.js',
                    './assets/javascript/custom.js',
                ],
                dest: 'public/dist/app.js',
            },
        },
        copy: {
            main: {
                files: [
                    {expand: true, flatten: true, src: ['assets/plugins/summernote/font/*'], dest: 'public/dist/font/'},
                    {expand: true, flatten: true, src: ['assets/plugins/slick/fonts/*'], dest: 'public/dist/fonts/'},
                    {expand: true, flatten: true, src: ['assets/plugins/slick/*.gif'], dest: 'public/dist/'},
                    {expand: true, flatten: true, src: ['assets/plugins/leaflet/images/*'], dest: 'public/dist/images/'},
                    {expand: true, flatten: true, src: ['assets/plugins/datatables/*.json'], dest: 'public/dist/plugins/datatables/'},
                ],
            },
        },

        cacheBust: {
            options: {
                assets: ['dist/app.css', 'dist/app.js'],
                outputDir: 'dist/',
                baseDir: 'public/',
                jsonOutput: true,
                jsonOutputFilename: 'dist/grunt-cache-bust.json',
            },
            static_mappings: {
                files: [
                    {src: 'dist/app.css', dest: 'dist/app.css'},
                    {src: 'dist/app.js', dest: 'dist/app.js'},
                ],
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-cache-bust');

    grunt.registerTask('default', ['concat', 'copy', 'cacheBust']);
};