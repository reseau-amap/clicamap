<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapTest extends ContainerAwareTestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testSiretLuhnInvalid(): void
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'siret', '11111111111111');
        self::assertCount(1, $violations);
        self::assertSame('Le SIRET est invalide.', $violations[0]->getMessage());
    }

    public function testSiretLuhnValid(): void
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'siret', '44835745900024');
        self::assertEmpty($violations);
    }

    /**
     * @dataProvider rnaInvalidProvider
     */
    public function testRnaInvalid(string $rna): void
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'rna', $rna);
        self::assertCount(1, $violations);
        self::assertSame('Le numéro RNA doit être composé de W suivi de 9 chiffres.', $violations[0]->getMessage());
    }

    public function rnaInvalidProvider()
    {
        return [
            ['123'],
            ['W12345678'],
            ['W1234567891'],
            ['w123456789'],
            ['W12345678a'],
        ];
    }

    public function testRnaValid(): void
    {
        $violations = $this->validator->validatePropertyValue(Amap::class, 'rna', 'W123456789');
        self::assertEmpty($violations);
    }

    public function testCountFermeRefAmapiensEmpty(): void
    {
        $amap = new Amap();
        self::assertSame(0, $amap->countAmapiensRef());
    }

    public function testCountFermeRefAmapiens(): void
    {
        $amap = new Amap();
        $user1 = $this->prophesize(User::class);
        $user1->isRefProduitOfAmap($amap)->willReturn(true);
        $amapien1 = $this->prophesize(Amapien::class);
        $amapien1->isRefProduit()->willReturn(true);
        $amapien1->getUser()->willReturn($user1->reveal());
        $amapien1->setAmap($amap)->shouldBeCalled();

        $user2 = $this->prophesize(User::class);
        $user2->isRefProduitOfAmap($amap)->willReturn(false);
        $amapien2 = $this->prophesize(Amapien::class);
        $amapien2->isRefProduit()->willReturn(false);
        $amapien2->getUser()->willReturn($user2->reveal());
        $amapien2->setAmap($amap)->shouldBeCalled();

        $amap->addAmapien($amapien1->reveal());
        $amap->addAmapien($amapien2->reveal());

        self::assertSame(1, $amap->countAmapiensRef());
    }
}
