<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratForm6Test extends ModeleContratAbstractTest
{
    public function testValidateEmpty(): void
    {
        $violations = $this->validator->validate(new ModeleContrat(), null, ['form6']);
        self::assertCount(2, $violations);
        self::assertViolationContainsMessage($violations, 'Merci de sélectionner au moins un mode de règlement', 'reglementType');
        self::assertViolationContainsMessage($violations, 'This value should not be null.', 'reglementNbMax');
    }

    /**
     * @dataProvider providerValidateReglementNbMax
     */
    public function testValidateReglementNbMax(?int $reglementMax, int $reglementCount, bool $isValidated): void
    {
        $mc = new ModeleContrat();
        $mc->setReglementNbMax($reglementMax);
        for ($i = 0; $i < $reglementCount; ++$i) {
            $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));
        }

        $violations = $this->validator->validate($mc, null, ['form6']);
        if ($isValidated) {
            self::assertViolationNotContainsMessage($violations, 'Le nombre de règlements maximum doit être inférieur ou égal au nombre de dates des règlements', 'reglementNbMax');
        } else {
            self::assertViolationContainsMessage($violations, 'Le nombre de règlements maximum doit être inférieur ou égal au nombre de dates des règlements', 'reglementNbMax');
        }
    }

    public function providerValidateReglementNbMax()
    {
        return [
            [null, 0, true],
            [0, 0, true],
            [1, 0, false],
            [0, 1, true],
            [1, 1, true],
            [2, 1, false],
        ];
    }

    public function testValidateDateReglementNoDuplication(): void
    {
        $mc = new ModeleContrat();
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationContainsMessage($violations, 'Les dates de règlement ne doivent pas être identiques', 'dateReglements');
    }

    public function testValidateDateReglementNotBeforeForclusion(): void
    {
        $mc = new ModeleContrat();
        $mc->setForclusion(new \DateTime('2000-01-04'));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-05')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-03')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationContainsMessage($violations, 'Les dates de règlement ne peuvent pas être inférieures ou égales à la date de fin de souscription', 'dateReglements');
    }

    public function testValidateDateReglementNotEqualForclusion(): void
    {
        $mc = new ModeleContrat();
        $mc->setForclusion(new \DateTime('2000-01-04'));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-05')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-04')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationContainsMessage($violations, 'Les dates de règlement ne peuvent pas être inférieures ou égales à la date de fin de souscription', 'dateReglements');
    }

    public function testValidateDateReglementAfterForclusionValid(): void
    {
        $mc = new ModeleContrat();
        $mc->setForclusion(new \DateTime('2000-01-03'));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-05')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-04')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationNotContainsMessage($violations, 'Les dates de règlement ne peuvent pas être inférieures ou égales à la date de fin de souscription', 'dateReglements');
    }

    public function testValidateDateReglementNotAfterLastDate(): void
    {
        $mc = new ModeleContrat();
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-05')));

        $mc->addDate($this->buildDate(new \DateTime('2000-01-02')));
        $mc->addDate($this->buildDate(new \DateTime('2000-01-04')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationContainsMessage($violations, 'Les dates de règlement ne peuvent pas être après la fin des livraisons', 'dateReglements');
    }

    public function testValidateDateReglementEqualLastDateValid(): void
    {
        $mc = new ModeleContrat();
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-04')));

        $mc->addDate($this->buildDate(new \DateTime('2000-01-02')));
        $mc->addDate($this->buildDate(new \DateTime('2000-01-04')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationNotContainsMessage($violations, 'Les dates de règlement ne peuvent pas être après la fin des livraisons', 'dateReglements');
    }

    public function testValidateDateReglementBeforeLastDateValid(): void
    {
        $mc = new ModeleContrat();
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-01')));
        $mc->addDateReglement($this->buildDateReglement(new \DateTime('2000-01-03')));

        $mc->addDate($this->buildDate(new \DateTime('2000-01-02')));
        $mc->addDate($this->buildDate(new \DateTime('2000-01-04')));

        $violations = $this->validator->validate($mc, null, ['form6']);
        self::assertViolationNotContainsMessage($violations, 'Les dates de règlement ne peuvent pas être après la fin des livraisons', 'dateReglements');
    }

    private function buildDateReglement(\DateTime $date): ModeleContratDatesReglement
    {
        $dateReglement = new ModeleContratDatesReglement();
        $dateReglement->setDate($date);

        return $dateReglement;
    }

    private function buildDate(\DateTime $dateLivraison): ModeleContratDate
    {
        $date = new ModeleContratDate();
        $date->setDateLivraison($dateLivraison);

        return $date;
    }
}
