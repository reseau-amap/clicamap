<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratTest extends ModeleContratAbstractTest
{
    public function testDatePositionNoDates(): void
    {
        $mc = new ModeleContrat();
        $date = $this->buildModeleContratDate(new \DateTime('2000-01-01'));

        self::assertNull($mc->getDatePosition($date));
    }

    public function testDatePositionDateOrdered(): void
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $mc->addDate($date1);
        $mc->addDate($date2);

        self::assertSame(1, $mc->getDatePosition($date1));
        self::assertSame(2, $mc->getDatePosition($date2));
    }

    public function testDatePositionDateReverseOrdered(): void
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $date1->setId(1);
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $date2->setId(2);
        $mc->addDate($date1);
        $mc->addDate($date2);

        self::assertSame(2, $mc->getDatePosition($date1));
        self::assertSame(1, $mc->getDatePosition($date2));
    }

    public function testDatePositionDateNotAssociated(): void
    {
        $mc = new ModeleContrat();
        $date1 = $this->buildModeleContratDate(new \DateTime('2000-01-02'));
        $date2 = $this->buildModeleContratDate(new \DateTime('2000-01-01'));
        $mc->addDate($date1);

        self::assertNull($mc->getDatePosition($date2));
    }

    public function testNbLivPlafondDepassement(): void
    {
        $mc = new ModeleContrat();
        $mc->getInformationLivraison()->setNblivPlancherDepassement(true);
        $mc->addDate(new ModeleContratDate());
        $mc->addDate(new ModeleContratDate());
        $mc->getInformationLivraison()->setNblivPlancher(3);

        self::assertSame(2, $mc->getNbLivPlafond());
    }

    public function testNbLivPlafondSansDepassement(): void
    {
        $mc = new ModeleContrat();
        $mc->getInformationLivraison()->setNblivPlancherDepassement(false);
        $mc->addDate(new ModeleContratDate());
        $mc->addDate(new ModeleContratDate());
        $mc->getInformationLivraison()->setNblivPlancher(3);

        self::assertSame(3, $mc->getNbLivPlafond());
    }

    public function testGetFutureDates(): void
    {
        $mc = new ModeleContrat();
        Carbon::setTestNow('2000-06-01');
        $mc->addDate($this->buildModeleContratDate(new Carbon('2000-05-01')));
        $mc->addDate($this->buildModeleContratDate(new Carbon('2000-06-01')));
        $mc->addDate($this->buildModeleContratDate(new Carbon('2000-06-02')));

        $res = $mc->getFutureDates();
        self::assertCount(2, $res);
        self::assertSame('2000-06-01', $res[0]->getDateLivraison()->format('Y-m-d'));
        self::assertSame('2000-06-02', $res[1]->getDateLivraison()->format('Y-m-d'));
        Carbon::setTestNow();
    }

    /**
     * @dataProvider getPremiereDateLivrableAvecDelaisProvider
     */
    public function testGetPremiereDateLivrableAvecDelais(?int $delais, Carbon $expected): void
    {
        $mc = new ModeleContrat();
        $mc->setDelaiModifContrat($delais);

        $res = $mc->getPremiereDateLivrableAvecDelais(new Carbon('01-01-2000'));
        self::assertTrue($expected->eq($res));
    }

    public function getPremiereDateLivrableAvecDelaisProvider()
    {
        return [
            [null, new Carbon('01-01-2000')],
            [-1, new Carbon('01-01-2000')],
            [0, new Carbon('01-01-2000')],
            [1, new Carbon('02-01-2000')],
            [2, new Carbon('03-01-2000')],
            [3, new Carbon('04-01-2000')],
        ];
    }

    public function testCdpNoFerme(): void
    {
        $mc = new ModeleContrat();
        self::assertFalse($mc->isCdp());
    }

    public function testCdpFermeNoRegroupement(): void
    {
        $ferme = $this->prophesize(Ferme::class);
        $ferme->getRegroupement()->willReturn(null);
        $mc = new ModeleContrat();
        $mc->setFerme($ferme->reveal());
        self::assertFalse($mc->isCdp());
    }

    public function testCdpFermeRegroupementNoCdp(): void
    {
        $regroupement = $this->prophesize(FermeRegroupement::class);
        $regroupement->isCdp()->willReturn(false);
        $ferme = $this->prophesize(Ferme::class);
        $ferme->getRegroupement()->willReturn($regroupement->reveal());
        $mc = new ModeleContrat();
        $mc->setFerme($ferme->reveal());
        self::assertFalse($mc->isCdp());
    }

    public function testCdpFermeRegroupementCdp(): void
    {
        $regroupement = $this->prophesize(FermeRegroupement::class);
        $regroupement->isCdp()->willReturn(true);
        $ferme = $this->prophesize(Ferme::class);
        $ferme->getRegroupement()->willReturn($regroupement->reveal());
        $mc = new ModeleContrat();
        $mc->setFerme($ferme->reveal());
        self::assertTrue($mc->isCdp());
    }
}
