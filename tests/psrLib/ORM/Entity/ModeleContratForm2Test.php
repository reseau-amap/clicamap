<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PsrLib\ORM\Entity\ModeleContrat;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratForm2Test extends ModeleContratAbstractTest
{
    /**
     * @dataProvider validationDateForclusionProvider
     *
     * @param Carbon[] $datesLivraisons
     */
    public function testValidationDateForclusion(?Carbon $dateForclusion, int $delaisModifContrat, $datesLivraisons, bool $hasViolation): void
    {
        $mc = new ModeleContrat();
        $mc->setForclusion($dateForclusion);
        $mc->setDelaiModifContrat($delaisModifContrat);

        foreach ($datesLivraisons as $datesLivraison) {
            $mc->addDate($this->buildModeleContratDate($datesLivraison));
        }

        $violations = $this->validator->validate($mc, null, ['form2']);
        if ($hasViolation) {
            self::assertViolationContainsMessage($violations, 'La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)');
        } else {
            self::assertViolationNotContainsMessage($violations, 'La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)');
        }
    }

    public function validationDateForclusionProvider()
    {
        return [
            [null, 1, [], false],
            [new Carbon('2000-01-01'), 1, [new Carbon('2000-01-02'), new Carbon('2000-01-04')], false],
            [new Carbon('2000-01-01'), 1, [new Carbon('2000-01-01'), new Carbon('2000-01-03')], true],
            [new Carbon('2000-01-01'), 2, [new Carbon('2000-01-02'), new Carbon('2000-01-03')], true],
        ];
    }
}
