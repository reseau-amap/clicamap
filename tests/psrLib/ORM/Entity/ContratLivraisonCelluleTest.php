<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ContratLivraisonCellule;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratLivraisonCelluleTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;

    public function testConstructNoRegul(): void
    {
        $liv = $this->prophesize(ContratLivraison::class)->reveal();
        $cel = $this->prophesizeContratCellule(false, 10.0);
        $livCellule = new ContratLivraisonCellule($liv, $cel);

        self::assertSame($liv, $livCellule->getContratLivraison());
        self::assertSame($cel, $livCellule->getContratCellule());
        self::assertSame(10.0, $livCellule->getQuantite());
    }

    public function testConstructRegul(): void
    {
        $liv = $this->prophesize(ContratLivraison::class)->reveal();
        $cel = $this->prophesizeContratCellule(true, 10.0);
        $livCellule = new ContratLivraisonCellule($liv, $cel);

        self::assertSame($liv, $livCellule->getContratLivraison());
        self::assertSame($cel, $livCellule->getContratCellule());
        self::assertNull($livCellule->getQuantite());
    }

    private function prophesizeContratCellule(bool $regul, float $qty)
    {
        $cellule = $this->prophesize(ContratCellule::class);
        $mcPro = $this->prophesize(ModeleContratProduit::class);
        $mcPro->getRegulPds()->willReturn($regul);
        $cellule->getModeleContratProduit()->willReturn($mcPro->reveal());
        $cellule->getQuantite()->willReturn($qty);

        return $cellule->reveal();
    }
}
