<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ContratLivraisonCellule;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratLivraisonTest extends TestCase
{
    use ProphecyTrait;

    public function testGetCelluleByContratCelluleEmpty(): void
    {
        $liv = $this->buildContratLivraison();
        self::assertEmpty($liv->getCelluleByContratCellule(
            $this->prophesizeContratCellule()
        ));
    }

    public function testGetCelluleByContratCellule(): void
    {
        $liv = $this->buildContratLivraison();

        $cell1 = $this->prophesizeContratCellule();
        $cell2 = $this->prophesizeContratCellule();
        $livCell1 = $this->buildContratLivraisonCellule($cell1);
        $livCell2 = $this->buildContratLivraisonCellule($cell2);
        $liv->addCellule($livCell1);
        $liv->addCellule($livCell2);

        self::assertSame($livCell1, $liv->getCelluleByContratCellule($cell1));
    }

    public function testGetCelluleByContratCelluleInvalid(): void
    {
        $liv = $this->buildContratLivraison();

        $cell1 = $this->prophesizeContratCellule();
        $cell2 = $this->prophesizeContratCellule();
        $livCell1 = $this->buildContratLivraisonCellule($cell1);
        $liv->addCellule($livCell1);

        self::assertNull($liv->getCelluleByContratCellule($cell2));
    }

    private function buildContratLivraison()
    {
        return new ContratLivraison(
            $this->prophesize(Amapien::class)->reveal(),
            $this->prophesize(Ferme::class)->reveal(),
            Carbon::now()
        );
    }

    private function buildContratLivraisonCellule(ContratCellule $cellule)
    {
        return new ContratLivraisonCellule(
            $this->prophesize(ContratLivraison::class)->reveal(),
            $cellule
        );
    }

    private function prophesizeContratCellule()
    {
        $mcPro = $this->prophesize(ModeleContratProduit::class);
        $mcPro->getRegulPds()->willReturn(true);
        $cel = $this->prophesize(ContratCellule::class);
        $cel->getModeleContratProduit()->willReturn($mcPro->reveal());

        return $cel->reveal();
    }
}
