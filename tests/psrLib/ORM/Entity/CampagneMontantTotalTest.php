<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use PsrLib\ORM\Entity\CampagneMontant;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneMontantTotalTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;

    public function testTotalEmpty(): void
    {
        $campagne = new \PsrLib\ORM\Entity\Campagne();
        self::assertTrue(Money::EUR(0)->equals($campagne->montantTotal()));
    }

    public function testTotalMontantNonLibre(): void
    {
        $campagne = new \PsrLib\ORM\Entity\Campagne();
        $campagne->addMontant($this->prophesizeCampagneMontant(false, Money::EUR(10)));
        $campagne->addMontant($this->prophesizeCampagneMontant(false, Money::EUR(20)));
        self::assertTrue(Money::EUR(30)->equals($campagne->montantTotal()));
    }

    public function testTotalMontantLibre(): void
    {
        $campagne = new \PsrLib\ORM\Entity\Campagne();
        $campagne->addMontant($this->prophesizeCampagneMontant(false, Money::EUR(10)));
        $campagne->addMontant($this->prophesizeCampagneMontant(true, Money::EUR(20)));
        $campagne->addMontant($this->prophesizeCampagneMontant(false, Money::EUR(40)));
        self::assertTrue(Money::EUR(50)->equals($campagne->montantTotal()));
    }

    private function prophesizeCampagneMontant(bool $montantLibre, Money $montantMoney): CampagneMontant
    {
        $montant = $this->prophesize(\PsrLib\ORM\Entity\CampagneMontant::class);
        $montant->isMontantLibre()->willReturn($montantLibre);
        $montant->getMontant()->willReturn($montantMoney);
        $montant->setCampagne(Argument::type(\PsrLib\ORM\Entity\Campagne::class))->willReturn($montant);

        return $montant->reveal();
    }
}
