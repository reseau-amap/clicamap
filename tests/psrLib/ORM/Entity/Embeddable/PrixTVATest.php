<?php

declare(strict_types=1);

namespace Test\ORM\Entity\Embeddable;

use Money\Money;
use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;

/**
 * @internal
 *
 * @coversNothing
 */
class PrixTVATest extends TestCase
{
    public function testHasTVAFalse(): void
    {
        $prix = new PrixTVA();
        $prix->setTva(null);
        self::assertFalse($prix->hasTVA());
    }

    public function testHasTVATrue(): void
    {
        $prix = new PrixTVA();
        $prix->setTva(1);
        self::assertTrue($prix->hasTVA());
    }

    public function testGetPrixHTTVANull(): void
    {
        $prix = new PrixTVA();
        $prix->setTva(null);
        $prix->setPrixTTC(Money::EUR(100));
        self::assertTrue(Money::EUR(100)->equals($prix->getPrixHT()));
        self::assertTrue(Money::EUR(0)->equals($prix->getTvaAmount()));
    }

    public function testGetPrixHTTVA0(): void
    {
        $prix = new PrixTVA();
        $prix->setTva(0);
        $prix->setPrixTTC(Money::EUR(100));
        self::assertTrue(Money::EUR(100)->equals($prix->getPrixHT()));
        self::assertTrue(Money::EUR(0)->equals($prix->getTvaAmount()));
    }

    /**
     * @dataProvider providerGetPrixHTTVA
     */
    public function testGetPrixHTTVA(int $ttc, int $ht, int $tvaAmount, float $tva): void
    {
        $prix = new PrixTVA();
        $prix->setTva($tva);
        $prix->setPrixTTC(Money::EUR($ttc));
        self::assertTrue(Money::EUR($ht)->equals($prix->getPrixHT()));
        self::assertTrue(Money::EUR($tvaAmount)->equals($prix->getTvaAmount()));
    }

    public function providerGetPrixHTTVA()
    {
        return [
            [120, 100, 20, 20],
            [105, 100, 5, 5.5],
        ];
    }
}
