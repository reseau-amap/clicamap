<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\ModeleContrat;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratForm4Test extends ModeleContratAbstractTest
{
    /**
     * @dataProvider amapienPermissionReportLivraisonValidProvider
     */
    public function testAmapienPermissionReportLivraisonValid(bool $produitsIdentiquesAmapien, bool $produitIdentiquesPaysan, ?bool $permissionReport): void
    {
        $mc = new ModeleContrat();
        $mc->getInformationLivraison()->setProduitsIdentiqueAmapien($produitsIdentiquesAmapien);
        $mc->getInformationLivraison()->setProduitsIdentiquePaysan($produitIdentiquesPaysan);
        $mc->getInformationLivraison()->setAmapienPermissionReportLivraison($permissionReport);

        $violations = $this->validator->validateProperty($mc, 'amapienPermissionReportLivraison', ['form4']);
        self::assertEmpty($violations);
    }

    public function amapienPermissionReportLivraisonValidProvider()
    {
        return [
            [false, false, null],
            [true, false, null],
            [false, true, null],
            [true, true, true],
            [true, true, false],
        ];
    }

    public function testAmapienPermissionReportLivraisonInvalid(): void
    {
        $mc = new ModeleContrat();
        $mcInfoLivraison = $mc->getInformationLivraison();
        $mcInfoLivraison->setProduitsIdentiqueAmapien(true);
        $mcInfoLivraison->setProduitsIdentiquePaysan(true);
        $mcInfoLivraison->setAmapienPermissionReportLivraison(null);

        $violations = $this->validator->validateProperty($mcInfoLivraison, 'amapienPermissionReportLivraison', ['form4']);
        self::assertNotEmpty($violations);
        self::assertSame('Merci de sélectionner une option', $violations[0]->getMessage());
    }
}
