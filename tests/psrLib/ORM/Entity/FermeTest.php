<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeAnneeAdhesion;
use PsrLib\ORM\Entity\FermeRegroupement;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeTest extends ContainerAwareTestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
        Carbon::setTestNow(new Carbon('2010-01-01'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    public function testInRegroupementFalse(): void
    {
        $ferme = new Ferme();
        self::assertFalse($ferme->inRegroupement());
    }

    public function testInRegroupementTrue(): void
    {
        $ferme = new Ferme();
        $ferme->setRegroupement(new FermeRegroupement());

        self::assertTrue($ferme->inRegroupement());
    }

    public function testNomRequiredNotProvided(): void
    {
        $ferme = new Ferme();
        $ferme->setNom(null);

        $violations = $this->validator->validate($ferme);
        self::assertViolationContainsMessage($violations, 'This value should not be blank.', 'nom');
    }

    public function testNomRequiredProvided(): void
    {
        $ferme = new Ferme();
        $ferme->setNom('nom');

        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'nom');
    }

    public function testSiretRequiredNoSiret(): void
    {
        $ferme = new Ferme();
        $ferme->setSiret(null);
        $violations = $this->validator->validate($ferme);
        self::assertViolationContainsMessage($violations, 'This value should not be blank.', 'siret');
    }

    public function testSiretRequiredSiret(): void
    {
        $ferme = new Ferme();
        $ferme->setSiret('1234');
        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'siret');
    }

    public function testSiretRequiredRegroupementNoSiret(): void
    {
        $ferme = new Ferme();
        $ferme->setRegroupement(new FermeRegroupement());
        $violations = $this->validator->validate($ferme);
        self::assertViolationNotContainsMessage($violations, 'This value should not be blank.', 'siret');
    }

    public function testIsAdhesionActive3Yo(): void
    {
        $ferme = new Ferme();
        $ferme->addAnneeAdhesion($this->prophesizeFermeAdhesion(2007));

        self::assertFalse($ferme->isAdhesionActive());
    }

    /**
     * @dataProvider validAdhesionProvider
     */
    public function testIsAdhesionActiveValide(int $adhesion): void
    {
        $ferme = new Ferme();
        $ferme->addAnneeAdhesion($this->prophesizeFermeAdhesion(2007));
        $ferme->addAnneeAdhesion($this->prophesizeFermeAdhesion($adhesion));

        self::assertTrue($ferme->isAdhesionActive());
    }

    public function validAdhesionProvider()
    {
        return [
            [2010],
            [2009],
            [2008],
        ];
    }

    private function prophesizeFermeAdhesion(int $adhesion): FermeAnneeAdhesion
    {
        $adhesionProphecy = $this->prophesize(FermeAnneeAdhesion::class);
        $adhesionProphecy->getAnnee()->willReturn($adhesion);

        return $adhesionProphecy->reveal();
    }
}
