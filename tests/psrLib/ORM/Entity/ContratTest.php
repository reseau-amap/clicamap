<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratTest extends TestCase
{
    use ProphecyTrait;

    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    public function testIsCdpTrue(): void
    {
        $contrat = new Contrat();
        $contrat->setModeleContrat($this->prophetiseMcCdp(true));

        self::assertTrue($contrat->isCdp());
    }

    public function testIsCdpFalse(): void
    {
        $contrat = new Contrat();
        $contrat->setModeleContrat($this->prophetiseMcCdp(false));

        self::assertFalse($contrat->isCdp());
    }

    private function prophetiseMcCdp(bool $isCdp): ModeleContrat
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->isCdp()->willReturn($isCdp);

        return $mc->reveal();
    }

    public function testGetCellulesDatesEmpty(): void
    {
        $contrat = new Contrat();
        self::assertEmpty($contrat->getCellulesDate(Carbon::now()));
    }

    public function testGetCellulesDates(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDate(Carbon::create(2000, 1, 1)));
        $contrat->addCellule($this->buildCelluleDate(Carbon::create(2000, 1, 2)));

        $res = $contrat->getCellulesDate(Carbon::create(2000, 1, 2));
        self::assertCount(1, $res);
    }

    public function testGetCellulesDateTotalQty(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 1), 1));
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 2), 2));
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 1), 4));

        $res = $contrat->getCellulesDateTotalQty(Carbon::create(2000, 1, 1));
        self::assertSame(5.0, $res);
    }

    public function testGetCellulesDatesProduitEmpty(): void
    {
        $contrat = new Contrat();
        self::assertNull(
            $contrat->getCelluleDateProduit(
                $this->prophesize(ModeleContratProduit::class)->reveal(),
                $this->prophesize(ModeleContratDate::class)->reveal(),
            )
        );
    }

    public function testGetCellulesDatesProduit(): void
    {
        $p1 = $this->prophesize(ModeleContratProduit::class)->reveal();
        $p2 = $this->prophesize(ModeleContratProduit::class)->reveal();

        $d1 = $this->prophesize(ModeleContratDate::class)->reveal();
        $d2 = $this->prophesize(ModeleContratDate::class)->reveal();
        $d3 = $this->prophesize(ModeleContratDate::class)->reveal();

        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateProduit($p1, $d1));
        $contrat->addCellule($this->buildCelluleDateProduit($p1, $d2));
        $contrat->addCellule($this->buildCelluleDateProduit($p2, $d3));

        $res = $contrat->getCelluleDateProduit($p1, $d2);
        self::assertNotNull($res);
        self::assertSame($p1, $res->getModeleContratProduit());
        self::assertSame($d2, $res->getModeleContratDate());
    }

    public function testHasRegulPdsEmpty(): void
    {
        $contrat = new Contrat();
        self::assertFalse($contrat->hasRegulPds());
    }

    public function testHasRegulPdsFalse(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleProduitRegul(false));
        $contrat->addCellule($this->buildCelluleProduitRegul(false));
        self::assertFalse($contrat->hasRegulPds());
    }

    public function testHasRegulPdsTrue(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleProduitRegul(false));
        $contrat->addCellule($this->buildCelluleProduitRegul(true));
        self::assertTrue($contrat->hasRegulPds());
    }

    public function testTotalQtyByProductEmpty(): void
    {
        $contrat = new Contrat();
        self::assertSame(0.0, $contrat->totalQtyByProduct($this->prophesize(ModeleContratProduit::class)->reveal()));
    }

    public function testTotalQtyByProduct(): void
    {
        $mcp1 = $this->prophesize(ModeleContratProduit::class)->reveal();
        $mcp2 = $this->prophesize(ModeleContratProduit::class)->reveal();

        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleProduct($mcp1, 0.5));
        $contrat->addCellule($this->buildCelluleProduct($mcp1, 1));
        $contrat->addCellule($this->buildCelluleProduct($mcp2, 2));
        self::assertSame(1.5, $contrat->totalQtyByProduct($mcp1));
        self::assertSame(2.0, $contrat->totalQtyByProduct($mcp2));
    }

    public function testGetFirstLivraisonDateWithFermeDelaiEmpty(): void
    {
        $contrat = new Contrat();
        self::assertNull($contrat->getFirstLivraisonDateWithFermeDelai());
    }

    public function testGetFirstLivraisonDateWithFermeDelaiFirstDateNoFermeDelay(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 10), 1));
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(0));

        self::assertTrue(Carbon::create(2000, 1, 10)->eq($contrat->getFirstLivraisonDateWithFermeDelai()));
    }

    public function testGetFirstLivraisonDateWithFermeDelaiFirstDateFermeDelay(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 10), 1));
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(2));

        self::assertTrue(Carbon::create(2000, 1, 8)->eq($contrat->getFirstLivraisonDateWithFermeDelai()));
    }

    public function testGetFirstLivraisonDateWithFermeDelaiOrder(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 11), 1));
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 10), 1));
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(0));

        self::assertTrue(Carbon::create(2000, 1, 10)->eq($contrat->getFirstLivraisonDateWithFermeDelai()));
    }

    public function testGetFirstLivraisonDateWithFermeDelaiIgnoreNoQty(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 10), 0));
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 11), 1));
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(0));

        self::assertTrue(Carbon::create(2000, 1, 11)->eq($contrat->getFirstLivraisonDateWithFermeDelai()));
    }

    public function testGetFirstLivraisonDateWithFermeDelaiIgnoreNoQtySameDate(): void
    {
        $contrat = new Contrat();
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 11), 0));
        $contrat->addCellule($this->buildCelluleDateQty(Carbon::create(2000, 1, 11), 1));
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(0));

        self::assertTrue(Carbon::create(2000, 1, 11)->eq($contrat->getFirstLivraisonDateWithFermeDelai()));
    }

    /**
     * @dataProvider providerNowBeforeFirstLivraisonDateWithFermeDelai
     */
    public function testNowBeforeFirstLivraisonDateWithFermeDelai(Carbon $now, ?Carbon $date, bool $res): void
    {
        $contrat = new Contrat();
        $contrat->setModeleContrat($this->prophesizeMcFermeDelay(0));
        if (null !== $date) {
            $contrat->addCellule($this->buildCelluleDateQty($date, 1));
        }
        Carbon::setTestNow($now);

        self::assertSame($res, $contrat->isNowBeforeOrEqualFirstLivraisonDateWithFermeDelai());
    }

    public function providerNowBeforeFirstLivraisonDateWithFermeDelai()
    {
        return [
            [Carbon::parse('2000-01-01'), null, false],
            [Carbon::parse('2000-01-02'), Carbon::parse('2000-01-01'), false],
            [Carbon::parse('2000-01-01 10:00:00'), Carbon::parse('2000-01-01'), true],
            [Carbon::parse('2000-01-01'), Carbon::parse('2000-01-02'), true],
        ];
    }

    private function buildCelluleDateProduit(ModeleContratProduit $modeleContratProduit, ModeleContratDate $modeleContratDate)
    {
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratDate()->willReturn($modeleContratDate);
        $cellule->getModeleContratProduit()->willReturn($modeleContratProduit);
        $cellule->setContrat(Argument::any())->willReturn($cellule);

        return $cellule->reveal();
    }

    private function buildCelluleDate(Carbon $date)
    {
        $mcDate = $this->prophesize(ModeleContratDate::class);
        $mcDate->getDateLivraison()->willReturn($date);
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratDate()->willReturn($mcDate->reveal());
        $cellule->setContrat(Argument::any())->willReturn($cellule);

        return $cellule->reveal();
    }

    private function buildCelluleProduct(ModeleContratProduit $modeleContratProduit, float $qty)
    {
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratProduit()->willReturn($modeleContratProduit);
        $cellule->setContrat(Argument::any())->willReturn($cellule);
        $cellule->getQuantite()->willReturn($qty);

        return $cellule->reveal();
    }

    private function buildCelluleDateQty(Carbon $date, float $qty)
    {
        $mcDate = $this->prophesize(ModeleContratDate::class);
        $mcDate->getDateLivraison()->willReturn($date);
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratDate()->willReturn($mcDate->reveal());
        $cellule->setContrat(Argument::any())->willReturn($cellule);
        $cellule->getQuantite()->willReturn($qty);

        return $cellule->reveal();
    }

    private function buildCelluleProduitRegul(bool $regul)
    {
        $mcProduit = $this->prophesize(ModeleContratProduit::class);
        $mcProduit->getRegulPds()->willReturn($regul);
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratProduit()->willReturn($mcProduit->reveal());
        $cellule->setContrat(Argument::any())->willReturn($cellule);

        return $cellule->reveal();
    }

    private function prophesizeMcFermeDelay(int $delay): ModeleContrat
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getDelaiModifContrat()->willReturn($delay);

        return $mc->reveal();
    }
}
