<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use PsrLib\Enum\CampagneEtat;
use PsrLib\ORM\Entity\Campagne;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneEtatTest extends TestCase
{
    public function setUp(): void
    {
        Carbon::setTestNow(new Carbon('2000-06-15'));
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testEtatEmpty(): void
    {
        $campagne = new Campagne();

        self::expectException(\RuntimeException::class);
        self::expectExceptionMessage('Period not initialized');
        $campagne->getEtat();
    }

    public function testGetEtatBeforeStart(): void
    {
        $campagne = $this->buildCampagne(new Carbon('2000-06-16'), new Carbon('2000-06-17'));
        self::assertSame(CampagneEtat::ETAT_BROUILLON, $campagne->getEtat());
    }

    public function testGetEtatStart(): void
    {
        $campagne = $this->buildCampagne(new Carbon('2000-06-15'), new Carbon('2000-06-17'));
        self::assertSame(CampagneEtat::ETAT_ENCOURS, $campagne->getEtat());
    }

    public function testGetEtatDansPeriod(): void
    {
        $campagne = $this->buildCampagne(new Carbon('2000-06-14'), new Carbon('2000-06-16'));
        self::assertSame(CampagneEtat::ETAT_ENCOURS, $campagne->getEtat());
    }

    public function testGetEtatAfterEnd(): void
    {
        $campagne = $this->buildCampagne(new Carbon('2000-06-10'), new Carbon('2000-06-14'));
        self::assertSame(CampagneEtat::ETAT_TERMINE, $campagne->getEtat());
    }

    public function testGetEtatEnd(): void
    {
        $campagne = $this->buildCampagne(new Carbon('2000-06-10'), new Carbon('2000-06-15'));
        self::assertSame(CampagneEtat::ETAT_ENCOURS, $campagne->getEtat());
    }

    private function buildCampagne(Carbon $start, Carbon $end)
    {
        $campagne = new Campagne();
        $campagne->getPeriod()->setStartAt($start);
        $campagne->getPeriod()->setEndAt($end);

        return $campagne;
    }
}
