<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 *
 * @coversNothing
 */
abstract class ModeleContratAbstractTest extends ContainerAwareTestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    protected function buildModeleContratDate(\DateTime $dateTime)
    {
        $date = new ModeleContratDate();
        $date->setDateLivraison($dateTime);

        return $date;
    }
}
