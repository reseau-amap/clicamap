<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneMontant;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneCountMontantsLibreTest extends TestCase
{
    use ProphecyTrait;

    public function testCountEmpty(): void
    {
        $campagne = new Campagne();

        self::assertSame(0, $campagne->countMontantLibres());
    }

    public function testCount(): void
    {
        $campagne = new Campagne();
        $campagne->addMontant($this->prophesizeMontant(true));
        $campagne->addMontant($this->prophesizeMontant(false));
        $campagne->addMontant($this->prophesizeMontant(true));

        self::assertSame(2, $campagne->countMontantLibres());
    }

    private function prophesizeMontant(bool $montantLibre): CampagneMontant
    {
        $montant = $this->prophesize(CampagneMontant::class);
        $montant->isMontantLibre()->willReturn($montantLibre);
        $montant->setCampagne(Argument::any());

        return $montant->reveal();
    }
}
