<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapDistributionTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;

    public function testGetHeureDebut(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        self::assertTrue(Carbon::parse('2000-01-01 10:00:00')->eq($distribution->getDateDebut()));
    }

    public function testGetHeureFin(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        self::assertTrue(Carbon::parse('2000-01-01 11:00:00')->eq($distribution->getDateFin()));
    }

    public function testgetGetPlacesRestantesDefaut(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        self::assertSame(10, $distribution->getPlacesRestantes());
    }

    public function testgetGetPlacesRestantesinscrit(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        $distribution->addAmapien($this->prophesize(Amapien::class)->reveal());
        self::assertSame(9, $distribution->getPlacesRestantes());
    }

    public function testIsCompletedFalse(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        $distribution->addAmapien($this->prophesize(Amapien::class)->reveal());
        self::assertFalse($distribution->isCompleted());
    }

    public function testIsCompletedTrue(): void
    {
        $distribution = $this
            ->buildDistribution(
                new Carbon('2000-01-01'),
                new Time(10, 0),
                new Time(11, 0)
            )
        ;
        for ($i = 0; $i < 10; ++$i) {
            $distribution->addAmapien($this->prophesize(Amapien::class)->reveal());
        }
        self::assertTrue($distribution->isCompleted());
    }

    private function buildDistribution(?Carbon $date, ?Time $heureDebut, ?Time $heureFin)
    {
        return new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, 10, 'tache'),
            $this->prophesize(AmapLivraisonLieu::class)->reveal(),
            $date
        );
    }
}
