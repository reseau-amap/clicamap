<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\ORM\Entity;

use Prophecy\Argument;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class AdhesionFermeTest extends ContainerAwareTestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testFermeAccessNoPermission(): void
    {
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $value = $this->prophesize(AdhesionValueFerme::class)->reveal();
        $creator = $this->prophesize(User::class);
        $creator->__toString()->willReturn('amapien');
        $creator->isAdminOf(Argument::is($ferme))->willReturn(false);
        $adhesion = new AdhesionFerme($ferme, $value, $creator->reveal());
        $violations = $this->validator->validate($adhesion, null, 'import');

        self::assertCount(1, $violations);
        self::assertSame("Erreur d'accès à la ferme", $violations[0]->getMessage());
    }

    public function testFermeAccessPermission(): void
    {
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $value = $this->prophesize(AdhesionValueFerme::class)->reveal();
        $creator = $this->prophesize(User::class);
        $creator->isAdminOf(Argument::is($ferme))->willReturn(true);
        $adhesion = new AdhesionFerme($ferme, $value, $creator->reveal());
        $violations = $this->validator->validate($adhesion, null, 'import');

        self::assertCount(0, $violations);
    }
}
