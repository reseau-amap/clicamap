<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\LocalisableEntity;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;

/**
 * @internal
 *
 * @coversNothing
 */
class UserRoleTest extends TestCase
{
    use ProphecyTrait;

    public function testHasNoRight(): void
    {
        $user = new User();
        self::assertTrue($user->hasNoRights());
    }

    public function testUserSuperAdminDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isSuperAdmin());
        self::assertFalse($user->isAdmin());
    }

    public function testUserSuperAdminFalse(): void
    {
        $user = new User();
        $user->setSuperAdmin(false);
        self::assertFalse($user->isSuperAdmin());
        self::assertFalse($user->isAdmin());
    }

    public function testUserSuperAdminTrue(): void
    {
        $user = new User();
        $user->setSuperAdmin(true);
        self::assertTrue($user->isSuperAdmin());
        self::assertTrue($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsAdminRegionDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isAdminRegion());
        self::assertFalse($user->isAdmin());
    }

    public function testIsAdminRegion(): void
    {
        $user = new User();
        $user->addAdminRegion($this->prophesize(Region::class)->reveal());
        self::assertTrue($user->isAdminRegion());
        self::assertTrue($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsAdminDepartmentDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isAdminDepartment());
        self::assertFalse($user->isAdmin());
    }

    public function testIsAdminDepartment(): void
    {
        $user = new User();
        $user->addAdminDepartment($this->prophesize(Departement::class)->reveal());
        self::assertTrue($user->isAdminDepartment());
        self::assertTrue($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsPaysanDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isPaysan());
        self::assertFalse($user->isAdmin());
    }

    public function testIsPaysanTrue(): void
    {
        $user = new User();
        $user->setPaysan($this->prophesize(Paysan::class)->reveal());
        self::assertTrue($user->isPaysan());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsFermeRegroupementAdminDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isFermeRegroupementAdmin());
        self::assertFalse($user->isAdmin());
    }

    public function testIsFermeRegroupementAdmin(): void
    {
        $user = new User();

        $fermeRegroupement = $this->prophesize(FermeRegroupement::class);
        $fermeRegroupement->addAdmin($user)->shouldBeCalled();

        $user->addAdminFermeRegroupement($fermeRegroupement->reveal());
        self::assertTrue($user->isFermeRegroupementAdmin());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsAmapienDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isAmapien());
        self::assertFalse($user->isAdmin());
    }

    public function testIsAmapien(): void
    {
        $user = new User();
        $user->addAmapienAmap($this->prophesize(Amapien::class)->reveal());
        self::assertTrue($user->isAmapien());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsAmapAdminDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isAmapAdmin());
        self::assertFalse($user->isAdmin());
    }

    public function testIsAmapAdminTrue(): void
    {
        $user = new User();
        $user->addAdminAmap($this->prophesize(Amap::class)->reveal());
        self::assertTrue($user->isAmapAdmin());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsRefProduitDefault(): void
    {
        $user = new User();
        self::assertFalse($user->isRefProduit());
        self::assertFalse($user->isAdmin());
    }

    public function testIsRefProduitFalse(): void
    {
        $user = new User();
        $user->addAmapienAmap($this->prophesizeAmapienAmap(false));
        self::assertFalse($user->isRefProduit());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsRefProduitTrue(): void
    {
        $user = new User();
        $user->addAmapienAmap($this->prophesizeAmapienAmap(true));
        self::assertTrue($user->isRefProduit());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsRefProduitMixed(): void
    {
        $user = new User();
        $user->addAmapienAmap($this->prophesizeAmapienAmap(false));
        $user->addAmapienAmap($this->prophesizeAmapienAmap(true));
        self::assertTrue($user->isRefProduit());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsRefProduitFalseMultiple(): void
    {
        $user = new User();
        $user->addAmapienAmap($this->prophesizeAmapienAmap(false));
        $user->addAmapienAmap($this->prophesizeAmapienAmap(false));
        self::assertFalse($user->isAmapAdmin());
        self::assertFalse($user->isAdmin());
        self::assertFalse($user->hasNoRights());
    }

    public function testIsAdminOfNull(): void
    {
        $user = new User();
        self::assertFalse($user->isAdminOf(null));
    }

    public function testIsAdminOfSuperAdmin(): void
    {
        $user = new User();
        $user->setSuperAdmin(true);
        self::assertTrue($user->isAdminOf($this->prophesizeLocalizableEntity(null, null)));
    }

    public function testIsAdminOfAdminRegionTrue(): void
    {
        $region1 = $this->prophesizeRegion();
        $region2 = $this->prophesizeRegion();
        $user = new User();
        $user->addAdminRegion($region1);
        $user->addAdminRegion($region2);
        self::assertTrue($user->isAdminOf($this->prophesizeLocalizableEntity($region1, null)));
    }

    public function testIsAdminOfAdminRegionFalse(): void
    {
        $region1 = $this->prophesizeRegion();
        $region2 = $this->prophesizeRegion();
        $user = new User();
        $user->addAdminRegion($region1);
        self::assertFalse($user->isAdminOf($this->prophesizeLocalizableEntity($region2, null)));
    }

    public function testIsAdminOfAdminDepartementTrue(): void
    {
        $department1 = $this->prophesizeDepartement();
        $department2 = $this->prophesizeDepartement();
        $user = new User();
        $user->addAdminDepartment($department1);
        $user->addAdminDepartment($department2);
        self::assertTrue($user->isAdminOf($this->prophesizeLocalizableEntity(null, $department1)));
    }

    public function testIsAdminOfAdminDepartementFalse(): void
    {
        $departement1 = $this->prophesizeDepartement();
        $departement2 = $this->prophesizeDepartement();
        $user = new User();
        $user->addAdminDepartment($departement1);
        self::assertFalse($user->isAdminOf($this->prophesizeLocalizableEntity(null, $departement2)));
    }

    public function testIsAdminOfFalse(): void
    {
        $user = new User();
        self::assertFalse($user->isAdminOf($this->prophesizeLocalizableEntity(null, null)));
    }

    private function prophesizeLocalizableEntity(?Region $region, ?Departement $departement): LocalisableEntity
    {
        $entity = $this->prophesize(LocalisableEntity::class);
        $entity->getRegion()->willReturn($region);
        $entity->getDepartement()->willReturn($departement);

        return $entity->reveal();
    }

    private function prophesizeRegion(): Region
    {
        $entity = $this->prophesize(Region::class);

        return $entity->reveal();
    }

    private function prophesizeDepartement(): Departement
    {
        $entity = $this->prophesize(Departement::class);

        return $entity->reveal();
    }

    private function prophesizeAmapienAmap($refProduit): Amapien
    {
        $amapienAmap = $this->prophesize(Amapien::class);
        $amapienAmap->isRefProduit()->willReturn($refProduit);
        $amapienAmap->setUser(Argument::any());
        $amapienAmap->getAmap()->willReturn($this->prophesize(Amap::class)->reveal());

        return $amapienAmap->reveal();
    }
}
