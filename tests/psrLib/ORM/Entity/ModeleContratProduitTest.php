<?php

declare(strict_types=1);

namespace Test\ORM\Entity;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratProduitTest extends TestCase
{
    use ProphecyTrait;

    public function testHasExclusionForModeleContratDateReturnsTrueWhenExclusionExists(): void
    {
        $mcd = $this->prophesize(ModeleContratDate::class)->reveal();
        $exclusion = $this->prophesize(ModeleContratProduitExclure::class);
        $exclusion->getModeleContratDate()->willReturn($mcd);
        $exclusion->setModeleContratProduit(Argument::any())->willReturn($this->prophesize(ModeleContratProduitExclure::class)->reveal());

        $modeleContratProduit = new ModeleContratProduit();
        $modeleContratProduit->addExclusion($exclusion->reveal());

        $this->assertTrue($modeleContratProduit->hasExclusionForModeleContratDate($mcd));
    }

    public function testHasExclusionForModeleContratDateReturnsFalseWhenNoExclusionExists(): void
    {
        $mcd = $this->prophesize(ModeleContratDate::class)->reveal();

        $modeleContratProduit = new ModeleContratProduit();

        $this->assertFalse($modeleContratProduit->hasExclusionForModeleContratDate($mcd));
    }

    public function testHasExclusionForModeleContratDateReturnsFalseWhenExclusionDoesNotMatch(): void
    {
        $mcd1 = $this->prophesize(ModeleContratDate::class)->reveal();
        $mcd2 = $this->prophesize(ModeleContratDate::class)->reveal();
        $exclusion = $this->prophesize(ModeleContratProduitExclure::class);
        $exclusion->getModeleContratDate()->willReturn($mcd1);
        $exclusion->setModeleContratProduit(Argument::any())->willReturn($this->prophesize(ModeleContratProduitExclure::class)->reveal());

        $modeleContratProduit = new ModeleContratProduit();
        $modeleContratProduit->addExclusion($exclusion->reveal());

        $this->assertFalse($modeleContratProduit->hasExclusionForModeleContratDate($mcd2));
    }
}
