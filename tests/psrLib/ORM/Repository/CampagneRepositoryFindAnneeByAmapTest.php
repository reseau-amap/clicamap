<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Repository\CampagneRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneRepositoryFindAnneeByAmapTest extends RepositoryTestAbstract
{
    private CampagneRepository $campagneRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->campagneRepository = $this->em->getRepository(Campagne::class);
        Carbon::setTestNow('2000-01-15');
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testFindAnneeByAmapFilterAmap(): void
    {
        $amap1 = $this->getAmap('amap test');
        $amap2 = $this->getAmap('amap test 2');
        $this->storeCampagne(
            $amap1,
            2000,
        );
        $this->storeCampagne(
            $amap1,
            2001,
        );
        $this->storeCampagne(
            $amap2,
            2002,
        );

        $annees = $this->campagneRepository->findAnneeByAmap($amap1);
        self::assertSame([2000, 2001], $annees);
    }

    public function testFindAnneeByAmapDistinct(): void
    {
        $amap1 = $this->getAmap('amap test');
        $this->storeCampagne(
            $amap1,
            2000,
        );
        $this->storeCampagne(
            $amap1,
            2000,
        );

        $annees = $this->campagneRepository->findAnneeByAmap($amap1);
        self::assertSame([2000], $annees);
    }

    private function getAmap(string $name)
    {
        return $this->em->getRepository(Amap::class)->findOneBy(['nom' => $name]);
    }

    private function storeCampagne(Amap $amap, int $annee): Campagne
    {
        $campagne = new Campagne();
        $campagne->setAnneeAdhesionAmapien($annee);
        $campagne->setPaiementMethode('test');
        $campagne->setPaiementDescription('test');
        $campagne->setChampLibre('test');
        $campagne->setNom('nom');
        $campagne->setAmap($amap);
        $campagne->getPeriod()->setStartAt(new Carbon('2000-01-01'));
        $campagne->getPeriod()->setEndAt(new Carbon('2000-01-01'));
        $campagne->getAutheurInfo()->setNom('test');
        $campagne->getAutheurInfo()->setAdresse('test');
        $campagne->getAutheurInfo()->setTelephone('test');
        $campagne->getAutheurInfo()->setSite('test');
        $campagne->getAutheurInfo()->setVille('test');
        $campagne->getAutheurInfo()->setEmails('test');
        $campagne->getAutheurInfo()->setSiret('test');
        $campagne->getAutheurInfo()->setRna('test');
        $this->em->persist($campagne);
        $this->em->flush();

        return $campagne;
    }
}
