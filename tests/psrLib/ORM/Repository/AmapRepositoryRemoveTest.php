<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\AdhesionValueAmapAmapien;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAbsence;
use PsrLib\ORM\Entity\AmapCollectif;
use PsrLib\ORM\Entity\AmapienInvitation;
use PsrLib\ORM\Entity\AmapRecherchePaysan;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ContratLivraisonCellule;
use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapRepositoryRemoveTest extends AmapRepositoryTest
{
    /**
     * @var AmapRepository
     */
    private \Doctrine\ORM\EntityRepository $amapRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->amapRepo = $this->em->getRepository(Amap::class);
    }

    public function testRemoveWithAdhesionAmapAmapien(): void
    {
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $adhesionValue = new AdhesionValueAmapAmapien();
        $adhesionValue->setAmount(Money::EUR(100));
        $adhesionValue->setPaymentDate(new Carbon());
        $adhesionValue->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmapAmapien(
            $amap->getAmapiens()->first(),
            $adhesionValue,
            $amap
        );
        $this->em->persist($adhesion);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmapAmapien::class)
            ->findAll()
        ;
        self::assertCount(0, $adhesions);
    }

    public function testRemoveWithAdhesion(): void
    {
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $creator = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'admin_aura',
            ])
        ;
        $adhesionValue = new AdhesionValueAmap();
        $adhesionValue->setAmount(Money::EUR(100));
        $adhesionValue->setPaymentDate(new Carbon());
        $adhesionValue->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmap(
            $amap,
            $adhesionValue,
            $creator
        );
        $this->em->persist($adhesion);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        $adhesions = $this
            ->em
            ->getRepository(AdhesionAmap::class)
            ->findAll()
        ;
        self::assertCount(1, $adhesions);
        self::assertNull($adhesions[0]->getAmap());
    }

    public function testRemoveWithCollectif(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $user = $this->getUser('amapien');
        $collectif = new AmapCollectif($amap);
        $collectif->setAmapien($user->getUserAmapienAmapForAmap($amap));
        $this->em->persist($collectif);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
    }

    public function testRemoveWithContrat(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $contrat = $this->buildContratWithCellule($amap);
        $this->em->persist($contrat);
        $this->em->persist($contrat->getCellules()->first());
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
    }

    public function testRemoveWithContratDateReglement(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $user = $this->getUser('amapien');
        $contrat = new Contrat();
        $contrat->setAmapien($user->getUserAmapienAmapForAmap($amap));
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        $contrat->setModeleContrat($mc);
        $reglement = new ContratDatesReglement();
        $reglement->setMontant(Money::EUR(100));
        $contrat->addDatesReglement($reglement);
        $this->em->persist($contrat);
        $this->em->persist($reglement);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
    }

    public function testRemoveWithContratLivraison(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        /** @var Ferme $ferme */
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $contrat = $this->buildContratWithCellule($amap);
        $cellule = $contrat->getCellules()->first();

        $livraison = new ContratLivraison(
            $this->getUser('amapien')->getUserAmapienAmapForAmap($amap),
            $ferme,
            new Carbon()
        );
        $livraisonCellule = new ContratLivraisonCellule(
            $livraison,
            $cellule
        );
        $livraisonCellule->setQuantite(1);
        $cellule->setContratLivraisonCellule($livraisonCellule);

        $this->em->persist($contrat);
        $this->em->persist($cellule);
        $this->em->persist($livraison);
        $this->em->persist($livraisonCellule);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(ContratLivraisonCellule::class)
                ->findAll()
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(ContratLivraison::class)
                ->findAll()
        );
    }

    public function testRemoveWithRecherchePaysan(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $recherche = new AmapRecherchePaysan(
            $amap
        );

        $this->em->persist($recherche);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(AmapRecherchePaysan::class)
                ->findAll()
        );
    }

    public function testRemoveWithEvenementAbonnement(): void
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
        ;

        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $evenementAbonnement = new EvenementAbonnementAmap($user);
        $evenementAbonnement->setRelated($amap);

        $this->em->persist($evenementAbonnement);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(EvenementAbonnement::class)
                ->findAll()
        );
    }

    public function testRemoveWithAmapienInvitation(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $invitation = new AmapienInvitation(
            $this->getUser('amap'),
            $amap,
        );
        $this->em->persist($invitation);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(AmapienInvitation::class)
                ->findAll()
        );
    }

    public function testRemoveWithAmapienRefReseau(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $amap->setAmapienRefReseau($amap->getAmapiens()->first());
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
    }

    public function testRemoveWithAmapienRefReseauSec(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $amap->setAmapienRefReseauSecondaire($amap->getAmapiens()->first());
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
    }

    public function testRemoveWithAmapAbsence(): void
    {
        /** @var Amap $amap */
        $amap = $this
            ->amapRepo
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;

        $absence = new AmapAbsence($amap);
        $absence->setTitre('test');
        $absence->setAbsenceDebut(new Carbon());
        $absence->setAbsenceFin(new Carbon());
        $this->em->persist($absence);
        $this->em->flush();

        $this->amapRepo->remove($amap);
        $this->em->clear();

        self::assertNull(
            $this
                ->amapRepo
                ->findOneBy([
                    'email' => 'amap@test.amap-aura.org',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(AmapAbsence::class)
                ->findAll()
        );
    }

    private function getUser(string $username): User
    {
        return $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username,
            ])
        ;
    }

    private function buildContratWithCellule(Amap $amap): Contrat
    {
        $contrat = new Contrat();
        $contrat->setAmapien($amap->getAmapiens()->first());
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        $contrat->setModeleContrat($mc);
        $cellule = new ContratCellule();
        $cellule->setModeleContratProduit($mc->getProduits()[0]);
        $cellule->setModeleContratDate($mc->getDates()[0]);
        $contrat->addCellule($cellule);

        return $contrat;
    }
}
