<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @internal
 *
 * @coversNothing
 */
class UserAmapienAmapRepositoryTest extends RepositoryTestAbstract
{
    private AmapienRepository $userAmapienAmapRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->userAmapienAmapRepository = $this->em->getRepository(Amapien::class);
    }

    public function testDistributionAmapienAvailiable(): void
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapiens = $this->userAmapienAmapRepository->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(2, $amapiens);
        self::assertSame('amapien', $amapiens[0]->getUser()->getUsername());
        self::assertSame('amapienref', $amapiens[1]->getUser()->getUsername());
    }

    /**
     * @dataProvider providerAmapienSortByName
     */
    public function testqbDistributionAmapienAvailiableAmapienSorted(
        string $amapienNewFirstName,
        string $amapienNewLastName,
        string $amapienRefNewFirstName,
        string $amapienRefNewLastName,
        string $expectedFirstAmapienUsername,
        string $expectedSecondAmapienUsername
    ): void {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $this->renameAmapien('amapien', $amapienNewFirstName, $amapienNewLastName);
        $this->renameAmapien('amapienref', $amapienRefNewFirstName, $amapienRefNewLastName);
        $amapiens = $this->userAmapienAmapRepository->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(2, $amapiens);
        self::assertSame($expectedFirstAmapienUsername, $amapiens[0]->getUser()->getUsername());
        self::assertSame($expectedSecondAmapienUsername, $amapiens[1]->getUser()->getUsername());
    }

    private function renameAmapien(string $username, string $newFirstName, string $newLastName): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username,
            ])
        ;
        $amapien->getName()->setFirstName($newFirstName);
        $amapien->getName()->setLastName($newLastName);
        $this->em->flush();
    }

    public function testDistributionAmapienAvailiableInactive(): void
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amapien->setEtat(ActivableUserInterface::ETAT_INACTIF);
        $this->em->flush();

        $amapiens = $this->userAmapienAmapRepository->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(1, $amapiens);
        self::assertSame('amapienref', $amapiens[0]->getUser()->getUsername());
    }

    public function testDistributionAmapienAvailiableRecorded(): void
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $distribution = $this->recordDistribution($amap);

        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $distribution->addAmapien($amapien);
        $this->em->flush();

        $amapiens = $this->userAmapienAmapRepository->qbDistributionAmapienAvailiable($distribution)->getQuery()->getResult();
        self::assertCount(1, $amapiens);
        self::assertSame('amapienref', $amapiens[0]->getUser()->getUsername());
    }

    public function testQbActiveAmapienWithContratDateSuccess(): void
    {
        $date = new Carbon('2030-01-01');

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertCount(1, $res);
        self::assertSame('amapien', $res[0]->getUser()->getUsername());
    }

    public function testQbActiveAmapienWithContratDateNonValidatedContract(): void
    {
        $date = new Carbon('2030-01-01');

        $contract = $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $contract->setState(ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE);
        $this->em->flush();

        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertCount(0, $res);
    }

    public function testQbActiveAmapienWithContratDateEmptyDate(): void
    {
        $date = new Carbon('2030-01-01');

        $contrat = $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $this->em->refresh($contrat);
        foreach ($contrat->getCellules() as $cellule) {
            $cellule->setQuantite(0);
        }
        $this->em->flush();

        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertCount(0, $res);
    }

    public function testQbActiveAmapienWithContratDateInvalidAmap(): void
    {
        $date = new Carbon('2030-01-01');

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test 2',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertEmpty($res);
    }

    public function testQbActiveAmapienWithContratDateInvalidFerme(): void
    {
        $date = new Carbon('2030-01-01');

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'Ferme 2',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertEmpty($res);
    }

    public function testQbActiveAmapienWithContratDateInvalidDate(): void
    {
        $date = new Carbon('2030-01-01');

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'Ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                new Carbon('2030-01-02')
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertEmpty($res);
    }

    public function testQbActiveAmapienWithContratDateInvalidState(): void
    {
        $date = new Carbon('2030-01-01');

        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amapien->setEtat(ActivableUserInterface::ETAT_INACTIF);
        $this->em->flush();

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertEmpty($res);
    }

    /**
     * @dataProvider providerAmapienSortByName
     */
    public function testQbActiveAmapienWithContratDateSortAmapien(
        string $amapienNewFirstName,
        string $amapienNewLastName,
        string $amapienRefNewFirstName,
        string $amapienRefNewLastName,
        string $expectedFirstAmapienUsername,
        string $expectedSecondAmapienUsername
    ): void {
        $this->renameAmapien('amapien', $amapienNewFirstName, $amapienNewLastName);
        $this->renameAmapien('amapienref', $amapienRefNewFirstName, $amapienRefNewLastName);

        $date = new Carbon('2030-01-01');

        $this->recordContract(
            'contrat 1',
            'amapien',
            $date
        );
        $this->recordContract(
            'contrat 1',
            'amapienref',
            $date
        );
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;

        $res = $this
            ->userAmapienAmapRepository
            ->qbActiveAmapienWithContratValidatedDate(
                $amap,
                $ferme,
                $date
            )
            ->getQuery()
            ->getResult()
        ;
        self::assertCount(2, $res);
        self::assertSame($expectedFirstAmapienUsername, $res[0]->getUser()->getUsername());
        self::assertSame($expectedSecondAmapienUsername, $res[1]->getUser()->getUsername());
    }

    private function recordDistribution(Amap $amap)
    {
        $distribution = new AmapDistribution(
            AmapDistributionDetail::create(new Time(17, 0), new Time(18, 0), 10, 'test'),
            $amap->getLivraisonLieux()->first(),
            new Carbon('2000-01-01')
        );
        $this->em->persist($distribution);
        $this->em->flush();

        return $distribution;
    }

    private function recordContract(string $mcNom, string $amapienUsername, Carbon $date)
    {
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => $mcNom,
            ])
        ;
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $amapienUsername,
            ])
            ->getAmapienAmaps()
            ->first()
        ;

        $contrat = new Contrat();
        $contrat->setAmapien($amapien);
        $contrat->setModeleContrat($mc);
        $cellule = new ContratCellule();
        $cellule->setQuantite(1);
        $cellule->setModeleContratProduit($mc->getProduits()->first());
        $mcDate = $mc->getDates()->filter(fn (ModeleContratDate $mcDate) => $date->eq($mcDate->getDateLivraison()))->first();
        $cellule->setModeleContratDate($mcDate);
        $cellule->setContrat($contrat);

        $this->em->persist($cellule);
        $this->em->persist($contrat);

        $this->em->flush();

        return $contrat;
    }

    public function providerAmapienSortByName()
    {
        return [
            ['a', 'a', 'a', 'b', 'amapien', 'amapienref'],
            ['a', 'b', 'a', 'a', 'amapienref', 'amapien'],
            ['a', 'a', 'b', 'a', 'amapien', 'amapienref'],
            ['b', 'a', 'a', 'a', 'amapienref', 'amapien'],
        ];
    }
}
