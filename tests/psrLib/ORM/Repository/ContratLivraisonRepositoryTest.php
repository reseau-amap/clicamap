<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Repository\ContratLivraisonRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratLivraisonRepositoryTest extends RepositoryTestAbstract
{
    use ContratLivraisonTrait;

    /**
     * @var ContratLivraisonRepository
     */
    private \Doctrine\ORM\EntityRepository $contratLivraisonRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->contratLivraisonRepository = $this->em->getRepository(ContratLivraison::class);
    }

    public function testCountTotalDeliveredByMcpNoRegulFilterDate(): void
    {
        $contrat = $this->storeContract([
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-01'),
                1
            ),
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-08'),
                1
            ),
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-15'),
                1
            ),
        ]);

        $amapien = $contrat->getAmapien();
        $ferme = $contrat->getModeleContrat()->getFerme();
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-01'),
            true
        );
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-08'),
            true
        );

        self::assertSame(2.0, $this->contratLivraisonRepository->countTotalDeliveredByMcpNoRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }

    public function testCountTotalDeliveredByMcpNoRegulFilterProduct(): void
    {
        $contrat = $this->storeContract([
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-01'),
                1
            ),
            $this->buildCellule(
                'produit 2',
                Carbon::parse('2030-01-01'),
                1
            ),
        ]);

        $amapien = $contrat->getAmapien();
        $ferme = $contrat->getModeleContrat()->getFerme();
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-01'),
            true
        );

        self::assertSame(1.0, $this->contratLivraisonRepository->countTotalDeliveredByMcpNoRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }

    public function testCountTotalDeliveredByMcpNoRegulFilterQuantity(): void
    {
        $contrat = $this->storeContract([
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-01'),
                1
            ),
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-08'),
                0
            ),
        ]);

        $amapien = $contrat->getAmapien();
        $ferme = $contrat->getModeleContrat()->getFerme();
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-01'),
            true
        );
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-08'),
            true
        );

        self::assertSame(1.0, $this->contratLivraisonRepository->countTotalDeliveredByMcpNoRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }

    public function testCountTotalDeliveredByMcpNoRegulFilterDelivered(): void
    {
        $contrat = $this->storeContract([
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-01'),
                1
            ),
            $this->buildCellule(
                'produit 1',
                Carbon::parse('2030-01-08'),
                1
            ),
        ]);

        $amapien = $contrat->getAmapien();
        $ferme = $contrat->getModeleContrat()->getFerme();
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-01'),
            true
        );
        $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-08'),
            false
        );

        self::assertSame(1.0, $this->contratLivraisonRepository->countTotalDeliveredByMcpNoRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }
}
