<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmapAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmapien;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\UserRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapienRepositoryRemoveTest extends RepositoryTestAbstract
{
    private AmapienRepository $amapienRepo;
    private UserRepository $userRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->amapienRepo = $this->em->getRepository(Amapien::class);
        $this->userRepository = $this->em->getRepository(User::class);
    }

    public function testRemoveWithDistribution(): void
    {
        /** @var Amapien $amapien */
        $amapien = $this->userRepository->findOneBy([
            'username' => 'amapien',
        ])->getAmapienAmaps()->first();

        /** @var AmapDistribution $distribution */
        $distribution = $this
            ->em
            ->getRepository(AmapDistribution::class)
            ->findAll()[0]
        ;

        $distribution->addAmapien($amapien);
        $this->em->flush();

        $this->em->remove($amapien);
        $this->em->flush();
        $this->em->clear();

        self::assertFalse($this->userRepository->findOneBy([
            'username' => 'amapien',
        ])->getAmapienAmaps()->first());
        self::assertCount(
            1,
            $this
                ->em
                ->getRepository(AmapDistribution::class)
                ->findAll()
        );
    }

    public function testRemoveWithAdhesionAdmin(): void
    {
        $amapien_user = $this->userRepository->findOneBy([
            'username' => 'amapien',
        ]);
        $amapien = $amapien_user->getAmapienAmaps()->first();
        $creator = $this->userRepository->findOneBy([
            'username' => 'admin_aura',
        ]);
        $adhesionValue = new AdhesionValueAmapien();
        $adhesionValue->setAmount(Money::EUR(100));
        $adhesionValue->setPaymentDate(new Carbon());
        $adhesionValue->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmapien(
            $amapien_user,
            $adhesionValue,
            $creator
        );
        $this->em->persist($adhesion);
        $this->em->flush();

        $this->em->remove($amapien);
        $this->em->flush();
        $this->em->clear();

        self::assertFalse($this->userRepository->findOneBy([
            'username' => 'amapien',
        ])->getAmapienAmaps()->first());
        self::assertCount(
            1,
            $this
                ->em
                ->getRepository(AdhesionAmapien::class)
                ->findAll()
        );
    }

    public function testRemoveWithAdhesionAmap(): void
    {
        $amapien = $this->userRepository->findOneBy([
            'username' => 'amapien',
        ])->getAmapienAmaps()->first();
        $creator = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'email' => 'amap@test.amap-aura.org',
            ])
        ;
        $adhesionValue = new AdhesionValueAmapAmapien();
        $adhesionValue->setAmount(Money::EUR(100));
        $adhesionValue->setPaymentDate(new Carbon());
        $adhesionValue->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmapAmapien(
            $amapien,
            $adhesionValue,
            $creator
        );
        $this->em->persist($adhesion);
        $this->em->flush();

        $this->em->remove($amapien);
        $this->em->flush();
        $this->em->clear();

        self::assertFalse($this->userRepository->findOneBy([
            'username' => 'amapien',
        ])->getAmapienAmaps()->first());
        $adhesionFromDb = $this
            ->em
            ->getRepository(AdhesionAmapAmapien::class)
            ->findAll()
        ;
        self::assertCount(
            1,
            $adhesionFromDb
        );
    }
}
