<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Repository\AmapRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var AmapRepository
     */
    private \Doctrine\ORM\EntityRepository $amapRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->amapRepo = $this->em->getRepository(Amap::class);
    }

    public function testGetAmapsWithModeleContractValidatedEmpty(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $amaps = $this->amapRepo->getAmapsWithModeleContractValidated($ferme, new Carbon('2000-01-01'));
        self::assertEmpty($amaps);
    }

    public function testGetAmapsWithModeleContractValidatedBadDate(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $this->recordContract($ferme, new Carbon('2000-01-01'), ['ll amap test']);
        $amaps = $this->amapRepo->getAmapsWithModeleContractValidated($ferme, new Carbon('2000-01-02'));
        self::assertEmpty($amaps);
    }

    public function testGetAmapsWithModeleContractValidatedBadFerme(): void
    {
        $ferme1 = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $ferme2 = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'Ferme 2',
            ])
        ;
        $this->recordContract($ferme1, new Carbon('2000-01-01'), ['ll amap test']);
        $amaps = $this->amapRepo->getAmapsWithModeleContractValidated($ferme2, new Carbon('2000-01-01'));
        self::assertEmpty($amaps);
    }

    public function testGetAmapsWithModeleContractValidatedInvalidStates(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $this->recordContract($ferme, new Carbon('2000-01-01'), ['ll amap test'], ModeleContratEtat::ETAT_CREATION);
        $amaps = $this->amapRepo->getAmapsWithModeleContractValidated($ferme, new Carbon('2000-01-01'));
        self::assertEmpty($amaps);
    }

    public function testGetAmapsWithModeleContractValidatedSuccess(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $this->recordContract($ferme, new Carbon('2000-01-01'), ['ll amap test']);
        $amaps = $this->amapRepo->getAmapsWithModeleContractValidated($ferme, new Carbon('2000-01-01'));
        self::assertCount(1, $amaps);
        self::assertSame('amap@test.amap-aura.org', $amaps[0]->getEmail());
    }

    /**
     * @param string[] $llNoms
     */
    private function recordContract(Ferme $ferme, \DateTime $date, array $llNoms, ModeleContratEtat $etat = ModeleContratEtat::ETAT_VALIDE): void
    {
        foreach ($llNoms as $llNom) {
            $ll = $this
                ->em
                ->getRepository(AmapLivraisonLieu::class)
                ->findOneBy([
                    'nom' => $llNom,
                ])
            ;

            $mc = new ModeleContrat();
            $mc->setFerme($ferme);
            $mc->setReglementType(['type']);
            $mc->setLivraisonLieu($ll);
            $mc->setEtat($etat);
            $mc->setDelaiModifContrat(2);
            $this->em->persist($mc);

            $mcDate = new ModeleContratDate();
            $mcDate->setModeleContrat($mc);
            $mcDate->setDateLivraison($date);
            $this->em->persist($mcDate);
        }

        $this->em->flush();
    }
}
