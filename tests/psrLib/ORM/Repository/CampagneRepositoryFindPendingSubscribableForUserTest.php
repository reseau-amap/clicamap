<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\CampagneRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneRepositoryFindPendingSubscribableForUserTest extends RepositoryTestAbstract
{
    private CampagneRepository $campagneRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->campagneRepository = $this->em->getRepository(Campagne::class);
        Carbon::setTestNow('2000-01-15');
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testFindPendingSubscribableForUserFilterDate(): void
    {
        $campagne1 = $this->storeCampagne(
            'Campagne 1',
            $this->getAmap('amap test'),
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $campagne2 = $this->storeCampagne(
            'Campagne 2',
            $this->getAmap('amap test'),
            new Carbon('2000-01-10'),
            new Carbon('2000-01-15'),
        );
        $this->storeCampagne(
            'Campagne 3',
            $this->getAmap('amap test'),
            new Carbon('2000-01-01'),
            new Carbon('2000-01-14'),
        );
        $this->storeCampagne(
            'Campagne 4',
            $this->getAmap('amap test'),
            new Carbon('2000-01-16'),
            new Carbon('2000-01-17'),
        );

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($this->getUser('amapien'));
        self::assertCount(2, $campagnes);
        self::assertSame($campagne1, $campagnes[0]);
        self::assertSame($campagne2, $campagnes[1]);
    }

    public function testFindPendingSubscribableForUserFilterAmap(): void
    {
        $campagne1 = $this->storeCampagne(
            'Campagne 1',
            $this->getAmap('amap test'),
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $this->storeCampagne(
            'Campagne 2',
            $this->getAmap('amap test 2'),
            new Carbon('2000-01-10'),
            new Carbon('2000-01-15'),
        );

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($this->getUser('amapien'));
        self::assertCount(1, $campagnes);
        self::assertSame($campagne1, $campagnes[0]);
    }

    public function testFindPendingSubscribableForUserFilterVersion(): void
    {
        $campagne1 = $this->storeCampagne(
            'Campagne 1',
            $this->getAmap('amap test'),
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $campagne2 = $this->storeCampagne(
            'Campagne 2',
            $this->getAmap('amap test'),
            new Carbon('2000-01-10'),
            new Carbon('2000-01-15'),
        );
        $campagne1->setVersionSuivante($campagne2);
        $this->em->flush();

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($this->getUser('amapien'));
        self::assertCount(1, $campagnes);
        self::assertSame($campagne2, $campagnes[0]);
    }

    public function testFindPendingSubscribableForUserFilterBulletin(): void
    {
        $user = $this->getUser('amapien');
        $amap = $this->getAmap('amap test');
        $campagne1 = $this->storeCampagne(
            'Campagne 1',
            $amap,
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $bulletin = new CampagneBulletin();
        $bulletin->setCampagne($campagne1);
        $bulletin->setAmapien($amap->getAmapiens()->filter(fn (Amapien $amapien) => $amapien->getUser() === $user)->first());
        $bulletin->setPayeur('test');
        $bulletin->setPaiementDate(new Carbon('2000-01-15'));
        $this->em->persist($bulletin);
        $this->em->flush();

        $campagne2 = $this->storeCampagne(
            'Campagne 2',
            $amap,
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($user);
        self::assertCount(1, $campagnes);
        self::assertSame($campagne2, $campagnes[0]);
    }

    public function testFindPendingSubscribableForUserFilterBulletinOtherUser(): void
    {
        $user = $this->getUser('amapien');
        $amap = $this->getAmap('amap test');
        $campagne1 = $this->storeCampagne(
            'Campagne 1',
            $amap,
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $bulletin = new CampagneBulletin();
        $bulletin->setCampagne($campagne1);
        $bulletin->setAmapien($amap->getAmapiens()->filter(fn (Amapien $amapien) => $amapien->getUser() === $user)->first());
        $bulletin->setPayeur('test');
        $bulletin->setPaiementDate(new Carbon('2000-01-15'));
        $this->em->persist($bulletin);
        $this->em->flush();

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($this->getUser('amapienref'));
        self::assertCount(1, $campagnes);
        self::assertSame($campagne1, $campagnes[0]);
    }

    public function testFindPendingSubscribableForUserFilterBulletinVersion(): void
    {
        $user = $this->getUser('amapien');
        $amap = $this->getAmap('amap test');
        $campagne11 = $this->storeCampagne(
            'Campagne 11',
            $amap,
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $bulletin = new CampagneBulletin();
        $bulletin->setCampagne($campagne11);
        $bulletin->setAmapien($amap->getAmapiens()->filter(fn (Amapien $amapien) => $amapien->getUser() === $user)->first());
        $bulletin->setPayeur('test');
        $bulletin->setPaiementDate(new Carbon('2000-01-15'));
        $this->em->persist($bulletin);
        $this->em->flush();

        $campagne12 = $this->storeCampagne(
            'Campagne 12',
            $amap,
            new Carbon('2000-01-15'),
            new Carbon('2000-01-20'),
        );
        $campagne11->setVersionSuivante($campagne12);
        $this->em->flush();

        $campagnes = $this->campagneRepository->findPendingSubscribableForUser($user);
        self::assertEmpty($campagnes);
    }

    private function getUser(string $username)
    {
        return $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
    }

    private function getAmap(string $name)
    {
        return $this->em->getRepository(Amap::class)->findOneBy(['nom' => $name]);
    }

    private function storeCampagne(string $nom, Amap $amap, Carbon $startAt, Carbon $endAt): Campagne
    {
        $campagne = new Campagne();
        $campagne->setAnneeAdhesionAmapien(2000);
        $campagne->setPaiementMethode('test');
        $campagne->setPaiementDescription('test');
        $campagne->setChampLibre('test');
        $campagne->setNom($nom);
        $campagne->setAmap($amap);
        $campagne->getPeriod()->setStartAt($startAt);
        $campagne->getPeriod()->setEndAt($endAt);

        $campagne->getAutheurInfo()->setNom('test');
        $campagne->getAutheurInfo()->setAdresse('test');
        $campagne->getAutheurInfo()->setTelephone('test');
        $campagne->getAutheurInfo()->setSite('test');
        $campagne->getAutheurInfo()->setVille('test');
        $campagne->getAutheurInfo()->setEmails('test');
        $campagne->getAutheurInfo()->setSiret('test');
        $campagne->getAutheurInfo()->setRna('test');

        $this->em->persist($campagne);
        $this->em->flush();

        return $campagne;
    }
}
