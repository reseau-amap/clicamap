<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\UserRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class UserRepositoryReseauAdminFromAmapTest extends RepositoryTestAbstract
{
    private UserRepository $userRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->em->getRepository(User::class);
    }

    public function testGetReseauAdminFromAmapNoLivraisonLieu(): void
    {
        $amap = new Amap();

        $this->assertNull($this->userRepository->getReseauAdminFromAmap($amap));
    }

    public function testGetReseauAdminFromAmapAdminDep(): void
    {
        $ll = new AmapLivraisonLieu();
        $ll->setVille($this->getVille(69001));

        $amap = new Amap();
        $amap->addLivraisonLieux($ll);

        $dto = $this->userRepository->getReseauAdminFromAmap($amap);
        $this->assertSame('admin_rhone', $dto->getUser()->getUsername());
        $this->assertSame('RHÔNE', $dto->getLocation()->getNom());
    }

    public function testGetReseauAdminFromAmapAdminReg(): void
    {
        $ll = new AmapLivraisonLieu();
        $ll->setVille($this->getVille(26000));

        $amap = new Amap();
        $amap->addLivraisonLieux($ll);

        $dto = $this->userRepository->getReseauAdminFromAmap($amap);

        $this->assertSame('admin_aura', $dto->getUser()->getUsername());
        $this->assertSame('AUVERGNE-RHÔNE-ALPES', $dto->getLocation()->getNom());
    }

    public function testGetReseauAdminFromAmapNoAdmin(): void
    {
        $ll = new AmapLivraisonLieu();
        $ll->setVille($this->getVille(59200));

        $amap = new Amap();
        $amap->addLivraisonLieux($ll);

        $dto = $this->userRepository->getReseauAdminFromAmap($amap);

        $this->assertNull($dto);
    }

    private function getVille(int $cp): Ville
    {
        return $this
            ->em
            ->getRepository(Ville::class)
            ->findOneBy(['cp' => $cp])
        ;
    }
}
