<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Repository\ModeleContratRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var ModeleContratRepository
     */
    private \Doctrine\ORM\EntityRepository $modeleContratRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->modeleContratRepository = $this->em->getRepository(ModeleContrat::class);
    }

    public function testGetFromAmapMultiple(): void
    {
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;

        $contrats = $this
            ->modeleContratRepository
            ->getFromAmapMultipleByAmap([$amap])
        ;

        self::assertCount(1, $contrats);
        self::assertSame('contrat 1', $contrats[$amap->getId()][0]->getNom());
    }
}
