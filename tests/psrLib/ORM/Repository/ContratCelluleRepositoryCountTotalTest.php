<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\CarbonImmutable;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\ContratCelluleRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratCelluleRepositoryCountTotalTest extends RepositoryTestAbstract
{
    private ContratCelluleRepository $contratCelluleRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->contratCelluleRepository = $this->em->getRepository(ContratCellule::class);
    }

    public function tearDown(): void
    {
        CarbonImmutable::setTestNow();
    }

    private function getAmapien(string $username): Amapien
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username,
            ])
        ;

        return $user->getAmapienAmaps()->first();
    }

    private function subscribeContrat(Amapien $amapien, array $orders): Contrat
    {
        $mc = $this->em->getRepository(ModeleContrat::class)->findOneBy(['nom' => 'contrat 1']);
        $contrat = new Contrat();
        $contrat->setAmapien($amapien);
        $contrat->setModeleContrat($mc);
        $mcProduitRepo = $this->em->getRepository(ModeleContratProduit::class);
        $mcDateRepo = $this->em->getRepository(ModeleContratDate::class);

        foreach ($orders as $order) {
            $cellule = new ContratCellule();
            $cellule->setQuantite($order['qty']);
            $prod = $mcProduitRepo->findOneBy(['nom' => $order['produit'], 'modeleContrat' => $mc]);
            $date = $mcDateRepo->findOneBy(['dateLivraison' => $order['date'], 'modeleContrat' => $mc]);
            $cellule->setModeleContratDate($date);
            $cellule->setModeleContratProduit($prod);
            $cellule->setContrat($contrat);
            $this->em->persist($cellule);
        }
        $this->em->persist($contrat);
        $this->em->flush();

        return $contrat;
    }

    public function testCountEmpty(): void
    {
        self::assertSame(
            0,
            $this->contratCelluleRepository->countTotalForAmapienInFutureWithQtyPositive(
                $this->getAmapien('amapien')
            )
        );
    }

    public function testCountDateFilter(): void
    {
        CarbonImmutable::setTestNow('2030-01-08');
        $amapien = $this->getAmapien('amapien');
        $this->subscribeContrat($amapien, [
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-01'), 'qty' => 1],
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-08'), 'qty' => 2],
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-15'), 'qty' => 3],
        ]);

        self::assertSame(
            2,
            $this->contratCelluleRepository->countTotalForAmapienInFutureWithQtyPositive(
                $this->getAmapien('amapien')
            )
        );
    }

    public function testCountQtyFilter(): void
    {
        CarbonImmutable::setTestNow('2030-01-08');
        $amapien = $this->getAmapien('amapien');
        $this->subscribeContrat($amapien, [
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-01'), 'qty' => 1],
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-08'), 'qty' => 0],
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-15'), 'qty' => 0],
        ]);

        self::assertSame(
            0,
            $this->contratCelluleRepository->countTotalForAmapienInFutureWithQtyPositive(
                $this->getAmapien('amapien')
            )
        );
    }

    public function testCountAmapienFilter(): void
    {
        CarbonImmutable::setTestNow('2030-01-08');
        $amapien = $this->getAmapien('amapien');
        $amapienref = $this->getAmapien('amapienref');
        $this->subscribeContrat($amapien, [
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-15'), 'qty' => 3],
        ]);
        $this->subscribeContrat($amapienref, [
            ['produit' => 'produit 1', 'date' => new \DateTime('2030-01-15'), 'qty' => 3],
        ]);

        self::assertSame(
            1,
            $this->contratCelluleRepository->countTotalForAmapienInFutureWithQtyPositive(
                $this->getAmapien('amapien')
            )
        );
    }
}
