<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionAmapAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmapAmapien;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AdhesionAmapAmapienRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AdhesionAmapAmapienRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var AdhesionAmapAmapienRepository
     */
    private \Doctrine\ORM\EntityRepository $adhesionAmapAmapienRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->adhesionAmapAmapienRepository = $this->em->getRepository(AdhesionAmapAmapien::class);
    }

    public function testCountAdhesionsByAmapMultiple(): void
    {
        $amap1 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $amap2 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test 2',
            ])
        ;
        $amapien1 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
        ;
        $amapien2 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien2',
            ])
        ;

        $this->recordAdhesion($amap1, $amapien1->getUserAmapienAmapForAmap($amap1));
        $this->recordAdhesion($amap1, $amapien1->getUserAmapienAmapForAmap($amap1));
        $this->recordAdhesion($amap2, $amapien2->getUserAmapienAmapForAmap($amap2));
        $this->em->flush();

        $res = $this->adhesionAmapAmapienRepository->countAdhesionsByAmapMultiple([$amap1]);
        self::assertSame([
            $amap1->getId() => 2,
        ], $res);
    }

    private function recordAdhesion(Amap $amap, Amapien $amapien): void
    {
        $value = new AdhesionValueAmapAmapien();
        $value->setAmount(Money::EUR(0));
        $value->setPaymentDate(new Carbon());
        $value->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmapAmapien(
            $amapien,
            $value,
            $amap
        );
        $this->em->persist($adhesion);
    }
}
