<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use PsrLib\DTO\SearchDocumentUtilisateurState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\DocumentUtilisateur;
use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Repository\DocumentUtilisateurRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class DocumentUtilisateurRepositorySearchTest extends RepositoryTestAbstract
{
    private DocumentUtilisateurRepository $documentUtilisateurRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->documentUtilisateurRepository = $this->em->getRepository(DocumentUtilisateur::class);
    }

    public function testEmpty(): void
    {
        $this->storeDocumentUtilisateurAmap(
            $this->getAmapByName('amap test'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testAmapFilterByRegion(): void
    {
        $this->storeDocumentUtilisateurAmap(
            $this->getAmapByName('amap test'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setRegion($this->getRegion('BOURGOGNE-FRANCHE-COMTÉ'));
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testFermeFilterByRegion(): void
    {
        $this->storeDocumentUtilisateurFerme(
            $this->getFermeByName('ferme'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setRegion($this->getRegion('BOURGOGNE-FRANCHE-COMTÉ'));
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testAmapFilterDepartement(): void
    {
        $this->storeDocumentUtilisateurAmap(
            $this->getAmapByName('amap test'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setDepartement($this->getDepartement('RHÔNE'));
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setDepartement($this->getDepartement('AIN'));
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testFermeFilterDepartement(): void
    {
        $this->storeDocumentUtilisateurFerme(
            $this->getFermeByName('ferme'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setDepartement($this->getDepartement('RHÔNE'));
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setDepartement($this->getDepartement('AIN'));
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testFermeFilterYear(): void
    {
        $this->storeDocumentUtilisateurFerme(
            $this->getFermeByName('ferme'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setYear(2020);
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setYear(2019);
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testAmapFilterKeywordDocumentNom(): void
    {
        $this->storeDocumentUtilisateurAmap(
            $this->getAmapByName('amap test'),
            'nom du document',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setKeyword('nom');
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setKeyword('invalid');
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testAmapFilterKeywordAmap(): void
    {
        $this->storeDocumentUtilisateurAmap(
            $this->getAmapByName('amap test'),
            'nom du document',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setKeyword('amap');
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setKeyword('invalid');
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    public function testFermeFilterKeywordFerme(): void
    {
        $this->storeDocumentUtilisateurFerme(
            $this->getFermeByName('ferme'),
            'test',
            2020
        );

        $search = new SearchDocumentUtilisateurState();
        $search->setKeyword('ferme');
        self::assertCount(1, $this->documentUtilisateurRepository->search($search));

        $search->setKeyword('invalid');
        self::assertCount(0, $this->documentUtilisateurRepository->search($search));
    }

    private function getRegion(string $name): Region
    {
        return $this
            ->em
            ->getRepository(Region::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }

    private function getDepartement(string $name): Departement
    {
        return $this
            ->em
            ->getRepository(Departement::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }

    private function getAmapByName(string $name)
    {
        return $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy(['nom' => $name])
        ;
    }

    private function getFermeByName(string $name)
    {
        return $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy(['nom' => $name])
        ;
    }

    private function storeDocumentUtilisateurAmap(Amap $amap, string $nom, int $annee): DocumentUtilisateurAmap
    {
        $fichier = new \PsrLib\ORM\Entity\Files\DocumentUtilisateur('test', 'test');
        $documentUtilisateur = new DocumentUtilisateurAmap(
            $annee,
            $nom,
            $fichier,
            $amap
        );
        $this->em->persist($documentUtilisateur);
        $this->em->flush();

        return $documentUtilisateur;
    }

    private function storeDocumentUtilisateurFerme(Ferme $ferme, string $nom, int $annee): DocumentUtilisateurFerme
    {
        $fichier = new \PsrLib\ORM\Entity\Files\DocumentUtilisateur('test', 'test');
        $documentUtilisateur = new DocumentUtilisateurFerme(
            $annee,
            $nom,
            $fichier,
            $ferme
        );
        $this->em->persist($documentUtilisateur);
        $this->em->flush();

        return $documentUtilisateur;
    }
}
