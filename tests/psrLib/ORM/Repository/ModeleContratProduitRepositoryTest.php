<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\ModeleContratProduitRepository;
use PsrLib\Workflow\ContratStatusWorkflow;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratProduitRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var ModeleContratProduitRepository
     */
    private \Doctrine\ORM\EntityRepository $modeleContratProduitRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->modeleContratProduitRepository = $this->em->getRepository(ModeleContratProduit::class);
    }

    public function testTotalByProductFromMcDateEmpty(): void
    {
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        self::assertEmpty($this->modeleContratProduitRepository->totalByProductFromMcDateContractValidated($mc, new Carbon('2000-01-01')));
    }

    public function testTotalByProductFromMcDate(): void
    {
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $user1 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
        ;
        $user2 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapienref',
            ])
        ;

        $date1 = $mc->getDates()[0];
        $date2 = $mc->getDates()[1];

        $product1 = $mc->getProduits()[0];
        $product2 = $mc->getProduits()[1];

        $contrat1 = new Contrat();
        $contrat1->setModeleContrat($mc);
        $contrat1->setAmapien($user1->getUserAmapienAmapForAmap($amap));
        $this->em->persist($contrat1);

        $this->buildCellulle(
            $contrat1,
            $date1,
            $product1,
            1
        );
        $this->buildCellulle(
            $contrat1,
            $date1,
            $product2,
            2
        );
        $this->buildCellulle(
            $contrat1,
            $date2,
            $product1,
            1
        );
        $this->buildCellulle(
            $contrat1,
            $date2,
            $product2,
            2
        );

        $contrat2 = new Contrat();
        $contrat2->setModeleContrat($mc);
        $contrat2->setAmapien($user2->getUserAmapienAmapForAmap($amap));
        $this->em->persist($contrat2);

        $this->buildCellulle(
            $contrat2,
            $date1,
            $product1,
            3
        );
        $this->buildCellulle(
            $contrat2,
            $date2,
            $product1,
            2
        );
        $this->buildCellulle(
            $contrat2,
            $date1,
            $product2,
            4.5
        );
        $this->buildCellulle(
            $contrat2,
            $date2,
            $product2,
            3.5
        );

        $this->em->flush();

        $res = $this
            ->modeleContratProduitRepository
            ->totalByProductFromMcDateContractValidated($mc, $date1->getDateLivraison())
        ;
        self::assertCount(2, $res);
        self::assertSame($product1, $res[0]['product']);
        self::assertSame($product2, $res[1]['product']);
        self::assertSame(4.0, $res[0]['qty']);
        self::assertSame(6.5, $res[1]['qty']);
    }

    public function testTotalByProductFromMcDateContractValidated(): void
    {
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        $amap = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $user1 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
        ;
        $user2 = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapienref',
            ])
        ;

        $date1 = $mc->getDates()[0];

        $product1 = $mc->getProduits()[0];

        $contrat1 = new Contrat();
        $contrat1->setState(ContratStatusWorkflow::STATE_AMAPIEN_TO_VALIDATE);
        $contrat1->setModeleContrat($mc);
        $contrat1->setAmapien($user1->getUserAmapienAmapForAmap($amap));
        $this->em->persist($contrat1);

        $this->buildCellulle(
            $contrat1,
            $date1,
            $product1,
            1
        );

        $contrat2 = new Contrat();
        $contrat2->setModeleContrat($mc);
        $contrat2->setAmapien($user2->getUserAmapienAmapForAmap($amap));
        $this->em->persist($contrat2);

        $this->buildCellulle(
            $contrat2,
            $date1,
            $product1,
            3
        );

        $this->em->flush();

        $res = $this
            ->modeleContratProduitRepository
            ->totalByProductFromMcDateContractValidated($mc, $date1->getDateLivraison())
        ;
        self::assertCount(1, $res);
        self::assertSame($product1, $res[0]['product']);
        self::assertSame(3.0, $res[0]['qty']);
    }

    private function buildCellulle(Contrat $contrat, ModeleContratDate $date, ModeleContratProduit $produit, float $qty)
    {
        $c = new ContratCellule();
        $c->setContrat($contrat);
        $c->setModeleContratDate($date);
        $c->setModeleContratProduit($produit);
        $c->setQuantite($qty);
        $this->em->persist($c);

        return $c;
    }
}
