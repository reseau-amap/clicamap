<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\DTO\SearchAdhesionAmapState;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;

/**
 * @internal
 *
 * @coversNothing
 */
class AdhesionAmapRepositoryTest extends RepositoryTestAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    /**
     * @var AdhesionAmapRepositoryTest
     */
    private \Doctrine\ORM\EntityRepository $adhesionAmapAmapienRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->adhesionAmapAmapienRepository = $this->em->getRepository(AdhesionAmap::class);
    }

    public function testListNotDeletedCreator(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesion(
            $creator,
            $this->getAmap('amap test')
        );
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));

        self::assertCount(
            1,
            $this->adhesionAmapAmapienRepository->search($state, $creator)
        );
    }

    public function testListNotDeletedDiffrentCreator(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesion(
            $creator,
            $this->getAmap('amap test')
        );
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));

        self::assertCount(
            0,
            $this->adhesionAmapAmapienRepository->search(
                $state,
                $this->getUser('admin_rhone')
            )
        );
    }

    public function testListNotDeletedDiffrentSuperAdmin(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesion(
            $creator,
            $this->getAmap('amap test')
        );
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));

        self::assertCount(
            1,
            $this->adhesionAmapAmapienRepository->search(
                $state,
                $this->getUser('admin')
            )
        );
    }

    public function testListDeletedNotCheckedCreator(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesionDeleted($creator);
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));

        self::assertCount(
            0,
            $this->adhesionAmapAmapienRepository->search($state, $creator)
        );
    }

    public function testListDeletedCheckedCreator(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesionDeleted($creator);
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));
        $state->setAmapSupprimee(true);

        self::assertCount(
            1,
            $this->adhesionAmapAmapienRepository->search($state, $creator)
        );
    }

    public function testListDeletedCheckedNotCreator(): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesionDeleted($creator);
        $state = new SearchAdhesionAmapState();
        $state->setRegion($this->getRegion('AUVERGNE-RHÔNE-ALPES'));
        $state->setAmapSupprimee(true);

        self::assertCount(
            0,
            $this->adhesionAmapAmapienRepository->search(
                $state,
                $this->getUser('admin_rhone')
            )
        );
    }

    private function getAmap(string $name): Amap
    {
        return $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }

    private function getUser(string $email): User
    {
        return $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $email,
            ])
        ;
    }

    private function getRegion(string $name): Region
    {
        return $this
            ->em
            ->getRepository(Region::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }

    private function recordAdhesion(User $creator, Amap $amap): void
    {
        $value = new AdhesionValueAmap();
        $value->setAmount(Money::EUR(0));
        $value->setPaymentDate(new Carbon());
        $value->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmap(
            $amap,
            $value,
            $creator
        );
        $this->em->persist($adhesion);
        $this->em->flush();
    }

    private function recordAdhesionDeleted(User $creator): void
    {
        $value = new AdhesionValueAmap();
        $value->setAmount(Money::EUR(0));
        $value->setPaymentDate(new Carbon());
        $value->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmap(
            $this->prophesize(Amap::class)->reveal(),
            $value,
            $creator
        );
        $adhesion->setAmap(null);
        $this->em->persist($adhesion);
        $this->em->flush();
    }
}
