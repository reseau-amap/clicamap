<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\ORM\Repository\TypeProductionRepository;
use Test\DatabaseTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class TypeProductionRepositoryTest extends RepositoryTestAbstract
{
    use DatabaseTrait;

    /**
     * @var TypeProductionRepository
     */
    private \Doctrine\ORM\EntityRepository $tpRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->tpRepo = $this->em->getRepository(TypeProduction::class);
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testGetActiveTpsFilterAmap(): void
    {
        $tps = $this->tpRepo->getActiveTpsFromAmap($this->getAmap('amap test'));
        self::assertCount(2, $tps);
        self::assertSame('Autres / divers', $tps[0]->getNom());
        self::assertSame('Céréales et légumineuses', $tps[1]->getNom());

        $tps = $this->tpRepo->getActiveTpsFromAmap($this->getAmap('amap test 2'));
        self::assertCount(0, $tps);
    }

    public function testGetActiveTpsFilterDate(): void
    {
        $amap = $this->getAmap('amap test');

        $lastDate = CarbonImmutable::create(2030, 4, 30);
        Carbon::setTestNow($lastDate);

        $tps = $this->tpRepo->getActiveTpsFromAmap($amap);
        self::assertCount(2, $tps);

        Carbon::setTestNow($lastDate->addYear());
        $tps = $this->tpRepo->getActiveTpsFromAmap($amap);
        self::assertCount(2, $tps);

        Carbon::setTestNow($lastDate->addYear()->addDay());
        $tps = $this->tpRepo->getActiveTpsFromAmap($amap);
        self::assertCount(0, $tps);
    }

    private function getAmap(string $name): Amap
    {
        return $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }
}
