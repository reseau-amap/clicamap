<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\ContratLivraisonCellule;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratLivraisonCelluleRepositoryTest extends RepositoryTestAbstract
{
    use ContratLivraisonTrait;

    /**
     * @var ContratLivraisonCellule
     */
    private \Doctrine\ORM\EntityRepository $contratLivraisonCellule;

    public function setUp(): void
    {
        parent::setUp();
        $this->contratLivraisonCellule = $this->em->getRepository(ContratLivraisonCellule::class);

        $p1 = $this->getMcpByName('produit 1');
        $p1->setRegulPds(true);
        $this->em->flush();
    }

    public function testEmpty(): void
    {
        $contrat = $this->storeContract([]);
        self::assertSame(0.0, $this->contratLivraisonCellule->countTotalDeliveredByMcpRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }

    public function testCountTotalDeliveredByMcpRegul(): void
    {
        $c1 = $this->buildCellule(
            'produit 1',
            Carbon::parse('2030-01-01'),
            1
        );
        $c2 = $this->buildCellule(
            'produit 1',
            Carbon::parse('2030-01-08'),
            1
        );
        $c3 = $this->buildCellule(
            'produit 2',
            Carbon::parse('2030-01-08'),
            1
        );
        $contrat = $this->storeContract([$c1, $c2, $c3]);

        $amapien = $contrat->getAmapien();
        $ferme = $contrat->getModeleContrat()->getFerme();
        $liv = $this->storeLivraisons(
            $amapien,
            $ferme,
            Carbon::parse('2030-01-01'),
            true
        );
        $this->storeCelluleLivraison($liv, $c1, 1);
        $this->storeCelluleLivraison($liv, $c2, 0.5);
        $this->storeCelluleLivraison($liv, $c3, 0.7);

        self::assertSame(1.5, $this->contratLivraisonCellule->countTotalDeliveredByMcpRegul(
            $contrat,
            $this->getMcpByName('produit 1')
        ));
    }

    private function storeCelluleLivraison(ContratLivraison $contratLivraison, ContratCellule $cellule, float $qty): void
    {
        $cliv = new ContratLivraisonCellule($contratLivraison, $cellule);
        $cliv->setQuantite($qty);
        $this->em->persist($cliv);
        $this->em->flush();
    }
}
