<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapRepositoryQbEvenementCreationForUserTest extends RepositoryTestAbstract
{
    /**
     * @var AmapRepository
     */
    private \Doctrine\ORM\EntityRepository $amapRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->amapRepository = $this->em->getRepository(Amap::class);
    }

    /**
     * @dataProvider providerUsers
     */
    public function testQbEvenementCreationForUser(string $user, array $expectedAmaps): void
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $user,
            ])
        ;
        $resAmaps = $this
            ->amapRepository
            ->qbEvenementCreationForUser($user)
            ->getQuery()
            ->getResult()
        ;

        $resAmapNames = array_map(fn (Amap $amap) => $amap->getNom(), $resAmaps);
        self::assertSame($expectedAmaps, $resAmapNames);
    }

    public function providerUsers()
    {
        return [
            ['empty', []],
            ['admin', ['amap test', 'amap test 2']],
            ['admin_aura', ['amap test', 'amap test 2']],
            ['admin_rhone', ['amap test', 'amap test 2']],
            ['paysan', []],
            ['admin_idf', []],
            ['admin_paris', []],
            ['amapien', []],
            ['amapienref', ['amap test']],
            ['amap', ['amap test']],
            ['amap2', ['amap test 2']],
            ['paysan', []],
        ];
    }
}
