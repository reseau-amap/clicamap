<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionAmapien;
use PsrLib\ORM\Entity\AdhesionValueAmapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AdhesionAmapienRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AdhesionAmapienRepositoryTest extends RepositoryTestAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    /**
     * @var AdhesionAmapienRepository
     */
    private \Doctrine\ORM\EntityRepository $adhesionAmapienRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->adhesionAmapienRepository = $this->em->getRepository(AdhesionAmapien::class);
    }

    /**
     * @dataProvider listCreatorRegionAdminProvider
     */
    public function testListCreatorRegionAdmin(string $searchRegion, ?string $searchDepartement, int $expectedCount): void
    {
        $creator = $this->getUser('admin_aura');
        $this->recordAdhesion(
            $creator,
            $this->getUser('amapien')
        );

        self::assertCount(
            $expectedCount,
            $this->adhesionAmapienRepository->search(
                $this->getRegion($searchRegion),
                null === $searchDepartement ? null : $this->getDepartement($searchDepartement),
                null,
                '',
                null
            )
        );
    }

    public function listCreatorRegionAdminProvider()
    {
        return [
            ['AUVERGNE-RHÔNE-ALPES', null, 1],
            ['AUVERGNE-RHÔNE-ALPES', 'AIN', 0],
            ['BRETAGNE', null, 0],
        ];
    }

    /**
     * @dataProvider listCreatorDepartementAdminProvider
     */
    public function testListCreatorDepartementAdmin(string $searchRegion, ?string $searchDepartement, int $expectedCount): void
    {
        $creator = $this->getUser('admin_rhone');
        $this->recordAdhesion(
            $creator,
            $this->getUser('amapien')
        );

        self::assertCount(
            $expectedCount,
            $this->adhesionAmapienRepository->search(
                $this->getRegion($searchRegion),
                null === $searchDepartement ? null : $this->getDepartement($searchDepartement),
                null,
                '',
                null
            )
        );
    }

    public function listCreatorDepartementAdminProvider()
    {
        return [
            ['AUVERGNE-RHÔNE-ALPES', null, 1],
            ['AUVERGNE-RHÔNE-ALPES', 'AIN', 0],
            ['AUVERGNE-RHÔNE-ALPES', 'RHÔNE', 1],
            ['BRETAGNE', null, 0],
        ];
    }

    private function recordAdhesion(User $creator, User $target): void
    {
        $value = new AdhesionValueAmapien();
        $value->setAmount(Money::EUR(0));
        $value->setPaymentDate(new Carbon());
        $value->setProcessingDate(new Carbon());
        $adhesion = new AdhesionAmapien(
            $target,
            $value,
            $creator
        );
        $this->em->persist($adhesion);
        $this->em->flush();
    }

    private function getUser(string $username)
    {
        return $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $username,
            ])
        ;
    }

    private function getRegion(string $name): Region
    {
        return $this
            ->em
            ->getRepository(Region::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }

    private function getDepartement(string $name): Departement
    {
        return $this
            ->em
            ->getRepository(Departement::class)
            ->findOneBy([
                'nom' => $name,
            ])
        ;
    }
}
