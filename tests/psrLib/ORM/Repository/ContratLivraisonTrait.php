<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ContratLivraison;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\User;

trait ContratLivraisonTrait
{
    private function storeLivraisons(Amapien $amapien, Ferme $ferme, Carbon $date, bool $livre)
    {
        $liv = new ContratLivraison(
            $amapien,
            $ferme,
            $date
        );
        $liv->setLivre($livre);
        $this->em->persist($liv);
        $this->em->flush();

        return $liv;
    }

    private function getMcpByName(string $productName): ModeleContratProduit
    {
        return $this
            ->em
            ->getRepository(ModeleContratProduit::class)
            ->findOneBy([
                'nom' => $productName,
            ])
        ;
    }

    /**
     * @param ContratCellule[] $cellules
     *
     * @return Contrat
     */
    private function storeContract(array $cellules)
    {
        $mc = $this
            ->em
            ->getRepository(ModeleContrat::class)
            ->findOneBy([
                'nom' => 'contrat 1',
            ])
        ;
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;

        $contrat = new Contrat();
        $contrat->setModeleContrat($mc);
        $contrat->setAmapien($amapien);
        foreach ($cellules as $cellule) {
            $contrat->addCellule($cellule);
        }

        $this->em->persist($contrat);
        $this->em->flush();

        return $contrat;
    }

    private function buildCellule(string $productName, \DateTime $date, float $quantity)
    {
        $cellule = new ContratCellule();
        $cellule->setQuantite($quantity);
        $cellule->setModeleContratProduit(
            $this->getMcpByName($productName)
        );
        $cellule->setModeleContratDate(
            $this
                ->em
                ->getRepository(ModeleContratDate::class)
                ->findOneBy([
                    'dateLivraison' => $date,
                ])
        );
        $this->em->persist($cellule);

        return $cellule;
    }
}
