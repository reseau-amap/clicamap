<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Test\ContainerAwareTestCase;
use Test\DatabaseTrait;

abstract class RepositoryTestAbstract extends ContainerAwareTestCase
{
    use DatabaseTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function setUp(): void
    {
        parent::setUp();
        $this->em = self::$container->get(EntityManagerInterface::class);
        $this->initDb();
        $this->em->clear();
    }
}
