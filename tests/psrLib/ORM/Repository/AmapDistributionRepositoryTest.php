<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\DTO\SearchDistributionAmapienState;
use PsrLib\DTO\SearchDistributionAmapState;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\Period;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapDistributionRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapDistributionRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var AmapDistributionRepository
     */
    private \Doctrine\ORM\EntityRepository $amapDistributionRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->amapDistributionRepo = $this->em->getRepository(AmapDistribution::class);
    }

    public function testSearchAmapienStatePeriodIncluded(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 10);

        $searchState = new SearchDistributionAmapienState($amapien->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testSearchAmapienStatePeriodIncludedMine(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded1 = $this->recordDistribution($amap, new Carbon('2000-01-15'), 10);
        $distributionRecorded1->addAmapien($amapien);
        $this->em->flush();
        $this->recordDistribution($amap, new Carbon('2000-01-16'), 10);

        $searchState = new SearchDistributionAmapienState($amapien->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setFilter(SearchDistributionAmapienState::FILTER_MINE);
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded1, $distributionRequesteds[0]);
    }

    public function testSearchAmapStatePeriodIncluded(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 10);

        $searchState = new SearchDistributionAmapState();
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $distributionRequesteds = $this->amapDistributionRepo->search($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testSearchAmapStatePeriodStartLimit(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 10);

        $searchState = new SearchDistributionAmapState();
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-15'),
                new Carbon('2000-01-30')
            )
        );
        $distributionRequesteds = $this->amapDistributionRepo->search($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testSearchAmapStatePeriodEndLimit(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 10);

        $searchState = new SearchDistributionAmapState();
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-15')
            )
        );
        $distributionRequesteds = $this->amapDistributionRepo->search($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testSearchAmapStatePeriodIncludedDefaultHideCompleted(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapState();
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $distributionRequesteds = $this->amapDistributionRepo->search($searchState);

        self::assertEmpty($distributionRequesteds);
    }

    public function testSearchAmapStateCurrentMonthShowCompleted(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapState();
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setComplete(true);
        $distributionRequesteds = $this->amapDistributionRepo->search($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testGetDistributionInDaysFromNow(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);

        Carbon::setTestNow('2000-01-10');
        $distributionRequesteds = $this->amapDistributionRepo->getDistributionInDaysFromNow(5);
        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded->getId(), $distributionRequesteds[0]->getId());
        Carbon::setTestNow();
    }

    public function testGetLastDistributionByAmap(): void
    {
        $amap1 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $this->recordDistribution($amap1, new Carbon('2000-01-01'), 10);

        $amap2 = new Amap();
        $amap2->setEmail('amap2@test.amap-aura.org');
        $amap2->setPossedeAssurance(true);
        $amap2->setAmapEtudiante(true);
        $amap2ll = new AmapLivraisonLieu();
        $amap2ll->setAmap($amap2);
        $amap2->addLivraisonLieux($amap2ll);
        $this->em->persist($amap2);
        $this->em->persist($amap2ll);
        $this->em->flush();

        $this->recordDistribution($amap2, new Carbon('2000-01-01'), 10);
        $this->recordDistribution($amap2, new Carbon('2000-01-01'), 10);

        $amap3 = new Amap();
        $amap3->setEmail('amap3@test.amap-aura.org');
        $amap3->setPossedeAssurance(true);
        $amap3->setAmapEtudiante(true);
        $amap3ll = new AmapLivraisonLieu();
        $amap3ll->setAmap($amap3);
        $amap3->addLivraisonLieux($amap3ll);
        $this->em->persist($amap3);
        $this->em->persist($amap3ll);
        $this->em->flush();

        $this->recordDistribution($amap3, new Carbon('2000-01-02'), 10);
        $this->recordDistribution($amap3, new Carbon('2000-01-03'), 10);

        $res = $this->amapDistributionRepo->getLastDistributionByAmap();
        self::assertCount(3, $res);
        self::assertTrue(Carbon::createFromDate(2030, 1, 1)->startOfDay()->eq($res[0]->getDate()));
        self::assertSame($amap1, $res[0]->getAmapLivraisonLieu()->getAmap());
        self::assertTrue(Carbon::createFromDate(2000, 1, 1)->startOfDay()->eq($res[1]->getDate()));
        self::assertSame($amap2, $res[1]->getAmapLivraisonLieu()->getAmap());
        self::assertTrue(Carbon::createFromDate(2000, 1, 3)->startOfDay()->eq($res[2]->getDate()));
        self::assertSame($amap2, $res[1]->getAmapLivraisonLieu()->getAmap());
    }

    public function testSearchAmapienStateFilterAll(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapienState($amapien->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setFilter(SearchDistributionAmapienState::FILTER_ALL);
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testSearchAmapienStateFilterAllDiffrentAmap(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $amap2 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test 2',
            ])
        ;

        $distributionRecorded = $this->recordDistribution($amap2, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapienState($amapien->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setFilter(SearchDistributionAmapienState::FILTER_ALL);
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertEmpty($distributionRequesteds);
    }

    public function testSearchAmapienStateHideCompletedNotSubscribed(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amapienRef = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapienref',
            ])
            ->getAmapienAmaps()
            ->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapienState($amapienRef->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setFilter(SearchDistributionAmapienState::FILTER_MINE);
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertEmpty($distributionRequesteds);
    }

    public function testSearchAmapienStateShowCompletedSubscribed(): void
    {
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()->first()
        ;
        $amap = $amapien->getAmap();

        $distributionRecorded = $this->recordDistribution($amap, new Carbon('2000-01-15'), 1);
        $distributionRecorded->addAmapien($amapien);
        $this->em->flush();

        $searchState = new SearchDistributionAmapienState($amapien->getUser());
        $searchState->setLivLieu($amap->getLivraisonLieux()->first());
        $searchState->setPeriod(
            Period::buildFromDates(
                new Carbon('2000-01-01'),
                new Carbon('2000-01-30')
            )
        );
        $searchState->setFilter(SearchDistributionAmapienState::FILTER_MINE);
        $distributionRequesteds = $this->amapDistributionRepo->searchAmapienState($searchState);

        self::assertCount(1, $distributionRequesteds);
        self::assertSame($distributionRecorded, $distributionRequesteds[0]);
    }

    public function testCountDistributionWithAmapienByDateAmap(): void
    {
        $amap1 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test',
            ])
        ;
        $amap2 = $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy([
                'nom' => 'amap test 2',
            ])
        ;
        $amapien = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
            ->getAmapienAmaps()->first()
        ;
        $amapienRef = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapienref',
            ])
            ->getAmapienAmaps()
            ->first()
        ;

        $distribution1 = $this->recordDistribution($amap1, new Carbon('2000-01-01'), 10);
        $distribution2 = $this->recordDistribution($amap1, new Carbon('2000-01-15'), 10);
        $distribution3 = $this->recordDistribution($amap1, new Carbon('2000-01-31'), 10);
        $distribution4 = $this->recordDistribution($amap1, new Carbon('2000-02-01'), 10);

        $distribution5 = $this->recordDistribution($amap2, new Carbon('2000-01-15'), 10);

        $distribution1->addAmapien($amapien);
        $distribution1->addAmapien($amapienRef);
        $distribution3->addAmapien($amapien);
        $distribution4->addAmapien($amapien);
        $distribution5->addAmapien($amapien);
        $this->em->flush();

        $res = $this->amapDistributionRepo->countDistributionWithAmapienByDateAmapMultiple(
            [$amap1],
            new Carbon('2000-01-01'),
            new Carbon('2000-01-31')
        );
        self::assertCount(1, $res);
        self::assertSame(2, $res[$amap1->getId()]);
    }

    private function recordDistribution(Amap $amap, Carbon $date, int $nbPersonnes)
    {
        $distribution = new AmapDistribution(
            AmapDistributionDetail::create(new Time(17, 0), new Time(18, 0), $nbPersonnes, 'test'),
            $amap->getLivraisonLieux()->first(),
            $date
        );
        $this->em->persist($distribution);
        $this->em->flush();

        return $distribution;
    }
}
