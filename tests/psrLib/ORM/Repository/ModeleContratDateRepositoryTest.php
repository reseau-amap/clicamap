<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\DTO\SearchFermeLivraisonListState;
use PsrLib\ORM\Entity\Embeddable\Period;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Repository\ModeleContratDateRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratDateRepositoryTest extends RepositoryTestAbstract
{
    /**
     * @var ModeleContratDateRepository
     */
    private \Doctrine\ORM\EntityRepository $modeleContratDateRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->modeleContratDateRepo = $this->em->getRepository(ModeleContratDate::class);
    }

    public function testSearchDateLimit(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $this->recordMcDates($ferme, [
            new Carbon('2000-05-15'),
            new Carbon('2000-06-01'),
            new Carbon('2000-06-15'),
            new Carbon('2000-06-30'),
            new Carbon('2000-07-15'),
        ]);

        $period = new Period();
        $period->setStartAt(new Carbon('2000-06-01'));
        $period->setEndAt(new Carbon('2000-06-30'));
        $state = new SearchFermeLivraisonListState($ferme);
        $state->setPeriod($period);

        $dates = $this
            ->modeleContratDateRepo
            ->searchDate($state)
        ;
        self::assertCount(3, $dates);
        self::assertTrue(Carbon::parse('2000-06-01')->eq($dates[0]));
        self::assertTrue(Carbon::parse('2000-06-15')->eq($dates[1]));
        self::assertTrue(Carbon::parse('2000-06-30')->eq($dates[2]));
    }

    public function testSearchDateDistinct(): void
    {
        $ferme = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $this->recordMcDates($ferme, [
            new Carbon('2000-06-15'),
        ]);
        $this->recordMcDates($ferme, [
            new Carbon('2000-06-15'),
            new Carbon('2000-06-16'),
        ]);

        $period = new Period();
        $period->setStartAt(new Carbon('2000-06-01'));
        $period->setEndAt(new Carbon('2000-06-30'));
        $state = new SearchFermeLivraisonListState($ferme);
        $state->setPeriod($period);

        $dates = $this
            ->modeleContratDateRepo
            ->searchDate($state)
        ;
        self::assertCount(2, $dates);
        self::assertTrue(Carbon::parse('2000-06-15')->eq($dates[0]));
        self::assertTrue(Carbon::parse('2000-06-16')->eq($dates[1]));
    }

    public function testSearchDateFerme(): void
    {
        $ferme1 = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $ferme2 = $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy([
                'nom' => 'Ferme 2',
            ])
        ;
        $this->recordMcDates($ferme1, [
            new Carbon('2000-06-15'),
        ]);
        $this->recordMcDates($ferme2, [
            new Carbon('2000-06-16'),
        ]);

        $period = new Period();
        $period->setStartAt(new Carbon('2000-06-01'));
        $period->setEndAt(new Carbon('2000-06-30'));
        $state = new SearchFermeLivraisonListState($ferme1);
        $state->setPeriod($period);

        $dates = $this
            ->modeleContratDateRepo
            ->searchDate($state)
        ;
        self::assertCount(1, $dates);
        self::assertTrue(Carbon::parse('2000-06-15')->eq($dates[0]));
    }

    /**
     * @param Carbon[] $dates
     */
    private function recordMcDates(Ferme $ferme, array $dates): void
    {
        $mc = new ModeleContrat();
        $mc->setFerme($ferme);
        $mc->setReglementType(['type']);
        $mc->setDelaiModifContrat(2);
        $this->em->persist($mc);

        foreach ($dates as $date) {
            $mcDate = new ModeleContratDate();
            $mcDate->setModeleContrat($mc);
            $mcDate->setDateLivraison($date);
            $this->em->persist($mcDate);
        }

        $this->em->flush();
    }
}
