<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use Money\Money;
use PsrLib\ORM\Entity\AdhesionFerme;
use PsrLib\ORM\Entity\AdhesionValueFerme;
use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\EvenementAbonnementFerme;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeRepositoryRemoveTest extends RepositoryTestAbstract
{
    /**
     * @var FermeRepository
     */
    private \Doctrine\ORM\EntityRepository $fermeRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->fermeRepository = $this->em->getRepository(Ferme::class);
    }

    public function testRemoveWithAdhesion(): void
    {
        $ferme = $this
            ->fermeRepository
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'admin_aura',
            ])
        ;
        $adhesionValue = new AdhesionValueFerme();
        $adhesionValue->setAmount(Money::EUR(100));
        $adhesionValue->setPaymentDate(new Carbon());
        $adhesionValue->setProcessingDate(new Carbon());
        $adhesion = new AdhesionFerme(
            $ferme,
            $adhesionValue,
            $user
        );
        $this->em->persist($adhesion);
        $this->em->flush();

        $this->em->remove($ferme);
        $this->em->flush();
        $this->em->clear();

        self::assertNull(
            $this
                ->fermeRepository
                ->findOneBy([
                    'nom' => 'ferme',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(AdhesionFerme::class)
                ->findAll()
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(ModeleContrat::class)
                ->findAll()
        );
    }

    public function testRemoveWithEvenementAbonnement(): void
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'amapien',
            ])
        ;

        $ferme = $this
            ->fermeRepository
            ->findOneBy([
                'nom' => 'ferme',
            ])
        ;
        $evenementAbonnement = new EvenementAbonnementFerme($user);
        $evenementAbonnement->setRelated($ferme);
        $this->em->persist($evenementAbonnement);
        $this->em->flush();

        $this->em->remove($ferme);
        $this->em->flush();
        $this->em->clear();

        self::assertNull(
            $this
                ->fermeRepository
                ->findOneBy([
                    'nom' => 'ferme',
                ])
        );
        self::assertEmpty(
            $this
                ->em
                ->getRepository(EvenementAbonnement::class)
                ->findAll()
        );
    }
}
