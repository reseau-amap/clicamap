<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use Carbon\Carbon;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\CampagneBulletinRecus;
use PsrLib\ORM\Repository\CampagneBulletinRecusRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinRecusRepositoryNumeroInterneSuivantTest extends RepositoryTestAbstract
{
    private CampagneBulletinRecusRepository $bulletinRecusRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->bulletinRecusRepository = $this->em->getRepository(CampagneBulletinRecus::class);
    }

    public function testEmpty(): void
    {
        $amap = $this->getAmap('amap test');
        $this->assertEquals(1, $this->bulletinRecusRepository->getNumeroInterneSuivantForAmap($amap));
    }

    public function testNumeroSuivant(): void
    {
        $amap = $this->getAmap('amap test');
        $campagne = $this->storeCampagne($amap);
        $this->storeBulletinRecu($campagne, 1);

        $this->assertEquals(2, $this->bulletinRecusRepository->getNumeroInterneSuivantForAmap($amap));
    }

    public function testNumeroSuivantTrou(): void
    {
        $amap = $this->getAmap('amap test');
        $campagne = $this->storeCampagne($amap);
        $this->storeBulletinRecu($campagne, 1);
        $this->storeBulletinRecu($campagne, 3);

        $this->assertEquals(2, $this->bulletinRecusRepository->getNumeroInterneSuivantForAmap($amap));
    }

    public function testNumeroSuivantPremier(): void
    {
        $amap = $this->getAmap('amap test');
        $campagne = $this->storeCampagne($amap);
        $this->storeBulletinRecu($campagne, 2);
        $this->storeBulletinRecu($campagne, 3);

        $this->assertEquals(1, $this->bulletinRecusRepository->getNumeroInterneSuivantForAmap($amap));
    }

    public function testNumeroSuivantFiltreAmap(): void
    {
        $amap1 = $this->getAmap('amap test');
        $amap2 = $this->getAmap('amap test 2');
        $campagne = $this->storeCampagne($amap1);
        $this->storeBulletinRecu($campagne, 1);

        $this->assertEquals(1, $this->bulletinRecusRepository->getNumeroInterneSuivantForAmap($amap2));
    }

    private function storeBulletinRecu(Campagne $campagne, int $numeroInterne): void
    {
        $bulletin = new CampagneBulletin();
        $bulletin->setPayeur('test');
        $bulletin->setPaiementDate(Carbon::now());
        $bulletin->setCampagne($campagne);

        $recus = new CampagneBulletinRecus($numeroInterne, 'numero', null);
        $bulletin->setRecus($recus);
        $this->em->persist($bulletin);
        $this->em->persist($recus);

        $this->em->flush();
    }

    private function storeCampagne(Amap $amap): Campagne
    {
        $campagne = new Campagne();
        $campagne->setAnneeAdhesionAmapien(1000);
        $campagne->setPaiementMethode('test');
        $campagne->setPaiementDescription('test');
        $campagne->setChampLibre('test');
        $campagne->setNom('nom');
        $campagne->setAmap($amap);
        $campagne->getPeriod()->setStartAt(new Carbon('2000-01-01'));
        $campagne->getPeriod()->setEndAt(new Carbon('2000-01-01'));
        $campagne->getAutheurInfo()->setNom('test');
        $campagne->getAutheurInfo()->setAdresse('test');
        $campagne->getAutheurInfo()->setTelephone('test');
        $campagne->getAutheurInfo()->setSite('test');
        $campagne->getAutheurInfo()->setVille('test');
        $campagne->getAutheurInfo()->setEmails('test');
        $campagne->getAutheurInfo()->setSiret('test');
        $campagne->getAutheurInfo()->setRna('test');

        $this->em->persist($campagne);
        $this->em->flush();

        return $campagne;
    }

    private function getAmap(string $name): Amap
    {
        return $this->em->getRepository(Amap::class)->findOneBy(['nom' => $name]);
    }
}
