<?php

declare(strict_types=1);

namespace Test\ORM\Repository;

use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\FermeRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeRepositoryQbEvenementCreationForUserTest extends RepositoryTestAbstract
{
    /**
     * @var FermeRepository
     */
    private \Doctrine\ORM\EntityRepository $fermeRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->fermeRepository = $this->em->getRepository(Ferme::class);
    }

    /**
     * @dataProvider providerUsers
     */
    public function testQbEvenementCreationForUser(string $user, array $expectedFermes): void
    {
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => $user,
            ])
        ;
        $resFermes = $this
            ->fermeRepository
            ->qbEvenementCreationForUser($user)
            ->getQuery()
            ->getResult()
        ;

        $resFermesNames = array_map(fn (Ferme $ferme) => $ferme->getNom(), $resFermes);
        self::assertSame($expectedFermes, $resFermesNames);
    }

    public function providerUsers()
    {
        return [
            ['empty', []],
            ['admin', ['ferme', 'Ferme 2']],
            ['admin_aura', ['ferme', 'Ferme 2']],
            ['admin_rhone', ['ferme', 'Ferme 2']],
            ['paysan', ['ferme']],
            ['admin_idf', []],
            ['admin_paris', []],
            ['amapien', []],
            ['amapienref', []],
            ['amap', []],
            ['paysan', ['ferme']],
            ['paysan2', ['Ferme 2']],
        ];
    }

    public function testQbEvenementCreationForUserRegroupement(): void
    {
        $ferme = $this->fermeRepository->findOneBy(['nom' => 'ferme']);
        $regroupement = $this->em->getRepository(FermeRegroupement::class)->findOneBy(['nom' => 'Regroupement']);
        $regroupement->addFerme($ferme);
        $this->em->flush();

        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy([
                'username' => 'regroupement',
            ])
        ;
        $resFermes = $this
            ->fermeRepository
            ->qbEvenementCreationForUser($user)
            ->getQuery()
            ->getResult()
        ;

        self::assertCount(1, $resFermes);
        self::assertSame('ferme', $resFermes[0]->getNom());
    }
}
