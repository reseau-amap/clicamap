<?php

declare(strict_types=1);

namespace Test\ORM\EventSubscriber;

use Money\Money;
use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\AdhesionAmap;
use PsrLib\ORM\Entity\AdhesionValueAmap;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAnneeAdhesion;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\EventSubscriber\AdhesionAmapPostGenerateSubscriber;
use PsrLib\ORM\EventSubscriber\AdhesionPostGenerateEventArgs;

/**
 * @internal
 *
 * @coversNothing
 */
class AdhesionAmapPostGenerateSubscriberTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \PsrLib\ORM\EventSubscriber\AdhesionAmapPostGenerateSubscriber $subscriber;

    public function setUp(): void
    {
        $this->subscriber = new AdhesionAmapPostGenerateSubscriber();
    }

    public function testAdhesionNoMembership(): void
    {
        $amap = new Amap();
        $value = new AdhesionValueAmap();
        $value->setAmountMembership(null);
        $adhesion = new AdhesionAmap(
            $amap,
            $value,
            $this->prophesize(User::class)->reveal()
        );

        $this->subscriber->adhesionPostGenerate(new AdhesionPostGenerateEventArgs($adhesion));
        self::assertEmpty($amap->getAnneeAdhesions());
    }

    public function testAdhesionExistingYear(): void
    {
        $amap = new Amap();
        $amap->addAnneeAdhesion(new AmapAnneeAdhesion(2000, $amap));

        $value = new AdhesionValueAmap();
        $value->setAmountMembership(Money::EUR(100));
        $value->setYear(2000);
        $adhesion = new AdhesionAmap(
            $amap,
            $value,
            $this->prophesize(User::class)->reveal()
        );

        $this->subscriber->adhesionPostGenerate(new AdhesionPostGenerateEventArgs($adhesion));
        self::assertCount(1, $amap->getAnneeAdhesions());
    }

    public function testAdhesionNotExistingYear(): void
    {
        $amap = new Amap();

        $value = new AdhesionValueAmap();
        $value->setAmountMembership(Money::EUR(100));
        $value->setYear(2000);
        $adhesion = new AdhesionAmap(
            $amap,
            $value,
            $this->prophesize(User::class)->reveal()
        );

        $this->subscriber->adhesionPostGenerate(new AdhesionPostGenerateEventArgs($adhesion));
        self::assertCount(1, $amap->getAnneeAdhesions());
        self::assertSame(2000, $amap->getAnneeAdhesions()->first()->getAnnee());
    }
}
