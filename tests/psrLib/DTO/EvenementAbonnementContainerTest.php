<?php

declare(strict_types=1);

namespace Test\DTO;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\EvenementAbonnementContainer;
use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\EvenementAbonnementWithRelatedEntity;
use PsrLib\ORM\Entity\EvenementRelatedEntity;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;

/**
 * @internal
 *
 * @coversNothing
 */
class EvenementAbonnementContainerTest extends TestCase
{
    use ProphecyTrait;

    public function testGetRelatedEntitiesFor(): void
    {
        $user1 = $this->prophesizeUser();
        $user2 = $this->prophesizeUser();

        $ferme1 = $this->prophesize(Ferme::class)->reveal();
        $ferme2 = $this->prophesize(Ferme::class)->reveal();
        $ferme3 = $this->prophesize(Ferme::class)->reveal();
        $amap1 = $this->prophesize(Amap::class)->reveal();

        $container = new EvenementAbonnementContainer([
            $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme1),
            $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme2),
            $this->prophetizeEvenementAbonnementWithRelatedEntity($user2, $ferme3),
            $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme2),
            $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $amap1),
        ]);
        $res = $container->getRelatedEntitiesFor(EvenementType::FERME);
        self::assertCount(3, $res);
        self::assertSame($ferme1, $res[0]);
        self::assertSame($ferme2, $res[1]);
        self::assertSame($ferme3, $res[2]);
    }

    public function testGetAbonnement(): void
    {
        $user1 = $this->prophesizeUser();
        $user2 = $this->prophesizeUser();

        $ferme1 = $this->prophesize(Ferme::class)->reveal();
        $ferme2 = $this->prophesize(Ferme::class)->reveal();

        $abo1 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme1);
        $abo2 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme2);
        $abo3 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user2, $ferme1);

        $container = new EvenementAbonnementContainer([
            $abo1,
            $abo2,
            $abo3,
        ]);
        $res = $container->getAbonnement($user1, $ferme1);
        self::assertSame($abo1, $res);
    }

    public function testHasAbonnement(): void
    {
        $user1 = $this->prophesizeUser();
        $user2 = $this->prophesizeUser();

        $ferme1 = $this->prophesize(Ferme::class)->reveal();
        $ferme2 = $this->prophesize(Ferme::class)->reveal();

        $abo1 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme1);
        $abo2 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user1, $ferme2);
        $abo3 = $this->prophetizeEvenementAbonnementWithRelatedEntity($user2, $ferme1);

        $container = new EvenementAbonnementContainer([
            $abo1,
            $abo2,
            $abo3,
        ]);
        self::assertTrue($container->hasAbonnement($user1, $ferme1));
        self::assertFalse($container->hasAbonnement($user2, $ferme2));
    }

    private function prophetizeEvenementAbonnementWithRelatedEntity(User $user, EvenementRelatedEntity $relatedEntity)
    {
        $abonnement = $this->prophesize(EvenementAbonnementWithRelatedEntity::class);
        $abonnement->getRelated()->willReturn($relatedEntity);
        $abonnement->getUser()->willReturn($user);

        return $abonnement->reveal();
    }

    private function prophesizeUser()
    {
        return $this->prophesize(User::class)->reveal();
    }
}
