<?php

declare(strict_types=1);

namespace Test\DTO;

use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ContratCommandeItem;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeItemTest extends TestCase
{
    use ProphecyTrait;

    private $mcp;
    private $mcd;

    protected function setUp(): void
    {
        $this->mcp = $this->prophesize(ModeleContratProduit::class);
        $this->mcd = $this->prophesize(ModeleContratDate::class);
    }

    public function testQtyIsInitiallyZero(): void
    {
        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(false);
        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $this->assertEquals(0.0, $item->getQty());
    }

    public function testCanSetQtyWhenNotDisabled(): void
    {
        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(false);
        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $item->setQty(5.0);
        $this->assertEquals(5.0, $item->getQty());
    }

    public function testCannotSetQtyWhenDisabled(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Cannot set qty for disabled item');
        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(true);
        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $item->setQty(5.0);
    }

    public function testTotalCalculatesCorrectly(): void
    {
        $price = $this->prophesize(PrixTVA::class);
        $price->getPrixTTC()->willReturn(Money::EUR(1000));

        $this->mcp->getPrix()->willReturn($price->reveal());
        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(false);

        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $item->setQty(5.0);

        $this->assertTrue(Money::EUR(5000)->equals($item->total()));
    }

    public function testCIsDisabledReturnsCorrectValue(): void
    {
        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(true);
        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $this->assertTrue($item->isDisabled());

        $this->mcp->hasExclusionForModeleContratDate($this->mcd->reveal())->willReturn(false);
        $item = new ContratCommandeItem($this->mcp->reveal(), $this->mcd->reveal());
        $this->assertFalse($item->isDisabled());
    }
}
