<?php

declare(strict_types=1);

namespace Test\DTO;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ModeleContratFrom5DTO;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\ModeleContratProduitExclure;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratFrom5DTOTest extends TestCase
{
    use ProphecyTrait;

    public function testConstructEmpty(): void
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getProduits()->willReturn(new ArrayCollection([]));
        $mc->getDatesOrderedAsc()->willReturn([]);

        $dto = new ModeleContratFrom5DTO($mc->reveal());
        $this->assertEquals([], $dto->getExclusions());
    }

    public function testConstructNoExclusions(): void
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getProduits()->willReturn(new ArrayCollection([$this->prophesizeProduit(2, [])]));
        $mc->getDatesOrderedAsc()->willReturn([$this->prophesizeDate(new \DateTime('2021-01-01'))]);

        $dto = new ModeleContratFrom5DTO($mc->reveal());
        $this->assertEquals([
            '2021-01-01_2' => true,
        ], $dto->getExclusions());
    }

    public function testConstructExclusions(): void
    {
        $date1 = $this->prophesizeDate(new \DateTime('2021-01-01'));
        $exclusion1 = $this->prophesizeExclusion($date1);
        $produit1 = $this->prophesizeProduit(1, [$exclusion1]);

        $date2 = $this->prophesizeDate(new \DateTime('2021-01-02'));
        $produit2 = $this->prophesizeProduit(2, []);

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getProduits()->willReturn(new ArrayCollection([$produit1, $produit2]));
        $mc->getDatesOrderedAsc()->willReturn([$date1, $date2]);

        $dto = new ModeleContratFrom5DTO($mc->reveal());
        $this->assertEquals([
            '2021-01-01_2' => true,
            '2021-01-01_1' => false,
            '2021-01-02_1' => true,
            '2021-01-02_2' => true,
        ], $dto->getExclusions());
    }

    private function prophesizeDate(\DateTime $dt): ModeleContratDate
    {
        $date = $this->prophesize(ModeleContratDate::class);
        $date->getDateLivraison()->willReturn($dt);

        return $date->reveal();
    }

    private function prophesizeProduit(int $fermeProduitId, array $exclusions): ModeleContratProduit
    {
        $fermeProduit = $this->prophesize(FermeProduit::class);
        $fermeProduit->getId()->willReturn($fermeProduitId);

        $produit = $this->prophesize(ModeleContratProduit::class);
        $produit->getFermeProduit()->willReturn($fermeProduit->reveal());
        $produit->getExclusions()->willReturn(new ArrayCollection($exclusions));

        return $produit->reveal();
    }

    private function prophesizeExclusion(ModeleContratDate $mcd): ModeleContratProduitExclure
    {
        $exclusion = $this->prophesize(ModeleContratProduitExclure::class);
        $exclusion->getModeleContratDate()->willReturn($mcd);

        return $exclusion->reveal();
    }
}
