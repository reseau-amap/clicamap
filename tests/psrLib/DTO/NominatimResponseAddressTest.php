<?php

declare(strict_types=1);

namespace Test\DTO;

use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Nominatim\NominatimResponseAddress;

/**
 * @internal
 *
 * @coversNothing
 */
class NominatimResponseAddressTest extends TestCase
{
    /**
     * @dataProvider getPlaceNameProvider
     */
    public function testGetPlaceName(
        ?string $hamlet,
        ?string $village,
        ?string $town,
        ?string $city,
        ?string $expected
    ): void {
        $addr = new NominatimResponseAddress();
        $addr->setHamlet($hamlet);
        $addr->setVillage($village);
        $addr->setTown($town);
        $addr->setCity($city);

        self::assertSame($expected, $addr->getPlaceName());
    }

    public function getPlaceNameProvider()
    {
        return [
            [null, null, null, null, null],
            ['hamlet', 'village', 'town', 'city', 'hamlet'],
            [null, 'village', 'town', 'city', 'village'],
            [null, null, 'town', 'city', 'town'],
            [null, null, null, 'city', 'city'],
        ];
    }
}
