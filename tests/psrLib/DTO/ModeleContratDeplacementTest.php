<?php

declare(strict_types=1);

namespace Test\DTO;

use Assert\InvalidArgumentException;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ModeleContratDeplacement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratDeplacementTest extends ContainerAwareTestCase
{
    use ProphecyTrait;
    use ValidationViolationTrait;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testConstructMcNoDate(): void
    {
        self::expectException(InvalidArgumentException::class);
        new ModeleContratDeplacement($this->prophesizeMc([]));
    }

    public function testConstructMc(): void
    {
        $mcdate = $this->prophesize(ModeleContratDate::class)->reveal();
        $mc = $this->prophesizeMc([$mcdate]);

        $mcd = new ModeleContratDeplacement($mc);
        self::assertSame($mc, $mcd->getMc());
        self::assertSame($mcdate, $mcd->getSrc());
        self::assertNull($mcd->getDst());
    }

    public function testValidationDstNotNull(): void
    {
        $violations = $this
            ->validator
            ->validatePropertyValue(ModeleContratDeplacement::class, 'dst', null)
        ;
        self::assertCount(1, $violations);
        self::assertViolationContainsMessage($violations, 'Merci d\'indiquer une date de destination.');
    }

    public function testValidationDstNotInMcDates(): void
    {
        $mc = $this->prophesizeMc([$this->prophesizeMcDate(new Carbon('2000-01-01'))]);
        $mcd = new ModeleContratDeplacement($mc);
        $mcd->setDst(new Carbon('2000-01-02'));

        $violations = $this->validator->validate($mcd);
        self::assertEmpty($violations);
    }

    public function testValidationDstInMcDates(): void
    {
        $mc = $this->prophesizeMc([$this->prophesizeMcDate(Carbon::create(2000, 1, 1, 1))]);
        $mcd = new ModeleContratDeplacement($mc);
        $mcd->setDst(Carbon::create(2000, 1, 1, 2));

        $violations = $this->validator->validate($mcd);
        self::assertCount(1, $violations);
        self::assertViolationContainsMessage($violations, 'La date sélectionnée est déjà une date de livraison.');
    }

    private function prophesizeMc(array $mcDates): ModeleContrat
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getDates()->willReturn(new ArrayCollection($mcDates));

        return $mc->reveal();
    }

    private function prophesizeMcDate(\DateTime $dateTime)
    {
        $mcdate = $this->prophesize(ModeleContratDate::class);
        $mcdate->getDateLivraison()->willReturn($dateTime);

        return $mcdate->reveal();
    }
}
