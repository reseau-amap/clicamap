<?php

declare(strict_types=1);

namespace Test\DTO;

use Money\Money;
use PsrLib\DTO\CampagneBulletinImportLineDTO;
use PsrLib\ORM\Entity\Campagne;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;
use Test\ValidationViolationTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinImportLineDTOTest extends ContainerAwareTestCase
{
    use ValidationViolationTrait;

    use \Prophecy\PhpUnit\ProphecyTrait;

    private ValidatorInterface $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    /**
     * @dataProvider permissionImageProvider
     */
    public function testValidatePermissionImageValid(string $value): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setPermissionImage($value);

        $violations = $this->validator->validate($dto);
        self::assertViolationNotContainsMessage($violations, 'Cette valeur n\'est pas valide.', 'permissionImage');
    }

    public function permissionImageProvider()
    {
        return [
            ['O'],
            ['N'],
        ];
    }

    public function testValidatePermissionImageInvalid(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setPermissionImage('invalid');

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'The value you selected is not a valid choice.', 'permissionImage');
    }

    public function testValidatePermissionImageNull(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setPermissionImage(null);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'La permission d\'image est obligatoire', 'permissionImage');
    }

    public function testValidateAmapienNull(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setAmapien(null);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'Amapien introuvable', 'amapien');
    }

    public function testValidatePayeurNull(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setPayeur(null);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'Le nom du payeur est obligatoire', 'payeur');
    }

    public function testValidatePaiementDateNull(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setPaiementDate(null);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'La date de paiement est obligatoire', 'paiementDate');
    }

    public function testValidateMontantLibresCountSup(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(0));
        $dto->setMontantLibres([Money::EUR(1), Money::EUR(2)]);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'Le nombre de montants libres ne correspond pas au nombre de montants libres attendus', 'montantLibres');
    }

    public function testValidateMontantLibresCountInf(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(1));
        $dto->setMontantLibres([]);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'Le nombre de montants libres ne correspond pas au nombre de montants libres attendus', 'montantLibres');
    }

    public function testValidateMontantLibresCountEqual(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(1));
        $dto->setMontantLibres([Money::EUR(1)]);

        $violations = $this->validator->validate($dto);
        self::assertViolationNotContainsMessage($violations, 'Le nombre de montants libres ne correspond pas au nombre de montants libres attendus', 'montantLibres');
    }

    public function testValidateMontantLibresNotEmpty(): void
    {
        $dto = new CampagneBulletinImportLineDTO($this->prophesizeCampagne(4));
        $dto->setMontantLibres([Money::EUR(1), null, Money::EUR(2), Money::EUR(0)]);

        $violations = $this->validator->validate($dto);
        self::assertViolationContainsMessage($violations, 'Le montant libre 2 n\'est pas valide', 'montantLibres');
        self::assertViolationContainsMessage($violations, 'Le montant libre 4 n\'est pas valide', 'montantLibres');
    }

    private function prophesizeCampagne(int $countMontantLibre): Campagne
    {
        $campagne = $this->prophesize(Campagne::class);
        $campagne->countMontantLibres()->willReturn($countMontantLibre);

        return $campagne->reveal();
    }
}
