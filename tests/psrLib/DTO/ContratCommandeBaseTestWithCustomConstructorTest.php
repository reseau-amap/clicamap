<?php

declare(strict_types=1);

namespace Test\DTO;

use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ContratCommandeBase;
use PsrLib\DTO\ContratCommandeItem;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratCommandeBaseTestWithCustomConstructorTest extends ContratCommandeBase
{
    /**
     * @param ContratCommandeItem[] $items
     */
    public function __construct(ModeleContrat $mc, array $items)
    {
        $this->mc = $mc;
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getSubscribableMcp(): array
    {
        return $this->mc->getProduits()->toArray();
    }
}

class ContratCommandeBaseTestImplementation extends ContratCommandeBase
{
    public function getItems(): array
    {
        return $this->items;
    }

    public function getSubscribableMcp(): array
    {
        return $this->mc->getProduits()->toArray();
    }
}

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeBaseTest extends TestCase
{
    use ProphecyTrait;

    public function testCreateFromMc(): void
    {
        [$mc, $produit1, $produit2, $date1, $date2] = $this->prophesizeMc();
        $order = ContratCommandeBaseTestImplementation::createFromMc($mc);

        $this->assertSame($mc, $order->getMc());

        $this->assertCount(4, $order->getItems());
        $item1 = $order->getItem($produit1, $date1);
        $this->assertSame($produit1, $item1->getMcp());
        $this->assertSame($date1, $item1->getMcd());
        $this->assertFalse($item1->isDisabled());

        $item2 = $order->getItem($produit2, $date1);
        $this->assertSame($produit2, $item2->getMcp());
        $this->assertSame($date1, $item2->getMcd());
        $this->assertFalse($item2->isDisabled());

        $item3 = $order->getItem($produit1, $date2);
        $this->assertSame($produit1, $item3->getMcp());
        $this->assertSame($date2, $item3->getMcd());
        $this->assertFalse($item3->isDisabled());

        $item4 = $order->getItem($produit2, $date2);
        $this->assertSame($produit2, $item4->getMcp());
        $this->assertSame($date2, $item4->getMcd());
        $this->assertTrue($item4->isDisabled());
    }

    public function testCreateFromContratCreatesOrderCorrectly(): void
    {
        [$mc, $produit1, $produit2, $date1, $date2] = $this->prophesizeMc();

        $contrat = $this->prophesize(Contrat::class);
        $contrat->getModeleContrat()->willReturn($mc);

        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratProduit()->willReturn($produit1);
        $cellule->getModeleContratDate()->willReturn($date1);
        $cellule->getQuantite()->willReturn(5.0);

        $contrat->getCellules()->willReturn([$cellule->reveal()]);

        $order = ContratCommandeBaseTestImplementation::createFromContrat($contrat->reveal());

        $this->assertInstanceOf(ContratCommandeBase::class, $order);
        $this->assertCount(4, $order->getItems());
        $this->assertEquals(5.0, $order->getItem($produit1, $date1)->getQty());
    }

    public function testCreateFromContratHandlesEmptyContrat(): void
    {
        $contrat = $this->prophesize(Contrat::class);
        $modeleContrat = $this->prophesize(ModeleContrat::class);

        $contrat->getModeleContrat()->willReturn($modeleContrat->reveal());
        $contrat->getCellules()->willReturn([]);
        $modeleContrat->getDatesOrderedAsc()->willReturn([]);
        $modeleContrat->getProduits()->willReturn([]);

        $order = ContratCommandeBaseTestImplementation::createFromContrat($contrat->reveal());

        $this->assertInstanceOf(ContratCommandeBase::class, $order);
        $this->assertCount(0, $order->getItems());
    }

    private function prophesizeMc(): array
    {
        $mcProphecy = $this->prophesize(ModeleContrat::class);

        $date1Prophecy = $this->prophesize(ModeleContratDate::class);
        $date2Prophecy = $this->prophesize(ModeleContratDate::class);

        $date1Prophecy->getId()->willReturn(1);
        $date2Prophecy->getId()->willReturn(2);

        $date1 = $date1Prophecy->reveal();
        $date2 = $date2Prophecy->reveal();

        $produit1Prophecy = $this->prophesize(ModeleContratProduit::class);
        $produit2Prophecy = $this->prophesize(ModeleContratProduit::class);

        $produit1Prophecy->getId()->willReturn(1);
        $produit2Prophecy->getId()->willReturn(2);

        $produit1Prophecy->hasExclusionForModeleContratDate($date1)->willReturn(false);
        $produit1Prophecy->hasExclusionForModeleContratDate($date2)->willReturn(false);
        $produit2Prophecy->hasExclusionForModeleContratDate($date1)->willReturn(false);
        $produit2Prophecy->hasExclusionForModeleContratDate($date2)->willReturn(true);

        $produit1 = $produit1Prophecy->reveal();
        $produit2 = $produit2Prophecy->reveal();

        $mcProphecy->getDatesOrderedAsc()->willReturn([$date1, $date2]);
        $mcProphecy->getProduits()->willReturn([$produit1, $produit2]);

        $mc = $mcProphecy->reveal();

        return [$mc, $produit1, $produit2, $date1, $date2];
    }

    public function testGetKey(): void
    {
        $mcpProphecy = $this->prophesize(ModeleContratProduit::class);
        $mcdProphecy = $this->prophesize(ModeleContratDate::class);

        $mcpProphecy->getId()->willReturn(1);
        $mcdProphecy->getId()->willReturn(2);

        $mcp = $mcpProphecy->reveal();
        $mcd = $mcdProphecy->reveal();

        $key = ContratCommandeBase::getKey($mcp, $mcd);

        $this->assertEquals('1_2', $key);
    }

    public function testTotal(): void
    {
        $item1 = $this->prophesize(ContratCommandeItem::class);
        $item2 = $this->prophesize(ContratCommandeItem::class);

        $item1->total()->willReturn(Money::EUR(10));
        $item2->total()->willReturn(Money::EUR(20));

        $order = new ContratCommandeBaseTestImplementationWithCustomConstructor(
            $this->prophesize(ModeleContrat::class)->reveal(),
            [$item1->reveal(), $item2->reveal()]
        );

        $this->assertEquals(Money::EUR(30), $order->total());
    }

    public function testTotalByMcp(): void
    {
        $mcp1 = $this->prophesize(ModeleContratProduit::class)->reveal();
        $mcp2 = $this->prophesize(ModeleContratProduit::class)->reveal();

        $item1 = $this->prophesize(ContratCommandeItem::class);
        $item2 = $this->prophesize(ContratCommandeItem::class);
        $item3 = $this->prophesize(ContratCommandeItem::class);

        $item1->getMcp()->willReturn($mcp1);
        $item2->getMcp()->willReturn($mcp2);
        $item3->getMcp()->willReturn($mcp1);

        $item1->total()->willReturn(Money::EUR(10));
        $item2->total()->willReturn(Money::EUR(20));
        $item3->total()->willReturn(Money::EUR(30));

        $order = new ContratCommandeBaseTestImplementationWithCustomConstructor(
            $this->prophesize(ModeleContrat::class)->reveal(),
            [$item1->reveal(), $item2->reveal(), $item3->reveal()]
        );

        $this->assertEquals(Money::EUR(40), $order->totalByMcp($mcp1));
    }

    public function testTotalByMcd(): void
    {
        $mcd1 = $this->prophesize(ModeleContratDate::class)->reveal();
        $mcd2 = $this->prophesize(ModeleContratDate::class)->reveal();

        $item1 = $this->prophesize(ContratCommandeItem::class);
        $item2 = $this->prophesize(ContratCommandeItem::class);
        $item3 = $this->prophesize(ContratCommandeItem::class);

        $item1->getMcd()->willReturn($mcd1);
        $item2->getMcd()->willReturn($mcd2);
        $item3->getMcd()->willReturn($mcd1);

        $item1->total()->willReturn(Money::EUR(10));
        $item2->total()->willReturn(Money::EUR(20));
        $item3->total()->willReturn(Money::EUR(30));

        $order = new ContratCommandeBaseTestImplementationWithCustomConstructor(
            $this->prophesize(ModeleContrat::class)->reveal(),
            [$item1->reveal(), $item2->reveal(), $item3->reveal()]
        );

        $this->assertEquals(Money::EUR(40), $order->totalByMcd($mcd1));
    }

    public function testCountNbLivraisons(): void
    {
        $mcProphecy = $this->prophesize(ModeleContrat::class);

        $date1Prophecy = $this->prophesize(ModeleContratDate::class);
        $date2Prophecy = $this->prophesize(ModeleContratDate::class);

        $date1Prophecy->getId()->willReturn(1);
        $date2Prophecy->getId()->willReturn(2);

        $date1 = $date1Prophecy->reveal();
        $date2 = $date2Prophecy->reveal();

        $mcProphecy->getDatesOrderedAsc()->willReturn([$date1, $date2]);

        $item1 = $this->prophesize(ContratCommandeItem::class);
        $item2 = $this->prophesize(ContratCommandeItem::class);
        $item3 = $this->prophesize(ContratCommandeItem::class);

        $item1->getMcd()->willReturn($date1);
        $item2->getMcd()->willReturn($date2);
        $item3->getMcd()->willReturn($date1);

        $item1->total()->willReturn(Money::EUR(10));
        $item2->total()->willReturn(Money::EUR(0));
        $item3->total()->willReturn(Money::EUR(20));

        $order = new ContratCommandeBaseTestImplementationWithCustomConstructor($mcProphecy->reveal(), [$item1->reveal(), $item2->reveal(), $item3->reveal()]);

        $this->assertEquals(1, $order->countNbLivraisons());
    }

    public function testTotalQtyByMcp(): void
    {
        $modeleContratProduit1 = $this->prophesize(ModeleContratProduit::class)->reveal();
        $modeleContratProduit2 = $this->prophesize(ModeleContratProduit::class)->reveal();

        $item1 = $this->prophesize(ContratCommandeItem::class);
        $item1->getMcp()->willReturn($modeleContratProduit1);
        $item1->getQty()->willReturn(2.0);

        $item2 = $this->prophesize(ContratCommandeItem::class);
        $item2->getMcp()->willReturn($modeleContratProduit1);
        $item2->getQty()->willReturn(3.0);

        $item3 = $this->prophesize(ContratCommandeItem::class);
        $item3->getMcp()->willReturn($modeleContratProduit2);
        $item3->getQty()->willReturn(4.0);

        $contractCommandeBase = new ContratCommandeBaseTestImplementationWithCustomConstructor(
            $this->prophesize(ModeleContrat::class)->reveal(),
            [$item1->reveal(), $item2->reveal(), $item3->reveal()]
        );

        $totalQty = $contractCommandeBase->totalQtyByMcp($modeleContratProduit1);
        $this->assertEquals(5.0, $totalQty);
    }

    public function testApplyToContract(): void
    {
        $p1 = $this->prophesizeMcp(1);
        $p2 = $this->prophesizeMcp(2);

        $d1 = $this->prophesizeMcd(3);
        $d2 = $this->prophesizeMcd(4);

        $item1 = $this->prophesizeItem($p1, $d1, false, 2);
        $item2 = $this->prophesizeItem($p1, $d2, false, 3);
        $item3 = $this->prophesizeItem($p2, $d1, false, 4);
        $item4 = $this->prophesizeItem($p2, $d2, true, 5);

        $items = [];
        foreach ([$item1, $item2, $item3, $item4] as $item) {
            $items[ContratCommandeBase::getKey($item->getMcp(), $item->getMcd())] = $item;
        }

        $contractCommandeBase = new ContratCommandeBaseTestImplementationWithCustomConstructor(
            $this->prophesize(ModeleContrat::class)->reveal(),
            $items
        );

        $cellule1 = $this->prophesizeCellule($p1, $d1, 2);
        $cellule2 = $this->prophesizeCellule($p1, $d2, 3);
        $cellule3 = $this->prophesizeCellule($p2, $d1, 4);
        $cellule4 = $this->prophesizeCellule($p2, $d2, null);

        $contrat = $this->prophesize(Contrat::class);
        $contrat->getCellules()->willReturn([$cellule1, $cellule2, $cellule3, $cellule4]);

        $contractCommandeBase->applyToContract($contrat->reveal());
    }

    private function prophesizeCellule(ModeleContratProduit $mcp, ModeleContratDate $mcd, ?int $qty): ContratCellule
    {
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getModeleContratProduit()->willReturn($mcp);
        $cellule->getModeleContratDate()->willReturn($mcd);
        if (null !== $qty) {
            $cellule->setQuantite($qty)->shouldBeCalled();
        } else {
            $cellule->setQuantite(Argument::any())->shouldNotBeCalled();
        }

        return $cellule->reveal();
    }

    private function prophesizeItem(ModeleContratProduit $mcp, ModeleContratDate $mcd, bool $disabled, int $qty): ContratCommandeItem
    {
        $i = $this->prophesize(ContratCommandeItem::class);
        $i->getMcp()->willReturn($mcp);
        $i->getMcd()->willReturn($mcd);
        $i->isDisabled()->willReturn($disabled);
        $i->getQty()->willReturn($qty);

        return $i->reveal();
    }

    private function prophesizeMcp(int $id): ModeleContratProduit
    {
        $p = $this->prophesize(ModeleContratProduit::class);
        $p->getId()->willReturn($id);

        return $p->reveal();
    }

    private function prophesizeMcd(int $id): ModeleContratDate
    {
        $d = $this->prophesize(ModeleContratDate::class);
        $d->getId()->willReturn($id);

        return $d->reveal();
    }
}
