<?php

declare(strict_types=1);

namespace Test\DTO;

use PsrLib\DTO\Time;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class TimeTest extends ContainerAwareTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = self::$container->get(ValidatorInterface::class);
    }

    public function testValidateHourMin(): void
    {
        $violations = $this
            ->validator
            ->validatePropertyValue(Time::class, 'hour', -1)
        ;
        self::assertCount(1, $violations);
        self::assertSame("L'heure doit etre comprise entre 0 et 23", $violations[0]->getMessage());
    }

    public function testValidateHourMax(): void
    {
        $violations = $this
            ->validator
            ->validatePropertyValue(Time::class, 'hour', 25)
        ;
        self::assertCount(1, $violations);
        self::assertSame("L'heure doit etre comprise entre 0 et 23", $violations[0]->getMessage());
    }

    public function testValidateMinutesMin(): void
    {
        $violations = $this
            ->validator
            ->validatePropertyValue(Time::class, 'minute', -1)
        ;
        self::assertCount(1, $violations);
        self::assertSame('Les minutes doivent être comprises entre 0 et 59', $violations[0]->getMessage());
    }

    public function testValidateMinutesMax(): void
    {
        $violations = $this
            ->validator
            ->validatePropertyValue(Time::class, 'minute', 60)
        ;
        self::assertCount(1, $violations);
        self::assertSame('Les minutes doivent être comprises entre 0 et 59', $violations[0]->getMessage());
    }

    public function testToString(): void
    {
        $time = new Time(1, 2);
        self::assertSame('01:02', (string) $time);
    }

    public function testMinuteTotal(): void
    {
        $time = new Time(1, 2);
        self::assertSame(62, $time->getMinutesTotal());
    }
}
