<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;

trait EntityHelperTrait
{
    private function getUserByUsername(string $username): User
    {
        return $this
            ->em
            ->getRepository(User::class)
            ->findOneBy(['username' => $username])
        ;
    }

    private function getAmapByName(string $name): Amap
    {
        return $this
            ->em
            ->getRepository(Amap::class)
            ->findOneBy(['nom' => $name])
        ;
    }

    private function getFermeByName(string $name): Ferme
    {
        return $this
            ->em
            ->getRepository(Ferme::class)
            ->findOneBy(['nom' => $name])
        ;
    }

    private function getRegionByName(string $name): Region
    {
        return $this
            ->em
            ->getRepository(Region::class)
            ->findOneBy(['nom' => $name])
        ;
    }

    private function getDepartementByName(string $name): Departement
    {
        return $this
            ->em
            ->getRepository(Departement::class)
            ->findOneBy(['nom' => $name])
        ;
    }
}
