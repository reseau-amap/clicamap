<?php

declare(strict_types=1);

namespace Test\Form\Type;

use Money\Money;
use PsrLib\Form\Type\MoneyCustomType;
use Test\Form\FormTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class MoneyTypeTest extends FormTestAbstract
{
    public function testFormValid(): void
    {
        $form = $this->formFactory->create(MoneyCustomType::class);
        $form->submit('1.23');

        $money = $form->getData();
        self::assertInstanceOf(Money::class, $money);
        self::assertTrue(Money::EUR(123)->equals($money));
    }

    public function testFormValidNull(): void
    {
        $form = $this->formFactory->create(MoneyCustomType::class);
        $form->submit(null);

        $money = $form->getData();
        self::assertNull($money);
    }

    public function testFormValidEmpty(): void
    {
        $form = $this->formFactory->create(MoneyCustomType::class);
        $form->submit('');

        $money = $form->getData();
        self::assertNull($money);
    }

    public function testFormView(): void
    {
        $view = $this
            ->formFactory
            ->create(MoneyCustomType::class, Money::EUR(123))
        ;

        self::assertSame('1.23', $view->getViewData());
    }
}
