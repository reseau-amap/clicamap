<?php

declare(strict_types=1);

namespace Test\Form;

use PsrLib\Form\FermeType;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeRegroupement;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\User;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeTypeTest extends FormTestAbstract
{
    public function testSiretEditablePaysanFermeNonRegroupement(): void
    {
        $paysan = $this->buildPaysan();
        $this->authentification->mockUser($paysan);
        $form = $this->formFactory->create(FermeType::class, $paysan->getPaysan()->getFermes()->first(), [
            'current_user' => $this->authentification->getCurrentUser(),
        ]);

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditablePaysanFermeRegroupement(): void
    {
        $paysan = $this->buildPaysan(true);
        $this->authentification->mockUser($paysan);
        $form = $this->formFactory->create(FermeType::class, $paysan->getPaysan()->getFermes()->first(), [
            'current_user' => $this->authentification->getCurrentUser(),
        ]);

        $siretField = $form->get('siret');
        self::assertTrue($siretField->isDisabled());
        self::assertSame(['readonly' => true], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditableAmapienFermeNonRegroupement(): void
    {
        $paysan = $this->buildPaysan();
        $this->authentification->mockUser(new User());
        $form = $this->formFactory->create(FermeType::class, $paysan->getPaysan()->getFermes()->first(), [
            'current_user' => $this->authentification->getCurrentUser(),
        ]);

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    public function testSiretEditableAmapienFermeRegroupement(): void
    {
        $paysan = $this->buildPaysan(true);
        $this->authentification->mockUser(new User());
        $form = $this->formFactory->create(FermeType::class, $paysan->getPaysan()->getFermes()->first(), [
            'current_user' => $this->authentification->getCurrentUser(),
        ]);

        $siretField = $form->get('siret');
        self::assertFalse($siretField->isDisabled());
        self::assertSame([], $siretField->getConfig()->getOption('attr'));
    }

    private function buildPaysan(bool $regroupement = false): User
    {
        $ferme = new Ferme();
        if ($regroupement) {
            $ferme->setRegroupement(new FermeRegroupement());
        }
        $paysan = new Paysan();
        $paysan->addFerme($ferme);

        $user = new User();
        $user->setPaysan($paysan);

        return $user;
    }
}
