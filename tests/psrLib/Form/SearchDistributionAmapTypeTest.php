<?php

declare(strict_types=1);

namespace Test\Form;

use Doctrine\Common\Collections\ArrayCollection;
use PsrLib\DTO\SearchDistributionAmapState;
use PsrLib\Form\SearchDistributionAmapType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;

/**
 * @internal
 *
 * @coversNothing
 */
class SearchDistributionAmapTypeTest extends FormTestAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;

    public function testAmapPreselectLLIfOne(): void
    {
        $amap = new Amap();
        $amap->addLivraisonLieux($this->buildLL(1, 'll1'));

        $form = $this->formFactory->create(SearchDistributionAmapType::class, null, [
            'current_user' => $this->prophesizeUser($amap),
        ]);

        $form->submit([]);

        /** @var SearchDistributionAmapState $data */
        $data = $form->getData();
        self::assertSame('ll1', $data->getLivLieu()->getNom());
        self::assertCount(1, $form->createView()->children['livLieu']->vars['choices']);
        self::assertSame('ll1', $form->createView()->children['livLieu']->vars['choices'][0]->label);
        self::assertNull($form->createView()->children['livLieu']->vars['placeholder']);
    }

    public function testAmapLLMultiple(): void
    {
        $amap = new Amap();
        $amap->addLivraisonLieux($this->buildLL(1, 'll1'));
        $amap->addLivraisonLieux($this->buildLL(2, 'll2'));

        $form = $this->formFactory->create(SearchDistributionAmapType::class, null, [
            'current_user' => $this->prophesizeUser($amap),
        ]);
        $form->submit([]);

        /** @var SearchDistributionAmapState $data */
        $data = $form->getData();
        self::assertNull($data->getLivLieu());
        self::assertCount(2, $form->createView()->children['livLieu']->vars['choices']);
        self::assertSame('ll1', $form->createView()->children['livLieu']->vars['choices'][0]->label);
        self::assertSame('ll2', $form->createView()->children['livLieu']->vars['choices'][1]->label);
        self::assertSame('', $form->createView()->children['livLieu']->vars['placeholder']);
    }

    private function buildLL(int $id, string $nom): AmapLivraisonLieu
    {
        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getId()->willReturn($id);
        $ll->getNom()->willReturn($nom);

        return $ll->reveal();
    }

    private function prophesizeUser(Amap $amap)
    {
        $user = $this->prophesize(User::class);
        $user->getAdminAmaps()->willReturn(new ArrayCollection([$amap]));

        return $user->reveal();
    }
}
