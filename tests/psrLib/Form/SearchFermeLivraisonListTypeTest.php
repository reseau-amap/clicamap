<?php

declare(strict_types=1);

namespace Test\Form;

use Carbon\Carbon;
use PsrLib\DTO\SearchFermeLivraisonListState;
use PsrLib\Form\SearchFermeLivraisonListType;
use PsrLib\ORM\Entity\Ferme;

/**
 * @internal
 *
 * @coversNothing
 */
class SearchFermeLivraisonListTypeTest extends FormTestAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;

    public function setUp(): void
    {
        parent::setUp();
        Carbon::setTestNow(new Carbon('2000-01-15'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    public function testDefaultData(): void
    {
        $state = new SearchFermeLivraisonListState($this->prophesize(Ferme::class)->reveal());
        $form = $this->formFactory->create(SearchFermeLivraisonListType::class, $state);
        $form->submit(null);

        self::assertTrue(Carbon::parse('2000-01-15')->eq($state->getPeriod()->getStartAt()));
        self::assertTrue(Carbon::parse('2000-02-15')->eq($state->getPeriod()->getEndAt()));
    }
}
