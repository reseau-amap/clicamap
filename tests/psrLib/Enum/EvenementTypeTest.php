<?php

declare(strict_types=1);

namespace Test\Enum;

use PHPUnit\Framework\TestCase;
use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\EvenementAbonnementDepartement;
use PsrLib\ORM\Entity\EvenementAbonnementFerme;
use PsrLib\ORM\Entity\EvenementAbonnementRegion;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Region;

/**
 * @internal
 *
 * @coversNothing
 */
class EvenementTypeTest extends TestCase
{
    public function testGetRelatedEvtClassDefinedForAllCases(): void
    {
        $cases = EvenementType::cases();
        foreach ($cases as $case) {
            $class = $case->getRelatedEvtClass();
            self::assertTrue(is_a($class, \PsrLib\ORM\Entity\Evenement::class, true));
        }
    }

    public function testGetRelatedEntityClassDefinedForAllCases(): void
    {
        self::assertSame(Ferme::class, EvenementType::FERME->getRelatedEntityClass());
        self::assertSame(Amap::class, EvenementType::AMAP->getRelatedEntityClass());
        self::assertSame(Departement::class, EvenementType::DEPARTEMENT->getRelatedEntityClass());
        self::assertSame(Region::class, EvenementType::REGION->getRelatedEntityClass());
        self::assertSame(false, EvenementType::SUPER_ADMIN->getRelatedEntityClass());
    }

    public function testFromRelatedEntity(): void
    {
        self::assertSame(EvenementType::DEPARTEMENT, EvenementType::fromRelatedEntity(new \PsrLib\ORM\Entity\Departement()));
        self::assertSame(EvenementType::REGION, EvenementType::fromRelatedEntity(new \PsrLib\ORM\Entity\Region()));
        self::assertSame(EvenementType::AMAP, EvenementType::fromRelatedEntity(new \PsrLib\ORM\Entity\Amap()));
        self::assertSame(EvenementType::FERME, EvenementType::fromRelatedEntity(new \PsrLib\ORM\Entity\Ferme()));
    }

    public function testRelatedAbonnementEntityClassDefinedForAllCases(): void
    {
        self::assertSame(EvenementAbonnementFerme::class, EvenementType::FERME->getRelatedAbonnementEntityClass());
        self::assertSame(EvenementAbonnementAmap::class, EvenementType::AMAP->getRelatedAbonnementEntityClass());
        self::assertSame(EvenementAbonnementDepartement::class, EvenementType::DEPARTEMENT->getRelatedAbonnementEntityClass());
        self::assertSame(EvenementAbonnementRegion::class, EvenementType::REGION->getRelatedAbonnementEntityClass());
        self::assertSame(null, EvenementType::SUPER_ADMIN->getRelatedAbonnementEntityClass());
    }
}
