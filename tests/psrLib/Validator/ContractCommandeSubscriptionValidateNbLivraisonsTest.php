<?php

declare(strict_types=1);

namespace Test\Validator;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ContratCommandeSouscription;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratInformationLivraison;
use PsrLib\Validator\ContratCommandeSouscriptionValidateNbLivraisons;
use PsrLib\Validator\ContratCommandeSouscriptionValidateNbLivraisonsValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeSubscriptionValidateNbLivraisonsTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    /**
     * @dataProvider provideTestBypass
     */
    public function testBypass(int $nbLivAmapienMin, int $nbLiv, bool $result): void
    {
        $constraint = new ContratCommandeSouscriptionValidateNbLivraisons();
        $subscription = $this->prophesizeSubscription(true, $nbLivAmapienMin, $nbLiv);

        $this->validator->validate($subscription, $constraint);

        if ($result) {
            $this->assertCount(1, $this->context->getViolations());
            $this
                ->buildViolation('Vous devez sélectionner au moins <b>%nb%</b> livraisons')
                ->setParameter('%nb%', (string) $nbLivAmapienMin)
                ->assertRaised()
            ;
        } else {
            $this->assertNoViolation();
        }
    }

    public function provideTestBypass(): array
    {
        return [
            [0, 0, false],
            [0, 1, false],
            [1, 0, true],
            [2, 1, true],
            [1, 1, false],
            [1, 2, false],
        ];
    }

    /**
     * @dataProvider provideTestNoBypass
     */
    public function testNoBypass(int $nbLivAmapienMin, int $nbLiv, bool $result): void
    {
        $constraint = new ContratCommandeSouscriptionValidateNbLivraisons();
        $subscription = $this->prophesizeSubscription(false, $nbLivAmapienMin, $nbLiv);

        $this->validator->validate($subscription, $constraint);

        if ($result) {
            $this->assertCount(1, $this->context->getViolations());
            $this
                ->buildViolation('Vous devez sélectionner exactement <b>%nb%</b> livraisons')
                ->setParameter('%nb%', (string) $nbLivAmapienMin)
                ->assertRaised()
            ;
        } else {
            $this->assertNoViolation();
        }
    }

    public function provideTestNoBypass(): array
    {
        return [
            [0, 0, false],
            [0, 1, true],
            [1, 1, false],
            [1, 0, true],
        ];
    }

    public function prophesizeSubscription(bool $mcNblivBypass, int $nbLivAmapienMin, int $nbLiv): ContratCommandeSouscription
    {
        $infoLiv = $this->prophesize(ModeleContratInformationLivraison::class);
        $infoLiv->getNblivPlancherDepassement()->willReturn($mcNblivBypass);
        $infoLiv->getNblivPlancher()->willReturn($nbLivAmapienMin);

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getInformationLivraison()->willReturn($infoLiv->reveal());

        $subscription = $this->prophesize(ContratCommandeSouscription::class);
        $subscription->getMc()->willReturn($mc->reveal());
        $subscription->countNbLivraisons()->willReturn($nbLiv);

        return $subscription->reveal();
    }

    protected function createValidator()
    {
        return new ContratCommandeSouscriptionValidateNbLivraisonsValidator();
    }
}
