<?php

declare(strict_types=1);

namespace Test\Validator;

use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\DTO\ContratCommandeDeplacement;
use PsrLib\DTO\ContratCommandeItem;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;

/**
 * @internal
 *
 * @coversNothing
 */
trait ContractCommandeMoveTrait
{
    use ProphecyTrait;

    /**
     * @return [ObjectProphecy<ContratCommandeDeplacement>, ModeleContratDate[]]
     */
    private function prophesizeMove(array $before, array $after): array
    {
        $produits = [];
        $dates = [];

        $contrat = $this->prophesize(Contrat::class);
        for ($i = 0; $i < count($before); ++$i) {
            $date = $this->prophesize(ModeleContratDate::class)->reveal();
            $dates[] = $date;
        }
        for ($i = 0; $i < count($before[0]); ++$i) {
            $produit = $this->prophesize(ModeleContratProduit::class)->reveal();
            $produits[] = $produit;
        }

        for ($dateIndex = 0; $dateIndex < count($before); ++$dateIndex) {
            for ($productIndex = 0; $productIndex < count($before[$dateIndex]); ++$productIndex) {
                $cellule = $this->prophesize(ContratCellule::class);
                $cellule->getQuantite()->willReturn($before[$dateIndex][$productIndex]);
                $contrat->getCelluleDateProduit($produits[$productIndex], $dates[$dateIndex])->willReturn($cellule->reveal());
            }
        }

        $move = $this->prophesize(ContratCommandeDeplacement::class);

        $items = [];
        for ($dateIndex = 0; $dateIndex < count($after); ++$dateIndex) {
            for ($productIndex = 0; $productIndex < count($after[$dateIndex]); ++$productIndex) {
                $item = $this->prophesize(ContratCommandeItem::class);
                $item->getMcd()->willReturn($dates[$dateIndex]);
                $item->getMcp()->willReturn($produits[$productIndex]);
                $item->getQty()->willReturn($after[$dateIndex][$productIndex]);
                $items[] = $item->reveal();
                $move->getItem($produits[$productIndex], $dates[$dateIndex])->willReturn($item->reveal());
            }
        }

        $move->getContrat()->willReturn($contrat->reveal());
        $move->getItems()->willReturn($items);

        return [$move, $produits];
    }
}
