<?php

declare(strict_types=1);

namespace Test\Validator;

use PsrLib\DTO\ContratCommandeDeplacement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratInformationLivraison;
use PsrLib\Validator\ContratCommandeDeplacementValidateDeplacement;
use PsrLib\Validator\ContratCommandeDeplacementValidateDeplacementValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeMoveValidateDeplacementValidatorTest extends ConstraintValidatorTestCase
{
    use ContractCommandeMoveTrait;

    public function testPermissionDeplacementValid(): void
    {
        $order = $this
            ->prophesizeMoveWithPermission(
                true,
                [
                    [0, 1],
                    [0, 1],
                    [0, 0],
                ],
                [
                    [0, 0],
                    [0, 1],
                    [0, 1],
                ]
            )
        ;

        $constraint = new ContratCommandeDeplacementValidateDeplacement();

        $this->validator->validate($order, $constraint);

        $this->assertNoViolation();
    }

    public function testPermissionDeplacementInvalid(): void
    {
        $order = $this
            ->prophesizeMoveWithPermission(
                false,
                [
                    [0, 1],
                    [0, 1],
                    [0, 0],
                ],
                [
                    [0, 0],
                    [0, 1],
                    [0, 1],
                ]
            )
        ;

        $constraint = new ContratCommandeDeplacementValidateDeplacement();

        $this->validator->validate($order, $constraint);

        $this->assertCount(1, $this->context->getViolations());
        $this
            ->buildViolation('Impossible de déplacer une livraison sur une date sans livraison.')
            ->assertRaised()
        ;
    }

    private function prophesizeMoveWithPermission(bool $permissionDeplacement, array $before, array $after): ContratCommandeDeplacement
    {
        $mcInfoLiv = $this->prophesize(ModeleContratInformationLivraison::class);
        $mcInfoLiv->getAmapienPermissionDeplacementLivraison()->willReturn($permissionDeplacement);

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getInformationLivraison()->willReturn($mcInfoLiv->reveal());

        [$move, $produits] = $this->prophesizeMove($before, $after);
        $move->getMc()->willReturn($mc->reveal());

        return $move->reveal();
    }

    protected function createValidator()
    {
        return new ContratCommandeDeplacementValidateDeplacementValidator();
    }
}
