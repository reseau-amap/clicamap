<?php

declare(strict_types=1);

namespace Test\Validator;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ContratCommandeDeplacement;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Validator\ContratCommandeDeplacementValidateTotalSame;
use PsrLib\Validator\ContratCommandeDeplacementValidateTotalSameValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeMoveValidateTotalSameValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    public function testConstraint(): void
    {
        $p1 = $this->prophesize(ModeleContratProduit::class)->reveal();
        $p2Prophecy = $this->prophesize(ModeleContratProduit::class);
        $p2Prophecy->getNom()->willReturn('p2');
        $p2 = $p2Prophecy->reveal();

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getProduits()->willReturn([$p1, $p2]);

        $contrat = $this->prophesize(Contrat::class);
        $contrat->totalQtyByProduct($p1)->willReturn(10);
        $contrat->totalQtyByProduct($p2)->willReturn(20);

        $order = $this->prophesize(ContratCommandeDeplacement::class);
        $order->getMc()->willReturn($mc->reveal());
        $order->getContrat()->willReturn($contrat->reveal());
        $order->totalQtyByMcp($p1)->willReturn(10);
        $order->totalQtyByMcp($p2)->willReturn(21);

        $constraint = new ContratCommandeDeplacementValidateTotalSame();

        $this->validator->validate($order->reveal(), $constraint);

        $this->assertCount(1, $this->context->getViolations());
        $this->buildViolation('La quantité total de produit "{{ produit }}" doit être la même que dans le contrat')
            ->setParameter('{{ produit }}', 'p2')
            ->assertRaised()
        ;
    }

    protected function createValidator()
    {
        return new ContratCommandeDeplacementValidateTotalSameValidator();
    }
}
