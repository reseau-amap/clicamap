<?php

declare(strict_types=1);

namespace Test\Validator;

use Money\Money;
use PsrLib\Validator\MoneyGreaterThanOrEqual;
use PsrLib\Validator\MoneyGreaterThanOrEqualValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class MoneyGreaterThanOrEqualValidatorTest extends ConstraintValidatorTestCase
{
    public function testValidateValueNotMoney(): void
    {
        $this->expectException(\Assert\InvalidArgumentException::class);
        $this->validator->validate('invalid', new MoneyGreaterThanOrEqual(['value' => 0]));
    }

    public function testValidateValueMoneyCompatedInvalid(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validate(Money::EUR(0), new MoneyGreaterThanOrEqual(['value' => 'invalid']));
    }

    public function testValidateValueNull(): void
    {
        $this->validator->validate(null, new MoneyGreaterThanOrEqual(['value' => 0]));
        $this->assertNoViolation();
    }

    public function testValidateValueEqual(): void
    {
        $this->validator->validate(Money::EUR(0), new MoneyGreaterThanOrEqual(['value' => 0]));
        $this->assertNoViolation();
    }

    public function testValidateValueGreater(): void
    {
        $this->validator->validate(Money::EUR(1), new MoneyGreaterThanOrEqual(['value' => 0]));
        $this->assertNoViolation();
    }

    public function testValidateValueLess(): void
    {
        $this->validator->validate(Money::EUR(0), new MoneyGreaterThanOrEqual(['value' => 1]));
        $violations = $this->context->getViolations();
        self::assertCount(1, $violations);
        self::assertSame('Cette valeur doit être supérieure ou égale à {{ compared_value }}.', $violations[0]->getMessage());
    }

    protected function createValidator()
    {
        return new MoneyGreaterThanOrEqualValidator();
    }
}
