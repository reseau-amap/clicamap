<?php

declare(strict_types=1);

namespace Test\Validator;

use PsrLib\DTO\ContratCommandeDeplacement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratInformationLivraison;
use PsrLib\Validator\ContratCommandeDeplacementValidateReport;
use PsrLib\Validator\ContratCommandeDeplacementValidateReportValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeMoveValidateReportValidatorTest extends ConstraintValidatorTestCase
{
    use ContractCommandeMoveTrait;

    public function testPermissionReportValid(): void
    {
        $order = $this
            ->prophesizeMoveWithPermission(
                true,
                [
                    [0, 1],
                    [0, 1],
                    [0, 0],
                ],
                [
                    [0, 0],
                    [0, 2],
                    [0, 0],
                ]
            )
        ;

        $constraint = new ContratCommandeDeplacementValidateReport();

        $this->validator->validate($order, $constraint);

        $this->assertNoViolation();
    }

    public function testPermissionReportInvalid(): void
    {
        $order = $this
            ->prophesizeMoveWithPermission(
                false,
                [
                    [0, 1],
                    [0, 1],
                    [0, 0],
                ],
                [
                    [0, 0],
                    [0, 2],
                    [0, 0],
                ]
            )
        ;

        $constraint = new ContratCommandeDeplacementValidateReport();

        $this->validator->validate($order, $constraint);

        $this->assertCount(1, $this->context->getViolations());
        $this
            ->buildViolation('Impossible de reporter une livraison sur une date ayant déjà une livraison.')
            ->assertRaised()
        ;
    }

    private function prophesizeMoveWithPermission(bool $permissionReport, array $before, array $after): ContratCommandeDeplacement
    {
        $mcInfoLiv = $this->prophesize(ModeleContratInformationLivraison::class);
        $mcInfoLiv->getAmapienPermissionReportLivraison()->willReturn($permissionReport);

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getInformationLivraison()->willReturn($mcInfoLiv->reveal());

        [$move, $produits] = $this->prophesizeMove($before, $after);
        $move->getMc()->willReturn($mc->reveal());

        return $move->reveal();
    }

    protected function createValidator()
    {
        return new ContratCommandeDeplacementValidateReportValidator();
    }
}
