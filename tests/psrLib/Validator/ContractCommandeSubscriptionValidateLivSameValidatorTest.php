<?php

declare(strict_types=1);

namespace Test\Validator;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ContratCommandeBase;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratInformationLivraison;
use PsrLib\Validator\ContratCommandeValidateLivSame;
use PsrLib\Validator\ContratCommandeValidateLivSameValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCommandeSubscriptionValidateLivSameValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    /**
     * @dataProvider provideTestConstraint
     */
    public function testConstraint(bool $livSame, bool $produitsIdentiqueAmapien, bool $result): void
    {
        $constraint = new ContratCommandeValidateLivSame();
        $subscription = $this->prophesizeCommande($livSame, $produitsIdentiqueAmapien);

        $this->validator->validate($subscription, $constraint);

        if ($result) {
            $this
                ->buildViolation('Toutes les livraisons doivent être identiques')
                ->assertRaised()
            ;
        } else {
            $this->assertNoViolation();
        }
    }

    public function provideTestConstraint(): array
    {
        return [
            [true, true, false],
            [true, false, false],
            [false, true, true],
            [false, false, false],
        ];
    }

    public function prophesizeCommande(bool $livSame, bool $produitsIdentiqueAmapien): ContratCommandeBase
    {
        $infoLiv = $this->prophesize(ModeleContratInformationLivraison::class);
        $infoLiv->getProduitsIdentiqueAmapien()->willReturn($produitsIdentiqueAmapien);

        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getInformationLivraison()->willReturn($infoLiv->reveal());

        $subscription = $this->prophesize(ContratCommandeBase::class);
        $subscription->areLivSame()->willReturn($livSame);
        $subscription->getMc()->willReturn($mc->reveal());

        return $subscription->reveal();
    }

    protected function createValidator()
    {
        return new ContratCommandeValidateLivSameValidator();
    }
}
