<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Services\EntityBuilder\ContratLivraisonFactory;

/**
 * @internal
 *
 * @coversNothing
 */
class ContratLivraisonFactoryTest extends TestCase
{
    use ProphecyTrait;

    private \PsrLib\Services\EntityBuilder\ContratLivraisonFactory $factrory;

    public function setUp(): void
    {
        $this->factrory = new ContratLivraisonFactory();
    }

    public function testCreateMultipleContracts(): void
    {
        $amapien = $this->prophesize(Amapien::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $date = new Carbon('2000-01-01');

        $cellule11 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 1.0);
        $c1 = $this->prophesize(Contrat::class);
        $c1->getCellules()->willReturn(new ArrayCollection([$cellule11]));

        $cellule21 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 1.0);
        $c2 = $this->prophesize(Contrat::class);
        $c2->getCellules()->willReturn(new ArrayCollection([$cellule21]));

        $livraison = $this->factrory->create($amapien, $ferme, $date, [$c1->reveal(), $c2->reveal()]);
        self::assertSame($amapien, $livraison->getAmapien());
        self::assertSame($date, $livraison->getDate());
        self::assertSame($ferme, $livraison->getFerme());

        $livCellules = $livraison->getCellules();
        self::assertCount(2, $livCellules);
        self::assertSame($cellule11, $livCellules[0]->getContratCellule());
        self::assertSame($cellule21, $livCellules[1]->getContratCellule());
        self::assertNull($livCellules[0]->getQuantite());
        self::assertNull($livCellules[1]->getQuantite());
    }

    public function testCreateMultipleRegul(): void
    {
        $amapien = $this->prophesize(Amapien::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $date = new Carbon('2000-01-01');

        $cellule11 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 1.0);
        $c1 = $this->prophesize(Contrat::class);
        $c1->getCellules()->willReturn(new ArrayCollection([$cellule11]));

        $cellule21 = $this->prophetizeCellule(false, new Carbon('2000-01-01'), 1.0);
        $c2 = $this->prophesize(Contrat::class);
        $c2->getCellules()->willReturn(new ArrayCollection([$cellule21]));

        $livraison = $this->factrory->create($amapien, $ferme, $date, [$c1->reveal(), $c2->reveal()]);
        self::assertSame($amapien, $livraison->getAmapien());
        self::assertSame($date, $livraison->getDate());
        self::assertSame($ferme, $livraison->getFerme());

        $livCellules = $livraison->getCellules();
        self::assertCount(1, $livCellules);
        self::assertSame($cellule11, $livCellules[0]->getContratCellule());
        self::assertNull($livCellules[0]->getQuantite());
    }

    public function testCreateMultipleDate(): void
    {
        $amapien = $this->prophesize(Amapien::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $date = new Carbon('2000-01-01');

        $cellule11 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 1.0);
        $c1 = $this->prophesize(Contrat::class);
        $c1->getCellules()->willReturn(new ArrayCollection([$cellule11]));

        $cellule21 = $this->prophetizeCellule(true, new Carbon('2000-01-02'), 1.0);
        $c2 = $this->prophesize(Contrat::class);
        $c2->getCellules()->willReturn(new ArrayCollection([$cellule21]));

        $livraison = $this->factrory->create($amapien, $ferme, $date, [$c1->reveal(), $c2->reveal()]);
        self::assertSame($amapien, $livraison->getAmapien());
        self::assertSame($date, $livraison->getDate());
        self::assertSame($ferme, $livraison->getFerme());

        $livCellules = $livraison->getCellules();
        self::assertCount(1, $livCellules);
        self::assertSame($cellule11, $livCellules[0]->getContratCellule());
        self::assertNull($livCellules[0]->getQuantite());
    }

    public function testHideCelluleZero(): void
    {
        $amapien = $this->prophesize(Amapien::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $date = new Carbon('2000-01-01');

        $cellule11 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 1.0);
        $cellule12 = $this->prophetizeCellule(true, new Carbon('2000-01-01'), 0.0);
        $c1 = $this->prophesize(Contrat::class);
        $c1->getCellules()->willReturn(new ArrayCollection([$cellule11, $cellule12]));

        $livraison = $this->factrory->create($amapien, $ferme, $date, [$c1->reveal()]);
        self::assertSame($amapien, $livraison->getAmapien());
        self::assertSame($date, $livraison->getDate());
        self::assertSame($ferme, $livraison->getFerme());

        $livCellules = $livraison->getCellules();
        self::assertCount(1, $livCellules);
        self::assertSame($cellule11, $livCellules[0]->getContratCellule());
        self::assertNull($livCellules[0]->getQuantite());
    }

    private function prophetizeCellule(bool $regul, Carbon $date, float $quantite)
    {
        $cellule = $this->prophesize(ContratCellule::class);
        $cellule->getQuantite()->willReturn($quantite);

        $mcPro = $this->prophesize(ModeleContratProduit::class);
        $mcPro->getRegulPds()->willReturn($regul);
        $cellule->getModeleContratProduit()->willReturn($mcPro->reveal());

        $mcDate = $this->prophesize(ModeleContratDate::class);
        $mcDate->getDateLivraison()->willReturn($date);
        $cellule->getModeleContratDate()->willReturn($mcDate->reveal());

        return $cellule->reveal();
    }
}
