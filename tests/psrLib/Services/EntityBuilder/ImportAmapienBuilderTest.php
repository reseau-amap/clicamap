<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\ORM\Entity\ActivableUserInterface;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\ORM\Repository\UserRepository;
use PsrLib\ORM\Repository\VilleRepository;
use PsrLib\Services\EntityBuilder\ImportAmapienBuilder;
use PsrLib\Services\UsernameGenerator;

/**
 * @internal
 *
 * @coversNothing
 */
class ImportAmapienBuilderTest extends TestCase
{
    use ProphecyTrait;

    private ImportAmapienBuilder $importAmapienBuilder;

    /**
     * @var ObjectProphecy<VilleRepository>
     */
    private ObjectProphecy $villeRepo;

    /**
     * @var ObjectProphecy<UserRepository>
     */
    private ObjectProphecy $userRepo;

    /**
     * @var ObjectProphecy<UsernameGenerator>
     */
    private ObjectProphecy $usernameGenerator;

    /**
     * @var ObjectProphecy<AmapienRepository>
     */
    private ObjectProphecy $amapienRepo;

    public function setUp(): void
    {
        $this->villeRepo = $this->prophesize(VilleRepository::class);
        $this->userRepo = $this->prophesize(UserRepository::class);
        $this->usernameGenerator = $this->prophesize(UsernameGenerator::class);
        $this->amapienRepo = $this->prophesize(AmapienRepository::class);

        $this->importAmapienBuilder = new ImportAmapienBuilder(
            $this->villeRepo->reveal(),
            $this->userRepo->reveal(),
            $this->usernameGenerator->reveal(),
            $this->amapienRepo->reveal(),
        );
    }

    public function testExistingUser(): void
    {
        $values = [[
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'yes',
        ]];
        $existingUser = $this->prophesize(User::class)->reveal();
        $this->userRepo->getOneByEmail('john.doe@example.com')->willReturn($existingUser);

        $amap = $this->prophesize(Amap::class)->reveal();
        $amapiens = $this->importAmapienBuilder->buildAmapiensFromImport($values, $amap);

        $this->assertCount(1, $amapiens);
        $this->assertSame($existingUser, $amapiens[0]->getUser());
        $this->assertSame($amap, $amapiens[0]->getAmap());
        $this->assertSame(2022, $amapiens[0]->getAnneeAdhesions()[0]->getAnnee());
        $this->assertSame(ActivableUserInterface::ETAT_INACTIF, $amapiens[0]->getEtat());
    }

    public function testNewUser(): void
    {
        $values = [[
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'oui',
        ]];
        $this->userRepo->getOneByEmail('john.doe@example.com')->willReturn(null);
        $this->userRepo->count(['username' => 'johndoe'])->willReturn(0);
        $this->usernameGenerator->generate('John', 'Doe')->willReturn('johndoe');

        $amap = $this->prophesize(Amap::class)->reveal();
        $amapiens = $this->importAmapienBuilder->buildAmapiensFromImport($values, $amap);

        $this->assertCount(1, $amapiens);
        $this->assertSame($amap, $amapiens[0]->getAmap());
        $this->assertSame(2022, $amapiens[0]->getAnneeAdhesions()[0]->getAnnee());
        $this->assertSame(ActivableUserInterface::ETAT_INACTIF, $amapiens[0]->getEtat());

        $this->assertSame('John', $amapiens[0]->getUser()->getName()->getFirstName());
        $this->assertSame('Doe', $amapiens[0]->getUser()->getName()->getLastName());
        $this->assertSame('johndoe', $amapiens[0]->getUser()->getUsername());
        $this->assertSame('1234567890', $amapiens[0]->getUser()->getNumTel1());
        $this->assertSame('123 Street', $amapiens[0]->getUser()->getAddress()->getAdress());
        $this->assertTrue($amapiens[0]->getUser()->getNewsletter());
    }

    public function testExistingUserInDb(): void
    {
        $values = [[
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'oui',
        ]];
        $this->userRepo->getOneByEmail('john.doe@example.com')->willReturn(null);
        $this->userRepo->count(['username' => 'johndoe'])->willReturn(1);
        $this->userRepo->count(['username' => 'johndoe2'])->willReturn(1);
        $this->userRepo->count(['username' => 'johndoe3'])->willReturn(0);
        $this->usernameGenerator->generate('John', 'Doe')->willReturn('johndoe');

        $amap = $this->prophesize(Amap::class)->reveal();
        $amapiens = $this->importAmapienBuilder->buildAmapiensFromImport($values, $amap);

        $this->assertCount(1, $amapiens);
        $this->assertSame($amap, $amapiens[0]->getAmap());
        $this->assertSame(2022, $amapiens[0]->getAnneeAdhesions()[0]->getAnnee());
        $this->assertSame(ActivableUserInterface::ETAT_INACTIF, $amapiens[0]->getEtat());

        $this->assertSame('John', $amapiens[0]->getUser()->getName()->getFirstName());
        $this->assertSame('Doe', $amapiens[0]->getUser()->getName()->getLastName());
        $this->assertSame('johndoe3', $amapiens[0]->getUser()->getUsername());
        $this->assertSame('1234567890', $amapiens[0]->getUser()->getNumTel1());
        $this->assertSame('123 Street', $amapiens[0]->getUser()->getAddress()->getAdress());
        $this->assertTrue($amapiens[0]->getUser()->getNewsletter());
    }

    public function testDuplicatedUsernameGenerated(): void
    {
        $values = [[
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'oui',
        ], [
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe2@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'oui',
        ]];
        $this->userRepo->getOneByEmail('john.doe@example.com')->willReturn(null);
        $this->userRepo->getOneByEmail('john.doe2@example.com')->willReturn(null);
        $this->userRepo->count(['username' => 'johndoe'])->willReturn(0);
        $this->userRepo->count(['username' => 'johndoe2'])->willReturn(0);
        $this->usernameGenerator->generate('John', 'Doe')->willReturn('johndoe');

        $amap = $this->prophesize(Amap::class)->reveal();
        $amapiens = $this->importAmapienBuilder->buildAmapiensFromImport($values, $amap);

        $this->assertCount(2, $amapiens);
        $this->assertSame($amap, $amapiens[0]->getAmap());
        $this->assertSame(2022, $amapiens[0]->getAnneeAdhesions()[0]->getAnnee());
        $this->assertSame(ActivableUserInterface::ETAT_INACTIF, $amapiens[0]->getEtat());
        $this->assertSame('John', $amapiens[0]->getUser()->getName()->getFirstName());
        $this->assertSame('Doe', $amapiens[0]->getUser()->getName()->getLastName());
        $this->assertSame('johndoe', $amapiens[0]->getUser()->getUsername());
        $this->assertSame('1234567890', $amapiens[0]->getUser()->getNumTel1());
        $this->assertSame('123 Street', $amapiens[0]->getUser()->getAddress()->getAdress());
        $this->assertTrue($amapiens[0]->getUser()->getNewsletter());

        $this->assertSame($amap, $amapiens[1]->getAmap());
        $this->assertSame(2022, $amapiens[1]->getAnneeAdhesions()[0]->getAnnee());
        $this->assertSame(ActivableUserInterface::ETAT_INACTIF, $amapiens[1]->getEtat());
        $this->assertSame('John', $amapiens[1]->getUser()->getName()->getFirstName());
        $this->assertSame('Doe', $amapiens[1]->getUser()->getName()->getLastName());
        $this->assertSame('johndoe2', $amapiens[1]->getUser()->getUsername());
        $this->assertSame('1234567890', $amapiens[1]->getUser()->getNumTel1());
        $this->assertSame('123 Street', $amapiens[1]->getUser()->getAddress()->getAdress());
        $this->assertTrue($amapiens[1]->getUser()->getNewsletter());
    }

    public function testDeletedAmapien(): void
    {
        $values = [[
            'A' => 'John',
            'B' => 'Doe',
            'C' => 'john.doe@example.com',
            'D' => '1234567890',
            'E' => '123 Street',
            'F' => '12345',
            'G' => 'City',
            'H' => '2022',
            'I' => 'oui',
        ]];
        $user = $this->prophesize(User::class)->reveal();
        $this->userRepo->getOneByEmail('john.doe@example.com')->willReturn($user);

        $deleteAmapien = $this->prophesize(Amapien::class);
        $deleteAmapien->setDeletedAt(null)->shouldBeCalled();
        $amap = $this->prophesize(Amap::class)->reveal();
        $this->amapienRepo->findOneDeletedByAmapUser($amap, $user)->willReturn($deleteAmapien->reveal());

        $amapiens = $this->importAmapienBuilder->buildAmapiensFromImport($values, $amap);

        $this->assertCount(1, $amapiens);
        $this->assertSame($deleteAmapien->reveal(), $amapiens[0]);
    }
}
