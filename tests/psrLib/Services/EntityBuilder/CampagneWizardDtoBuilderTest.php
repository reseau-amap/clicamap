<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\DTO\UserRepositoryReseauAdminFromAmapDTO;
use PsrLib\Exception\CampagneWizardDtoBuilderNoAdminException;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\Files\AmapLogo;
use PsrLib\ORM\Entity\Files\Logo;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use PsrLib\ORM\Entity\Ville;
use PsrLib\Services\EntityBuilder\CampagneWizardDtoBuilder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneWizardDtoBuilderTest extends TestCase
{
    use ProphecyTrait;

    private \PsrLib\Services\EntityBuilder\CampagneWizardDtoBuilder $builder;
    private ObjectProphecy $userRepository;
    private ObjectProphecy $fs;

    public function setUp(): void
    {
        $this->userRepository = $this->prophesize(\PsrLib\ORM\Repository\UserRepository::class);
        $this->fs = $this->prophesize(Filesystem::class);
        $this->builder = new CampagneWizardDtoBuilder(
            $this->userRepository->reveal(),
            $this->fs->reveal()
        );
    }

    public function testAmapPasDeFaitNoLogo(): void
    {
        $amap = $this->prophesizeAmap(false)->reveal();

        $dto = $this->builder->createNew($amap);
        self::assertSame($amap, $dto->getCampagne()->getAmap());

        $this->expectAmapAutheurInfo($dto->getCampagne());
        self::assertNull($dto->getCampagne()->getAutheurLogo());
        self::assertCount(1, $dto->getCampagne()->getMontants());
    }

    public function testAmapPasDeFaitLogo(): void
    {
        $amap = $this->prophesizeAmap(false);
        $logo = $this->prophesizeLogo(AmapLogo::class);
        $amap->getLogo()->willReturn($logo);
        $amapReveled = $amap->reveal();

        $dto = $this->builder->createNew($amapReveled);
        self::assertSame($amapReveled, $dto->getCampagne()->getAmap());
        $this->expectAmapAutheurInfo($dto->getCampagne());
        self::assertSame('fileName', $dto->getCampagne()->getAutheurLogo()->getFileName());
        self::assertSame('originalFileName', $dto->getCampagne()->getAutheurLogo()->getOriginalFileName());
        self::assertCount(1, $dto->getCampagne()->getMontants());
    }

    public function testAmapDeFaitAdmin(): void
    {
        $amap = $this->prophesizeAmap(true)->reveal();

        $reseauAdmin = $this->prophesize(UserRepositoryReseauAdminFromAmapDTO::class);

        $departement = $this->prophesize(\PsrLib\ORM\Entity\Departement::class);
        $departement->getLogo()->willReturn(null);
        $reseauAdmin->getLocation()->willReturn($departement->reveal());

        $user = $this->prophesizeUserAdmin();
        $reseauAdmin->getUser()->willReturn($user);

        $this->userRepository->getReseauAdminFromAmap($amap)->willReturn($reseauAdmin->reveal());

        $dto = $this->builder->createNew($amap);
        self::assertSame($amap, $dto->getCampagne()->getAmap());

        $this->expectReeauAutheurInfo($dto->getCampagne());
        self::assertNull($dto->getCampagne()->getAutheurLogo());
        self::assertCount(1, $dto->getCampagne()->getMontants());
    }

    public function testAmapDeFaitAdminLogo(): void
    {
        $amap = $this->prophesizeAmap(true)->reveal();

        $reseauAdmin = $this->prophesize(UserRepositoryReseauAdminFromAmapDTO::class);

        $departement = $this->prophesize(\PsrLib\ORM\Entity\Departement::class);
        $departement->getLogo()->willReturn($this->prophesizeLogo(Logo::class));
        $reseauAdmin->getLocation()->willReturn($departement->reveal());

        $user = $this->prophesizeUserAdmin();
        $reseauAdmin->getUser()->willReturn($user);

        $this->userRepository->getReseauAdminFromAmap($amap)->willReturn($reseauAdmin->reveal());

        $dto = $this->builder->createNew($amap);
        self::assertSame($amap, $dto->getCampagne()->getAmap());
        $this->expectReeauAutheurInfo($dto->getCampagne());
        self::assertCount(1, $dto->getCampagne()->getMontants());
    }

    public function testAmapDeFaitNoAdmin(): void
    {
        $amap = $this->prophesizeAmap(true)->reveal();

        $this->userRepository->getReseauAdminFromAmap($amap)->willReturn(null);

        self::expectException(CampagneWizardDtoBuilderNoAdminException::class);
        $this->builder->createNew($amap);
    }

    private function prophesizeUserAdmin(): User
    {
        $adhesionInfo = $this->prophesize(\PsrLib\ORM\Entity\AdhesionInfo::class);
        $adhesionInfo->getNomReseau()->willReturn('RESEAU Nom');
        $adhesionInfo->getSite()->willReturn('RESEAU site');
        $adhesionInfo->getSiret()->willReturn('RESEAU siret');
        $adhesionInfo->getRna()->willReturn('RESEAU rna');

        $ville = $this->prophesize(Ville::class);
        $ville->getNom()->willReturn('RESEAU ville');
        $ville->getCpString()->willReturn('RESEAU cp');

        $adress = $this->prophesize(\PsrLib\ORM\Entity\Embeddable\Address::class);
        $adress->getAdress()->willReturn('RESEAU adress');
        $adress->getAdressCompl()->willReturn('RESEAU adress compl');

        $user = $this->prophesize(\PsrLib\ORM\Entity\User::class);
        $user->getAddress()->willReturn($adress->reveal());
        $user->getNumTel1()->willReturn('RESEAU tel1');
        $user->getAdhesionInfo()->willReturn($adhesionInfo->reveal());
        $user->getVille()->willReturn($ville->reveal());
        $user->getEmails()->willReturn(
            new ArrayCollection([$this->prophesizeUserEmail('RESEAU mail 1'), $this->prophesizeUserEmail('RESEAU mail 2')])
        );

        return $user->reveal();
    }

    private function prophesizeLogo(string $class)
    {
        $logo = $this->prophesize($class);
        $logo->getFileName()->willReturn('fileName');
        $logo->getOriginalFileName()->willReturn('originalFileName');
        $logo->getFileFullPath()->willReturn('fileFullPath');

        return $logo->reveal();
    }

    private function prophesizeAmap(bool $amapDeFait): ObjectProphecy
    {
        $amap = $this->prophesize(\PsrLib\ORM\Entity\Amap::class);
        $amap->getAssociationDeFait()->willReturn($amapDeFait);
        $amap->getNom()->willReturn('AMAP Nom');
        $amap->getLogo()->willReturn(null);
        $amap->getUrl()->willReturn('AMAP url');
        $amap->getSiret()->willReturn('AMAP siret');
        $amap->getRna()->willReturn('AMAP rna');
        $amap->getEmail()->willReturn('AMAP email');
        $amap->getAdresseAdmin()->willReturn('AMAP address admin');

        $adresseAdminVille = $this->prophesize(Ville::class);
        $adresseAdminVille->getCpString()->willReturn('AMAP cp');
        $adresseAdminVille->getNom()->willReturn('AMAP ville');
        $amap->getAdresseAdminVille()->willReturn($adresseAdminVille->reveal());

        return $amap;
    }

    private function expectAmapAutheurInfo(Campagne $campagne): void
    {
        self::assertSame('AMAP Nom', $campagne->getAutheurInfo()->getNom());
        self::assertSame('AMAP url', $campagne->getAutheurInfo()->getSite());
        self::assertSame('AMAP siret', $campagne->getAutheurInfo()->getSiret());
        self::assertSame('AMAP rna', $campagne->getAutheurInfo()->getRna());
        self::assertSame('AMAP email', $campagne->getAutheurInfo()->getEmails());
        self::assertSame('AMAP address admin AMAP cp, AMAP ville', $campagne->getAutheurInfo()->getAdresse());
        self::assertSame('AMAP ville', $campagne->getAutheurInfo()->getVille());
    }

    private function expectReeauAutheurInfo(Campagne $campagne): void
    {
        self::assertSame('RESEAU Nom', $campagne->getAutheurInfo()->getNom());
        self::assertSame('RESEAU site', $campagne->getAutheurInfo()->getSite());
        self::assertSame('RESEAU siret', $campagne->getAutheurInfo()->getSiret());
        self::assertSame('RESEAU rna', $campagne->getAutheurInfo()->getRna());
        self::assertSame('RESEAU mail 1', $campagne->getAutheurInfo()->getEmails());
        self::assertSame('RESEAU adress RESEAU adress compl RESEAU cp, RESEAU ville', $campagne->getAutheurInfo()->getAdresse());
        self::assertSame('RESEAU ville', $campagne->getAutheurInfo()->getVille());
    }

    private function prophesizeUserEmail(string $email): UserEmail
    {
        $userEmail = $this->prophesize(\PsrLib\ORM\Entity\UserEmail::class);
        $userEmail->getEmail()->willReturn($email);

        return $userEmail->reveal();
    }
}
