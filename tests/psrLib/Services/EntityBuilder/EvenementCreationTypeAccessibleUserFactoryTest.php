<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\EvenementCreationTypeAccessibleUserFactory;

/**
 * @internal
 *
 * @coversNothing
 */
class EvenementCreationTypeAccessibleUserFactoryTest extends TestCase
{
    use ProphecyTrait;

    private EvenementCreationTypeAccessibleUserFactory $factory;

    public function setUp(): void
    {
        $this->factory = new EvenementCreationTypeAccessibleUserFactory();
    }

    /**
     * @dataProvider getTypeAccessibleForUserProvider
     */
    public function testGetTypeAccessibleForUser(bool $isRefProduit, bool $isPaysan, bool $isAmapAdmin, bool $isAdminDepartment, bool $isAdminRegion, bool $isSuperAdmin, array $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isRefProduit()->willReturn($isRefProduit);
        $user->isPaysan()->willReturn($isPaysan);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);
        $user->isAdminDepartment()->willReturn($isAdminDepartment);
        $user->isAdminRegion()->willReturn($isAdminRegion);
        $user->isSuperAdmin()->willReturn($isSuperAdmin);

        $res = $this->factory->getTypeAccessibleForUser($user->reveal());
        self::assertSame($expected, $res);
    }

    public function getTypeAccessibleForUserProvider()
    {
        return [
            [true, false, false, false, false, false, [EvenementType::AMAP]],
            [false, true, false, false, false, false, [EvenementType::FERME]],
            [false, false, true, false, false, false, [EvenementType::AMAP]],
            [false, false, false, true,  false, false, [EvenementType::DEPARTEMENT]],
            [false, false, false, false, true, false, [EvenementType::DEPARTEMENT, EvenementType::REGION]],
            [false, false, false, false,  false, false, []],
            [false, true, true, false,  false, false, [EvenementType::FERME, EvenementType::AMAP]],
            [false, false, false, false,  false, true, [EvenementType::SUPER_ADMIN, EvenementType::REGION, EvenementType::DEPARTEMENT]],
        ];
    }
}
