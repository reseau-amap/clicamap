<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use Carbon\CarbonImmutable;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\Enum\ModeleContratEtat;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\TypeProduction;
use PsrLib\Services\EntityBuilder\ModeleContratCopy;
use PsrLib\Services\PhpDiContrainerSingleton;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratCopyTest extends TestCase
{
    use ProphecyTrait;

    private ModeleContratCopy $contratCopy;

    public function setUp(): void
    {
        $this->contratCopy = new ModeleContratCopy(
            PhpDiContrainerSingleton::getContainer()->get(PropertyAccessor::class)
        );
    }

    public function tearDown(): void
    {
        CarbonImmutable::setTestNow();
    }

    public function testCopyBase(): void
    {
        $ll = $this->prophesize(AmapLivraisonLieu::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();

        $mcOrig = new ModeleContrat();
        $mcOrig->setNom('Nom');
        $mcOrig->setEtat(ModeleContratEtat::ETAT_ACTIF);
        $mcOrig->setDescription('Description');
        $mcOrig->setLivraisonLieu($ll);
        $mcOrig->setFrequence(7);
        $mcOrig->setVersion(2);
        $mcOrig->setFiliere('Filiere');
        $mcOrig->setSpecificite('Specificite');
        $mcOrig->setRegulPoid(true);
        $mcOrig->setAmapienPermissionIntervenirPlanning(true);
        $mcOrig->setReglementType(['type' => 'value']);
        $mcOrig->setReglementModalite('ReglementModalite');
        $mcOrig->setContratAnnexes('ContratAnnexes');
        $mcOrig->setFerme($ferme);
        $mcOrig->setDelaiModifContrat(2);

        $mcOrigInfoLivraison = $mcOrig->getInformationLivraison();
        $mcOrigInfoLivraison->setNblivPlancher(1);
        $mcOrigInfoLivraison->setNblivPlancherDepassement(false);
        $mcOrigInfoLivraison->setProduitsIdentiqueAmapien(true);
        $mcOrigInfoLivraison->setProduitsIdentiquePaysan(true);
        $mcOrigInfoLivraison->setAmapienPermissionDeplacementLivraison(true);
        $mcOrigInfoLivraison->setAmapienDeplacementMode('AmapienDeplacementMode');
        $mcOrigInfoLivraison->setAmapienDeplacementNb(3);
        $mcOrigInfoLivraison->setAmapienPermissionReportLivraison(true);

        $mcNew = $this->contratCopy->copy($mcOrig);

        self::assertSame('Copie de Nom', $mcNew->getNom());
        self::assertSame(ModeleContratEtat::ETAT_BROUILLON, $mcNew->getEtat());
        self::assertSame('Description', $mcNew->getDescription());
        self::assertSame($ll, $mcNew->getLivraisonLieu());
        self::assertSame(7, $mcNew->getFrequence());
        self::assertSame(2, $mcNew->getVersion());
        self::assertSame('Filiere', $mcNew->getFiliere());
        self::assertSame('Specificite', $mcNew->getSpecificite());
        self::assertTrue($mcNew->getRegulPoid());
        self::assertTrue($mcNew->getAmapienPermissionIntervenirPlanning());
        self::assertSame(['type' => 'value'], $mcNew->getReglementType());
        self::assertSame('ReglementModalite', $mcNew->getReglementModalite());
        self::assertSame('ContratAnnexes', $mcNew->getContratAnnexes());
        self::assertSame($ferme, $mcNew->getFerme());
        self::assertSame(2, $mcNew->getDelaiModifContrat());

        $mcNewInfoLivraison = $mcNew->getInformationLivraison();
        self::assertTrue($mcNewInfoLivraison->getAmapienPermissionReportLivraison());
        self::assertSame(1, $mcNewInfoLivraison->getNblivPlancher());
        self::assertSame(false, $mcNewInfoLivraison->getNblivPlancherDepassement());
        self::assertTrue($mcNewInfoLivraison->getProduitsIdentiqueAmapien());
        self::assertTrue($mcNewInfoLivraison->getProduitsIdentiquePaysan());
        self::assertTrue($mcNewInfoLivraison->getAmapienPermissionDeplacementLivraison());
        self::assertSame('AmapienDeplacementMode', $mcNewInfoLivraison->getAmapienDeplacementMode());
        self::assertSame(3, $mcNewInfoLivraison->getAmapienDeplacementNb());
    }

    public function testProductCopySame(): void
    {
        $tp = $this->prophesize(TypeProduction::class)->reveal();
        $fermeProduitPrixTva = new PrixTVA();
        $fermeProduitPrixTva->setPrixTTC(Money::EUR(100));
        $fermeProduitPrixTva->setTva(10);

        $fermeProduit = new FermeProduit();
        $fermeProduit->setPrix($fermeProduitPrixTva);

        $mcProduitPrixTva = new PrixTVA();
        $mcProduitPrixTva->setPrixTTC(Money::EUR(100));
        $mcProduitPrixTva->setTva(10);
        $mcProduit = new ModeleContratProduit();
        $mcProduit->setPrix($mcProduitPrixTva);
        $mcProduit->setFermeProduit($fermeProduit);
        $mcProduit->setNom('Produit nom');
        $mcProduit->setConditionnement('produit conditionnement');
        $mcProduit->setRegulPds(false);
        $mcProduit->setTypeProduction($tp);

        $mcOrig = new ModeleContrat();
        $mcOrig->addProduit($mcProduit);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(1, $mcNew->getProduits());

        /** @var ModeleContratProduit $mcNewProduit */
        $mcNewProduit = $mcNew->getProduits()->first();
        self::assertTrue($mcNewProduit->getPrix()->getPrixTTC()->equals(Money::EUR(100)));
        self::assertSame(10.0, $mcNewProduit->getPrix()->getTva());
        self::assertSame($fermeProduit, $mcNewProduit->getFermeProduit());
        self::assertSame('Produit nom', $mcNewProduit->getNom());
        self::assertSame('produit conditionnement', $mcNewProduit->getConditionnement());
        self::assertFalse($mcNewProduit->getRegulPds());
        self::assertSame($tp, $mcNewProduit->getTypeProduction());
    }

    public function testProductWithNewPrice(): void
    {
        $fermeProduitPrixTva = new PrixTVA();
        $fermeProduitPrixTva->setPrixTTC(Money::EUR(200));
        $fermeProduitPrixTva->setTva(20);

        $fermeProduit = new FermeProduit();
        $fermeProduit->setPrix($fermeProduitPrixTva);

        $mcProduitPrixTva = new PrixTVA();
        $mcProduitPrixTva->setPrixTTC(Money::EUR(100));
        $mcProduitPrixTva->setTva(10);
        $mcProduit = new ModeleContratProduit();
        $mcProduit->setPrix($mcProduitPrixTva);
        $mcProduit->setFermeProduit($fermeProduit);

        $mcOrig = new ModeleContrat();
        $mcOrig->addProduit($mcProduit);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(1, $mcNew->getProduits());

        /** @var ModeleContratProduit $mcNewProduit */
        $mcNewProduit = $mcNew->getProduits()->first();
        self::assertTrue($mcNewProduit->getPrix()->getPrixTTC()->equals(Money::EUR(200)));
        self::assertSame(20.0, $mcNewProduit->getPrix()->getTva());
    }

    public function testProductFermeProduitRemoved(): void
    {
        $fermeProduitPrixTva = new PrixTVA();
        $fermeProduitPrixTva->setPrixTTC(Money::EUR(200));
        $fermeProduitPrixTva->setTva(20);

        $fermeProduit = new FermeProduit();
        $fermeProduit->setPrix($fermeProduitPrixTva);

        $mcProduit1PrixTva = new PrixTVA();
        $mcProduit1PrixTva->setPrixTTC(Money::EUR(100));
        $mcProduit1PrixTva->setTva(10);
        $mcProduit1 = new ModeleContratProduit();
        $mcProduit1->setPrix($mcProduit1PrixTva);
        $mcProduit1->setFermeProduit($fermeProduit);

        $mcProduit2 = new ModeleContratProduit();
        $mcProduit2->setFermeProduit(null);

        $mcOrig = new ModeleContrat();
        $mcOrig->addProduit($mcProduit1);
        $mcOrig->addProduit($mcProduit2);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(1, $mcNew->getProduits());

        /** @var ModeleContratProduit $mcNewProduit */
        $mcNewProduit = $mcNew->getProduits()->first();
        self::assertTrue($mcNewProduit->getPrix()->getPrixTTC()->equals(Money::EUR(200)));
        self::assertSame(20.0, $mcNewProduit->getPrix()->getTva());
    }

    public function testForclusionPast(): void
    {
        $forclusion = new \DateTime('2000-01-01');
        CarbonImmutable::setTestNow(new \DateTime('2000-01-02'));

        $mcOrig = new ModeleContrat();
        $mcOrig->setForclusion($forclusion);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertNull($mcNew->getForclusion());
    }

    public function testForclusionSame(): void
    {
        $forclusion = new \DateTime('2000-01-01');
        CarbonImmutable::setTestNow(new \DateTime('2000-01-01'));

        $mcOrig = new ModeleContrat();
        $mcOrig->setForclusion($forclusion);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertNull($mcNew->getForclusion());
    }

    public function testForclusionFuture(): void
    {
        $forclusion = new \DateTime('2000-01-02');
        CarbonImmutable::setTestNow(new \DateTime('2000-01-01'));

        $mcOrig = new ModeleContrat();
        $mcOrig->setForclusion($forclusion);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertTrue(CarbonImmutable::instance($forclusion)->equalTo($mcNew->getForclusion()));
    }

    public function testForclusionNull(): void
    {
        $mcOrig = new ModeleContrat();
        $mcOrig->setForclusion(null);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertNull($mcNew->getForclusion());
    }

    public function testDateReglementCopieEmpty(): void
    {
        CarbonImmutable::setTestNow(new \DateTime('2020-01-02'));

        $mcOrig = new ModeleContrat();
        $mcOrig->setReglementNbMax(1);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(0, $mcNew->getDateReglements());
        self::assertNull($mcNew->getReglementNbMax());
    }

    public function testDateReglementCopieFirstDateBeforeNow(): void
    {
        $dateReglement1 = new ModeleContratDatesReglement();
        $dateReglement1->setDate(new \DateTime('2020-01-03'));
        $dateReglement2 = new ModeleContratDatesReglement();
        $dateReglement2->setDate(new \DateTime('2020-01-01'));

        CarbonImmutable::setTestNow(new \DateTime('2020-01-02'));

        $mcOrig = new ModeleContrat();
        $mcOrig->addDateReglement($dateReglement1);
        $mcOrig->addDateReglement($dateReglement2);
        $mcOrig->setReglementNbMax(1);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(0, $mcNew->getDateReglements());
        self::assertNull($mcNew->getReglementNbMax());
    }

    public function testDateReglementCopieFirstDateNow(): void
    {
        $dateReglement1 = new ModeleContratDatesReglement();
        $dateReglement1->setDate(new \DateTime('2020-01-02'));
        $dateReglement2 = new ModeleContratDatesReglement();
        $dateReglement2->setDate(new \DateTime('2020-01-03'));

        CarbonImmutable::setTestNow(new \DateTime('2020-01-02'));

        $mcOrig = new ModeleContrat();
        $mcOrig->addDateReglement($dateReglement1);
        $mcOrig->addDateReglement($dateReglement2);
        $mcOrig->setReglementNbMax(1);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(0, $mcNew->getDateReglements());
        self::assertNull($mcNew->getReglementNbMax());
    }

    public function testDateReglementCopieFirstDateAfterNow(): void
    {
        $dateReglement1 = new ModeleContratDatesReglement();
        $dateReglement1->setDate(new \DateTime('2020-01-03'));
        $dateReglement2 = new ModeleContratDatesReglement();
        $dateReglement2->setDate(new \DateTime('2020-01-02'));

        CarbonImmutable::setTestNow(new \DateTime('2020-01-01'));

        $mcOrig = new ModeleContrat();
        $mcOrig->addDateReglement($dateReglement1);
        $mcOrig->addDateReglement($dateReglement2);
        $mcOrig->setReglementNbMax(1);

        $mcNew = $this->contratCopy->copy($mcOrig);
        self::assertCount(2, $mcNew->getDateReglements());
        self::assertSame(1, $mcNew->getReglementNbMax());
    }
}
