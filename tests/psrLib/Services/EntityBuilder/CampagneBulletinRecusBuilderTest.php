<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\ORM\Repository\CampagneBulletinRecusRepository;
use PsrLib\Services\EntityBuilder\CampagneBulletinRecusBuilder;
use PsrLib\Services\MPdfGeneration;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinRecusBuilderTest extends TestCase
{
    use ProphecyTrait;

    private ObjectProphecy $mPdfGeneration;
    private ObjectProphecy $campagneBulletinRecusRepository;
    private CampagneBulletinRecusBuilder $campagneBulletinRecusBuilder;

    public function setUp(): void
    {
        $this->mPdfGeneration = $this->prophesize(MPdfGeneration::class);
        $this->campagneBulletinRecusRepository = $this->prophesize(CampagneBulletinRecusRepository::class);
        $this->campagneBulletinRecusBuilder = new CampagneBulletinRecusBuilder(
            $this->mPdfGeneration->reveal(),
            $this->campagneBulletinRecusRepository->reveal()
        );
    }

    public function testCreate(): void
    {
        $campagneBulletin = $this->prophesize(\PsrLib\ORM\Entity\CampagneBulletin::class);
        $campagne = $this->prophesize(\PsrLib\ORM\Entity\Campagne::class);
        $amap = $this->prophesize(\PsrLib\ORM\Entity\Amap::class);

        $amapReveled = $amap->reveal();
        $campagneBulletinReveled = $campagneBulletin->reveal();

        $campagneBulletin->getCampagne()->willReturn($campagne->reveal());
        $campagne->getAmap()->willReturn($amapReveled);
        $this->campagneBulletinRecusRepository->getNumeroInterneSuivantForAmap($amapReveled)->willReturn(2);
        $amap->getId()->willReturn(1);
        $this->mPdfGeneration->genererCampagneBulletinRecusFile($campagneBulletinReveled, '0000100002')->willReturn('recus_pdf_file_name');

        $recus = $this->campagneBulletinRecusBuilder->create($campagneBulletinReveled);

        $this->assertEquals(2, $recus->getNumeroInterne());
        $this->assertEquals('0000100002', $recus->getNumero());
        $this->assertEquals('recus_pdf_file_name', $recus->getPdf()->getFileName());
    }
}
