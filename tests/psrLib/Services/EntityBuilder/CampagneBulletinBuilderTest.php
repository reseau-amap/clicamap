<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\CampagneMontant;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\CampagneBulletinBuilder;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinBuilderTest extends TestCase
{
    use ProphecyTrait;

    private CampagneBulletinBuilder $builder;

    public function setUp(): void
    {
        $this->builder = new CampagneBulletinBuilder();
    }

    public function testCreateFilterMontant(): void
    {
        $amapien = $this->prophesize(Amapien::class)->reveal();
        $amap = $this->prophesize(Amap::class)->reveal();

        $user = $this->prophesize(User::class);
        $user->getUserAmapienAmapForAmap($amap)->willReturn($amapien);

        $campagne = $this->prophesize(\PsrLib\ORM\Entity\Campagne::class);
        $montant1 = $this->prophesizeCampagneMontant(true);

        $campagne->getMontantLibres()->willReturn(new ArrayCollection([$montant1]));
        $campagne->getAmap()->willReturn($amap);
        $campagneReveled = $campagne->reveal();
        $bulletin = $this->builder->createFromCampagne($campagneReveled, $user->reveal());
        self::assertSame($campagneReveled, $bulletin->getCampagne());

        self::assertCount(1, $bulletin->getMontants());
        self::assertSame($montant1, $bulletin->getMontants()->first()->getCampagneMontant());
    }

    public function testCreateAmapienInvalid(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();

        $user = $this->prophesize(User::class);
        $user->getUserAmapienAmapForAmap($amap)->willReturn(null);

        $campagne = $this->prophesize(\PsrLib\ORM\Entity\Campagne::class);
        $campagne->getAmap()->willReturn($amap);
        $campagneReveled = $campagne->reveal();

        self::expectException(\RuntimeException::class);
        self::expectExceptionMessage('User must be in campaign amap');
        $this->builder->createFromCampagne($campagneReveled, $user->reveal());
    }

    private function prophesizeCampagneMontant(bool $montantLibre): CampagneMontant
    {
        $campagneMontant = $this->prophesize(\PsrLib\ORM\Entity\CampagneMontant::class);
        $campagneMontant->isMontantLibre()->willReturn($montantLibre);

        return $campagneMontant->reveal();
    }
}
