<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\DTO\DocumentUtilisateurFormState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\DocumentUtilisateurAmap;
use PsrLib\ORM\Entity\DocumentUtilisateurFerme;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Files\DocumentUtilisateur as DocumentUtilisateurFile;
use PsrLib\Services\EntityBuilder\DocumentUtilisateurFactory;
use PsrLib\Services\Uploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @internal
 *
 * @coversNothing
 */
class DocumentUtilisateurFactoryTest extends TestCase
{
    use ProphecyTrait;
    private DocumentUtilisateurFactory $documentUtilisateurFactory;

    private ObjectProphecy $uploader;

    public function setUp(): void
    {
        $this->uploader = $this->prophesize(Uploader::class);
        $this->documentUtilisateurFactory = new DocumentUtilisateurFactory($this->uploader->reveal());
    }

    public function testCreateAmap(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();

        [$state, $uploadedFile] = $this->buildState($amap);
        $fichier = $this->prophesize(DocumentUtilisateurFile::class)->reveal();
        $this->uploader->uploadFile($uploadedFile, DocumentUtilisateurFile::class)->willReturn($fichier);
        $document = $this->documentUtilisateurFactory->create($state);
        $this->assertInstanceOf(DocumentUtilisateurAmap::class, $document);

        $this->assertSame(2021, $document->getAnnee());
        $this->assertSame('nom', $document->getNom());
        $this->assertSame($fichier, $document->getFicher());
        $this->assertSame($amap, $document->getAmap());
    }

    public function testCreateFerme(): void
    {
        $ferme = $this->prophesize(Ferme::class)->reveal();

        [$state, $uploadedFile] = $this->buildState($ferme);

        $fichier = $this->prophesize(DocumentUtilisateurFile::class)->reveal();
        $this->uploader->uploadFile($uploadedFile, DocumentUtilisateurFile::class)->willReturn($fichier);
        $document = $this->documentUtilisateurFactory->create($state);

        $this->assertInstanceOf(DocumentUtilisateurFerme::class, $document);

        $this->assertSame(2021, $document->getAnnee());
        $this->assertSame('nom', $document->getNom());
        $this->assertSame($fichier, $document->getFicher());
        $this->assertSame($ferme, $document->getFerme());
    }

    private function buildState(Amap|Ferme $target)
    {
        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();

        $state = new DocumentUtilisateurFormState();
        $state->setAnnee(2021);
        $state->setNom('nom');
        $state->setFile($uploadedFile);
        $state->setTarget($target);

        return [$state, $uploadedFile];
    }
}
