<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EntityBuilder\EvenementAbonnementDefaultFactory;

/**
 * @internal
 *
 * @coversNothing
 */
class EvenementAbonnementDefaultFactoryTest extends TestCase
{
    use ProphecyTrait;

    private EvenementAbonnementDefaultFactory $factory;

    public function setUp(): void
    {
        $this->factory = new EvenementAbonnementDefaultFactory();
    }

    public function testAmapien(): void
    {
        $ferme1 = $this->prophesize(Ferme::class)->reveal();
        $region = $this->prophesize(Region::class)->reveal();
        $departement = $this->prophesize(Departement::class)->reveal();
        $amap = $this->prophesizeAmap($region, $departement, [$ferme1]);

        $user = $this->prophesize(User::class);
        $user->getAmapsAsAmapienOrAdmin()->willReturn([$amap]);
        $user->getAdminRegions()->willReturn(new ArrayCollection([]));
        $user->getAdminDepartments()->willReturn(new ArrayCollection([]));
        $user->getPaysan()->willReturn(null);

        $res = $this->factory->getDefaultForUser($user->reveal())->getAbonnements();
        self::assertCount(4, $res);
        self::assertSame($amap, $res[0]->getRelated());
        self::assertSame($departement, $res[1]->getRelated());
        self::assertSame($region, $res[2]->getRelated());
        self::assertSame($ferme1, $res[3]->getRelated());
    }

    public function testAdminRegion(): void
    {
        $departement = $this->prophesize(Departement::class)->reveal();
        $region = $this->prophesize(Region::class);
        $region->getDepartements()->willReturn(new ArrayCollection([$departement]));
        $regionReveled = $region->reveal();

        $user = $this->prophesize(User::class);
        $user->getAmapsAsAmapienOrAdmin()->willReturn([]);
        $user->getAdminRegions()->willReturn(new ArrayCollection([$regionReveled]));
        $user->getAdminDepartments()->willReturn(new ArrayCollection([]));
        $user->getPaysan()->willReturn(null);

        $res = $this->factory->getDefaultForUser($user->reveal())->getAbonnements();
        self::assertCount(2, $res);
        self::assertSame($regionReveled, $res[0]->getRelated());
        self::assertSame($departement, $res[1]->getRelated());
    }

    public function testAdminDepartement(): void
    {
        $departement = $this->prophesize(Departement::class)->reveal();

        $user = $this->prophesize(User::class);
        $user->getAmapsAsAmapienOrAdmin()->willReturn([]);
        $user->getAdminRegions()->willReturn(new ArrayCollection([]));
        $user->getAdminDepartments()->willReturn(new ArrayCollection([$departement]));
        $user->getPaysan()->willReturn(null);

        $res = $this->factory->getDefaultForUser($user->reveal())->getAbonnements();
        self::assertCount(1, $res);
        self::assertSame($departement, $res[0]->getRelated());
    }

    public function testPaysan(): void
    {
        $paysan = $this->prophesize(Paysan::class);

        $ferme = $this->prophesize(Ferme::class);
        $departement = $this->prophesize(Departement::class)->reveal();
        $region = $this->prophesize(Region::class)->reveal();
        $ferme->getDepartement()->willReturn($departement);
        $ferme->getRegion()->willReturn($region);
        $fermeReveled = $ferme->reveal();

        $amapienRef = $this->prophesize(Amapien::class);
        $amap = $this->prophesize(Amap::class)->reveal();
        $amapienRef->getAmap()->willReturn($amap);

        $ferme->getAmapienRefs()->willReturn(new ArrayCollection([$amapienRef->reveal()]));
        $paysan->getFermes()->willReturn(new ArrayCollection([$fermeReveled]));

        $user = $this->prophesize(User::class);
        $user->getAmapsAsAmapienOrAdmin()->willReturn([]);
        $user->getAdminRegions()->willReturn(new ArrayCollection([]));
        $user->getAdminDepartments()->willReturn(new ArrayCollection([]));
        $user->getPaysan()->willReturn($paysan->reveal());

        $res = $this->factory->getDefaultForUser($user->reveal())->getAbonnements();
        self::assertCount(4, $res);
        self::assertSame($fermeReveled, $res[0]->getRelated());
        self::assertSame($departement, $res[1]->getRelated());
        self::assertSame($region, $res[2]->getRelated());
        self::assertSame($amap, $res[3]->getRelated());
    }

    /**
     * @param Ferme[] $fermes
     */
    private function prophesizeAmap(Region $region, Departement $departement, $fermes): Amap
    {
        $amap = $this->prophesize(Amap::class);
        $amap->getRegion()->willReturn($region);
        $amap->getDepartement()->willReturn($departement);
        $amap->getFermesWithRef()->willReturn($fermes);

        return $amap->reveal();
    }
}
