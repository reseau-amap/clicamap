<?php

declare(strict_types=1);

namespace Test\Services\EntityBuilder;

use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Nominatim\NominatimResponse;
use PsrLib\DTO\Nominatim\NominatimResponseAddress;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Repository\DepartementRepository;
use PsrLib\Services\EntityBuilder\VilleFactory;

/**
 * @internal
 *
 * @coversNothing
 */
class VilleFactoryTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \Prophecy\Prophecy\ObjectProphecy $departementRepo;

    private \PsrLib\Services\EntityBuilder\VilleFactory $villeFactory;

    public function setUp(): void
    {
        $this->departementRepo = $this->prophesize(DepartementRepository::class);
        $this->villeFactory = new VilleFactory($this->departementRepo->reveal());
    }

    public function testAddressNoDepInAddress(): void
    {
        $address = $this->prophesizeResponse();
        $res = $this->villeFactory->create($address);

        self::assertNull($res);
    }

    public function testAddressPostCodeInAddress(): void
    {
        $address = $this->prophesizeResponse('test');
        $res = $this->villeFactory->create($address);

        self::assertNull($res);
    }

    public function testAddressLocInAddress(): void
    {
        $address = $this->prophesizeResponse('test', 123);
        $res = $this->villeFactory->create($address);

        self::assertNull($res);
    }

    public function testAddressNoDepInDb(): void
    {
        $address = $this->prophesizeResponse('test', 123, 'loc');

        $this->departementRepo->findSingleByNameLiked('test')->willReturn(null)->shouldBeCalled();
        $res = $this->villeFactory->create($address);

        self::assertNull($res);
    }

    public function testAddressDepInDb(): void
    {
        $address = $this->prophesizeResponse('test', 123, 'loc');

        $dep = $this->prophesize(Departement::class)->reveal();
        $this->departementRepo->findSingleByNameLiked('test')->willReturn($dep);
        $res = $this->villeFactory->create($address);

        self::assertNotNull($res);
        self::assertSame(123, $res->getCp());
        self::assertSame('loc', $res->getNom());
        self::assertSame($dep, $res->getDepartement());
    }

    private function prophesizeResponse(string $county = null, int $postCode = null, string $locality = ''): NominatimResponse
    {
        $address = $this->prophesize(NominatimResponseAddress::class);
        $address->getCounty()->willReturn($county);
        $address->getPostCode()->willReturn($postCode);
        $address->getPlaceName()->willReturn($locality);

        $res = $this->prophesize(NominatimResponse::class);
        $res->getAddress()->willReturn($address->reveal());

        return $res->reveal();
    }
}
