<?php

declare(strict_types=1);

namespace Test\Services;

use PsrLib\ORM\Entity\Evenement;
use PsrLib\ORM\Entity\EvenementAbonnement;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\EvenementAbonnementDepartement;
use PsrLib\ORM\Entity\EvenementAbonnementFerme;
use PsrLib\ORM\Entity\EvenementAbonnementRegion;
use PsrLib\ORM\Entity\EvenementAmap;
use PsrLib\ORM\Entity\EvenementDepartement;
use PsrLib\ORM\Entity\EvenementFerme;
use PsrLib\ORM\Entity\EvenementRegion;
use PsrLib\ORM\Entity\EvenementSuperAdmin;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\EvenementRepositoryUserAbonnement;
use Test\EntityHelperTrait;
use Test\ORM\Repository\RepositoryTestAbstract;

/**
 * As test is quite complex, use a repository tests.
 *
 * @internal
 *
 * @coversNothing
 */
class EvenementRepositoryUserAbonnementTest extends RepositoryTestAbstract
{
    use EntityHelperTrait;
    private EvenementRepositoryUserAbonnement $repo;

    public function setUp(): void
    {
        parent::setUp();
        $this->repo = self::$container->get(EvenementRepositoryUserAbonnement::class);
    }

    public function testEmpty(): void
    {
        $empty = $this->getUserByUsername('empty');

        $this->assertEmpty($this->repo->getForUser($empty));
    }

    public function testSuperAdminFindAll(): void
    {
        $admin = $this->getUserByUsername('admin');
        $empty = $this->getUserByUsername('empty');

        $amap = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $empty, $this->getAmapByName('amap test'));
        $ferme = $this->storeEvenementWithRelatedEntity(EvenementFerme::class, $empty, $this->getFermeByName('ferme'));
        $region = $this->storeEvenementWithRelatedEntity(EvenementRegion::class, $empty, $this->getRegionByName('GUADELOUPE'));
        $departement = $this->storeEvenementWithRelatedEntity(EvenementDepartement::class, $empty, $this->getDepartementByName('AIN'));

        $res = $this->repo->getForUser($admin);

        self::assertCount(4, $res);
        self::assertSame($departement, $res[0]);
        self::assertSame($region, $res[1]);
        self::assertSame($ferme, $res[2]);
        self::assertSame($amap, $res[3]);
    }

    public function testOrderLastAddedFirst(): void
    {
        $admin = $this->getUserByUsername('admin');

        $amap1 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $admin, $this->getAmapByName('amap test'));
        $amap2 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $admin, $this->getAmapByName('amap test'));

        $res = $this->repo->getForUser($admin);

        self::assertCount(2, $res);
        self::assertSame($amap2, $res[0]);
        self::assertSame($amap1, $res[1]);
    }

    public function testSuperAdminFilterDefaultAccess(): void
    {
        $admin = $this->getUserByUsername('admin');
        $empty = $this->getUserByUsername('empty');

        $amap1 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $empty, $this->getAmapByName('amap test'));
        $amap2 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $empty, $this->getAmapByName('amap test'));
        $amap2->setAccesAbonnesDefautUniquement(true);
        $this->em->flush();

        $res = $this->repo->getForUser($admin);

        self::assertCount(1, $res);
        self::assertSame($amap1, $res[0]);
    }

    public function testAccessSuperAdminEventForAllUser(): void
    {
        $evt = $this->storeSuperAdminEvent();

        $res = $this->repo->getForUser($this->getUserByUsername('empty'));
        self::assertCount(1, $res);
        self::assertSame($evt, $res[0]);
    }

    public function testDefaultAccessForEvtAmap(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $this->getUserByUsername('amap'), $this->getAmapByName('amap test'));
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $this->getUserByUsername('amap'), $this->getAmapByName('amap test 2'));

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testDefaultAccessForEvtFerme(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementFerme::class, $this->getUserByUsername('paysan'), $this->getFermeByName('ferme'));
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementFerme::class, $this->getUserByUsername('paysan'), $this->getFermeByName('Ferme 2'));

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testDefaultAccessForEvtRegion(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementRegion::class, $this->getUserByUsername('admin_aura'), $this->getRegionByName('AUVERGNE-RHÔNE-ALPES'));
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementRegion::class, $this->getUserByUsername('admin_aura'), $this->getRegionByName('GUADELOUPE'));

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testDefaultAccessForEvDepartement(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementDepartement::class, $this->getUserByUsername('admin_rhone'), $this->getDepartementByName('RHÔNE'));
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementDepartement::class, $this->getUserByUsername('admin_rhone'), $this->getDepartementByName('AIN'));

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testSubscriptionAmap(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $amap = $this->getAmapByName('amap test 2');
        $sub1 = $this->storeSubscription(EvenementAbonnementAmap::class, $amapien, $amap);

        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $this->getUserByUsername('amap'), $amap);
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementAmap::class, $this->getUserByUsername('amap'), $amap, true);

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testSubscriptionFerme(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $ferme = $this->getFermeByName('Ferme 2');
        $sub1 = $this->storeSubscription(EvenementAbonnementFerme::class, $amapien, $ferme);

        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementFerme::class, $this->getUserByUsername('paysan'), $ferme);
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementFerme::class, $this->getUserByUsername('paysan'), $ferme, true);

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testSubscriptionRegion(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $region = $this->getRegionByName('GUADELOUPE');
        $sub1 = $this->storeSubscription(EvenementAbonnementRegion::class, $amapien, $region);

        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementRegion::class, $this->getUserByUsername('admin'), $region);
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementRegion::class, $this->getUserByUsername('admin'), $region, true);

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    public function testSubscriptionDepartement(): void
    {
        $amapien = $this->getUserByUsername('amapien');
        $departement = $this->getDepartementByName('AIN');
        $sub1 = $this->storeSubscription(EvenementAbonnementDepartement::class, $amapien, $departement);

        $evt1 = $this->storeEvenementWithRelatedEntity(EvenementDepartement::class, $this->getUserByUsername('admin'), $departement);
        $evt2 = $this->storeEvenementWithRelatedEntity(EvenementDepartement::class, $this->getUserByUsername('admin'), $departement, true);

        $res = $this->repo->getForUser($amapien);
        self::assertCount(1, $res);
        self::assertSame($evt1, $res[0]);
    }

    private function storeSubscription(string $evenementSubscriptionClass, User $user, object $related): EvenementAbonnement
    {
        $evt = new $evenementSubscriptionClass($user);
        $evt->setRelated($related);
        $this->em->persist($evt);
        $this->em->flush();

        return $evt;
    }

    private function storeSuperAdminEvent(): EvenementSuperAdmin
    {
        $evt = new EvenementSuperAdmin($this->getUserByUsername('admin'));
        $evt->setTitre('titre');
        $evt->setDescription('description');
        $evt->setDescriptionCourte('description courte');
        $this->em->persist($evt);
        $this->em->flush();

        return $evt;
    }

    private function storeEvenementWithRelatedEntity(string $evenementClass, User $user, object $related, bool $restrictToDefault = false): Evenement
    {
        $evt = new $evenementClass($user);
        $evt->setTitre('titre');
        $evt->setDescription('description');
        $evt->setDescriptionCourte('description courte');
        $evt->setRelated($related);
        $evt->setAccesAbonnesDefautUniquement($restrictToDefault);
        $this->em->persist($evt);
        $this->em->flush();

        return $evt;
    }
}
