<?php

declare(strict_types=1);

namespace Test\Services;

use PHPUnit\Framework\TestCase;
use PsrLib\Services\TvaLabel;

/**
 * @internal
 *
 * @coversNothing
 */
class TVALabelTest extends TestCase
{
    private \PsrLib\Services\TvaLabel $tvaLabel;

    public function setUp(): void
    {
        $this->tvaLabel = new TvaLabel();
    }

    /**
     * @dataProvider providerTestLabel
     */
    public function testLabel(?float $TVA, string $expected): void
    {
        $res = $this->tvaLabel->getLabel($TVA);
        self::assertSame($expected, $res);
    }

    public function providerTestLabel()
    {
        return [
            [null, 'Aucune TVA'],
            [5.5, '5,5 %'],
            [20, '20 %'],
            [0.0, '0 %'],
        ];
    }
}
