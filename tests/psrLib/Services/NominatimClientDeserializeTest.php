<?php

declare(strict_types=1);

namespace Test\Services;

use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use PsrLib\Services\NominatimClient;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Component\Serializer\SerializerInterface;
use Test\ContainerAwareTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class NominatimClientDeserializeTest extends ContainerAwareTestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private ?\PsrLib\Services\NominatimClient $nominatimFacade = null;

    private \Prophecy\Prophecy\ObjectProphecy $guzzle;

    public function setUp(): void
    {
        parent::setUp();
        $this->guzzle = $this->prophesize(\GuzzleHttp\Client::class);
    }

    public function testDeserializeFullHamlet(): void
    {
        $this->initService(__DIR__.'/../../data/SerializerTest/nominatim/full_hamlet.json');
        $res = $this->nominatimFacade->geocode('test');

        self::assertCount(1, $res);
    }

    private function initService(string $fixturePath): void
    {
        $this->nominatimFacade = new NominatimClient(
            $this->guzzle->reveal(),
            self::$container->get(SerializerInterface::class),
            new NullAdapter()
        );

        $response = new Response(200, [], file_get_contents($fixturePath));
        $this->guzzle->request('GET', Argument::any(), Argument::any())->willReturn($response);
    }
}
