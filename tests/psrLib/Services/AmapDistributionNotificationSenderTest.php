<?php

declare(strict_types=1);

namespace Test\Services;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Repository\AmapDistributionRepository;
use PsrLib\Services\AmapDistributionNotificationSender;
use PsrLib\Services\Email_Sender;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapDistributionNotificationSenderTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \PsrLib\Services\AmapDistributionNotificationSender $sender;

    /**
     * @var Prophet
     */
    private \Prophecy\Prophecy\ObjectProphecy $emailSenderProphet;

    /**
     * @var Prophet
     */
    private \Prophecy\Prophecy\ObjectProphecy $amapDistributionRepo;

    public function setUp(): void
    {
        $this->emailSenderProphet = $this->prophesize(Email_Sender::class);
        $this->amapDistributionRepo = $this->prophesize(AmapDistributionRepository::class);

        $this->sender = new AmapDistributionNotificationSender(
            $this->emailSenderProphet->reveal(),
            $this->amapDistributionRepo->reveal()
        );
    }

    public function testAmapienRappels(): void
    {
        $amapien11 = $this->buildAmapien(true);
        $amapien12 = $this->buildAmapien(true);
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $distribution1->getPlacesRestantes()->willReturn(1);

        $amapien21 = $this->buildAmapien(true);
        $amapien22 = $this->buildAmapien(true);
        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getAmapiens()->willReturn(new ArrayCollection([$amapien21, $amapien22]));
        $distribution2->getPlacesRestantes()->willReturn(1);

        $amapien31 = $this->buildAmapien(true);
        $amapien32 = $this->buildAmapien(false);
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getAmapiens()->willReturn(new ArrayCollection([$amapien31, $amapien32]));
        $distribution3->getPlacesRestantes()->willReturn(1);

        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([$distribution1])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([$distribution2, $distribution3])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution1, $amapien11)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution1, $amapien12)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution2, $amapien21)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution2, $amapien22)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution3, $amapien31)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapienRappel($distribution3, $amapien32)->shouldNotBeCalled();

        $this->sender->amapienRappels();
    }

    public function testAmapRelance(): void
    {
        $amapien11 = $this->buildAmapien(true);
        $amapien12 = $this->buildAmapien(true);
        $amap1 = $this->prophesize(Amap::class);
        $amap1->getId()->willReturn(1);
        $amap1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $ll1 = new AmapLivraisonLieu();
        $ll1->setAmap($amap1->reveal());
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([$amapien11]));
        $distribution1->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution1->getDateDebut()->willReturn(Carbon::create(2000, 1, 1, 17, 0));
        $distribution1->getPlacesRestantes()->willReturn(1);
        $distribution1Revealed = $distribution1->reveal();

        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution2->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution2->getDateDebut()->willReturn(Carbon::create(2000, 1, 1, 16, 0));
        $distribution2->getPlacesRestantes()->willReturn(1);
        $distribution2Revealed = $distribution2->reveal();

        $amapien21 = $this->buildAmapien(true);
        $amapien22 = $this->buildAmapien(false);
        $amap2 = $this->prophesize(Amap::class);
        $amap2->getId()->willReturn(2);
        $amap2->getAmapiens()->willReturn(new ArrayCollection([$amapien21, $amapien22]));
        $ll2 = new AmapLivraisonLieu();
        $ll2->setAmap($amap2->reveal());
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution3->getAmapLivraisonLieu()->willReturn($ll2);
        $distribution3->getDateDebut()->willReturn(Carbon::create(2000, 1, 1, 18, 0));
        $distribution3->getPlacesRestantes()->willReturn(1);
        $distribution3Revealed = $distribution3->reveal();

        $distribution4 = $this->prophesize(AmapDistribution::class);
        $distribution4->getPlacesRestantes()->willReturn(0);
        $distribution4Revealed = $distribution4->reveal();

        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([$distribution1Revealed, $distribution2Revealed, $distribution3Revealed])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([$distribution4Revealed])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(1)->willReturn([])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution2Revealed, $distribution1Revealed]),
            Argument::exact([$amapien12])
        )->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution3Revealed]),
            Argument::exact([$amapien21])
        )->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution4Revealed]),
            Argument::any()
        )->shouldNotBeCalled();

        $this->sender->amapRelance();
    }

    public function testAmapRelanceAmapCopie(): void
    {
        $amapien11 = $this->buildAmapien(true);
        $amapien12 = $this->buildAmapien(true);
        $amap1 = $this->prophesize(Amap::class);
        $amap1->getId()->willReturn(1);
        $amap1->getAmapiens()->willReturn(new ArrayCollection([$amapien11, $amapien12]));
        $ll1 = new AmapLivraisonLieu();
        $ll1->setAmap($amap1->reveal());
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getAmapiens()->willReturn(new ArrayCollection([]));
        $distribution1->getAmapLivraisonLieu()->willReturn($ll1);
        $distribution1->getDateDebut()->willReturn(Carbon::create(2000, 1, 1, 17, 0));
        $distribution1->getPlacesRestantes()->willReturn(1);
        $distribution1Revealed = $distribution1->reveal();

        $this->amapDistributionRepo->getDistributionInDaysFromNow(18)->willReturn([])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(6)->willReturn([])->shouldBeCalled();
        $this->amapDistributionRepo->getDistributionInDaysFromNow(1)->willReturn([$distribution1Revealed])->shouldBeCalled();

        $this->emailSenderProphet->envoyerDistributionAmapiensRelance(
            Argument::exact([$distribution1Revealed]),
            Argument::exact([$amapien11, $amapien12])
        )->shouldBeCalled();

        $this->sender->amapRelance();
    }

    public function testAmapAlerteFin(): void
    {
        $distribution1 = $this->prophesize(AmapDistribution::class);
        $distribution1->getDate()->willReturn(new Carbon('2000-05-31'));
        $distribution2 = $this->prophesize(AmapDistribution::class);
        $distribution2->getDate()->willReturn(new Carbon('2000-06-30'));
        $distribution3 = $this->prophesize(AmapDistribution::class);
        $distribution3->getDate()->willReturn(new Carbon('2000-08-01'));

        $this->amapDistributionRepo->getLastDistributionByAmap()->wilLReturn([$distribution1, $distribution2, $distribution3]);
        $this->emailSenderProphet->envoyerDistributionAmapAlerteFin($distribution1)->shouldBeCalled();
        $this->emailSenderProphet->envoyerDistributionAmapAlerteFin($distribution2)->shouldBeCalled();

        Carbon::setTestNow(Carbon::create(2000, 5, 1, 1, 0, 0));
        $this->sender->amapAlerteFin();
        Carbon::setTestNow();
    }

    private function buildAmapien(bool $actif)
    {
        $amapien = $this->prophesize(Amapien::class);
        $amapien->estActif()->willReturn($actif);

        return $amapien->reveal();
    }
}
