<?php

declare(strict_types=1);

namespace Test\Services\Security;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Test\ContainerAwareTestCase;

abstract class SecurityTestAbstract extends ContainerAwareTestCase
{
    use ProphecyTrait;

    protected VoterInterface $voter;

    private function propehesizeUser(User $user, bool $isAmap): void
    {
    }

    protected function prophesizeToken(User $user): TokenInterface
    {
        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($user);

        return $token->reveal();
    }

    protected function assertVote(int $result, User $user, ?object $subject, string $attribute): void
    {
        self::assertSame(
            $result,
            $this->voter->vote(
                $this->prophesizeToken($user),
                $subject,
                [$attribute]
            )
        );
    }
}
