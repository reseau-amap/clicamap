<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\AmapienVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\AmapienVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class DistributionVoterListOwnTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new AmapienVoter();
    }

    /**
     * @dataProvider providerListOwn
     */
    public function testListOwn(bool $amapien, int $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapien()->willReturn($amapien);

        $this->assertVote(
            $expected,
            $user->reveal(),
            null,
            AmapienVoter::ACTION_DISTRIBUTION_AMAPIEN_LIST_OWN
        );
    }

    public function providerListOwn(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
