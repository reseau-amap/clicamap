<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractAmapienValidationVoter;

use Carbon\Carbon;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractAmapienValidationVoter;
use PsrLib\Workflow\ContratStatusWorkflow;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractAmapienValidationVoterTest extends SecurityTestAbstract
{
    protected ObjectProphecy $contratStatusWorkflow;

    public function setUp(): void
    {
        $this->contratStatusWorkflow = $this->prophesize(ContratStatusWorkflow::class);

        $this->voter = new ContractAmapienValidationVoter(
            $this->contratStatusWorkflow->reveal(),
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    /**
     * @dataProvider providerAmapienValidation
     */
    public function testAmapienValidation(
        User $user,
        User $contractUser,
        bool $transitionValid,
        bool $isNowBeforeFirstLivraisonDateWithFermeDelai,
        int $expectedVoter
    ): void {
        $amapien = $this->prophesize(Amapien::class);
        $amapien->getUser()->willReturn($contractUser);

        $contract = $this->prophesize(Contrat::class);
        $contract->getAmapien()->willReturn($amapien->reveal());
        $contract->isNowBeforeOrEqualFirstLivraisonDateWithFermeDelai()->willReturn($isNowBeforeFirstLivraisonDateWithFermeDelai);

        $contractReveled = $contract->reveal();

        $this->contratStatusWorkflow->can($contractReveled, ContratStatusWorkflow::TRANSITION_AMAPIEN_VALIDATION)->willReturn($transitionValid);

        $this->assertVote(
            $expectedVoter,
            $user,
            $contractReveled,
            ContractAmapienValidationVoter::ACTION_CONTRACT_SIGNED_AMAPIEN_VALIDATION
        );
    }

    public function providerAmapienValidation(): array
    {
        $user1 = $this->prophesize(User::class)->reveal();
        $user2 = $this->prophesize(User::class)->reveal();

        return [
            [$user1, $user1, true, true, Voter::ACCESS_GRANTED],
            [$user1, $user2, true, true, Voter::ACCESS_DENIED],
            [$user1, $user1, false, true, Voter::ACCESS_DENIED],
            [$user1, $user1, true, false, Voter::ACCESS_DENIED],
        ];
    }
}
