<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ModelContratVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratVoterListValidateTest extends ModelContratVoterAbstract
{
    /**
     * @dataProvider provideListValidate
     */
    public function testListValidate(bool $isPaysan, bool $isFermeRegroupementAdmin, int $expectedRes): void
    {
        $user = $this->prophesize(User::class);
        $user->isPaysan()->willReturn($isPaysan);
        $user->isFermeRegroupementAdmin()->willReturn($isFermeRegroupementAdmin);

        $this->assertVote(
            $expectedRes,
            $user->reveal(),
            null,
            ModelContratVoter::ACTION_CONTRACT_MODEL_LIST_VALIDATE
        );
    }

    public function provideListValidate(): array
    {
        return [
            [true, false, Voter::ACCESS_GRANTED],
            [false, true, Voter::ACCESS_GRANTED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
