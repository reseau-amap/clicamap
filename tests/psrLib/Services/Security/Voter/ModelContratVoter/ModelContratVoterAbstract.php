<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ModelContratVoter;

use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\ORM\Repository\FermeRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Test\Services\Security\SecurityTestAbstract;

abstract class ModelContratVoterAbstract extends SecurityTestAbstract
{
    protected ObjectProphecy $authorizationChecker;
    protected ObjectProphecy $modelContratRepository;
    protected ObjectProphecy $fermeRepository;

    public function setUp(): void
    {
        $this->authorizationChecker = $this->prophesize(AuthorizationCheckerInterface::class);
        $this->modelContratRepository = $this->prophesize(ModeleContratRepository::class);
        $this->fermeRepository = $this->prophesize(FermeRepository::class);

        $this->voter = new ModelContratVoter(
            $this->authorizationChecker->reveal(),
            $this->modelContratRepository->reveal(),
            $this->fermeRepository->reveal()
        );
    }
}
