<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ModelContratVoter;

use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ModelContratVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ModelContratVoterMoveDateTest extends ModelContratVoterAbstract
{
    /**
     * @dataProvider contractModelMoveDateProvider
     */
    public function testContractModelMoveDate(
        bool $isAmapien,
        bool $isAmapAdmin,
        bool $isGrantedDisplay,
        bool $isArchived,
        int $expectedVote
    ): void {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->isArchived()->willReturn($isArchived);
        $mcReveled = $mc->reveal();

        $user = $this->prophesize(User::class);
        $user->isAmapien()->willReturn($isAmapien);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);

        $this->authorizationChecker->isGranted(ModelContratVoter::ACTION_CONTRACT_MODEL_DISPLAY, $mcReveled)->willReturn($isGrantedDisplay);

        $this->assertVote(
            $expectedVote,
            $user->reveal(),
            $mcReveled,
            ModelContratVoter::ACTION_CONTRACT_MODEL_MOVE_DATE
        );
    }

    public function contractModelMoveDateProvider(): array
    {
        return [
            [true, true, true, false, Voter::ACCESS_GRANTED],
            [true, true, true, true, Voter::ACCESS_DENIED],
            [true, true, false, false, Voter::ACCESS_DENIED],
            [true, true, false, true, Voter::ACCESS_DENIED],
            [true, false, true, false, Voter::ACCESS_GRANTED],
            [true, false, true, true, Voter::ACCESS_DENIED],
            [true, false, false, false, Voter::ACCESS_DENIED],
            [true, false, false, true, Voter::ACCESS_DENIED],
            [false, true, true, false, Voter::ACCESS_GRANTED],
            [false, true, true, true, Voter::ACCESS_DENIED],
            [false, true, false, false, Voter::ACCESS_DENIED],
            [false, true, false, true, Voter::ACCESS_DENIED],
            [false, false, true, false, Voter::ACCESS_DENIED],
            [false, false, true, true, Voter::ACCESS_DENIED],
            [false, false, false, false, Voter::ACCESS_DENIED],
            [false, false, false, true, Voter::ACCESS_DENIED],
        ];
    }
}
