<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CampagneVoter;

use Doctrine\Common\Collections\ArrayCollection;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\CampagneBulletin;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CampagneSubscriptionVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneSubscriptionVoterTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CampagneSubscriptionVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testAmapienOfAmapNoBulletin(bool $isAmapien, int $expected): void
    {
        $campagne = $this->prophesize(Campagne::class);

        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn($isAmapien);

        $campagne->getAmap()->willReturn($amap);
        $campagne->getBulletins()->willReturn(new ArrayCollection());
        $campagne->getVersionSuivante()->willReturn(null);

        $this->assertVote(
            $expected,
            $user->reveal(),
            $campagne->reveal(),
            CampagneSubscriptionVoter::ACTION_CAMPAGNE_SUBSCRIBE
        );
    }

    public function providerList()
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }

    public function testAmapienOfAmapBulletinNotAmapien(): void
    {
        $campagne = $this->prophesize(Campagne::class);

        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn(true);

        $bulletin = $this->prophesize(CampagneBulletin::class);
        $amapien = $this->prophesize(Amapien::class);
        $amapien->getUser()->willReturn($this->prophesize(User::class)->reveal());
        $bulletin->getAmapien()->willReturn($amapien->reveal());

        $campagne->getAmap()->willReturn($amap);
        $campagne->getBulletins()->willReturn(new ArrayCollection([$bulletin->reveal()]));
        $campagne->getVersionSuivante()->willReturn(null);

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user->reveal(),
            $campagne->reveal(),
            CampagneSubscriptionVoter::ACTION_CAMPAGNE_SUBSCRIBE
        );
    }

    public function testAmapienOfAmapVersionSuivante(): void
    {
        $campagne = $this->prophesize(Campagne::class);

        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn(true);

        $campagne->getAmap()->willReturn($amap);
        $campagne->getBulletins()->willReturn(new ArrayCollection([]));
        $campagne->getVersionSuivante()->willReturn($this->prophesize(Campagne::class)->reveal());

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user->reveal(),
            $campagne->reveal(),
            CampagneSubscriptionVoter::ACTION_CAMPAGNE_SUBSCRIBE
        );
    }

    public function testAmapienOfAmapBulletinAmapien(): void
    {
        $campagne = $this->prophesize(Campagne::class);

        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn(true);

        $bulletin = $this->prophesize(CampagneBulletin::class);
        $amapien = $this->prophesize(Amapien::class);
        $amapien->getUser()->willReturn($user->reveal());
        $bulletin->getAmapien()->willReturn($amapien->reveal());

        $campagne->getAmap()->willReturn($amap);
        $campagne->getBulletins()->willReturn(new ArrayCollection([$bulletin->reveal()]));
        $campagne->getVersionSuivante()->willReturn(null);

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user->reveal(),
            $campagne->reveal(),
            CampagneSubscriptionVoter::ACTION_CAMPAGNE_SUBSCRIBE
        );
    }
}
