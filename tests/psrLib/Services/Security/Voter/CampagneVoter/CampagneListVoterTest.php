<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CampagneVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CampagneListVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneListVoterTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CampagneListVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testList(bool $isAdmin, int $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapAdmin()->willReturn($isAdmin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            null,
            CampagneListVoter::ACTION_CAMPAGNE_LIST
        );
    }

    public function providerList()
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
