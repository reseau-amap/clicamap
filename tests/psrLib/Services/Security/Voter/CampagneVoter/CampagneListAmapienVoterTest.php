<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CampagneVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CampagneListAmapienVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneListAmapienVoterTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CampagneListAmapienVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testList(bool $isAdmin, int $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapien()->willReturn($isAdmin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            null,
            CampagneListAmapienVoter::ACTION_CAMPAGNE_LIST_AMAPIEN
        );
    }

    public function providerList()
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
