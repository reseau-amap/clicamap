<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CampagneVoter;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CampagneNewVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneNewVoterTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CampagneNewVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testNew(bool $isAdminOfAmap, bool $isAdmin, int $expected): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn($isAdminOfAmap);
        $user->isAdminOf($amap)->willReturn($isAdmin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            $amap,
            CampagneNewVoter::ACTION_CAMPAGNE_NEW
        );
    }

    public function providerList()
    {
        return [
            [true, true, Voter::ACCESS_GRANTED],
            [true, false, Voter::ACCESS_GRANTED],
            [false, true, Voter::ACCESS_GRANTED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
