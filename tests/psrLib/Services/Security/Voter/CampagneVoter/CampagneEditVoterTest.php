<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CampagneVoter;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CampagneEditVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CampagneEditVoterTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CampagneEditVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testEdit(bool $isAdminOfAmap, bool $isAdminOf, bool $versionSuivante, bool $amapDeFait, int $expected): void
    {
        $amap = $this->prophesize(Amap::class);
        $amap->getAssociationDeFait()->willReturn($amapDeFait);
        $amap = $amap->reveal();

        $campagne = $this->prophesize(Campagne::class);
        if ($versionSuivante) {
            $campagne->getVersionSuivante()->willReturn($this->prophesize(Campagne::class)->reveal());
        } else {
            $campagne->getVersionSuivante()->willReturn(null);
        }
        $campagne->getAmap()->willReturn($amap);

        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn($isAdminOfAmap);
        $user->isAdminOf($amap)->willReturn($isAdminOf);

        $this->assertVote(
            $expected,
            $user->reveal(),
            $campagne->reveal(),
            CampagneEditVoter::ACTION_CAMPAGNE_EDIT
        );
    }

    public function providerList()
    {
        return [
            [true, true, true, true, Voter::ACCESS_DENIED],
            [true, true, true, false, Voter::ACCESS_DENIED],
            [true, true, false, true, Voter::ACCESS_GRANTED],
            [true, true, false, false, Voter::ACCESS_GRANTED],
            [true, false, true, true, Voter::ACCESS_DENIED],
            [true, false, true, false, Voter::ACCESS_DENIED],
            [true, false, false, true, Voter::ACCESS_GRANTED],
            [true, false, false, false, Voter::ACCESS_GRANTED],
            [false, true, true, true, Voter::ACCESS_DENIED],
            [false, true, true, false, Voter::ACCESS_DENIED],
            [false, true, false, true, Voter::ACCESS_GRANTED],
            [false, true, false, false, Voter::ACCESS_DENIED],
            [false, false, true, true, Voter::ACCESS_DENIED],
            [false, false, true, false, Voter::ACCESS_DENIED],
            [false, false, false, true, Voter::ACCESS_DENIED],
            [false, false, false, false, Voter::ACCESS_DENIED],
        ];
    }
}
