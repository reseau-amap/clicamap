<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\DocumentVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\DocumentVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class DocumentVoterOwnTest extends SecurityTestAbstract
{
    use ProphecyTrait;

    public function setUp(): void
    {
        $this->voter = new DocumentVoter(
        );
    }

    public function testGetOwnHasNoRight(): void
    {
        $this->assertVote(
            Voter::ACCESS_DENIED,
            $this->prophesizeUserWithHasToRightProperty(true),
            null,
            DocumentVoter::ACTION_DOCUMENT_GETOWN
        );
    }

    public function testGetOwnNoHasNoRight(): void
    {
        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $this->prophesizeUserWithHasToRightProperty(false),
            null,
            DocumentVoter::ACTION_DOCUMENT_GETOWN
        );
    }

    public function prophesizeUserWithHasToRightProperty(bool $hasNoRight)
    {
        $user = $this->prophesize(User::class);
        $user->hasNoRights()->willReturn($hasNoRight);

        return $user->reveal();
    }
}
