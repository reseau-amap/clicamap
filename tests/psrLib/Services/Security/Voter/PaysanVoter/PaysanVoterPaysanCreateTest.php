<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\PaysanVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\PaysanVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class PaysanVoterPaysanCreateTest extends PaysanAbstractVoter
{
    /**
     * @dataProvider providePaysanCreation
     */
    public function testPaysanCreation(
        bool $isAdmin,
        bool $isRefProduit,
        bool $isAmapAdmin,
        bool $isFermeRegroupementAdmin,
        int $expectedResult,
    ): void {
        $user = $this->prophesize(User::class);
        $user->isAdmin()->willReturn($isAdmin);
        $user->isRefProduit()->willReturn($isRefProduit);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);
        $user->isFermeRegroupementAdmin()->willReturn($isFermeRegroupementAdmin);

        $this->assertVote(
            $expectedResult,
            $user->reveal(),
            null,
            PaysanVoter::ACTION_PAYSAN_CREATE
        );
    }

    public function providePaysanCreation(): array
    {
        return [
            'admin' => [true, false, false, false, Voter::ACCESS_GRANTED],
            'ref produit' => [false, true, false, false, Voter::ACCESS_GRANTED],
            'amap admin' => [false, false, true, false, Voter::ACCESS_GRANTED],
            'ferme regroupement admin' => [false, false, false, true, Voter::ACCESS_GRANTED],
            'none' => [false, false, false, false, Voter::ACCESS_DENIED],
        ];
    }
}
