<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\PaysanVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\PaysanVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class PaysanVoterDownloadListTest extends PaysanAbstractVoter
{
    /**
     * @dataProvider provideDownloadListRegroupementRegroupement
     */
    public function testDownloadListRegroupementRegroupement(bool $isFermeRegroupementAdmin, int $expectedResult): void
    {
        $user = $this->prophesize(User::class);
        $user->isFermeRegroupementAdmin()->willReturn($isFermeRegroupementAdmin);
        $this->assertVote(
            $expectedResult,
            $user->reveal(),
            null,
            PaysanVoter::ACTION_PAYSAN_DOWNLOAD_LIST_REGROUPEMENT
        );
    }

    public function provideDownloadListRegroupementRegroupement(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
