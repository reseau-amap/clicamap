<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\PaysanVoter;

use Doctrine\Common\Collections\ArrayCollection;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Paysan;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\PaysanVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class PaysanVoterDisplayProfilTest extends PaysanAbstractVoter
{
    public function testPaysanDisplayProfilNull(): void
    {
        $user = $this->prophesize(User::class)->reveal();

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            null,
            PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL
        );
    }

    public function testPaysanDisplayProfilOwn(): void
    {
        $user = $this->prophesize(User::class)->reveal();
        $paysan = $this->prophesize(Paysan::class);
        $paysan->getUser()->willReturn($user);

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user,
            $paysan->reveal(),
            PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL
        );
    }

    public function testPaysanDisplayProfilFermeRegroupementAdmin(): void
    {
        $user = $this->prophesize(User::class);
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $paysan = $this->prophesize(Paysan::class);
        $paysan->getUser()->willReturn($this->prophesize(User::class)->reveal());
        $paysan->getFermes()->willReturn(new ArrayCollection([$ferme]));

        $user->isFermeRegroupementAdminOf($ferme)->willReturn(true);

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user->reveal(),
            $paysan->reveal(),
            PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL
        );
    }

    /**
     * @dataProvider paysanDisplayProfilEditProfilExternalProvider
     */
    public function testPaysanDisplayProfilEditProfilExternal(bool $externalPermission, int $res): void
    {
        $user = $this->prophesize(User::class);
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $paysan = $this->prophesize(Paysan::class);
        $paysan->getUser()->willReturn($this->prophesize(User::class)->reveal());
        $paysan->getFermes()->willReturn(new ArrayCollection([$ferme]));
        $paysanReveled = $paysan->reveal();

        $user->isFermeRegroupementAdminOf($ferme)->willReturn(false);
        $this->authorizationCheckerProphecy->isGranted(PaysanVoter::ACTION_PAYSAN_EDIT_PROFIL_EXTERNAL, $paysanReveled)->willReturn($externalPermission);

        $this->assertVote(
            $res,
            $user->reveal(),
            $paysanReveled,
            PaysanVoter::ACTION_PAYSAN_DISPLAY_PROFIL
        );
    }

    public function paysanDisplayProfilEditProfilExternalProvider(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
