<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\PaysanVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\Services\Security\Voters\PaysanVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Test\Services\Security\SecurityTestAbstract;

abstract class PaysanAbstractVoter extends SecurityTestAbstract
{
    use ProphecyTrait;

    protected ObjectProphecy $authorizationCheckerProphecy;

    public function setUp(): void
    {
        $this->authorizationCheckerProphecy = $this->prophesize(AuthorizationCheckerInterface::class);
        $this->voter = new PaysanVoter($this->authorizationCheckerProphecy->reveal());
    }
}
