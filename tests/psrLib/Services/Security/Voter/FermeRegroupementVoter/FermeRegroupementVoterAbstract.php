<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeRegroupementVoter;

use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use Test\Services\Security\SecurityTestAbstract;

abstract class FermeRegroupementVoterAbstract extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new FermeRegroupementVoter();
    }
}
