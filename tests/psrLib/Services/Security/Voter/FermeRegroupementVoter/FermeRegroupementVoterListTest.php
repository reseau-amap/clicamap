<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeRegroupementVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeRegroupementVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeRegroupementVoterListTest extends FermeRegroupementVoterAbstract
{
    use ProphecyTrait;

    /**
     * @dataProvider provideList
     */
    public function testList(bool $isAdminRegion, bool $isSuperAdmin, int $res): void
    {
        $user = $this->prophesize(User::class);
        $user->isAdminRegion()->willReturn($isAdminRegion);
        $user->isSuperAdmin()->willReturn($isSuperAdmin);

        $this->assertVote(
            $res,
            $user->reveal(),
            null,
            FermeRegroupementVoter::ACTION_FERME_REGROUPEMENT_LIST
        );
    }

    public function provideList(): array
    {
        return [
            [true, false, Voter::ACCESS_GRANTED],
            [false, true, Voter::ACCESS_GRANTED],
            [true, true, Voter::ACCESS_GRANTED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
