<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\CdpAuthVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\CdpAuthVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class CdpAuthAuthVoter extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new CdpAuthVoter();
    }

    /**
     * @dataProvider providerAuth
     */
    public function testAuth(bool $isAmapien, int $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapien()->willReturn($isAmapien);

        $this->assertVote(
            $expected,
            $user->reveal(),
            null,
            CdpAuthVoter::ACTION_CDP_AUTH
        );
    }

    public function providerAuth()
    {
        return [
            'isAmapien' => [true, Voter::ACCESS_GRANTED],
            'isNotAmapien' => [false, Voter::ACCESS_DENIED],
        ];
    }
}
