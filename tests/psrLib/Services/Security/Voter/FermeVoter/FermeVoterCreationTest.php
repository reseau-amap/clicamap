<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterCreationTest extends FermeAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider createFermeProvider
     */
    public function testCreateAmapien(bool $isAdmin, bool $isFermeRegroupementAdmin, bool $isAmapAdmin, int $res): void
    {
        $this->assertVote(
            $res,
            $this->prophesizeUser($isAdmin, $isFermeRegroupementAdmin, $isAmapAdmin),
            null,
            FermeVoter::ACTION_FERME_CREATE
        );
    }

    public function createFermeProvider(): array
    {
        return [
            [false, false, false, Voter::ACCESS_DENIED],
            [true, false, false, Voter::ACCESS_GRANTED],
            [false, true, false, Voter::ACCESS_GRANTED],
            [false, false, true, Voter::ACCESS_GRANTED],
        ];
    }

    public function prophesizeUser(bool $isAdmin, bool $isFermeRegroupementAdmin, bool $isAmapAdmin): User
    {
        $user = $this->prophesize(User::class);
        $user->isAdmin()->willReturn($isAdmin);
        $user->isFermeRegroupementAdmin()->willReturn($isFermeRegroupementAdmin);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);

        return $user->reveal();
    }
}
