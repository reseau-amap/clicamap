<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterRefProduitTest extends FermeAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider provideRefProduitRegroupement
     */
    public function testRefProduitRegroupement(bool $isAdminOf, bool $isGrantedDisplay, int $expectedVote): void
    {
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isFermeRegroupementAdminOf($ferme)->willReturn($isAdminOf);
        $this->authorizationChecker->isGranted(FermeVoter::ACTION_FERME_DISPLAY, $ferme)->willReturn($isGrantedDisplay);

        $this->assertVote(
            $expectedVote,
            $user->reveal(),
            $ferme,
            FermeVoter::ACTION_FERME_REF_PRODUITS
        );
    }

    public function provideRefProduitRegroupement(): array
    {
        return [
            [true, true, Voter::ACCESS_DENIED],
            [true, false, Voter::ACCESS_DENIED],
            [false, true, Voter::ACCESS_GRANTED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
