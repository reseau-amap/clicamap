<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterListTest extends FermeAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider fermeListProvider
     */
    public function testFermeLivraisonList(bool $isPaysanOf, bool $isAdhesionActive, int $expectedVote): void
    {
        $ferme = $this->prophesize(Ferme::class);
        $ferme->isAdhesionActive()->willReturn($isAdhesionActive);
        $fermeReveled = $ferme->reveal();
        $user = $this->prophesize(User::class);
        $user->isPaysanActifOf($fermeReveled)->willReturn($isPaysanOf);

        $this->assertVote(
            $expectedVote,
            $user->reveal(),
            $fermeReveled,
            FermeVoter::ACTION_FERME_LIVRAISON_LIST
        );
    }

    public function fermeListProvider(): array
    {
        return [
            [true, true, Voter::ACCESS_GRANTED],
            [true, false, Voter::ACCESS_DENIED],
            [false, true, Voter::ACCESS_DENIED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
