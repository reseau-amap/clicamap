<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterEditCommentTest extends FermeAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider fermeListProvider
     */
    public function testFermeCommentEditComment(bool $isAdminOf, int $expectedVote): void
    {
        $ferme = $this->prophesize(Ferme::class);
        $fermeReveled = $ferme->reveal();
        $user = $this->prophesize(User::class);
        $user->isAdminOf($fermeReveled)->willReturn($isAdminOf);

        $this->assertVote(
            $expectedVote,
            $user->reveal(),
            $fermeReveled,
            FermeVoter::ACTION_FERME_EDIT_COMMENT
        );
    }

    public function fermeListProvider(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
