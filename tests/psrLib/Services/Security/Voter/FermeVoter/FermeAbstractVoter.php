<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Test\Services\Security\SecurityTestAbstract;

abstract class FermeAbstractVoter extends SecurityTestAbstract
{
    use ProphecyTrait;

    protected ObjectProphecy $authorizationChecker;

    public function setUp(): void
    {
        $this->authorizationChecker = $this->prophesize(AuthorizationCheckerInterface::class);
        $this->voter = new FermeVoter($this->authorizationChecker->reveal());
    }
}
