<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Doctrine\Common\Collections\Collection;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterRemoveTest extends FermeAbstractVoter
{
    /**
     * @dataProvider removeFermeProvider
     */
    public function testRemoveFerme(bool $admin, bool $contractsEmpty, int $expected): void
    {
        $mcs = $this->prophesize(Collection::class);
        $mcs->isEmpty()->willReturn($contractsEmpty);

        $ferme = $this->prophesize(Ferme::class);
        $ferme->getModeleContrats()->willReturn($mcs->reveal());

        $user = $this->prophesize(User::class);
        $user->isAdmin()->willReturn($admin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            $ferme->reveal(),
            FermeVoter::ACTION_FERME_REMOVE
        );
    }

    public function removeFermeProvider()
    {
        return [
            [true, true, Voter::ACCESS_GRANTED],
            [true, false, Voter::ACCESS_DENIED],
            [false, true, Voter::ACCESS_DENIED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
