<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\FermeVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\FermeVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class FermeVoterDisplayTest extends FermeAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider fermeDisplayProvider
     */
    public function testFermeDisplay(
        bool $isAmapAdmin,
        bool $isAdmin,
        bool $isRefProduitOfFerme,
        bool $isPaysanOf,
        bool $isFermeRegroupementAdminOf,
        int $expectedVote
    ): void {
        $ferme = new Ferme();

        $user = $this->prophesize(User::class);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);
        $user->isAdmin()->willReturn($isAdmin);
        $user->isRefProduitOfFerme($ferme)->willReturn($isRefProduitOfFerme);
        $user->isPaysanActifOf($ferme)->willReturn($isPaysanOf);
        $user->isFermeRegroupementAdminOf($ferme)->willReturn($isFermeRegroupementAdminOf);

        $this->assertVote(
            $expectedVote,
            $user->reveal(),
            $ferme,
            FermeVoter::ACTION_FERME_DISPLAY
        );
    }

    public function fermeDisplayProvider(): array
    {
        return [
            [true, false, false, false, false, Voter::ACCESS_GRANTED],
            [false, true, false, false, false, Voter::ACCESS_GRANTED],
            [false, false, true, false, false, Voter::ACCESS_GRANTED],
            [false, false, false, true, false, Voter::ACCESS_GRANTED],
            [false, false, false, false, true, Voter::ACCESS_GRANTED],
            [false, false, false, false, false, Voter::ACCESS_DENIED],
        ];
    }
}
