<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ModelContratCopyVoter;

use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\DTO\ControllerModeleContratCopyArgumentDTO;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\Security\Voters\ModelContratCopyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class ModelContratCopyVoterCopyTest extends SecurityTestAbstract
{
    use ProphecyTrait;

    private ObjectProphecy $modelContratRepository;

    public function setUp(): void
    {
        $this->modelContratRepository = $this->prophesize(ModeleContratRepository::class);
        $this->voter = new ModelContratCopyVoter(
            $this->modelContratRepository->reveal()
        );
    }

    /**
     * @dataProvider providerAmapAdmin
     */
    public function testAmapAdmin(bool $isAmapAdmin, int $nbContrats, int $expected): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();

        $this->modelContratRepository->countV2ByAmapFerme($amap, $ferme)->willReturn($nbContrats);
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn($isAmapAdmin);
        $user->isAmapienOfAmap($amap)->willReturn(false);

        $this->assertVote(
            $expected,
            $user->reveal(),
            new ControllerModeleContratCopyArgumentDTO($amap, $ferme),
            ModelContratCopyVoter::ACTION_CONTRACT_MODEL_COPY
        );
    }

    public function providerAmapAdmin()
    {
        return [
            [true, 0, Voter::ACCESS_DENIED],
            [true, 1, Voter::ACCESS_GRANTED],
            [false, 0, Voter::ACCESS_DENIED],
            [false, 1, Voter::ACCESS_DENIED],
        ];
    }

    /**
     * @dataProvider providerAmapienRef
     */
    public function testAmapienRef(bool $isAmapien, bool $isFermeRef, int $nbContrats, int $expected): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();

        $this->modelContratRepository->countV2ByAmapFerme($amap, $ferme)->willReturn($nbContrats);
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn(false);
        $user->isAmapienOfAmap($amap)->willReturn($isAmapien);
        $user->isRefProduitOfFerme($ferme)->willReturn($isFermeRef);

        $this->assertVote(
            $expected,
            $user->reveal(),
            new ControllerModeleContratCopyArgumentDTO($amap, $ferme),
            ModelContratCopyVoter::ACTION_CONTRACT_MODEL_COPY
        );
    }

    public function providerAmapienRef()
    {
        return [
            [true, true, 0, Voter::ACCESS_DENIED],
            [true, true, 1, Voter::ACCESS_GRANTED],
            [false, true, 0, Voter::ACCESS_DENIED],
            [false, true, 1, Voter::ACCESS_DENIED],
            [true, false, 0, Voter::ACCESS_DENIED],
            [true, false, 1, Voter::ACCESS_DENIED],
            [false, false, 0, Voter::ACCESS_DENIED],
            [false, false, 1, Voter::ACCESS_DENIED],
        ];
    }
}
