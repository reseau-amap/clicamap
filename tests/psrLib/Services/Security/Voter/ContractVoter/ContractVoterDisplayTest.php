<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterDisplayTest extends ContratAbstractVoter
{
    use ProphecyTrait;

    public function testAmapSameContract(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn(true);

        $contract = $this->buildContract($amap, $this->prophesize(Ferme::class)->reveal());

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user->reveal(),
            $contract,
            ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY
        );
    }

    public function testPaysanSameContract(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn(false);
        $user->isPaysanActifOf($ferme)->willReturn(true);

        $contract = $this->buildContract($amap, $ferme);

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user->reveal(),
            $contract,
            ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY
        );
    }

    public function testFermeRegroupementSame(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ferme = $this->prophesize(Ferme::class)->reveal();
        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn(false);
        $user->isPaysanActifOf($ferme)->willReturn(false);
        $user->isFermeRegroupementAdminOf($ferme)->willReturn(true);

        $contract = $this->buildContract($amap, $ferme);

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user->reveal(),
            $contract,
            ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY
        );
    }

    private function buildContract(Amap $amap, Ferme $ferme)
    {
        $ll = new AmapLivraisonLieu();
        $ll->setAmap($amap);
        $mc = new ModeleContrat();
        $mc->setLivraisonLieu($ll);
        $mc->setFerme($ferme);
        $contrat = new Contrat();
        $contrat->setModeleContrat($mc);

        return $contrat;
    }
}
