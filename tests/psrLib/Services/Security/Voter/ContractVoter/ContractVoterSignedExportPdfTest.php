<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Files\ContratPdf;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterSignedExportPdfTest extends ContratAbstractVoter
{
    /**
     * @dataProvider exportPdfProvider
     */
    public function testExportPdf(int $version, bool $withPdf, bool $mcPermission, int $result): void
    {
        $user = $this->prophesize(User::class)->reveal();

        $contract = new Contrat();
        $mc = new ModeleContrat();
        $mc->setVersion($version);

        $contract->setModeleContrat($mc);
        if ($withPdf) {
            $contract->setPdf($this->prophesize(ContratPdf::class)->reveal());
        }

        $this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY, $contract)->willReturn($mcPermission);

        $this->assertVote(
            $result,
            $user,
            $contract,
            ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_PDF
        );
    }

    public function exportPdfProvider()
    {
        return [
            [1, false, false, Voter::ACCESS_DENIED],
            [2, false, false, Voter::ACCESS_DENIED],
            [1, true, true, Voter::ACCESS_DENIED],
            [2, true, false, Voter::ACCESS_DENIED],
            [2, false, true, Voter::ACCESS_DENIED],
            [2, true, true, Voter::ACCESS_GRANTED],
        ];
    }
}
