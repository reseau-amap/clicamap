<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterRedirectCdpTest extends ContratAbstractVoter
{
    use ProphecyTrait;

    /**
     * @dataProvider provideData
     */
    public function testRedirectCdp(bool $isCdp, bool $isContractUser, int $expected): void
    {
        $contrat = $this->prophesize(Contrat::class);
        $contrat->isCdp()->willReturn($isCdp);

        $user = $this->prophesize(User::class);

        $amapien = $this->prophesize(Amapien::class);
        if ($isContractUser) {
            $amapien->getUser()->willReturn($user->reveal());
        } else {
            $amapien->getUser()->willReturn($this->prophesize(User::class)->reveal());
        }

        $contrat->getAmapien()->willReturn($amapien->reveal());

        $this->assertVote(
            $expected,
            $user->reveal(),
            $contrat->reveal(),
            ContractVoter::ACTION_CONTRACT_SIGNED_REDIRECT_CDP
        );
    }

    public function provideData()
    {
        return [
            [true, true, Voter::ACCESS_GRANTED],
            [true, false, Voter::ACCESS_DENIED],
            [false, true, Voter::ACCESS_DENIED],
            [false, false, Voter::ACCESS_DENIED],
        ];
    }
}
