<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterListPaysanTest extends ContratAbstractVoter
{
    /**
     * @dataProvider providerUser
     */
    public function testListPaysan(bool $isPaysan, bool $isFermeRegroupementAdmin, int $res): void
    {
        $this->assertVote(
            $res,
            $this->prophesizeUser($isPaysan, $isFermeRegroupementAdmin),
            null,
            ContractVoter::ACTION_CONTRACT_SIGNED_LIST_PAYSAN
        );
    }

    public function providerUser(): array
    {
        return [
            [false, false, Voter::ACCESS_DENIED],
            [true, false, Voter::ACCESS_GRANTED],
            [false, true, Voter::ACCESS_GRANTED],
            [true, true, Voter::ACCESS_GRANTED],
        ];
    }

    public function prophesizeUser(bool $isPaysan, bool $isFermeRegroupementAdmin): User
    {
        $user = $this->prophesize(User::class);
        $user->isPaysan()->willReturn($isPaysan);
        $user->isFermeRegroupementAdmin()->willReturn($isFermeRegroupementAdmin);

        return $user->reveal();
    }
}
