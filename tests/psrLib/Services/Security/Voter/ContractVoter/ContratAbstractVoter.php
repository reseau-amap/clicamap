<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Test\Services\Security\SecurityTestAbstract;

abstract class ContratAbstractVoter extends SecurityTestAbstract
{
    protected ObjectProphecy $authorizationChecker;

    public function setUp(): void
    {
        $this->authorizationChecker = $this->prophesize(AuthorizationCheckerInterface::class);

        $this->voter = new ContractVoter(
            $this->authorizationChecker->reveal(),
        );
    }
}
