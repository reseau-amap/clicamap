<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterSignedExportXlsTest extends ContratAbstractVoter
{
    /**
     * @dataProvider exportXslProvider
     */
    public function testExportXls(bool $mcArchived, bool $grantedDisplay, int $result): void
    {
        $user = $this->prophesize(User::class)->reveal();

        $contract = new Contrat();
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->isArchived()->willReturn($mcArchived);

        $contract->setModeleContrat($mc->reveal());

        $this->authorizationChecker->isGranted(ContractVoter::ACTION_CONTRACT_SIGNED_DISPLAY, $contract)->willReturn($grantedDisplay);

        $this->assertVote(
            $result,
            $user,
            $contract,
            ContractVoter::ACTION_CONTRACT_SIGNED_EXPORT_XLS
        );
    }

    public function exportXslProvider()
    {
        return [
            [false, false, Voter::ACCESS_DENIED],
            [false, true, Voter::ACCESS_GRANTED],
            [true, false, Voter::ACCESS_DENIED],
            [true, true, Voter::ACCESS_DENIED],
        ];
    }
}
