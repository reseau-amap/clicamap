<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterListAmapTest extends ContratAbstractVoter
{
    /**
     * @dataProvider providerContractListAdmin
     */
    public function testContractListAdmin(bool $isAmapAdmin, int $expectedVoter): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapAdmin()->willReturn($isAmapAdmin);
        $this->assertVote(
            $expectedVoter,
            $user->reveal(),
            null,
            ContractVoter::ACTION_CONTRACT_SIGNED_LIST_AMAP
        );
    }

    public function providerContractListAdmin(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
