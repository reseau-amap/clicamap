<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\ContractVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\ContractVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractVoterListAmapienRefTest extends ContratAbstractVoter
{
    /**
     * @dataProvider providerContractListAdmin
     */
    public function testContractListAmapienRef(bool $isAmapienRef, int $expectedVoter): void
    {
        $user = $this->prophesize(User::class);
        $user->isRefProduit()->willReturn($isAmapienRef);
        $this->assertVote(
            $expectedVoter,
            $user->reveal(),
            null,
            ContractVoter::ACTION_CONTRACT_SIGNED_LIST_AMAPIENREF
        );
    }

    public function providerContractListAdmin(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
