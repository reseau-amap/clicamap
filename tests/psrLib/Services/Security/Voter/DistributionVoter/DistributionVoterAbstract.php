<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\DistributionVoter;

use Carbon\Carbon;
use Prophecy\Argument;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapDistributionDetail;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\DistributionVoter;
use Test\Services\Security\SecurityTestAbstract;

abstract class DistributionVoterAbstract extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new DistributionVoter();
    }

    protected function buildUser(Amap $amap)
    {
        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn(true);
        $user->isAmapienOfAmap(Argument::not($amap))->willReturn(false);

        return $user->reveal();
    }

    protected function buildAmapien(User $user)
    {
        $amapien = $this->prophesize(Amapien::class);
        $amapien->getUser()->willReturn($user);

        return $amapien->reveal();
    }

    protected function buildLl(Amap $amap): AmapLivraisonLieu
    {
        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getAmap()->willReturn($amap);

        return $ll->reveal();
    }

    protected function buildDistribution(AmapLivraisonLieu $amapLivraisonLieu, Time $heureDebut, Time $heureFin, int $nbPersonnes, Carbon $date)
    {
        return new AmapDistribution(
            AmapDistributionDetail::create($heureDebut, $heureFin, $nbPersonnes, 'tache'),
            $amapLivraisonLieu,
            $date
        );
    }
}
