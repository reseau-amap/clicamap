<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\DistributionVoter;

use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\DistributionVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class DistributionVoterDetailTest extends DistributionVoterAbstract
{
    /**
     * @dataProvider providerDetailAsAmapAdmin
     */
    public function testDetailAsAmapAdmin(bool $amapAdmin, int $expected): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ll = $this->buildLl($amap);

        $distribution = $this->prophesize(AmapDistribution::class);
        $distribution->getAmapLivraisonLieu()->willReturn($ll);

        $user = $this->prophesize(User::class);
        $user->isAmapAdminOfAmap($amap)->willReturn($amapAdmin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            $distribution->reveal(),
            DistributionVoter::ACTION_DISTRIBUTION_DETAIL
        );
    }

    public function providerDetailAsAmapAdmin(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
