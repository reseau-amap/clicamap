<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\DistributionVoter;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\Services\Security\Voters\DistributionVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class DistributionVoterAmapienUnRegisterTest extends DistributionVoterAbstract
{
    public function testUnRegisterAsAmapienValid(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->buildUser($amap);

        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien($this->buildAmapien($user));
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER
        );
    }

    public function testUnRegisterAsAmapienNotRegistered(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->buildUser($amap);

        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER
        );
    }

    public function testUnRegisterAsAmapienAfterStart(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->buildUser($amap);

        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien($this->buildAmapien($user));
        Carbon::setTestNow('2000-01-01 12:00:00');

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_UNREGISTER
        );
    }
}
