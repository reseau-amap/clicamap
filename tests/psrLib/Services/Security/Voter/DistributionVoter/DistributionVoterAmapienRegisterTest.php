<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\DistributionVoter;

use Carbon\Carbon;
use PsrLib\DTO\Time;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapDistribution;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\DistributionVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @internal
 *
 * @coversNothing
 */
class DistributionVoterAmapienRegisterTest extends DistributionVoterAbstract
{
    public function testRegisterAsNotAmapienOfAmap(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getAmap()->willReturn($amap);
        $distribution = $this->prophesize(AmapDistribution::class);
        $distribution->getAmapLivraisonLieu()->willReturn($ll->reveal());

        $user = $this->prophesize(User::class);
        $user->isAmapienOfAmap($amap)->willReturn(false);

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user->reveal(),
            $distribution->reveal(),
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }

    public function testRegisterAsAmapienValid(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->buildUser($amap);
        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->assertVote(
            Voter::ACCESS_GRANTED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }

    public function testRegisterAsAmapienInvalidAmap(): void
    {
        $amapien = $this->buildUser($this->prophesize(Amap::class)->reveal());
        $ll = $this->buildLl($this->prophesize(Amap::class)->reveal());
        $distribution = $this->buildDistribution(
            $ll,
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $amapien,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }

    public function testRegisterAsAmapienAlreadyRegistered(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();
        $user = $this->buildUser($amap);

        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien($this->buildAmapien($user));
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }

    public function testRegisterAsAmapienAfterStart(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();

        $user = $this->buildUser($amap);
        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            10,
            new Carbon('2000-01-01')
        );
        Carbon::setTestNow('2000-01-01 12:00:00');

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }

    public function testRegisterAsAmapienFull(): void
    {
        $amap = $this->prophesize(Amap::class)->reveal();

        $user = $this->buildUser($amap);
        $distribution = $this->buildDistribution(
            $this->buildLl($amap),
            new Time(12, 0),
            new Time(13, 0),
            3,
            new Carbon('2000-01-01')
        );
        $distribution->addAmapien($this->buildAmapien($this->buildUser($amap)));
        $distribution->addAmapien($this->buildAmapien($this->buildUser($amap)));
        $distribution->addAmapien($this->buildAmapien($this->buildUser($amap)));
        Carbon::setTestNow('2000-01-01 11:00:00');

        $this->assertVote(
            Voter::ACCESS_DENIED,
            $user,
            $distribution,
            DistributionVoter::ACTION_DISTRIBUTION_AMAPIEN_REGISTER
        );
    }
}
