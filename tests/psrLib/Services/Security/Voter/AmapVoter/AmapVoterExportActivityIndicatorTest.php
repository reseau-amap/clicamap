<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\AmapVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\AmapVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class AmapVoterExportActivityIndicatorTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new AmapVoter();
    }

    /**
     * @dataProvider userProvider
     */
    public function testUser(bool $adminDep, bool $adminRegion, int $result): void
    {
        $this->assertVote(
            $result,
            $this->prophesizeUser($adminDep, $adminRegion),
            null,
            AmapVoter::ACTION_AMAP_EXPORT_ACTIVITY_INDICATORS
        );
    }

    public function userProvider(): array
    {
        return [
            [false, false, Voter::ACCESS_DENIED],
            [true, false, Voter::ACCESS_GRANTED],
            [false, true, Voter::ACCESS_GRANTED],
        ];
    }

    public function prophesizeUser(bool $adminDep, bool $adminRegion): User
    {
        $user = $this->prophesize(User::class);
        $user->isAdminDepartment()->willReturn($adminDep);
        $user->isAdminRegion()->willReturn($adminRegion);

        return $user->reveal();
    }
}
