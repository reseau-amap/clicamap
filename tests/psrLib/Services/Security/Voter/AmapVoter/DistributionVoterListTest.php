<?php

declare(strict_types=1);

namespace Test\Services\Security\Voter\AmapVoter;

use PsrLib\ORM\Entity\User;
use PsrLib\Services\Security\Voters\AmapVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Test\Services\Security\SecurityTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class DistributionVoterListTest extends SecurityTestAbstract
{
    public function setUp(): void
    {
        $this->voter = new AmapVoter();
    }

    /**
     * @dataProvider providerList
     */
    public function testList(bool $amapAdmin, int $expected): void
    {
        $user = $this->prophesize(User::class);
        $user->isAmapAdmin()->willReturn($amapAdmin);

        $this->assertVote(
            $expected,
            $user->reveal(),
            null,
            AmapVoter::ACTION_DISTRIBUTION_LIST
        );
    }

    public function providerList(): array
    {
        return [
            [true, Voter::ACCESS_GRANTED],
            [false, Voter::ACCESS_DENIED],
        ];
    }
}
