<?php

declare(strict_types=1);

namespace Test\Services;

use PHPUnit\Framework\TestCase;
use PsrLib\DTO\Nominatim\NominatimResponse;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\VilleRepository;
use PsrLib\Services\EntityBuilder\VilleFactory;
use PsrLib\Services\GeocodeQuery;
use PsrLib\Services\NominatimClient;

/**
 * @internal
 *
 * @coversNothing
 */
class GeocodeQueryTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \PsrLib\Services\GeocodeQuery $geocodeQuery;

    private \Prophecy\Prophecy\ObjectProphecy $nominatimClient;

    private \Prophecy\Prophecy\ObjectProphecy $villeRepo;

    private \Prophecy\Prophecy\ObjectProphecy $villeFactory;

    public function setUp(): void
    {
        $this->villeRepo = $this->prophesize(VilleRepository::class);

        $this->nominatimClient = $this->prophesize(NominatimClient::class);
        $this->villeFactory = $this->prophesize(VilleFactory::class);

        $this->geocodeQuery = new GeocodeQuery(
            $this->nominatimClient->reveal(),
            $this->villeRepo->reveal(),
            $this->villeFactory->reveal()
        );
    }

    public function testEmpty(): void
    {
        $this->nominatimClient->geocode('test')->willReturn([]);
        $res = $this->geocodeQuery->geocode_suggestions('test');

        self::assertSame([], $res);
    }

    public function testFilterNoCoordinates(): void
    {
        $address1 = $this->prophesize(NominatimResponse::class)->reveal();
        $address2 = $this->prophesizeNominatimResponseWithCoordinates();

        $ville1 = $this->prophesize(Ville::class)->reveal();

        $this->villeRepo->getOneFromAddress($address1)->shouldNotBeCalled();
        $this->villeRepo->getOneFromAddress($address2)->willReturn($ville1);

        $this->nominatimClient->geocode('test')->willReturn([$address1, $address2]);
        $res = $this->geocodeQuery->geocode_suggestions('test');

        self::assertCount(1, $res);
        self::assertSame($address2, $res[0]->getAddress());
        self::assertSame($ville1, $res[0]->getVille());
    }

    public function testCityInDb(): void
    {
        $address1 = $this->prophesizeNominatimResponseWithCoordinates();
        $address2 = $this->prophesizeNominatimResponseWithCoordinates();

        $ville1 = $this->prophesize(Ville::class)->reveal();
        $ville2 = $this->prophesize(Ville::class)->reveal();

        $this->villeRepo->getOneFromAddress($address1)->willReturn($ville1);
        $this->villeRepo->getOneFromAddress($address2)->willReturn($ville2);

        $this->nominatimClient->geocode('test')->willReturn([$address1, $address2]);
        $res = $this->geocodeQuery->geocode_suggestions('test');

        self::assertCount(2, $res);
        self::assertSame($address1, $res[0]->getAddress());
        self::assertSame($ville1, $res[0]->getVille());
        self::assertSame($address2, $res[1]->getAddress());
        self::assertSame($ville2, $res[1]->getVille());
    }

    public function testCityNotInDbCreationSuccess(): void
    {
        $address1 = $this->prophesizeNominatimResponseWithCoordinates();
        $address2 = $this->prophesizeNominatimResponseWithCoordinates();

        $ville1 = $this->prophesize(Ville::class)->reveal();
        $ville2 = $this->prophesize(Ville::class)->reveal();

        $this->villeRepo->getOneFromAddress($address1)->willReturn($ville1);
        $this->villeRepo->getOneFromAddress($address2)->willReturn(null);

        $this->villeFactory->create($address2)->willReturn($ville2);
        $this->villeRepo->add($ville2, true);

        $this->nominatimClient->geocode('test')->willReturn([$address1, $address2]);
        $res = $this->geocodeQuery->geocode_suggestions('test');

        self::assertCount(2, $res);
        self::assertSame($address1, $res[0]->getAddress());
        self::assertSame($ville1, $res[0]->getVille());
        self::assertSame($address2, $res[1]->getAddress());
        self::assertSame($ville2, $res[1]->getVille());
    }

    public function testCityNotInDbCreationError(): void
    {
        $address1 = $this->prophesizeNominatimResponseWithCoordinates();
        $address2 = $this->prophesizeNominatimResponseWithCoordinates();

        $ville1 = $this->prophesize(Ville::class)->reveal();

        $this->villeRepo->getOneFromAddress($address1)->willReturn($ville1);
        $this->villeRepo->getOneFromAddress($address2)->willReturn(null);

        $this->villeFactory->create($address2)->willReturn(null);

        $this->nominatimClient->geocode('test')->willReturn([$address1, $address2]);
        $res = $this->geocodeQuery->geocode_suggestions('test');

        self::assertCount(1, $res);
        self::assertSame($address1, $res[0]->getAddress());
        self::assertSame($ville1, $res[0]->getVille());
    }

    private function prophesizeNominatimResponseWithCoordinates(): NominatimResponse
    {
        $address = $this->prophesize(NominatimResponse::class);
        $address->getLat()->willReturn(123);
        $address->getLon()->willReturn(123);

        return $address->reveal();
    }
}
