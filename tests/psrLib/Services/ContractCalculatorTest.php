<?php

declare(strict_types=1);

namespace Test\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Money\Money;
use PHPUnit\Framework\TestCase;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratCellule;
use PsrLib\ORM\Entity\Embeddable\PrixTVA;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\Services\ContractCalculator;

/**
 * @internal
 *
 * @coversNothing
 */
class ContractCalculatorTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \PsrLib\Services\ContractCalculator $contractCalculator;

    public function setUp(): void
    {
        $this->contractCalculator = new ContractCalculator();
    }

    public function testTotalTTCEmpty(): void
    {
        $contract = $this->prophesizeContract([]);
        $res = $this->contractCalculator->totalTTC($contract);

        self::assertTrue(Money::EUR(0)->equals($res));
    }

    public function testTotalTTC(): void
    {
        $contract = $this->prophesizeContract([
            $this->prophesizeCell(Money::EUR(100), null, 1),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
        ]);
        $res = $this->contractCalculator->totalTTC($contract);

        self::assertTrue(Money::EUR(500)->equals($res));
    }

    public function testTotalHT(): void
    {
        $contract = $this->prophesizeContract([
            $this->prophesizeCell(Money::EUR(100), null, 1),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
        ]);
        $res = $this->contractCalculator->totalHT($contract);

        self::assertTrue(Money::EUR(464)->equals($res));
    }

    public function testTotalTVA(): void
    {
        $contract = $this->prophesizeContract([
            $this->prophesizeCell(Money::EUR(100), null, 1),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 20, 2),
        ]);
        $res = $this->contractCalculator->totalWithTva($contract, 10);

        self::assertTrue(Money::EUR(72)->equals($res));
    }

    public function testTotalTVANull(): void
    {
        $contract = $this->prophesizeContract([
            $this->prophesizeCell(Money::EUR(100), null, 1),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 20, 2),
        ]);
        $res = $this->contractCalculator->totalWithTva($contract, null);

        self::assertTrue(Money::EUR(0)->equals($res));
    }

    public function testTotalTVANotInContract(): void
    {
        $contract = $this->prophesizeContract([
            $this->prophesizeCell(Money::EUR(100), null, 1),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 10, 2),
            $this->prophesizeCell(Money::EUR(200), 20, 2),
        ]);
        $res = $this->contractCalculator->totalWithTva($contract, 5.5);

        self::assertTrue(Money::EUR(0)->equals($res));
    }

    private function prophesizeContract(array $cells): Contrat
    {
        $c = $this->prophesize(Contrat::class);
        $c->getCellules()->willReturn(new ArrayCollection($cells));

        return $c->reveal();
    }

    private function prophesizeCell(Money $priceTTC, ?float $tva, int $quantity)
    {
        $cell = $this->prophesize(ContratCellule::class);
        $cell->getQuantite()->willReturn($quantity);
        $mcp = $this->prophesize(ModeleContratProduit::class);
        $prix = new PrixTVA();
        $prix->setPrixTTC($priceTTC);
        $prix->setTva($tva);

        $mcp->getPrix()->willReturn($prix);

        $cell->getModeleContratProduit()->willReturn($mcp->reveal());

        return $cell->reveal();
    }
}
