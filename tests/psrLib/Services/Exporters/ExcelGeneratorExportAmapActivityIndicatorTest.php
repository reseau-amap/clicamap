<?php

declare(strict_types=1);

namespace Test\Services\Exporters;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use PsrLib\DTO\SearchAmapState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\AmapAnneeAdhesion;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Embeddable\Name;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\Region;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\AdhesionAmapAmapienRepository;
use PsrLib\ORM\Repository\AmapDistributionRepository;
use PsrLib\ORM\Repository\AmapRepository;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\Exporters\ExcelGeneratorExportAmapActivityIndicator;

/**
 * @internal
 *
 * @coversNothing
 */
class ExcelGeneratorExportAmapActivityIndicatorTest extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    private \PsrLib\Services\Exporters\ExcelGeneratorExportAmapActivityIndicator $generator;

    private \Prophecy\Prophecy\ObjectProphecy $amapRepo;

    private \Prophecy\Prophecy\ObjectProphecy $modeleContratRepo;

    private \Prophecy\Prophecy\ObjectProphecy $amapDistributionRepo;

    private \Prophecy\Prophecy\ObjectProphecy $adhesionAmapAmapienRepository;

    public function setUp(): void
    {
        $this->amapRepo = $this->prophesize(AmapRepository::class);
        $this->modeleContratRepo = $this->prophesize(ModeleContratRepository::class);
        $this->amapDistributionRepo = $this->prophesize(AmapDistributionRepository::class);
        $this->adhesionAmapAmapienRepository = $this->prophesize(AdhesionAmapAmapienRepository::class);

        $this->generator = new ExcelGeneratorExportAmapActivityIndicator(
            $this->amapRepo->reveal(),
            $this->modeleContratRepo->reveal(),
            $this->amapDistributionRepo->reveal(),
            $this->adhesionAmapAmapienRepository->reveal()
        );

        Carbon::setTestNow('2000-01-02');
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testHeaderRegion(): void
    {
        $region = $this->prophesize(Region::class);
        $region->getNom()->willReturn('Region_nom');
        $search = new SearchAmapState();
        $search->setRegion($region->reveal());

        $this->amapRepo->search($search)->willReturn([]);
        $this
            ->amapDistributionRepo
            ->countDistributionWithAmapienByDateAmapMultiple(
                Argument::any(),
                Argument::any(),
                Argument::any(),
            )
            ->willReturn([])
        ;
        $this
            ->adhesionAmapAmapienRepository
            ->countAdhesionsByAmapMultiple(
                Argument::any()
            )
            ->willReturn([])
        ;

        [$amapCount, $file] = $this->generator->genererExportAmap($search);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSame(0, $amapCount);
        self::assertSame('Région Region_nom', $sheet->getCell('A1')->getValue());
    }

    public function testHeaderDepartement(): void
    {
        $region = $this->prophesize(Region::class);
        $region->getNom()->willReturn('Region_nom');

        $departement = $this->prophesize(Departement::class);
        $departement->getNom()->willReturn('Departement_nom');

        $search = new SearchAmapState();
        $search->setRegion($region->reveal());
        $search->setDepartement($departement->reveal());

        $this->amapRepo->search($search)->willReturn([]);
        $this
            ->amapDistributionRepo
            ->countDistributionWithAmapienByDateAmapMultiple(
                Argument::any(),
                Argument::any(),
                Argument::any(),
            )
            ->willReturn([])
        ;
        $this
            ->adhesionAmapAmapienRepository
            ->countAdhesionsByAmapMultiple(
                Argument::any()
            )
            ->willReturn([])
        ;

        [$amapCount, $file] = $this->generator->genererExportAmap($search);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSame(0, $amapCount);
        self::assertSame('Département Departement_nom', $sheet->getCell('A1')->getValue());
    }

    public function testGeneration(): void
    {
        $region = $this->prophesize(Region::class);
        $region->getNom()->willReturn('Region_nom');
        $search = new SearchAmapState();
        $search->setRegion($region->reveal());

        $amap1 = $this->buildAmap(
            1,
            'amap nom 1',
            123,
            'amap1@email',
            Amap::ETAT_CREATION,
            12,
            21,
            [1998],
            null
        );

        $referent = $this
            ->prophesizeAmapien('prenom ref', 'nom ref', 'referent@email')
        ;
        $amap2 = $this->buildAmap(
            2,
            'amap nom 2',
            456,
            'amap2@email',
            Amap::ETAT_FONCIONNEMENT,
            13,
            22,
            [1999],
            $referent
        );

        $amap3 = $this->buildAmap(
            3,
            'amap nom 3',
            null,
            'amap3@email',
            Amap::ETAT_ATTENTE,
            14,
            23,
            [2000],
            null
        );

        $this->buildContractbyAmap([$amap1, $amap2, $amap3]);
        $this->amapRepo->search($search)->willReturn([$amap1, $amap2, $amap3]);

        $this
            ->amapDistributionRepo
            ->countDistributionWithAmapienByDateAmapMultiple(
                Argument::exact([$amap1, $amap2, $amap3]),
                Argument::exact((new Carbon('2000-01-01'))->startOfDay()),
                Argument::exact((new Carbon('2000-01-31'))->endOfDay())
            )
            ->willReturn([
                2 => '10',
            ])
        ;
        $this
            ->adhesionAmapAmapienRepository
            ->countAdhesionsByAmapMultiple(
                Argument::exact([$amap1, $amap2, $amap3])
            )
            ->willReturn([
                2 => '11',
            ])
        ;

        [$amapCount, $file] = $this->generator->genererExportAmap($search);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();

        self::assertSame(3, $amapCount);
        self::assertSame('Région Region_nom', $sheet->getCell('A1')->getValue());
        self::assertSame('3 résultats (Extrait le 02/01/2000)', $sheet->getCell('A2')->getValue());

        self::assertSame('ID', $sheet->getCell('A3')->getValue());
        self::assertSame('Nom de l\'AMAP', $sheet->getCell('B3')->getValue());
        self::assertSame('Code postal', $sheet->getCell('C3')->getValue());
        self::assertSame('Email public', $sheet->getCell('D3')->getValue());
        self::assertSame('Nom & Prénom du correspondant réseau', $sheet->getCell('E3')->getValue());
        self::assertSame('adresse mail du correspondant réseau', $sheet->getCell('F3')->getValue());
        self::assertSame("AMAP active\u{a0}?", $sheet->getCell('G3')->getValue());
        self::assertSame('Nombre d\'amapiens actifs', $sheet->getCell('H3')->getValue());
        self::assertSame('Nombre de référents', $sheet->getCell('I3')->getValue());
        self::assertSame('Nombre de contrat signés en cours', $sheet->getCell('J3')->getValue());
        self::assertSame('Nombre de créneau distrib’ AMAP avec inscrits dans le mois', $sheet->getCell('K3')->getValue());
        self::assertSame('Nombre de reçus émis', $sheet->getCell('L3')->getValue());
        self::assertSame('Est adhérente cette année ou l\'année précédente ?', $sheet->getCell('M3')->getValue());

        self::assertSame(1, $sheet->getCell('A4')->getValue());
        self::assertSame(2, $sheet->getCell('A5')->getValue());
        self::assertSame(3, $sheet->getCell('A6')->getValue());

        self::assertSame('amap nom 1', $sheet->getCell('B4')->getValue());
        self::assertSame('amap nom 2', $sheet->getCell('B5')->getValue());
        self::assertSame('amap nom 3', $sheet->getCell('B6')->getValue());

        self::assertSame('00123', $sheet->getCell('C4')->getValue());
        self::assertSame('00456', $sheet->getCell('C5')->getValue());
        self::assertSame(null, $sheet->getCell('C6')->getValue());

        self::assertSame('amap1@email', $sheet->getCell('D4')->getValue());
        self::assertSame('amap2@email', $sheet->getCell('D5')->getValue());
        self::assertSame('amap3@email', $sheet->getCell('D6')->getValue());

        self::assertSame(null, $sheet->getCell('E4')->getValue());
        self::assertSame('Prenom ref NOM REF', $sheet->getCell('E5')->getValue());
        self::assertSame(null, $sheet->getCell('E6')->getValue());

        self::assertSame(null, $sheet->getCell('F4')->getValue());
        self::assertSame('referent@email', $sheet->getCell('F5')->getValue());
        self::assertSame(null, $sheet->getCell('F6')->getValue());

        self::assertSame('non', $sheet->getCell('G4')->getValue());
        self::assertSame('oui', $sheet->getCell('G5')->getValue());
        self::assertSame('non', $sheet->getCell('G6')->getValue());

        self::assertSame(12, $sheet->getCell('H4')->getValue());
        self::assertSame(13, $sheet->getCell('H5')->getValue());
        self::assertSame(14, $sheet->getCell('H6')->getValue());

        self::assertSame(21, $sheet->getCell('I4')->getValue());
        self::assertSame(22, $sheet->getCell('I5')->getValue());
        self::assertSame(23, $sheet->getCell('I6')->getValue());

        self::assertSame(0.0, $sheet->getCell('J4')->getValue());
        self::assertSame(1, $sheet->getCell('J5')->getValue());
        self::assertSame(0.0, $sheet->getCell('J6')->getValue());

        self::assertSame(0.0, $sheet->getCell('K4')->getValue());
        self::assertSame(10, $sheet->getCell('K5')->getValue());
        self::assertSame(0.0, $sheet->getCell('K6')->getValue());

        self::assertSame(0.0, $sheet->getCell('L4')->getValue());
        self::assertSame(11, $sheet->getCell('L5')->getValue());
        self::assertSame(0.0, $sheet->getCell('L6')->getValue());

        self::assertSame('non', $sheet->getCell('M4')->getValue());
        self::assertSame('oui', $sheet->getCell('M5')->getValue());
        self::assertSame('oui', $sheet->getCell('M6')->getValue());
    }

    /**
     * @param int[] $adhesionsAnnees
     */
    private function buildAmap(
        int $amapId,
        string $amapNom,
        ?int $amapCp,
        string $amapEmail,
        string $status,
        int $nbAmapiensActifs,
        int $nbAmapiensRef,
        array $adhesionsAnnees,
        Amapien $referent = null
    ) {
        $ville = null;
        if (null !== $amapCp) {
            $ville = new Ville($amapCp, 'nom', $this->prophesize(Departement::class)->reveal(), 'src');
        }

        $amap = $this->prophesize(Amap::class);
        $amap->getId()->willReturn($amapId);
        $amap->getNom()->willReturn($amapNom);
        $amap->getAdresseAdminVille()->willReturn($ville);
        $amap->getEmail()->willReturn($amapEmail);
        $amap->getAmapienRefReseau()->willReturn($referent);
        $amap->getAmapienRefReseau()->willReturn($referent);
        $amap->getEtat()->willReturn($status);
        $amap->countActiveAmapiens()->willReturn($nbAmapiensActifs);
        $amap->countAmapiensRef()->willReturn($nbAmapiensRef);

        $adhesions = [];
        foreach ($adhesionsAnnees as $adhesionsAnnee) {
            $adhesionProphecy = $this->prophesize(AmapAnneeAdhesion::class);
            $adhesionProphecy->getAnnee()->willReturn($adhesionsAnnee);
            $adhesions[] = $adhesionProphecy->reveal();
        }
        $amap->getAnneeAdhesions()->willReturn(new ArrayCollection($adhesions));

        return $amap->reveal();
    }

    /**
     * @param Amap[] $amaps
     */
    private function buildContractbyAmap($amaps): void
    {
        $mc1 = $this->prophesize(ModeleContrat::class);
        $mc1->isArchived()->willReturn(true);

        $mc2 = $this->prophesize(ModeleContrat::class);
        $mc2->isArchived()->willReturn(false);

        $this
            ->modeleContratRepo
            ->getFromAmapMultipleByAmap(Argument::exact($amaps))
            ->willReturn([
                2 => [
                    $mc1,
                    $mc2,
                ],
            ])
        ;
    }

    private function prophesizeAmapien(string $prenom, string $nom, string $emailString)
    {
        $name = new Name();
        $name->setFirstName($prenom);
        $name->setLastName($nom);

        $user = $this->prophesize(User::class);
        $user->getName()->willReturn($name);
        $email = new UserEmail();
        $email->setEmail($emailString);
        $user->getEmails()->willReturn(new ArrayCollection([$email]));

        $amapien = $this->prophesize(Amapien::class);
        $amapien->getUser()->willReturn($user->reveal());

        return $amapien->reveal();
    }
}
