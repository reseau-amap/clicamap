<?php

declare(strict_types=1);

namespace Test\Services\Exporters;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Ferme;
use PsrLib\ORM\Entity\Ville;
use PsrLib\ORM\Repository\ModeleContratRepository;
use PsrLib\Services\ContractCalculator;

abstract class ExcelGeneratorExportContratAccountAbstract extends TestCase
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    /**
     * @var ObjectProphecy
     */
    protected $contractCalculator;

    /**
     * @var ObjectProphecy
     */
    protected $modeleContratRepo;

    public function setUp(): void
    {
        $this->modeleContratRepo = $this->prophesize(ModeleContratRepository::class);

        $this->contractCalculator = $this->prophesize(ContractCalculator::class);

        Carbon::setTestNow(Carbon::create(2000, 1, 2, 3, 4, 5));
    }

    public function tearDown(): void
    {
        Carbon::setTestNow();
    }

    protected function prophesizeFerme(bool $vat): Ferme
    {
        $ferme = $this->prophesize(Ferme::class);
        $ferme->getNom()->willReturn('ferme nom');
        $ferme->getLibAdr()->willReturn('addr');
        $ferme->getVille()->willReturn(new Ville(1, 'ville', $this->prophesize(Departement::class)->reveal()));
        $ferme->getSiret()->willReturn('siret');
        $ferme->isTVA()->willReturn($vat);
        $ferme->getTvaDate()->willReturn(new Carbon('2000-01-03'));

        return $ferme->reveal();
    }
}
