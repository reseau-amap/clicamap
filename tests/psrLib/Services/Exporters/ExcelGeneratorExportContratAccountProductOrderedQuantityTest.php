<?php

declare(strict_types=1);

namespace Test\Services\Exporters;

use Doctrine\Common\Collections\ArrayCollection;
use Money\Money;
use Prophecy\Argument;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Embeddable\Address;
use PsrLib\ORM\Entity\Embeddable\Name;
use PsrLib\ORM\Entity\FermeProduit;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratProduit;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\Ville;
use PsrLib\Services\Exporters\ExcelGeneratorExportContratAccountProductOrderedQuantity;
use PsrLib\Services\TvaLabel;

/**
 * @internal
 *
 * @coversNothing
 */
class ExcelGeneratorExportContratAccountProductOrderedQuantityTest extends ExcelGeneratorExportContratAccountAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    use SheetValueTestTrait;

    /**
     * @var ExcelGeneratorExportContratAccountProductOrderedQuantity
     */
    protected $generator;

    public function setUp(): void
    {
        parent::setUp();

        $this->generator = new ExcelGeneratorExportContratAccountProductOrderedQuantity(
            $this->contractCalculator->reveal(),
            new TvaLabel(),
            $this->modeleContratRepo->reveal()
        );
    }

    public function testRenderNoVATMcSelected(): void
    {
        $p1 = $this->prophesizeFermeProduit(1);
        $p2 = $this->prophesizeFermeProduit(2);

        $mcp1 = $this->prophesizeModeleContratProduit('produit 1', false, $p1);
        $mcp2 = $this->prophesizeModeleContratProduit('produit 2', true, $p2);

        $state = new SearchContratSigneState();
        $state->setMc($this->prophesizeMc(
            $this->prophesizeAmap('Nom de l\'AMAP'),
            'Nom du contrat',
            [$mcp1, $mcp2],
            [
                [
                    ['product' => $mcp1, 'qty' => 1.5],
                    ['product' => $mcp2, 'qty' => 1.0],
                ],
                [
                    ['product' => $mcp1, 'qty' => 1.0],
                    ['product' => $mcp2, 'qty' => 0.0],
                ],
            ]
        ));
        $ferme = $this->prophesizeFerme(false);

        $file = $this->generator->generate($state, $ferme);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A4');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A6');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B6');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C6');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E6');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F6');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G6');

        self::assertSheetValueSame('produit 1 - quantité', $sheet, 'H6');
        self::assertSheetValueSame('produit 2 - poids', $sheet, 'I6');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'J6');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B7');
        self::assertSheetValueSame('nom 1', $sheet, 'C7');
        self::assertSheetValueSame('prenom 1', $sheet, 'D7');
        self::assertSheetValueSame(null, $sheet, 'E7');
        self::assertSheetValueSame(null, $sheet, 'F7');
        self::assertSheetValueSame(null, $sheet, 'G7');
        self::assertSheetValueSame(1.5, $sheet, 'H7');
        self::assertSheetValueSame(1, $sheet, 'I7');
        self::assertSheetValueSame('1,89', $sheet, 'J7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B8');
        self::assertSheetValueSame('nom 2', $sheet, 'C8');
        self::assertSheetValueSame('prenom 2', $sheet, 'D8');
        self::assertSheetValueSame('addr2', $sheet, 'E8');
        self::assertSheetValueSame('00002', $sheet, 'F8');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G8');
        self::assertSheetValueSame(1, $sheet, 'H8');
        self::assertSheetValueSame(null, $sheet, 'I8');
        self::assertSheetValueSame('2,89', $sheet, 'J8');
    }

    public function testRenderVATMcSelected(): void
    {
        $p1 = $this->prophesizeFermeProduit(1);
        $p2 = $this->prophesizeFermeProduit(2);

        $mcp1 = $this->prophesizeModeleContratProduit('produit 1', false, $p1);
        $mcp2 = $this->prophesizeModeleContratProduit('produit 2', true, $p2);

        $state = new SearchContratSigneState();
        $state->setMc($this->prophesizeMc(
            $this->prophesizeAmap('Nom de l\'AMAP'),
            'Nom du contrat',
            [$mcp1, $mcp2],
            [
                [
                    ['product' => $mcp1, 'qty' => 1.5],
                    ['product' => $mcp2, 'qty' => 1.0],
                ],
                [
                    ['product' => $mcp1, 'qty' => 1.0],
                    ['product' => $mcp2, 'qty' => 0.0],
                ],
            ]
        ));
        $ferme = $this->prophesizeFerme(true);

        $file = $this->generator->generate($state, $ferme);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('03/01/2000', $sheet, 'A4');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A5');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B7');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C7');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E7');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F7');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G7');

        self::assertSheetValueSame('produit 1 - quantité', $sheet, 'H7');
        self::assertSheetValueSame('produit 2 - poids', $sheet, 'I7');
        self::assertSheetValueSame('TOTAL HT', $sheet, 'J7');
        self::assertSheetValueSame('TVA 0 %', $sheet, 'K7');
        self::assertSheetValueSame('TVA 5,5 %', $sheet, 'L7');
        self::assertSheetValueSame('TVA 10 %', $sheet, 'M7');
        self::assertSheetValueSame('TVA 20 %', $sheet, 'N7');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'O7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B8');
        self::assertSheetValueSame('nom 1', $sheet, 'C8');
        self::assertSheetValueSame('prenom 1', $sheet, 'D8');
        self::assertSheetValueSame(null, $sheet, 'E8');
        self::assertSheetValueSame(null, $sheet, 'F8');
        self::assertSheetValueSame(null, $sheet, 'G8');
        self::assertSheetValueSame(1.5, $sheet, 'H8');
        self::assertSheetValueSame(1, $sheet, 'I8');
        self::assertSheetValueSame('1,01', $sheet, 'J8');
        self::assertSheetValueSame('1,03', $sheet, 'K8');
        self::assertSheetValueSame('1,04', $sheet, 'L8');
        self::assertSheetValueSame('1,05', $sheet, 'M8');
        self::assertSheetValueSame('1,06', $sheet, 'N8');
        self::assertSheetValueSame('1,89', $sheet, 'O8');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A9');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B9');
        self::assertSheetValueSame('nom 2', $sheet, 'C9');
        self::assertSheetValueSame('prenom 2', $sheet, 'D9');
        self::assertSheetValueSame('addr2', $sheet, 'E9');
        self::assertSheetValueSame('00002', $sheet, 'F9');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G9');
        self::assertSheetValueSame(1, $sheet, 'H9');
        self::assertSheetValueSame(null, $sheet, 'I9');
        self::assertSheetValueSame('2,01', $sheet, 'J9');
        self::assertSheetValueSame('2,03', $sheet, 'K9');
        self::assertSheetValueSame('2,04', $sheet, 'L9');
        self::assertSheetValueSame('2,05', $sheet, 'M9');
        self::assertSheetValueSame('2,06', $sheet, 'N9');
        self::assertSheetValueSame('2,89', $sheet, 'O9');
    }

    public function testRenderMultipleMcs(): void
    {
        $p1 = $this->prophesizeFermeProduit(1);
        $p2 = $this->prophesizeFermeProduit(2);

        $mcp1 = $this->prophesizeModeleContratProduit('produit 1', false, $p1);
        $mcp2 = $this->prophesizeModeleContratProduit('produit 2', true, $p2);
        $mcp3 = $this->prophesizeModeleContratProduit('produit 3', false, null);
        $mcp4 = $this->prophesizeModeleContratProduit('produit 3', false, null);
        $mcp5 = $this->prophesizeModeleContratProduit('produit 4', false, null);

        $amap = $this->prophesizeAmap('Nom de l\'AMAP');
        $ferme = $this->prophesizeFerme(false);

        $mc1 = $this->prophesizeMc(
            $amap,
            'Nom du contrat 1',
            [$mcp1, $mcp2, $mcp3],
            [
                [
                    ['product' => $mcp1, 'qty' => 1.5],
                    ['product' => $mcp2, 'qty' => 1.0],
                    ['product' => $mcp3, 'qty' => 1.0],
                ],
                [
                    ['product' => $mcp1, 'qty' => 1.0],
                    ['product' => $mcp2, 'qty' => 0.0],
                    ['product' => $mcp3, 'qty' => 1.0],
                ],
            ]
        );
        $mc2 = $this->prophesizeMc(
            $amap,
            'Nom du contrat 2',
            [$mcp4, $mcp5],
            [
                [
                    ['product' => $mcp4, 'qty' => 1.5],
                    ['product' => $mcp5, 'qty' => 1.5],
                ],
                [
                    ['product' => $mcp4, 'qty' => 2],
                    ['product' => $mcp5, 'qty' => 2],
                ],
            ]
        );

        $this->modeleContratRepo->findValidatedByFermeAmap(
            $ferme,
            $amap
        )->willReturn([$mc1, $mc2]);

        $state = new SearchContratSigneState();
        $state->setAmap($amap);

        $file = $this->generator->generate($state, $ferme);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A4');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A6');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B6');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C6');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E6');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F6');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G6');

        self::assertSheetValueSame('produit 1 - quantité', $sheet, 'H6');
        self::assertSheetValueSame('produit 2 - poids', $sheet, 'I6');
        self::assertSheetValueSame('produit 3 - quantité', $sheet, 'J6');
        self::assertSheetValueSame('produit 4 - quantité', $sheet, 'K6');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'L6');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat 1', $sheet, 'B7');
        self::assertSheetValueSame('nom 1', $sheet, 'C7');
        self::assertSheetValueSame('prenom 1', $sheet, 'D7');
        self::assertSheetValueSame(null, $sheet, 'E7');
        self::assertSheetValueSame(null, $sheet, 'F7');
        self::assertSheetValueSame(null, $sheet, 'G7');
        self::assertSheetValueSame(1.5, $sheet, 'H7');
        self::assertSheetValueSame(1, $sheet, 'I7');
        self::assertSheetValueSame(1, $sheet, 'J7');
        self::assertSheetValueSame(null, $sheet, 'K7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat 1', $sheet, 'B8');
        self::assertSheetValueSame('nom 2', $sheet, 'C8');
        self::assertSheetValueSame('prenom 2', $sheet, 'D8');
        self::assertSheetValueSame('addr2', $sheet, 'E8');
        self::assertSheetValueSame('00002', $sheet, 'F8');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G8');
        self::assertSheetValueSame(1, $sheet, 'H8');
        self::assertSheetValueSame(null, $sheet, 'I8');
        self::assertSheetValueSame(1, $sheet, 'J8');
        self::assertSheetValueSame(null, $sheet, 'K8');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A9');
        self::assertSheetValueSame('Nom du contrat 2', $sheet, 'B9');
        self::assertSheetValueSame('nom 1', $sheet, 'C9');
        self::assertSheetValueSame('prenom 1', $sheet, 'D9');
        self::assertSheetValueSame(null, $sheet, 'E9');
        self::assertSheetValueSame(null, $sheet, 'F9');
        self::assertSheetValueSame(null, $sheet, 'G9');
        self::assertSheetValueSame(null, $sheet, 'H9');
        self::assertSheetValueSame(null, $sheet, 'I9');
        self::assertSheetValueSame(1.5, $sheet, 'J9');
        self::assertSheetValueSame(1.5, $sheet, 'K9');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A10');
        self::assertSheetValueSame('Nom du contrat 2', $sheet, 'B10');
        self::assertSheetValueSame('nom 2', $sheet, 'C10');
        self::assertSheetValueSame('prenom 2', $sheet, 'D10');
        self::assertSheetValueSame('addr2', $sheet, 'E10');
        self::assertSheetValueSame('00002', $sheet, 'F10');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G10');
        self::assertSheetValueSame(null, $sheet, 'H10');
        self::assertSheetValueSame(null, $sheet, 'I10');
        self::assertSheetValueSame(2, $sheet, 'J10');
        self::assertSheetValueSame(2, $sheet, 'K10');
    }

    private function prophesizeMc(Amap $amap, string $mcName, array $mcps, array $contratProducts): ModeleContrat
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getNom()->willReturn($mcName);
        $mc->getProduits()->willReturn(new ArrayCollection($mcps));
        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getAmap()->willReturn($amap);
        $mc->getLivraisonLieu()->willReturn($ll->reveal());

        $mcReveled = $mc->reveal();
        $c1 = $this->prophesizeContract(
            $mcReveled,
            $this->prophesizeAmapien($amap, 'prenom 1', 'nom 1', null, null),
            $contratProducts[0]
        );
        $c2 = $this->prophesizeContract(
            $mcReveled,
            $this->prophesizeAmapien($amap, 'prenom 2', 'nom 2', 'addr2', $this->prophesizeVille(2, 'nom ville 2')),
            $contratProducts[1]
        );
        $this->contractCalculator->totalHT($c1)->willReturn(Money::EUR(101));
        $this->contractCalculator->totalHT($c2)->willReturn(Money::EUR(201));

        $this->contractCalculator->totalWithTva($c1, Argument::is(0.0))->willReturn(Money::EUR(103));
        $this->contractCalculator->totalWithTva($c2, Argument::is(0.0))->willReturn(Money::EUR(203));

        $this->contractCalculator->totalWithTva($c1, 5.5)->willReturn(Money::EUR(104));
        $this->contractCalculator->totalWithTva($c2, 5.5)->willReturn(Money::EUR(204));

        $this->contractCalculator->totalWithTva($c1, 10)->willReturn(Money::EUR(105));
        $this->contractCalculator->totalWithTva($c2, 10)->willReturn(Money::EUR(205));

        $this->contractCalculator->totalWithTva($c1, 20)->willReturn(Money::EUR(106));
        $this->contractCalculator->totalWithTva($c2, 20)->willReturn(Money::EUR(206));

        $this->contractCalculator->totalTTC($c1)->willReturn(Money::EUR(189));
        $this->contractCalculator->totalTTC($c2)->willReturn(Money::EUR(289));
        $mc->getContrats()->willReturn(new ArrayCollection([
            $c1,
            $c2,
        ]));

        return $mcReveled;
    }

    private function prophesizeFermeProduit(int $id): FermeProduit
    {
        $p = $this->prophesize(FermeProduit::class);
        $p->getId()->willReturn($id);

        return $p->reveal();
    }

    private function prophesizeModeleContratProduit(string $nom, bool $hasRegul, ?FermeProduit $fermeProduit)
    {
        $mcp = $this->prophesize(ModeleContratProduit::class);
        $mcp->getNom()->willReturn($nom);
        $mcp->getRegulPds()->willReturn($hasRegul);
        $mcp->getFermeProduit()->willReturn($fermeProduit);

        return $mcp->reveal();
    }

    private function prophesizeAmapien(
        Amap $amap,
        string $prenom,
        string $nom,
        ?string $addr,
        ?Ville $ville
    ): Amapien {
        $name = new Name();
        $name->setFirstName($prenom);
        $name->setLastName($nom);

        $address = new Address();
        $address->setAdress($addr);

        $user = $this->prophesize(User::class);
        $user->getName()->willReturn($name);
        $user->getAddress()->willReturn($address);
        $user->getVille()->willReturn($ville);

        $amapien = $this->prophesize(Amapien::class);
        $amapien->getAmap()->willReturn($amap);
        $amapien->getUser()->willReturn($user->reveal());

        return $amapien->reveal();
    }

    private function prophesizeVille(int $cp, string $nom): Ville
    {
        return new Ville($cp, $nom, $this->prophesize(Departement::class)->reveal());
    }

    private function prophesizeAmap(string $nom): Amap
    {
        $amap = $this->prophesize(Amap::class);
        $amap->getNom()->willReturn($nom);

        return $amap->reveal();
    }

    private function prophesizeContract(ModeleContrat $mc, Amapien $amapien, array $totalByProducts)
    {
        $contrat = $this->prophesize(Contrat::class);
        $contrat->getModeleContrat()->willReturn($mc);
        $contrat->getAmapien()->willReturn($amapien);

        foreach ($totalByProducts as $totalByProduct) {
            $contrat->totalQtyByProduct($totalByProduct['product'])->willReturn($totalByProduct['qty']);
        }

        return $contrat->reveal();
    }
}
