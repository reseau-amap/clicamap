<?php

declare(strict_types=1);

namespace Test\Services\Exporters;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\Constraint\IsIdentical;

trait SheetValueTestTrait
{
    public static function assertSheetValueSame($expected, Worksheet $worksheet, string $position): void
    {
        static::assertThat(
            $expected,
            new IsIdentical($worksheet->getCell($position)->getValue())
        );
    }
}
