<?php

declare(strict_types=1);

namespace Test\Services\Exporters;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Money\Money;
use Prophecy\Argument;
use PsrLib\DTO\SearchContratSigneState;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\AmapLivraisonLieu;
use PsrLib\ORM\Entity\Contrat;
use PsrLib\ORM\Entity\ContratDatesReglement;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\Embeddable\Address;
use PsrLib\ORM\Entity\Embeddable\Name;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDatesReglement;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\Ville;
use PsrLib\Services\Exporters\ExcelGeneratorExportContratAccountDate;
use PsrLib\Services\TvaLabel;

/**
 * @internal
 *
 * @coversNothing
 */
class ExcelGeneratorExportContratAccountDateTest extends ExcelGeneratorExportContratAccountAbstract
{
    use \Prophecy\PhpUnit\ProphecyTrait;
    use SheetValueTestTrait;

    /**
     * @var ExcelGeneratorExportContratAccountDate
     */
    protected $generator;

    public function setUp(): void
    {
        parent::setUp();
        $this->generator = new ExcelGeneratorExportContratAccountDate(
            $this->contractCalculator->reveal(),
            new TvaLabel(),
            $this->modeleContratRepo->reveal()
        );
    }

    public function testRenderNoVATMcSelected(): void
    {
        $state = new SearchContratSigneState();
        $state->setMc(
            $this->prophesizeMc(
                $this->prophesizeAmap('Nom de l\'AMAP'),
                'Nom du contrat',
                3,
                [
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-01')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-02')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-03')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-04')),
                ],
                [
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                        ['date' => new Carbon('2000-01-02'), 'amount' => Money::EUR(2050)],
                        ['date' => new Carbon('2000-01-03'), 'amount' => Money::EUR(3012)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                        ['date' => new Carbon('2000-01-02'), 'amount' => Money::EUR(2050)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-04'), 'amount' => Money::EUR(4000)],
                    ],
                ]
            )
        );
        $ferme = $this->prophesizeFerme(false);

        $file = $this->generator->generate($state, $ferme);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A4');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A6');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B6');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C6');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E6');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F6');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G6');

        self::assertSheetValueSame('Date de paiement 1', $sheet, 'H6');
        self::assertSheetValueSame('Montant du paiement 1', $sheet, 'I6');
        self::assertSheetValueSame('Date de paiement 2', $sheet, 'J6');
        self::assertSheetValueSame('Montant du paiement 2', $sheet, 'K6');
        self::assertSheetValueSame('Date de paiement 3', $sheet, 'L6');
        self::assertSheetValueSame('Montant du paiement 3', $sheet, 'M6');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'N6');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B7');
        self::assertSheetValueSame('nom 1', $sheet, 'C7');
        self::assertSheetValueSame('prenom 1', $sheet, 'D7');
        self::assertSheetValueSame(null, $sheet, 'E7');
        self::assertSheetValueSame(null, $sheet, 'F7');
        self::assertSheetValueSame(null, $sheet, 'G7');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H7');
        self::assertSheetValueSame('10,00', $sheet, 'I7');
        self::assertSheetValueSame('2000-01-02', $sheet, 'J7');
        self::assertSheetValueSame('20,50', $sheet, 'K7');
        self::assertSheetValueSame('2000-01-03', $sheet, 'L7');
        self::assertSheetValueSame('30,12', $sheet, 'M7');
        self::assertSheetValueSame('1,89', $sheet, 'N7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B8');
        self::assertSheetValueSame('nom 2', $sheet, 'C8');
        self::assertSheetValueSame('prenom 2', $sheet, 'D8');
        self::assertSheetValueSame('addr2', $sheet, 'E8');
        self::assertSheetValueSame('00002', $sheet, 'F8');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G8');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H8');
        self::assertSheetValueSame('10,00', $sheet, 'I8');
        self::assertSheetValueSame('2000-01-02', $sheet, 'J8');
        self::assertSheetValueSame('20,50', $sheet, 'K8');
        self::assertSheetValueSame(null, $sheet, 'L8');
        self::assertSheetValueSame(null, $sheet, 'M8');
        self::assertSheetValueSame('2,89', $sheet, 'N8');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A9');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B9');
        self::assertSheetValueSame('nom 3', $sheet, 'C9');
        self::assertSheetValueSame('prenom 3', $sheet, 'D9');
        self::assertSheetValueSame('addr3', $sheet, 'E9');
        self::assertSheetValueSame('00003', $sheet, 'F9');
        self::assertSheetValueSame('nom ville 3', $sheet, 'G9');
        self::assertSheetValueSame('2000-01-04', $sheet, 'H9');
        self::assertSheetValueSame('40,00', $sheet, 'I9');
        self::assertSheetValueSame(null, $sheet, 'J9');
        self::assertSheetValueSame(null, $sheet, 'K9');
        self::assertSheetValueSame(null, $sheet, 'L9');
        self::assertSheetValueSame(null, $sheet, 'M9');
        self::assertSheetValueSame('3,89', $sheet, 'N9');
    }

    public function testRenderVATMcSelected(): void
    {
        $state = new SearchContratSigneState();
        $state->setMc(
            $this->prophesizeMc(
                $this->prophesizeAmap('Nom de l\'AMAP'),
                'Nom du contrat',
                3,
                [
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-01')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-02')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-03')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-04')),
                ],
                [
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                        ['date' => new Carbon('2000-01-02'), 'amount' => Money::EUR(2050)],
                        ['date' => new Carbon('2000-01-03'), 'amount' => Money::EUR(3012)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                        ['date' => new Carbon('2000-01-02'), 'amount' => Money::EUR(2050)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-04'), 'amount' => Money::EUR(4000)],
                    ],
                ]
            )
        );
        $ferme = $this->prophesizeFerme(true);

        $file = $this->generator->generate($state, $ferme);

        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('03/01/2000', $sheet, 'A4');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A5');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B7');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C7');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E7');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F7');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G7');

        self::assertSheetValueSame('Date de paiement 1', $sheet, 'H7');
        self::assertSheetValueSame('Montant du paiement 1', $sheet, 'I7');
        self::assertSheetValueSame('Date de paiement 2', $sheet, 'J7');
        self::assertSheetValueSame('Montant du paiement 2', $sheet, 'K7');
        self::assertSheetValueSame('Date de paiement 3', $sheet, 'L7');
        self::assertSheetValueSame('Montant du paiement 3', $sheet, 'M7');
        self::assertSheetValueSame('TOTAL HT', $sheet, 'N7');
        self::assertSheetValueSame('TVA 0 %', $sheet, 'O7');
        self::assertSheetValueSame('TVA 5,5 %', $sheet, 'P7');
        self::assertSheetValueSame('TVA 10 %', $sheet, 'Q7');
        self::assertSheetValueSame('TVA 20 %', $sheet, 'R7');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'S7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B8');
        self::assertSheetValueSame('nom 1', $sheet, 'C8');
        self::assertSheetValueSame('prenom 1', $sheet, 'D8');
        self::assertSheetValueSame(null, $sheet, 'E8');
        self::assertSheetValueSame(null, $sheet, 'F8');
        self::assertSheetValueSame(null, $sheet, 'G8');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H8');
        self::assertSheetValueSame('10,00', $sheet, 'I8');
        self::assertSheetValueSame('2000-01-02', $sheet, 'J8');
        self::assertSheetValueSame('20,50', $sheet, 'K8');
        self::assertSheetValueSame('2000-01-03', $sheet, 'L8');
        self::assertSheetValueSame('30,12', $sheet, 'M8');
        self::assertSheetValueSame('1,01', $sheet, 'N8');
        self::assertSheetValueSame('1,03', $sheet, 'O8');
        self::assertSheetValueSame('1,04', $sheet, 'P8');
        self::assertSheetValueSame('1,05', $sheet, 'Q8');
        self::assertSheetValueSame('1,06', $sheet, 'R8');
        self::assertSheetValueSame('1,89', $sheet, 'S8');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A9');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B9');
        self::assertSheetValueSame('nom 2', $sheet, 'C9');
        self::assertSheetValueSame('prenom 2', $sheet, 'D9');
        self::assertSheetValueSame('addr2', $sheet, 'E9');
        self::assertSheetValueSame('00002', $sheet, 'F9');
        self::assertSheetValueSame('nom ville 2', $sheet, 'G9');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H9');
        self::assertSheetValueSame('10,00', $sheet, 'I9');
        self::assertSheetValueSame('2000-01-02', $sheet, 'J9');
        self::assertSheetValueSame('20,50', $sheet, 'K9');
        self::assertSheetValueSame(null, $sheet, 'L9');
        self::assertSheetValueSame(null, $sheet, 'M9');
        self::assertSheetValueSame('2,01', $sheet, 'N9');
        self::assertSheetValueSame('2,03', $sheet, 'O9');
        self::assertSheetValueSame('2,04', $sheet, 'P9');
        self::assertSheetValueSame('2,05', $sheet, 'Q9');
        self::assertSheetValueSame('2,06', $sheet, 'R9');
        self::assertSheetValueSame('2,89', $sheet, 'S9');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A10');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B10');
        self::assertSheetValueSame('nom 3', $sheet, 'C10');
        self::assertSheetValueSame('prenom 3', $sheet, 'D10');
        self::assertSheetValueSame('addr3', $sheet, 'E10');
        self::assertSheetValueSame('00003', $sheet, 'F10');
        self::assertSheetValueSame('nom ville 3', $sheet, 'G10');
        self::assertSheetValueSame('2000-01-04', $sheet, 'H10');
        self::assertSheetValueSame('40,00', $sheet, 'I10');
        self::assertSheetValueSame(null, $sheet, 'J10');
        self::assertSheetValueSame(null, $sheet, 'K10');
        self::assertSheetValueSame(null, $sheet, 'L10');
        self::assertSheetValueSame(null, $sheet, 'M10');
        self::assertSheetValueSame('3,01', $sheet, 'N10');
        self::assertSheetValueSame('3,03', $sheet, 'O10');
        self::assertSheetValueSame('3,04', $sheet, 'P10');
        self::assertSheetValueSame('3,05', $sheet, 'Q10');
        self::assertSheetValueSame('3,06', $sheet, 'R10');
        self::assertSheetValueSame('3,89', $sheet, 'S10');
    }

    public function testRenderMultipleMcs(): void
    {
        $amap = $this->prophesizeAmap('Nom de l\'AMAP');
        $ferme = $this->prophesizeFerme(false);

        $this->modeleContratRepo->findValidatedByFermeAmap(
            $ferme,
            $amap
        )->willReturn([
            $this->prophesizeMc(
                $amap,
                'Nom du contrat 1',
                1,
                [
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-01')),
                ],
                [
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(1000)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-01'), 'amount' => Money::EUR(4000)],
                    ],
                ]
            ),
            $this->prophesizeMc(
                $amap,
                'Nom du contrat 2',
                2,
                [
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-03')),
                    $this->prophesizeMcDateReglement(new Carbon('2000-01-04')),
                ],
                [
                    [
                        ['date' => new Carbon('2000-01-03'), 'amount' => Money::EUR(500)],
                        ['date' => new Carbon('2000-01-04'), 'amount' => Money::EUR(500)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-03'), 'amount' => Money::EUR(1000)],
                    ],
                    [
                        ['date' => new Carbon('2000-01-04'), 'amount' => Money::EUR(4000)],
                    ],
                ]
            ),
        ]);
        $state = new SearchContratSigneState();
        $state->setAmap($amap);

        $file = $this->generator->generate($state, $ferme);
        $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file)->getActiveSheet();
        self::assertSheetValueSame('ferme nom', $sheet, 'A1');
        self::assertSheetValueSame('addr 00001 ville', $sheet, 'A2');
        self::assertSheetValueSame('siret', $sheet, 'A3');
        self::assertSheetValueSame('Extrait le 02/01/2000 03:04:05', $sheet, 'A4');

        self::assertSheetValueSame('Dans le cadre de l’AMAP', $sheet, 'A6');
        self::assertSheetValueSame('Nom du contrat', $sheet, 'B6');
        self::assertSheetValueSame('Nom de l’amapien', $sheet, 'C6');
        self::assertSheetValueSame('Adresse de l’amapien', $sheet, 'E6');
        self::assertSheetValueSame('Code postal de l’amapien', $sheet, 'F6');
        self::assertSheetValueSame('Ville de l’amapien', $sheet, 'G6');

        self::assertSheetValueSame('Date de paiement 1', $sheet, 'H6');
        self::assertSheetValueSame('Montant du paiement 1', $sheet, 'I6');
        self::assertSheetValueSame('Date de paiement 2', $sheet, 'J6');
        self::assertSheetValueSame('Montant du paiement 2', $sheet, 'K6');
        self::assertSheetValueSame('TOTAL TTC', $sheet, 'l6');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A7');
        self::assertSheetValueSame('Nom du contrat 1', $sheet, 'B7');
        self::assertSheetValueSame('nom 1', $sheet, 'C7');
        self::assertSheetValueSame('prenom 1', $sheet, 'D7');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H7');
        self::assertSheetValueSame('10,00', $sheet, 'I7');
        self::assertSheetValueSame(null, $sheet, 'J7');
        self::assertSheetValueSame(null, $sheet, 'K7');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A8');
        self::assertSheetValueSame('Nom du contrat 1', $sheet, 'B8');
        self::assertSheetValueSame('nom 2', $sheet, 'C8');
        self::assertSheetValueSame('prenom 2', $sheet, 'D8');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H8');
        self::assertSheetValueSame('10,00', $sheet, 'I8');
        self::assertSheetValueSame(null, $sheet, 'J8');
        self::assertSheetValueSame(null, $sheet, 'K8');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A9');
        self::assertSheetValueSame('Nom du contrat 1', $sheet, 'B9');
        self::assertSheetValueSame('nom 3', $sheet, 'C9');
        self::assertSheetValueSame('prenom 3', $sheet, 'D9');
        self::assertSheetValueSame('2000-01-01', $sheet, 'H9');
        self::assertSheetValueSame('40,00', $sheet, 'I9');
        self::assertSheetValueSame(null, $sheet, 'J9');
        self::assertSheetValueSame(null, $sheet, 'K9');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A10');
        self::assertSheetValueSame('Nom du contrat 2', $sheet, 'B10');
        self::assertSheetValueSame('nom 1', $sheet, 'C10');
        self::assertSheetValueSame('prenom 1', $sheet, 'D10');
        self::assertSheetValueSame('2000-01-03', $sheet, 'H10');
        self::assertSheetValueSame('5,00', $sheet, 'I10');
        self::assertSheetValueSame('2000-01-04', $sheet, 'J10');
        self::assertSheetValueSame('5,00', $sheet, 'K10');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A11');
        self::assertSheetValueSame('Nom du contrat 2', $sheet, 'B11');
        self::assertSheetValueSame('nom 2', $sheet, 'C11');
        self::assertSheetValueSame('prenom 2', $sheet, 'D11');
        self::assertSheetValueSame('2000-01-03', $sheet, 'H11');
        self::assertSheetValueSame('10,00', $sheet, 'I11');
        self::assertSheetValueSame(null, $sheet, 'J11');
        self::assertSheetValueSame(null, $sheet, 'K11');

        self::assertSheetValueSame('Nom de l\'AMAP', $sheet, 'A12');
        self::assertSheetValueSame('Nom du contrat 2', $sheet, 'B12');
        self::assertSheetValueSame('nom 3', $sheet, 'C12');
        self::assertSheetValueSame('prenom 3', $sheet, 'D12');
        self::assertSheetValueSame('2000-01-04', $sheet, 'H12');
        self::assertSheetValueSame('40,00', $sheet, 'I12');
        self::assertSheetValueSame(null, $sheet, 'J12');
        self::assertSheetValueSame(null, $sheet, 'K12');
    }

    private function prophesizeMc(Amap $amap, string $mcName, int $nbReglementsMax, array $mcDates, array $contractsDates): ModeleContrat
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getDateReglements()->willReturn(new ArrayCollection($mcDates));
        $mc->getReglementNbMax()->willReturn($nbReglementsMax);
        $mc->getNom()->willReturn($mcName);

        $ll = $this->prophesize(AmapLivraisonLieu::class);
        $ll->getAmap()->willReturn($amap);
        $mc->getLivraisonLieu()->willReturn($ll->reveal());

        $mcReveled = $mc->reveal();
        $c1 = $this->prophesizeContract(
            $mcReveled,
            $this->prophesizeAmapien($amap, 'prenom 1', 'nom 1', null, null),
            $contractsDates[0]
        );
        $c2 = $this->prophesizeContract(
            $mcReveled,
            $this->prophesizeAmapien($amap, 'prenom 2', 'nom 2', 'addr2', $this->prophesizeVille(2, 'nom ville 2')),
            $contractsDates[1]
        );
        $c3 = $this->prophesizeContract(
            $mcReveled,
            $this->prophesizeAmapien($amap, 'prenom 3', 'nom 3', 'addr3', $this->prophesizeVille(3, 'nom ville 3')),
            $contractsDates[2]
        );
        $this->contractCalculator->totalHT($c1)->willReturn(Money::EUR(101));
        $this->contractCalculator->totalHT($c2)->willReturn(Money::EUR(201));
        $this->contractCalculator->totalHT($c3)->willReturn(Money::EUR(301));

        $this->contractCalculator->totalWithTva($c1, Argument::is(0.0))->willReturn(Money::EUR(103));
        $this->contractCalculator->totalWithTva($c2, Argument::is(0.0))->willReturn(Money::EUR(203));
        $this->contractCalculator->totalWithTva($c3, Argument::is(0.0))->willReturn(Money::EUR(303));

        $this->contractCalculator->totalWithTva($c1, 5.5)->willReturn(Money::EUR(104));
        $this->contractCalculator->totalWithTva($c2, 5.5)->willReturn(Money::EUR(204));
        $this->contractCalculator->totalWithTva($c3, 5.5)->willReturn(Money::EUR(304));

        $this->contractCalculator->totalWithTva($c1, 10)->willReturn(Money::EUR(105));
        $this->contractCalculator->totalWithTva($c2, 10)->willReturn(Money::EUR(205));
        $this->contractCalculator->totalWithTva($c3, 10)->willReturn(Money::EUR(305));

        $this->contractCalculator->totalWithTva($c1, 20)->willReturn(Money::EUR(106));
        $this->contractCalculator->totalWithTva($c2, 20)->willReturn(Money::EUR(206));
        $this->contractCalculator->totalWithTva($c3, 20)->willReturn(Money::EUR(306));

        $this->contractCalculator->totalTTC($c1)->willReturn(Money::EUR(189));
        $this->contractCalculator->totalTTC($c2)->willReturn(Money::EUR(289));
        $this->contractCalculator->totalTTC($c3)->willReturn(Money::EUR(389));
        $mc->getContrats()->willReturn(new ArrayCollection([
            $c1,
            $c2,
            $c3,
        ]));

        return $mcReveled;
    }

    private function prophesizeMcDateReglement(Carbon $date): ModeleContratDatesReglement
    {
        $mcd = $this->prophesize(ModeleContratDatesReglement::class);
        $mcd->getDate()->willReturn($date);

        return $mcd->reveal();
    }

    private function prophesizeContractDateReglement(ModeleContratDatesReglement $date, Money $amount): ContratDatesReglement
    {
        $cd = $this->prophesize(ContratDatesReglement::class);
        $cd->getModeleContratDatesReglement()->willReturn($date);
        $cd->getMontant()->willReturn($amount);

        return $cd->reveal();
    }

    private function prophesizeAmapien(
        Amap $amap,
        string $prenom,
        string $nom,
        ?string $addr,
        ?Ville $ville
    ): Amapien {
        $name = new Name();
        $name->setFirstName($prenom);
        $name->setLastName($nom);

        $address = new Address();
        $address->setAdress($addr);

        $user = $this->prophesize(User::class);
        $user->getName()->willReturn($name);
        $user->getAddress()->willReturn($address);
        $user->getVille()->willReturn($ville);

        $amapien = $this->prophesize(Amapien::class);
        $amapien->getAmap()->willReturn($amap);
        $amapien->getUser()->willReturn($user->reveal());

        return $amapien->reveal();
    }

    private function prophesizeVille(int $cp, string $nom): Ville
    {
        return new Ville($cp, $nom, $this->prophesize(Departement::class)->reveal());
    }

    private function prophesizeAmap(string $nom): Amap
    {
        $amap = $this->prophesize(Amap::class);
        $amap->getNom()->willReturn($nom);

        return $amap->reveal();
    }

    private function prophesizeContract(ModeleContrat $mc, Amapien $amapien, array $datesReglement)
    {
        $contrat = $this->prophesize(Contrat::class);
        $contrat->getModeleContrat()->willReturn($mc);
        $contrat->getAmapien()->willReturn($amapien);

        $cds = [];
        foreach ($datesReglement as $date) {
            foreach ($mc->getDateReglements() as $mcDateReglement) {
                if ($date['date']->eq($mcDateReglement->getDate())) {
                    $cds[] = $this->prophesizeContractDateReglement($mcDateReglement, $date['amount']);
                }
            }
        }
        $contrat->getDatesReglements()->willReturn(new ArrayCollection($cds));

        return $contrat->reveal();
    }
}
