<?php

declare(strict_types=1);

namespace Test\Services;

use PsrLib\Services\StringUtils;

/**
 * @internal
 *
 * @coversNothing
 */
class StringUtilsTest extends \PHPUnit\Framework\TestCase
{
    private StringUtils $stringUtils;

    protected function setUp(): void
    {
        $this->stringUtils = new StringUtils();
    }

    /**
     * @dataProvider slugToFileNameProvider
     */
    public function testSlugToFileName(string $in, string $expected): void
    {
        self::assertSame($expected, $this->stringUtils->slugToFileName($in));
    }

    public function slugToFileNameProvider()
    {
        return [
            ['', '.'],
            ['a', 'a.'],
            ['a.pdf', 'a.pdf'],
            ['A.pdf', 'a.pdf'],
            ['é.pdf', 'e.pdf'],
            ['aa bb.pdf', 'aa bb.pdf'],
            ['aa bb.pdf', 'aa bb.pdf'],
            ['aa bb.pdf', 'aa bb.pdf'],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.pdf', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.pdf'],
        ];
    }
}
