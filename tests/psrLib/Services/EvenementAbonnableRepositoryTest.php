<?php

declare(strict_types=1);

namespace Test\Services;

use PsrLib\DTO\EvenemntAbonnableDTO;
use PsrLib\DTO\SearchDatatableColumn;
use PsrLib\DTO\SearchDatatableOrder;
use PsrLib\DTO\SearchEvenementAbonnable;
use PsrLib\Enum\EvenementType;
use PsrLib\ORM\Entity\Departement;
use PsrLib\ORM\Entity\EvenementAbonnementAmap;
use PsrLib\ORM\Entity\Region;
use PsrLib\Services\EvenementAbonnableRepository;
use Test\EntityHelperTrait;
use Test\ORM\Repository\RepositoryTestAbstract;

/**
 * @internal
 *
 * @coversNothing
 */
class EvenementAbonnableRepositoryTest extends RepositoryTestAbstract
{
    use EntityHelperTrait;
    public EvenementAbonnableRepository $evenementAbonnableRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->evenementAbonnableRepository = self::$container->get(EvenementAbonnableRepository::class);
    }

    public function testDefault(): void
    {
        $search = new SearchEvenementAbonnable();
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '01',
                'nom' => 'AIN',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '02',
                'nom' => 'AISNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '03',
                'nom' => 'ALLIER',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '04',
                'nom' => 'ALPES-DE-HAUTE-PROVENCE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '06',
                'nom' => 'ALPES-MARITIMES',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '1',
                'nom' => 'amap test',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '2',
                'nom' => 'amap test 2',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '07',
                'nom' => 'ARDÈCHE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '08',
                'nom' => 'ARDENNES',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '09',
                'nom' => 'ARIÈGE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '10',
                'nom' => 'AUBE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '11',
                'nom' => 'AUDE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '84',
                'nom' => 'AUVERGNE-RHÔNE-ALPES',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '12',
                'nom' => 'AVEYRON',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '67',
                'nom' => 'BAS-RHIN',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '13',
                'nom' => 'BOUCHES-DU-RHÔNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '27',
                'nom' => 'BOURGOGNE-FRANCHE-COMTÉ',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '53',
                'nom' => 'BRETAGNE',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '14',
                'nom' => 'CALVADOS',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '15',
                'nom' => 'CANTAL',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '24',
                'nom' => 'CENTRE-VAL DE LOIRE',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '16',
                'nom' => 'CHARENTE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '17',
                'nom' => 'CHARENTE-MARITIME',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '18',
                'nom' => 'CHER',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '19',
                'nom' => 'CORRÈZE',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(122, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testMaxResult(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setMaxResults('2');
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '01',
                'nom' => 'AIN',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '02',
                'nom' => 'AISNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(122, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testOffset(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setMaxResults('2');
        $search->setOffset('1');
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '02',
                'nom' => 'AISNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '03',
                'nom' => 'ALLIER',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(122, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testOrderInvalidColumn(): void
    {
        $searchOrder = new SearchDatatableOrder();
        $searchOrder->setDir('asc');
        $searchOrder->setColumn('0');
        $searchColumn = new SearchDatatableColumn();
        $searchColumn->setData('invalid');

        $search = new SearchEvenementAbonnable();
        $search->setColumns([$searchColumn]);
        $search->setOrder([$searchOrder]);

        $this->expectException(\InvalidArgumentException::class);
        $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);
    }

    public function testFilterRegion(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setRegion($this->em->getRepository(Region::class)->findOneBy(['nom' => 'AUVERGNE-RHÔNE-ALPES']));
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '01',
                'nom' => 'AIN',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '03',
                'nom' => 'ALLIER',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '1',
                'nom' => 'amap test',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '2',
                'nom' => 'amap test 2',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '07',
                'nom' => 'ARDÈCHE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '84',
                'nom' => 'AUVERGNE-RHÔNE-ALPES',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '15',
                'nom' => 'CANTAL',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '26',
                'nom' => 'DRÔME',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '1',
                'nom' => 'ferme',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '2',
                'nom' => 'Ferme 2',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '43',
                'nom' => 'HAUTE-LOIRE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '74',
                'nom' => 'HAUTE-SAVOIE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '38',
                'nom' => 'ISÈRE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '42',
                'nom' => 'LOIRE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '69M',
                'nom' => 'MÉTROPOLE DE LYON',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '63',
                'nom' => 'PUY-DE-DÔME',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '69',
                'nom' => 'RHÔNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
            [
                'id' => '73',
                'nom' => 'SAVOIE',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(18, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testFilterDepartement(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setDepartement($this->em->getRepository(Departement::class)->findOneBy(['nom' => 'RHÔNE']));
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '1',
                'nom' => 'amap test',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '2',
                'nom' => 'amap test 2',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '1',
                'nom' => 'ferme',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '2',
                'nom' => 'Ferme 2',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '69',
                'nom' => 'RHÔNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(5, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testFilterProfile(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setType(EvenementType::FERME);
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '1',
                'nom' => 'ferme',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '2',
                'nom' => 'Ferme 2',
                'type' => EvenementType::FERME,
            ],
        ], $res);
        self::assertSame(2, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testFilterKeyword(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setKeyword('ferme');
        $res = $this->evenementAbonnableRepository->search($this->getUserByUsername('empty'), $search);

        self::assertResultSame([
            [
                'id' => '1',
                'nom' => 'ferme',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '2',
                'nom' => 'Ferme 2',
                'type' => EvenementType::FERME,
            ],
        ], $res);
        self::assertSame(2, $this->evenementAbonnableRepository->searchCountResults($this->getUserByUsername('empty'), $search));
    }

    public function testFilterSubscriptionOnly(): void
    {
        $search = new SearchEvenementAbonnable();
        $search->setSubscriptionOnly(true);

        $paysan = $this->getUserByUsername('paysan');
        $evt = new EvenementAbonnementAmap($paysan);
        $evt->setRelated($this->getAmapByName('amap test 2'));
        $this->em->persist($evt);
        $this->em->flush();

        $res = $this->evenementAbonnableRepository->search($paysan, $search);

        self::assertResultSame([
            [
                'id' => '1',
                'nom' => 'amap test',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '2',
                'nom' => 'amap test 2',
                'type' => EvenementType::AMAP,
            ],
            [
                'id' => '84',
                'nom' => 'AUVERGNE-RHÔNE-ALPES',
                'type' => EvenementType::REGION,
            ],
            [
                'id' => '1',
                'nom' => 'ferme',
                'type' => EvenementType::FERME,
            ],
            [
                'id' => '69',
                'nom' => 'RHÔNE',
                'type' => EvenementType::DEPARTEMENT,
            ],
        ], $res);
        self::assertSame(5, $this->evenementAbonnableRepository->searchCountResults($paysan, $search));
    }

    /**
     * @param EvenemntAbonnableDTO[] $res
     */
    private function assertResultSame(array $expected, array $res): void
    {
        $resArray = array_map(fn (EvenemntAbonnableDTO $item) => [
            'id' => $item->getId(),
            'nom' => $item->getNom(),
            'type' => $item->getType(),
        ], $res);

        self::assertSame($expected, $resArray);
    }
}
