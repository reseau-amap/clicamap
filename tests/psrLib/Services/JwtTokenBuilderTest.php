<?php

declare(strict_types=1);

namespace Test\Services;

use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Validator;
use PHPUnit\Framework\TestCase;
use PsrLib\Services\JwtToken;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 *
 * @coversNothing
 */
class JwtTokenBuilderTest extends TestCase
{
    private const KEY_PATH_DST = __DIR__.'/../../../application/writable/keys/';
    private const KEY_PATH_SRC = __DIR__.'/../../data/';

    public function setUp(): void
    {
        $fs = new Filesystem();
        $fs->mkdir(self::KEY_PATH_DST);
        $fs->copy(self::KEY_PATH_SRC.'jwtCdp.key', self::KEY_PATH_DST.'jwtCdp.key');
        $fs->copy(self::KEY_PATH_SRC.'jwtCdp.pub', self::KEY_PATH_DST.'jwtCdp.pub');
    }

    public function testGenerateToken(): void
    {
        $builder = new JwtToken();
        $token = $builder->buildToken('subject', ['key1' => 'val1', 'key2' => 'val2']);

        self::assertSame('https://www.clicamap.org', $token->claims()->get('iss'));
        self::assertSame('val1', $token->claims()->get('key1'));
        self::assertSame('val2', $token->claims()->get('key2'));
        self::assertSame('subject', $token->claims()->get('sub'));

        $parsedToken = (new Parser(new JoseEncoder()))->parse($token->toString());
        self::assertTrue(
            (new Validator())->validate(
                $parsedToken,
                new SignedWith(
                    new Sha256(),
                    InMemory::file(
                        __DIR__.'/../../../application/writable/keys/jwtCdp.pub'
                    )
                )
            )
        );
    }
}
