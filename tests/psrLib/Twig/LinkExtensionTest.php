<?php

declare(strict_types=1);

namespace Test\Twig;

use PHPUnit\Framework\TestCase;
use PsrLib\Twig\LinkExtension;

/**
 * @internal
 *
 * @coversNothing
 */
class LinkExtensionTest extends TestCase
{
    private LinkExtension $linkExtension;

    public function setUp(): void
    {
        $this->linkExtension = new LinkExtension();
    }

    /**
     * @dataProvider stringCaseProvider
     */
    public function testString(?string $input, string $expected): void
    {
        $this->assertSame($expected, $this->linkExtension->linkOrText($input));
    }

    public function stringCaseProvider()
    {
        return [
            'null' => [null, ''],
            'empty' => ['', ''],
            'string' => ['abcd', 'abcd'],
            'invalid url' => ['http:/aaaa', 'http:/aaaa'],
            'invalid protocol' => ['ftp://aaaa', 'ftp://aaaa'],
        ];
    }

    /**
     * @dataProvider urlCaseProvider
     */
    public function testUrl(?string $input, string $expected): void
    {
        $this->assertSame($expected, $this->linkExtension->linkOrText($input));
    }

    public function urlCaseProvider()
    {
        return [
            'http' => ['http://clicamap.org', '<a href="http://clicamap.org" target="_blank">http://clicamap.org</a>'],
            'https' => ['https://clicamap.org', '<a href="https://clicamap.org" target="_blank">https://clicamap.org</a>'],
        ];
    }
}
