<?php

declare(strict_types=1);

namespace Test\Twig;

use PHPUnit\Framework\TestCase;
use PsrLib\Twig\StringExtension;

/**
 * @internal
 *
 * @coversNothing
 */
class StringExtensionTest extends TestCase
{
    private \PsrLib\Twig\StringExtension $extension;

    public function setUp(): void
    {
        $this->extension = new StringExtension();
    }

    public function testUcfirst(): void
    {
        $res = $this->extension->ucfirst('test string');

        self::assertSame('Test string', $res);
    }

    public function testUcfirstNull(): void
    {
        $res = $this->extension->ucfirst(null);

        self::assertSame('', $res);
    }

    public function testEllipsisShort(): void
    {
        $res = $this->extension->ellipis('Test String');

        self::assertSame('Test String', $res);
    }

    public function testEllipsisLong(): void
    {
        $res = $this->extension->ellipis('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam gravida mattis mi, fringilla aliquam lorem porta id');

        self::assertSame('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam grav...', $res);
    }
}
