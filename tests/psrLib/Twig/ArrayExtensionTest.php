<?php

declare(strict_types=1);

namespace Test\Twig;

use PHPUnit\Framework\TestCase;
use PsrLib\Twig\ArrayExtension;

/**
 * @internal
 *
 * @coversNothing
 */
class ArrayExtensionTest extends TestCase
{
    private \PsrLib\Twig\ArrayExtension $extension;

    public function setUp(): void
    {
        $this->extension = new ArrayExtension();
    }

    public function testArrayUnique(): void
    {
        $res = $this->extension->arrayUnique(['a', 'b', 'a']);

        self::assertSame(['a', 'b'], $res);
    }
}
