<?php

declare(strict_types=1);

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Test\Serializer;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\ModeleContratDeplacement;
use PsrLib\ORM\Entity\ModeleContrat;
use PsrLib\ORM\Entity\ModeleContratDate;
use PsrLib\Serializer\ModeleContratDeplacementNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ModeleContratDeplacementDenormalizerTest extends TestCase
{
    use ProphecyTrait;

    private \PsrLib\Serializer\ModeleContratDeplacementNormalizer $denormalizer;

    private \Prophecy\Prophecy\ObjectProphecy $em;

    private \Prophecy\Prophecy\ObjectProphecy $denormalizerProphecy;

    private \Prophecy\Prophecy\ObjectProphecy $normalizerProphecy;

    public function setUp(): void
    {
        $this->em = $this->prophesize(EntityManagerInterface::class);
        $this->denormalizer = new ModeleContratDeplacementNormalizer($this->em->reveal());
        $this->denormalizerProphecy = $this->prophesize(DenormalizerInterface::class);
        $this->normalizerProphecy = $this->prophesize(NormalizerInterface::class);
        $this->denormalizer->setDenormalizer($this->denormalizerProphecy->reveal());
        $this->denormalizer->setNormalizer($this->normalizerProphecy->reveal());
    }

    public function testNormalizeDstNull(): void
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getId()->willReturn(1);
        $mcDate = $this->prophesize(ModeleContratDate::class);
        $mcDate->getId()->willReturn(2);

        $mcd = $this->prophesize(ModeleContratDeplacement::class);
        $mcd->getSrc()->willReturn($mcDate->reveal());
        $mcd->getMc()->willReturn($mc->reveal());
        $mcd->getDst()->willReturn(null);

        $res = $this->denormalizer->normalize($mcd->reveal(), 'json');
        self::assertSame([
            'mcId' => 1,
            'srcId' => 2,
            'dst' => null,
        ], $res);
    }

    public function testNormalizeDstNotNull(): void
    {
        $mc = $this->prophesize(ModeleContrat::class);
        $mc->getId()->willReturn(1);
        $mcDate = $this->prophesize(ModeleContratDate::class);
        $mcDate->getId()->willReturn(2);

        $mcd = $this->prophesize(ModeleContratDeplacement::class);
        $mcd->getSrc()->willReturn($mcDate->reveal());
        $mcd->getMc()->willReturn($mc->reveal());
        $dst = Carbon::now();
        $mcd->getDst()->willReturn($dst);
        $this->normalizerProphecy->normalize($dst)->willReturn('dstNormalized');

        $res = $this->denormalizer->normalize($mcd->reveal(), 'json');
        self::assertSame([
            'mcId' => 1,
            'srcId' => 2,
            'dst' => 'dstNormalized',
        ], $res);
    }

    public function testDeNormalizeDstNotNull(): void
    {
        [$mc, $mcDate] = $this->initDenormalize();

        $dst = new \DateTime();
        $this->denormalizerProphecy->denormalize('dstEncoded', \DateTime::class)->willReturn($dst);

        $mcd = $this->denormalizer->denormalize([
            'mcId' => 1,
            'srcId' => 2,
            'dst' => 'dstEncoded',
        ], ModeleContratDeplacement::class, 'json');

        self::assertInstanceOf(ModeleContratDeplacement::class, $mcd);
        self::assertSame($mc, $mcd->getMc());
        self::assertSame($mcDate, $mcd->getSrc());
        self::assertSame($dst, $mcd->getDst());
    }

    public function testDeNormalizeDstNull(): void
    {
        [$mc, $mcDate] = $this->initDenormalize();

        $mcd = $this->denormalizer->denormalize([
            'mcId' => 1,
            'srcId' => 2,
            'dst' => null,
        ], ModeleContratDeplacement::class, 'json');

        self::assertInstanceOf(ModeleContratDeplacement::class, $mcd);
        self::assertSame($mc, $mcd->getMc());
        self::assertSame($mcDate, $mcd->getSrc());
        self::assertNull($mcd->getDst());
    }

    private function initDenormalize()
    {
        $mcDate = $this->prophesize(ModeleContratDate::class)->reveal();

        $mcProphecy = $this->prophesize(ModeleContrat::class);
        $mcProphecy->getDates()->willReturn(new ArrayCollection([$mcDate]));
        $mc = $mcProphecy->reveal();

        $this->em->getReference(ModeleContrat::class, 1)->willReturn($mc);
        $this->em->getReference(ModeleContratDate::class, 2)->willReturn($mcDate);

        return [$mc, $mcDate];
    }
}
