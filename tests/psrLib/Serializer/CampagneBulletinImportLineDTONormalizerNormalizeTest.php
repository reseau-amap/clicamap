<?php

declare(strict_types=1);

namespace Test\Serializer;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use PsrLib\DTO\CampagneBulletinImportLineDTO;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Entity\User;
use PsrLib\ORM\Entity\UserEmail;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\Serializer\CampagneBulletinImportLineDTONormalizer;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinImportLineDTONormalizerNormalizeTest extends TestCase
{
    use ProphecyTrait;

    private CampagneBulletinImportLineDTONormalizer $normalizer;

    public function setUp(): void
    {
        $this->normalizer = new CampagneBulletinImportLineDTONormalizer(
            $this->prophesize(AmapienRepository::class)->reveal(),
        );
    }

    public function testNormalizeAucunMontantLibre(): void
    {
        $line = new CampagneBulletinImportLineDTO($this->prophesize(Campagne::class)->reveal());
        $line->setAmapien($this->prophesizeAmapien('email'));
        $line->setPayeur('payeur');
        $line->setPaiementDate(new Carbon('2021-01-02'));
        $line->setPermissionImage('O');

        $normalized = $this->normalizer->normalize($line);

        $this->assertEquals([
            'Email' => 'email',
            'Payeur' => 'payeur',
            'Date de paiement' => '02/01/2021',
            'Autorisation image (O/N)' => 'O',
        ], $normalized);
    }

    public function testNormalizeAmapienAucunEMail(): void
    {
        $line = new CampagneBulletinImportLineDTO($this->prophesize(Campagne::class)->reveal());
        $line->setAmapien($this->prophesizeAmapien(null));
        $line->setPayeur('payeur');
        $line->setPaiementDate(new Carbon('2021-01-02'));
        $line->setPermissionImage('O');

        $normalized = $this->normalizer->normalize($line);

        $this->assertEquals([
            'Email' => '',
            'Payeur' => 'payeur',
            'Date de paiement' => '02/01/2021',
            'Autorisation image (O/N)' => 'O',
        ], $normalized);
    }

    public function testNormalizeMontantLibre(): void
    {
        $line = new CampagneBulletinImportLineDTO($this->prophesize(Campagne::class)->reveal());
        $line->setAmapien($this->prophesizeAmapien('email'));
        $line->setPayeur('payeur');
        $line->setPaiementDate(new Carbon('2021-01-02'));
        $line->setPermissionImage('O');
        $line->setMontantLibres([Money::EUR(100), Money::EUR(200)]);

        $normalized = $this->normalizer->normalize($line);

        $this->assertEquals([
            'Email' => 'email',
            'Payeur' => 'payeur',
            'Date de paiement' => '02/01/2021',
            'Autorisation image (O/N)' => 'O',
            'Montant libre 1' => '1.00',
            'Montant libre 2' => '2.00',
        ], $normalized);
    }

    private function prophesizeAmapien(?string $email)
    {
        $amapien = $this->prophesize(Amapien::class);
        $user = $this->prophesize(User::class);

        if (null === $email) {
            $user->getEmails()->willReturn(new ArrayCollection([]));
        } else {
            $userEmail = $this->prophesize(UserEmail::class);
            $userEmail->getEmail()->willReturn($email);
            $user->getEmails()->willReturn(new ArrayCollection([$userEmail->reveal()]));
        }

        $amapien->getUser()->willReturn($user->reveal());

        return $amapien->reveal();
    }
}
