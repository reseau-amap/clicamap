<?php

declare(strict_types=1);

namespace Test\Serializer;

use Carbon\Carbon;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use PsrLib\DTO\CampagneBulletinImportLineDTO;
use PsrLib\ORM\Entity\Amap;
use PsrLib\ORM\Entity\Amapien;
use PsrLib\ORM\Entity\Campagne;
use PsrLib\ORM\Repository\AmapienRepository;
use PsrLib\Serializer\CampagneBulletinImportLineDTONormalizer;

/**
 * Copyright (c) 2017-2022, Réseau AMAP Auvergne-Rhône-Alpes.
 *
 * This file is part of Clic'AMAP.
 *
 * Clic'AMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clic'AMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Clic'AMAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @internal
 *
 * @coversNothing
 */
class CampagneBulletinImportLineDTONormalizerDenormalizeTest extends TestCase
{
    use ProphecyTrait;

    private CampagneBulletinImportLineDTONormalizer $normalizer;

    private ObjectProphecy $amapienRepo;

    public function setUp(): void
    {
        $this->amapienRepo = $this->prophesize(AmapienRepository::class);
        $this->normalizer = new CampagneBulletinImportLineDTONormalizer(
            $this->amapienRepo->reveal(),
        );
    }

    public function testDenormalizeEmpty(): void
    {
        $amap = $this->prophesizeAmap();
        $campagne = $this->prophesizeCampagne($amap);
        $this->amapienRepo->getByAmapEmail($amap, '')->willReturn(null);

        $line = $this->normalizer->denormalize(
            [],
            CampagneBulletinImportLineDTO::class,
            null,
            ['campagne' => $campagne]
        );

        self::assertSame(null, $line->getAmapien());
        self::assertSame(null, $line->getPayeur());
        self::assertSame(null, $line->getPaiementDate());
        self::assertSame(null, $line->getPermissionImage());
        self::assertSame([], $line->getMontantLibres());
    }

    public function testDenormalizeAucunMontantLibre(): void
    {
        $amapien = $this->prophesizeAmapien();
        $amap = $this->prophesizeAmap();
        $campagne = $this->prophesizeCampagne($amap);
        $this->amapienRepo->getByAmapEmail($amap, 'email')->willReturn($amapien);

        $line = $this->normalizer->denormalize(
            [
                'Email' => 'email',
                'Payeur' => 'payeur',
                'Date de paiement' => '02/01/2021',
                'Autorisation image (O/N)' => 'O',
            ],
            CampagneBulletinImportLineDTO::class,
            null,
            ['campagne' => $campagne]
        );

        self::assertSame($amapien, $line->getAmapien());
        self::assertSame('payeur', $line->getPayeur());
        self::assertTrue($line->getPaiementDate()->isSameDay(Carbon::createFromFormat('d/m/Y', '02/01/2021')));
        self::assertSame('O', $line->getPermissionImage());
        self::assertSame([], $line->getMontantLibres());
    }

    public function testDenormalizeMontantLibre(): void
    {
        $amapien = $this->prophesizeAmapien();
        $amap = $this->prophesizeAmap();
        $campagne = $this->prophesizeCampagne($amap);
        $this->amapienRepo->getByAmapEmail($amap, 'email')->willReturn($amapien);

        $line = $this->normalizer->denormalize(
            [
                'Email' => 'email',
                'Payeur' => 'payeur',
                'Date de paiement' => '02/01/2021',
                'Autorisation image (O/N)' => 'O',
                'Montant libre 1' => '1.23',
                'Montant libre 2' => 'invalid',
                'Montant libre 3' => '4.56',
                'Montant libre 4' => '0',
            ],
            CampagneBulletinImportLineDTO::class,
            null,
            ['campagne' => $campagne]
        );

        self::assertSame($amapien, $line->getAmapien());
        self::assertSame('payeur', $line->getPayeur());
        self::assertTrue($line->getPaiementDate()->isSameDay(Carbon::createFromFormat('d/m/Y', '02/01/2021')));
        self::assertSame('O', $line->getPermissionImage());
        self::assertCount(4, $line->getMontantLibres());
        self::assertTrue(Money::EUR(123)->equals($line->getMontantLibres()[0]));
        self::assertNull($line->getMontantLibres()[1]);
        self::assertTrue(Money::EUR(456)->equals($line->getMontantLibres()[2]));
        self::assertTrue(Money::EUR(0)->equals($line->getMontantLibres()[3]));
    }

    private function prophesizeAmapien(): Amapien
    {
        $amap = $this->prophesize(Amapien::class);

        return $amap->reveal();
    }

    private function prophesizeAmap(): Amap
    {
        $amap = $this->prophesize(Amap::class);

        return $amap->reveal();
    }

    private function prophesizeCampagne(Amap $amap): Campagne
    {
        $campagne = $this->prophesize(Campagne::class);
        $campagne->getAmap()->willReturn($amap);

        return $campagne->reveal();
    }
}
