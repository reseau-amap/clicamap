<?php

declare(strict_types=1);

namespace Test;

use DI\Container;
use PHPUnit\Framework\TestCase;
use PsrLib\Services\PhpDiContrainerSingleton;

/**
 * @internal
 *
 * @coversNothing
 */
class ContainerAwareTestCase extends TestCase
{
    /**
     * @var Container
     */
    public static $container;

    public function setUp(): void
    {
        self::$container = PhpDiContrainerSingleton::getContainer();
    }

    public function tearDown(): void
    {
        PhpDiContrainerSingleton::cleanContainer();
        self::$container = null;
    }
}
