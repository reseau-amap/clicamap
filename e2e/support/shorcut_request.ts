import { Page, expect } from "@playwright/test";

const _request = async (page: Page, url: string) => {
  await page.goto(url);
  await expect(page.locator("body")).toHaveText(/ok/);
};

export const requestResetDb = async (page: Page) => {
  await _request(page, "/CI/resetdb");
};

export const requestAddLlToAmap = async (
  page: Page,
  llname: string,
  amapEmail: string
) => {
  await _request(page, `/CI/addLlToAmap?amap=${amapEmail}&ll=${llname}`);
};
export const requestAddUserToAmap = async (
  page: Page,
  username: string,
  amap: string
) => {
  await _request(
    page,
    `/CI/addUserToAmap?user_name=${username}&amap_name=${amap}`
  );
};
export const forceContratDepassement = async (page: Page, mc: string) => {
  await _request(page, `/CI/forceContratDepassement?mc=${mc}`);
};
export const forceContratProduitRegul = async (
  page: Page,
  mc: string,
  produit: string
) => {
  await _request(
    page,
    `/CI/forceContratProduitRegul?mc=${mc}&produit=${produit}`
  );
};

export const forceContratPaye = async (
  page: Page,
  mc: string,
  amapien: string
) => {
  await _request(page, `/CI/forceContratPaye?mc=${mc}&amapien=${amapien}`);
};
