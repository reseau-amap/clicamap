import { Page } from "@playwright/test";

export const login = async (page: Page, username: string) => {
  await page.goto(`/CI/login?username=${username}`);
  await page.waitForURL("/evenement");
};

export const logout = async (page: Page) => {
  await page.goto(`/portail/deconnexion`);
};
