import { Download, Locator, Page } from "@playwright/test";
import pdfParse from "pdf-parse";

const readPdfInternal = async (download: Download): Promise<string> => {
  const stream = await download.createReadStream();

  const chunks = [];
  for await (let chunk of stream) {
    chunks.push(chunk);
  }
  const pdfData = Buffer.concat(chunks);
  const pdfText = await pdfParse(pdfData);

  return pdfText.text;
};

export const readPdfFromHref = async (
  page: Page,
  link: string
): Promise<string> => {
  const downloadPromise = page.waitForEvent("download");
  // @ref https://stackoverflow.com/a/75890678
  try {
    await page.goto(link, { waitUntil: "networkidle" });
  } catch (e) {}
  const download = await downloadPromise;

  return await readPdfInternal(download);
};

export const readPdf = async (
  page: Page,
  locator: Locator
): Promise<string> => {
  const downloadPromise = page.waitForEvent("download");
  await locator.click();

  const download = await downloadPromise;

  return await readPdfInternal(download);
};
