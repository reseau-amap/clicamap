import { Page } from "@playwright/test";

export const clickTableSelectCheckbox = async (
  page: Page,
  lineContent: string
) => {
  await page
    .locator("tr")
    .filter({
      has: page.locator("td").getByText(lineContent),
    })
    .locator(".js-tableselect-row")
    .click();
};
