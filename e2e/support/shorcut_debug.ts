import { Page } from "@playwright/test";

/**
 * Enable xdebug on server for following requests
 */
export const enableExdebug = async (page: Page) => {
  if (process.env.CI) {
    throw new Error("XDEBUG_SESSION is not available in CI");
  }

  await page.context().addCookies([
    {
      name: "XDEBUG_SESSION",
      value: "PHPSTORM",
      domain: "localhost",
      path: "/",
    },
  ]);
};
