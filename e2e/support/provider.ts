export const admins = ["admin_aura", "admin_rhone"];

export const allUsersNotEmpty = [
  "admin",
  "amapien",
  "amapienref",
  "amap",
  "amap2",
  "paysan",
  "paysan2",
  "regroupement",
].concat(admins);

export const allUsers = ["empty"].concat(allUsersNotEmpty);
