import { writeFileSync } from "fs";

export const writeFile = (path: string, content: Buffer) => {
  writeFileSync(path, content);
};
