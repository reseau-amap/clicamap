import { expect, Locator, Page } from "@playwright/test";

export const clickButtonOnLine = async (
  page: Page,
  lineContent: string,
  link: string
) => {
  const line = page.locator("body .container table tr").filter({
    has: page.locator("> *").getByText(lineContent, { exact: true }),
  });
  await line.getByRole("link", { name: link }).click();
};

export const clickMenuLinkOnLine = async (
  page: Page,
  lineContent: string,
  link: string
) => {
  const line = page.locator("body .container table tr").filter({
    has: page.locator("> *").getByText(lineContent, { exact: true }),
  });
  await line.locator(".dropdown-toggle").click();
  await line
    .locator(".dropdown-menu a")
    .getByText(link, { exact: true })
    .click();
};

export const checkRadio = async (
  page: Page,
  inputLocator: Locator,
  label: string
) => {
  await page
    .locator("label")
    .filter({ has: inputLocator })
    .getByLabel(label)
    .click();
};
export const checkCheckbox = async (
  page: Page,
  inputLocator: Locator,
  label: string
) => {
  await page
    .locator("div.checkbox")
    .filter({ has: inputLocator })
    .getByLabel(label)
    .click();
};

export const select2 = async (page: Page, fieldName, option) => {
  await page.locator(`[name="${fieldName}"] + .select2`).click();
  await page
    .locator(".select2-container li")
    .getByText(option, { exact: true })
    .click();
  await page.locator(`.navbar-default`).click(); // Hide
};

export const fillDatepickerRange = async (page: Page, fieldName, value) => {
  const elt = page.locator(`input[name="${fieldName}"].flatpickr-input`);
  await elt.evaluate((elt, value) => {
    const $elt = $(elt);
    $elt.val(value);
    const searchForm = $elt.parents(".js-form-search");
    if (searchForm.length > 0) {
      searchForm.submit();
    }
  }, value);
};

export const fillDatepicker = async (
  page: Page,
  selector: string,
  date: Date
) => {
  const elt = page.locator(selector);

  await elt.evaluate((e, date) => {
    $(e).datepicker("setValue", date);
    $(e).trigger("changeDate");
  }, date);
};

export const fillNoteEditor = async (
  page: Page,
  fieldName: string,
  value: string
) => {
  await page.evaluate(
    ({ fieldName, value }) => {
      $(`textarea[name="${fieldName}"]`).summernote("editor.insertText", value);
    },
    { fieldName, value }
  );
};

export const fillReadOnly = async (locator: Locator, value: string) => {
  await locator.evaluate((node) => node.removeAttribute("readonly"));
  await locator.fill(value);
  await locator.evaluate((node) => node.setAttribute("readonly", "readonly"));
};

export const getSelectedOption = async (locator: Locator) => {
  return await locator.evaluate((sel) => $(sel).find("option:selected").text());
};
