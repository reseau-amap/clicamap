const DATE_FILE = ".date.override";
import { writeFileSync } from "fs";

export const dateSet = (date: string) => {
  writeFileSync(DATE_FILE, `${date}`);
};
export const dateClean = () => {
  writeFileSync(DATE_FILE, "");
};
