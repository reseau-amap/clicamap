import { Page, expect } from "@playwright/test";

export const menuLinkOnLineShouldContain = async (
  page: Page,
  lineContent: string,
  link: string
) => {
  const lineLocator = page.locator("tr").filter({
    has: page.locator("td").getByText(lineContent, { exact: true }),
  });
  await lineLocator.locator(".dropdown-toggle").click();
  await expect(lineLocator.locator(".dropdown-menu")).toContainText(link);
  await lineLocator.locator(".dropdown-toggle").click(); //close
};

export const menuLinkOnLineShouldNotContain = async (
  page: Page,
  lineContent: string,
  link: string
) => {
  const lineLocator = page.locator("tr").filter({
    has: page.locator("td").getByText(lineContent, { exact: true }),
  });
  await lineLocator.locator(".dropdown-toggle").click();
  await expect(lineLocator.locator(".dropdown-menu")).not.toContainText(link);
  await lineLocator.locator(".dropdown-toggle").click(); //close
};

export const flashMessageShouldContain = async (
  page: Page,
  content: string
) => {
  const locator = page.locator("#flash-alert");
  await expect(locator).toContainText(content);
  await locator.click(); // Force hide
};

export const currentPathShouldBe = async (page: Page, path: string) => {
  const url = new URL(page.url());
  await expect(url.pathname).toBe(path);
};
