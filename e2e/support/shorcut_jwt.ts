import { jwtDecode } from "jwt-decode";
import { expect } from "@playwright/test";

export const extractJwtTokenFromUrl = (urlString: string) => {
  const url = new URL(urlString);
  return url.searchParams.get("token");
};

export const jwtTokenShouldHaveField = (
  token: string,
  field: string,
  value: string
) => {
  const decrypted = jwtDecode(token);
  expect(decrypted[field]).toBe(value);
};
