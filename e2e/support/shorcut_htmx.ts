import { expect, Locator, Page } from "@playwright/test";

export const waitForHTMXRequestEnd = async (form: Locator) => {
  await expect(form).not.toHaveClass(/htmx-.*/);
};
