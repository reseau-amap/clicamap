import { expect, Page } from "@playwright/test";
import { readFileSync } from "fs";

const SMOCKER_HOST = process.env.CI
  ? "http://smocker:8081/"
  : "http://localhost:8081/";

export const smockerMock = async (payload: string) => {
  const response = await fetch(`${SMOCKER_HOST}mocks`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-yaml",
    },
    body: payload,
  });
  expect(response.status).toBe(200);
};
export const smockerReset = async () => {
  const response = await fetch(`${SMOCKER_HOST}reset`, { method: "post" });
  expect(response.status).toBe(200);
};

export const smockerMockAmapAura58rueraulin = async () => {
  const body = readFileSync("e2e/data/nominatim_response_58rueraulin.json", {
    encoding: "utf-8",
  });
  await smockerMock(`
- request:
    method: GET
    path: /search
    query_params:
      q: 58 rue Raulin, 69007 Lyon
      format: jsonv2
      addressdetails: 1
      limit: 5
      countrycodes: fr
  response:
    status: 200
    body: '${body}'`);
};

export const smockerMockAmapAura19quaitilsitt = async () => {
  const body = readFileSync("e2e/data/nominatim_response_19quaitilsitt.json", {
    encoding: "utf-8",
  });
  await smockerMock(`
- request:
    method: GET
    path: /search
    query_params:
      q: 19, Quai Tilsitt 69002 Lyon
      format: jsonv2
      addressdetails: 1
      limit: 5
      countrycodes: fr
  response:
    status: 200
    body: '${body}'`);
};
