import { expect, Page } from "@playwright/test";
import { decode } from "quoted-printable";

const MAILHOG_HOST = process.env.CI
  ? "http://mailhog:8025/"
  : "http://localhost:8025/";

const decodeMimeHeaderQEncoding = (header: string) => {
  return header.replace(
    /=\?([^?]+)\?([Qq])\?([^?]+)\?=/g,
    function (match, charset, encoding, value) {
      value = value.replace(/_/g, " ");
      value = value.replace(/=([0-9a-fA-F]{2})/g, function (match, hex) {
        return String.fromCharCode(parseInt(hex, 16));
      });
      return decodeURIComponent(escape(value));
    }
  );
};

export const expectMailShoudBeReceived = async (
  title: string,
  targets: string[]
) => {
  const response = await fetch(`${MAILHOG_HOST}api/v1/messages`);
  const messages = await response.json();
  const mails = messages.map((message: any) => ({
    title: decodeMimeHeaderQEncoding(message.Content.Headers.Subject[0]),
    target: message.To.map((to: any) => to.Mailbox + "@" + to.Domain),
  }));
  expect(mails).toEqual(
    expect.arrayContaining([
      {
        title: title,
        target: targets,
      },
    ])
  );
};

export const expectMailCount = async (count: number) => {
  const response = await fetch(`${MAILHOG_HOST}api/v1/messages`);
  const messages = await response.json();
  expect(messages).toHaveLength(count);
};

export const expectMailboxEmpty = async () => {
  await expectMailCount(0);
};

export const mailClean = async () => {
  const res = await fetch(`${MAILHOG_HOST}api/v1/messages`, {
    method: "DELETE",
  });
  expect(res.status).toEqual(200);
};

export const mailClickLink = async (page: Page, mailIndex: number) => {
  const response = await fetch(`${MAILHOG_HOST}api/v1/messages`);
  const messages = await response.json();
  const message = messages[mailIndex];
  const messageDecoded = decode(message.Content.Body);
  const link = messageDecoded.match('href="([^"]*)"')[1];
  await page.goto(link);
};
