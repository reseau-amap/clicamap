const ENV_FILE = ".env.test.override";
import { writeFileSync } from "fs";

export const environmentClean = () => {
  writeFileSync(ENV_FILE, "");
};
export const environmentSet = (variable: string, value: string) => {
  writeFileSync(ENV_FILE, `${variable}=${value}`, { flag: "a+" });
};
