import { Page, expect } from "@playwright/test";
import { login } from "./shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "./shorcut_assertions";
import {
  checkCheckbox,
  checkRadio,
  clickMenuLinkOnLine,
  fillDatepicker,
  fillNoteEditor,
  select2,
} from "./shorcut_actions";
import { enableExdebug } from "./shorcut_debug";

export const gotoContractCreation = async (page: Page, ferme: string) => {
  let link = page.getByRole("link", { name: "Gestionnaire AMAP" });
  if (!(await link.isVisible())) {
    link = page.getByRole("link", { name: "Gestionnaire référent" });
  }
  await link.click();

  await page
    .getByRole("link", { name: "Gestion des contrats vierges" })
    .click();
  await page
    .locator('select[name="search_contrat_vierge_amap[ferme]"]')
    .selectOption(ferme);
  const newLink = page.getByRole("button", {
    name: "Créer un nouveau contrat",
  });
  await expect(newLink).toBeEnabled();
  await newLink.click();
  await page.getByRole("link", { name: "A partir de zéro" }).click();
};

export const gotoContractStep2 = async (page: Page, ferme: string) => {
  await gotoContractCreation(page, ferme);

  await page.locator('input[name="model_contrat_form1[nom]"]').fill("nom");
  await page
    .locator('textarea[name="model_contrat_form1[filiere]"]')
    .fill("filiere");
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const gotoContractStep3 = async (page: Page, ferme: string) => {
  await gotoContractStep2(page, ferme);

  await fillDatepicker(
    page,
    'input[name="model_contrat_form2[forclusion]"]',
    new Date("2030-01-01")
  );
  await fillDatepicker(
    page,
    'input[name="date_livraison_debut"]',
    new Date("2030-02-01")
  );
  await fillDatepicker(
    page,
    'input[name="date_livraison_fin"]',
    new Date("2030-03-01")
  );

  await page
    .locator('[name="model_contrat_form2[delaiModifContrat]"]')
    .fill("2");

  await page.getByRole("button", { name: "Générer des dates" }).click();
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const gotoContractStep4 = async (page: Page, ferme: string) => {
  await gotoContractStep3(page, ferme);
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const gotoContractStep5 = async (page: Page, ferme: string) => {
  await gotoContractStep4(page, ferme);

  await checkRadio(
    page,
    page.locator(
      'input[name="model_contrat_form4[amapienGestionDeplacement]"]'
    ),
    "Non"
  );
  await checkRadio(
    page,
    page.locator('input[name="model_contrat_form4[produitsIdentiquePaysan]"]'),
    "Non"
  );
  await checkRadio(
    page,
    page.locator('input[name="model_contrat_form4[produitsIdentiqueAmapien]"]'),
    "Non"
  );
  await expect(
    page.locator('[name="model_contrat_form4[nblivPlancher]"]')
  ).toBeEnabled();
  await page
    .locator('input[name="model_contrat_form4[nblivPlancher]"]')
    .pressSequentially("1");
  await checkRadio(
    page,
    page.locator('input[name="model_contrat_form4[nblivPlancherDepassement]"]'),
    "Oui"
  );
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const gotoContractStep6 = async (page: Page, ferme: string) => {
  await gotoContractStep5(page, ferme);
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const gotoContractStep7 = async (page: Page, ferme: string) => {
  await gotoContractStep6(page, ferme);
  await page
    .locator('input[name="model_contrat_form6[reglementNbMax]"]')
    .fill("1");
  await page.locator("button#btn-plus").click();
  await fillDatepicker(
    page,
    'input[name="model_contrat_form6[dateReglements][1][date]"]',
    new Date("2030-01-10")
  );
  await checkCheckbox(
    page,
    page.locator('input[name="model_contrat_form6[reglementType][]"]'),
    "Chèque"
  );
  await page
    .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
    .fill("modalite");
  await page.getByRole("button", { name: "Étape suivante" }).click();
};

export const createContract = async (page: Page, ferme: string) => {
  await gotoContractStep7(page, ferme);
  await page.getByRole("button", { name: "Valider" }).click();
};

export const createContractWithCustomDelivery = async (
  page: Page,
  ferme: string,
  callback: Function
) => {
  await gotoContractStep4(page, ferme);
  await callback(page);

  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page
    .locator('input[name="model_contrat_form6[reglementNbMax]"]')
    .fill("1");
  await page.locator("button#btn-plus").click();
  await fillDatepicker(
    page,
    'input[name="model_contrat_form6[dateReglements][1][date]"]',
    new Date("2030-01-10")
  );
  await checkCheckbox(
    page,
    page.locator('input[name="model_contrat_form6[reglementType][]"]'),
    "Chèque"
  );
  await page
    .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
    .fill("modalite");
  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page.getByRole("button", { name: "Valider" }).click();
};

export const createValidatedContractWithCustomDelivery = async (
  page: Page,
  ferme: string,
  callback: Function
) => {
  await createContractWithCustomDelivery(page, ferme, callback);

  await clickMenuLinkOnLine(
    page,
    "En création",
    "Envoyer le contrat au paysan pour validation"
  );
  await page.locator(".modal.in").getByText(/OUI/).first().click();
  await validatePaysanContrat(page, "paysan", "nom");
};

export const gotoContractSubscriptionOwn = async (page: Page) => {
  await page.goto("/contrat_signe/contrat_own_new");
  await expect(
    page.locator(".container table tbody tr:nth-child(1) td:nth-child(1)")
  ).toHaveText("contrat 1");
  await page.getByRole("link", { name: "Souscrire" }).click();
};

export const subscribeContractInternal = async (
  page: Page,
  amapien: string,
  contract: string,
  values: object
) => {
  await login(page, amapien);
  await page.goto("/contrat_signe/contrat_own_new");

  await expect(page.locator(".container table")).toContainText(contract);
  await page.getByRole("link", { name: "Souscrire" }).click();
  for (const name in values) {
    await page
      .locator(`input[name="contrat_souscription_form1[items][${name}][qty]"]`)
      .fill(values[name].toString());
  }
  await page.getByRole("button", { name: "Suivant" }).click();
  await page.getByRole("button", { name: "Suivant" }).click();

  await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
  await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
  await page.getByRole("button", { name: "Valider" }).click();
};

export const subscribeContractAmapienToValidate = async (
  page: Page,
  amap: string,
  amapien: string,
  contract: string,
  values: object
) => {
  await login(page, amap);

  await page.goto("/contrat_signe/accueil_amap");
  await page
    .locator('[name="search_contrat_signe_amap[ferme]"]')
    .selectOption({ label: "ferme" });
  await page
    .locator('[name="search_contrat_signe_amap[mc]"]')
    .selectOption({ label: contract });
  await page
    .getByRole("link", { name: "Ajouter le contrat d'un amapien" })
    .click();
  await select2(page, "contrat_signe_new_force[amapien]", amapien);
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();

  for (const name in values) {
    await page
      .locator(`input[name="contrat_souscription_form1[items][${name}][qty]"]`)
      .fill(values[name].toString());
  }
  await page.getByRole("button", { name: "Suivant" }).click();
  await page.getByRole("button", { name: "Suivant" }).click();

  await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
  await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
  await expect(page.locator(".alert-danger")).toHaveCount(2);
  await expect(page.locator(".alert-danger").nth(1)).toContainText(
    "Attention : le contrat nécessite une validation de l'amapien"
  );
  await page.getByRole("button", { name: "Valider" }).click();
};

export const subscribeContract = async (
  page: Page,
  amapien: string,
  contract: string,
  values: object
) => {
  await subscribeContractInternal(page, amapien, contract, values);

  await currentPathShouldBe(page, "/contrat_signe/contrat_own_new");
  await expect(page.locator(".container table")).not.toContainText(contract);
};

export const fermeAdherentYear = async (page: Page, ferme, year) => {
  await login(page, "admin");

  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, ferme, "Modifier la ferme");
  await page.locator('[name="ferme[anneeAdhesions][]"]').selectOption(year);
  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "La ferme a été modifiée avec succès.");
};

export const fermeAdherentCurrentYear = async (page: Page, ferme: string) => {
  await fermeAdherentYear(page, ferme, new Date().getFullYear().toString());
};

export const fermeAssocieeRegroupement = async (
  page: Page,
  ferme: string,
  regroupement: string
) => {
  await login(page, "admin");
  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, ferme, "Modifier la ferme");
  await page.locator('[name="ferme[regroupement]"]').selectOption(regroupement);
  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "La ferme a été modifiée avec succès.");
};

export const associerPaysanFerme = async (page: Page, paysan, ferme) => {
  await login(page, "admin");
  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, ferme, "Paysans de la ferme");
  await page.locator('[name="user_verification[user]"]').fill(paysan);
  await page.getByRole("button", { name: "Ajouter" }).click();
  await flashMessageShouldContain(page, "Le paysan a été ajouté avec succès.");
};

export const associerAmapienReferentFerme = async (
  page: Page,
  amap,
  amapien,
  ferme
) => {
  await login(page, amap);
  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, ferme, "Référents produits de la ferme");
  await page
    .locator('[name="ferme_ref_produit_amapien[amapien]"]')
    .selectOption(amapien);
  await page.getByRole("button", { name: "Ajouter" }).click();
  await flashMessageShouldContain(
    page,
    "Le référent a été ajouté avec succès."
  );
};
export const deAssocierAmapienReferentFerme = async (
  page: Page,
  amap,
  amapien,
  ferme
) => {
  await login(page, amap);
  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, ferme, "Référents produits de la ferme");
  await clickMenuLinkOnLine(page, amapien, "Supprimer le référent");
  await page.locator(".modal.in").getByText("OUI").click();
  await flashMessageShouldContain(
    page,
    "Le référent a été supprimé avec succès."
  );
};

export const clearPaysanFerme = async (page: Page) => {
  await login(page, "admin");
  await page.goto("/ferme");
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, "ferme", "Paysans de la ferme");
  await clickMenuLinkOnLine(
    page,
    "paysan@test.amap-aura.org",
    "Retirer le paysan"
  );
  await page.locator(".modal").getByText("OUI").click();
  await flashMessageShouldContain(
    page,
    "Le paysan a été supprimé avec succès."
  );
};

export const setAmapienAdmin = async (page: Page, amapien, amap) => {
  await login(page, amap);
  await page.getByText("Gestionnaire AMAP").click();
  await page.getByText("Gestion de mon AMAP").click();
  await clickMenuLinkOnLine(page, amap, "Gérer les administrateurs");
  await page.locator('[name="user_verification[user]"]').fill(amapien);
  await page.getByRole("button", { name: "Ajouter" }).click();
  await flashMessageShouldContain(
    page,
    "Les administrateurs ont été mis à jour avec succès."
  );
};

export const desactiverAmapien = async (page: Page, amapien) => {
  await login(page, "admin");
  await page.getByText("Gestionnaire Admin").click();
  await page.getByText("Gestion des amapiens").click();
  await page
    .locator('[name="search_amapien[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amapien, "Désactiver le compte de l'amapien");
  await flashMessageShouldContain(
    page,
    "L'état de l'amapien a été modifié avec succès."
  );
};

export const desactiverPaysan = async (page: Page, paysan) => {
  await login(page, "admin");
  await page.getByText("Gestionnaire Admin").click();
  await page.getByText("Gestion des paysans").click();
  await page
    .locator('[name="search_paysan[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, paysan, "Désactiver le compte du paysan");
  await flashMessageShouldContain(
    page,
    "L'état du paysan a été modifié avec succès."
  );
};

export const supprimerAmapien = async (page: Page, amapien) => {
  await login(page, "admin");
  await page.getByText("Gestionnaire Admin").click();
  await page.getByText("Gestion des amapiens").click();
  await page
    .locator('[name="search_amapien[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amapien, "Supprimer l'amapien");
  await page.locator(".modal.in").getByText("OUI").click();
  await flashMessageShouldContain(page, "L'amapien a été supprimé avec succès");
};

export const supprimerAmap = async (page: Page, amap: string) => {
  await login(page, "admin");
  await clickMainMenuLink(page, "Gestionnaire Admin");
  await clickMainMenuLink(page, "Gestion des AMAP");
  await page
    .locator('select[name="search_amap[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amap, "Supprimer l'AMAP");
  await page.locator(".modal.in").getByText("OUI").click();
  await flashMessageShouldContain(page, "L'AMAP a été supprimée avec succès");
};

export const creerCopieContrat = async (page: Page, contrat) => {
  await login(page, "amap");
  await page.getByText("Gestionnaire AMAP").click();
  await page.getByText("Gestion des contrats vierges").click();
  await page
    .locator('[name="search_contrat_vierge_amap[ferme]"]')
    .selectOption("ferme");
  await page.getByText("Créer un nouveau contrat").click();
  await page.getByText("A partir d'un contrat existant").click();
  await select2(page, "modele_contrat_copy_form[mc]", contrat);
  await page.getByRole("button", { name: "Ajouter" }).click();
  await flashMessageShouldContain(page, "Contrat vierge copié avec succès.");
};

export const ajouterFermeCommentaire = async (
  page: Page,
  ferme,
  commentaire
) => {
  await login(page, "admin");
  await page.getByText("Gestionnaire Admin").click();
  await page.getByText("Gestion des fermes").click();
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");

  await clickMenuLinkOnLine(page, ferme, "Détails de la ferme");
  await page.locator('i[title="Écrire un commentaire daté"]').click();
  await page
    .locator('[name="ferme_commentaire[commentaire]"]')
    .fill(commentaire);
  await page.getByText("Valider").click();
};

export const amapDeFait = async (page: Page, amap) => {
  await login(page, "admin");
  await clickMainMenuLink(page, "Gestionnaire Admin");
  await clickMainMenuLink(page, "Gestion des AMAP");
  await page
    .locator('select[name="search_amap[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amap, "Modifier les informations générales");
  await page
    .locator("#amap_edition_associationDeFait")
    .getByLabel("Oui")
    .check();

  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
};

export const campagneCreationInternal = async (page: Page, nom: string) => {
  await expect(page.locator("h3")).toContainText("Étape 1 : paramètres");

  await page.locator('[name="campagne_form1[nom]"]').fill(nom);
  await page
    .locator('[name="campagne_form1[period][startAt]"]')
    .fill("2000-01-01");
  await page
    .locator('[name="campagne_form1[period][endAt]"]')
    .fill("2030-01-01");
  await page.locator("h3").click(); // Hide popup
  await page
    .locator('[name="campagne_form1[anneeAdhesionAmapien]"]')
    .selectOption("2011");
  await page.locator(".js-new").click();
  await page.locator('[name="campagne_form1[montants][0][montant]"]').fill("1");
  await page
    .locator('[name="campagne_form1[montants][0][titre]"]')
    .fill("Montant 1");
  await fillNoteEditor(
    page,
    "campagne_form1[montants][0][description]",
    "Description montant 1"
  );
  await page
    .locator('[name="campagne_form1[montants][1][montantLibre]"][value="1"]')
    .check();
  await page
    .locator('[name="campagne_form1[montants][1][titre]"]')
    .fill("Montant 2");
  await fillNoteEditor(
    page,
    "campagne_form1[montants][1][description]",
    "Description montant 2"
  );

  await page.getByRole("button", { name: "Étape 2" }).click();

  await page
    .locator('[name="campagne_form2[paiementMethode]"]')
    .fill("Méthode de paiement");
  await fillNoteEditor(
    page,
    "campagne_form2[paiementDescription]",
    "Description de la méthode de paiement"
  );

  await page.getByRole("button", { name: "Étape 3" }).click();

  await fillNoteEditor(page, "campagne_form3[champLibre]", "Champ libre");

  await page.getByRole("button", { name: "Étape 4" }).click();

  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "La campagne a été créée avec succès.");
};

export const campagneCreationAmap = async (
  page: Page,
  amap: string,
  nom: string
) => {
  await login(page, amap);

  await clickMainMenuLink(page, "Gestionnaire AMAP");
  await clickMainMenuLink(page, "Gestion des adhésions");
  await page.getByRole("link", { name: "Créer une campagne" }).click();

  await campagneCreationInternal(page, nom);
};
export const campagneCreationAdmin = async (
  page: Page,
  admin: string,
  amap: string,
  nom: string
) => {
  await login(page, admin);

  await clickMainMenuLink(page, "Reçus");
  await clickMainMenuLink(page, "Amapien");
  await page.locator('select[name="search_campagne[amap]"]').selectOption(amap);
  await page.getByRole("link", { name: "Créer une campagne" }).click();
  await campagneCreationInternal(page, nom);
};

export const campagneSouscription = async (page: Page, amapien, campagne) => {
  await login(page, amapien);

  await clickMainMenuLink(page, "Mon compte");
  await clickMainMenuLink(page, "Adhésions");

  await page
    .locator(".container table tr")
    .filter({ has: page.locator("td").getByText(campagne, { exact: true }) })
    .getByText("Souscrire")
    .click();

  await expect(page.locator("h3")).toContainText("Étape 1 : paramètres");

  await page
    .locator('[name="campagne_bulletin_form1[montants][0][montant]"]')
    .fill("3");

  await page.getByRole("button", { name: "Étape 2" }).click();

  await page.locator('[name="campagne_bulletin_form2[payeur]"]').fill("Payeur");
  await page
    .locator('[name="campagne_bulletin_form2[paiementDate]"]')
    .fill("2020-01-01");
  await page.locator("h3").click(); // Hide popup

  await page.locator('[name="campagne_bulletin_form2[chartre]"]').check();
  await page.getByRole("button", { name: "Étape 3" }).click();
  await page.getByRole("button", { name: "Souscrire" }).click();

  await flashMessageShouldContain(page, "La campagne a été souscrite.");
};

export const amapChangerLieuLivraison = async (
  page: Page,
  amap,
  lieu,
  cpVille
) => {
  await login(page, amap);
  await page.getByText("Gestionnaire AMAP").click();
  await page.getByText("Gestion de mon AMAP").click();
  await clickMenuLinkOnLine(page, amap, "Modifier les lieux de livraison");
  await clickMenuLinkOnLine(page, lieu, "Modifier le lieu de livraison");

  const llLocator = page.locator('[name="amap_livraison_lieu[ville]"]');
  await llLocator.evaluate((node) => node.removeAttribute("readonly"));
  await llLocator.fill(cpVille);
  await page.getByText("Valider").click();
};

export const creerDocumentUtilisateur = async (
  page: Page,
  role,
  nom,
  cible
) => {
  await login(page, role);
  await page.getByText("Communication").click();
  await page.getByText("Gestion des documents").click();
  await page.getByRole("link", { name: "Ajouter" }).click();

  await page
    .locator('[name="document_utilisateur[annee]"]')
    .selectOption("2019");
  await page.locator('[name="document_utilisateur[nom]"]').fill(nom);
  await select2(page, "document_utilisateur[target]", cible);
  await page
    .locator('input[name="document_utilisateur[file]"]')
    .setInputFiles("e2e/data/test.pdf");
  await page.getByText("Sauvegarder").click();

  await flashMessageShouldContain(page, "Le document a bien été créé.");
};

export const createNewEvent = async (page: Page, event: object) => {
  await page.getByText("Créer un nouvel événement").click();

  await page
    .locator("#reg_84")
    .evaluate((node) => node.setAttribute("onclick", null));
  await page
    .locator("#regions")
    .evaluate((node) => node.setAttribute("onclick", null));
  await page.locator("#reg_84").click();
  await page.getByRole("button", { name: "Étape suivante" }).click();

  await fillNoteEditor(page, "evt_nom", event.name);
  await fillNoteEditor(page, "evt_txt", event.description);
  await page.locator('[name="url"]').fill(event.url);
  await page.locator('[name="date_deb"]').fill(event.start_date);
  await page.locator('[name="hor_deb"]').fill(event.start_hour);
  await page.locator('[name="date_fin"]').fill(event.end_date);
  await page.locator('[name="hor_fin"]').fill(event.end_hour);
  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page.getByText("Sauvegarder").click();
};

export const disableProduct = async (
  page: Page,
  paysan: string,
  product: string
) => {
  await login(page, paysan);
  await clickMainMenuLink(page, "Gestionnaire ferme");
  await clickMainMenuLink(page, "Mes produits");

  await clickMenuLinkOnLine(page, product, "Modifier le produit");
  await page.locator('[name="ferme_produit[enabled]"]').uncheck();
  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "Produit modifié avec succès.");
};

export const clickMainMenuLink = async (page: Page, link: string) => {
  await page
    .locator("nav.navbar-default")
    .getByRole("link", { name: link, exact: true })
    .click();
};
export const goViaMenu = async (page: Page, links: string[]) => {
  for (const link of links) {
    await clickMainMenuLink(page, link);
  }
};

export const createNewActu = async (
  page: Page,
  titre: string,
  date: Date,
  description: string
) => {
  await login(page, "admin");
  await clickMainMenuLink(page, "Communication");
  await clickMainMenuLink(page, "Gestion des versions");

  await page.getByRole("link", { name: "Ajouter" }).click();

  await page.locator('[name="actualite[titre]"]').fill(titre);
  await fillDatepicker(page, '[name="actualite[date]"]', date);
  await fillNoteEditor(page, "actualite[description]", description);

  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "L'actualité a bien été enregistrée");

  await page.goto("/evenement");
  await page
    .locator("#modal_actualite.in")
    .getByText("J'ai compris, merci")
    .click();
};

export const validatePaysanContrat = async (
  page: Page,
  paysan: string,
  contrat: string
) => {
  await login(page, paysan);
  await clickMainMenuLink(page, "Gestion des contrats");
  await clickMainMenuLink(page, "Contrats à valider");
  const line = page.locator("body .container table tbody tr").filter({
    has: page.locator("td:nth-child(1)").getByText(contrat, { exact: true }),
  });
  await line.getByRole("link", { name: "Voir le contrat" }).click();

  await page
    .locator('[name="modele_contrat_paysan_approval[confirmSign]"]')
    .check();
  await page
    .locator('[name="modele_contrat_paysan_approval[confirmCharter]"]')
    .check();
  await page
    .getByText(/Valider/)
    .first()
    .click();
};

export const createNewEvtAmap = async (page: Page, titre: string) => {
  await login(page, "amap");

  await page.getByRole("link", { name: "Ajouter une actualité" }).click();

  await page.locator('[name="evenement[titre]"]').fill("Nouvelle actualité");
  await fillNoteEditor(
    page,
    "evenement[description]",
    "Description de l'actualité"
  );
  await fillNoteEditor(
    page,
    "evenement[descriptionCourte]",
    "Description courte de l'actualité"
  );
  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "L'actualité a bien été ajoutée");
};

export const removeUserEmails = async (page: Page, user: string) => {
  await login(page, user);

  await clickMainMenuLink(page, "Mon compte");
  await clickMainMenuLink(page, "Mon profil");
  await page.locator('a[title="Éditer mon profil"]').click();
  await page.locator('button[title="Supprimer l\'adresse email"]').click();

  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "Votre profil a bien été mis à jour.");
};

export const setProduitRegul = async (
  page: Page,
  paysan: string,
  produit: string,
  check: boolean
) => {
  await login(page, paysan);
  await goViaMenu(page, ["Gestionnaire ferme", "Mes produits"]);

  await clickMenuLinkOnLine(page, produit, "Modifier le produit");
  await page.locator('[name="ferme_produit[regulPds]"]').setChecked(check);
  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "Produit modifié avec succès.");
};

export const addProduit = async (
  page: Page,
  paysan: string,
  tp: string,
  nom: string,
  cond: string,
  prix: string,
  regulPds: boolean
) => {
  await login(page, paysan);
  await goViaMenu(page, ["Gestionnaire ferme", "Mes produits"]);

  await page.getByRole("link", { name: "Ajouter un nouveau produit" }).click();
  await page.locator('[name="ferme_produit[typeProduction]"]').selectOption(tp);
  await page.locator('[name="ferme_produit[nom]"]').fill(nom);
  await page.locator('[name="ferme_produit[conditionnement]"]').fill(cond);
  await page.locator('[name="ferme_produit[prix][prixTTC]"]').fill(prix);
  await page.locator('[name="ferme_produit[regulPds]"]').setChecked(regulPds);
  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "Produit ajouté avec succès.");
};

export const setAmapEtudiante = async (
  page: Page,
  amap: string,
  etudiante: boolean
) => {
  await login(page, "admin");
  await goViaMenu(page, ["Gestionnaire Admin", "Gestion des AMAP"]);
  await page
    .locator('[name="search_amap[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amap, "Modifier les informations générales");
  await page
    .locator(
      `[name="amap_edition[amapEtudiante]"][value="${etudiante ? "1" : "0"}"]`
    )
    .check();
  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
};

export const addAmapEtudianteAbsence = async (
  page: Page,
  amap: string,
  titre: string,
  dateDeb: Date,
  dateFin: Date
) => {
  await login(page, "admin");
  await goViaMenu(page, ["Gestionnaire Admin", "Gestion des AMAP"]);
  await page
    .locator('[name="search_amap[region]"]')
    .selectOption("AUVERGNE-RHÔNE-ALPES");
  await clickMenuLinkOnLine(page, amap, "Planning des absences");
  await page
    .getByRole("link", { name: "Ajouter une nouvelle période d'absence" })
    .click();
  await page.locator('[name="amap_absence[titre]"]').fill(titre);
  await fillDatepicker(page, '[name="amap_absence[absenceDebut]"]', dateDeb);
  await fillDatepicker(page, '[name="amap_absence[absenceFin]"]', dateFin);
  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await expect(page.locator("table.table tbody")).toContainText(titre);
};
