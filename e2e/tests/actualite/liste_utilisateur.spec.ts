import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { createNewActu } from "../../support/shorcut";

test.describe("Lister les actualités - footer", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Tri par défaut par date", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 3",
      new Date("2000-01-03"),
      "Description 3"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await page.locator("footer").getByText("Les dernières versions").click();

    await expect(page.locator("h3")).toContainText("Les dernières versions");
    await expect(page.locator("h3 ~.row").nth(0)).toContainText("03.01.2000");
    await expect(page.locator("h3 ~.row").nth(1)).toContainText("02.01.2000");
    await expect(page.locator("h3 ~.row").nth(2)).toContainText("01.01.2000");
  });
});
