import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { clickMainMenuLink, createNewActu } from "../../support/shorcut";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Supprimer les actualités", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Suppression depuis le menu", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await clickMainMenuLink(page, "Communication");
    await clickMainMenuLink(page, "Gestion des versions");

    await clickMenuLinkOnLine(page, "Titre 1", "Supprimer");
    await page.locator(".modal.in").getByText("OUI").click();
    await flashMessageShouldContain(
      page,
      "Les actualités ont bien été supprimées"
    );
    await expect(
      page.locator(".container table.table-datatable tbody tr td")
    ).toHaveText("Aucune donnée disponible dans le tableau");
  });

  test("Suppression d'une actu depuis les checkboxes", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await clickMainMenuLink(page, "Communication");
    await clickMainMenuLink(page, "Gestion des versions");

    await expect(
      page.locator(".col-md-12 > button").getByText("Supprimer")
    ).toHaveAttribute("disabled");

    await page
      .locator(".container table.table-datatable tbody tr:nth-child(1) input")
      .check();
    await expect(
      page.getByRole("button", { name: "Supprimer" })
    ).not.toHaveAttribute("disabled");
    await page.getByRole("button", { name: "Supprimer" }).click();

    await page.locator(".modal.in").getByText("OUI").click();
    await flashMessageShouldContain(
      page,
      "Les actualités ont bien été supprimées"
    );
    await expect(
      page.locator(".container table.table-datatable tbody tr")
    ).toHaveCount(1);
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(3)"
      )
    ).toContainText("Titre 1");
  });

  test("Suppression de toutes les actus", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await clickMainMenuLink(page, "Communication");
    await clickMainMenuLink(page, "Gestion des versions");

    await expect(
      page.locator(".col-md-12 > button").getByText("Supprimer")
    ).toHaveAttribute("disabled");

    await page.locator(".js-tableselect-all").check();
    await expect(
      page.getByRole("button", { name: "Supprimer" })
    ).not.toHaveAttribute("disabled");
    await page.getByRole("button", { name: "Supprimer" }).click();

    await page.locator(".modal.in").getByText("OUI").click();
    await flashMessageShouldContain(
      page,
      "Les actualités ont bien été supprimées"
    );
    await expect(
      page.locator(".container table.table-datatable tbody tr")
    ).toHaveCount(1);
    await expect(
      page.locator(".container table.table-datatable tbody tr td")
    ).toHaveText("Aucune donnée disponible dans le tableau");
  });
});
