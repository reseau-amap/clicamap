import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMainMenuLink } from "../../support/shorcut";
import { fillDatepicker, fillNoteEditor } from "../../support/shorcut_actions";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";

const goToForm = async (page: Page) => {
  await login(page, "admin");
  await clickMainMenuLink(page, "Communication");
  await clickMainMenuLink(page, "Gestion des versions");

  await page.getByRole("link", { name: "Ajouter" }).click();
};

test.describe("Ajouter une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Validation des champs vide", async ({ page }) => {
    await goToForm(page);
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await expect(
      page.locator('[name="actualite[titre]"] + .alert-danger')
    ).toHaveText(/Cette valeur ne doit pas être vide./);
    await expect(
      page.locator('[name="actualite[date]"] + .alert-danger')
    ).toHaveText(/Cette valeur ne doit pas être nulle./);
    await expect(
      page.locator('[name="actualite[description]"] ~ .alert-danger')
    ).toHaveText(/Cette valeur ne doit pas être vide./);
  });

  test("Ajout valide", async ({ page }) => {
    await goToForm(page);

    await expect(page.locator(".col-md-12 > h3")).toContainText(
      "Nouvelle version"
    );
    await expect(page.locator(".col-md-12 > h5")).toContainText(
      "Nouvelle version"
    );

    await page.locator('[name="actualite[titre]"]').fill("Titre");
    await fillDatepicker(
      page,
      '[name="actualite[date]"]',
      new Date("2000-01-02")
    );
    await fillNoteEditor(page, "actualite[description]", "description");

    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(page, "L'actualité a bien été enregistrée");

    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(2)"
      )
    ).toContainText("02/01/2000");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(3)"
      )
    ).toContainText("Titre");
  });

  test("Erreur description trop longue", async ({ page }) => {
    await goToForm(page);

    await page.locator('[name="actualite[titre]"]').fill("Titre");
    await fillDatepicker(
      page,
      '[name="actualite[date]"]',
      new Date("2000-01-02")
    );
    await fillNoteEditor(page, "actualite[description]", "a".repeat(3001));

    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await expect(
      page.locator("#actualite_description ~ .alert-danger")
    ).toHaveText("La description est trop longue.");
  });

  test("Annulation ajout", async ({ page }) => {
    await goToForm(page);

    await page.getByRole("link", { name: "Annuler" }).click();

    await currentPathShouldBe(page, "/actualites");
    await expect(page.locator("h3")).toContainText("Gestion des versions");
  });
});
