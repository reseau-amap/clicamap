import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { clickMainMenuLink, createNewActu } from "../../support/shorcut";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Modification d'une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Modification depuis le menu", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );

    await clickMainMenuLink(page, "Communication");
    await clickMainMenuLink(page, "Gestion des versions");

    await clickMenuLinkOnLine(page, "Titre 1", "Modifier");
    await expect(page.locator(".col-md-12 > h3")).toContainText(
      "Modification version"
    );
    await expect(page.locator(".col-md-12 > h5")).toContainText(
      "Modification version"
    );

    await page.locator('[name="actualite[titre]"]').fill("Nouveau titre");
    await page.getByRole("button", { name: "Sauvegarder" }).click();

    await flashMessageShouldContain(page, "L'actualité a bien été enregistrée");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(3)"
      )
    ).toContainText("Nouveau titre");
  });
});
