import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { clickMainMenuLink, createNewActu } from "../../support/shorcut";
import { login } from "../../support/shorcut_auth";
import { currentPathShouldBe } from "../../support/shorcut_assertions";

test.describe("Lister les actualités - modal", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Affichage de la dernière actu en modal tant que pas validé sur la page des évènements", async ({
    page,
  }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await login(page, "amapien");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(page.locator("#modal_actualite .modal-header")).toContainText(
      "Titre 2"
    );
    await page.locator("#modal_actualite .modal-header button").click();

    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Mon profil");
    await expect(page.locator("#modal_actualite")).not.toBeVisible();

    await page.goto("/evenement");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(page.locator("#modal_actualite .modal-header")).toContainText(
      "Titre 2"
    );
  });

  test("Disparition des actualités avec le bouton compris", async ({
    page,
  }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await login(page, "amapien");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(page.locator("#modal_actualite .modal-header")).toContainText(
      "Titre 2"
    );
    await page
      .locator("#modal_actualite.in")
      .getByText("J'ai compris, merci")
      .click();
    await expect(page.locator("#modal_actualite")).not.toBeVisible();

    await page.goto("/evenement");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(page.locator("#modal_actualite .modal-header")).toContainText(
      "Titre 1"
    );
    await page
      .locator("#modal_actualite.in")
      .getByText("J'ai compris, merci")
      .click();
    await expect(page.locator("#modal_actualite")).not.toBeVisible();

    await page.goto("/evenement");
    await expect(page.locator("#modal_actualite")).not.toBeVisible();
  });

  test('Masquer le bouton "Voir tout" avec une seule actu', async ({
    page,
  }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );

    await login(page, "amapien");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(
      page.locator("#modal_actualite .modal-footer button")
    ).toHaveCount(1);
  });

  test('Afficher toutes les actus avec le bouton "Voir tout"', async ({
    page,
  }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-01"),
      "Description 1"
    );

    await login(page, "amapien");
    await expect(page.locator("#modal_actualite")).toBeVisible();
    await expect(
      page.locator("#modal_actualite .modal-footer button")
    ).toHaveCount(2);
    await page
      .locator("#modal_actualite .modal-footer button")
      .getByText("Voir les 1 autres versions")
      .click();

    await expect(page.locator("h3")).toContainText("Les dernières versions");
    await currentPathShouldBe(page, "/actualites/user");

    await page.goto("/evenement");
    await expect(page.locator("#modal_actualite")).not.toBeVisible();
  });
});
