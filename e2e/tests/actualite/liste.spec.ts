import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import { clickMainMenuLink, createNewActu } from "../../support/shorcut";

test.describe("Lister les actualités", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Tri par défaut par date", async ({ page }) => {
    await createNewActu(
      page,
      "Titre 1",
      new Date("2000-01-01"),
      "Description 1"
    );
    await createNewActu(
      page,
      "Titre 3",
      new Date("2000-01-03"),
      "Description 3"
    );
    await createNewActu(
      page,
      "Titre 2",
      new Date("2000-01-02"),
      "Description 2"
    );

    await clickMainMenuLink(page, "Communication");
    await clickMainMenuLink(page, "Gestion des versions");

    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(2)"
      )
    ).toContainText("03/01/2000");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(1) td:nth-child(3)"
      )
    ).toContainText("Titre 3");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(2) td:nth-child(2)"
      )
    ).toContainText("02/01/2000");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(2) td:nth-child(3)"
      )
    ).toContainText("Titre 2");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(3) td:nth-child(2)"
      )
    ).toContainText("01/01/2000");
    await expect(
      page.locator(
        ".container table.table-datatable tbody tr:nth-child(3) td:nth-child(3)"
      )
    ).toContainText("Titre 1");
  });
});
