import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";

test.describe("Comme utilisateur, je devrais pouvoir m'authentifier", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const db = [
    [
      "empty",
      "Bonjour Empty_prenom EMPTY_NOM, bienvenue sur Clic'AMAP ! Pour l'instant, aucun droits spécifiques ne vous a été attribué. En attendant, vous pouvez vérifier / modifier les informations de votre profil.",
    ],

    ["admin", "Bonjour Admin_prenom ADMIN_NOM, bienvenue sur Clic'AMAP !"],
    [
      "admin_aura",
      "Bonjour Admin_aura_prenom ADMIN_AURA_NOM, bienvenue sur Clic'AMAP !",
    ],

    [
      "admin_rhone",
      "Bonjour Admin_rhone_prenom ADMIN_RHONE_NOM, bienvenue sur Clic'AMAP !",
    ],

    [
      "amapien",
      "Bonjour Prenom amapien NOM AMAPIEN, bienvenue sur Clic'AMAP !",
    ],

    [
      "amapienref",
      "Bonjour Prénom amapien référent NOM AMAPIEN RÉFÉRENT, bienvenue sur Clic'AMAP !",
    ],

    ["amap", "Bonjour Amap_prenom AMAP_NOM, bienvenue sur Clic'AMAP !"],
    ["amap2", "Bonjour Amap2_prenom AMAP2_NOM, bienvenue sur Clic'AMAP !"],
    ["paysan", "Bonjour Paysan_prenom PAYSAN_NOM, bienvenue sur Clic'AMAP !"],
    [
      "paysan2",
      "Bonjour Paysan_prenom2 PAYSAN_NOM2, bienvenue sur Clic'AMAP !",
    ],

    [
      "regroupement",
      "Bonjour Regroupement_prenom REGROUPEMENT_NOM, bienvenue sur Clic'AMAP !",
    ],
  ];

  db.forEach((row) => {
    test(`Authentification succès nom utilisateur ${row[0]}`, async ({
      page,
    }) => {
      await page.goto("/");
      await page.locator('[name="user_login[user]"]').fill(row[0]);
      await page
        .locator('[name="user_login[plainPassword]"]')
        .fill("P@ssw0rd!");
      await page
        .getByText(/Connexion/)
        .first()
        .click();
      await expect(page.locator(".alert-success")).toContainText(row[1]);
    });
  });

  const dbEmails = [
    "admin@test.amap-aura.org",
    "admin_aura@test.amap-aura.org",
    "admin_rhone@test.amap-aura.org",
    "amapien@test.amap-aura.org",
    "amapienref@test.amap-aura.org",
    "amap@test.amap-aura.org",
    "amap2@test.amap-aura.org",
    "paysan@test.amap-aura.org",
    "paysan2@test.amap-aura.org",
    "regroupement@test.amap-aura.org",
  ];

  dbEmails.forEach((email) => {
    test(`Authentification succès email ${email}`, async ({ page }) => {
      await page.goto("/");
      await page.locator('[name="user_login[user]"]').fill(email);
      await page
        .locator('[name="user_login[plainPassword]"]')
        .fill("P@ssw0rd!");
      await page
        .getByText(/Connexion/)
        .first()
        .click();
      await expect(page.locator(".alert-success")).toContainText(
        "bienvenue sur Clic'AMAP !"
      );
    });
  });

  test(`Nom d'utilisateur inexistant`, async ({ page }) => {
    await page.goto("/");
    await page.locator('[name="user_login[user]"]').fill("invalid");
    await page.locator('[name="user_login[plainPassword]"]').fill("P@ssw0rd!");
    await page
      .getByText(/Connexion/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      "Utilisateur non trouvé"
    );
  });

  test(`Mauvais mdp`, async ({ page }) => {
    await page.goto("/");
    await page.locator('[name="user_login[user]"]').fill("empty");
    await page.locator('[name="user_login[plainPassword]"]').fill("invalid");
    await page
      .getByText(/Connexion/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      "Mot de passe incorrect"
    );
  });

  test(`Casse independant - Email`, async ({ page }) => {
    await page.goto("/");
    await page
      .locator('[name="user_login[user]"]')
      .fill("Admin@Test.amap-aura.org");
    await page.locator('[name="user_login[plainPassword]"]').fill("P@ssw0rd!");
    await page
      .getByText(/Connexion/)
      .first()
      .click();
    await expect(page.locator(".alert-success")).toContainText(
      "bienvenue sur Clic'AMAP !"
    );
  });

  test(`Casse independant - Username`, async ({ page }) => {
    await page.goto("/");
    await page.locator('[name="user_login[user]"]').fill("Admin");
    await page.locator('[name="user_login[plainPassword]"]').fill("P@ssw0rd!");
    await page
      .getByText(/Connexion/)
      .first()
      .click();
    await expect(page.locator(".alert-success")).toContainText(
      "bienvenue sur Clic'AMAP !"
    );
  });
});
