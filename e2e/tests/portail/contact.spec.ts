import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Comme utilisateur, je devrais pouvoir envoyer un message de contact", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test(`Accès depuis l'accueil`, async ({ page }) => {
    await page.goto("/");
    await page
      .getByText(/contactez-nous/)
      .first()
      .click();
    await expect(page).toHaveURL(/\/portail\/contact/);
  });

  test(`Envoi d'une demande de contact`, async ({ page }) => {
    await page.goto("/portail/contact");
    await page.locator('[name="portail_contact[nom]"]').fill("Nom");
    await page.locator('[name="portail_contact[prenom]"]').fill("Prénom");
    await page
      .locator('[name="portail_contact[email]"]')
      .fill("test@test.amap-aura.org");
    await page.locator('[name="portail_contact[cp]"]').fill("00000");
    await page.locator('[name="portail_contact[contenu]"]').fill("contenu");
    await page
      .locator('[name="portail_contact[captcha]"]')
      .fill("captchaValid");
    await page
      .getByText(/Envoyer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Votre message est bien envoyé. Merci et bonne journée"
    );
    await expectMailShoudBeReceived(
      "Nouveau message du formulaire de contact Clic'AMAP",
      ["clicamap@amap-aura.org"]
    );
  });

  const dbCp = [
    ["69001", "admin_rhone@test.amap-aura.org"],
    ["01000", "admin_aura@test.amap-aura.org"],
    ["21000", "clicamap@amap-aura.org"],
  ];

  dbCp.forEach((row) => {
    test(`Vérification de l'envoi aux bonnes adresses mail ${row[0]}`, async ({
      page,
    }) => {
      await page.goto("/portail/contact");
      await page.locator('[name="portail_contact[nom]"]').fill("Martin");
      await page.locator('[name="portail_contact[prenom]"]').fill("Eugène");
      await page
        .locator('[name="portail_contact[email]"]')
        .fill("test@test.amap-aura.org");
      await page.locator('[name="portail_contact[cp]"]').fill(row[0]);
      await page
        .locator('[name="portail_contact[contenu]"]')
        .fill("Du contenu");
      await page
        .locator('[name="portail_contact[captcha]"]')
        .fill("captchaValid");
      await page
        .getByText(/Envoyer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Votre message est bien envoyé. Merci et bonne journée"
      );
      await expectMailShoudBeReceived(
        "Nouveau message du formulaire de contact Clic'AMAP",
        [row[1]]
      );
    });
  });
});
