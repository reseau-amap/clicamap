import { test, expect, Page } from "@playwright/test";
import { login } from "../../support/shorcut_auth";

test.describe('Comme utilisateur, je doit revenir sur la page "evenement" si je suis connecté au portail', () => {
  test(`Redirection sans authentifiation`, async ({ page }) => {
    await page.goto("/portail");
    await expect(page).toHaveURL(/\/portail\/connexion/);
  });

  test(`Redirection authentifié`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/portail");
    await expect(page).toHaveURL(/\/evenement/);
  });
});
