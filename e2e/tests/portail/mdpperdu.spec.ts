import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Comme utilisateur, je dois pouvoir récupérer mon mot de passe oublié", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.beforeEach(async ({ page }) => {
    await mailClean();
  });

  test(`Accéder à la page depuis l'accueil`, async ({ page }) => {
    await page.goto("/");
    await page
      .getByText(/Mot de passe oublié \?/)
      .first()
      .click();
    await expect(page.locator(".modal.in")).toHaveText(/Mot de passe oublié/);
    await expect(page).toHaveURL(/\/portail\/mdp_oublie/);
  });

  test(`Nom d'utilisateur inconnu`, async ({ page }) => {
    await page.goto("/portail/mdp_oublie");
    await page
      .locator('[name="user_mdp_oublie[user]"]')
      .fill("inconnu@domain.tld");
    await page
      .getByText(/Envoyer/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Nom d'utilisateur ou email invalide"
    );
  });

  ["admin", "admin@test.amap-aura.org"].forEach((user) => {
    test(`Utilisateur valide ${user}`, async ({ page }) => {
      await page.goto("/portail/mdp_oublie");
      await page.locator('[name="user_mdp_oublie[user]"]').fill(user);
      await page
        .getByText(/Envoyer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Un email contenant un lien vous permettant de générer un nouveau mot de passe vient de vous être envoyé, veuillez consulter votre boite mail"
      );
      await expectMailShoudBeReceived(
        "Réinitialisation de votre mot de passe Clic'AMAP",
        ["admin@test.amap-aura.org"]
      );
    });
  });
});
