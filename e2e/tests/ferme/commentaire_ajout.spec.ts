import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { dateClean, dateSet } from "../../support/shorcut_date";

const goToPage = async (page: Page) => {
  await login(page, "admin");
  await page
    .getByText(/Gestionnaire Admin/)
    .first()
    .click();
  await page
    .getByText(/Gestion des fermes/)
    .first()
    .click();
  await page
    .locator('[name="search_ferme[region]"]')
    .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
  await clickMenuLinkOnLine(page, "Ferme", "Détails de la ferme");
  await expect(
    page.locator('form[name="ferme_commentaire"]')
  ).not.toBeVisible();
  await page.locator('i[title="Écrire un commentaire daté"]').click();
  await expect(page.locator('form[name="ferme_commentaire"]')).toBeVisible();
};

test.describe("Ajout d'un commentaire sur une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Ajout d'un commentaire avec succès comme admin`, async ({ page }) => {
    dateSet("2000-01-01");
    await goToPage(page);
    await page
      .locator('[name="ferme_commentaire[commentaire]"]')
      .fill("Commentaire de test");
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(
      page.locator(".ferme_commentaires .alert-warning")
    ).toContainText("Commentaire de test");
  });

  test(`Afficher le formulaire en cas d'erreur comme admin`, async ({
    page,
  }) => {
    await goToPage(page);
    await page.locator('[name="ferme_commentaire[commentaire]"]').fill("    ");
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator('form[name="ferme_commentaire"]')).toBeVisible();
    await expect(page.locator("form .alert-danger")).toContainText(
      "Cette valeur ne doit pas être vide."
    );
  });

  test(`Masquer le formulaire pour les non admins`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Ma ferme/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "ferme", "Détails de la ferme");
    await expect(
      page.locator('i[title="Écrire un commentaire daté"]')
    ).not.toBeVisible();
    await expect(
      page.locator('form[name="ferme_commentaire"]')
    ).not.toBeVisible();
  });
});
