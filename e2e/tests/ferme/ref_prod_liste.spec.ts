import { test, expect, Page } from "@playwright/test";
import { login } from "../../support/shorcut_auth";
import {
  clickMenuLinkOnLine,
  getSelectedOption,
} from "../../support/shorcut_actions";

test.describe("Lister les réferents produits d'une ferme", () => {
  test(`En tant que super admin`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    expect(
      await getSelectedOption(
        page.locator('[name="ferme_ref_produit[regionDepartement][region]"]')
      )
    ).toBe("");
    expect(
      await getSelectedOption(
        page.locator(
          '[name="ferme_ref_produit[regionDepartement][departement]"]'
        )
      )
    ).toBe("");
    await expect(
      page.locator('[name="ferme_ref_produit[amap]"] option')
    ).toHaveCount(0);
    await expect(page.locator(".container table")).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toHaveCount(0);
    await page
      .locator('[name="ferme_ref_produit[regionDepartement][region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(
      page.locator('[name="ferme_ref_produit[amap]"] option')
    ).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit[amapien]"]')
    ).toHaveCount(0);
    await expect(page.locator(".container table")).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toHaveCount(0);
    await page
      .locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
      .selectOption({ label: "RHÔNE" });
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
    await page
      .locator('[name="ferme_ref_produit[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });

  test(`En tant qu'admin aura`, async ({ page }) => {
    await login(page, "admin_aura");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][region]"]')
    ).toHaveText("AUVERGNE-RHÔNE-ALPES");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
    ).toHaveValue("");
    await expect(
      page.locator('[name="ferme_ref_produit[amap]"] option')
    ).toHaveCount(0);
    await expect(page.locator(".container table")).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toHaveCount(0);
    await page
      .locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
      .selectOption({ label: "RHÔNE" });
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
    await page
      .locator('[name="ferme_ref_produit[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });

  test(`En tant qu'admin rhone`, async ({ page }) => {
    await login(page, "admin_rhone");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][region]"]')
    ).toHaveText("AUVERGNE-RHÔNE-ALPES");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
    ).toHaveText("RHÔNE");
    await expect(page.locator('[name="ferme_ref_produit[amap]"]')).toHaveValue(
      ""
    );
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toHaveCount(0);
    await page
      .locator('[name="ferme_ref_produit[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });

  test(`En tant qu'amap`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][region]"]')
    ).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
    ).toHaveCount(0);
    await expect(page.locator('[name="ferme_ref_produit[amap]"]')).toHaveText(
      "amap test"
    );
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });

  test(`En tant qu'amapien référent`, async ({ page }) => {
    await login(page, "amapienref");
    await page.goto("/ferme/home_refprod/");
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][region]"]')
    ).toHaveCount(0);
    await expect(
      page.locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
    ).toHaveCount(0);
    await expect(page.locator('[name="ferme_ref_produit[amap]"]')).toHaveText(
      "amap test"
    );
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(page.locator(".container table")).toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });
});
