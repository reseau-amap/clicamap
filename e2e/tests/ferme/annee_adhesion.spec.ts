import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Modification des années d'adhésion d'une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Modification des annes d'adhesion d'une ferme`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/ferme/informations_generales/1");
    await page
      .locator('[name="ferme[anneeAdhesions][]"]')
      .selectOption({ label: "2011" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "La ferme a été modifiée avec succès."
    );
    await page.goto("/ferme/display/1");
    await expect(page.locator(".container > .row .well")).toHaveText(/2011/);
    await page.goto("/paysan/display/1");
    await expect(page.locator(".container > .row .well")).toHaveText(/2011/);
  });
});
