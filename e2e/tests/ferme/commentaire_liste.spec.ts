import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { ajouterFermeCommentaire } from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Afficher la liste des commentaires d'une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Afficher les commentaires par ordre de date comme admin`, async ({
    page,
  }) => {
    dateSet("2000-01-01");
    await ajouterFermeCommentaire(page, "Ferme", "Commentaire de test 1");
    dateSet("2000-01-02");
    await ajouterFermeCommentaire(page, "Ferme", "Commentaire de test 2");
    await expect(
      page.locator(".ferme_commentaires .alert-warning:nth-child(1)")
    ).toContainText("Commentaire de test 2");
    await expect(
      page.locator(".ferme_commentaires .alert-warning:nth-child(2)")
    ).toContainText("Commentaire de test 1");
  });

  test(`Masquer les commentaires pour les non admins`, async ({ page }) => {
    await ajouterFermeCommentaire(page, "Ferme", "Commentaire de test 1");
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Ma ferme/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "ferme", "Détails de la ferme");
    await expect(
      page.locator(".ferme_commentaires .alert-warning")
    ).not.toBeVisible();
  });
});
