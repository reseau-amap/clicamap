import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";

import { ajouterFermeCommentaire } from "../../support/shorcut";

test.describe("Supprimer un commentaire sur une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Supprimer un commentaire avec succès comme admin`, async ({ page }) => {
    await ajouterFermeCommentaire(page, "Ferme", "Commentaire de test");
    await page.locator(".alert-warning i.glyphicon-remove").click();
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await expect(
      page.locator(".alert-warning i.glyphicon-remove")
    ).not.toBeVisible();
  });
});
