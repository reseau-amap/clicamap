import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Suppression d'un référent produit", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Supprimer un référent produit à l'AMAP`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await clickMenuLinkOnLine(
      page,
      "amapienref@test.amap-aura.org",
      "Supprimer le référent"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le référent a été supprimé avec succès."
    );
    await expect(page.locator(".container table")).toHaveCount(0);
  });
});
