import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  smockerMockAmapAura58rueraulin,
  smockerReset,
} from "../../support/shorcut_smocker";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Ajouter une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await smockerReset();
  });

  test(`Valeurs par défaut du formulaire d'ajout de ferme`, async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .getByText(/Créer une nouvelle ferme/)
      .first()
      .click();
    await expect(page).toHaveURL(/\/ferme\/informations_generales_creation/);
    await expect(
      page.locator('select[name="ferme[regroupement]"]')
    ).toHaveValue("");
    await expect(page.locator('input[name="ferme[nom]"]')).toHaveValue("");
    await expect(page.locator('input[name="ferme[siret]"]')).toHaveValue("");
    await expect(page.locator('input[name="ferme[ordreCheque]"]')).toHaveValue(
      ""
    );
    await expect(
      page.locator('textarea[name="ferme[description]"]')
    ).toHaveValue("");
    await expect(page.locator('input[name="ferme[libAdr]"]')).toHaveValue("");
    await expect(
      page
        .locator(".radio")
        .filter({ has: page.getByText("Oui") })
        .locator('[name="ferme[tva]"]')
    ).toBeChecked();
    await expect(page.locator('input[name="ferme[tvaTaux]"]')).toHaveValue("");
    await expect(page.locator('input[name="ferme[tvaDate]"]')).toHaveValue("");
    await expect(
      page.locator('select[name="ferme[certification]"]')
    ).toHaveValue("0");
    await expect(page.locator('input[name="ferme[url]"]')).toHaveValue("");
    await expect(
      page.locator('input[name="ferme[dateInstallation]"]')
    ).toHaveValue("");
    await expect(
      page.locator('input[name="ferme[anneeDebCommercialisationAmap]"]')
    ).toHaveValue("");
    await expect(page.locator('input[name="ferme[msa]"]')).toHaveValue("");
    await expect(page.locator('input[name="ferme[spg]"]')).toHaveValue("");
  });

  const testErrorDb = [
    [
      "admin",
      " ",
      "12345678901234",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin",
      "Ferme",
      " ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin",
      "Ferme",
      " 1",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      'Le champ "SIRET" doit contenir exactement 14 caractères.',
    ],

    [
      "admin",
      "Ferme",
      "12345678901234",
      " ",
      " ",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin",
      "Ferme",
      " 11111111111111 ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Ce SIRET existe déjà dans la base de donnée",
    ],

    [
      "admin_aura",
      "Ferme",
      " ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin_aura",
      "Ferme",
      " 1",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      'Le champ "SIRET" doit contenir exactement 14 caractères.',
    ],

    [
      "admin_aura",
      "Ferme",
      "12345678901234",
      " ",
      " ",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin_aura",
      "Ferme",
      " 11111111111111 ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Ce SIRET existe déjà dans la base de donnée",
    ],

    [
      "amap",
      " ",
      "12345678901234",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "amap",
      "Ferme",
      " ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "amap",
      "Ferme",
      " 1",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      'Le champ "SIRET" doit contenir exactement 14 caractères.',
    ],

    [
      "amap",
      "Ferme",
      "12345678901234",
      " ",
      " ",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "amap",
      "Ferme",
      " 11111111111111 ",
      "58 rue Raulin, 69007 Lyon",
      "ul.ui-autocomplete li",
      "Ce SIRET existe déjà dans la base de donnée",
    ],
  ];

  testErrorDb.forEach((row, i) => {
    test(`Tester les messages d'erreur, tous les profils ${row[0]} ${i}`, async ({
      page,
    }) => {
      await smockerMockAmapAura58rueraulin();
      await login(page, row[0]);
      await page.goto("/ferme");
      await page
        .getByText(/Créer une nouvelle ferme/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/ferme\/informations_generales_creation/);
      await page.locator('input[name="ferme[nom]"]').fill(row[1]);
      await page.locator('input[name="ferme[siret]"]').fill(row[2]);
      await page.locator('input[name="f_autocomplete"]').fill(row[3]);
      if (row[4] !== " ") {
        await page.locator(row[4]).first().click();
      }
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(
        page.locator(".alert-danger").getByText(row[5])
      ).not.toHaveCount(0);
    });
  });

  const testValidDb = [["admin"], ["admin_aura"], ["amap"]];

  testValidDb.forEach((row) => {
    test(`Intégration réussie de la ferme, tous profils amapiens ${row[0]}`, async ({
      page,
    }) => {
      await smockerMockAmapAura58rueraulin();
      await login(page, row[0]);
      await page.goto("/ferme");
      await page.getByText(/Créer une nouvelle ferme/).click();
      await currentPathShouldBe(page, "/ferme/informations_generales_creation");
      await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
      await page
        .locator('input[name="ferme[ordreCheque]"]')
        .fill("ordre cheque");
      await page.locator('input[name="ferme[siret]"]').fill("12345678901234");
      await page
        .locator('input[name="f_autocomplete"]')
        .fill("58 rue Raulin, 69007 Lyon");
      await page.locator("ul.ui-autocomplete li").first().click();
      await page.getByRole("button", { name: "Sauvegarder" }).click();
      await flashMessageShouldContain(
        page,
        "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
      );
      await currentPathShouldBe(
        page,
        "/paysan/informations_generales_creation"
      );
    });
  });

  test(`Intégration réussie de la ferme, comme regroupement`, async ({
    page,
  }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "regroupement");
    await clickMainMenuLink(page, "Gestionnaire regroupement Regroupement");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page.getByText(/Créer une nouvelle ferme/).click();
    await currentPathShouldBe(page, "/ferme/informations_generales_creation");
    await expect(
      page.locator('[name="ferme[regroupement]"] option')
    ).toHaveCount(1);
    await expect(
      page.locator('[name="ferme[regroupement]"] option')
    ).toHaveText("Regroupement");

    await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
    await page.locator('input[name="ferme[ordreCheque]"]').fill("ordre cheque");
    await page.locator('input[name="ferme[siret]"]').clear();
    await page
      .locator('input[name="f_autocomplete"]')
      .fill("58 rue Raulin, 69007 Lyon");
    await page.locator("ul.ui-autocomplete li").first().click();
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(
      page,
      "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
    );
    await currentPathShouldBe(page, "/paysan/informations_generales_creation");
    await clickMainMenuLink(page, "Gestionnaire regroupement Regroupement");
    await clickMainMenuLink(page, "Gestion des fermes");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) th:nth-child(1)")
    ).toContainText("FermeTest");
  });

  const testRegroupementDb = [["admin"], ["admin_aura"]];
  testRegroupementDb.forEach((row) => {
    test(`Ajouter le regroupement à l'édition d'une ferme pour les super admin et les admins région ${row[0]}`, async ({
      page,
    }) => {
      await login(page, row[0]);
      await page.goto("/ferme");
      await page
        .getByText(/Créer une nouvelle ferme/)
        .first()
        .click();
      await expect(page.locator("form")).toContainText("Regroupement");
    });
  });

  const testRegroupementMasqueDb = [
    "admin_rhone",
    "amapienref",
    "amap",
    "paysan",
  ];

  testRegroupementMasqueDb.forEach((user) => {
    test(`Masquer le champ regroupement pour les utilisateurs non admin ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page.goto("/ferme/informations_generales/1");
      await expect(page.locator("form")).not.toContainText("Regroupement");
    });
  });

  test(`Retour sur l'ajout d'une nouvelle ferme après j'ajout d'une première`, async ({
    page,
  }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "admin");
    await page.goto("/ferme/informations_generales_creation");
    await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
    await page.locator('input[name="ferme[siret]"]').clear();
    await page
      .locator('input[name="f_autocomplete"]')
      .fill("58 rue Raulin, 69007 Lyon");
    await page.locator("ul.ui-autocomplete li").first().click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await page.goto("/ferme/informations_generales_creation");
    await expect(page.locator('input[name="ferme[nom]"]')).toHaveValue("");
  });

  test(`Annulation de l'ajout du paysan après ajout de la ferme - amap`, async ({
    page,
  }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "amap");
    await page.goto("/ferme");
    await page.getByText(/Créer une nouvelle ferme/).click();
    await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
    await page.locator('input[name="ferme[ordreCheque]"]').fill("ordre cheque");
    await page.locator('input[name="ferme[siret]"]').fill("12345678901234");
    await page
      .locator('input[name="f_autocomplete"]')
      .fill("58 rue Raulin, 69007 Lyon");
    await page.locator("ul.ui-autocomplete li").first().click();
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(
      page,
      "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
    );
    await page.getByRole("link", { name: "Annuler" }).click();
    await currentPathShouldBe(page, "/ferme");
    await expect(page.locator("h3")).toHaveText("Gestion des fermes");
  });

  test(`Annulation de l'ajout du paysan après ajout de la ferme - regroupement`, async ({
    page,
  }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "regroupement");
    await clickMainMenuLink(page, "Gestionnaire regroupement Regroupement");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page.getByText(/Créer une nouvelle ferme/).click();
    await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
    await page.locator('input[name="ferme[ordreCheque]"]').fill("ordre cheque");
    await page
      .locator('input[name="f_autocomplete"]')
      .fill("58 rue Raulin, 69007 Lyon");
    await page.locator("ul.ui-autocomplete li").first().click();
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(
      page,
      "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
    );
    await page.getByRole("link", { name: "Annuler" }).click();
    await currentPathShouldBe(page, "/ferme/home_regroupement/1");
    await expect(page.locator("h3")).toHaveText("Gestion des fermes");
  });

  test(`Pré selection de la ferme lors de l'ajout du paysan après ajout de la ferme`, async ({
    page,
  }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "amap");
    await page.goto("/ferme");
    await page.getByText(/Créer une nouvelle ferme/).click();
    await page.locator('input[name="ferme[nom]"]').fill("FermeTest");
    await page.locator('input[name="ferme[ordreCheque]"]').fill("ordre cheque");
    await page.locator('input[name="ferme[siret]"]').fill("12345678901234");
    await page
      .locator('input[name="f_autocomplete"]')
      .fill("58 rue Raulin, 69007 Lyon");
    await page.locator("ul.ui-autocomplete li").first().click();
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(
      page,
      "Vous venez de créer une ferme. Vous pouvez d'ores et déjà lui ajouter un ou plusieurs paysans."
    );
    await expect(
      page.locator('select[name="paysan_creation[ferme]"] option[selected]')
    ).toHaveText("FermeTest");
  });
});
