import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  smockerMockAmapAura58rueraulin,
  smockerReset,
} from "../../support/shorcut_smocker";

test.describe("Ajouter un paysan à une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await smockerReset();
  });

  test(`Email invalide`, async ({ page }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Paysans de la ferme");
    await page.locator('[name="user_verification[user]"]').fill("invalid");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Aucun utilisateur trouvé avec cet email ou ce nom d'utilisateur."
    );
  });

  test(`Email valide utilisateur non paysan`, async ({ page }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Paysans de la ferme");
    await page.locator('[name="user_verification[user]"]').fill("amapien");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le paysan a été ajouté avec succès."
    );
    await expect(page.locator(".container table")).toHaveText(/NOM AMAPIEN/);
  });

  test(`Email valide utilisateur paysan`, async ({ page }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Paysans de la ferme");
    await page.locator('[name="user_verification[user]"]').fill("paysan2");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le paysan a été ajouté avec succès."
    );
    await expect(page.locator(".container table")).toHaveText(/PAYSAN_NOM2/);
  });

  test(`Email valide utilisateur paysan même ferme`, async ({ page }) => {
    await smockerMockAmapAura58rueraulin();
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Paysans de la ferme");
    await page.locator('[name="user_verification[user]"]').fill("paysan");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le paysan est déjà lié à cette ferme."
    );
  });
});
