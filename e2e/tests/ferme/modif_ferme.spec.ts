import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Modifier une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const messageDb = [
    ["Ferme test", " ", "ordre cheque", "Cette valeur ne doit pas être vide."],
    [
      " ",
      "11111111111111",
      "ordre cheque",
      "Cette valeur ne doit pas être vide.",
    ],
    [
      "Ferme test",
      "1",
      "ordre cheque",
      'Le champ "SIRET" doit contenir exactement 14 caractères.',
    ],

    [
      "Ferme test",
      "111111111111111",
      "ordre cheque",
      'Le champ "SIRET" doit contenir exactement 14 caractères.',
    ],

    [
      "Ferme test",
      "22222222222222",
      "ordre cheque",
      "Ce SIRET existe déjà dans la base de donnée.",
    ],

    ["Ferme test", "11111111111111", "", "Cette valeur ne doit pas être vide."],
    [
      "Ferme test",
      "11111111111111",
      "ordre cheque",
      "La ferme a été modifiée avec succès.",
    ],
  ];

  messageDb.forEach((row, i) => {
    test(`Tester les messages d'erreur avec le profil AMAPien référent ${i}`, async ({
      page,
    }) => {
      await login(page, "amapienref");
      await page
        .getByText(/Gestionnaire référent/)
        .first()
        .click();
      await page
        .getByText(/Gestion des fermes/)
        .first()
        .click();
      await clickMenuLinkOnLine(page, "Ferme", "Modifier la ferme");
      await page.locator('input[name="ferme[nom]"]').clear();
      await page.locator('input[name="ferme[nom]"]').fill(row[0]);
      await page.locator('input[name="ferme[siret]"]').clear();
      await page.locator('input[name="ferme[siret]"]').fill(row[1]);
      await page.locator('input[name="ferme[ordreCheque]"]').clear();
      await page.locator('input[name="ferme[ordreCheque]"]').fill(row[2]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator("body")).toContainText(row[3]);
    });
  });
  messageDb.forEach((row, i) => {
    test(`Tester les messages d'erreur avec le profil paysan ${i}`, async ({
      page,
    }) => {
      await login(page, "paysan");
      await page
        .getByText(/Gestionnaire ferme/)
        .first()
        .click();
      await page
        .getByText(/Ma ferme/)
        .first()
        .click();
      await clickMenuLinkOnLine(page, "ferme", "Modifier la ferme");
      await page.locator('input[name="ferme[nom]"]').clear();
      await page.locator('input[name="ferme[nom]"]').fill(row[0]);
      await page.locator('input[name="ferme[siret]"]').clear();
      await page.locator('input[name="ferme[siret]"]').fill(row[1]);
      await page.locator('input[name="ferme[ordreCheque]"]').clear();
      await page.locator('input[name="ferme[ordreCheque]"]').fill(row[2]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator("body")).toContainText(row[3]);
    });
  });
  ["admin", "admin_aura", "admin_rhone"].forEach((user) => {
    messageDb.forEach((row, i) => {
      test(`Tester les messages avec les profils Réseau, super admin et AMAP ${user} - ${i}`, async ({
        page,
      }) => {
        await login(page, user);
        await page.goto("/ferme");
        await page
          .locator('[name="search_ferme[region]"]')
          .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
        await clickMenuLinkOnLine(page, "Ferme", "Modifier la ferme");
        await page.locator('input[name="ferme[nom]"]').clear();
        await page.locator('input[name="ferme[nom]"]').fill(row[0]);
        await page.locator('input[name="ferme[siret]"]').clear();
        await page.locator('input[name="ferme[siret]"]').fill(row[1]);
        await page.locator('input[name="ferme[ordreCheque]"]').clear();
        await page.locator('input[name="ferme[ordreCheque]"]').fill(row[2]);
        await page
          .getByText(/Sauvegarder/)
          .first()
          .click();
        await expect(page.locator("body")).toContainText(row[3]);
      });
    });
  });

  ["admin", "admin_aura"].forEach((user) => {
    test(`Modifier le regroupement à l'édition d'une ferme pour les super admin et les admins région ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page.goto("/ferme/informations_generales/1");
      await page
        .locator('[name="ferme[regroupement]"]')
        .selectOption({ label: "Regroupement" });
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await page.goto("/ferme_regroupement");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) th:nth-child(1)")
      ).toHaveText("Regroupement");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
      ).toHaveText("ferme");
    });
  });

  const nonAdminDb = ["admin_rhone", "amapienref", "amap", "paysan"];
  nonAdminDb.forEach((user) => {
    test(`Masquer le champ regroupement pour les utilisateurs non admin ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page.goto("/ferme/informations_generales/1");
      await expect(
        page.locator('[name="ferme[regroupement]"]')
      ).not.toBeVisible();
    });
  });
});
