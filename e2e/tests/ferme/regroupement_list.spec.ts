import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { fermeAssocieeRegroupement } from "../../support/shorcut";

test.describe("Liste des fermes associée au regroupement", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Liste des fermes associée au regroupement`, async ({ page }) => {
    await fermeAssocieeRegroupement(page, "Ferme", "Regroupement");
    await login(page, "regroupement");
    await page
      .getByText(/Gestionnaire regroupement/)
      .first()
      .click();
    await page
      .getByText(/Gestion des fermes/)
      .first()
      .click();
    await expect(page.locator(".container table")).toContainText(
      "Paysan_prenom PAYSAN_NOM"
    );
  });
});
