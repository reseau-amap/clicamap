import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { setAmapienAdmin } from "../../support/shorcut";

test.describe("Ajout d'un référent produit", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Ajouter un référent produit à l'AMAP - amap`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await page
      .locator('[name="ferme_ref_produit_amapien[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent a été ajouté avec succès"
    );
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
  });

  test(`Ajouter un référent produit à l'AMAP - multiples amap`, async ({
    page,
  }) => {
    await setAmapienAdmin(page, "amap", "amap2@test.amap-aura.org");
    await login(page, "amap");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await page
      .locator('[name="ferme_ref_produit[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="ferme_ref_produit_amapien[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent a été ajouté avec succès"
    );
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
  });

  test(`Ajouter un référent produit à l'AMAP - admin`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await page
      .locator('[name="ferme_ref_produit[regionDepartement][region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await page
      .locator('[name="ferme_ref_produit[regionDepartement][departement]"]')
      .selectOption({ label: "RHÔNE" });
    await page
      .locator('[name="ferme_ref_produit[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="ferme_ref_produit_amapien[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent a été ajouté avec succès"
    );
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
  });

  test(`Masquer l'ajout de référent produit déjà référent`, async ({
    page,
  }) => {
    await login(page, "amap");
    await page.goto("/ferme/");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Référents produits de la ferme");
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).not.toContainText("Prénom amapien référent NOM AMAPIEN RÉFÉRENT");
    await page.goto("/ferme/");
    await clickMenuLinkOnLine(
      page,
      "Ferme 2",
      "Référents produits de la ferme"
    );
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prenom amapien NOM AMAPIEN");
    await expect(
      page.locator('[name="ferme_ref_produit_amapien[amapien]"]')
    ).toContainText("Prénom amapien référent NOM AMAPIEN RÉFÉRENT");
  });
});
