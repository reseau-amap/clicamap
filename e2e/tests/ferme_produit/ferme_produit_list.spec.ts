import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  checkCheckbox,
  clickMenuLinkOnLine,
} from "../../support/shorcut_actions";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMainMenuLink, disableProduct } from "../../support/shorcut";

test.describe("Lister les produits existant en tant que paysan", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`La liste des produits comporte 2 produits`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody tr:nth-child(1) th:nth-child(2)")
    ).toContainText("Produit 1");
    await expect(
      page.locator(".container table tbody tr:nth-child(2) th:nth-child(2)")
    ).toContainText("Produit 2");
  });

  test(`La liste des produits comporte le nom, le type, le conditionnement, le prix, la TVA`, async ({
    page,
  }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await expect(
      page.locator(".container table thead tr th:nth-child(1)")
    ).toContainText("Type de Production");
    await expect(
      page.locator(".container table thead tr th:nth-child(2)")
    ).toContainText("Nom");
    await expect(
      page.locator(".container table thead tr th:nth-child(3)")
    ).toContainText("Conditionnement");
    await expect(
      page.locator(".container table thead tr th:nth-child(4)")
    ).toContainText("Prix TTC");
    await expect(
      page.locator(".container table thead tr th:nth-child(5)")
    ).toContainText("TVA");
    await expect(
      page.locator(".container table thead tr th:nth-child(6)")
    ).toContainText("Label");
    await expect(
      page.locator(".container table thead tr th:nth-child(7)")
    ).toContainText("Régularisationde poids ?");
    await expect(
      page.locator(".container table thead tr th:nth-child(8)")
    ).toContainText("Actif ?");
  });

  test(`La liste des produits affiche le taux de TVA`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await expect(
      page.locator(".container table thead th:nth-child(5)")
    ).toContainText("TVA");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(5)")
    ).toContainText("5,5 %");
  });

  test(`Modifiation des permissions pour les produits inactifs - amapien référent`, async ({
    page,
  }) => {
    await disableProduct(page, "paysan", "Produit 1");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toContainText("Non");
    await expect(
      page.locator(".container table tbody tr:nth-child(2) td:nth-child(8)")
    ).toContainText("Oui");

    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeEnabled();

    await login(page, "amapienref");
    await clickMainMenuLink(page, "Gestionnaire référent");
    await clickMainMenuLink(page, "Gestion des fermes");
    await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toContainText("Non");
    await expect(
      page.locator(".container table tbody tr:nth-child(2) td:nth-child(8)")
    ).toContainText("Oui");

    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeDisabled();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(2) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeEnabled();
  });

  test(`Modifiation des permissions pour les produits inactifs - AMAP`, async ({
    page,
  }) => {
    await disableProduct(page, "paysan", "Produit 1");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toContainText("Non");
    await expect(
      page.locator(".container table tbody tr:nth-child(2) td:nth-child(8)")
    ).toContainText("Oui");

    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeEnabled();

    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toContainText("Non");
    await expect(
      page.locator(".container table tbody tr:nth-child(2) td:nth-child(8)")
    ).toContainText("Oui");

    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeDisabled();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(2) td:nth-child(9) button.dropdown-toggle"
      )
    ).toBeEnabled();
  });
});
