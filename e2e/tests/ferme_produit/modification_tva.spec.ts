import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("En tant que paysan, je doit pouvoir modifier le taux de TVA de mes produits", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Liste des options comme assujetti`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("Aucune TVA");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("0 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("5,5 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("10 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("20 %");
  });

  test(`Liste des options comme non assujetti`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/ferme");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Modifier la ferme");
    await page
      .locator('[name="ferme[tva]"]')
      .locator('input[value="0"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "La ferme a été modifiée avec succès."
    );
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).toContainText("Aucune TVA");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).not.toContainText("0 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).not.toContainText("5,5 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).not.toContainText("10 %");
    await expect(
      page.locator('[name="ferme_produit[prix][tva]"]')
    ).not.toContainText("20 %");
  });

  test(`Modification du prix TTC`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await page.locator('[name="ferme_produit[prix][prixTTC]"]').clear();
    await page.locator('[name="ferme_produit[prix][prixTTC]"]').fill("10");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Produit modifié avec succès.");
    await expect(page.locator(".container table")).toContainText("10,00");
  });

  test(`Modification du taux de TVA`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await page
      .locator('[name="ferme_produit[prix][tva]"]')
      .selectOption({ label: "10 %" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Produit modifié avec succès.");
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(page.locator('[name="ferme_produit[prix][tva]"]')).toHaveValue(
      "10"
    );
  });
});
