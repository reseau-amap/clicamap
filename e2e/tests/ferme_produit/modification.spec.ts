import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Modification d'un produit de ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant que référent produit, je ne doit pas pouvoir changer le status d'un produit actif`, async ({
    page,
  }) => {
    await login(page, "amapienref");
    await clickMainMenuLink(page, "Gestionnaire référent");
    await clickMainMenuLink(page, "Gestion des fermes");
    await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(
      page.locator('[name="ferme_produit[enabled]"]')
    ).toBeDisabled();
  });

  test(`En tant qu'AMAP', je ne doit pas pouvoir changer le status d'un produit actif`, async ({
    page,
  }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(
      page.locator('[name="ferme_produit[enabled]"]')
    ).toBeDisabled();
  });

  test(`En tant qu'admin, je doit pouvoir changer le status d'un produit actif`, async ({
    page,
  }) => {
    await login(page, "admin_aura");
    await clickMainMenuLink(page, "Gestionnaire Admin");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await expect(page.locator('[name="ferme_produit[enabled]"]')).toBeEnabled();
  });
});
