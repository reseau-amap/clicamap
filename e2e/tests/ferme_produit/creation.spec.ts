import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { clickMainMenuLink } from "../../support/shorcut";

const addProductAndCheck = async (page: Page) => {
  await clickMenuLinkOnLine(page, "Ferme", "Produits proposés");

  await page.getByRole("link", { name: "Ajouter un nouveau produit" }).click();
  await expect(page.locator('[name="ferme_produit[enabled]"]')).toBeDisabled();
  await page
    .locator('[name="ferme_produit[typeProduction]"]')
    .selectOption("Autres / divers");
  await page.locator('[name="ferme_produit[nom]"]').fill("test ajout");
  await page
    .locator('[name="ferme_produit[conditionnement]"]')
    .fill("test conditionnement");
  await page.getByRole("button", { name: "Sauvegarder" }).click();
  await flashMessageShouldContain(page, "Produit ajouté avec succès.");

  await expect(
    page.locator(".container table tbody tr:nth-child(2) th:nth-child(2)")
  ).toContainText("Test ajout");
  await expect(
    page.locator(".container table tbody tr:nth-child(2) td:nth-child(8)")
  ).toContainText("Oui");
};

test.describe("Ajout d'un produit de ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant que référent produit, je ne doit pouvoir ajouter que des produits actifs`, async ({
    page,
  }) => {
    await login(page, "amapienref");
    await clickMainMenuLink(page, "Gestionnaire référent");
    await clickMainMenuLink(page, "Gestion des fermes");

    await addProductAndCheck(page);
  });

  test(`En tant qu'AMAP', je ne doit pouvoir ajouter que des produits actifs`, async ({
    page,
  }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des fermes");
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await addProductAndCheck(page);
  });
});
