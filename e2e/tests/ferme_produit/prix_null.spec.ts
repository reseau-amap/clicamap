import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("En tant que paysan, je ne doit pas pouvoir créer un produit sans prix", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Modification du prix TTC`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await page.locator('[name="ferme_produit[prix][prixTTC]"]').clear();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Le prix est obligatoire"
    );
  });
});
