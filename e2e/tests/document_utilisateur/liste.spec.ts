import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";

import { creerDocumentUtilisateur } from "../../support/shorcut";

test.describe("Comme administrateur, devrais pouvoir ajouter un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant qu'admin, je ne doit voir aucun document par défaut`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "AMAP : amap test"
    );
    await page
      .getByText(/Communication/)
      .first()
      .click();
    await page
      .getByText(/Gestion des documents/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun résultat pour cette recherche\./
    );
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je doit filter par défaut par ma région`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();
      await expect(page.locator(".alert-warning")).toHaveText(
        /1 résultat pour cette recherche\./
      );
    });
  });

  test(`En tant que admin, je devrais pouvoir utiliser le filtre de recherche`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "AMAP : amap test"
    );
    await page
      .getByText(/Communication/)
      .first()
      .click();
    await page
      .getByText(/Gestion des documents/)
      .first()
      .click();
    await page
      .locator('[name="search_document_utilisateur_form[year]"]')
      .selectOption({ label: "2019" });
    await expect(page.locator(".alert-warning")).toHaveText(
      /1 résultat pour cette recherche\./
    );
    await page
      .locator('[name="search_document_utilisateur_form[year]"]')
      .selectOption({ label: "2020" });
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun résultat pour cette recherche\./
    );
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir utiliser le filtre de recherche`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2020" });
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await expect(page.locator(".alert-warning")).toHaveText(
        /1 résultat pour cette recherche\./
      );
    });

    test(`Activer/Desactiver les boutons du header en fonction de la sélection ${role}`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();

      const buttonsShould = async (expectDisabled: boolean) => {
        const selectors = [
          'button[name="action-download"]',
          'button[name="action-delete"]',
          'button[name="action-notify"]',
          'button[data-target="#modal_email"]',
        ];

        for (const selector of selectors) {
          if (expectDisabled) {
            await expect(page.locator(selector)).toBeDisabled();
          } else {
            await expect(page.locator(selector)).toBeEnabled();
          }
        }
      };

      await buttonsShould(true);
      await page.locator(".js-tableselect-all").click();
      await buttonsShould(false);
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await buttonsShould(true);
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await buttonsShould(false);
    });
  });
});
