import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { select2 } from "../../support/shorcut_actions";

test.describe("Comme administrateur, devrais pouvoir ajouter un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir ajouter un document lié à une amap`, async ({
      page,
    }) => {
      await login(page, role);
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();
      await page
        .getByText(/Ajouter/)
        .first()
        .click();
      await page
        .locator('[name="document_utilisateur[annee]"]')
        .selectOption({ label: "2019" });
      await page
        .locator('[name="document_utilisateur[nom]"]')
        .fill("nom du document");
      await select2(page, "document_utilisateur[target]", "AMAP : amap test");
      await page
        .locator('input[name="document_utilisateur[file]"]')
        .setInputFiles("e2e/data/test.pdf");
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await flashMessageShouldContain(page, "Le document a bien été créé.");
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
      ).toHaveText("2019");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(3)")
      ).toHaveText("nom du document");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(4)")
      ).toHaveText("amap test");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(5)")
      ).toHaveText("AMAP_NOM Amap_prenom");
    });

    test(`En tant que ${role}, je devrais pouvoir ajouter un document lié à une ferme`, async ({
      page,
    }) => {
      await login(page, role);
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();
      await page
        .getByText(/Ajouter/)
        .first()
        .click();
      await page
        .locator('[name="document_utilisateur[annee]"]')
        .selectOption({ label: "2019" });
      await page
        .locator('[name="document_utilisateur[nom]"]')
        .fill("nom du document");
      await select2(page, "document_utilisateur[target]", "Ferme : ferme");
      await page
        .locator('input[name="document_utilisateur[file]"]')
        .setInputFiles("e2e/data/test.pdf");
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await flashMessageShouldContain(page, "Le document a bien été créé.");
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
      ).toHaveText("2019");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(3)")
      ).toHaveText("nom du document");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(4)")
      ).toHaveText("ferme");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(5)")
      ).toHaveText("PAYSAN_NOM Paysan_prenom");
    });
  });
});
