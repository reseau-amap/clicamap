import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { creerDocumentUtilisateur } from "../../support/shorcut";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Comme administrateur, devrais pouvoir téléchargr un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await mailClean();
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir notifier un document amap depuis le menu`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Les notifications ont bien été envoyées."
      );
      await expectMailShoudBeReceived(
        "Un nouveau document est disponible sur votre profil clic'AMAP",
        ["amap@test.amap-aura.org"]
      );
    });

    test(`En tant que ${role}, je devrais pouvoir notifier un document ferme depuis le menu`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "Ferme : ferme"
      );
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Les notifications ont bien été envoyées."
      );
      await expectMailShoudBeReceived(
        "Un nouveau document est disponible sur votre profil clic'AMAP",
        ["paysan@test.amap-aura.org"]
      );
    });
  });
});
