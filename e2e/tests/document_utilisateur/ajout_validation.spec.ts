import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { writeFile } from "../../support/shorcut_file";

test.describe("Comme administrateur, je ne devrais pas pouvoir indiquer d'information invalide lors de l'ajout", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);

    await login(page, "admin");
    await page
      .getByText(/Communication/)
      .first()
      .click();
    await page
      .getByText(/Gestion des documents/)
      .first()
      .click();
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
  });

  test("Vérification des champs obligatoires", async ({ page }) => {
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(
      page.locator('[name="document_utilisateur[annee]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être nulle.");
    await expect(
      page.locator('[name="document_utilisateur[nom]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page.locator('[name="document_utilisateur[target]"] ~ .alert-danger')
    ).toContainText("Vous devez sélectionner une AMAP ou une ferme.");
    await expect(page.locator(".input-group ~ .alert-danger")).toContainText(
      "Vous devez sélectionner un fichier."
    );
  });

  test("Vérification taille maximale", async ({ page }) => {
    writeFile("/tmp/test11mb.dat", Buffer.alloc(10 * 1024 * 1024 + 1, "0"));
    await page
      .locator('input[name="document_utilisateur[file]"]')
      .setInputFiles("/tmp/test11mb.dat");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".input-group ~ .alert-danger")).toContainText(
      "Le fichier que vous avez sélectionné est trop volumineux. La taille maximale autorisée est de 10 Mo."
    );
  });
});
