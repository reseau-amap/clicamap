import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";

import { creerDocumentUtilisateur } from "../../support/shorcut";

test.describe("Comme administrateur, devrais pouvoir ajouter un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Masquer le bouton liste si aucun document affiché`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "AMAP : amap test"
    );
    await expect(page.getByText(/Emails/).first()).not.toBeVisible();
    await page
      .locator('[name="search_document_utilisateur_form[year]"]')
      .selectOption({ label: "2019" });
    await expect(page.getByText(/Emails/).first()).toBeVisible();
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir utiliser le filtre de recherche`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "Ferme : ferme"
      );
      await page
        .getByText(/Communication/)
        .first()
        .click();
      await page
        .getByText(/Gestion des documents/)
        .first()
        .click();
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await page
        .getByText(/Emails/)
        .first()
        .click();
      await expect(page.locator(".modal.in textarea")).toHaveValue(
        "amap@test.amap-aura.org"
      );
      await page.locator('.modal.in button[data-dismiss="modal"]').click();
      await page
        .locator('input[type="checkbox"][data-target-name="selected[2]"]')
        .click();
      await page
        .getByText(/Emails/)
        .first()
        .click();
      await expect(page.locator(".modal.in textarea")).toHaveValue(
        "amap@test.amap-aura.org\npaysan@test.amap-aura.org"
      );
      await page.locator('.modal.in button[data-dismiss="modal"]').click();
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await page
        .getByText(/Emails/)
        .first()
        .click();
      await expect(page.locator(".modal.in textarea")).toHaveValue(
        "paysan@test.amap-aura.org"
      );
    });
  });
});
