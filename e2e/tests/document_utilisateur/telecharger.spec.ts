import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";

import { creerDocumentUtilisateur } from "../../support/shorcut";
import { readPdf } from "../../support/shorcut_pdf";

const expectDownload = async (page: Page) => {
  await page
    .locator('[name="search_document_utilisateur_form[year]"]')
    .selectOption({ label: "2019" });
  await page
    .locator(
      ".container table.table-datatable tbody tr:nth-child(1) .btn-group-action button"
    )
    .click();
  const text = await readPdf(
    page,
    page
      .locator(
        ".container table.table-datatable tbody tr:nth-child(1) .dropdown-menu-right a"
      )
      .getByText(/Télécharger/)
  );
  expect(text).toContain(`Test de pdf`);
};

test.describe("Comme administrateur, devrais pouvoir télécharger un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir télécharger un document amap depuis le menu`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await expectDownload(page);
    });

    test(`En tant que ${role}, je devrais pouvoir télécharger un document ferme depuis le menu`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "Ferme : ferme"
      );
      await expectDownload(page);
    });
  });
});
