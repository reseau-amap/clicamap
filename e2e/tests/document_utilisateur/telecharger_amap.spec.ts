import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { creerDocumentUtilisateur } from "../../support/shorcut";
import { readPdf } from "../../support/shorcut_pdf";

test.describe("Comme amap, devrais pouvoir téléchargr un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant qu'amap, je devrais pouvoir télécharger un document amap depuis le menu`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "AMAP : amap test"
    );
    await login(page, "amap");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Autres documents/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody tr:nth-child(1)")
    ).toHaveText(/document test/);
    await page
      .locator(
        ".container table.table-datatable tbody tr:nth-child(1) .btn-group-action button"
      )
      .click();
    const text = await readPdf(
      page,
      page
        .locator(
          ".container table.table-datatable tbody tr:nth-child(1) .dropdown-menu-right a"
        )
        .getByText(/Télécharger le document/)
    );
    expect(text).toContain(`Test de pdf`);
  });

  test(`En tant que paysan, je devrais pouvoir télécharger un document ferme depuis le menu`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "Ferme : ferme"
    );
    await login(page, "paysan");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Autres documents/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody tr:nth-child(1)")
    ).toHaveText(/document test/);
    await page
      .locator(
        ".container table.table-datatable tr:nth-child(1) .btn-group-action button"
      )
      .click();
    const text = await readPdf(
      page,
      page
        .locator(
          ".container table.table-datatable tr:nth-child(1) .dropdown-menu-right a"
        )
        .getByText(/Télécharger le document/)
    );
    expect(text).toContain(`Test de pdf`);
  });
});
