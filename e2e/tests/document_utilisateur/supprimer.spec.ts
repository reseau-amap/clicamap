import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { creerDocumentUtilisateur } from "../../support/shorcut";

test.describe("Comme administrateur, devrais pouvoir supprimer un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir supprimer depuis le menu`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await clickMenuLinkOnLine(page, "document test", "Supprimer le document");
      await page.locator(".modal.in").getByText(/OUI/).first().click();
      await flashMessageShouldContain(page, "Le document a bien été supprimé.");
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
    });
  });

  test(`Masquer le bouton suppression si aucun document affiché`, async ({
    page,
  }) => {
    await creerDocumentUtilisateur(
      page,
      "admin",
      "document test",
      "AMAP : amap test"
    );
    await expect(page.getByText(/Supprimer/).first()).not.toBeVisible();
    await page
      .locator('[name="search_document_utilisateur_form[year]"]')
      .selectOption({ label: "2019" });
    await expect(page.getByText(/Supprimer/).first()).toBeVisible();
  });

  ["admin", "admin_aura", "admin_rhone"].forEach((role) => {
    test(`En tant que ${role}, je devrais pouvoir supprimer depuis le header`, async ({
      page,
    }) => {
      await creerDocumentUtilisateur(
        page,
        role,
        "document test",
        "AMAP : amap test"
      );
      await page
        .locator('[name="search_document_utilisateur_form[year]"]')
        .selectOption({ label: "2019" });
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Supprimer/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Les documents ont bien été supprimé."
      );
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
    });
  });
});
