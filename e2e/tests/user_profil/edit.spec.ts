import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { admins } from "../../support/provider";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Comme utilisateur, je devrais pouvoir éditer depuis le menu", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Accès depuis le menu`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil");
    await page.locator('a[title="Éditer mon profil"]').click();
    await expect(page).toHaveURL(/\/user_profil\/edit/);
    await expect(page.locator("h3")).toHaveText(/Édition de mon profil/);
  });

  test(`Retours depuis le formulaire`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await expect(page).toHaveURL(/\/user_profil\/edit/);
    await page
      .getByText(/Annuler/)
      .first()
      .click();
    await expect(page).toHaveURL(/\/user_profil/);
  });

  test(`Prénom et prénom désactivé`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await expect(
      page.locator('input[name="user_profil[name][firstName]"]')
    ).toHaveAttribute("disabled", /.*/);
    await expect(
      page.locator('input[name="user_profil[name][lastName]"]')
    ).toHaveAttribute("disabled", /.*/);
  });
  test(`Email vide`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('button[title="Supprimer l\'adresse email"]').click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Votre profil a bien été mis à jour."
    );
    await expect(page.locator(".container table")).not.toContainText(
      "admin@test.amap-aura.org"
    );
  });
  test(`Email modifié vide`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('input[name="user_profil[emails][0][email]"]').clear();
    await page
      .locator('input[name="user_profil[emails][0][email]"]')
      .fill("admin2@test.amap-aura.org");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Votre profil a bien été mis à jour."
    );
    await expect(page.locator(".container table")).not.toContainText(
      "admin@test.amap-aura.org"
    );
    await expect(page.locator(".container table")).toContainText(
      "admin2@test.amap-aura.org"
    );
  });
  test(`Email existant`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('input[name="user_profil[emails][0][email]"]').clear();
    await page
      .locator('input[name="user_profil[emails][0][email]"]')
      .fill("admin_aura@test.amap-aura.org");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Un autre utilisateur utilise déjà cette adresse email\./
    );
  });
  test(`Plusieurs fois le même email`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator("#add_email").click();
    await page.locator('input[name="user_profil[emails][0][email]"]').clear();
    await page
      .locator('input[name="user_profil[emails][0][email]"]')
      .fill("newemail@test.amap-aura.org");
    await page
      .locator('input[name="user_profil[emails][1][email]"]')
      .fill("newemail@test.amap-aura.org");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(
      page.locator(
        'input[name="user_profil[emails][1][email]"] + .alert-danger'
      )
    ).toHaveText(/Impossible d'indiquer plusieurs fois la même adresse mail\./);
  });
  test(`Nom utilisateur vide`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('input[name="user_profil[username]"]').clear();
    await page.locator('input[name="user_profil[username]"]').fill(" ");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Cette valeur ne doit pas être vide/
    );
  });

  test(`Nom utilisateur est un email`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('input[name="user_profil[username]"]').clear();
    await page
      .locator('input[name="user_profil[username]"]')
      .fill("test@test.amap-aura.org");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Le nom d'utilisateur ne peut pas être un email/
    );
  });
  test(`Nom utilisateur existant`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/user_profil/edit");
    await page.locator('input[name="user_profil[username]"]').clear();
    await page.locator('input[name="user_profil[username]"]').fill("empty");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Cette valeur est déjà utilisée\./
    );
  });

  test(`Masquage des champs information adhésion pour les amapiens non admin`, async ({
    page,
  }) => {
    await login(page, "amapien");
    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Mon profil");
    await page.locator('a[title="Éditer mon profil"]').click();
    await expect(page.locator(".container")).not.toContainText("Nom du réseau");
  });

  admins.forEach((user) => {
    test(`Validation des champs information adhésion pour les amapiens admin ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await clickMainMenuLink(page, "Mon compte");
      await clickMainMenuLink(page, "Mon profil");
      await page.locator('a[title="Éditer mon profil"]').click();
      await expect(page.locator(".container")).toContainText("Nom du réseau");
      await page
        .locator('[name="user_profil[adhesionInfo][siret]"]')
        .fill("123");
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator(".alert-danger").nth(0)).toContainText(
        "Le SIRET doit faire exactement 14 caractères."
      );
    });
  });

  admins.forEach((user) => {
    test(`Sauvegarde des champs information adhésion pour les amapiens admin ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await clickMainMenuLink(page, "Mon compte");
      await clickMainMenuLink(page, "Mon profil");
      await page.locator('a[title="Éditer mon profil"]').click();
      await page
        .locator('[name="user_profil[adhesionInfo][nomReseau]"]')
        .fill("Nom");
      await page
        .locator('[name="user_profil[adhesionInfo][siret]"]')
        .fill("58932981186575");
      await page
        .locator('[name="user_profil[adhesionInfo][rna]"]')
        .fill("W123456789");
      await page
        .locator('[name="user_profil[adhesionInfo][villeSignature]"]')
        .fill("Ville");
      await page
        .locator('[name="user_profil[adhesionInfo][site]"]')
        .fill("http://www.amap-aura.org");
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Votre profil a bien été mis à jour."
      );
    });
  });
});
