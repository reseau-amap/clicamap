import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login, logout } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { allUsers } from "../../support/provider";

test.describe("Comme utilisateur, je devrais pouvoir modifier mon mot de passe", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  allUsers.forEach((user) => {
    test(`MDP edit - Accès par le menu comme ${user}`, async ({ page }) => {
      await login(page, user);
      await page
        .getByText(/Mon compte/)
        .first()
        .click();
      await page
        .getByText(/Mot de passe/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/user_profil\/mot_de_passe/);
      await expect(page.locator("h3")).toHaveText(/Nouveau mot de passe/);
    });
  });

  allUsers.forEach((user) => {
    test(`Modification de mot de passe ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/user_profil/mot_de_passe");
      await expect(page.locator("#progress-password-strength")).toHaveText("");
      await page
        .locator('[name="user_password[password][first]"]')
        .fill("r4gLZàKHA");
      await page
        .locator('[name="user_password[password][second]"]')
        .fill("r4gLZàKHA");
      await expect(page.locator("#progress-password-strength")).toHaveText(
        /3\/4/
      );
      await page
        .getByText(/Modifier le mot de passe/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Mot de passe modifié avec succès."
      );
      await expect(page).toHaveURL(/\/evenement/);
      await logout(page);
      await page.locator('[name="user_login[user]"]').fill(user);
      await page
        .locator('[name="user_login[plainPassword]"]')
        .fill("r4gLZàKHA");
      await page
        .getByText(/Connexion/)
        .first()
        .click();
      await expect(page.locator(".alert-success")).toContainText("Bonjour");
    });
  });

  allUsers.forEach((user) => {
    test(`Validation mot de passe trop faible ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/user_profil/mot_de_passe");
      await expect(page.locator("#progress-password-strength")).toHaveText("");
      await page
        .locator('[name="user_password[password][first]"]')
        .fill("weakaaaa");
      await page
        .locator('[name="user_password[password][second]"]')
        .fill("weakaaaa");
      await expect(page.locator("#progress-password-strength")).toHaveText(
        /1\/4/
      );
      await page
        .getByText(/Modifier le mot de passe/)
        .first()
        .click();
      await expect(page.locator(".alert-danger")).toContainText(
        "Mot de passe trop faible"
      );
    });
  });

  allUsers.forEach((user) => {
    test(`Validation mot de passe trop court ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/user_profil/mot_de_passe");
      await expect(page.locator("#progress-password-strength")).toHaveText("");
      await page.locator('[name="user_password[password][first]"]').fill("a");
      await page.locator('[name="user_password[password][second]"]').fill("a");
      await expect(page.locator("#progress-password-strength")).toHaveText(
        /0\/4/
      );
      await page
        .getByText(/Modifier le mot de passe/)
        .first()
        .click();
      await expect(page.locator(".alert-danger").nth(0)).toContainText(
        'Le champ "Mot de passe" doit contenir au moins 8 caractères.'
      );
    });
  });

  allUsers.forEach((user) => {
    test(`Validation mot de passe différents ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/user_profil/mot_de_passe");
      await expect(page.locator("#progress-password-strength")).toHaveText("");
      await page
        .locator('[name="user_password[password][first]"]')
        .fill("aaaaaaa1");
      await page
        .locator('[name="user_password[password][second]"]')
        .fill("aaaaaaa2");
      await expect(page.locator("#progress-password-strength")).toHaveText(
        /1\/4/
      );
      await page
        .getByText(/Modifier le mot de passe/)
        .first()
        .click();
      await expect(page.locator(".alert-danger")).toContainText(
        "Les deux mot de passe ne correspondent pas"
      );
    });
  });
});
