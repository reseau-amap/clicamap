import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { allUsers } from "../../support/provider";

test.describe("Comme utilisateur, je devrais pouvoir accéder à mon profil depuis le menu", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  allUsers.forEach((user) => {
    test(`Profil utilisateur - Accès par le menu comme ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page
        .getByText(/Mon compte/)
        .first()
        .click();
      await page
        .getByText(/Mon profil/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/user_profil/);
      await expect(page.locator("h3")).toHaveText(/Mon profil/);
    });
  });
});
