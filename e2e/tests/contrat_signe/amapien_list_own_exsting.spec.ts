import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  desactiverAmapien,
  subscribeContract,
  subscribeContractAmapienToValidate,
} from "../../support/shorcut";

test.describe("Lister les contrats existant en tant que amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Ne pas afficher l'amap associée aux contrats si l'amapien associé à une seule amap", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Mes contrats existants/)
      .first()
      .click();
    await expect(page.locator(".container table thead")).toContainText(
      "Choix identiques"
    );
    await expect(
      page.locator(".container table tbody td > span")
    ).toContainText("OUI");
    await expect(page.locator(".container table thead")).not.toContainText(
      "Amap"
    );
    await expect(
      page.locator(".container table tbody td > span")
    ).not.toContainText("amap test");
  });

  test("Afficher l'amap associée aux contrats si l'amapien associé à plusieurs amaps", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await requestAddUserToAmap(page, "amapien", "amap test 2");
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Mes contrats existants/)
      .first()
      .click();
    await expect(page.locator(".container table thead")).not.toContainText(
      "Choix identiques"
    );
    await expect(
      page.locator(".container table tbody td > span")
    ).not.toContainText("OUI");
    await expect(page.locator(".container table thead")).toContainText("Amap");
    await expect(
      page.locator(".container table tbody td > span")
    ).toContainText("amap test");
  });

  test("Masquer les contrats pour les amaps dont l'amapien est désactivé", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });

    await desactiverAmapien(page, "Prenom amapien");
    await requestAddUserToAmap(page, "amapien", "amap test 2");
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Mes contrats existants/)
      .first()
      .click();
    await expect(page.locator(".container table tbody")).not.toContainText(
      "contrat 1"
    );
  });

  test("Afficher le montant total du contrat dans la liste", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Mes contrats existants/)
      .first()
      .click();
    await expect(
      page.locator(".container table thead th:nth-child(6)")
    ).toContainText("Montant total");
    await expect(
      page.locator(".container table tbody td:nth-child(6)")
    ).toContainText("15,00");
  });

  test("Masquer les contrats en cours de validation", async ({ page }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );

    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Mes contrats existants/)
      .first()
      .click();
    await expect(page.locator(".container table tbody")).not.toContainText(
      "contrat 1"
    );
  });
});
