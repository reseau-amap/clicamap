import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  desactiverAmapien,
  goViaMenu,
  subscribeContractAmapienToValidate,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Lister les contrats souscrivables en tant que amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Ne pas afficher l'amap associée aux contrats si l'amapien associé à une seule amap", async ({
    page,
  }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Les nouveaux contrats disponibles/)
      .first()
      .click();
    await expect(page.locator(".container table thead")).toContainText(
      "Choix identiques"
    );
    await expect(page.locator(".container table tbody")).toContainText("OUI");
    await expect(page.locator(".container table thead")).not.toContainText(
      "Amap"
    );
    await expect(page.locator(".container table tbody")).not.toContainText(
      "amap test                            \n            01/01/2030"
    );
  });

  test("Afficher l'amap associée aux contrats si l'amapien associé à plusieurs amaps", async ({
    page,
  }) => {
    await requestAddUserToAmap(page, "amapien", "amap test 2");
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Les nouveaux contrats disponibles/)
      .first()
      .click();
    await expect(page.locator(".container table thead")).not.toContainText(
      "Choix identiques"
    );
    await expect(page.locator(".container table tbody")).not.toContainText(
      "OUI"
    );
    await expect(page.locator(".container table thead")).toContainText("Amap");
    await expect(page.locator(".container table tbody")).toContainText(
      "amap test                            \n            01/01/2030"
    );
  });

  test("Masquer les contrats pour les amaps dont l'amapien est désactivé", async ({
    page,
  }) => {
    await desactiverAmapien(page, "Prenom amapien");
    await requestAddUserToAmap(page, "amapien", "amap test 2");
    await login(page, "amapien");
    await page
      .getByText(/Mes contrats/)
      .first()
      .click();
    await page
      .getByText(/Les nouveaux contrats disponibles/)
      .first()
      .click();
    await expect(page.locator(".container table tbody")).not.toContainText(
      "contrat 1"
    );
  });

  test("Afficher les contrats en cours de validation", async ({ page }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    await login(page, "amapien");
    await goViaMenu(page, [
      "Mes contrats",
      "Les nouveaux contrats disponibles",
    ]);

    await expect(page.locator(".container table tbody tr")).toHaveCount(1);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("contrat 1");
    const validationButton = page
      .locator(".container table tbody tr:nth-child(1) td:nth-child(6)")
      .getByRole("link");
    await expect(validationButton).toHaveCount(1);
    await expect(validationButton).toContainText("Valider");
    await expect(validationButton).toHaveClass("btn btn-success");
  });

  test("Désactiver le bouton pour les contrats en cours de validation", async ({
    page,
  }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    dateSet("2040-03-19");
    await login(page, "amapien");
    await goViaMenu(page, [
      "Mes contrats",
      "Les nouveaux contrats disponibles",
    ]);

    await expect(page.locator(".container table tbody tr")).toHaveCount(1);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("contrat 1");
    const validationButton = page
      .locator(".container table tbody tr:nth-child(1) td:nth-child(6)")
      .getByRole("link");
    await expect(validationButton).toHaveCount(1);
    await expect(validationButton).toContainText("Validation échouée");
    await expect(validationButton).toHaveClass("btn btn-default");
    await expect(validationButton).toHaveAttribute("disabled", /.*/);
  });
});
