import { test, expect, Page } from "@playwright/test";
import {
  forceContratDepassement,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";

import {
  clickMainMenuLink,
  fermeAssocieeRegroupement,
  gotoContractSubscriptionOwn,
  removeUserEmails,
} from "../../support/shorcut";
import { select2 } from "../../support/shorcut_actions";
import { expectMailCount, mailClean } from "../../support/shorcut_mail";

const gotoStep3 = async (page: Page, scenario: any, amapien: string) => {
  await fermeAssocieeRegroupement(page, "ferme", "Regroupement Cdp");

  await login(page, scenario.role);
  await clickMainMenuLink(page, scenario.menu);
  await clickMainMenuLink(page, "Gestion des contrats signés");
  await page
    .locator(`[name="search_contrat_signe_amap[ferme]"]`)
    .selectOption({ label: "ferme" });
  await page
    .locator(`[name="search_contrat_signe_amap[mc]"]`)
    .selectOption("contrat 1");
  await page
    .getByRole("link", { name: "Ajouter le contrat d'un amapien" })
    .click();

  await select2(page, "contrat_signe_new_force[amapien]", amapien);
  await page.getByRole("button", { name: "Étape suivante" }).click();

  await page
    .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    .fill("1");
  await page.getByRole("button", { name: "Suivant" }).click();

  await page.getByRole("button", { name: "Suivant" }).click();
};

const postValidationExpect = async (page: Page, scenario: any) => {
  await currentPathShouldBe(page, scenario.home);
  await flashMessageShouldContain(
    page,
    "Le contrat a bien été créé mais n'est pas réglé. L'Amapien doit se rendre dans son interface \"Mes contrats existants\" pour effectuer le paiement"
  );

  await expect(
    page.locator("table.table-datatable tbody tr:nth-child(1) th:nth-child(2)")
  ).toHaveText("NOM AMAPIEN");
  await expect(
    page.locator("table.table-datatable tbody tr:nth-child(1) td:nth-child(3)")
  ).toHaveText("Prenom amapien");

  await expectMailCount(0);
};

test.describe("Souscrire à un nouveau contrat signé champ des possible en tant qu'amap ou référent  pour un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  const scenarios = [
    {
      role: "amap",
      menu: "Gestionnaire AMAP",
      home: "/contrat_signe/accueil_amap",
    },
    {
      role: "amapienref",
      menu: "Gestionnaire référent",
      home: "/contrat_signe/accueil_ref_produit",
    },
  ];

  scenarios.forEach((scenario) => {
    test(`Souscrire à un nouveau contrat signé champ des possible en tant que ${scenario.role}`, async ({
      page,
    }) => {
      await gotoStep3(
        page,
        scenario,
        "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
      );

      await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
      await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();

      await expect(page.locator(".alert-warning")).toHaveCount(0);

      await page.getByRole("button", { name: "Valider" }).click();

      await postValidationExpect(page, scenario);
    });

    test(`Souscrire à un nouveau contrat signé champ des possible en tant que ${scenario.role} - amapien sans email`, async ({
      page,
    }) => {
      await removeUserEmails(page, "amapien");

      await gotoStep3(page, scenario, "Prenom amapien NOM AMAPIEN -");

      await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
      await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();

      await expect(page.locator(".alert-warning")).toHaveCount(0);

      await page.getByRole("button", { name: "Valider" }).click();

      await postValidationExpect(page, scenario);
    });
  });
});
