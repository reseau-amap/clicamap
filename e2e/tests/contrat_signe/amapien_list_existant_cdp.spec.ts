import { test, expect, Page } from "@playwright/test";
import {
  forceContratPaye,
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  clickMainMenuLink,
  desactiverAmapien,
  fermeAssocieeRegroupement,
  subscribeContractInternal,
} from "../../support/shorcut";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { currentPathShouldBe } from "../../support/shorcut_assertions";
import {
  extractJwtTokenFromUrl,
  jwtTokenShouldHaveField,
} from "../../support/shorcut_jwt";

const createContract = async (page: Page) => {
  await fermeAssocieeRegroupement(page, "ferme", "Regroupement Cdp");
  await login(page, "amapien");
  await subscribeContractInternal(page, "amapien", "contrat 1", {
    "1_1": 1,
    "1_2": 1,
    "2_1": 1,
    "2_2": 1,
    "3_1": 1,
    "3_2": 1,
    "4_1": 1,
    "4_2": 1,
    "5_1": 1,
    "5_2": 1,
  });
  await expect(page.locator("h3")).toHaveText("Règlement du contrat");
};

test.describe("Comme amapien, je doit pouvoir lister les contrats existants", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Redirection vers l'interface de règlement Cdp", async ({ page }) => {
    await login(page, "amap");
    await createContract(page);

    await clickMainMenuLink(page, "Mes contrats");
    await clickMainMenuLink(page, "Mes contrats existants");

    await expect(page.locator("table thead th:nth-child(6)")).toHaveText(
      "Status"
    );
    await expect(
      page.locator("table tbody tr:nth-child(1) td:nth-child(1)")
    ).toHaveText("contrat 1");
    await expect(
      page.locator("table tbody tr:nth-child(1) td:nth-child(6)")
    ).toHaveText("En attente de règlement");

    await clickMenuLinkOnLine(page, "contrat 1", "Effectuer le règlement");
    await currentPathShouldBe(page, "/CI/empty");
    const token = extractJwtTokenFromUrl(page.url());
    jwtTokenShouldHaveField(token, "sub", "contract_payment");
    jwtTokenShouldHaveField(
      token,
      "amapien_uuid",
      "794d28b9-3d3d-45ad-82c3-d46cbe2046df"
    );
  });

  test("Redirection vers l'interface d'échéance Cdp", async ({ page }) => {
    await login(page, "amap");
    await createContract(page);
    await forceContratPaye(page, "contrat 1", "amapien");

    await login(page, "amapien");
    await clickMainMenuLink(page, "Mes contrats");
    await clickMainMenuLink(page, "Mes contrats existants");

    await expect(page.locator("table thead th:nth-child(6)")).toHaveText(
      "Status"
    );
    await expect(
      page.locator("table tbody tr:nth-child(1) td:nth-child(1)")
    ).toHaveText("contrat 1");
    await expect(
      page.locator("table tbody tr:nth-child(1) td:nth-child(6)")
    ).toHaveText("Payé");

    await clickMenuLinkOnLine(page, "contrat 1", "Voir les échéances");
    await currentPathShouldBe(page, "/CI/empty");

    const token = extractJwtTokenFromUrl(page.url());
    jwtTokenShouldHaveField(token, "sub", "contract_show");
    jwtTokenShouldHaveField(
      token,
      "amapien_uuid",
      "794d28b9-3d3d-45ad-82c3-d46cbe2046df"
    );
  });
});
