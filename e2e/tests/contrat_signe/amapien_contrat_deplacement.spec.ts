import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  checkRadio,
  clickButtonOnLine,
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";
import {
  clickMainMenuLink,
  createValidatedContractWithCustomDelivery,
  goViaMenu,
  validatePaysanContrat,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { waitForHTMXRequestEnd } from "../../support/shorcut_htmx";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

const creerContratDeplacement = async (page: Page, delai: number) => {
  await login(page, "amap");
  await clickMainMenuLink(page, "Gestionnaire AMAP");
  await clickMainMenuLink(page, "Gestion des contrats vierges");
  await page
    .locator('select[name="search_contrat_vierge_amap[ferme]"]')
    .selectOption({ label: "ferme" });
  await page
    .getByText(/Créer un nouveau contrat/)
    .first()
    .click();
  await page
    .getByText(/A partir de zéro/)
    .first()
    .click();
  await page.locator('[name="model_contrat_form1[nom]"]').fill("nom");
  await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await fillDatepicker(
    page,
    '[name="model_contrat_form2[forclusion]"]',
    new Date("2030-01-01")
  );
  await page
    .locator('[name="model_contrat_form2[delaiModifContrat]"]')
    .fill(delai.toString());
  await fillDatepicker(
    page,
    '[name="date_livraison_debut"]',
    new Date("2030-02-01")
  );
  await fillDatepicker(
    page,
    '[name="date_livraison_fin"]',
    new Date("2030-03-01")
  );

  await page
    .getByText(/Générer des dates/)
    .first()
    .click();
  await page
    .getByText(/Étape suivante/)
    .first()
    .click({
      timeout: 10000,
    });
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await checkRadio(
    page,
    page.locator('[name="model_contrat_form4[produitsIdentiquePaysan]"]'),
    "Oui"
  );
  await checkRadio(
    page,
    page.locator('[name="model_contrat_form4[produitsIdentiqueAmapien]"]'),
    "Oui"
  );
  await waitForHTMXRequestEnd(page.locator("form"));
  await page
    .locator('[name="model_contrat_form4[nblivPlancher]"]')
    .pressSequentially("1");
  await checkRadio(
    page,
    page.locator('[name="model_contrat_form4[nblivPlancherDepassement]"]'),
    "Oui"
  );
  await checkRadio(
    page,
    page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
    "Oui"
  );
  await checkRadio(
    page,
    page.locator(
      '[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
    ),
    "Oui"
  );
  await checkRadio(
    page,
    page.locator(
      '[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
    ),
    "Oui"
  );
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await page.locator('[name="model_contrat_form6[reglementNbMax]"]').fill("1");
  await page.locator("#btn-plus").click();
  await page
    .locator('[name="model_contrat_form6[dateReglements][1][date]"]')
    .fill("2030-01-10", {
      force: true,
    });
  await page.locator("h3").click(); // Hide popup
  await page
    .locator('[name="model_contrat_form6[reglementType][]"]')
    .locator('input[value="cheque"]:scope')
    .check();
  await page
    .locator('[name="model_contrat_form6[reglementModalite]"]')
    .fill("modalite");
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await page
    .getByText(/Valider/)
    .first()
    .click();
  await clickMenuLinkOnLine(
    page,
    "En création",
    "Envoyer le contrat au paysan pour validation"
  );
  await page.locator(".modal.in").getByText(/OUI/).first().click();

  await validatePaysanContrat(page, "paysan", "nom");

  await login(page, "amapien");
  await page
    .getByText(/Mes contrats/)
    .first()
    .click();
  await page
    .getByText(/Les nouveaux contrats disponibles/)
    .first()
    .click();
  await page
    .locator("tr")
    .filter({ has: page.locator("td").getByText(/nom/).first() })
    .locator(".btn")
    .click();
  await page
    .locator(`[name="contrat_souscription_form1[items][19_3][qty]"]`)
    .clear();
  await page
    .locator(`[name="contrat_souscription_form1[items][19_3][qty]"]`)
    .fill("1");
  await page
    .locator(`[name="contrat_souscription_form1[items][19_4][qty]"]`)
    .clear();
  await page
    .locator(`[name="contrat_souscription_form1[items][19_4][qty]"]`)
    .fill("1");
  await page
    .locator(`[name="contrat_souscription_form1[items][20_3][qty]"]`)
    .clear();
  await page
    .locator(`[name="contrat_souscription_form1[items][20_3][qty]"]`)
    .fill("1");
  await page
    .locator(`[name="contrat_souscription_form1[items][20_4][qty]"]`)
    .clear();
  await page
    .locator(`[name="contrat_souscription_form1[items][20_4][qty]"]`)
    .fill("1");
  await page
    .getByText(/Suivant/)
    .first()
    .click();
  await page
    .getByText(/Suivant/)
    .first()
    .click();
  await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
  await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
  await page
    .getByText(/Valider/)
    .first()
    .click();
  dateSet("2030-01-02");
  await page
    .getByText(/Mes contrats/)
    .first()
    .click();
  await page
    .getByText(/Mes contrats existants/)
    .first()
    .click();
};

export const createSubscribedContractWithCustomDelivery = async (
  page: Page,
  produitsIdentiquesAmapien: boolean,
  permissionDeplacement: boolean,
  permissionReport: boolean
) => {
  await login(page, "amap");

  await createValidatedContractWithCustomDelivery(
    page,
    "ferme",
    async (page) => {
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[produitsIdentiquePaysan]"]'
        ),
        "Non"
      );
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[produitsIdentiqueAmapien]"]'
        ),
        produitsIdentiquesAmapien ? "Oui" : "Non"
      );
      await expect(
        page.locator('[name="model_contrat_form4[nblivPlancher]"]')
      ).toBeEnabled();
      await page
        .locator('input[name="model_contrat_form4[nblivPlancher]"]')
        .pressSequentially("2");
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[nblivPlancherDepassement]"]'
        ),
        "Oui"
      );
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[amapienGestionDeplacement]"]'
        ),
        "Oui"
      );
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
        ),
        permissionDeplacement ? "Oui" : "Non"
      );
      await checkRadio(
        page,
        page.locator(
          'input[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
        ),
        permissionReport ? "Oui" : "Non"
      );
    }
  );
  await login(page, "amapien");
  await page.goto("/contrat_signe/contrat_own_new");
  await clickButtonOnLine(page, "nom", "Souscrire");

  await page
    .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
    .clear();
  await page
    .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
    .fill("2");
  await page
    .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
    .clear();
  await page
    .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
    .fill("2");

  await page
    .getByText(/Suivant/)
    .first()
    .click();
  await page
    .getByText(/Suivant/)
    .first()
    .click();
  await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
  await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();

  await page
    .getByText(/Valider/)
    .first()
    .click();

  dateSet("2030-01-02");
  await goViaMenu(page, ["Mes contrats", "Mes contrats existants"]);

  await clickMenuLinkOnLine(page, "nom", "Déplacer des livraisons");
  await expect(page.locator("h3")).toContainText(
    "Déplacer des dates de livraison"
  );
};

test.describe("Comme amapien, je doit pouvoir déplacer les dates de livraison d'un contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Masquer le bouton de copie", async ({ page }) => {
    await creerContratDeplacement(page, 2);
    await clickMenuLinkOnLine(page, "nom", "Déplacer des livraisons");
    await page.waitForLoadState();
    await expect(page.locator(".btn-copy")).toHaveCount(0);
  });

  test("Prise en compte délai ferme", async ({ page }) => {
    await creerContratDeplacement(page, 2);
    dateSet("2030-01-31");
    await page.reload();
    await clickMenuLinkOnLine(page, "nom", "Déplacer des livraisons");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
    ).not.toHaveAttribute("readonly");
  });

  test("Vérification total identique", async ({ page }) => {
    await createSubscribedContractWithCustomDelivery(page, false, true, true);

    await page
      .locator('[name="contrat_souscription_form1[items][22_3][qty]"]')
      .fill("1");
    await page.getByText(/Valider/).click();
    await expect(page.locator(".alert-danger")).toContainText(
      'La quantité total de produit "produit 1" doit être la même que dans le contrat'
    );
  });

  test("Vérification déplacement", async ({ page }) => {
    await createSubscribedContractWithCustomDelivery(page, false, false, true);

    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .fill("0");
    await page
      .locator('[name="contrat_souscription_form1[items][21_3][qty]"]')
      .fill("2");
    await page.getByText(/Valider/).click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Impossible de déplacer une livraison sur une date sans livraison."
    );
  });

  test("Vérification report", async ({ page }) => {
    await createSubscribedContractWithCustomDelivery(page, false, true, false);

    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .fill("0");
    await page
      .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
      .fill("4");
    await page.getByText(/Valider/).click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Impossible de reporter une livraison sur une date ayant déjà une livraison."
    );
  });

  test("Masquer produits non disponibles", async ({ page }) => {
    await createSubscribedContractWithCustomDelivery(page, false, true, false);

    await expect(
      page.locator('[name="contrat_souscription_form1[items][19_4][qty]"]')
    ).toHaveCount(0);
    await expect(page.locator("#order-form-container thead")).not.toContainText(
      "produit 2"
    );
  });

  test("Déplacement valide", async ({ page }) => {
    await createSubscribedContractWithCustomDelivery(page, false, true, true);

    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .fill("0");
    await page
      .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
      .fill("4");
    await page.getByText(/Valider/).click();
    await flashMessageShouldContain(page, "Le déplacement a bien été effectué");
    await clickMenuLinkOnLine(page, "nom", "Déplacer des livraisons");

    await expect(
      page.locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
    ).toHaveValue("0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
    ).toHaveValue("4");
  });
});
