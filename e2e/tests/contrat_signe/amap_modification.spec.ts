import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine, select2 } from "../../support/shorcut_actions";

import {
  clickMainMenuLink,
  goViaMenu,
  removeUserEmails,
  subscribeContract,
  subscribeContractAmapienToValidate,
  supprimerAmapien,
} from "../../support/shorcut";
import {
  expectMailCount,
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Comme amap, je doit pouvoir modifier le contrat d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Préservation des dates anciennes dates pour les contrats signés amapien", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
    });
    dateSet("2030-01-10");

    await login(page, "amap");
    await page.goto("/contrat_signe/accueil_amap");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });

    await clickMenuLinkOnLine(page, "NOM AMAPIEN", "Modifier le contrat");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
    ).toHaveValue("1.0");

    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
    ).not.toHaveAttribute("readonly");
  });
});
