import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine, select2 } from "../../support/shorcut_actions";

import { subscribeContract } from "../../support/shorcut";

test.describe("Comme amapien référent, je doit pouvoir ajouter un contrat signé à un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Ajouter un contrat signé à un amapien sans controle", async ({
    page,
  }) => {
    await login(page, "amapienref");
    await page.goto("/contrat_signe/accueil_ref_produit");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page
      .getByText(/Ajouter le contrat d'un amapien/)
      .first()
      .click();
    await select2(
      page,
      "contrat_signe_new_force[amapien]",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Attention, avec ce module de gestion des contrats signés, les contrôles cités dans le bandeau bleu ne s'appliquent pas lors de la souscription d'un contrat."
    );
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .fill("1");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody th:nth-child(2)")
    ).toContainText("NOM AMAPIEN");
  });

  test("Modifier un contrat signé à un amapien sans controle", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amapienref");
    await page.goto("/contrat_signe/accueil_ref_produit");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await clickMenuLinkOnLine(page, "Prenom amapien", "Modifier le contrat");
    await expect(page.locator(".alert-danger")).toContainText(
      "Attention, avec ce module de gestion des contrats signés, les contrôles cités dans le bandeau bleu ne s'appliquent pas lors de la souscription d'un contrat."
    );
  });
});
