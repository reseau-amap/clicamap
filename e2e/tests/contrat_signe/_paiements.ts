import { expect, Page } from "@playwright/test";
import { subscribeContract } from "../../support/shorcut";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  expectMailCount,
  expectMailShoudBeReceived,
} from "../../support/shorcut_mail";

export const testPaiementSubscribeContract = async (page: Page) => {
  await subscribeContract(page, "amapien", "contrat 1", {
    "1_1": 1,
    "1_2": 1,
    "2_1": 1,
    "2_2": 1,
    "3_1": 1,
    "3_2": 1,
    "4_1": 1,
    "4_2": 1,
    "5_1": 1,
    "5_2": 1,
  });
  await subscribeContract(page, "amapienref", "contrat 1", {
    "1_1": 1,
    "1_2": 1,
    "2_1": 1,
    "2_2": 1,
    "3_1": 1,
    "3_2": 1,
    "4_1": 1,
    "4_2": 1,
    "5_1": 1,
    "5_2": 1,
  });
};

export const testPaiementRappelTest = async (page: Page) => {
  await expect(
    page.getByRole("link", { name: "Envoyer un rappel" })
  ).toHaveAttribute("disabled");

  await page
    .locator("body .container table tr")
    .filter({
      has: page.locator("th").getByText("NOM AMAPIEN", { exact: true }),
    })
    .locator("td input.js-tableselect-row")
    .check();

  await expect(
    page.getByRole("link", { name: "Envoyer un rappel" })
  ).not.toHaveAttribute("disabled");

  await page.getByRole("link", { name: "Envoyer un rappel" }).click();
  await page.locator("body .modal.in").getByText("OUI").click();
  await flashMessageShouldContain(
    page,
    "Les rappels de paiement ont bien été envoyés"
  );

  await expectMailCount(1);
  await expectMailShoudBeReceived(
    "Règlement des contrats avec l'AMAP de amap test",
    ["amapien@test.amap-aura.org"]
  );
};

export const testPaiementStatutTest = async (page: Page) => {
  await expect(
    page.getByRole("link", { name: "Gérer les paiements" })
  ).toHaveAttribute("disabled");
  await expect(
    page.getByRole("link", { name: "Gérer les paiements" })
  ).toHaveAttribute("href", "#");

  await page
    .locator("body .container table tr")
    .filter({
      has: page.locator("th").getByText("NOM AMAPIEN", { exact: true }),
    })
    .locator("td input.js-tableselect-row")
    .check();

  await expect(
    page.getByRole("link", { name: "Gérer les paiements" })
  ).not.toHaveAttribute("disabled");
  await expect(
    page.getByRole("link", { name: "Gérer les paiements" })
  ).toHaveAttribute(
    "href",
    "http://localhost/contrat_signe/paiement_statut?selected=1"
  );

  await page.getByRole("link", { name: "Gérer les paiements" }).click();

  await page
    .locator('[name="contrat_paiement_statut[contratPaiementStatut]"]')
    .selectOption("Partiel");
  await page.getByRole("button", { name: "Valider" }).click();

  await flashMessageShouldContain(
    page,
    "Les statuts de paiement ont bien été modifiés"
  );
  await expect(
    page.locator("body .container table.table tr:nth-child(1) td:nth-child(4)")
  ).toHaveText("Partiel");
};
