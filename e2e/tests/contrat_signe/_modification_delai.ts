import { expect, Page } from "@playwright/test";
import { select2 } from "../../support/shorcut_actions";

export const delaiNegatifTest = async (page: Page) => {
  await page
    .locator('[name="modele_contrat_delai[delaiModifContrat]"]')
    .fill("-1");
  await page.getByText(/Valider/).click();
  await expect(page.locator(".alert-danger")).toContainText(
    "Cette valeur doit être supérieure ou égale à 0."
  );
};

export const delaiNbJourVideTest = async (page: Page) => {
  await page
    .locator('[name="modele_contrat_delai[delaiModifContrat]"]')
    .clear();
  await page.getByText(/Valider/).click();
  await expect(page.locator(".alert-danger")).toContainText(
    "Cette valeur ne doit pas être vide."
  );
};

export const delai0JoursTest = async (page: Page) => {
  await page
    .getByRole("link", { name: "Ajouter le contrat d'un amapien" })
    .click();
  await select2(
    page,
    "contrat_signe_new_force[amapien]",
    "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
  );
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await expect(
    page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
  ).toHaveAttribute("readonly");
  await expect(
    page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
  ).toHaveAttribute("readonly");
  await expect(
    page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
  ).not.toHaveAttribute("readonly");
  await expect(
    page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
  ).not.toHaveAttribute("readonly");
};
