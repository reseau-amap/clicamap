import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { expectMailCount, mailClean } from "../../support/shorcut_mail";
import { clickMainMenuLink, supprimerAmapien } from "../../support/shorcut";
import { login } from "../../support/shorcut_auth";

import {
  testPaiementRappelTest,
  testPaiementSubscribeContract,
} from "./_paiements";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme paysan, je doit pouvoir envoyer un rappel de paiement aux amapiens", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
    dateClean();
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Comme paysan, je doit pouvoir envoyer un rappel de paiement aux amapiens pour les contrats en cours", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);
    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats en cours");
    await page
      .locator(
        '[name="search_contrat_signe_paysan_with_paiement_status[amap]"]'
      )
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan_with_paiement_status[mc]"]')
      .selectOption({ label: "contrat 1" });

    await testPaiementRappelTest(page);

    await expect(page.locator("h3")).toHaveText("Gestion des contrats signés");
  });

  test("Comme paysan, je doit pouvoir envoyer un rappel de paiement aux amapiens pour les contrats archivés", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);
    dateSet("2040-01-01");
    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats archivés");
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });

    await testPaiementRappelTest(page);
    await expect(page.locator("h3")).toHaveText(
      "Gestion des contrats archivés"
    );
  });

  test("Comme paysan, je doit avoir un message d'erreur si l'amapien est supprimé", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);

    dateSet("2040-01-01");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats archivés");
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });

    for (const amapien of ["NOM AMAPIEN RÉFÉRENT", "NOM AMAPIEN"]) {
      await page
        .locator("body .container table tr")
        .filter({
          has: page.locator("th").getByText(amapien, { exact: true }),
        })
        .locator("td input.js-tableselect-row")
        .check();
    }

    await page.getByRole("link", { name: "Envoyer un rappel" }).click();
    await page.locator("body .modal.in").getByText("OUI").click();
    await flashMessageShouldContain(
      page,
      `Impossible d'envoyer les rappels car l'amapien "Prenom amapien NOM AMAPIEN" car il a été supprimé`
    );
    await expectMailCount(0);
  });
});
