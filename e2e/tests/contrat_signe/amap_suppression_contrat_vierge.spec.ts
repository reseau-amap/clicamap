import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

test.describe("Comme amap, je doit pouvoir supprimer un contrat vierge", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Suppression du contrat vierge, si aucun contrat signé", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page
      .getByText(/Supprimer le contrat vierge/)
      .first()
      .click();
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun contrat sélectionné\./
    );
    await expect(
      page
        .locator('[name="search_contrat_signe_amap[mc]"] option')
        .getByText("contrat 1")
    ).toHaveCount(0);
  });
});
