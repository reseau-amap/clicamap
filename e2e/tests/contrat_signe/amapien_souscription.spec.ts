import { test, expect, Page } from "@playwright/test";
import {
  forceContratDepassement,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";

import { gotoContractSubscriptionOwn } from "../../support/shorcut";
import { expectMailCount, mailClean } from "../../support/shorcut_mail";

test.describe("Souscrire à un nouveau contrats signé en tant que amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Souscrire un nouveau contrat signé en tant qu'amapien", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await expect(page.locator("h4")).not.toBeVisible();

    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][4_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][5_2][qty]"]')
      .fill("1");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();

    await expect(page.locator(".alert-warning")).toHaveCount(0);

    await page
      .getByText(/Valider/)
      .first()
      .click();
    await currentPathShouldBe(page, "/contrat_signe/contrat_own_new");
    await expect(page.locator(".container table")).not.toContainText(
      "contrat 1"
    );
    await page.goto("/contrat_signe/contrat_own_existing/");
    await expect(page.locator(".container table")).toContainText("contrat 1");

    await expectMailCount(0);
  });
});
