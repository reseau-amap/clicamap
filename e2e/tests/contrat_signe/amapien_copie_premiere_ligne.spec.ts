import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  checkCheckbox,
  clickMenuLinkOnLine,
  fillDatepicker,
  select2,
} from "../../support/shorcut_actions";

import {
  gotoContractStep5,
  gotoContractSubscriptionOwn,
  validatePaysanContrat,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Pouvoir copier la premiere ligne lors de la souscription d'un contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("En tant qu'amapien je doit pouvoir copier la premiere ligne d'un contrat pour faciliter sa souscription", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .fill("2");
    await page
      .getByText(/Copier la première ligne partout/)
      .first()
      .click();
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    ).toHaveValue("2");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][18_1][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][18_2][qty]"]')
    ).toHaveValue("2");
  });

  test("En tant qu'amap je doit pouvoir copier la premiere ligne d'un contrat pour faciliter sa souscription meme si les premières lignes sont grisées", async ({
    page,
  }) => {
    dateSet("2030-01-02");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page
      .getByText(/Ajouter le contrat d'un amapien/)
      .first()
      .click();
    await select2(
      page,
      "contrat_signe_new_force[amapien]",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .fill("2");
    await page
      .getByText(/Copier la première ligne partout/)
      .first()
      .click();
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    ).toHaveValue("2");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][18_1][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][18_2][qty]"]')
    ).toHaveValue("2");
  });

  test("En tant qu'amap je doit pouvoir copier la premiere ligne d'un contrat pour faciliter sa souscription meme si les premières lignes sont exclues", async ({
    page,
  }) => {
    await login(page, "amap");

    await gotoContractStep5(page, "ferme");
    await page
      .locator('[name="model_contrat_form5[exclusions][2030-02-01_2]"]')
      .uncheck();
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page
      .locator('input[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("button#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );
    await checkCheckbox(
      page,
      page.locator('input[name="model_contrat_form6[reglementType][]"]'),
      "Chèque"
    );
    await page
      .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
      .fill("modalite");
    await page.getByRole("button", { name: "Étape suivante" }).click();

    await page.getByRole("button", { name: "Valider" }).click();
    await clickMenuLinkOnLine(
      page,
      "Nom",
      "Envoyer le contrat au paysan pour validation"
    );
    await page.locator(".modal.in").getByText("OUI").click();
    await validatePaysanContrat(page, "paysan", "nom");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "nom" });
    await page
      .getByText(/Ajouter le contrat d'un amapien/)
      .first()
      .click();
    await select2(
      page,
      "contrat_signe_new_force[amapien]",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="contrat_souscription_form1[items][20_4][qty]"]')
      .fill("1");
    await page
      .getByText(/Copier la première ligne partout/)
      .first()
      .click();
    await expect(
      page.locator('[name="contrat_souscription_form1[items][20_4][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][21_4][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][22_4][qty]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][23_4][qty]"]')
    ).toHaveValue("1");
  });
});
