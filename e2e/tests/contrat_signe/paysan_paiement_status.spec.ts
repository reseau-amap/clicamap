import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { clickMainMenuLink, subscribeContract } from "../../support/shorcut";
import { login } from "../../support/shorcut_auth";
import {
  testPaiementStatutTest,
  testPaiementSubscribeContract,
} from "./_paiements";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Comme paysan, je doit pouvoir changer le statut de paiement des contrats", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateClean();
  });

  test("Comme paysan, je doit pouvoir changer le statut de paiement des contrats en cours", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);
    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats en cours");
    await page
      .locator(
        '[name="search_contrat_signe_paysan_with_paiement_status[amap]"]'
      )
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan_with_paiement_status[mc]"]')
      .selectOption({ label: "contrat 1" });

    await testPaiementStatutTest(page);

    await expect(page.locator("h3")).toHaveText("Gestion des contrats signés");
  });

  test("Comme paysan, je doit pouvoir changer le statut de paiement des contrats archivés", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);
    dateSet("2040-01-01");
    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats archivés");
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });

    await testPaiementStatutTest(page);

    await expect(page.locator("h3")).toHaveText(
      "Gestion des contrats archivés"
    );
  });
});
