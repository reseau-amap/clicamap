import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  subscribeContract,
  subscribeContractAmapienToValidate,
  supprimerAmapien,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("En tant que paysan je doit pouvoir lister les contrats signés", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Liste des contrats archivés des amapiens supprimés - paysan", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    dateSet("2040-03-19");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "paysan");
    await page
      .getByText(/Gestion des contrats/)
      .first()
      .click();
    await page
      .getByText(/Contrats archivés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator(".container table.table-datatable")).toHaveText(
      /nom amapien/
    );
  });

  test("Masquer les contrats en cours de validation amapien", async ({
    page,
  }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    dateSet("2040-03-19");
    await login(page, "paysan");
    await page
      .getByText(/Gestion des contrats/)
      .first()
      .click();
    await page
      .getByText(/Contrats archivés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun contrat n'a encore été créé./
    );
  });
});
