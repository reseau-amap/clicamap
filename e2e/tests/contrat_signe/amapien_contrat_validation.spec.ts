import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  desactiverAmapien,
  goViaMenu,
  subscribeContractAmapienToValidate,
} from "../../support/shorcut";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

const goToSubscription = async (page: Page) => {
  await subscribeContractAmapienToValidate(
    page,
    "amap",
    "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
    "contrat 1",
    {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    }
  );

  await login(page, "amapien");
  await goViaMenu(page, ["Mes contrats", "Les nouveaux contrats disponibles"]);
  await page.getByRole("link", { name: "Valider" }).click();
};

test.describe("Comme amapien je doit pouvoir valider mes contrats ", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Checkboxes obligatoires", async ({ page }) => {
    await goToSubscription(page);

    await page.getByRole("button", { name: "Valider" }).click();
    await expect(page.locator("form .alert-danger").nth(0)).toContainText(
      "Le champ Confirmation de signature est requis."
    );
    await expect(page.locator("form .alert-danger").nth(1)).toContainText(
      "Le champ Confirmation de la charte est requis."
    );
  });

  test("Validation contrat", async ({ page }) => {
    await goToSubscription(page);

    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await page.getByRole("button", { name: "Valider" }).click();

    await flashMessageShouldContain(page, "Le contrat a bien été validé");
    await expect(page.locator("h3")).toContainText(
      "Les nouveaux contrats disponibles"
    );

    await expect(page.locator(".container table tbody tr")).not.toContainText(
      "contrat 1"
    );
    await goViaMenu(page, ["Mes contrats", "Mes contrats existants"]);
    await expect(page.locator(".container table tbody tr")).toContainText(
      "contrat 1"
    );
  });
});
