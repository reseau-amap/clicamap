import { test, expect, Page } from "@playwright/test";
import {
  forceContratDepassement,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";

import {
  clickMainMenuLink,
  fermeAssocieeRegroupement,
  gotoContractSubscriptionOwn,
} from "../../support/shorcut";
import { select2 } from "../../support/shorcut_actions";
import {
  extractJwtTokenFromUrl,
  jwtTokenShouldHaveField,
} from "../../support/shorcut_jwt";
import { expectMailCount, mailClean } from "../../support/shorcut_mail";

test.describe("Souscrire à un nouveau contrat signé champ des possible en tant que amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test(`Souscrire un nouveau contrat signé en tant qu'amapien`, async ({
    page,
  }) => {
    await fermeAssocieeRegroupement(page, "ferme", "Regroupement Cdp");

    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);

    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_2][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_2][qty]"]')
      .fill("1");

    await page.getByText(/Suivant/).click();

    await page.getByText(/Suivant/).click();
    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();

    await expect(page.locator(".alert-warning")).toHaveCount(0);

    await page.getByText(/Valider/).click();

    await currentPathShouldBe(page, "/contrat_signe/redirection_cdp/1");
    await expect(page.locator("h3")).toHaveText(/Règlement du contrat/);
    await page.getByText(/Valider/).click();

    await currentPathShouldBe(page, "/CI/empty");
    const token = extractJwtTokenFromUrl(page.url());
    jwtTokenShouldHaveField(token, "sub", "contract_new");
    jwtTokenShouldHaveField(
      token,
      "amapien_uuid",
      "794d28b9-3d3d-45ad-82c3-d46cbe2046df"
    );

    await expectMailCount(0);
  });
});
