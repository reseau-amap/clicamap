import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import { subscribeContract } from "../../support/shorcut";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme amapien référent, je doit pouvoir déplacer la date d'un contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Controle de validité de la date de destination", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amapienref");
    await page.goto("/contrat_signe/accueil_ref_produit");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page.getByRole("link", { name: "Autre" }).click();
    await page
      .locator(".modal.in")
      .getByText(/Déplacer une date de livraison/)
      .first()
      .click();
    await page
      .locator('[name="modele_contrat_deplacement[src]"]')
      .selectOption({ label: "2030-01-01" });
    await fillDatepicker(
      page,
      '[name="modele_contrat_deplacement[dst]"]',
      new Date("2030-01-08")
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "La date sélectionnée est déjà une date de livraison."
    );
  });

  test("Déplacement de la date de livraison", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await subscribeContract(page, "amapienref", "contrat 1", {
      "1_1": 2,
      "1_2": 2,
      "3_1": 2,
      "3_2": 2,
      "4_1": 2,
      "4_2": 2,
      "5_1": 2,
      "5_2": 2,
      "6_1": 2,
      "6_2": 2,
    });
    await login(page, "amapienref");
    await page.goto("/contrat_signe/accueil_ref_produit");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page.getByRole("link", { name: "Autre" }).click();
    await page
      .locator(".modal.in")
      .getByText(/Déplacer une date de livraison/)
      .first()
      .click();
    await page
      .locator('[name="modele_contrat_deplacement[src]"]')
      .selectOption({ label: "2030-01-08" });
    await fillDatepicker(
      page,
      '[name="modele_contrat_deplacement[dst]"]',
      new Date("2030-01-09")
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "La livraison du 08/01/2030 va être déplacée au 09/01/2030."
    );
    await expect(page.locator(".alert-warning").nth(0)).toContainText(
      "NOM AMAPIEN Prenom amapien"
    );
    await expect(page.locator(".alert-warning").nth(1)).toContainText(
      "amapien@test.amap-aura.org"
    );
    await expect(page.locator(".alert-warning").nth(0)).not.toContainText(
      "NOM AMAPIEN RÉFÉRENT Prénom amapien référent"
    );
    await expect(page.locator(".alert-warning").nth(1)).not.toContainText(
      "amapienref@test.amap-aura.org"
    );
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les contrats ont été modifiés avec succès."
    );
    await clickMenuLinkOnLine(page, "Prenom amapien", "Modifier le contrat");
    await expect(
      page
        .locator("#order-form-container table tbody td:nth-child(1)")
        .getByText("09/01/2030")
    ).toHaveCount(1);
    await expect(
      page
        .locator("#order-form-container table tbody td:nth-child(1)")
        .getByText("08/01/2030")
    ).toHaveCount(0);
  });

  test("Vérification redirection etape 2 si etape 1 pas validee", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amapienref");
    await page.goto("/contrat_signe/accueil_ref_produit");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page.getByRole("link", { name: "Autre" }).click();
    await page
      .locator(".modal.in")
      .getByText(/Déplacer une date de livraison/)
      .first()
      .click();
    await page.goto("/contrat_signe/move_step_2");
    await expect(page).toHaveURL(/\/contrat_signe\/accueil_ref_produit/);
  });
});
