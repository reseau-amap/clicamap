import { test, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { clickMainMenuLink, supprimerAmapien } from "../../support/shorcut";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { dateClean, dateSet } from "../../support/shorcut_date";
import {
  delai0JoursTest,
  delaiNbJourVideTest,
  delaiNegatifTest,
} from "./_modification_delai";

const goToPage = async (page: Page) => {
  await login(page, "paysan");
  await clickMainMenuLink(page, "Gestion des contrats");
  await clickMainMenuLink(page, "Contrats en cours");

  await page
    .locator('[name="search_contrat_signe_paysan_with_paiement_status[amap]"]')
    .selectOption({ label: "amap test" });
  await page
    .locator('[name="search_contrat_signe_paysan_with_paiement_status[mc]"]')
    .selectOption({ label: "contrat 1" });
  await page.getByRole("link", { name: "Autre" }).click();
  await page.locator(".modal.in").getByText("Délai de la ferme").click();
};

test.describe("Comme paysan, je doit pouvoir modifier le délai de la ferme depuis les contrats signés", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Nombre de jours négatif", async ({ page }) => {
    await goToPage(page);
    await delaiNegatifTest(page);
  });

  test("Nombre de jours vide", async ({ page }) => {
    await goToPage(page);
    await delaiNbJourVideTest(page);
  });

  test("Modification à 0 jours", async ({ page }) => {
    dateSet("2030-01-08");
    await goToPage(page);
    await page
      .locator('[name="modele_contrat_delai[delaiModifContrat]"]')
      .fill("0");
    await page.getByText(/Valider/).click();
    await flashMessageShouldContain(page, "Le délai a bien été modifié");
    await login(page, "amap");
    await page.goto("/contrat_signe/accueil_amap");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await delai0JoursTest(page);
  });
});
