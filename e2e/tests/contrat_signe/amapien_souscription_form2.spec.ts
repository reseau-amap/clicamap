import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  clickMainMenuLink,
  gotoContractStep3,
  gotoContractStep6,
  gotoContractSubscriptionOwn,
  validatePaysanContrat,
} from "../../support/shorcut";
import {
  checkCheckbox,
  checkRadio,
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { enableExdebug } from "../../support/shorcut_debug";

const createContractWithDates = async (
  page: Page,
  dateNbMax: number,
  dateCount: number
) => {
  await login(page, "amap");

  await gotoContractStep6(page, "ferme");
  await page
    .locator('input[name="model_contrat_form6[reglementNbMax]"]')
    .fill(dateNbMax.toString());

  for (let i = 0; i < dateCount; i++) {
    await page.locator("button#btn-plus").click();
  }
  for (let i = 1; i < dateCount + 1; i++) {
    await fillDatepicker(
      page,
      `input[name="model_contrat_form6[dateReglements][${i}][date]"]`,
      new Date(`2030-02-0${i}`)
    );
  }

  await checkCheckbox(
    page,
    page.locator('input[name="model_contrat_form6[reglementType][]"]'),
    "Chèque"
  );
  await checkCheckbox(
    page,
    page.locator('input[name="model_contrat_form6[reglementType][]"]'),
    "Virement"
  );
  await page
    .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
    .fill("modalite");
  await page.getByRole("button", { name: "Étape suivante" }).click();
  await page.getByRole("button", { name: "Valider" }).click();

  await clickMenuLinkOnLine(
    page,
    "En création",
    "Envoyer le contrat au paysan pour validation"
  );
  await page.locator(".modal.in").getByText("OUI").click();

  await validatePaysanContrat(page, "paysan", "nom");

  await login(page, "amapien");
  await page
    .getByText(/Mes contrats/)
    .first()
    .click();
  await page
    .getByText(/Les nouveaux contrats disponibles/)
    .first()
    .click();
  await page
    .locator("body .container table tr")
    .filter({
      has: page.locator("> *").getByText("nom", { exact: true }),
    })
    .getByRole("link", { name: "Souscrire" })
    .click();
  await page
    .locator(
      "#order-form-container table tbody tr:nth-child(1) td:nth-child(3) input"
    )
    .fill("1");
  await page
    .getByText(/Suivant/)
    .first()
    .click();
};

export const gotoSubscriptionPage2 = async (page: Page) => {
  await login(page, "amapien");
  await gotoContractSubscriptionOwn(page);

  await page
    .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][4_2][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][5_2][qty]"]')
    .fill("1");
  await page
    .getByText(/Suivant/)
    .first()
    .click();
};

const updateReglementField = async (page: Page, newValue: string) => {
  await page
    .locator('[name="contrat_souscription_form2[reglementNb]"]')
    .clear();
  await page
    .locator('[name="contrat_souscription_form2[reglementNb]"]')
    .pressSequentially(newValue);

  await page.waitForTimeout(500); // Wait for trigger delay
  // Ensure request ended
  await expect(page.locator("form.htmx-request")).toHaveCount(0);
};

test.describe("Souscrire à un nouveau contrats signé en tant que amapien - form 2", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Affichage de nom de l'amapien lors de la souscription", async ({
    page,
  }) => {
    await createContractWithDates(page, 2, 3);
    await expect(page.locator("p.h4")).toHaveText("Prenom amapien NOM AMAPIEN");
  });

  test("Valeurs par défaut des champs", async ({ page }) => {
    await createContractWithDates(page, 2, 3);
    await expect(page.locator("table#dates tbody tr")).toHaveCount(0);

    await expect(page.locator('[name="amount_total"]')).toHaveValue("1,00");
    await expect(
      page.locator('[name="contrat_souscription_form2[reglementNb]"]')
    ).toHaveValue("");
    await expect(
      page.locator('[name="contrat_souscription_form2[reglementNb]"]')
    ).toHaveAttribute("min", "1");
    await expect(
      page.locator('[name="contrat_souscription_form2[reglementNb]"]')
    ).toHaveAttribute("max", "2");
    await expect(
      page.locator('[name="contrat_souscription_form2[reglementNb]"]')
    ).toHaveAttribute("step", "1");
    await expect(
      page.locator(".form-group").filter({
        has: page.locator('[name="contrat_souscription_form2[reglementNb]"]'),
      })
    ).toHaveClass("form-group");
    await expect(
      page.locator(
        '[name="contrat_souscription_form2[reglementType]"][value="cheque"]'
      )
    ).not.toBeChecked();
    await expect(
      page.locator("label").filter({
        has: page.locator(
          '[name="contrat_souscription_form2[reglementType]"][value="cheque"]'
        ),
      })
    ).toHaveText("Chèque (à l'ordre de ferme 1 ordre cheque)");
    await expect(
      page.locator(
        '[name="contrat_souscription_form2[reglementType]"][value="virement"]'
      )
    ).not.toBeChecked();
  });

  test("Gestion de la création dynamique des champs dates", async ({
    page,
  }) => {
    await createContractWithDates(page, 3, 3);
    await updateReglementField(page, "2");

    await expect(page.locator("table#dates tbody tr")).toHaveCount(2, {
      timeout: 20000,
    });
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveText("01/02/2030");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator("table#dates tbody tr:nth-child(1) td:nth-child(2) input")
    ).toHaveValue("0,50");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) select option[selected]"
      )
    ).toBeHidden();
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) select option"
      )
    ).toHaveCount(4);
    await expect(
      page.locator("table#dates tbody tr:nth-child(2) td:nth-child(2) input")
    ).toHaveValue("0,50");

    await updateReglementField(page, "3");
    await expect(page.locator("table#dates tbody tr")).toHaveCount(3, {
      timeout: 20000,
    });
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveText("01/02/2030", { timeout: 20000 });
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator("table#dates tbody tr:nth-child(1) td:nth-child(2) input")
    ).toHaveValue("0,34");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveText("02/02/2030", { timeout: 20000 });
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator("table#dates tbody tr:nth-child(2) td:nth-child(2) input")
    ).toHaveValue("0,33");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(3) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveText("03/02/2030", { timeout: 20000 });
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(3) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator("table#dates tbody tr:nth-child(3) td:nth-child(2) input")
    ).toHaveValue("0,33");

    await updateReglementField(page, "2");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveText("01/02/2030");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) select option[selected]"
      )
    ).toHaveText("02/02/2030");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) select option"
      )
    ).toHaveCount(4);
  });

  test("Validation - dates requises", async ({ page }) => {
    await createContractWithDates(page, 3, 3);
    await updateReglementField(page, "2");

    await page.getByRole("button", { name: "Suivant" }).click();
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) .alert-danger"
      )
    ).toContainText(
      "Merci de sélectionner une date de règlement pour toutes les échéances."
    );
  });

  test("Validation - pas de duplication", async ({ page }) => {
    await createContractWithDates(page, 3, 3);
    await updateReglementField(page, "2");
    await page
      .locator("table#dates tbody tr:nth-child(2) td:nth-child(1) select")
      .selectOption("01/02/2030");

    await page.getByRole("button", { name: "Suivant" }).click();
    await expect(page.locator("table#dates ~ .alert-danger")).toContainText(
      "Merci de ne pas sélectionner plus d'une fois chaque date."
    );
  });

  test("Validation - type paiement requis", async ({ page }) => {
    await createContractWithDates(page, 3, 3);
    await updateReglementField(page, "1");
    await page.getByRole("button", { name: "Suivant" }).click();
    await expect(
      page.locator("#contrat_souscription_form2_reglementType ~ .alert-danger")
    ).toContainText("Merci de sélectionner un mode de règlement.");
  });

  test("Retour étape 2", async ({ page }) => {
    await createContractWithDates(page, 3, 3);
    await updateReglementField(page, "2");
    await page
      .locator("table#dates tbody tr:nth-child(1) td:nth-child(1) select")
      .selectOption("01/02/2030");
    await page
      .locator("table#dates tbody tr:nth-child(2) td:nth-child(1) select")
      .selectOption("02/02/2030");
    await page
      .locator("#contrat_souscription_form2_reglementType")
      .getByLabel("Chèque")
      .click();

    await page.getByRole("button", { name: "Suivant" }).click();
    await page.getByRole("link", { name: "Étape précédente" }).click();

    await updateReglementField(page, "2");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) select option[selected]"
      )
    ).toHaveText("01/02/2030");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(2) td:nth-child(1) select option[selected]"
      )
    ).toHaveText("02/02/2030");
    await expect(
      await page
        .locator("#contrat_souscription_form2_reglementType")
        .getByLabel("Chèque")
    ).toBeChecked();
  });

  test("Vérification limite dates paiement - min", async ({ page }) => {
    await createContractWithDates(page, 3, 3);

    const field = await page.locator(
      '[name="contrat_souscription_form2[reglementNb]"]'
    );
    await field.evaluate((e) => e.removeAttribute("min"));
    await field.clear();
    await field.pressSequentially("-1");
    await field.press("Enter");

    await expect(field).toHaveValue("1");
    await expect(page.locator("table#dates tbody tr")).toHaveCount(1);
  });

  test("Vérification limite dates paiement - max", async ({ page }) => {
    await createContractWithDates(page, 3, 3);

    const field = await page.locator(
      '[name="contrat_souscription_form2[reglementNb]"]'
    );
    await field.evaluate((e) => e.removeAttribute("max"));
    await field.clear();
    await field.pressSequentially("4");
    await field.press("Enter");

    await expect(field).toHaveValue("3");
    await expect(page.locator("table#dates tbody tr")).toHaveCount(3);
  });

  test("Auto selection automatique du moyen de paiement si un seul disponible", async ({
    page,
  }) => {
    await gotoSubscriptionPage2(page);
    await expect(
      page.locator(
        '[name="contrat_souscription_form2[reglementType]"][value="cheque"]'
      )
    ).toBeChecked();
  });

  test("Auto selection de la date si une seule date disponible", async ({
    page,
  }) => {
    await gotoSubscriptionPage2(page);
    await expect(
      page.locator('[name="contrat_souscription_form2[reglementNb]"]')
    ).toHaveValue("1");
    await expect(
      page.locator(".form-group").filter({
        has: page.locator('[name="contrat_souscription_form2[reglementNb]"]'),
      })
    ).toHaveClass("disabled form-group");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option[selected]"
      )
    ).toHaveText("01/01/2020");
    await expect(
      page.locator(
        "table#dates tbody tr:nth-child(1) td:nth-child(1) .form-group.disabled select option"
      )
    ).toHaveCount(1);
    await expect(
      page.locator("table#dates tbody tr:nth-child(1) td:nth-child(2) input")
    ).toHaveValue("15,00");
  });

  test("Validation nb dates max < date dispo", async ({ page }) => {
    await createContractWithDates(page, 4, 5);
    await updateReglementField(page, "4");
    await page
      .locator("table#dates tbody tr:nth-child(2) td:nth-child(1) select")
      .selectOption("02/02/2030");
    await page
      .locator("table#dates tbody tr:nth-child(3) td:nth-child(1) select")
      .selectOption("03/02/2030");
    await page
      .locator("table#dates tbody tr:nth-child(4) td:nth-child(1) select")
      .selectOption("04/02/2030");
    await page
      .locator("#contrat_souscription_form2_reglementType")
      .getByLabel("Chèque")
      .click();
    await page.getByRole("button", { name: "Suivant" }).click();
    await expect(page.locator("h3")).toContainText(
      "Validation juridique de mon contrat"
    );
  });

  test("Mise à jour des montants en cas de modification du contrat", async ({
    page,
  }) => {
    await gotoSubscriptionPage2(page);
    await expect(
      page.locator("table#dates tbody tr:nth-child(1) td:nth-child(2) input")
    ).toHaveValue("15,00");

    await page
      .getByText(/Suivant/)
      .first()
      .click();

    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await page
      .getByText(/Valider/)
      .first()
      .click();

    await clickMainMenuLink(page, "Mes contrats");
    await clickMainMenuLink(page, "Mes contrats existants");
    await clickMenuLinkOnLine(page, "contrat 1", "Modifier le contrat");
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][4_2][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("2");
    await page
      .locator('[name="contrat_souscription_form1[items][5_2][qty]"]')
      .fill("2");

    await page
      .getByText(/Suivant/)
      .first()
      .click();

    await expect(
      page.locator("table#dates tbody tr:nth-child(1) td:nth-child(2) input")
    ).toHaveValue("30,00");
  });
});

test.describe("Souscrire à un nouveau contrats signé en tant que amapien - form 2", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });
});
