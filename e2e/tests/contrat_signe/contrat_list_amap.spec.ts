import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { subscribeContract, supprimerAmapien } from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
test.describe("En tant qu'amap je doit pouvoir lister les contrats signés", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Titre de la page liste des contrats actifs", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Gestion des contrats signés/);
  });

  test("Titre de la page liste des contrats archivés", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats archivés/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(
      /Gestion des contrats archivés/
    );
  });

  test("Liste des contrats actifs", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator(".container table tbody tr th")).toHaveText(
      /NOM AMAPIEN/
    );
    await expect(
      page.locator(".container table tbody tr .dropdown-menu")
    ).toHaveText(/Télécharger le contrat \(excel\)/);
    await expect(
      page.locator(".btn-primary").getByText(/Autre/).first()
    ).toBeVisible();
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats archivés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await expect(
      page
        .locator('[name="search_contrat_signe_amap[mc]"] option')
        .getByText("contrat 1")
    ).toHaveCount(0);
  });

  test("Liste des contrats archivés", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    dateSet("2030-04-31");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await expect(
      page
        .locator('[name="search_contrat_signe_amap[mc]"] option')
        .getByText("contrat 1")
    ).toHaveCount(0);
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats archivés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator(".container table tbody tr th")).toHaveText(
      /NOM AMAPIEN/
    );
    await expect(
      page.locator(".container table tbody tr .dropdown-menu")
    ).not.toHaveText(/Télécharger le contrat \(excel\)/);
    await expect(
      page.locator(".btn-primary").getByText(/Autre/).first()
    ).not.toBeVisible();
  });

  test("Liste des contrats des amapiens supprimés", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    dateSet("2030-03-19");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    dateClean();
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats signés/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator(".container table tbody tr th")).toHaveText(
      /NOM AMAPIEN/
    );
  });
});
