import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine, select2 } from "../../support/shorcut_actions";

import {
  clickMainMenuLink,
  goViaMenu,
  removeUserEmails,
  subscribeContractAmapienToValidate,
  supprimerAmapien,
} from "../../support/shorcut";
import {
  expectMailCount,
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { dateClean, dateSet } from "../../support/shorcut_date";

const goToStep3 = async (page: Page, amapien: string) => {
  await login(page, "amap");
  await page.goto("/contrat_signe/accueil_amap");
  await page
    .locator('[name="search_contrat_signe_amap[ferme]"]')
    .selectOption({ label: "ferme" });
  await page
    .locator('[name="search_contrat_signe_amap[mc]"]')
    .selectOption({ label: "contrat 1" });
  await page
    .getByRole("link", { name: "Ajouter le contrat d'un amapien" })
    .click();
  await select2(page, "contrat_signe_new_force[amapien]", amapien);
  await page
    .getByText(/Étape suivante/)
    .first()
    .click();
  await page
    .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    .fill("1");
  await page
    .locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    .fill("1");
  await page
    .getByText(/Suivant/)
    .first()
    .click();
  await page
    .getByText(/Suivant/)
    .first()
    .click();
};

test.describe("Comme amap, je doit pouvoir ajouter le contrat d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Ajouter un contrat signé à un amapien", async ({ page }) => {
    await goToStep3(
      page,
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org"
    );
    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await expect(page.locator(".alert-danger")).toHaveCount(2);
    await expect(page.locator(".alert-danger").nth(1)).toContainText(
      "Attention : le contrat nécessite une validation de l'amapien. Celui-ci sera notifié à amapien@test.amap-aura.org."
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".container table tbody tr th")).toContainText(
      "NOM AMAPIEN"
    );
    await expect(
      page.locator(
        ".container table tbody tr td:nth-child(6) button.dropdown-toggle"
      )
    ).toHaveText("Contrat en cours de validation");
    await expectMailCount(1);
    await expectMailShoudBeReceived(
      'Votre contrat "contrat 1" est disponible pour validation',
      ["amapien@test.amap-aura.org"]
    );
  });

  test("Ajouter un contrat signé à un amapien - modification label bouton si validation expirée", async ({
    page,
  }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    dateSet("2030-01-10");
    await page.reload();
    await expect(
      page.locator(
        ".container table tbody tr td:nth-child(6) button.dropdown-toggle"
      )
    ).toHaveText("Validation échouée");
  });

  test("Ajouter un contrat signé à un amapien - préservation anciennes dates si validation échouée", async ({
    page,
  }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    await clickMenuLinkOnLine(page, "NOM AMAPIEN", "Modifier le contrat");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    ).toHaveValue("1.0");

    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    ).not.toHaveAttribute("readonly");
  });

  test("Ajouter un contrat signé à un amapien - reset anciennes dates si validation échouée", async ({
    page,
  }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    dateSet("2030-01-10");
    await page.reload();
    await expect(
      page.locator(
        ".container table tbody tr td:nth-child(6) button.dropdown-toggle"
      )
    ).toHaveText("Validation échouée");
    await clickMenuLinkOnLine(page, "NOM AMAPIEN", "Modifier le contrat");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    ).toHaveValue("0.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][2_2][qty]"]')
    ).toHaveAttribute("readonly");

    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    ).toHaveValue("1.0");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][3_2][qty]"]')
    ).not.toHaveAttribute("readonly");

    await page.getByRole("button", { name: "Suivant" }).click();
    await page.getByRole("button", { name: "Suivant" }).click();

    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await page.getByRole("button", { name: "Valider" }).click();
    await expect(
      page.locator(
        ".container table tbody tr td:nth-child(6) button.dropdown-toggle"
      )
    ).toHaveText("Contrat en cours de validation");
    await expectMailCount(2);
    await expectMailShoudBeReceived(
      'Votre contrat "contrat 1" est disponible pour validation',
      ["amapien@test.amap-aura.org"]
    );
  });

  test("Ajouter un contrat signé à un amapien sans email", async ({ page }) => {
    await removeUserEmails(page, "amapien");

    await goToStep3(page, "Prenom amapien NOM AMAPIEN -");
    await page.locator('[name="contrat_subscribe3[confirmSign]"]').check();
    await page.locator('[name="contrat_subscribe3[confirmCharter]"]').check();
    await expect(page.locator(".alert-danger")).toHaveCount(1);
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".container table tbody tr th")).toContainText(
      "NOM AMAPIEN"
    );
    await expect(
      page.locator(
        ".container table tbody tr td:nth-child(6) button.dropdown-toggle"
      )
    ).toHaveText("");
    await expectMailCount(0);
  });

  test("Ajouter un contrat signé à un amapien impossible pour un amapien supprimé", async ({
    page,
  }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page.goto("/contrat_signe/accueil_amap");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });
    await page
      .getByRole("link", { name: "Ajouter le contrat d'un amapien" })
      .click();
    await expect(
      page.locator('select[name="contrat_signe_new_force[amapien]"]')
    ).not.toHaveText(/amapien@test\.amap-aura\.org/);
  });
});
