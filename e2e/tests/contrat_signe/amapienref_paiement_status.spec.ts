import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import {
  expectMailCount,
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { clickMainMenuLink, subscribeContract } from "../../support/shorcut";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  testPaiementStatutTest,
  testPaiementSubscribeContract,
} from "./_paiements";

test.describe("Comme amapien référent, je doit pouvoir changer le statut de paiement des contrats", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Comme amapien référent, je doit pouvoir changer le statut de paiement des contrats", async ({
    page,
  }) => {
    await testPaiementSubscribeContract(page);
    await login(page, "amapienref");
    await clickMainMenuLink(page, "Gestionnaire référent");
    await clickMainMenuLink(page, "Gestion des contrats signés");
    await page
      .locator('[name="search_contrat_signe_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="search_contrat_signe_amap[mc]"]')
      .selectOption({ label: "contrat 1" });

    await testPaiementStatutTest(page);
  });
});
