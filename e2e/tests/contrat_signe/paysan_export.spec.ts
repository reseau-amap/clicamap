import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  associerAmapienReferentFerme,
  associerPaysanFerme,
  fermeAdherentCurrentYear,
  subscribeContract,
} from "../../support/shorcut";

test.describe("Comme paysan je doit pouvoir faire un export comptable de mes contrats", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Cacher le lien si la ferme du paysan n'est pas adhérente", async ({
    page,
  }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestion des contrats/)
      .first()
      .click();
    await expect(
      page.locator(".dropdown.open .dropdown-menu")
    ).not.toContainText("Exports comptable");
  });

  test("Afficher le lien d'export au moins une ferme du paysan est adhérente", async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestion des contrats/)
      .first()
      .click();
    await page
      .getByText(/Exports comptable/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Exports comptable");
    await expect(page.locator("#dropdownMenuDownload")).not.toBeVisible();
    await page
      .locator('[name="search_contrat_signe_paysan[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(page.locator("#dropdownMenuDownload")).toBeVisible();
    await page
      .locator('[name="search_contrat_signe_paysan[mc]"]')
      .selectOption({ label: "contrat 1" });
    await expect(page.locator("#dropdownMenuDownload")).toBeVisible();
  });

  test("Désactiver l'export pour les fermes pour lesquelles le paysan n'est pas adhérent", async ({
    page,
  }) => {
    await associerAmapienReferentFerme(
      page,
      "amap2",
      "Prenom amapien 2 NOM AMAPIEN 2",
      "Ferme 2"
    );
    await associerPaysanFerme(page, "paysan", "Ferme 2");
    await fermeAdherentCurrentYear(page, "ferme");
    await login(page, "paysan");
    await page
      .getByText(/Gestion des contrats/)
      .first()
      .click();
    await page
      .getByText(/Exports comptable/)
      .first()
      .click();
    await expect(
      page.locator('[name="search_contrat_signe_paysan[amap]"]')
    ).toContainText("amap test");
    await expect(
      page.locator('[name="search_contrat_signe_paysan[amap]"]')
    ).not.toContainText("amap test 2");
  });
});
