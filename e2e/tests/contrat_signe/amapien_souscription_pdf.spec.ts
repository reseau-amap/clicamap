import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractSubscriptionOwn } from "../../support/shorcut";
import { readPdfFromHref } from "../../support/shorcut_pdf";

test.describe("Comme amapien, je doit avoir un apercu du contrat avant sa validation", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Creation d'un contrat jusqu'à l'étape de validation", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("1");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await expect(page.locator("form")).toContainText(
      "J'accepte les termes de  mon contrat juridique"
    );
    await expect(page.locator("form")).toContainText(
      "Je confirme avoir pris connaissance de la Charte des AMAP"
    );

    const text = await readPdfFromHref(
      page,
      "http://localhost/contrat_signe/preview_pdf"
    );

    await expect(text).toContain(
      `Pour un panier de production
contrat 1
dans le cadre de l'AMAP amap test`
    );
    await expect(text).toContain(
      `Le présent contrat est signé entre :
Madame/Monsieur : Prenom amapien NOM AMAPIEN
Résidant : adresse 69001 Lyon 1er Arrondissement`
    );
    await expect(text).toContain(
      `La ferme : ferme
Production de production
Nom de l’entité juridique : ferme
Résidant : 58 Rue Raulin 69007 Lyon 7e Arrondissement`
    );
    await expect(text).toContain(
      `Article 4 : Durée du contrat
Le contrat court du 01/01/2030 au 30/04/2030 et comprend 18 livraisons.`
    );
    await expect(text).toContain(
      `6.1 Le/La paysan.ne s’engage
Type de panierPrix du panier TTCRegul de poids
produit 1 / cond11,00 €Non

produit 2 / cond22,00 €Non
Le prix du panier est constant tout au long de la durée du contrat. Toutefois, en cas d’aléas
sur la ferme du·de·la paysan·ne en AMAP, et avec l’accord de l’amapien·ne, le prix du panier
pourra être revalorisé pendant la durée de l’engagement solidaire et nécessite un avenant au
contrat initial précisant l’écart de prix.
Le/La paysan.ne s’engage au(x) date(s) suivante(s) :
produit 1produit 2
01/01/2030 1 1
08/01/2030 1 1
15/01/2030 1 1
22/01/2030 1 1
29/01/2030 1 1
05/02/2030 1 1
12/02/2030 1 1
19/02/2030 1 1
26/02/2030 1 1
05/03/2030 1 1
12/03/2030 1 1
19/03/2030 1 1
26/03/2030 1 1
02/04/2030 1 1
09/04/2030 1 1
16/04/2030 1 1
23/04/2030 1 1
30/04/2030 1 1`
    );
    await expect(text).toContain(
      `6.2 L’amapien.ne s’engage
produit 1 / cond1
01/01/20301
08/01/20301
15/01/20301
22/01/20301
29/01/20301`
    );
    await expect(text).toContain(
      `6.3 Modalités de règlement
Le règlement se fait à l'ordre de ferme 1 ordre cheque par Chèque.
modalite
Le(s) règlement(s) est/sont encaissé(s) à la/aux date(s) suivante(s) :

Nombre de règlementDate d’encaissementMontant
101/01/20205,00 €`
    );
    await expect(text).toContain(
      `L’amapien.ne
prenom amapien nom amapien
Le/La Paysan.ne
paysan_prenom paysan_nom`
    );
  });
});
