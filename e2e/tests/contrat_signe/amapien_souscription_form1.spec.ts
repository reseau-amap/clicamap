import { test, expect, Page } from "@playwright/test";
import {
  forceContratDepassement,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";

import {
  createValidatedContractWithCustomDelivery,
  gotoContractStep4,
  gotoContractStep5,
  gotoContractSubscriptionOwn,
  validatePaysanContrat,
} from "../../support/shorcut";
import {
  checkCheckbox,
  checkRadio,
  clickButtonOnLine,
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

test.describe("Souscrire à un nouveau contrats signé en tant que amapien - form 1", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Affichage de nom de l'amapien lors de la souscription", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await expect(page.locator("p.h4")).toHaveText("Prenom amapien NOM AMAPIEN");
  });

  test("Affichage d'un récapitulatif du contrat sur la page de selection des quantités", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await expect(page.locator(".alert-info")).toContainText(
      "Contrat : contrat 1"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Lieu de livraison : ll amap test"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Nombre de livraisons plancher/plafond : 5/5"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Produits identiques toutes les livraisons amapien : OUI"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Déplacement amapien sur date avec/sans livraison : NON"
    );
    await expect(page.locator(".alert-info")).toContainText("Ferme : ferme");
    await expect(page.locator(".alert-info")).toContainText(
      "Délai de la ferme : 2 jour(s)"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Date de fin de souscription : 01/01/2030"
    );
    await expect(page.locator(".alert-info")).toContainText(
      "Produits identiques toutes les livraisons paysan : OUI"
    );
  });

  test("Modification de l'affichage du plafond pour les contrat avec depassement", async ({
    page,
  }) => {
    await forceContratDepassement(page, "Contrat 1");
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await expect(page.locator(".alert-info")).toContainText(
      "Nombre de livraisons plancher/plafond : 5/18"
    );
  });

  test("Impossible de valider la commande si le nombre de commandes est inférieur au plancher", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_2][qty]"]')
      .fill("1");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Vous devez sélectionner exactement 5 livraisons"
    );
  });

  test("Impossible de valider la commande si le nombre de commandes est supérieure au plafond", async ({
    page,
  }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][6_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][6_1][qty]"]')
      .fill("1");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Vous devez sélectionner exactement 5 livraisons"
    );
  });

  test("Vérification livraisons identiques amapien", async ({ page }) => {
    await login(page, "amapien");
    await gotoContractSubscriptionOwn(page);
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][1_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][2_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][3_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][4_1][qty]"]')
      .fill("1");
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][5_1][qty]"]')
      .fill("2");
    await page
      .getByText(/Suivant/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Toutes les livraisons doivent être identiques"
    );
  });
});
