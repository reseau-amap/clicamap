import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { desactiverAmapien } from "../../support/shorcut";

test.describe("Comme amap, je ne doit pas pouvoir forcer la souscrition d'un amapien désactivé", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Forcer la souscription possible si pas désactivé", async ({ page }) => {
    await login(page, "amap");
    await page.goto("/contrat_signe/accueil_amap");
    await page.goto("/contrat_signe/contrat_new_force/1");
    await expect(
      page.locator('[name="contrat_signe_new_force[amapien]"]')
    ).toContainText("amapien@test.amap-aura.org");
  });

  test("Forcer souscription impossible pour les amapiens désactivés", async ({
    page,
  }) => {
    await desactiverAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page.goto("/amapien/search_amap");
    await page.goto("/contrat_signe/accueil_amap");
    await page.goto("/contrat_signe/contrat_new_force/1");
    await expect(
      page.locator('[name="contrat_signe_new_force[amapien]"]')
    ).not.toContainText("amapien@test.amap-aura.org");
  });
});
