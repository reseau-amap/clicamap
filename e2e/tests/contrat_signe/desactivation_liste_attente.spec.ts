import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Comme amapien, je ne doit pas pouvoir souscrire un contrat si je suis sur liste d'attente", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Souscription possible si pas en liste d'attente", async ({ page }) => {
    await login(page, "amapien");
    await page.goto("/contrat_signe/contrat_own_new/");
    await expect(page.locator(".container table")).toContainText("Souscrire");
    await expect(page.locator(".container table")).not.toContainText(
      "Liste d'attente"
    );
  });

  test("Souscription impossible en cas de liste d'attente", async ({
    page,
  }) => {
    await login(page, "amap");
    await page.goto("/amapien/search_amap");
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Modifier la liste d'attente de l'amapien"
    );
    await page
      .locator('[name="amapien_liste_attente[fermeAttentes][]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await login(page, "amapien");
    await page.goto("/contrat_signe/contrat_own_new/");
    await expect(page.locator(".container table")).not.toContainText(
      "Souscrire"
    );
    await expect(page.locator(".container table")).toContainText(
      "Liste d'attente"
    );
  });

  test("Forcer la souscription possible si pas en liste d'attente", async ({
    page,
  }) => {
    await login(page, "amap");
    await page.goto("/contrat_signe/contrat_new_force/1");
    await expect(
      page.locator('[name="contrat_signe_new_force[amapien]"]')
    ).toContainText("amapien@test.amap-aura.org");
  });

  test("Souscription impossible en cas de liste d'attente - force", async ({
    page,
  }) => {
    await login(page, "amap");
    await page.goto("/amapien/search_amap");
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Modifier la liste d'attente de l'amapien"
    );
    await page
      .locator('[name="amapien_liste_attente[fermeAttentes][]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await login(page, "amap");
    await page.goto("/contrat_signe/contrat_new_force/1");
    await expect(
      page.locator('[name="contrat_signe_new_force[amapien]"]')
    ).not.toContainText("amapien@test.amap-aura.org");
  });
});
