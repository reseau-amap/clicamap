import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

test.describe("Vérification des liens des réseaux avec le profil super-admin", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`J'ai accès aux liens depuis l'accueil`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/evenement");
    await page
      .getByText(/Réseaux/)
      .first()
      .click();
    await page
      .getByText(/Régions & Départements/)
      .first()
      .click();
    await expect(page).toHaveURL("http://localhost/region");
    await expect(page.locator(".alert-info")).toHaveText(
      /À l'aide des actions de cette liste, vous pourrez ajouter des administrateurs aux régions ainsi qu'aux départements\./
    );
    await expect(page.locator(".container table")).toHaveText(/Bretagne/);
  });
});
