import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Accès à la fonctionnalité d'import", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Accès non autorisé pour l'amapien`, async ({ page }) => {
    await login(page, "amapien");
    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Autres documents");
    await expect(page.locator("body")).not.toContainText("Import");
  });
});
