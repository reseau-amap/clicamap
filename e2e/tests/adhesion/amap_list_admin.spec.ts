import { test, expect, Page } from "@playwright/test";

import { requestResetDb } from "../../support/shorcut_request";
import {
  expectMailShoudBeReceived,
  mailClean,
  mailClickLink,
} from "../../support/shorcut_mail";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { supprimerAmap } from "../../support/shorcut";

test.describe("Ajouter un produit à la liste d'une AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await mailClean();
  });

  const importByUser = async (page: Page, user: string) => {
    await login(page, user);
    await page.goto("/adhesion/import");

    await page
      .locator('input[name="import_amap"]')
      .setInputFiles("e2e/data/Import_adhesion_amap_success_amap.xls");

    await page.locator('button[name="import_amap"]').click();
    await flashMessageShouldContain(page, "Import validé");
    await page.goto("/adhesion/amap");
    await expect(
      page.locator(".container table.table-datatable")
    ).toContainText("Brouillon");
    await expect(
      page.locator(".container table.table-datatable")
    ).toContainText("amap test");
    await page.locator(".js-tableselect-all").click();
  };

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Lancement de la génération ${user}`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "1 adhésions générées.");
      await expect(
        page.locator(".container table.table-datatable")
      ).toContainText("Généré");
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Suppression des recus ${user}`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Supprimer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "1 adhésions supprimées avec succès."
      );
      await expect(
        page.locator(".container table.table-datatable")
      ).toHaveCount(0);
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Liste des recus avec des amap supprimées ${user}`, async ({
      page,
    }) => {
      await importByUser(page, user);

      await supprimerAmap(page, "amap@test.amap-aura.org");
      await login(page, user);
      await page.goto("/adhesion/amap");
      await expect(page.locator("body")).not.toContainText("14A0103");
      await page
        .locator('[name="search_adhesion_amap[amapSupprimee]"]')
        .check();
      await expect(page.locator("body")).toContainText("14A0103");
      await expect(page.locator("body")).not.toContainText("amap test");
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Aucune adhésion en attente : aucun reçu ne sera généré."
      );
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Notifier des recus ${user} - recus brouillon`, async ({ page }) => {
      await importByUser(page, user);
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Aucune adhésion générée sélectionnée !"
      );
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Notifier des recus ${user} - recus générés`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "1 adhésions générées.");
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Notifications envoyées avec succès."
      );
      await expectMailShoudBeReceived(
        "Un nouveau document est disponible sur votre profil clic'AMAP",
        ["amap@test.amap-aura.org"]
      );
      await login(page, "amap");
      await mailClickLink(page, 0);
      await expect(page.locator("h3")).toContainText("Mes reçus");
      await expect(
        page.locator(".container table.table-datatable")
      ).toContainText("Reçu année 2019");
    });
  });
});
