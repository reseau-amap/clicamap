import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Une amap peut lister ses propres recus", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const importFile = async (page: Page, file: string) => {
    await login(page, "admin_aura");
    await page.getByText(/Reçus/).first().click();
    await page
      .locator(".dropdown.open")
      .getByText(/Import/)
      .first()
      .click();
    await page.locator('input[name="import_amap"]').setInputFiles(file);
    await page.locator('button[name="import_amap"]').click();
    await flashMessageShouldContain(page, "Import validé");
    await page.getByText(/Reçus/).first().click();
    await page.locator(".dropdown.open").getByText(/AMAP/).first().click();
    await page.locator(".js-tableselect-all").click();
  };

  test(`Affichage des contrats générés pour l'utilisateur`, async ({
    page,
  }) => {
    await importFile(page, "e2e/data/Import_adhesion_amap_success_amap.xls");
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "1 adhésions générées.");
    await login(page, "amap");
    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Mes reçus AMAP");
    await expect(page.locator(".container table")).toHaveText(
      /Reçu année 2019/
    );
  });

  test(`Masquer des contrats brouillons pour l'utilisateur`, async ({
    page,
  }) => {
    await importFile(page, "e2e/data/Import_adhesion_amap_success_amap.xls");
    await login(page, "amap");
    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Mes reçus AMAP");
    await expect(page.locator(".container table")).not.toHaveText(
      /Reçu année 2019/
    );
  });

  test(`Masquer des contrats générés pour un autre utilisateur`, async ({
    page,
  }) => {
    await importFile(page, "e2e/data/Import_adhesion_amap_success_amap2.xls");
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "1 adhésions générées.");
    await login(page, "amap");
    await clickMainMenuLink(page, "Mon compte");
    await clickMainMenuLink(page, "Mes reçus AMAP");
    await expect(page.locator(".container table")).not.toHaveText(
      /Reçu année 2019/
    );
  });
});
