import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Import des reçus amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Import avec succès ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/adhesion/import");
      await page
        .locator('input[name="import_amap"]')
        .setInputFiles("e2e/data/Import_adhesion_amap_success_amap.xls");
      await page.locator('button[name="import_amap"]').click();
      await flashMessageShouldContain(page, "Import validé");
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Import avec erreur ${user}`, async ({ page }) => {
      await login(page, user);
      await page.goto("/adhesion/import");
      await page
        .locator('input[name="import_amap"]')
        .setInputFiles("e2e/data/Import_adhesion_amap_error.xls");
      await page.locator('button[name="import_amap"]').click();
      await expect(page.locator(".alert-danger")).toHaveText(
        /Format de fichier invalide/
      );
    });
  });
});
