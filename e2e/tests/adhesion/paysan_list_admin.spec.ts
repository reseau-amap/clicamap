import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  expectMailShoudBeReceived,
  mailClickLink,
} from "../../support/shorcut_mail";

test.describe("Liste des recus paysans", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const importByUser = async (page: Page, user: string) => {
    await login(page, user);
    await page.goto("/adhesion/import");
    await page
      .locator('input[name="import_paysan"]')
      .setInputFiles("e2e/data/Import_adhesion_paysan_success_paysan.xls");
    await page.locator('button[name="import_paysan"]').click();
    await flashMessageShouldContain(page, "Import validé");
    await page.goto("/adhesion/paysan");
    await expect(page.locator(".container table")).toContainText("Brouillon");
    await expect(page.locator(".container table")).toContainText("ferme");
    await page.locator(".js-tableselect-all").click();
  };

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Lancement de la génération ${user}`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "1 adhésions générées.");
      await expect(page.locator(".container table")).toContainText("Généré");
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Suppression des recus${user}`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Supprimer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "1 adhésions supprimées avec succès."
      );
      await expect(page.locator(".container table")).toHaveCount(0);
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Notifier des recus ${user} - recus brouillon`, async ({ page }) => {
      await importByUser(page, user);
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Aucune adhésion générée sélectionnée !"
      );
    });
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Notifier des recus ${user} - recus générés`, async ({ page }) => {
      await importByUser(page, user);
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "1 adhésions générées.");
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Notifications envoyées avec succès."
      );
      await expectMailShoudBeReceived(
        "Un nouveau document est disponible sur votre profil clic'AMAP",
        ["paysan@test.amap-aura.org"]
      );
      await login(page, "paysan");
      await mailClickLink(page, 0);
      await expect(page.locator("h3")).toContainText("Mes reçus");
      await expect(await page.locator(".container table")).toContainText(
        "Reçu année 2020"
      );
    });
  });
});
