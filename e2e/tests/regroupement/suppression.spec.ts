import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { mailClean } from "../../support/shorcut_mail";

test.describe("Suppression d'un regroupement depuis différents profils", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  const db = ["admin", "admin_aura"];
  db.forEach((email) => {
    test(`Suppression du regroupement ${email}`, async ({ page }) => {
      await login(page, email);
      await page.goto("/evenement");
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des regroupements/)
        .first()
        .click();
      await clickMenuLinkOnLine(
        page,
        "Regroupement",
        "Supprimer le regroupement"
      );
      await page
        .locator('.modal.in [title="Confirmer la suppression du regroupement"]')
        .click();
      await flashMessageShouldContain(page, "Regroupement supprimé");
      await expect(page.locator(".container table")).not.toHaveText(
        /regroupement@test\.amap-aura\.org/
      );
    });
  });
});
