import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine, select2 } from "../../support/shorcut_actions";
import { mailClean } from "../../support/shorcut_mail";

test.describe("Gestion des administrateurs regroupement", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  const db = ["admin", "admin_aura"];
  db.forEach((email) => {
    test(`Ajout d'un administrateur regroupement ${email}`, async ({
      page,
    }) => {
      await login(page, email);
      await page.goto("/evenement");
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des regroupements/)
        .first()
        .click();
      await clickMenuLinkOnLine(
        page,
        "Regroupement",
        "Administrateurs du regroupement"
      );
      await select2(
        page,
        "user_select[user]",
        "Empty_prenom EMPTY_NOM (empty)"
      );
      await page
        .getByText(/Ajouter/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "L'administrateur a été ajouté avec succès."
      );
      await expect(
        page.locator(".btn-primary").getByText(/Empty_prenom EMPTY_NOM/)
      ).toHaveCount(1);
      await login(page, "empty");
      await expect(page.locator("nav")).toHaveText(
        /Gestionnaire regroupement Regroupement/
      );
    });
  });

  db.forEach((email) => {
    test(`Suppression d'un administrateur regroupement ${email}`, async ({
      page,
    }) => {
      await login(page, email);
      await page.goto("/evenement");
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des regroupements/)
        .first()
        .click();
      await clickMenuLinkOnLine(
        page,
        "Regroupement",
        "Administrateurs du regroupement"
      );
      await page
        .locator(".btn-primary")
        .getByText(/Regroupement_prenom REGROUPEMENT_NOM/)
        .first()
        .click();
      await page.locator(".modal.in").getByText(/OUI/).first().click();
      await flashMessageShouldContain(
        page,
        "L'administrateur a été retiré avec succès."
      );
      await expect(page.locator(".btn-primary")).not.toBeVisible();
    });
  });
});
