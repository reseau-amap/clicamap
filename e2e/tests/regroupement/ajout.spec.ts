import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Création d'un regroupement depuis différents profils", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const dbCreation = ["admin", "admin_aura"];
  dbCreation.forEach((email) => {
    test(`Création du regroupement ${email}`, async ({ page }) => {
      await login(page, email);
      await page.goto("/evenement");
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des regroupements/)
        .first()
        .click();
      await page
        .getByText(/Créer un regroupement/)
        .first()
        .click();
      await page
        .locator('[name="ferme_regroupement[nom]"]')
        .fill("Nouveau regroupement");
      await page
        .locator('[name="ferme_regroupement[common][siret]"]')
        .fill("77282657406395");
      await page
        .locator('[name="ferme_regroupement[common][adresseAdmin]"]')
        .fill("adresse");
      await page
        .locator('[name="ferme_regroupement[common][ville]"]')
        .fill("69001, LYON 1er Arrondissement");
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await flashMessageShouldContain(page, "Regroupement ajouté");
      await expect(page.locator(".container table")).toHaveText(
        /Nouveau regroupement/
      );
    });
  });

  const dbErreur = [
    [
      " ",
      "77282657406395",
      "adresse",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "Nouveau regroupement",
      " ",
      "adresse",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "Nouveau regroupement",
      "1111111111",
      "adresse",
      "69001, LYON 1er Arrondissement",
      "Le SIRET doit faire exactement 14 caractères.",
    ],

    [
      "Nouveau regroupement",
      "11111111111111",
      "adresse",
      "69001, LYON 1er Arrondissement",
      "Le SIRET est invalide.",
    ],

    [
      "Nouveau regroupement",
      "77282657406395",
      " ",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "Nouveau regroupement",
      "77282657406395",
      " ",
      " ",
      'Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      "Nouveau regroupement",
      "77282657406395",
      " ",
      "invalid",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],
  ];

  dbErreur.forEach((row, i) => {
    test(`Test des messages d'erreur - ${i}`, async ({ page }) => {
      await login(page, "admin");
      await page.goto("/evenement");
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des regroupements/)
        .first()
        .click();
      await page
        .getByText(/Créer un regroupement/)
        .first()
        .click();
      await page.locator('[name="ferme_regroupement[nom]"]').fill(row[0]);
      await page
        .locator('[name="ferme_regroupement[common][siret]"]')
        .fill(row[1]);
      await page
        .locator('[name="ferme_regroupement[common][adresseAdmin]"]')
        .fill(row[2]);
      await page
        .locator('[name="ferme_regroupement[common][ville]"]')
        .fill(row[3]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator(".alert-danger").getByText(row[4])).toHaveCount(
        1
      );
    });
  });
});
