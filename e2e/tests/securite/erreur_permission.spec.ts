import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme utilisateur, je doit retourner sur la page d'accueil en cas de tentative d'accès invalide", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Accès invalide`, async ({ page }) => {
    await login(page, "amapien");
    await page.goto("/amap");
    await expect(page).toHaveURL("http://localhost/portail/connexion");
    await flashMessageShouldContain(
      page,
      "Vous n'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter."
    );

    // Vérifie que l'on est bien déconnecte
    await page.goto("/evenement");
    await expect(page).toHaveURL("http://localhost/portail/connexion");
  });
});
