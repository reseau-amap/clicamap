import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Comme utilisateur, je devrais pouvoir accéder à la gestion du type de production d'une ferme", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Type production - Accès par le menu comme paysan`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Ma ferme/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "ferme", "Types de production");
    await expect(page).toHaveURL(/\/ferme\/type_production_display\/1/);
  });

  test(`Type production - Accès par le menu comme admin`, async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire/)
      .first()
      .click();
    await page
      .getByText(/Gestion des fermes/)
      .first()
      .click();
    await page
      .locator('[name="search_ferme[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "ferme", "Types de production");
    await expect(page).toHaveURL(/\/ferme\/type_production_display\/1/);
  });
});
