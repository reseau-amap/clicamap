import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme utilisateur, je devrait pouvoir ajouter des types de production aux fermes", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Accès depuis l'interface`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/ferme/type_production_display/1");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(
      /Ajouter des types de production/
    );
    await expect(page).toHaveURL(/\/ferme\/type_production_creation\/1/);
  });

  test(`Ajout sans fichier`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/ferme/type_production_creation/1");
    await page
      .getByText(/Ajouter le type de production/)
      .first()
      .click();
    await expect(
      page.locator(
        'input[name="ferme_type_production_propose[certification]"] ~ .alert-danger'
      )
    ).toHaveText(/Vous n'avez pas sélectionné de fichier à envoyer\./);
  });

  test(`Ajout fichier mauvais type`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/ferme/type_production_creation/1");
    await page
      .locator('input[name="ferme_type_production_propose[certification]"]')
      .setInputFiles("e2e/data/img_50k.jpg");
    await page
      .getByText(/Ajouter le type de production/)
      .first()
      .click();
    await expect(
      page.locator(
        'input[name="ferme_type_production_propose[certification]"] ~ .alert-danger'
      )
    ).toHaveText(
      /Le type de fichier que vous tentez d'envoyer n'est pas autorisé\./
    );
  });

  test(`Ajout de type de production par défaut`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/ferme/type_production_creation/1");
    await page
      .locator('input[name="ferme_type_production_propose[certification]"]')
      .setInputFiles("e2e/data/pdf_11m.pdf");
    await page
      .locator('select[name="ferme_type_production_propose[typeProduction]"]')
      .selectOption({ label: "Autres / divers" });
    await page
      .getByText(/Ajouter le type de production/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le type de production a été ajouté avec succès."
    );
    await expect(page.locator(".container table")).toHaveText(
      /Autres \/ divers/
    );
  });

  test(`Ajout multiple du meme type de production`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/ferme/type_production_creation/1");
    await page
      .locator('input[name="ferme_type_production_propose[certification]"]')
      .setInputFiles("e2e/data/pdf_11m.pdf");
    await page
      .locator('select[name="ferme_type_production_propose[typeProduction]"]')
      .selectOption({ label: "Autres / divers" });
    await page
      .getByText(/Ajouter le type de production/)
      .first()
      .click();
    await page.goto("/ferme/type_production_creation/1");
    await page
      .locator('input[name="ferme_type_production_propose[certification]"]')
      .setInputFiles("e2e/data/pdf_11m.pdf");
    await page
      .locator('select[name="ferme_type_production_propose[typeProduction]"]')
      .selectOption({ label: "Autres / divers" });
    await page
      .getByText(/Ajouter le type de production/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Le produit existe déjà dans la base de donnée\./
    );
  });
});
