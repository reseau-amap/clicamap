import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Désactication d'un paysan", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Désactivation d'un paysan depuis le profil amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .locator('[name="search_paysan[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "paysan@test.amap-aura.org",
      "Désactiver le compte du paysan"
    );
    await flashMessageShouldContain(
      page,
      "L'état du paysan a été modifié avec succès."
    );
    const menu = page.locator("tr").filter({
      has: page
        .locator("td")
        .getByText(/paysan@test\.amap-aura\.org/)
        .first(),
    });
    await menu.locator(".dropdown-toggle").click();
    await expect(menu.locator(".dropdown-menu")).toHaveText(
      /Activer le compte du paysan/
    );
    await login(page, "paysan");
    await expect(page.locator("nav")).not.toHaveText(/Gestionnaire ferme/);
    await expect(page.locator("nav")).not.toHaveText(/Gestion des contrats/);
  });
});
