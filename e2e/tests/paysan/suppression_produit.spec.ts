import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("suppression dun produit", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`suppression d'un produit en tant que paysan`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto("/produit/accueil/1");
    await clickMenuLinkOnLine(page, "Produit 1", "Supprimer le produit");
    await expect(page.locator(".modal.in")).toContainText(
      'Voulez-vous vraiment supprimer le produit "produit 1" ?'
    );
    await expect(page.locator(".modal.in .alert-danger")).toContainText(
      "Attention : ce produit est peut-être fournie à une autre AMAP !"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "Produit supprimé avec succès.");
    await expect(page.locator(".container table")).not.toContainText(
      "Produit 1"
    );
  });
});
