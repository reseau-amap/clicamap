import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("suppression d'un paysan", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_rhone" /*, "admin_aura"*/].forEach((email) => {
    test(`suppression d'un paysan en tant qu'administrateur ${email}`, async ({
      page,
    }) => {
      await login(page, email);
      await page.goto("/paysan");
      await page
        .locator('[name="search_paysan[region]"]')
        .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
      await clickMenuLinkOnLine(
        page,
        "paysan@test.amap-aura.org",
        "Supprimer le compte du paysan"
      );
      await expect(page.locator(".modal.in")).toContainText(
        'Voulez-vous vraiment supprimer le profil de "Paysan_prenom PAYSAN_NOM" ?'
      );
      await page.locator(".modal.in").getByText(/OUI/).first().click();
      await flashMessageShouldContain(
        page,
        "Le paysan a été supprimé avec succès."
      );
      await expect(page.locator(".container table")).not.toContainText(
        "paysan@test.amap-aura.org"
      );
    });
  });
});
