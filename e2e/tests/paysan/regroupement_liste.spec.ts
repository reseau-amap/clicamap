import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { fermeAssocieeRegroupement } from "../../support/shorcut";

test.describe("Liste des paysans associée au regroupement", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Liste des paysans associée au regroupement`, async ({ page }) => {
    await fermeAssocieeRegroupement(page, "Ferme", "Regroupement");
    await login(page, "regroupement");
    await page
      .getByText(/Gestionnaire regroupement/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await expect(page.locator(".container table")).toContainText("PAYSAN_NOM");
    await expect(page.locator(".container table")).not.toContainText(
      "PAYSAN_NOM2"
    );
  });
});
