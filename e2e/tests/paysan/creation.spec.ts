import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { fermeAssocieeRegroupement } from "../../support/shorcut";
import { getSelectedOption } from "../../support/shorcut_actions";

test.describe("Ajout d'un paysan", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Ajout d'un paysan comme admin - pas de ferme par défaut`, async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await expect(
      page.locator('select[name="paysan_creation[ferme]"]')
    ).toHaveValue("");
  });

  test(`Ajout d'un paysan comme admin - ferme obligatoire`, async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][username]"]')
      .fill("username");
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("new user first name");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("lastName");

    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(
      page.locator('[name="paysan_creation[ferme]"] ~ .alert-danger')
    ).toContainText("Merci de sélectionner une ferme");
  });

  test(`Ajout d'un paysan comme admin`, async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][username]"]')
      .fill("username");
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("new user first name");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("lastName");
    await page
      .locator('[name="paysan_creation[ferme]"]')
      .selectOption({ label: "ferme" });

    await page
      .locator('[name="paysan_creation[user][ville]"]')
      .fill("69001, Lyon 1er Arrondissement");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le paysan a été créé avec succès. Vous pouvez en rajouter un autre."
    );
    expect(
      await getSelectedOption(page.locator('[name="paysan_creation[ferme]"]'))
    ).toBe("ferme");
    await page
      .getByText(/Annuler/)
      .first()
      .click();
    await page
      .locator('[name="search_paysan[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(page.locator(".container table")).toContainText(
      "NEW USER FIRST NAME"
    );
  });

  test(`Ajout d'un paysan comme admin - vérification doublon nom utilisateur`, async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][username]"]')
      .fill("amapien");
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("new user first name");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("lastName");
    await page
      .locator('[name="paysan_creation[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .locator('[name="paysan_creation[user][ville]"]')
      .fill("69001, Lyon 1er Arrondissement");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Cette valeur est déjà utilisée."
    );
  });

  test(`Ajout d'un paysan comme regroupement - vérification fermes selectionnables`, async ({
    page,
  }) => {
    await fermeAssocieeRegroupement(page, "Ferme", "Regroupement");
    await login(page, "regroupement");
    await page
      .getByText(/Gestionnaire regroupement Regroupement/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await expect(page.locator('[name="paysan_creation[ferme]"]')).toContainText(
      "ferme"
    );
    await expect(
      page.locator('[name="paysan_creation[ferme]"]')
    ).not.toContainText("ferme 2");
  });

  test(`Ajout d'un paysan comme regroupement`, async ({ page }) => {
    await fermeAssocieeRegroupement(page, "Ferme", "Regroupement");
    await login(page, "regroupement");
    await page
      .getByText(/Gestionnaire regroupement Regroupement/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][username]"]')
      .fill("username");
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("new user first name");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("lastName");
    await page
      .locator('[name="paysan_creation[user][ville]"]')
      .fill("69001, Lyon 1er Arrondissement");
    await page
      .locator('[name="paysan_creation[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le paysan a été créé avec succès. Vous pouvez en rajouter un autre."
    );
    await page
      .getByText(/Annuler/)
      .first()
      .click();
    await expect(page.locator(".container table")).toContainText(
      "NEW USER FIRST NAME"
    );
  });

  test("Test de la génération du nom utilisateur", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("test prénom");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("test nom");
    await expect(
      page.locator('[name="paysan_creation[user][username]"]')
    ).toHaveValue("test-prenom.test-nom");
  });

  test("Test désactivation génération du nom utilisateur si fourni par utilisateur", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await page
      .getByText(/Créer un profil paysan/)
      .first()
      .click();
    await page
      .locator('[name="paysan_creation[user][username]"]')
      .fill("test username");
    await page
      .locator('[name="paysan_creation[user][name][firstName]"]')
      .fill("test prénom");
    await page
      .locator('[name="paysan_creation[user][name][lastName]"]')
      .fill("test nom");
    await expect(
      page.locator('[name="paysan_creation[user][username]"]')
    ).toHaveValue("test username");
  });
});
