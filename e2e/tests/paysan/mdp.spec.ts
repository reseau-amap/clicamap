import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { desactiverPaysan } from "../../support/shorcut";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Envoyer un mot de passe à un paysan", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Envoyer un mot de passe à un paysan", async ({ page }) => {
    await desactiverPaysan(page, "paysan@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des paysans/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "paysan@test.amap-aura.org",
      "Envoyer un mot de passe au paysan"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Voulez-vous vraiment envoyer un mot de passe au paysan Paysan_prenom PAYSAN_NOM ?"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le mot de passe a été envoyé avec succès."
    );
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "paysan@test.amap-aura.org",
    ]);
  });
});
