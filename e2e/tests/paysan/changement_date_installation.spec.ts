import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("En tant que paysan je souhaite modifier ma date d'installation", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Format date invalide`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Ma ferme/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "ferme", "Modifier la ferme");
    await page.locator('[name="ferme[dateInstallation]"]').clear();
    await page.locator('[name="ferme[dateInstallation]"]').fill("invalid");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Une date doit être au format suivant : Y-m-d \(exemple : 2018-06-28\)/
    );
  });

  test(`Format date valide`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Ma ferme/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "ferme", "Modifier la ferme");
    await page.locator('[name="ferme[dateInstallation]"]').clear();
    await page.locator('[name="ferme[dateInstallation]"]').fill("2018-06-28 ");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "La ferme a été modifiée avec succès."
    );
  });
});
