import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

const goToPage = async (page: Page) => {
  await login(page, "admin");
  await page
    .getByText(/Réseaux/)
    .first()
    .click();
  await page
    .getByText(/Gestion des utilisateurs/)
    .first()
    .click();
  await expect(page.locator("h3")).toHaveText(/Gestion des utilisateurs/);
};

test.describe("Liste des utilisateurs", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant que super admin, je ne doit lister aucun utilisateur par défaut", async ({
    page,
  }) => {
    await goToPage(page);
    await expect(page.locator(".alert-success")).toHaveText(
      /0 résultats pour cette recherche/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter par région", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(page.locator(".alert-success")).toHaveText(
      /15 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[region]"]')
      .selectOption({ label: "BOURGOGNE-FRANCHE-COMTÉ" });
    await expect(page.locator(".alert-success")).toHaveText(
      /0 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter le super admin", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[superAdmin]"]')
      .selectOption({ label: "O" });
    await expect(page.locator(".alert-success")).toHaveText(
      /1 résultat pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[superAdmin]"]')
      .selectOption({ label: "N" });
    await expect(page.locator(".alert-success")).toHaveText(
      /14 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter les admins", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[admin]"]')
      .selectOption({ label: "O" });
    await expect(page.locator(".alert-success")).toHaveText(
      /5 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[admin]"]')
      .selectOption({ label: "N" });
    await expect(page.locator(".alert-success")).toHaveText(
      /10 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter les paysans", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[paysan]"]')
      .selectOption({ label: "O" });
    await expect(page.locator(".alert-success")).toHaveText(
      /2 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[paysan]"]')
      .selectOption({ label: "N" });
    await expect(page.locator(".alert-success")).toHaveText(
      /13 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter les amaps", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[amap]"]')
      .selectOption({ label: "O" });
    await expect(page.locator(".alert-success")).toHaveText(
      /2 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[amap]"]')
      .selectOption({ label: "N" });
    await expect(page.locator(".alert-success")).toHaveText(
      /13 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
  });

  test("En tant que super admin, je doit pouvoir filter les amapiens", async ({
    page,
  }) => {
    await goToPage(page);
    await page
      .locator('[name="search_user[amapien]"]')
      .selectOption({ label: "O" });
    await expect(page.locator(".alert-success")).toHaveText(
      /3 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /regroupement_prenom/
    );
    await page
      .locator('[name="search_user[amapien]"]')
      .selectOption({ label: "N" });
    await expect(page.locator(".alert-success")).toHaveText(
      /12 résultats pour cette recherche/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /empty_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_aura_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_rhone_prenom/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /prenom amapien 2/
    );
    await expect(page.locator(".container table.table")).not.toHaveText(
      /Prénom amapien référent/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /amap2_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /paysan_prenom2/
    );
    await expect(page.locator(".container table.table")).toHaveText(
      /regroupement_prenom/
    );
  });
});
