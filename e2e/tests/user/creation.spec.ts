import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Creation d'un utilisateur", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant que super admin, je doit pouvoir créer un utilisateur", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Réseaux/)
      .first()
      .click();
    await page
      .getByText(/Gestion des utilisateurs/)
      .first()
      .click();
    await page
      .getByText(/Ajouter un nouvel utilisateur/)
      .first()
      .click();
    await page.locator('[name="user_profil[username]"]').fill("username");
    await page
      .locator('[name="user_profil[name][firstName]"]')
      .fill("firstName");
    await page.locator('[name="user_profil[name][lastName]"]').fill("lastName");
    await page.locator('[name="user_profil[ville]"]').fill("59000, Lille");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Utilisateur ajouté");
    await page
      .locator('[name="search_user[region]"]')
      .selectOption({ label: "HAUTS-DE-FRANCE" });
    await expect(page.locator(".container table.table")).toHaveText(
      /firstName/
    );
  });

  test("En tant que super admin, je doit pouvoir annuler la création un utilisateur", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Réseaux/)
      .first()
      .click();
    await page
      .getByText(/Gestion des utilisateurs/)
      .first()
      .click();
    await page
      .getByText(/Ajouter un nouvel utilisateur/)
      .first()
      .click();
    await page
      .getByText(/Annuler/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Gestion des utilisateurs/);
  });
});
