import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { expectMailShoudBeReceived } from "../../support/shorcut_mail";

test.describe("Envoi du mdp", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant que super admin, je doit pouvoir envoyer le mot de passe à un utilisateur", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Réseaux/)
      .first()
      .click();
    await page
      .getByText(/Gestion des utilisateurs/)
      .first()
      .click();
    await page
      .locator('[name="search_user[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "admin_nom",
      "Envoyer un mot de passe à l'utilisateur"
    );
    await expect(page.locator(".modal.in")).toContainText(
      'Êtes-vous sûr envoyer un mot de passe à "Admin_prenom ADMIN_NOM"'
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le mot de passe a été envoyé avec succès."
    );
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "admin@test.amap-aura.org",
    ]);
  });
});
