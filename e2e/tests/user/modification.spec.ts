import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Modification d'un utilisateur", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant que super admin, je doit pouvoir modifier un utilisateur", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Réseaux/)
      .first()
      .click();
    await page
      .getByText(/Gestion des utilisateurs/)
      .first()
      .click();
    await page
      .locator('[name="search_user[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(page, "admin_nom", "Modifier le profil");
    await page.locator('[name="user_profil[name][firstName]"]').clear();
    await page
      .locator('[name="user_profil[name][firstName]"]')
      .fill("admin_prenom_modif");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Utilisateur modifié");
    await expect(page.locator(".container table.table")).toHaveText(
      /admin_prenom_modif/
    );
  });
});
