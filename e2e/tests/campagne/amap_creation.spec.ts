import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import {
  amapChangerLieuLivraison,
  amapDeFait,
  campagneCreationAmap,
} from "../../support/shorcut";

test.describe("Créer une campagne", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Créer une nouvelle campagne comme AMAP`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await expect(
      page.locator(".container table tbody tr:nth-child(1)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1)")
    ).toHaveText(/01\/01\/2030/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1)")
    ).toHaveText(/En cours/);
  });

  test(`Créer une nouvelle campagne comme AMAP de fait sans réseau associé`, async ({
    page,
  }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await amapChangerLieuLivraison(
      page,
      "amap@test.amap-aura.org",
      "Ll amap test",
      "59000, Lille"
    );
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "L'amap est une amap de fait et aucun admin réseau n'a été trouvé pour cette AMAP."
    );
    await expect(page.locator("h3")).toHaveText(
      /Gestion des campagnes d'adhésions et de dons/
    );
  });
});
