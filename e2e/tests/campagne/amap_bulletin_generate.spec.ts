import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  amapDeFait,
  campagneCreationAmap,
  campagneSouscription,
  supprimerAmapien,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { readPdf } from "../../support/shorcut_pdf";

const expectRecuAmapien = async (page: Page) => {
  await expect(
    page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
  ).toHaveText(/Oui/);
  await page
    .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
    .click();

  const pdfText = await readPdf(
    page,
    page
      .locator(".container table tbody tr:nth-child(1) .dropdown-menu-right")
      .getByText(/Télécharger le reçu/)
  );
  await expect(pdfText).toContain(`amap@test.amap-aura.org`);
  await expect(pdfText).toContain(`Numéro de reçu : 0000100001`);
  await expect(pdfText).toContain(`RECU 2011`);
  await expect(pdfText).toContain(`la somme de 4,00 €`);
  await expect(pdfText).toContain(`de l'Amapien Payeur`);
  await expect(pdfText).toContain(`par Méthode de paiement`);
  await expect(pdfText).toContain(`en date du 01/01/2020`);
  await expect(pdfText).toContain(`la somme de 1,00 € au titre de Montant 1`);
  await expect(pdfText).toContain(`la somme de 3,00 € au titre de Montant 2`);
  await expect(pdfText).toContain(`Champ libre`);
  await expect(pdfText).toContain(`Fait à Lyon 1er Arrondissement`);
  await expect(pdfText).toContain(`le 02/01/2000`);
};

test.describe("Générer les recus des bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Générer les recus des bulletins - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) .dropdown-menu-right"
      )
    ).not.toHaveText(/Télécharger le reçu/);
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");

    await expectRecuAmapien(page);

    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Autres documents/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(1)")
    ).toHaveText(/Campagne 1/);
    await page.locator(".container table tbody tr:nth-child(1) button").click();

    const pdfText2 = await readPdf(
      page,
      page
        .locator(".container table tbody tr:nth-child(1) .dropdown-menu-right")
        .getByText(/Télécharger le reçu/)
    );
    await expect(pdfText2).toContain(`Numéro de reçu : 0000100001`);

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await expect(
      page.locator(".container table tbody tr:nth-child(5)")
    ).toHaveText(/Année\(s\) d'adhésion :/);
    await expect(
      page.locator(".container table tbody tr:nth-child(5)")
    ).toHaveText(/2011/);
  });

  test(`Générer les recus des bulletins - amapien supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");
    await expectRecuAmapien(page);
  });

  test(`Générer les recus des bulletins - amap de fait`, async ({ page }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(page.getByText(/Générer les reçus/).first()).not.toBeVisible();
  });
});
