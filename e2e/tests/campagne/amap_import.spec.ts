import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { amapDeFait, campagneCreationAmap } from "../../support/shorcut";

test.describe("Importer des bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Import impossible pour les amaps - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(
      page.getByText(/Importer des bulletins/).first()
    ).not.toBeVisible();
  });

  test(`Générer les recus des bulletins - amap de fait`, async ({ page }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(
      page.getByText(/Importer des bulletins/).first()
    ).not.toBeVisible();
  });
});
