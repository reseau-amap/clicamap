import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { setAmapienAdmin } from "../../support/shorcut";

test.describe("Listes les campagnes de mon amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Masquer le selecteur d'amap si je n'ai qu'une seule AMAP`, async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(
      /Gestion des campagnes d'adhésions et de dons/
    );
    await expect(page.locator("form")).not.toBeVisible();
  });

  test(`Afficher le selecteur d'amap si j'ai plusieurs amaps`, async ({
    page,
  }) => {
    await setAmapienAdmin(page, "amap", "amap2@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(
      /Gestion des campagnes d'adhésions et de dons/
    );
    await expect(page.locator("form")).toBeVisible();
    await expect(page.locator('[name="search_campagne[amap]"]')).toHaveValue(
      ""
    );
  });
});
