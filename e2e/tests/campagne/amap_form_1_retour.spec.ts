import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { fillNoteEditor } from "../../support/shorcut_actions";

test.describe("Valider le retour au premier formulaire", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Récupération des informations lors du retour à l'étape 1`, async ({
    page,
  }) => {
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne");
    await page
      .locator('[name="campagne_form1[period][startAt]"]')
      .fill("2020-01-01");
    await page
      .locator('[name="campagne_form1[period][endAt]"]')
      .fill("2020-12-31");
    await page.locator("h3").click(); // hide popup
    await page
      .locator('[name="campagne_form1[montants][0][montant]"]')
      .fill("1");
    await page
      .locator('[name="campagne_form1[montants][0][titre]"]')
      .fill("Montant 1");
    await fillNoteEditor(
      page,
      "campagne_form1[montants][0][description]",
      "Description"
    );
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(page.locator('[name="campagne_form1[nom]"]')).toHaveValue(
      "Campagne"
    );
    await expect(
      page.locator('[name="campagne_form1[period][startAt]"]')
    ).toHaveValue("2020-01-01");
    await expect(
      page.locator('[name="campagne_form1[period][endAt]"]')
    ).toHaveValue("2020-12-31");
    await expect(
      page.locator('[name="campagne_form1[montants][0][montant]"]')
    ).toHaveValue("1.00");
    await expect(
      page.locator('[name="campagne_form1[montants][0][titre]"]')
    ).toHaveValue("Montant 1");
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="0"]'
      )
    ).toBeChecked();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="1"]'
      )
    ).not.toBeChecked();
    await expect(
      page.locator('[name="campagne_form1[montants][0][description]"]')
    ).toHaveValue("<p>Description<br></p>");
  });

  test(`Récupération des informations lors du retour à l'étape 1 - montant libre`, async ({
    page,
  }) => {
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne");
    await page
      .locator('[name="campagne_form1[period][startAt]"]')
      .fill("2020-01-01");
    await page
      .locator('[name="campagne_form1[period][endAt]"]')
      .fill("2020-12-31");
    await page.locator("h3").click(); // hide popup
    await page
      .locator('[name="campagne_form1[montants][0][montantLibre]"][value="1"]')
      .check();
    await page
      .locator('[name="campagne_form1[montants][0][titre]"]')
      .fill("Montant 1");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(page.locator('[name="campagne_form1[nom]"]')).toHaveValue(
      "Campagne"
    );
    await expect(
      page.locator('[name="campagne_form1[period][startAt]"]')
    ).toHaveValue("2020-01-01");
    await expect(
      page.locator('[name="campagne_form1[period][endAt]"]')
    ).toHaveValue("2020-12-31");
    await expect(
      page.locator('[name="campagne_form1[montants][0][titre]"]')
    ).toHaveValue("Montant 1");
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="0"]'
      )
    ).not.toBeChecked();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="1"]'
      )
    ).toBeChecked();
    await expect(
      page.locator('[name="campagne_form1[montants][0][montant]"]')
    ).not.toBeVisible();
  });

  test(`Récupération des informations lors du retour à l'étape 1 - ordre des montants`, async ({
    page,
  }) => {
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne");
    await page
      .locator('[name="campagne_form1[period][startAt]"]')
      .fill("2020-01-01");
    await page
      .locator('[name="campagne_form1[period][endAt]"]')
      .fill("2020-12-31");
    await page.locator("h3").click(); // hide popup
    await page.locator(".js-new").click();
    await page
      .locator('[name="campagne_form1[montants][0][titre]"]')
      .fill("Montant 1");
    await page
      .locator('[name="campagne_form1[montants][0][montant]"]')
      .fill("1");
    await page
      .locator('[name="campagne_form1[montants][1][titre]"]')
      .fill("Montant 2");
    await page
      .locator('[name="campagne_form1[montants][1][montant]"]')
      .fill("2");
    await page
      .locator("#campagne_form1_montants > .form-group:nth-child(2) .js-move")
      .dragTo(
        page.locator("#campagne_form1_montants > .form-group:nth-child(1)")
      );
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form1[montants][0][titre]"]')
    ).toHaveValue("Montant 2");
    await expect(
      page.locator('[name="campagne_form1[montants][1][titre]"]')
    ).toHaveValue("Montant 1");
  });
});
