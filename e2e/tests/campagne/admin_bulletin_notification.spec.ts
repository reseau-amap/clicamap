import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import {
  amapDeFait,
  campagneCreationAdmin,
  campagneSouscription,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { expectMailShoudBeReceived } from "../../support/shorcut_mail";

test.describe("Notifier les bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Notifier les recus des bulletins depuis actions - amap de fait ${role}`, async ({
      page,
    }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await campagneSouscription(page, "amapien", "Campagne 1");
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await page
        .locator('[name="search_campagne[amap]"]')
        .selectOption({ label: "amap test" });
      await page
        .getByText(/Liste des bulletins et reçus/)
        .first()
        .click();
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Notifier par email/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "Les notifications ont été envoyées."
      );
      await expectMailShoudBeReceived(
        "Un nouveau document est disponible sur votre profil clic'AMAP",
        ["amapien@test.amap-aura.org"]
      );
    });
  });
});
