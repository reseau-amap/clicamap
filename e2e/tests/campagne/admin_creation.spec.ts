import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { amapDeFait, campagneCreationAdmin } from "../../support/shorcut";

test.describe("Créer une campagne", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Créer une nouvelle campagne comme admin ${role}`, async ({
      page,
    }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await login(page, role);

      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await expect(page.locator("h3")).toHaveText(
        /Gestion des campagnes d'adhésions et de dons/
      );
      await expect(
        page.locator(".container table tbody tr:nth-child(1)")
      ).toHaveText(/Campagne 1/);
      await expect(
        page.locator(".container table tbody tr:nth-child(1)")
      ).toHaveText(/01\/01\/2030/);
      await expect(
        page.locator(".container table tbody tr:nth-child(1)")
      ).toHaveText(/En cours/);
    });
  });
});
