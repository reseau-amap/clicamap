import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

test.describe("Valider le premier formulaire de creation/modification", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Valider le nom non vide`, async ({ page }) => {
    await page.locator('[name="campagne_form1[nom]"]').fill("   ");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form1[nom]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
  });

  test(`Valider la période obligatoire`, async ({ page }) => {
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form1[period][endAt]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page.locator('[name="campagne_form1[period][startAt]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
  });

  test(`Valider le minimum de montants`, async ({ page }) => {
    await page.locator(".js-delete").click();
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator('form[name="campagne_form1"] > .alert-danger')
    ).toContainText("Merci d'indiquer au moins un montant");
  });

  test(`Valider le titre du montant`, async ({ page }) => {
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][titre]"] + .alert-danger'
      )
    ).toContainText("Cette valeur ne doit pas être vide.");
  });

  test(`Valider le montant existant si montant pas libre`, async ({ page }) => {
    // Default value
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="0"]'
      )
    ).toBeChecked();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montantLibre]"][value="1"]'
      )
    ).not.toBeChecked();
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montant]"] + .alert-danger'
      )
    ).toContainText("Le montant ne peut pas être vide.");
  });

  test(`Valider le montant minimum`, async ({ page }) => {
    await page
      .locator('[name="campagne_form1[montants][0][montant]"]')
      .fill("-1");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montant]"] + .alert-danger'
      )
    ).toContainText("Cette valeur doit être supérieure ou égale à 0.");
  });

  test(`Masquer montant si montant libre`, async ({ page }) => {
    await page
      .locator('[name="campagne_form1[montants][0][montantLibre]"][value="1"]')
      .check();
    await expect(
      page.locator('[name="campagne_form1[montants][0][montant]"]')
    ).not.toBeVisible();
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="campagne_form1[montants][0][montant]"] + .alert-danger'
      )
    ).not.toBeVisible();
  });
});
