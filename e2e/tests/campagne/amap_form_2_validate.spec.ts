import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

test.describe("Valider le second formulaire de creation/modification", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne");
    await page
      .locator('[name="campagne_form1[period][startAt]"]')
      .fill("2020-12-31");
    await page
      .locator('[name="campagne_form1[period][endAt]"]')
      .fill("2020-12-31");
    await page
      .locator('[name="campagne_form1[montants][0][montant]"]')
      .fill("1");
    await page
      .locator('[name="campagne_form1[montants][0][titre]"]')
      .fill("Montant 1");
  });

  test(`Valider la méthode de paiement non vide`, async ({ page }) => {
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 2 : Moyen de paiement/);
    await page.locator('[name="campagne_form2[paiementMethode]"]').fill("   ");
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form2[paiementMethode]"] + .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
  });
});
