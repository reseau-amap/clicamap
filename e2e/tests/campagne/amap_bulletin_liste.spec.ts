import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  campagneCreationAmap,
  campagneCreationInternal,
  campagneSouscription,
  setAmapienAdmin,
  supprimerAmapien,
} from "../../support/shorcut";
import { getSelectedOption } from "../../support/shorcut_actions";
import { readPdf } from "../../support/shorcut_pdf";

test.describe("Listes les campagnes disponibles comme AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Afficher un message si aucune campagne disponible`, async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun résultat pour cette recherche\./
    );
  });

  test(`Afficher les campagnes disponibles`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(
      page,

      "amap"
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /1 résultat pour cette recherche\./
    );
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(3)")
    ).toHaveText("");
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(4)")
    ).toHaveText(/nom amapien/);
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(5)")
    ).toHaveText(/prenom amapien/);
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(6)")
    ).toHaveText(/4,00 €/);
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(7)")
    ).toHaveText(/01\/01\/2020/);
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
  });

  test(`Afficher les bulletins meme si l'amapien est supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(
      page,

      "amap"
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /1 résultat pour cette recherche\./
    );
    await expect(
      page.locator("table tr:nth-child(1) td:nth-child(4)")
    ).toHaveText(/nom amapien/);

    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();

    const text = await readPdf(
      page,
      page
        .locator(".container table tbody tr:nth-child(1) .dropdown-menu-right")
        .getByText(/Télécharger le bulletin/)
    );
    await expect(text).toContain(`amap@test.amap-aura.org`);
    await expect(text).toContain(`Campagne 1`);
  });

  test(`Filter par mot clé`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(
      page,

      "amap"
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(page.locator("table")).toHaveText(/Campagne 1/);
    await page
      .locator('[name="search_campagne_bulletin[keyword]"]')
      .fill("campagne");
    await page
      .locator('[name="search_campagne_bulletin[keyword]"]')
      .press("Enter");
    await expect(page.locator("table")).toHaveText(/Campagne 1/);
    await page.locator('[name="search_campagne_bulletin[keyword]"]').clear();
    await page
      .locator('[name="search_campagne_bulletin[keyword]"]')
      .fill("invalid");
    await page
      .locator('[name="search_campagne_bulletin[keyword]"]')
      .press("Enter");
    await expect(page.locator(".alert-warning")).toContainText(
      "Aucun résultat pour cette recherche."
    );
  });

  test(`Filter par campagne - meme amap`, async ({ page }) => {
    await login(page, "amap");
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneCreationAmap(page, "AMAP", "Campagne 2");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 2");
    await login(
      page,

      "amap"
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    expect(
      await getSelectedOption(
        page.locator('[name="search_campagne_bulletin[campagne]"]')
      )
    ).toBe("");
    await expect(page.locator("table")).toHaveText(/Campagne 1/);
    await expect(page.locator("table")).toHaveText(/Campagne 2/);
    await page
      .locator('[name="search_campagne_bulletin[campagne]"]')
      .selectOption({ label: "Campagne 1" });
    await expect(page.locator("table")).not.toHaveText(/Campagne 2/);
    await expect(page.locator("table")).toHaveText(/Campagne 1/);
    await page
      .locator('[name="search_campagne_bulletin[campagne]"]')
      .selectOption({ label: "Campagne 2" });
    await expect(page.locator("table")).not.toHaveText(/Campagne 1/);
    await expect(page.locator("table")).toHaveText(/Campagne 2/);
    await page
      .locator('[name="search_campagne_bulletin[campagne]"]')
      .selectOption({ label: "" });
    await expect(page.locator("table")).toHaveText(/Campagne 1/);
    await expect(page.locator("table")).toHaveText(/Campagne 2/);
  });

  test(`Filter par campagne - multiples amap`, async ({ page }) => {
    await setAmapienAdmin(page, "amap", "amap2@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .locator('[name="search_campagne[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();

    await campagneCreationInternal(page, "Campagne 11");
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();

    await campagneCreationInternal(page, "Campagne 12");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .locator('[name="search_campagne[amap]"]')
      .selectOption({ label: "amap test 2" });
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();

    await campagneCreationInternal(page, "Campagne 21");
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();

    await campagneCreationInternal(page, "Campagne 22");

    await campagneSouscription(page, "amapien", "Campagne 11");
    await campagneSouscription(page, "amapien2", "Campagne 21");
    await login(
      page,

      "amap"
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .locator('[name="search_campagne[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    expect(
      await getSelectedOption(
        page.locator('[name="search_campagne_bulletin[amap]"]')
      )
    ).toBe("amap test");
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 11")
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 12")
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 21")
    ).toHaveCount(0);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 22")
    ).toHaveCount(0);
    expect(
      await getSelectedOption(
        page.locator('[name="search_campagne_bulletin[campagne]"]')
      )
    ).toBe("");
    await expect(page.locator("table")).not.toHaveText(/Campagne 12/);
    await expect(page.locator("table")).toHaveText(/Campagne 11/);
    await expect(page.locator("table")).not.toHaveText(/Campagne 22/);
    await expect(page.locator("table")).not.toHaveText(/Campagne 21/);
    await page
      .locator('[name="search_campagne_bulletin[campagne]"]')
      .selectOption({ label: "Campagne 12" });
    await expect(page.locator(".alert-warning")).toContainText(
      "Aucun résultat pour cette recherche."
    );
    await page
      .locator('[name="search_campagne_bulletin[amap]"]')
      .selectOption({ label: "amap test 2" });
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 11")
    ).toHaveCount(0);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 12")
    ).toHaveCount(0);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 21")
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_campagne_bulletin[campagne]"] option')
        .getByText("Campagne 22")
    ).toHaveCount(1);
    await expect(page.locator("table")).not.toHaveText(/Campagne 11/);
    await expect(page.locator("table")).not.toHaveText(/Campagne 12/);
    await expect(page.locator("table")).not.toHaveText(/Campagne 22/);
    await expect(page.locator("table")).toHaveText(/Campagne 21/);
    await page
      .locator('[name="search_campagne_bulletin[campagne]"]')
      .selectOption({ label: "Campagne 22" });
    await expect(page.locator(".alert-warning")).toContainText(
      "Aucun résultat pour cette recherche."
    );
  });
});
