import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";

test.describe("Valider le premier formulaire de souscription", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);

    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Lancement de la validation des champs utilisateur`, async ({
    page,
  }) => {
    const champs = [
      "campagne_bulletin_form1[amapien][user][name][firstName]",
      "campagne_bulletin_form1[amapien][user][name][lastName]",
      "campagne_bulletin_form1[amapien][user][ville]",
    ];

    for (const champ of champs) {
      await page.locator(`[name="${champ}"]`).clear();
      await page.locator(`[name="${champ}"]`).fill("   ");
    }
    await page
      .getByText(/Étape 2/)
      .first()
      .click();

    for (const champ of champs) {
      await expect(
        page.locator(`[name="${champ}"]  ~ .alert-danger`)
      ).toContainText("Cette valeur ne doit pas être vide.");
    }
  });

  test(`Validation du montant obligatoire`, async ({ page }) => {
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        `[name="campagne_bulletin_form1[montants][0][montant]"]  ~ .alert-danger`
      )
    ).toContainText("Le montant est obligatoire");
  });

  test(`Validation du montant minimum`, async ({ page }) => {
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("-1");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        `[name="campagne_bulletin_form1[montants][0][montant]"]  ~ .alert-danger`
      )
    ).toContainText("Cette valeur doit être supérieure ou égale à 0.");
  });
});
