import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  amapDeFait,
  campagneCreationAmap,
  campagneSouscription,
  supprimerAmapien,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Supprimer les recus des bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Supprimer les recus des bulletins - amap de fait`, async ({ page }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(
      page.getByText(/Supprimer les reçus/).first()
    ).not.toBeVisible();
    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) .dropdown-menu-right"
      )
    ).not.toHaveText(/Supprimer le reçu/);
  });

  test(`Supprimer les recus des bulletins en brouillon depuis actions - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Supprimer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été supprimés.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
  });

  test(`Supprimer les recus des bulletins en brouillon depuis actions - amapien supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Supprimer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été supprimés.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
  });

  test(`Supprimer les recus des bulletins générés depuis actions - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Supprimer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été supprimés.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /Année\(s\) d'adhésion :/
    );
  });

  test(`Supprimer les recus des bulletins brouillon depuis menu - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) .dropdown-menu-right"
      )
    ).not.toHaveText(/Supprimer le reçu/);
  });

  test(`Supprimer les recus des bulletins générés depuis menu - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");
    await clickMenuLinkOnLine(page, "Campagne 1", "Supprimer le reçu");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "Le reçu a été supprimé.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /Année\(s\) d'adhésion :/
    );
  });

  test(`Supprimer les recus des bulletins générés depuis menu - amapien supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");
    await clickMenuLinkOnLine(page, "Campagne 1", "Supprimer le reçu");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "Le reçu a été supprimé.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
  });

  test(`Supprimer les recus des bulletins générés depuis menu - amap pas de fait & pas d'année adhésion associée`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Générer les reçus/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Les reçus ont été générés.");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Modifier le profil de l'amapien"
    );
    await page
      .locator('select[name="user_amapien_amap[anneeAdhesions][]"]')
      .selectOption([]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "L'amapien a été mis à jour avec succès."
    );
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Campagne 1", "Supprimer le reçu");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "Le reçu a été supprimé.");
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Campagne 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
    ).toHaveText(/Non/);
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /Année\(s\) d'adhésion :/
    );
  });
});
