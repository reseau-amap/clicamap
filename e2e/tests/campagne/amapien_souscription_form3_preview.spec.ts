import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { readPdfFromHref } from "../../support/shorcut_pdf";

export const readPdfAndCheck = async (page: Page) => {
  const text = await readPdfFromHref(page, "/campagne/preview_bulletin_pdf");
  await expect(text).toContain(`Campagne 1`);
  await expect(text).toContain(`01/01/2000 / 01/01/2030`);
  await expect(text).toContain(`NOM AMAPIEN Prenom amapien`);
  await expect(text).toContain(`Email.s : amapien@test.amap-aura.org`);
  await expect(text).toContain(`Adresse : adresse`);
  await expect(text).toContain(`69001 Lyon 1er Arrondissement`);
  await expect(text).toContain(`Montant 1`);
  await expect(text).toContain(`Description montant 1`);
  await expect(text).toContain(`3,00 €`);
  await expect(text).toContain(
    `Je m’engage à verser la somme de 4,00 € auprès de amap test en Méthode de
paiement`
  );
  await expect(text).toContain(`Description de la méthode de paiement`);
  await expect(text).toContain(`Lyon 1er Arrondissement, fait le : 02/01/2000`);
  await expect(text).toContain(`Prenom amapien NOM AMAPIEN`);
  return text;
};

test.describe("Afficher une pré visualisation à l'étape 3", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");

    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("3");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .locator(`[name="campagne_bulletin_form2[payeur]"]`)
      .fill("Payeur");
    await page
      .locator(`[name="campagne_bulletin_form2[paiementDate]"]`)
      .fill("2020-01-01");
    await page.locator("h3").click(); // Hide popup
    await page.locator(`[name="campagne_bulletin_form2[chartre]"]`).check();
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Afficher une pré visualisation à l'étape 3 - avec permisison image`, async ({
    page,
  }) => {
    await page
      .locator(`[name="campagne_bulletin_form2[permissionImage]"]`)
      .check();
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 3 : Prévisualisation/);

    const text = await readPdfAndCheck(page);
    await expect(text).toContain(
      `J’autorise amap test à utiliser mon image (vidéo, photo) dans un but ,de promouvoir
ses activités et évènements (presse flyers, site internet, facebook).`
    );
  });

  test(`Afficher une pré visualisation à l'étape 3 - sans permission image`, async ({
    page,
  }) => {
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 3 : Prévisualisation/);

    const text = await readPdfAndCheck(page);
    await expect(text).not.toContain(
      `J’autorise amap test à utiliser mon image (vidéo, photo) dans un but ,de promouvoir
ses activités et évènements (presse flyers, site internet, facebook).`
    );
  });
});
