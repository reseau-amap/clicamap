import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { amapDeFait, campagneCreationAdmin } from "../../support/shorcut";

const goToImport = async (page: Page, role: string) => {
  await amapDeFait(page, "amap@test.amap-aura.org");
  await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
  await login(page, role);
  await page.getByText(/Reçus/).first().click();
  await page
    .getByText(/Amapien/)
    .first()
    .click();
  await page
    .locator('[name="search_campagne[amap]"]')
    .selectOption({ label: "amap test" });
  await page
    .getByText(/Liste des bulletins et reçus/)
    .first()
    .click();
  await page
    .getByText(/Importer des bulletins/)
    .first()
    .click();
};

test.describe("Import des bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Import des bulletins ${role} - masquer le formulaire avant selection campagne`, async ({
      page,
    }) => {
      await goToImport(page, role);
      await expect(page.locator(".alert-warning")).toHaveText(
        /Veuillez sélectionner une campagne pour continuer/
      );
      await expect(
        page.getByText(/Télécharger un exemple de fichier/).first()
      ).not.toBeVisible();
      await expect(page.getByText(/Importer/).first()).not.toBeVisible();
    });

    test(`Import des bulletins ${role} - import valide`, async ({ page }) => {
      await goToImport(page, role);
      await page
        .locator('[name="search_campagne_bulletin_import[campagne]"]')
        .selectOption({ label: "Campagne 1" });
      await page
        .locator('input[name="campagne_bulletin_import[file]"]')
        .setInputFiles("e2e/data/Import_bulletin_success.xls");
      await page
        .getByText(/Importer/)
        .first()
        .click();
      await flashMessageShouldContain(
        page,
        "1 bulletins ont été importés avec succès."
      );
      await expect(page.locator("h3")).toHaveText(
        /Bulletins et reçus amapiens/
      );
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(4)")
      ).toHaveText(/nom amapien/);
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(6)")
      ).toHaveText(/3,00 €/);
    });

    test(`Import des bulletins ${role} - import invalide`, async ({ page }) => {
      await goToImport(page, role);
      await page
        .locator('[name="search_campagne_bulletin_import[campagne]"]')
        .selectOption({ label: "Campagne 1" });
      await page
        .locator('input[name="campagne_bulletin_import[file]"]')
        .setInputFiles("e2e/data/Import_bulletin_error.xls");
      await page
        .getByText(/Importer/)
        .first()
        .click();
      await expect(page.locator(".alert-danger")).toHaveText(
        /Ligne 2 : Amapien introuvable/
      );
    });
  });
});
