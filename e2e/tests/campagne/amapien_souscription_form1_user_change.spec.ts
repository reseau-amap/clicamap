import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";

test.describe("Changer les informations amapien lors de la souscription", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);

    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Modification des informations utilisateur`, async ({ page }) => {
    // For form validation
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("1");
    await page.locator("#add_email").click();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][firstName]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][firstName]"]`
      )
      .fill("prenom2");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][lastName]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][lastName]"]`
      )
      .fill("nom2");
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][numTel1]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][numTel1]"]`)
      .fill("tel1");
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][numTel2]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][numTel2]"]`)
      .fill("tel2");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][0][email]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][0][email]"]`
      )
      .fill("amapientest1@test.amap-aura.org");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][1][email]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][1][email]"]`
      )
      .fill("amapientest2@test.amap-aura.org");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][address][adress]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][address][adress]"]`
      )
      .fill("address2");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][address][adressCompl]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][address][adressCompl]"]`
      )
      .fill("address2Compl");
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][ville]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[amapien][user][ville]"]`)
      .fill("59000, Lille");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Mon profil/)
      .first()
      .click();
    await expect(
      page.locator(".container table tbody tr:nth-child(2) td:nth-child(2)")
    ).toHaveText(/nom2/);
    await expect(
      page.locator(".container table tbody tr:nth-child(3) td:nth-child(2)")
    ).toHaveText(/prenom2/);
    await expect(
      page.locator(".container table tbody tr:nth-child(4) td:nth-child(2)")
    ).toHaveText(/address2/);
    await expect(
      page.locator(".container table tbody tr:nth-child(5) td:nth-child(2)")
    ).toHaveText(/address2Compl/);
    await expect(
      page.locator(".container table tbody tr:nth-child(6) td:nth-child(2)")
    ).toHaveText(/59000 - Lille/);
    await expect(
      page.locator(".container table tbody tr:nth-child(7) td:nth-child(2)")
    ).toHaveText(
      /amapientest1@test\.amap-aura\.org,amapientest2@test\.amap-aura\.org/
    );
    await expect(
      page.locator(".container table tbody tr:nth-child(8) td:nth-child(2)")
    ).toHaveText(/tel1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(9) td:nth-child(2)")
    ).toHaveText(/tel2/);
  });

  test(`Utilisation d'un email existant`, async ({ page }) => {
    // For form validation
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("1");
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][0][email]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][emails][0][email]"]`
      )
      .fill("amapien2@test.amap-aura.org");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="campagne_bulletin_form1[amapien][user][emails][0][email]"] + .alert-danger'
      )
    ).toHaveText("Un autre utilisateur utilise déjà cette adresse email.");
  });
});
