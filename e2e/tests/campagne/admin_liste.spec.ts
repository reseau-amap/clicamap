import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { amapDeFait } from "../../support/shorcut";

test.describe("Listes les campagnes disponibles", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Ne pas afficher d'amap si pas de fait ${role}`, async ({ page }) => {
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
      await expect(
        page.locator('[name="search_campagne[amap]"]')
      ).not.toContainText("amap test");
    });

    test(`Afficher les amaps de fait ${role}`, async ({ page }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
      await expect(
        page.locator('[name="search_campagne[amap]"]')
      ).toContainText("amap test");
    });
  });
});
