import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  amapDeFait,
  campagneCreationAmap,
  campagneSouscription,
  supprimerAmapien,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { expectMailShoudBeReceived } from "../../support/shorcut_mail";

test.describe("Notifier les bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`Notifier les recus des bulletins - amap de fait`, async ({ page }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await expect(
      page
        .locator("form")
        .getByText(/Notifier par email/)
        .first()
    ).not.toBeVisible();
    await page
      .locator(".container table tbody tr:nth-child(1) button.dropdown-toggle")
      .click();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) .dropdown-menu-right"
      )
    ).not.toHaveText(/Notifier par email/);
  });

  test(`Notifier les recus des bulletins depuis actions - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Notifier par email/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les notifications ont été envoyées."
    );
    await expectMailShoudBeReceived(
      "Un nouveau document est disponible sur votre profil clic'AMAP",
      ["amapien@test.amap-aura.org"]
    );
  });

  test(`Notifier les recus des bulletins depuis actions - amapien supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await page.locator(".js-tableselect-all").click();
    await page
      .getByText(/Notifier par email/)
      .first()
      .click();
    await page
      .locator(".modal.in")
      .getByText(/Confirmer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les notifications ont été envoyées."
    );
    await expectMailShoudBeReceived(
      "Un nouveau document est disponible sur votre profil clic'AMAP",
      ["amapien@test.amap-aura.org"]
    );
  });

  test(`Notifier les recus des bulletins générés depuis menu - amap pas de fait`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Campagne 1", "Notifier par email");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "La notification a été envoyée.");
    await expectMailShoudBeReceived(
      "Un nouveau document est disponible sur votre profil clic'AMAP",
      ["amapien@test.amap-aura.org"]
    );
  });

  test(`Notifier les recus des bulletins générés depuis menu - amapien supprimé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await campagneSouscription(page, "amapien", "Campagne 1");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Liste des bulletins et reçus/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Campagne 1", "Notifier par email");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "La notification a été envoyée.");
    await expectMailShoudBeReceived(
      "Un nouveau document est disponible sur votre profil clic'AMAP",
      ["amapien@test.amap-aura.org"]
    );
  });
});
