import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  amapChangerLieuLivraison,
  amapDeFait,
  campagneCreationAmap,
} from "../../support/shorcut";

test.describe("Éditer une campagne", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Vérifification de la duplication des champs`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await clickMenuLinkOnLine(page, "Campagne 1", "Modifier");
    await expect(page.locator('[name="campagne_form1[nom]"]')).toHaveValue(
      "Campagne 1"
    );
    await expect(
      page.locator('[name="campagne_form1[period][startAt]"]')
    ).toHaveValue("2000-01-01");
    await expect(
      page.locator('[name="campagne_form1[period][endAt]"]')
    ).toHaveValue("2030-01-01");
    await expect(
      page.locator('[name="campagne_form1[montants][0][montant]"]')
    ).toHaveValue("1.00");
    await expect(
      page.locator('[name="campagne_form1[montants][0][titre]"]')
    ).toHaveValue("Montant 1");
    await expect(
      page.locator('[name="campagne_form1[montants][0][description]"]')
    ).toHaveValue("<p>Description montant 1<br></p>");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form2[paiementMethode]"]')
    ).toHaveValue("Méthode de paiement");
    await expect(
      page.locator('[name="campagne_form2[paiementDescription]"]')
    ).toHaveValue("<p>Description de la méthode de paiement<br></p>");
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form3[champLibre]"]')
    ).toHaveValue("<p>Champ libre<br></p>");
    await page
      .getByText(/Étape 4/)
      .first()
      .click();
  });

  test(`Modification du titre`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await clickMenuLinkOnLine(page, "Campagne 1", "Modifier");
    await page.locator('[name="campagne_form1[nom]"]').clear();
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne 2");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await page
      .getByText(/Étape 4/)
      .first()
      .click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "La campagne a été modifiée avec succès."
    );
    await expect(page.locator(".container table tbody")).toHaveText(
      /Campagne 2/
    );
    await expect(page.locator(".container table tbody")).not.toHaveText(
      /Campagne 1/
    );
  });

  test(`Editer une nouvelle campagne comme AMAP de fait sans réseau associé`, async ({
    page,
  }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await amapDeFait(page, "amap@test.amap-aura.org");
    await amapChangerLieuLivraison(
      page,
      "amap@test.amap-aura.org",
      "Ll amap test",
      "59000, Lille"
    );
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Campagne 1", "Modifier");
    await flashMessageShouldContain(
      page,
      "L'amap est une amap de fait et aucun admin réseau n'a été trouvé pour cette AMAP."
    );
    await expect(page.locator("h3")).toHaveText(
      /Gestion des campagnes d'adhésions et de dons/
    );
  });
});
