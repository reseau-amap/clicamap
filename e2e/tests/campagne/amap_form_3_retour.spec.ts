import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { fillNoteEditor } from "../../support/shorcut_actions";

test.describe("Valider le retour au troisieme formulaire", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des adhésions/)
      .first()
      .click();
    await page
      .getByText(/Créer une campagne/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Récupération des informations lors du retour à l'étape 3`, async ({
    page,
  }) => {
    await page.locator('[name="campagne_form1[nom]"]').fill("Campagne");
    await page
      .locator('[name="campagne_form1[period][startAt]"]')
      .fill("2020-01-01");
    await page
      .locator('[name="campagne_form1[period][endAt]"]')
      .fill("2020-12-31");
    await page
      .locator('[name="campagne_form1[montants][0][montant]"]')
      .fill("1");
    await page
      .locator('[name="campagne_form1[montants][0][titre]"]')
      .fill("Montant 1");
    await fillNoteEditor(
      page,
      "campagne_form1[montants][0][description]",
      "Description"
    );
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .locator('[name="campagne_form2[paiementMethode]"]')
      .fill("Méthode de paiement");
    await fillNoteEditor(
      page,
      "campagne_form2[paiementDescription]",
      "Description"
    );
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await fillNoteEditor(page, "campagne_form3[champLibre]", "Champ libre");
    await page
      .getByText(/Étape 4/)
      .first()
      .click();
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(
      page.locator('[name="campagne_form3[champLibre]"]')
    ).toHaveValue("<p>Champ libre<br></p>");
  });
});
