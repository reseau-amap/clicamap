import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { campagneCreationAmap } from "../../support/shorcut";
import { readPdf } from "../../support/shorcut_pdf";

test.describe("Souscrire à une campagne comme amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Souscrire à une campagne comme amapien`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("3");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .locator(`[name="campagne_bulletin_form2[payeur]"]`)
      .fill("Payeur");
    await page
      .locator(`[name="campagne_bulletin_form2[paiementDate]"]`)
      .fill("2020-01-01");
    await page.locator("h3").click(); // Hide popup
    await page.locator(`[name="campagne_bulletin_form2[chartre]"]`).check();
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await flashMessageShouldContain(page, "La campagne a été souscrite.");
    await expect(page.locator("h3")).toHaveText(/Autres documents/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(1)")
    ).toHaveText(/Campagne 1/);
    await page.locator(".container table tbody tr:nth-child(1) button").click();
    await expect(
      page.locator(
        ".container table tbody tr:nth-child(1) .dropdown-menu-right"
      )
    ).not.toHaveText(/Télécharger le reçu/);
    const text = await readPdf(
      page,
      page
        .locator(".container table tbody tr:nth-child(1) .dropdown-menu-right")
        .getByText(/Télécharger le bulletin/)
    );
    await expect(text).toContain(`amap@test.amap-aura.org`);
    await expect(text).toContain(`Campagne 1`);
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucune campagne d'adhésion active pour le moment/
    );
  });
});
