import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  amapDeFait,
  campagneCreationAdmin,
  campagneSouscription,
} from "../../support/shorcut";
import { getSelectedOption } from "../../support/shorcut_actions";

test.describe("Listes les campagnes disponibles comme admin", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Activer/Desactiver les boutons du header en fonction de la sélection ${role}`, async ({
      page,
    }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await campagneSouscription(page, "amapien", "Campagne 1");
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await page
        .locator('[name="search_campagne[amap]"]')
        .selectOption({ label: "amap test" });
      await page
        .getByText(/Liste des bulletins et reçus/)
        .first()
        .click();

      const buttonsShould = async (expectDisabled: boolean) => {
        const selectors = [
          'button[name="action-generate"]',
          'button[name="action-generate"]',
          'button[name="action-delete"]',
          'button[name="action-notification"]',
          "button.btn-success.dropdown-toggle",
        ];

        for (const selector of selectors) {
          if (expectDisabled) {
            await expect(page.locator(selector)).toBeDisabled();
          } else {
            await expect(page.locator(selector)).toBeEnabled();
          }
        }
      };

      await buttonsShould(true);
      await page.locator(".js-tableselect-all").click();
      await buttonsShould(false);
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await buttonsShould(true);
      await page
        .locator('input[type="checkbox"][data-target-name="selected[1]"]')
        .click();
      await buttonsShould(false);
    });

    test(`Afficher les campagnes disponibles ${role}`, async ({ page }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await amapDeFait(page, "amap2@test.amap-aura.org");
      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await campagneSouscription(page, "amapien", "Campagne 1");
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await page
        .locator('[name="search_campagne[amap]"]')
        .selectOption({ label: "amap test" });
      await page
        .getByText(/Liste des bulletins et reçus/)
        .first()
        .click();
      expect(
        await getSelectedOption(
          page.locator('[name="search_campagne_bulletin[amap]"]')
        )
      ).toBe("amap test");
      await expect(page.locator(".alert-warning")).toHaveText(
        /1 résultat pour cette recherche\./
      );
      await page
        .locator('[name="search_campagne_bulletin[amap]"]')
        .selectOption({ label: "amap test 2" });
      await expect(page.locator(".alert-warning")).toHaveText(
        /Aucun résultat pour cette recherche\./
      );
      await page
        .locator('[name="search_campagne_bulletin[amap]"]')
        .selectOption({ label: "amap test" });
      await expect(page.locator(".alert-warning")).toHaveText(
        /1 résultat pour cette recherche\./
      );
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(2)")
      ).toHaveText(/Campagne 1/);
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(3)")
      ).toHaveText("");
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(4)")
      ).toHaveText(/nom amapien/);
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(5)")
      ).toHaveText(/prenom amapien/);
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(6)")
      ).toHaveText(/4,00 €/);
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(7)")
      ).toHaveText(/01\/01\/2020/);
      await expect(
        page.locator("table tr:nth-child(1) td:nth-child(8)")
      ).toHaveText(/Non/);
    });
  });
});
