import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import {
  amapDeFait,
  campagneCreationAdmin,
  campagneSouscription,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Supprimer les recus des bulletins", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    dateSet("02-01-2000");
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Supprimer les recus des bulletins générés depuis actions - amap pas de fait ${role}`, async ({
      page,
    }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await campagneSouscription(page, "amapien", "Campagne 1");
      await login(page, role);
      await page.getByText(/Reçus/).first().click();
      await page
        .getByText(/Amapien/)
        .first()
        .click();
      await page
        .locator('[name="search_campagne[amap]"]')
        .selectOption({ label: "amap test" });
      await page
        .getByText(/Liste des bulletins et reçus/)
        .first()
        .click();
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Générer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "Les reçus ont été générés.");
      await page.locator(".js-tableselect-all").click();
      await page
        .getByText(/Supprimer les reçus/)
        .first()
        .click();
      await page
        .locator(".modal.in")
        .getByText(/Confirmer/)
        .first()
        .click();
      await flashMessageShouldContain(page, "Les reçus ont été supprimés.");
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
      ).toHaveText(/Campagne 1/);
      await expect(
        page.locator(".container table tbody tr:nth-child(1) td:nth-child(8)")
      ).toHaveText(/Non/);
    });
  });
});
