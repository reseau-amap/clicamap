import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";

test.describe("Listes les campagnes disponibles", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Afficher un message si aucune campagne disponible`, async ({
    page,
  }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucune campagne d'adhésion active pour le moment/
    );
  });

  test(`Afficher les campagnes disponibles`, async ({ page }) => {
    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).not.toBeVisible();
    await expect(page.locator(".container table")).toHaveText(/Campagne 1/);
  });
});
