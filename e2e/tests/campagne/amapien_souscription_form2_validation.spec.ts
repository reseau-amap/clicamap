import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";

test.describe("Valider le second formulaire de souscription", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);

    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("1");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
  });

  test(`Valider le second formulaire de souscription`, async ({ page }) => {
    await page
      .getByText(/Étape 3/)
      .first()
      .click();
    await expect(
      page.locator(`[name="campagne_bulletin_form2[payeur]"] + .alert-danger`)
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page.locator(
        `[name="campagne_bulletin_form2[paiementDate]"] + .alert-danger`
      )
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page
        .locator(".col-sm-10")
        .filter({
          has: page.locator(`[name="campagne_bulletin_form2[chartre]"]`),
        })
        .locator(".alert-danger")
    ).toContainText("Cette valeur est obligatoire.");
  });
});
