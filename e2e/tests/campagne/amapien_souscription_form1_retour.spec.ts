import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { campagneCreationAmap } from "../../support/shorcut";

test.describe("Retour à l'étape 1 lors de la souscription", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);

    await campagneCreationAmap(page, "AMAP", "Campagne 1");
    await login(page, "amapien");
    await page
      .getByText(/Mon compte/)
      .first()
      .click();
    await page
      .getByText(/Adhésions/)
      .first()
      .click();
    await page
      .getByText(/Souscrire/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  });

  test(`Retour à l'étape 1 lors de la souscription`, async ({ page }) => {
    await page.locator("#add_email").click();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][firstName]"]`
      )
      .clear();
    await page
      .locator(
        `[name="campagne_bulletin_form1[amapien][user][name][firstName]"]`
      )
      .fill("prenom2");
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .clear();
    await page
      .locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
      .fill("1");
    await page
      .getByText(/Étape 2/)
      .first()
      .click();
    await page
      .getByText(/Étape 1/)
      .first()
      .click();
    await expect(
      page.locator(
        `[name="campagne_bulletin_form1[amapien][user][name][firstName]"]`
      )
    ).toHaveValue("prenom2");
    await expect(
      page.locator(`[name="campagne_bulletin_form1[montants][0][montant]"]`)
    ).toHaveValue("1");
  });
});
