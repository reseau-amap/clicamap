import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { amapDeFait } from "../../support/shorcut";
import { readPdf, readPdfFromHref } from "../../support/shorcut_pdf";
import { enableExdebug } from "../../support/shorcut_debug";

export const goToPreview = async (page: Page) => {
  await login(page, "amap");
  await page
    .getByText(/Gestionnaire AMAP/)
    .first()
    .click();
  await page
    .getByText(/Gestion des adhésions/)
    .first()
    .click();
  await page
    .getByText(/Créer une campagne/)
    .first()
    .click();
  await expect(page.locator("h3")).toHaveText(/Étape 1 : paramètres/);
  await page.locator('[name="campagne_form1[nom]"]').fill("Nom de la campagne");
  await page
    .locator('[name="campagne_form1[period][startAt]"]')
    .fill("2020-12-01");
  await page
    .locator('[name="campagne_form1[period][endAt]"]')
    .fill("2020-12-31");
  await page.locator('[name="campagne_form1[montants][0][montant]"]').fill("1");
  await page
    .locator('[name="campagne_form1[montants][0][titre]"]')
    .fill("Montant 1");
  await page
    .locator('[name="campagne_form1[montants][0][description]"]')
    .fill("Description montant 1", {
      force: true,
    });
  await page
    .getByText(/Étape 2/)
    .first()
    .click();
  await page
    .locator('[name="campagne_form2[paiementMethode]"]')
    .fill("Méthode de paiement");
  await page
    .locator('[name="campagne_form2[paiementDescription]"]')
    .fill("Description de la méthode de paiement", {
      force: true,
    });
  await page
    .getByText(/Étape 3/)
    .first()
    .click();
  await page
    .locator('[name="campagne_form3[champLibre]"]')
    .fill("Champ libre", {
      force: true,
    });
  await page
    .getByText(/Étape 4/)
    .first()
    .click();
};

test.describe("Afficher une pré visualisation à l'étape 4", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Afficher une pré visualisation à l'étape 4`, async ({ page }) => {
    await goToPreview(page);

    const text = await readPdfFromHref(
      page,
      "http://localhost/campagne/preview_pdf"
    );

    await expect(text).toContain(`Nom de la campagne`);
    await expect(text).toContain(`01/12/2020 / 31/12/2020`);
    await expect(text).toContain(`NOM Prénom`);
    await expect(text).toContain(`Téléphone.s :`);
    await expect(text).toContain(`Email.s :`);
    await expect(text).toContain(`Complément d'adresse :`);
    await expect(text).toContain(`Adresse :`);
    await expect(text).toContain(`Montant 1`);
    await expect(text).toContain(`Description montant 1`);
    await expect(text).toContain(
      `Je m’engage à verser la somme de 1,00 € auprès de amap test en Méthode de
paiement`
    );
    await expect(text).toContain(`Description de la méthode de paiement`);
    await expect(text).toContain(
      `J’autorise amap test à utiliser mon image (vidéo, photo) dans un but ,de promouvoir
ses activités et évènements (presse flyers, site internet, facebook).`
    );
    await expect(text).toContain(
      `[nom de la ville de l’adresse de l’amapien.ne], fait le : [date du jour]`
    );
  });

  test(`Afficher une pré visualisation à l'étape 4 - amap de fait`, async ({
    page,
  }) => {
    await amapDeFait(page, "amap@test.amap-aura.org");
    await goToPreview(page);
    const text = await readPdfFromHref(
      page,
      "http://localhost/campagne/preview_pdf"
    );

    await expect(text).toContain(`Nom de la campagne`);
    await expect(text).toContain(`01/12/2020 / 31/12/2020`);
    await expect(text).toContain(`NOM Prénom`);
    await expect(text).toContain(`Téléphone.s :`);
    await expect(text).toContain(`Email.s :`);
    await expect(text).toContain(`Complément d'adresse :`);
    await expect(text).toContain(`Adresse :`);
    await expect(text).toContain(`Montant 1`);
    await expect(text).toContain(`Description montant 1`);
    await expect(text).toContain(
      `Je m’engage à verser la somme de 1,00 € auprès de Réseau des AMAP du Rhône en
Méthode de paiement`
    );
    await expect(text).toContain(`Description de la méthode de paiement`);
    await expect(text).toContain(
      `J’autorise Réseau des AMAP du Rhône à utiliser mon image (vidéo, photo) dans un but
,de promouvoir ses activités et évènements (presse flyers, site internet, facebook)`
    );
    await expect(text).toContain(
      `[nom de la ville de l’adresse de l’amapien.ne], fait le : [date du jour]`
    );
  });
});
