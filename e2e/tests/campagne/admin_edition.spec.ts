import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { amapDeFait, campagneCreationAdmin } from "../../support/shorcut";

test.describe("Éditer une campagne", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  ["admin_aura", "admin_rhone"].forEach((role) => {
    test(`Editer comme admin ${role}`, async ({ page }) => {
      await amapDeFait(page, "amap@test.amap-aura.org");
      await login(page, role);

      await campagneCreationAdmin(page, role, "amap test", "Campagne 1");
      await clickMenuLinkOnLine(page, "Campagne 1", "Modifier");
      await expect(page.locator('[name="campagne_form1[nom]"]')).toHaveValue(
        "Campagne 1"
      );
    });
  });
});
