import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Envoyer un email de contact à son amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test(`Contacter mon amap en amapien`, async ({ page }) => {
    await login(page, "amapien");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Contact");
    await clickMainMenuLink(page, "Mon AMAP amap test");
    await currentPathShouldBe(page, "/contact/mon_amap/1");
    await expect(page.locator("h3")).toHaveText(/Contacter mon AMAP/);
    await expect(
      page.locator(".container table tr:nth-child(1)")
    ).toContainText("amap@test.amap-aura.org");
    await expect(
      page.locator(".container table tr:nth-child(2)")
    ).toContainText(
      "Ll amap test - 58 rue raulin 69007 LYON 7E ARRONDISSEMENT"
    );
    await expect(
      page.locator(".container table tr:nth-child(2)")
    ).toContainText("Été -> Lundi : 16:00 - 18:00");
    await expect(
      page.locator(".container table tr:nth-child(2)")
    ).toContainText("Hiver -> Mardi : 17:00 - 19:00");
    await page.locator("button").getByText(/Email/).first().click();
    await expect(page.locator(".modal.fade.in")).toBeVisible(); // Wait for animation
    await page.waitForTimeout(250);
    await page.locator('input[name="contact[title]"]').fill("Sujet");
    await page
      .locator('textarea[name="contact[content]"]')
      .fill("Contenu du mail");
    await page.locator('form input[type="submit"]').click();
    await flashMessageShouldContain(
      page,
      "Le message a été envoyé avec succès."
    );
    await expectMailShoudBeReceived("Message Clic'AMAP", [
      "amap@test.amap-aura.org",
    ]);
  });
});
