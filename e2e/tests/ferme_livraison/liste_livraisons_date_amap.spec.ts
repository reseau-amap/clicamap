import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  fermeAdherentCurrentYear,
  subscribeContract,
} from "../../support/shorcut";
import { fillDatepickerRange } from "../../support/shorcut_actions";

test.describe("En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amap`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await expect(
      page.locator(".container > .row .col-md-6 .well")
    ).toContainText("Contrat : contrat 1");
    await expect(
      page.locator(".container > .row .col-md-6 .well")
    ).toContainText("Lieu : ll amap test");
    await expect(
      page.locator(".container > .row .col-md-6 .well")
    ).toContainText("produit 1 (conditionnement : cond1; prix : 1,00) : 1");
    await expect(
      page.locator(".container > .row .col-md-6 .well")
    ).toContainText("produit 2 (conditionnement : cond2; prix : 2,00) : 1");
  });
});
