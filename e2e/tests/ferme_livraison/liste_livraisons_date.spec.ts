import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { fermeAdherentCurrentYear } from "../../support/shorcut";
import { fillDatepickerRange } from "../../support/shorcut_actions";

test.describe("En tant que paysan adhérent, je doit pouvoir lister les livraisons par date", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant que paysan adhérent, je doit pouvoir lister les livraisons par date`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await expect(page.locator(".container")).toContainText(
      "Livraison du 01/01/2030"
    );
    await expect(page.locator(".container")).toContainText(
      "Livraison du 30/04/2030"
    );
  });

  test(`En tant que paysan adhérent, je doit afficher un résultat vide si le formulaire est invalide`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await login(page, "paysan");
    await page.goto(
      "/ferme_livraison/accueil/1?search_ferme_livraison_list[period]=invalid"
    );
    await expect(page.locator("h3")).toContainText("Calendrier des livraisons");
    await expect(page.locator(".container")).not.toContainText("Livraison du");
  });
});
