import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import { fermeAdherentCurrentYear } from "../../support/shorcut";

test.describe("En tant que paysan non adhérent, je doit voir un message d'erreur sur le calenderier de livraison", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Message d'erreur en tant qu'utilisateur non authentifie`, async ({
    page,
  }) => {
    await page.goto(`http://localhost/ferme_livraison/accueil/1`);
    await expect(page).toHaveURL("http://localhost/portail/connexion");
    await flashMessageShouldContain(
      page,
      "Vous n'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter."
    );
  });

  test(`Message d'erreur en tant que paysan non adhérent`, async ({ page }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await expect(page.locator("h4")).toContainText(
      "Cette fonctionnalité est réservée aux adhérents uniquement"
    );
  });

  test(`Pas de message d'erreur en tant que paysan non adhérent`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await expect(page.locator("h4")).not.toBeVisible();
  });
});
