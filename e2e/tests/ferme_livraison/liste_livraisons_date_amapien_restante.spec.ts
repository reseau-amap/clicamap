import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

import {
  fermeAdherentCurrentYear,
  subscribeContract,
  supprimerAmapien,
} from "../../support/shorcut";
import { fillDatepickerRange, select2 } from "../../support/shorcut_actions";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("En tant que paysan adhérent, je doit pouvoir voir les livraisons restantes", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons restantes`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
      "1_2": 1,
      "2_2": 1,
      "3_2": 1,
      "4_2": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await page
      .locator("a.btn-primary")
      .getByText(/Livraisons restantes/)
      .first()
      .click();
    await expect(page.locator(".container")).toContainText("1 / 1");
    await expect(page.locator(".container")).toContainText("100 %");
    await expect(page.locator(".container")).toContainText(
      "1 produit 1 (cond1)"
    );
    await expect(page.locator(".container")).toContainText(
      "1 produit 2 (cond2)"
    );
  });

  test(`En tant que paysan adhérent, je doit pouvoir modifier la liste des livraisons restantes en ajoutant une livraison`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
      "1_2": 1,
      "2_2": 1,
      "3_2": 1,
      "4_2": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await page.locator(".switch").click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Sauvegarde effectuée");
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await page
      .locator("a.btn-primary")
      .getByText(/Livraisons restantes/)
      .first()
      .click();
    await expect(page.locator(".container")).toContainText("0 / 1");
    await expect(page.locator(".container")).toContainText("0 %");
    await expect(page.locator(".container")).not.toContainText(
      "1 produit 1 (cond1)"
    );
    await expect(page.locator(".container")).not.toContainText(
      "1 produit 2 (cond2)"
    );
  });

  test(`En tant que paysan adhérent, je doit pouvoir modifier la liste des livraisons restantes en ajoutant une livraison meme pour un amapien supprimé`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
      "1_2": 1,
      "2_2": 1,
      "3_2": 1,
      "4_2": 1,
      "5_2": 1,
    });
    dateSet("2030-03-19");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    dateClean();
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await page.locator(".switch").click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Sauvegarde effectuée");
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await page
      .locator("a.btn-primary")
      .getByText(/Livraisons restantes/)
      .first()
      .click();
    await expect(page.locator(".container")).toContainText("0 / 1");
    await expect(page.locator(".container")).toContainText("0 %");
    await expect(page.locator(".container")).not.toContainText(
      "1 produit 1 (cond1)"
    );
    await expect(page.locator(".container")).not.toContainText(
      "1 produit 2 (cond2)"
    );
  });
});
