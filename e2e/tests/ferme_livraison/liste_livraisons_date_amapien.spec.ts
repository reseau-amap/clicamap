import { test, expect, Page } from "@playwright/test";
import {
  forceContratProduitRegul,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  fermeAdherentCurrentYear,
  subscribeContract,
  supprimerAmapien,
} from "../../support/shorcut";
import { fillDatepickerRange, select2 } from "../../support/shorcut_actions";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test(`En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amapien sans régularisation de poids`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 1 - cond1 - 1,00 € : 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 2 - cond2 - 2,00 € : 0"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "Produits sans régularisation de poids"
    );
    await expect(page.locator('[name="contrat_livraison"]')).not.toContainText(
      "Produits avec régularisation de poids"
    );
  });

  test(`En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amapien avec régularisation de poids`, async ({
    page,
  }) => {
    await forceContratProduitRegul(page, "contrat 1", "produit 1");
    await forceContratProduitRegul(page, "contrat 1", "produit 2");
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
      "1_2": 1,
      "2_2": 1,
      "3_2": 1,
      "4_2": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 1 : 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 2 : 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "Produits avec régularisation de poids"
    );
    await expect(
      page.locator('[name="contrat_livraison[cellules][0][quantite]"]')
    ).toHaveValue("");
    await expect(
      page.locator('[name="contrat_livraison[cellules][1][quantite]"]')
    ).toHaveValue("");
  });

  test(`En tant que paysan adhérent, je doit masquer les livraisons prévues à 0 en régularisation de poid`, async ({
    page,
  }) => {
    await forceContratProduitRegul(page, "contrat 1", "produit 1");
    await forceContratProduitRegul(page, "contrat 1", "produit 2");
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_2": 1,
      "2_2": 1,
      "3_2": 1,
      "4_2": 1,
      "5_2": 1,
    });
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".products_remaining")).toContainText(
      "produit 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 2 : 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "Produits avec régularisation de poids"
    );
  });

  test(`En tant que paysan adhérent, je doit pouvoir voir le détail des livraisons par date et par amapien même si il est supprimé`, async ({
    page,
  }) => {
    await fermeAdherentCurrentYear(page, "ferme");
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "2_1": 1,
      "3_1": 1,
      "4_1": 1,
      "5_1": 1,
    });
    dateSet("2030-03-19");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    dateClean();
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes livraisons/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_ferme_livraison_list[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/Livraison du 01\/01\/2030/)
      .first()
      .click();
    await page
      .getByText(/Suivi des livraisons en ligne/)
      .first()
      .click();
    await select2(
      page,
      "search_ferme_livraison_amapien[amapien]",
      "Prenom amapien NOM AMAPIEN"
    );
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 1 - cond1 - 1,00 € : 1"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "produit 2 - cond2 - 2,00 € : 0"
    );
    await expect(page.locator('[name="contrat_livraison"]')).toContainText(
      "Produits sans régularisation de poids"
    );
    await expect(page.locator('[name="contrat_livraison"]')).not.toContainText(
      "Produits avec régularisation de poids"
    );
  });
});
