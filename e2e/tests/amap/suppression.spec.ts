import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe(" suppression d'une amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`suppression d'une amap en tant que super admin`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Supprimer l'AMAP"
    );
    await expect(page.locator(".modal.in")).toContainText(
      'Voulez-vous vraiment supprimer l\'AMAP "AMAP TEST" ?'
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Attention ! Cette action supprimera tous les comptes et contrats liés à l'AMAP."
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "L'AMAP a été supprimée avec succès."
    );
    await expect(page.locator(".container table")).not.toContainText(
      "amap@test.amap-aura.org"
    );
  });

  const logins = [["admin_aura"], ["admin_rhone"]];
  logins.forEach((row, i) => {
    test(`suppression d'une amap en tant qu'admin ${row[0]}`, async ({
      page,
    }) => {
      await login(page, row[0]);
      await page.goto("/amap");
      await clickMenuLinkOnLine(
        page,
        "amap@test.amap-aura.org",
        "Supprimer l'AMAP"
      );
      await expect(page.locator(".modal.in")).toContainText(
        'Voulez-vous vraiment supprimer l\'AMAP "AMAP TEST" ?'
      );
      await expect(page.locator(".modal.in")).toContainText(
        "Attention ! Cette action supprimera tous les comptes et contrats liés à l'AMAP."
      );
      await page.locator(".modal.in").getByText(/OUI/).first().click();
      await flashMessageShouldContain(
        page,
        "L'AMAP a été supprimée avec succès."
      );
      await expect(page.locator(".container table")).not.toContainText(
        "amap@test.amap-aura.org"
      );
    });
  });
});
