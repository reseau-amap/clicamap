import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Modifier la liste des produits d'une AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Modification de produits, profil AMAP", async ({ page }) => {
    await login(page, "amap");
    await page.goto("/amap/recherche_paysan/1");
    await page
      .locator('[name="amap_produit_recherche[tpPropose][]"]')
      .selectOption([{ label: "Légumes" }, { label: "Fruits" }]);
    await page
      .locator('[name="amap_produit_recherche[tpRecherche][]"]')
      .selectOption([{ label: "Pain" }, { label: "Viande -> Porc" }]);
    await page.locator('[name="amap_produit_recherche[activation]"]').check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Fruits"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Pain | Porc"
    );
  });

  test("Modification de produits, profil réseau", async ({ page }) => {
    await login(page, "admin_aura");
    await page.goto("/amap/produits/1");
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption([{ label: "Légumes" }, { label: "Fruits" }]);
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption([{ label: "Pain" }, { label: "Viande -> Porc" }]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Fruits"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Pain | Porc"
    );
  });

  test("Modification de produits, profil super-admin", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amap/produits/1");
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption([{ label: "Légumes" }, { label: "Fruits" }]);
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption([{ label: "Pain" }, { label: "Viande -> Porc" }]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Fruits"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Pain | Porc"
    );
  });
});
