import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Ajouter un produit à la liste d'une AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Message erreur coche obligatoire, profil AMAP", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await page.locator("tr .dropdown-toggle").click();
    await page
      .getByText(/Modifier les produits proposés \/ recherchés/)
      .first()
      .click();
    await page
      .locator('[name="amap_produit_recherche[tpPropose][]"]')
      .selectOption({ label: "Autres / divers" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(
      "Le champ d'activation est requis."
    );
  });

  test("Ajout de produits effectué, profil AMAP", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await page.locator("tr .dropdown-toggle").click();
    await page
      .getByText(/Modifier les produits proposés \/ recherchés/)
      .first()
      .click();
    await page
      .locator('[name="amap_produit_recherche[tpPropose][]"]')
      .selectOption([
        { label: "Légumes" },
        { label: "Pain" },
        { label: "Viande -> Porc" },
      ]);
    await page
      .locator('[name="amap_produit_recherche[tpRecherche][]"]')
      .selectOption([
        { label: "Fruits" },
        { label: "Œufs" },
        { label: "Pisciculture" },
      ]);
    await page.locator('[name="amap_produit_recherche[activation]"]').click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Pain | Porc"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Fruits | Œufs | Pisciculture"
    );
  });

  test("Ajout de produits effectué, profil réseau", async ({ page }) => {
    await login(page, "admin_aura");
    await page.goto("/amap/produits/1");
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption([
        { label: "Légumes" },
        { label: "Pain" },
        { label: "Viande -> Porc" },
      ]);
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption([
        { label: "Fruits" },
        { label: "Œufs" },
        { label: "Pisciculture" },
      ]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Pain | Porc"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Fruits | Œufs | Pisciculture"
    );
  });

  test("Ajout de produits effectué, profil super admin", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amap/produits/1");
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption([
        { label: "Légumes" },
        { label: "Pain" },
        { label: "Viande -> Porc" },
      ]);
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption([
        { label: "Fruits" },
        { label: "Œufs" },
        { label: "Pisciculture" },
      ]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await page.goto("/amap/display/1");
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Légumes | Pain | Porc"
    );
    await expect(page.locator(".container > .row .well").first()).toContainText(
      "Fruits | Œufs | Pisciculture"
    );
  });
});
