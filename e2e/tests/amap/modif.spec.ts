import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillReadOnly,
} from "../../support/shorcut_actions";

test.describe("Vérifier la mise à jour d'une AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Comme amap, je doit pouvoir modifier mon logo`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/amap/mon_amap");
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Modifier les informations générales"
    );
    await expect(await page.locator(".amap-logo").getAttribute("src")).toEqual(
      ""
    );
    await page
      .locator('input[name="amap_edition[logo]"]')
      .setInputFiles("e2e/data/img_50k.jpg");
    await expect(
      await page.locator(".amap-logo").getAttribute("src")
    ).toContain("data:image/jpeg;base64");
    await expect(page.locator(".help-logo")).toBeVisible();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été modifiée avec succès.");
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Modifier les informations générales"
    );
    await expect(
      await page.locator(".amap-logo").getAttribute("src")
    ).toContain("http://localhost/uploads/logo_amap/");
  });

  const testAdmin = [
    [
      "admin_aura",
      " ",
      "69007, LYON 7e Arrondissement ",
      "amaptest@tutanota.com",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin_aura",
      "Nom de l'AMAP",
      "69007",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      "admin_aura",
      "Nom de l'AMAP",
      "Lyon",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      "admin_aura",
      "Nom de l'AMAP",
      " ",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      "admin_aura",
      "Nom de l'AMAP",
      "69007, LYON 7e Arrondissement",
      "amaptest",
      "Cette valeur n'est pas une adresse email valide.",
    ],

    [
      "admin_aura",
      " ",
      " ",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      "admin_aura",
      "Nom de l'AMAP",
      "69007, LYON 7e Arrondissement",
      "amaptest@tutanota.com",
      "L'AMAP a été modifiée avec succès.",
    ],
  ];

  testAdmin.forEach((row, i) => {
    test(`Tester tous les messages, profils admin ${i}`, async ({ page }) => {
      await login(page, row[0]);
      await page.goto("/amap");
      await clickMenuLinkOnLine(
        page,
        "amap@test.amap-aura.org",
        "Modifier les informations générales"
      );
      await expect(page).toHaveURL(/\/amap\/informations_generales_edition\/1/);
      await page.locator('input[name="amap_edition[nom]"]').clear();
      await page.locator('input[name="amap_edition[nom]"]').fill(row[1]);
      await fillReadOnly(
        page.locator('input[name="amap_edition[adresseAdminVille]"]'),
        row[2]
      );

      await page.locator('input[name="amap_edition[email]"]').clear();
      await page.locator('input[name="amap_edition[email]"]').fill(row[3]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator("body")).toContainText(row[4]);
    });
  });

  const testSuperAdmin = [
    [
      "admin",
      " ",
      "69007, LYON 7e Arrondissement",
      "amaptest@tutanota.com",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "admin",
      "Nom de l'AMAP",
      "69007",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      "admin",
      "Nom de l'AMAP",
      "Lyon",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      "admin",
      "Nom de l'AMAP",
      " ",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      "admin",
      "Nom de l'AMAP",
      "69007, LYON 7e Arrondissement",
      "amaptest",
      "Cette valeur n'est pas une adresse email valide.",
    ],

    [
      "admin",
      " ",
      " ",
      "amaptest@tutanota.com",
      'Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      "admin",
      "Nom de l'AMAP",
      "69007, LYON 7e Arrondissement",
      "amaptest@tutanota.com",
      "L'AMAP a été modifiée avec succès.",
    ],
  ];

  testSuperAdmin.forEach((row, i) => {
    test(`Tester tous les messages, profils super admin ${i}`, async ({
      page,
    }) => {
      await login(page, row[0]);
      await page.goto("/amap");
      await page
        .locator('[name="search_amap[region]"]')
        .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
      await clickMenuLinkOnLine(
        page,
        "amap@test.amap-aura.org",
        "Modifier les informations générales"
      );
      await expect(page).toHaveURL(/\/amap\/informations_generales_edition\/1/);
      await page.locator('input[name="amap_edition[nom]"]').clear();
      await page.locator('input[name="amap_edition[nom]"]').fill(row[1]);
      await fillReadOnly(
        page.locator('input[name="amap_edition[adresseAdminVille]"]'),
        row[2]
      );
      await page.locator('input[name="amap_edition[email]"]').clear();
      await page.locator('input[name="amap_edition[email]"]').fill(row[3]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator("body")).toContainText(row[4]);
    });
  });

  const testAmap = [
    [
      " ",
      " 69007, LYON 7e Arrondissement ",
      " amaptest@tutanota.com ",
      " Cette valeur ne doit pas être vide. ",
    ],

    [
      " Nom de l'AMAP ",
      " 69007 ",
      " amaptest@tutanota.com ",
      ' Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      " Nom de l'AMAP ",
      " Lyon",
      " amaptest@tutanota.com ",
      ' Le champ "Code postal, Ville" est mal formaté.',
    ],

    [
      " Nom de l'AMAP ",
      " ",
      " amaptest@tutanota.com ",
      ' Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      " Nom de l'AMAP ",
      " 69007, LYON 7e Arrondissement ",
      " amaptest",
      " Cette valeur n'est pas une adresse email valide.",
    ],

    [
      " ",
      " ",
      " amaptest@tutanota.com ",
      ' Le champ "Code postal, Ville administrative" est requis.',
    ],

    [
      " Nom de l'AMAP ",
      " 69007, LYON 7e Arrondissement ",
      " amaptest@tutanota.com ",
      " L'AMAP a été modifiée avec succès.",
    ],
  ];

  testAmap.forEach((row, i) => {
    test(`Tester tous les messages, profils amap ${i}`, async ({ page }) => {
      await login(page, "amap");
      await page.goto("/amap/mon_amap");
      await clickMenuLinkOnLine(
        page,
        "amap@test.amap-aura.org",
        "Modifier les informations générales"
      );
      await expect(page).toHaveURL(/\/amap\/informations_generales_edition\/1/);
      await page.locator('input[name="amap_edition[nom]"]').clear();
      await page.locator('input[name="amap_edition[nom]"]').fill(row[0]);
      await fillReadOnly(
        page.locator('input[name="amap_edition[adresseAdminVille]"]'),
        row[1]
      );
      await page.locator('input[name="amap_edition[email]"]').clear();
      await page.locator('input[name="amap_edition[email]"]').fill(row[2]);
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await expect(page.locator("body")).toContainText(row[3]);
    });
  });
});
