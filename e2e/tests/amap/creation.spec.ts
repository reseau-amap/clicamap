import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillReadOnly,
} from "../../support/shorcut_actions";

test.describe("Création d'une nouvelle AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Creation valide - profil admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .getByText(/Créer une nouvelle AMAP/)
      .first()
      .click();
    await page.locator('[name="amap_creation[nom]"]').fill("Nouvelle AMAP");
    await page
      .locator('[name="amap_creation[etat]"]')
      .locator('input[value="creation"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[associationDeFait]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[possedeAssurance]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[amapEtudiante]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[email]"]')
      .fill("public@email.com");
    await fillReadOnly(
      page.locator('input[name="amap_creation[adresseAdminVille]"]'),
      "69007, LYON 7e Arrondissement"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Les lieux de livraison/);
    await page.locator('[name="amap_livraison_lieu[nom]"]').fill("LL nouvelle");
    await page.locator('[name="amap_livraison_lieu[adresse]"]').fill("123");
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[ville]"]'),
      "69001, LYON 1er Arrondissement"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLatitude]"]'),
      "1.0"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLongitude]"]'),
      "2.0"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_livraison_horaire[heureDebut]"]')
      .fill("16:00");
    await page
      .locator('[name="amap_livraison_horaire[heureFin]"]')
      .fill("18:00");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption({ label: "Céréales et légumineuses" });
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption({ label: "Vin et bières" });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-success")).toHaveText(
      /LL nouvelle - 123, 69001 Lyon 1er Arrondissement GPS Lat : 1\.0\. GPS Long : 2\.0\./
    );
    await expect(page.locator(".alert-success")).toHaveText(
      /Horaire de livraison : Ete, Lundi : 16:00 - 16:00\./
    );
    await expect(page.locator(".container table")).toContainText(
      "Nom :Nouvelle AMAP"
    );
    await expect(page.locator(".container table")).toContainText(
      "Adresse administrative -> Adresse : 69007, Lyon 7e Arrondissement"
    );
    await expect(page.locator(".container table")).toContainText(
      "Email public :public@email.com"
    );
    await expect(page.locator(".container table")).toContainText(
      "Produits proposés :Céréales et légumineuses"
    );
    await expect(page.locator(".container table")).toContainText(
      "Produits recherchés :Vin et bières"
    );
    await expect(page.locator(".container table")).toContainText(
      "État :En création"
    );
    await expect(page.locator(".container table")).toContainText(
      "Assurance :Oui"
    );
    await expect(page.locator(".container table")).toContainText(
      "Etudiante :Oui"
    );
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été crée avec succès.");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(page.locator(".container table")).toContainText(
      "NOUVELLE AMAP"
    );
  });

  test("Creation valide avec logo - profil admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .getByText(/Créer une nouvelle AMAP/)
      .first()
      .click();
    await page.locator('[name="amap_creation[nom]"]').fill("Nouvelle AMAP");
    await page
      .locator('[name="amap_creation[etat]"]')
      .locator('input[value="creation"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[associationDeFait]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[possedeAssurance]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[amapEtudiante]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[email]"]')
      .fill("public@email.com");
    await fillReadOnly(
      page.locator('input[name="amap_creation[adresseAdminVille]"]'),
      "69007, LYON 7e Arrondissement"
    );

    await page
      .locator('input[name="amap_creation[logo]"]')
      .setInputFiles("e2e/data/img_50k.jpg");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Les lieux de livraison/);
    await page.locator('[name="amap_livraison_lieu[nom]"]').fill("LL nouvelle");
    await page.locator('[name="amap_livraison_lieu[adresse]"]').fill("123");
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[ville]"]'),
      "69001, LYON 1er Arrondissement"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLatitude]"]'),
      "1.0"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLongitude]"]'),
      "2.0"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_livraison_horaire[heureDebut]"]')
      .fill("16:00");
    await page
      .locator('[name="amap_livraison_horaire[heureFin]"]')
      .fill("18:00");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption({ label: "Céréales et légumineuses" });
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption({ label: "Vin et bières" });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-success")).toHaveText(
      /LL nouvelle - 123, 69001 Lyon 1er Arrondissement GPS Lat : 1\.0\. GPS Long : 2\.0\./
    );
    await expect(page.locator(".alert-success")).toHaveText(
      /Horaire de livraison : Ete, Lundi : 16:00 - 16:00\./
    );
    await expect(page.locator(".container table")).toContainText(
      "Nom :Nouvelle AMAP"
    );
    await expect(page.locator(".container table")).toContainText(
      "Adresse administrative -> Adresse : 69007, Lyon 7e Arrondissement"
    );
    await expect(page.locator(".container table")).toContainText(
      "Email public :public@email.com"
    );
    await expect(page.locator(".container table")).toContainText(
      "Produits proposés :Céréales et légumineuses"
    );
    await expect(page.locator(".container table")).toContainText(
      "Produits recherchés :Vin et bières"
    );
    await expect(page.locator(".container table")).toContainText(
      "État :En création"
    );
    await expect(page.locator(".container table")).toContainText(
      "Assurance :Oui"
    );
    await expect(page.locator(".container table")).toContainText(
      "Etudiante :Oui"
    );
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été crée avec succès.");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(page.locator(".container table")).toContainText(
      "NOUVELLE AMAP"
    );
    await clickMenuLinkOnLine(page, "NOUVELLE AMAP", "Détails de l'AMAP");
    await expect(page.locator(".amap-logo")).toBeVisible();
  });

  test("Creation valide avec un admin - profil admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .getByText(/Créer une nouvelle AMAP/)
      .first()
      .click();
    await page.locator('[name="amap_creation[nom]"]').fill("Nouvelle AMAP");
    await page
      .locator('[name="amap_creation[etat]"]')
      .locator('input[value="creation"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[associationDeFait]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[possedeAssurance]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[amapEtudiante]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[email]"]')
      .fill("public@email.com");
    await page.locator('[name="amap_creation[admin]"]').fill("empty");
    await fillReadOnly(
      page.locator('input[name="amap_creation[adresseAdminVille]"]'),
      "69007, LYON 7e Arrondissement"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page.locator('[name="amap_livraison_lieu[nom]"]').fill("LL nouvelle");
    await page.locator('[name="amap_livraison_lieu[adresse]"]').fill("123");
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[ville]"]'),
      "69001, LYON 1er Arrondissement"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLatitude]"]'),
      "1.0"
    );
    await fillReadOnly(
      page.locator('input[name="amap_livraison_lieu[gpsLongitude]"]'),
      "2.0"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_livraison_horaire[heureDebut]"]')
      .fill("16:00");
    await page
      .locator('[name="amap_livraison_horaire[heureFin]"]')
      .fill("18:00");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="amap_produit[tpPropose][]"]')
      .selectOption({ label: "Céréales et légumineuses" });
    await page
      .locator('[name="amap_produit[tpRecherche][]"]')
      .selectOption({ label: "Vin et bières" });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-success")).toHaveText(
      /LL nouvelle - 123, 69001 Lyon 1er Arrondissement GPS Lat : 1\.0\. GPS Long : 2\.0\./
    );
    await expect(page.locator(".alert-success")).toHaveText(
      /Horaire de livraison : Ete, Lundi : 16:00 - 16:00\./
    );
    await expect(page.locator(".container table")).toContainText(
      "Administrateur de l'AMAP -> Prénom / Nom"
    );
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'AMAP a été crée avec succès.");
    await login(page, "empty");
    await expect(page.locator("nav")).toHaveText(/Gestionnaire AMAP/);
  });

  test("Etape précédente admin - profil admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .getByText(/Créer une nouvelle AMAP/)
      .first()
      .click();
    await page.locator('[name="amap_creation[nom]"]').fill("Nouvelle AMAP");
    await page
      .locator('[name="amap_creation[etat]"]')
      .locator('input[value="creation"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[associationDeFait]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[possedeAssurance]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[amapEtudiante]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .locator('[name="amap_creation[email]"]')
      .fill("public@email.com");
    await page.locator('[name="amap_creation[admin]"]').fill("empty");
    await fillReadOnly(
      page.locator('input[name="amap_creation[adresseAdminVille]"]'),
      "69007, LYON 7e Arrondissement"
    );

    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape précédente/)
      .first()
      .click();
    await expect(page.locator('[name="amap_creation[admin]"]')).toHaveValue(
      "empty"
    );
  });
});
