import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Visionner le profil d'une AMAP avec tous les profils possibles", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Visualiser le profil de l'AMAP, profil AMAP`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion de mon AMAP");
    await currentPathShouldBe(page, "/amap/mon_amap");
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Détails de l'AMAP"
    );
    await currentPathShouldBe(page, "/amap/display/1");
    await expect(page.locator("h5")).toContainText("Gestion de mon AMAP");
  });

  test(`Visualiser le profil d'une AMAP, super-admin`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire Admin");
    await clickMainMenuLink(page, "Gestion des AMAP");
    await currentPathShouldBe(page, "/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Détails de l'AMAP"
    );
    await currentPathShouldBe(page, "/amap/display/1");
    await expect(page.locator("h5")).toContainText("Gestion des AMAP");
  });

  test(`Visualiser le profil d'une AMAP,réseau`, async ({ page }) => {
    await login(page, "admin_aura");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire Admin");
    await clickMainMenuLink(page, "Gestion des AMAP");
    await currentPathShouldBe(page, "/amap");
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Détails de l'AMAP"
    );
    await currentPathShouldBe(page, "/amap/display/1");
    await expect(page.locator("h5")).toContainText("Gestion des AMAP");
  });
});
