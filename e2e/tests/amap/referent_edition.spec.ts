import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Edition du référent réseau", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant qu'admin je doit pouvoir accéder à l'interface depuis le menu", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Modifier les rôles de l'AMAP"
    );
    await page.getByText("Le correspondant réseau AMAP").first().click();
    await expect(page.locator("body")).toContainText(
      "Le correspondant réseau AMAP"
    );
    await expect(page.locator("body")).toContainText(
      "Si vous voulez ajouter un correspondant réseau à l'AMAP"
    );
  });

  test("En tant qu'admin je doit ajouter un correspondant réseau", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap/referent_edition/1");
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await expect(page.locator("body")).toContainText(
      "Le référent réseau a été mis à jour avec succès."
    );
    await page.goto("/amap/referent_edition/1");
    await expect(page.locator(".btn-primary.btn-sm")).toContainText(
      "Prenom amapien NOM AMAPIEN"
    );
  });

  test("En tant qu'admin je ne doit pas pouvoir valider le formulaire d'édition sans référent selectionné", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap/referent_edition/1");
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Merci de sélectionner un correspondant."
    );
  });

  test("En tant qu'admin je doit supprimer un corresponant réseau", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap/referent_edition/1");
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await page.goto("/amap/referent_edition/1");
    await page
      .locator(".btn-sm")
      .getByText(/Prenom amapien NOM AMAPIEN/)
      .first()
      .click();
    await expect(page.locator(".modal.in .alert")).toContainText(
      'Voulez-vous vraiment supprimer le rôle de correspondant réseau à "Prenom amapien NOM AMAPIEN" ?'
    );
    await page.getByText(/OUI/).first().click();
    await expect(page.locator(".alert-warning")).toContainText(
      "L'AMAP n'a pas encore de correspondant réseau AMAP."
    );
  });
});
