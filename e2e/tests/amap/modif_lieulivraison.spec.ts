import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  smockerMockAmapAura19quaitilsitt,
  smockerReset,
} from "../../support/shorcut_smocker";

test.describe("Modification du lieu de livraison d'une amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await smockerReset();
  });

  test("En tant qu'admin modifier le lieu de livraison d'une amap", async ({
    page,
  }) => {
    await smockerMockAmapAura19quaitilsitt();
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Modifier les lieux de livraison"
    );
    await clickMenuLinkOnLine(
      page,
      "Ll amap test",
      "Modifier le lieu de livraison"
    );
    await page
      .locator('input[name="liv_autocomplete"]')
      .fill("19, Quai Tilsitt 69002 Lyon");
    await page.locator("#container_liv_autocomplete li").first().click();
    await page.locator('input[name="amap_livraison_lieu[adresse]"]').clear();
    await page
      .locator('input[name="amap_livraison_lieu[adresse]"]')
      .fill("test");
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Les lieux de livraison/);
    await expect(page.locator(".container table.table")).toHaveText(
      /Test, 69002 LYON/
    );
  });
});
