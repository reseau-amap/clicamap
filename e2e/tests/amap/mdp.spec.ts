import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Envoyer un mot de passe à une amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Envoyer un mot de passe à une amap", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Envoyer un mot de passe à l'AMAP"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Voulez-vous vraiment envoyer un mot de passe à l'AMAP amap test ?"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Attention ! cette action créera un mot de passe pour tous les administrateurs de l'AMAP (même s'ils en possèdent déjà un) qui leur seront envoyés par email : amap@test.amap-aura.org"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le mot de passe a été envoyé avec succès."
    );
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amap@test.amap-aura.org",
    ]);
  });
});
