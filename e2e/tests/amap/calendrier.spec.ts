import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { dateClean, dateSet } from "../../support/shorcut_date";
import {
  subscribeContract,
  subscribeContractAmapienToValidate,
  supprimerAmapien,
} from "../../support/shorcut";

test.describe("Calendrier de livraison de l'AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Affichage du détail de livraison par date", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });

    await subscribeContract(page, "amapienref", "contrat 1", {
      "1_1": 2,
      "1_2": 0,
      "2_1": 2,
      "2_2": 0,
      "3_1": 2,
      "3_2": 0,
      "4_1": 2,
      "4_2": 0,
      "5_1": 2,
      "5_2": 0,
    });
    await login(page, "amap");
    await page.goto("/amap/calendrier/1/2030/01");

    await expect(page.locator(".calendrier .btn-primary")).toHaveCount(5);
    await page
      .locator(".calendrier .btn-primary")
      .getByText(/1/)
      .first()
      .click();
    await page
      .locator(".modal.in .btn-primary")
      .getByText(/Détail par amapien/)
      .first()
      .click();
    await expect(page.locator(".modal.in .well > ul")).toHaveText(
      /3 Produit 1 \(cond1 \/ 1,00 €\)/
    );
    await expect(page.locator(".modal.in .well > ul")).toHaveText(
      /1 Produit 2 \(cond2 \/ 2,00 €\)/
    );
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/NOM AMAPIEN Prenom amapien/);
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/1 Produit 1 \(cond1 \/ 1,00 €\)/);
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/1 Produit 2 \(cond2 \/ 2,00 €\)/);
    await expect(
      page.locator(".modal.in .alert-success div:last-child")
    ).toHaveText(/NOM AMAPIEN RÉFÉRENT Prénom amapien référent/);
    await expect(
      page.locator(".modal.in .alert-success div:last-child")
    ).toHaveText(/2 Produit 1 \(cond1 \/ 1,00 €\)/);
  });

  test("Masquer les contrats en cours de validation", async ({ page }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );

    await login(page, "amap");
    await page.goto("/amap/calendrier/1/2030/01");

    await expect(page.locator(".calendrier .btn-primary")).toHaveCount(0);
  });

  test("Garder le détail des livraisons pour les amapiens supprimés", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    dateSet("2030-03-19");
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page.goto("/amap/calendrier/1/2030/01");
    await page
      .locator(".calendrier .btn-primary")
      .getByText(/1/)
      .first()
      .click();
    await page
      .locator(".modal.in .btn-primary")
      .getByText(/Détail par amapien/)
      .first()
      .click();
    await expect(page.locator(".modal.in .well > ul")).toHaveText(
      /1 Produit 1 \(cond1 \/ 1,00 €\)/
    );
    await expect(page.locator(".modal.in .well > ul")).toHaveText(
      /1 Produit 2 \(cond2 \/ 2,00 €\)/
    );
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/NOM AMAPIEN Prenom amapien/);
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/1 Produit 1 \(cond1 \/ 1,00 €\)/);
    await expect(
      page.locator(".modal.in .alert-success div:first-child")
    ).toHaveText(/1 Produit 2 \(cond2 \/ 2,00 €\)/);
  });
});
