import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Modifier les admnistrateurs d'une AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Ajout d'un administrateur à l'AMAP comme admin", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await page.locator('[name="user_verification[user]"]').fill("amapien");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les administrateurs ont été mis à jour avec succès."
    );
    await expect(page.locator(".container table")).toHaveText(/prenom amapien/);
    await login(page, "amapien");
    await expect(page.locator("nav")).toHaveText(/Gestionnaire AMAP/);
  });

  test("Suppression d'un administrateur à l'AMAP  comme admin", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await clickMenuLinkOnLine(
      page,
      "amap_prenom",
      "Supprimer l'administrateur"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Êtes-vous sûr de vouloir supprimer l'administrateur ?"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Les administrateurs ont été mis à jour avec succès."
    );
    await login(page, "amap");
    await expect(page.locator("nav")).not.toHaveText(/Gestionnaire AMAP/);
  });

  test("Ajout d'un administrateur à l'AMAP comme amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await page.locator('[name="user_verification[user]"]').fill("amapien");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les administrateurs ont été mis à jour avec succès."
    );
    await expect(page.locator(".container table")).toHaveText(/prenom amapien/);
    await login(page, "amapien");
    await expect(page.locator("nav")).toHaveText(/Gestionnaire AMAP/);
  });

  test("Suppression d'un administrateur à l'AMAP comme amap", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await page.locator('[name="user_verification[user]"]').fill("amapien");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Les administrateurs ont été mis à jour avec succès."
    );
    await clickMenuLinkOnLine(
      page,
      "prenom amapien",
      "Supprimer l'administrateur"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Êtes-vous sûr de vouloir supprimer l'administrateur ?"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Les administrateurs ont été mis à jour avec succès."
    );
    await login(page, "amapien");
    await expect(page.locator("nav")).not.toHaveText(/Gestionnaire AMAP/);
  });

  test("Suppression d'un administrateur à l'AMAP comme amap - erreur suppression soit meme", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await clickMenuLinkOnLine(
      page,
      "amap_prenom",
      "Supprimer l'administrateur"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Êtes-vous sûr de vouloir supprimer l'administrateur ?"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Impossible de se supprimer soit-même des administrateurs de l'AMAP."
    );
    await expect(page.locator("nav")).toHaveText(/Gestionnaire AMAP/);
  });

  test("Ajout d'un administrateur non existant à l'AMAP comme admin - retour", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await page.locator('[name="user_verification[user]"]').fill("invalid");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await expect(page.locator("form ~ .alert-danger")).toHaveText(
      /Aucun utilisateur n'a été trouvé avec cet email ou cet identifiant\./
    );
    await page
      .locator(".alert-danger")
      .getByText(/cliquez ici/)
      .first()
      .click();
    await page
      .getByText(/Annuler/)
      .first()
      .click();
    await expect(page.locator("h3")).toHaveText(/Administrateurs de l'AMAP/);
  });

  test("Ajout d'un administrateur non existant à l'AMAP comme admin", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amap");
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Gérer les administrateurs"
    );
    await page.locator('[name="user_verification[user]"]').fill("invalid");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await expect(page.locator("form ~ .alert-danger")).toHaveText(
      /Aucun utilisateur n'a été trouvé avec cet email ou cet identifiant\./
    );
    await page
      .locator(".alert-danger")
      .getByText(/cliquez ici/)
      .first()
      .click();
    await page.locator('[name="user_profil[username]"]').fill("username");
    await page
      .locator('[name="user_profil[name][firstName]"]')
      .fill("firstname");
    await page.locator('[name="user_profil[name][lastName]"]').fill("lastName");
    await page
      .locator('[name="user_profil[ville]"]')
      .fill("69001, Lyon 1er Arrondissement");
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Administrateur ajouté avec succès.");
    await expect(page.locator("h3")).toHaveText(/Administrateurs de l'AMAP/);
    await expect(page.locator(".container table")).toHaveText(/firstname/);
  });
});
