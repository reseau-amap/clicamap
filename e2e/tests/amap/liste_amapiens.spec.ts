import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  menuLinkOnLineShouldContain,
  menuLinkOnLineShouldNotContain,
} from "../../support/shorcut_assertions";

test.describe("Accès direct aux amapiens associés dans la liste des amaps", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("En tant que super admin, je doit voir un lien avec les amapiens associés dans la liste des amaps", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des AMAP/)
      .first()
      .click();
    await page
      .locator('[name="search_amap[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amap@test.amap-aura.org",
      "Voir les amapiens"
    );
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
    await expect(
      page.locator('[name="search_amapien[region]"] option[selected]')
    ).toHaveText("AUVERGNE-RHÔNE-ALPES");
    await expect(
      page.locator('[name="search_amapien[departement]"] option[selected]')
    ).toHaveText("RHÔNE");
    await expect(
      page.locator('[name="search_amapien[amap]"] option[selected]')
    ).toHaveText("amap test");
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(` En tant qu'admin, je doit voir un lien avec les amapiens associés dans la liste des amaps ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page
        .getByText(/Gestionnaire Admin/)
        .first()
        .click();
      await page
        .getByText(/Gestion des AMAP/)
        .first()
        .click();
      await clickMenuLinkOnLine(
        page,
        "amap@test.amap-aura.org",
        "Voir les amapiens"
      );
      await expect(page.locator("h3")).toContainText("Liste des amapiens");
      await expect(page.locator(".container table")).toContainText(
        "amapien@test.amap-aura.org"
      );
      await expect(
        page.locator('[name="search_amapien[region]"] option[selected]')
      ).toHaveText("AUVERGNE-RHÔNE-ALPES");
      await expect(
        page.locator('[name="search_amapien[departement]"] option[selected]')
      ).toHaveText("RHÔNE");
      await expect(
        page.locator('[name="search_amapien[amap]"] option[selected]')
      ).toHaveText("amap test");
    });
  });

  test("En tant qu'amap, je ne doit voir un lien avec les amapiens associés dans la liste des amaps", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de mon AMAP/)
      .first()
      .click();
    await menuLinkOnLineShouldContain(
      page,
      "amap@test.amap-aura.org",
      "Détails de l'AMAP"
    );
    await menuLinkOnLineShouldNotContain(
      page,
      "amap@test.amap-aura.org",
      "Voir les amapiens"
    );
  });
});
