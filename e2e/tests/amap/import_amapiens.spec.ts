import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMainMenuLink, supprimerAmapien } from "../../support/shorcut";

test.describe("Import des amapiens comme AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Import valide`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await page.getByRole("link", { name: "Importer une liste" }).click();
    await page
      .locator('input[name="amapiens"]')
      .setInputFiles("e2e/data/import_amapiens.xls");
    await page
      .getByText(/Importer/)
      .first()
      .click();
    await expect(page.locator(".modal.in")).toContainText(
      "Les amapiens ont été ajoutés avec succès."
    );
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await expect(page.locator(".container table")).toContainText("DE BATS");
    await expect(page.locator(".container table")).toContainText("PAYSAN_NOM");
  });

  test(`Import email duplication`, async ({ page }) => {
    await login(page, "amap");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await page.getByRole("link", { name: "Importer une liste" }).click();
    await page
      .locator('input[name="amapiens"]')
      .setInputFiles("e2e/data/import_amapiens_email_duplication.xls");
    await page
      .getByText(/Importer/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "L'email yves.debats@gmail.com n'est pas unique dans le fichier XLS."
    );
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await expect(page.locator(".container table")).not.toContainText("DE BATS");
    await expect(page.locator(".container table")).not.toContainText(
      "PAYSAN_NOM"
    );
  });

  test(`Import email amapien supprimé`, async ({ page }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page.goto("/evenement");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await page.getByRole("link", { name: "Importer une liste" }).click();
    await page
      .locator('input[name="amapiens"]')
      .setInputFiles("e2e/data/import_amapiens_suppression.xls");
    await page
      .getByText(/Importer/)
      .first()
      .click();
    await expect(page.locator(".modal.in")).toContainText(
      "Les amapiens ont été ajoutés avec succès."
    );
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
  });
});
