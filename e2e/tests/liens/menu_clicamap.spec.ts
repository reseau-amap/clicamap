import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { allUsersNotEmpty } from "../../support/provider";

test.describe("Je dois voir les liens du menu Clic'AMAP dès ma connexion quel que soit mon profil si j'ai au moins un role", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  allUsersNotEmpty.forEach((user) => {
    test(`J'ai accès aux mentions légales quel que soit mon profil ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page
        .getByText(/Clic'AMAP/)
        .first()
        .click();
      await page
        .getByText(/Les événements/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/evenement/);
      await page
        .getByText(/Clic'AMAP/)
        .first()
        .click();
      await page
        .getByText(/Documentation/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/documentation/);
    });
  });
});
