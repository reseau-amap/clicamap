import { test, expect, Page } from "@playwright/test";
import { login } from "../../support/shorcut_auth";

import { allUsers } from "../../support/provider";

test.describe("Je dois pouvoir accéder au lien vers les mentions légales en étant connecté et ce, quel que soit mon profil.", () => {
  allUsers.forEach((user) => {
    test(`J'ai accès aux mentions légales quel que soit mon profil ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page
        .getByText(/Mentions légales/)
        .first()
        .click();
      await expect(page).toHaveURL(/\/contact\/mentions_legales/);
    });
  });
});
