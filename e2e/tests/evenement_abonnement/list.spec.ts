import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { enableExdebug } from "../../support/shorcut_debug";
import exp = require("node:constants");

const goToList = async (page: Page, user: string = "empty") => {
  await login(page, user);
  await page.getByRole("link", { name: "Je veux suivre" }).click();
  await expect(page.locator("h3")).toContainText("Gestion des abonnements");

  // Wait for the datatable to be loaded
  await expect(
    page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
  ).toContainText("AIN");
};

test.describe("Comme utilisateur, je devrais pouvoir lister les abonnements disponibles", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Liste avec du serveur side rendering", async ({ page }) => {
    await goToList(page);
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("AIN");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("AISNE");
    await page.locator(".paginate_button").getByText("2").click();
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("DEUX-SÈVRES");
  });

  test("Filtre par région", async ({ page }) => {
    await goToList(page);
    await expect(
      page.locator('[name="search_evenement_abonnable[departement]"]')
    ).toBeDisabled();
    await page
      .locator('[name="search_evenement_abonnable[region]"]')
      .selectOption("BOURGOGNE-FRANCHE-COMTÉ");
    await expect(
      page.locator('[name="search_evenement_abonnable[departement]"]')
    ).toBeEnabled();
    await expect(
      page.locator('[name="search_evenement_abonnable[departement]"] option')
    ).toHaveCount(9);
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("BOURGOGNE-FRANCHE-COMTÉ");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("CÔTE-D'OR");
  });

  test("Filtre par département", async ({ page }) => {
    await goToList(page);
    await expect(
      page.locator('[name="search_evenement_abonnable[departement]"]')
    ).toBeDisabled();
    await page
      .locator('[name="search_evenement_abonnable[region]"]')
      .selectOption("BOURGOGNE-FRANCHE-COMTÉ");
    await page
      .locator('[name="search_evenement_abonnable[departement]"]')
      .selectOption("CÔTE-D'OR");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("CÔTE-D'OR");
  });

  test("Filtre par type", async ({ page }) => {
    await goToList(page);
    await page
      .locator('[name="search_evenement_abonnable[type]"]')
      .selectOption("AMAP");
    await expect(page.locator(".table-datatable tbody tr")).toHaveCount(2);
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("amap test");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("amap test 2");
  });

  test("Filtre par keyword", async ({ page }) => {
    await goToList(page);
    await page
      .locator('[name="search_evenement_abonnable[keyword]"]')
      .fill("amap");
    await page
      .locator('[name="search_evenement_abonnable[keyword]"] ~ ul')
      .locator("li")
      .getByText("amap test 2")
      .click();
    await expect(page.locator(".table-datatable tbody tr")).toHaveCount(1);
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("amap test 2");
  });

  test("Filtre par abonnements existants", async ({ page }) => {
    await goToList(page, "amap");
    await page
      .locator('[name="search_evenement_abonnable[keyword]"]')
      .fill("amap");
    await page
      .locator('[name="search_evenement_abonnable[keyword]"] ~ ul')
      .locator("li")
      .getByText("amap test 2")
      .click();
    const button = page.locator(
      ".table-datatable tbody tr:nth-child(1) button"
    );
    await button.click();
    await expect(button).toHaveClass("btn btn-danger");
    await page.locator('[name="search_evenement_abonnable[keyword]"]').clear();
    await page
      .locator('[name="search_evenement_abonnable[subscriptionOnly]"]')
      .check();
    await expect(page.locator(".table-datatable tbody tr")).toHaveCount(5);

    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("amap test");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("amap test 2");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(3) td:nth-child(1)")
    ).toContainText("AUVERGNE-RHÔNE-ALPES");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(4) td:nth-child(1)")
    ).toContainText("ferme");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(5) td:nth-child(1)")
    ).toContainText("RHÔNE");
  });

  test("Tri par nom", async ({ page }) => {
    await goToList(page);

    const headNom = await page
      .locator(".table-datatable thead th")
      .getByText("Nom");
    await headNom.click();
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("YVELINES");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("YONNE");

    await headNom.click();
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
    ).toContainText("AIN");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(1)")
    ).toContainText("AISNE");
  });

  test("Tri par type", async ({ page }) => {
    await goToList(page);

    const headProfil = await page
      .locator(".table-datatable thead th")
      .getByText("Profil");
    await headProfil.click();
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(2)")
    ).toContainText("Amap");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(2)")
    ).toContainText("Amap");

    await headProfil.click();
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(2)")
    ).toContainText("Région");
    await expect(
      page.locator(".table-datatable tbody tr:nth-child(2) td:nth-child(2)")
    ).toContainText("Région");
  });

  test("Pas de tri par action", async ({ page }) => {
    await goToList(page);

    await expect(
      page.locator(".table-datatable thead th").getByText("Action")
    ).toHaveClass("sorting_disabled");
  });
});
