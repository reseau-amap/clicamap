import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { enableExdebug } from "../../support/shorcut_debug";
import { createNewEvtAmap } from "../../support/shorcut";

const goToList = async (page: Page, username: string) => {
  await login(page, username);
  await page.getByRole("link", { name: "Je veux suivre" }).click();
  await expect(page.locator("h3")).toContainText("Gestion des abonnements");
  await page
    .locator('[name="search_evenement_abonnable[type]"]')
    .selectOption("AMAP");
  await expect(
    page.locator(".table-datatable tbody tr:nth-child(1) td:nth-child(1)")
  ).toContainText("amap test");
};

test.describe("Comme utilisateur, je devrais pouvoir m'abonner aux actualités", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Abonnement impossible pour les abonnements par défaut", async ({
    page,
  }) => {
    await goToList(page, "amap");
    await expect(
      page.locator(
        ".table-datatable tbody tr:nth-child(1) td:nth-child(3) button"
      )
    ).toBeDisabled();
    await expect(
      page.locator(
        ".table-datatable tbody tr:nth-child(1) td:nth-child(3) button"
      )
    ).toContainText("Par défaut");
  });

  test("Abonnement", async ({ page }) => {
    await createNewEvtAmap(page, "Nouvelle actualité");

    await goToList(page, "empty");
    const button = page.locator(
      ".table-datatable tbody tr:nth-child(1) td:nth-child(3) button"
    );
    await expect(button).toHaveText("Je veux suivre");
    await button.click();
    await expect(button).toBeDisabled();
    await expect(button).toBeEnabled({ timeout: 10000 });
    await expect(button).toHaveText("Je ne veux plus suivre");

    await page
      .locator("h5")
      .getByRole("link", { name: "Fil d'actualités" })
      .click();
    await expect(page.locator(".block-evenement")).toHaveCount(1);
    await expect(page.locator(".block-evenement-titre")).toContainText(
      "Nouvelle actualité"
    );
  });

  test("Désabonnement", async ({ page }) => {
    await createNewEvtAmap(page, "Nouvelle actualité");

    await goToList(page, "empty");
    const button = page.locator(
      ".table-datatable tbody tr:nth-child(1) td:nth-child(3) button"
    );
    await expect(button).toHaveText("Je veux suivre");
    await button.click();
    await expect(button).toHaveText("Je ne veux plus suivre", {
      timeout: 10000,
    });
    await button.click();
    await expect(button).toBeDisabled();
    await expect(button).toBeEnabled({ timeout: 10000 });
    await expect(button).toHaveText("Je veux suivre");

    await page
      .locator("h5")
      .getByRole("link", { name: "Fil d'actualités" })
      .click();
    await expect(page.locator(".block-evenement")).toHaveCount(0);
  });
});
