import { test, expect } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { mailClean } from "../../support/shorcut_mail";
import {
  currentPathShouldBe,
  flashMessageShouldContain,
} from "../../support/shorcut_assertions";
import {
  extractJwtTokenFromUrl,
  jwtTokenShouldHaveField,
} from "../../support/shorcut_jwt";
import { login } from "../../support/shorcut_auth";

test.describe("En tant que plateforme CDP, je doit pouvoir authentifier les utilisateurs via ClicAMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`Pas de redirect Url`, async ({ page }) => {
    await page.goto("/cdp/authentification");
    await page.waitForLoadState();
    await currentPathShouldBe(page, "/portail/connexion");
  });

  test(`Authentification comme amapien`, async ({ page }) => {
    await page.goto(
      "/cdp/authentification?redirectUrl=http://localhost/CI/empty"
    );

    await page.locator('[name="authentification_cdp[user]"]').fill("amapien");
    await page
      .locator('[name="authentification_cdp[plainPassword]"]')
      .fill("P@ssw0rd!");
    await page.getByRole("button", { name: /Connexion/ }).click();

    await currentPathShouldBe(page, "/CI/empty");
    const token = extractJwtTokenFromUrl(page.url());
    jwtTokenShouldHaveField(token, "sub", "authentification");
    jwtTokenShouldHaveField(
      token,
      "amapien_uuid",
      "794d28b9-3d3d-45ad-82c3-d46cbe2046df"
    );
  });

  test(`Authentification comme autre type`, async ({ page }) => {
    await page.goto(
      "/cdp/authentification?redirectUrl=http://localhost/CI/empty"
    );

    await page.locator('[name="authentification_cdp[user]"]').fill("paysan");
    await page
      .locator('[name="authentification_cdp[plainPassword]"]')
      .fill("P@ssw0rd!");
    await page.getByRole("button", { name: /Connexion/ }).click();

    await currentPathShouldBe(page, "/cdp/authentification");
    await expect(page.locator(".alert-danger")).toContainText(
      "Cette interface est uniquement disponible pour les amapiens"
    );
  });

  test(`Accès déjà authentifié comme amapien`, async ({ page }) => {
    await login(page, "amapien");
    await page.goto(
      "/cdp/authentification?redirectUrl=http://localhost/CI/empty"
    );

    await currentPathShouldBe(page, "/CI/empty");
    const token = extractJwtTokenFromUrl(page.url());
    jwtTokenShouldHaveField(token, "sub", "authentification");
    jwtTokenShouldHaveField(
      token,
      "amapien_uuid",
      "794d28b9-3d3d-45ad-82c3-d46cbe2046df"
    );
  });

  test(`Accès déjà authentifié comme autre type`, async ({ page }) => {
    await login(page, "paysan");
    await page.goto(
      "/cdp/authentification?redirectUrl=http://localhost/CI/empty"
    );

    await currentPathShouldBe(page, "/portail/connexion");
    await flashMessageShouldContain(
      page,
      "Vous n'avez pas les accès suffisant pour consulter cette page, veuillez vous reconnecter."
    );
  });
});
