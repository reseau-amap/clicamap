import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { format } from "date-fns";
import fr from "date-fns/locale/fr";
import { mailClean } from "../../support/shorcut_mail";
import { fillDatepickerRange } from "../../support/shorcut_actions";

test.describe("Liste des creneaux disponibles en tant qu'amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Affichage d'une liste vide", async ({ page }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mon agenda/)
      .first()
      .click();
    await page
      .getByText(/Mes distrib'AMAP/)
      .first()
      .click();
    await expect(page.locator(".alert-info")).toContainText(
      "Cette page vous permet de vous inscrire à une ou plusieurs tâches proposées par votre AMAP."
    );
    await expect(page.locator(".alert-warning")).toContainText(
      "Pas de créneau disponible pour cette recherche"
    );
  });

  test("Recherche selon une date", async ({ page }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mon agenda/)
      .first()
      .click();
    await page
      .getByText(/Mes distrib'AMAP/)
      .first()
      .click();

    const now = new Date();
    await expect(
      page.locator('[name="search_distribution_amapien[period]"]')
    ).toHaveAttribute(
      "data-min-date",
      format(now, "yyyy-MM-dd", { locale: fr })
    );
    await fillDatepickerRange(
      page,
      "search_distribution_amapien[period]",
      "01/01/2030 au 01/12/2030"
    );
    await expect(page.locator(".alert-warning")).not.toBeVisible();
    await expect(page.locator(".container table")).toContainText("01/01/2030");
    await expect(page.locator(".container table")).toContainText("test");
    await expect(page.locator(".container table")).toContainText("17:00");
    await expect(page.locator(".container table")).toContainText("18:00");
  });

  test("Selection d'une amap si plusieurs disponibles", async ({ page }) => {
    await requestAddUserToAmap(page, "amapien", "amap test 2");
    await login(page, "amapien");
    await page
      .getByText(/Mon agenda/)
      .first()
      .click();
    await page
      .getByText(/Mes distrib'AMAP/)
      .first()
      .click();
    await expect(
      page.locator('[name="search_distribution_amapien[amap]"]')
    ).toHaveValue("");
    await expect(
      page.locator('[name="search_distribution_amapien[livLieu]"]')
    ).toHaveAttribute("disabled", /.*/);
    await page
      .locator('[name="search_distribution_amapien[amap]"]')
      .selectOption({ label: "amap test 2" });
    await expect(
      page.locator('[name="search_distribution_amapien[livLieu]"]')
    ).toHaveText("ll amap 2 test");
  });
});
