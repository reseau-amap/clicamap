import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { mailClean } from "../../support/shorcut_mail";
import { fillDatepickerRange } from "../../support/shorcut_actions";

test.describe("Inscription à une distribution en tant qu'amapienn", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Inscription à une distribution en tant qu'amapien", async ({
    page,
  }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mon agenda/)
      .first()
      .click();
    await page
      .getByText(/Mes distrib'AMAP/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_distribution_amapien[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page
      .getByText(/S'inscrire/)
      .first()
      .click();
    await page
      .locator('[name="search_distribution_amapien[filter]"]')
      .selectOption({
        label: "Afficher uniquement les créneaux auxquels je suis inscrit",
      });
    await expect(page.locator(".container table")).toContainText("0/1");
    await page.getByText(/Voir/).first().click();
    await expect(page.locator(".modal.in")).toContainText(
      "Liste des personnes inscrites"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "NOM AMAPIEN Prenom amapien"
    );
  });

  test('Affichage du message "Aucun inscrit" sur un creneau sans inscriptions', async ({
    page,
  }) => {
    await login(page, "amapien");
    await page
      .getByText(/Mon agenda/)
      .first()
      .click();
    await page
      .getByText(/Mes distrib'AMAP/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_distribution_amapien[period]",
      "01/01/2030 au 01/12/2030"
    );
    await page.getByText(/Voir/).first().click();
    await expect(page.locator(".modal.in")).toContainText(
      "Liste des personnes inscrites"
    );
    await expect(page.locator(".modal.in")).toContainText("Aucun inscrit");
  });
});
