import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepickerRange,
  select2,
} from "../../support/shorcut_actions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Désinscription d'un amapien  à une distribution en tant qu'amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Désinscription d'un amapien à une distribution en tant qu'amap", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN Prenom amapien"
    );
    await page
      .getByText(/Ajouter un ou plusieurs amapiens/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Amapiens ajoutés avec succès.");
    await expect(
      page.locator(".container .well:nth-child(5) table")
    ).toContainText("prenom amapien");
    await page
      .getByText(/Désinscrire/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Amapiens désinscrits avec succès.");
    await expect(
      page.locator(".container .well:nth-child(5) table")
    ).not.toContainText("prenom amapien");
    await expectMailShoudBeReceived(
      "Votre désinscription à la Distrib'AMAP du 01/01/2030",
      ["amapien@test.amap-aura.org"]
    );
  });
});
