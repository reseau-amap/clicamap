import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { setAmapienAdmin } from "../../support/shorcut";
import { mailClean } from "../../support/shorcut_mail";
import { fillDatepickerRange } from "../../support/shorcut_actions";

test.describe("Liste des creneaux disponibles en tant qu'amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Affichage d'une liste vide", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(page.locator(".alert-info")).toContainText(
      "Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions"
    );
    await expect(page.locator(".alert-warning")).toContainText(
      "0 créneaux pour cette recherche"
    );
  });

  test("Recherche selon une date", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(
      page.locator('[name="search_distribution_amap[period]"]')
    ).not.toHaveAttribute("data-min-date", /.*/);
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await expect(page.locator(".alert-warning")).toContainText(
      "1 créneau pour cette recherche"
    );
    await expect(page.locator(".container table")).toContainText("01/01/2030");
    await expect(page.locator(".container table")).toContainText("test");
    await expect(page.locator(".container table")).toContainText("17:00");
    await expect(page.locator(".container table")).toContainText("18:00");
  });

  test("Selection d'une amap si plusieurs disponibles", async ({ page }) => {
    await requestAddUserToAmap(page, "amap", "amap test 2");
    await setAmapienAdmin(page, "amap", "amap2@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Agenda AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(
      page.locator('[name="search_distribution_amap[amap]"]')
    ).toHaveValue("");
    await expect(
      page.locator('[name="search_distribution_amap[livLieu]"]')
    ).toHaveAttribute("disabled", /.*/);
    await page
      .locator('[name="search_distribution_amap[amap]"]')
      .selectOption({ label: "amap test 2" });
    await expect(
      page.locator('[name="search_distribution_amap[livLieu]"]')
    ).toHaveText("ll amap 2 test");
  });
});
