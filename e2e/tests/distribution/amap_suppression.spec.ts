import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepickerRange,
  select2,
} from "../../support/shorcut_actions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Supression d'une distribution", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Supression d'une distribution en tant qu'amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(page.locator(".alert-info")).toContainText(
      "Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions"
    );
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN Prenom amapien"
    );
    await page
      .getByText(/Ajouter un ou plusieurs amapiens/)
      .first()
      .click();
    await page.goto("/distribution/index");
    await page.locator('[name="search_distribution_amap[complete]"]').check();
    await clickMenuLinkOnLine(page, "test", "Supprimer le créneau");
    await page
      .locator('.modal.in [title="Confirmer la suppression du créneau"]')
      .click({
        force: true,
      });
    await flashMessageShouldContain(
      page,
      "La distribution a été supprimée avec succès."
    );
    await expectMailShoudBeReceived(
      "Votre désinscription à la Distrib'AMAP du 01/01/2030",
      ["amapien@test.amap-aura.org"]
    );
  });
});
