import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepicker,
  fillDatepickerRange,
} from "../../support/shorcut_actions";
import { mailClean } from "../../support/shorcut_mail";

test.describe("Modification de la date d'une distribution", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Modification de la date d'une distribution en tant qu'amap", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(page.locator(".alert-info")).toContainText(
      "Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions"
    );
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 03/01/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Modifier le créneau");
    await fillDatepicker(
      page,
      '[name="amap_distribution_edit[date]"]',
      new Date("2030-01-02")
    );
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "La distribution a été modifiée avec succès."
    );
    await expect(page.locator(".container table")).toContainText("02/01/2030");
    await clickMenuLinkOnLine(page, "test", "Modifier le créneau");
    await expect(
      page.locator('[name="amap_distribution_edit[date]"]')
    ).toHaveValue("2030-01-02");
  });
});
