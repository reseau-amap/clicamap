import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepickerRange,
  select2,
} from "../../support/shorcut_actions";

import { supprimerAmapien } from "../../support/shorcut";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Inscription d'un amapien  à une distribution en tant qu'amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Inscription d'un amapien à une distribution en tant qu'amap", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(
      page
        .locator(".alert-info", {
          hasText:
            /Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions/,
        })
        .first()
    ).toBeVisible();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN Prenom amapien"
    );
    await page
      .getByText(/Ajouter un ou plusieurs amapiens/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Amapiens ajoutés avec succès.");
    await expect(page.locator(".table-datatable")).toContainText(
      "prenom amapien"
    );
    await expectMailShoudBeReceived(
      "Votre inscription à la Distrib'AMAP du 01/01/2030",
      ["amapien@test.amap-aura.org"]
    );
  });

  test("Inscription impossible d'un amapien supprimé à une distribution en tant qu'amap", async ({
    page,
  }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(
      page
        .locator(".alert-info", {
          hasText:
            /Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions/,
        })
        .first()
    ).toBeVisible();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await expect(
      page
        .locator(
          '[name="amap_distribution_amap_amapien_inscription[amapiens][]"] option'
        )
        .getByText("NOM AMAPIEN Prenom amapien")
    ).toHaveCount(0);
  });

  test("Suppression inscription lors de la suppression de l'amapien", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN Prenom amapien"
    );
    await page
      .getByText(/Ajouter un ou plusieurs amapiens/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Amapiens ajoutés avec succès.");

    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await expect(page.locator(".table-datatable")).toContainText(
      "Aucune donnée disponible dans le tableau"
    );
  });

  test("Inscription de plus amapiens que de place à une distribution en tant qu'amap", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Agenda/)
      .first()
      .click();
    await page
      .getByText(/Gestion de la distrib'AMAP/)
      .first()
      .click();
    await expect(
      page
        .locator(".alert-info", {
          hasText:
            /Cette page vous permet de gérer vos créneaux de distributions ainsi que les inscriptions/,
        })
        .first()
    ).toBeVisible();
    await fillDatepickerRange(
      page,
      "search_distribution_amap[period]",
      "01/01/2030 au 01/12/2030"
    );
    await clickMenuLinkOnLine(page, "test", "Gestion des inscriptions");
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN Prenom amapien"
    );
    await select2(
      page,
      "amap_distribution_amap_amapien_inscription[amapiens][]",
      "NOM AMAPIEN RÉFÉRENT Prénom amapien référent"
    );
    await page
      .getByText(/Ajouter un ou plusieurs amapiens/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Pas assez de places disponibles pour ajouter autant d'amapiens à cette distribution."
    );
    await expect(page.locator(".table-datatable")).not.toContainText(
      "prenom amapien"
    );
    await expect(page.locator(".table-datatable")).not.toContainText(
      "Prénom amapien référent"
    );
  });
});
