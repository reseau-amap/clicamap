import { test, expect, Page } from "@playwright/test";
import { environmentClean } from "../../support/shorcut_environment";

test.describe("En tant qu'utlisateur, je ne doit pas pouvoir accéder à la page de maintenance si celle-ci est désactivée", () => {
  test.beforeEach(async ({ page }) => {
    environmentClean();
  });
  test.afterEach(async ({ page }) => {
    environmentClean();
  });

  test(`Accès à la page d'accueil`, async ({ page }) => {
    await page.goto("/maintenance");
    await expect(page).toHaveURL(/\//);
  });
});
