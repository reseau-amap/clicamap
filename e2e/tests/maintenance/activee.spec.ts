import { test, expect, Page } from "@playwright/test";
import {
  environmentClean,
  environmentSet,
} from "../../support/shorcut_environment";

test.describe("En tant qu'utlisateur, je doit etre redirigé sur la page de maintenance si celle-ci est activée", () => {
  test.beforeEach(async ({ page }) => {
    environmentClean();
  });
  test.afterEach(async ({ page }) => {
    environmentClean();
  });

  test(`Accès à la page d'accueil`, async ({ page }) => {
    environmentSet("MAINTENANCE_ENABLED", "true");
    await page.goto("/");
    await expect(page).toHaveURL(/\/maintenance/);
    await expect(page.locator("h3")).toContainText("Maintenance");
    await expect(page.locator(".container > .row .well")).toContainText(
      "Clic'AMAP est en cours de maintenance. Merci de réessayer dans quelques minutes."
    );
  });
});
