import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Affichage de l'AMAP associée dans la liste des amapiens", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const testErrorsDb = [
    [
      " ",
      "prenom amapien",
      "amapien@test.amap-aura.org",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "nom amapien",
      " ",
      "amapien@test.amap-aura.org",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "nom amapien",
      "prenom amapien",
      " ",
      "69001, LYON 1er Arrondissement",
      "Cette valeur ne doit pas être vide.",
    ],

    [
      "nom amapien",
      "prenom amapien",
      "amapien@test.amap-aura.org",
      "69001",
      'Le champ "Code postal, Ville" est mal formaté.',
    ],
  ];

  const testErrorsRow = async (page: Page, row) => {
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Modifier le profil de l'amapien"
    );
    await page
      .locator('input[name="user_amapien_amap[user][name][firstName]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][name][firstName]"]')
      .fill(row[0]);
    await page
      .locator('input[name="user_amapien_amap[user][name][lastName]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][name][lastName]"]')
      .fill(row[1]);
    await page
      .locator('input[name="user_amapien_amap[user][emails][0][email]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][emails][0][email]"]')
      .fill(row[2]);
    await page.locator('input[name="user_amapien_amap[user][ville]"]').clear();
    await page
      .locator('input[name="user_amapien_amap[user][ville]"]')
      .fill(row[3]);
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toHaveText(row[4]);
  };
  testErrorsDb.forEach((row, i) => {
    test(`Tester les messages d'erreurs avec le profil super-admin ${i}`, async ({
      page,
    }) => {
      await login(page, "admin");
      await page.goto("/amapien/search_admin");
      await page
        .locator('[name="search_amapien[region]"]')
        .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
      await testErrorsRow(page, row);
    });
  });
  testErrorsDb.forEach((row, i) => {
    test(`Tester les messages d'erreurs avec le profil admin aura ${i}`, async ({
      page,
    }) => {
      await login(page, "admin_aura");
      await page.goto("/amapien/search_admin");
      await testErrorsRow(page, row);
    });
  });
  testErrorsDb.forEach((row, i) => {
    test(`Tester les messages d'erreurs avec le profil admin rhone ${i}`, async ({
      page,
    }) => {
      await login(page, "admin_rhone");
      await page.goto("/amapien/search_admin");
      await testErrorsRow(page, row);
    });
  });
  testErrorsDb.forEach((row, i) => {
    test(`Tester les messages d'erreurs avec le profil amap ${i}`, async ({
      page,
    }) => {
      await login(page, "amap");
      await page.goto("/amapien/search_amap");
      await testErrorsRow(page, row);
    });
  });

  const testSuccess = async (page: Page) => {
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Modifier le profil de l'amapien"
    );
    await page
      .locator('input[name="user_amapien_amap[user][name][firstName]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][name][firstName]"]')
      .fill("nom amapien");
    await page
      .locator('input[name="user_amapien_amap[user][name][lastName]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][name][lastName]"]')
      .fill("prenom amapien");
    await page
      .locator('input[name="user_amapien_amap[user][emails][0][email]"]')
      .clear();
    await page
      .locator('input[name="user_amapien_amap[user][emails][0][email]"]')
      .fill("amapien@test.amap-aura.org");
    await page.locator('input[name="user_amapien_amap[user][ville]"]').clear();
    await page
      .locator('input[name="user_amapien_amap[user][ville]"]')
      .fill("69001, LYON 1er Arrondissement");
    await page
      .locator('input[name="user_amapien_amap[user][newsletter]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "L'amapien a été mis à jour avec succès."
    );
  };

  test(`Tester l'édition du profil d'un amapien admin`, async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amapien/search_admin");
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await testSuccess(page);
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`Tester l'édition du profil d'un amapien ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await page.goto("/amapien/search_admin");
      await testSuccess(page);
    });
  });
});
