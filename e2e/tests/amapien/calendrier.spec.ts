import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { dateClean } from "../../support/shorcut_date";
import {
  subscribeContract,
  subscribeContractAmapienToValidate,
} from "../../support/shorcut";

test.describe("Calendrier de livraison de l'AMAPien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Affichage du détail de livraison par date", async ({ page }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });

    await login(page, "amapien");
    await page.goto("/amapien/calendrier/1/2030/01");

    await expect(page.locator(".calendrier .btn-primary")).toHaveCount(5);
    await page
      .locator(".calendrier .btn-primary")
      .getByText(/1/)
      .first()
      .click();

    await expect(page.locator(".modal.in .well h3")).toHaveText(/Contrat 1/);
    await expect(page.locator(".modal.in .well h4").first()).toHaveText(
      /Livraisons 1 \/ 18/
    );
    await expect(page.locator(".modal.in .well h4").nth(1)).toHaveText(
      /Lieu : Ll amap test - 58 Rue Raulin, 69007 Lyon 7e Arrondissement/
    );
    await expect(page.locator(".modal.in .well ul li:nth-child(1)")).toHaveText(
      "1 Produit 1 (cond1)"
    );
    await expect(page.locator(".modal.in .well ul li:nth-child(2)")).toHaveText(
      "1 Produit 2 (cond2)"
    );
  });

  test("Masquer les contrats en cours de validation", async ({ page }) => {
    await subscribeContractAmapienToValidate(
      page,
      "amap",
      "Prenom amapien NOM AMAPIEN - amapien@test.amap-aura.org",
      "contrat 1",
      {
        "1_1": 1,
        "1_2": 1,
        "2_1": 1,
        "2_2": 1,
        "3_1": 1,
        "3_2": 1,
        "4_1": 1,
        "4_2": 1,
        "5_1": 1,
        "5_2": 1,
      }
    );
    await login(page, "amapien");
    await page.goto("/amapien/calendrier/1/2030/01");

    await expect(page.locator(".calendrier .btn-primary")).toHaveCount(0);
  });
});
