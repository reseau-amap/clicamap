import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Vérification identifiants à la création d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Identifiant inexistant ", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amapien/search_admin");
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await page
      .locator('[name="search_amapien[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .getByText(/Créer un nouvel amapien/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).not.toBeVisible();
    await page.locator('[name="user_verification[user]"]').fill("invalid");
    await page
      .getByText(/Vérifier/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Aucun amapien n'a été trouvé avec cet email ou cet identifiant"
    );
  });

  test("Amapien existant dans l'amap", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amapien/search_admin");
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await page
      .locator('[name="search_amapien[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .getByText(/Créer un nouvel amapien/)
      .first()
      .click();
    await page.locator('[name="user_verification[user]"]').fill("amapien");
    await page
      .getByText(/Vérifier/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "L'amapien est déjà présent dans votre AMAP."
    );
  });

  test("Amapien existant pas dans l'amap", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/amapien/search_admin");
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await page
      .locator('[name="search_amapien[amap]"]')
      .selectOption({ label: "amap test" });
    await page
      .getByText(/Créer un nouvel amapien/)
      .first()
      .click();
    await page.locator('[name="user_verification[user]"]').fill("paysan");
    await page
      .getByText(/Vérifier/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "L'amapien existe déjà sur clic'amap : un email lui a été envoyé pour qu'il rejoigne votre AMAP"
    );
    await expectMailShoudBeReceived(
      "amap test souhaite vous ajouter à ses amapiens sur clic'AMAP",
      ["paysan@test.amap-aura.org"]
    );
  });
});
