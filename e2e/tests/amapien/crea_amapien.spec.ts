import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMainMenuLink } from "../../support/shorcut";

const gotoForm = async (page: Page) => {
  await login(page, "amap");
  await page.goto("/amapien/search_amap");
  await page.getByRole("link", { name: "Créer un nouvel amapien" }).click();
  await page.locator('[name="user_verification[user]"]').fill("invalid");
  await page
    .getByText(/Vérifier/)
    .first()
    .click();
  await page
    .getByText(/cliquez ici/)
    .first()
    .click();
};

const fillFormValid = async (page: Page) => {
  await page
    .locator('[name="user_amapien_amap_creation[user][username]"]')
    .fill("username");
  await page
    .locator('[name="user_amapien_amap_creation[user][name][firstName]"]')
    .fill("firstname");
  await page
    .locator('[name="user_amapien_amap_creation[user][name][lastName]"]')
    .fill("lastName");
  await page
    .locator('[name="user_amapien_amap_creation[user][ville]"]')
    .fill("69001, Lyon 1er Arrondissement");
};

test.describe("Création d'un AMAPien depuis différents profils", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Créer un amapien valide", async ({ page }) => {
    await gotoForm(page);

    await fillFormValid(page);
    await page
      .locator('[name="user_amapien_amap_creation[needWaiting]"]')
      .locator('input[value="0"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'amapien a été créé avec succès.");
    await expect(page.locator(".container table")).toContainText("LASTNAME");
  });

  test("Vérification du retour via le fil d'ariane en tant qu'AMAP", async ({
    page,
  }) => {
    await login(page, "amap");
    await page.goto("/amapien/search_amap");
    await page.getByRole("link", { name: "Créer un nouvel amapien" }).click();
    await page.locator("h5 a:first-child").click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });

  test("Vérification du retour via le fil d'ariane en tant qu'admin", async ({
    page,
  }) => {
    await login(page, "admin");
    await page.goto("/amapien/search_admin");
    await page.goto("/amapien/creation_verification/1");
    await page.locator("h5 a:first-child").click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });

  test("Cocher par défaut la coche 'newsletter'", async ({ page }) => {
    await gotoForm(page);
    await expect(
      page.locator('[name="user_amapien_amap_creation[user][newsletter]"]')
    ).toBeChecked();
  });

  test("Créer un amapien valide redirection", async ({ page }) => {
    await gotoForm(page);

    await fillFormValid(page);
    await page
      .locator('[name="user_amapien_amap_creation[needWaiting]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "L'amapien a été créé avec succès.");
    await expect(page.locator("h3")).toContainText("Liste d'attente");
  });

  test("Test de la validation", async ({ page }) => {
    await gotoForm(page);
    await page
      .locator('[name="user_amapien_amap_creation[needWaiting]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger").nth(0)).toContainText(
      "Cette valeur ne doit pas être vide."
    );
    await expect(page.locator(".alert-danger").nth(1)).toContainText(
      "Cette valeur ne doit pas être vide."
    );
    await expect(page.locator(".alert-danger").nth(2)).toContainText(
      "Cette valeur ne doit pas être vide."
    );
    await expect(page.locator(".alert-danger").nth(3)).toContainText(
      "Cette valeur ne doit pas être vide."
    );
  });

  test("Test de la génération du nom utilisateur", async ({ page }) => {
    await gotoForm(page);
    await page
      .locator('[name="user_amapien_amap_creation[user][name][firstName]"]')
      .fill("test prénom");
    await page
      .locator('[name="user_amapien_amap_creation[user][name][lastName]"]')
      .fill("test nom");
    await expect(
      page.locator('[name="user_amapien_amap_creation[user][username]"]')
    ).toHaveValue("test-prenom.test-nom");
  });

  test("Test désactivation génération du nom utilisateur si fourni par utilisateur", async ({
    page,
  }) => {
    await gotoForm(page);
    await page
      .locator('[name="user_amapien_amap_creation[user][username]"]')
      .fill("test username");
    await page
      .locator('[name="user_amapien_amap_creation[user][name][firstName]"]')
      .fill("test prénom");
    await page
      .locator('[name="user_amapien_amap_creation[user][name][lastName]"]')
      .fill("test nom");
    await expect(
      page.locator('[name="user_amapien_amap_creation[user][username]"]')
    ).toHaveValue("test username");
  });
});
