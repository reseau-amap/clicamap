import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { desactiverAmapien } from "../../support/shorcut";

test.describe("Envoyer un mot de passe à un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Envoyer un mot de passe à un amapien amap", async ({ page }) => {
    await desactiverAmapien(page, "Prenom amapien");
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Envoyer un mot de passe à l'amapien"
    );
    await expect(page.locator(".modal.in")).toContainText(
      "Voulez-vous vraiment envoyer un mot de passe à l'amapien(ne) Prenom amapien NOM AMAPIEN ?\n"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le mot de passe a été envoyé avec succès."
    );
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapien@test.amap-aura.org",
    ]);
  });
});
