import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { subscribeContract } from "../../support/shorcut";

test.describe("Suppression d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Suppression d'un amapien depuis le profil amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Supprimer l'amapien"
    );
    await expect(page.locator(".modal.in")).toHaveText(
      /Voulez-vous vraiment supprimer l'amapien\(ne\) Prenom amapien NOM AMAPIEN \?/
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "L'amapien a été supprimé avec succès"
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /amapien@test\.amap-aura\.org/
    );
    await login(page, "amapien");
    await expect(page.locator(".alert-success")).toHaveText(
      /Pour l'instant, aucun droits spécifiques ne vous a été attribué\. En attendant, vous pouvez vérifier \/ modifier les informations de votre profil\./
    );
  });

  test("Suppression impossible si l'amapien a encore des livraisons prévues", async ({
    page,
  }) => {
    await subscribeContract(page, "amapien", "contrat 1", {
      "1_1": 1,
      "1_2": 1,
      "2_1": 1,
      "2_2": 1,
      "3_1": 1,
      "3_2": 1,
      "4_1": 1,
      "4_2": 1,
      "5_1": 1,
      "5_2": 1,
    });
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Supprimer l'amapien"
    );
    await expect(page.locator(".modal.in")).toHaveText(
      /Voulez-vous vraiment supprimer l'amapien\(ne\) Prenom amapien NOM AMAPIEN \?/
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Impossible de supprimer un amapien qui a encore des livraisons prévues"
    );
  });
});
