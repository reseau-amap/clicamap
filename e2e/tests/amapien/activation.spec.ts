import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  expectMailCount,
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";
import { goViaMenu } from "../../support/shorcut";
import { clickTableSelectCheckbox } from "../../support/shortcut_tableselect";

const checkAmapienActivated = async (page: Page, amapien: string) => {
  const amapienLine = page.locator("tr").filter({
    has: page.locator("td").getByText(amapien).first(),
  });
  await amapienLine.locator(".dropdown-toggle").click();
  await expect(amapienLine.locator(".dropdown-menu")).toHaveText(
    /Désactiver le compte de l'amapien/
  );
  await login(page, amapien);
  await expect(page.locator("nav")).toHaveText(/Mes contrats/);
  await expect(page.locator("nav")).toHaveText(/Mon agenda/);
};

const desactiverAmapien = async (page: Page, amapien: string) => {
  await clickMenuLinkOnLine(page, amapien, "Désactiver le compte de l'amapien");
  await flashMessageShouldContain(
    page,
    "L'état de l'amapien a été modifié avec succès."
  );
};

test.describe("Activation d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Activation d'un amapien depuis le profil amap", async ({ page }) => {
    await login(page, "amap");
    await goViaMenu(page, ["Gestionnaire AMAP", "Gestion des amapiens"]);
    await desactiverAmapien(page, "amapien@test.amap-aura.org");

    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Activer le compte de l'amapien"
    );

    await expectMailCount(1);
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapien@test.amap-aura.org",
    ]);
    await checkAmapienActivated(page, "amapien@test.amap-aura.org");
  });

  test("Activation de plusieurs amapiens depuis le profil amap - status du bouton", async ({
    page,
  }) => {
    await login(page, "amap");
    await goViaMenu(page, ["Gestionnaire AMAP", "Gestion des amapiens"]);

    const button = await page.locator("a").getByText("Activer la sélection");
    await expect(button).toHaveAttribute("disabled");
    await clickTableSelectCheckbox(page, "amapien@test.amap-aura.org");
    await expect(button).not.toHaveAttribute("disabled");
    await clickTableSelectCheckbox(page, "amapien@test.amap-aura.org");
    await expect(button).toHaveAttribute("disabled");
  });

  test("Activation de plusieurs amapiens depuis le profil amap - un seul selectionne", async ({
    page,
  }) => {
    await login(page, "amap");
    await goViaMenu(page, ["Gestionnaire AMAP", "Gestion des amapiens"]);
    await desactiverAmapien(page, "amapien@test.amap-aura.org");

    await clickTableSelectCheckbox(page, "amapien@test.amap-aura.org");

    await page.locator("a").getByText("Activer la sélection").click();
    await page.locator(".modal.in").getByText("OUI").click();

    await flashMessageShouldContain(
      page,
      "1 amapien(s) sélectionné(s) activé(s)."
    );

    await expectMailCount(1);
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapien@test.amap-aura.org",
    ]);
    await checkAmapienActivated(page, "amapien@test.amap-aura.org");
  });

  test("Activation de plusieurs amapiens depuis le profil amap - plusieurs selections", async ({
    page,
  }) => {
    await login(page, "amap");
    await goViaMenu(page, ["Gestionnaire AMAP", "Gestion des amapiens"]);
    await desactiverAmapien(page, "amapien@test.amap-aura.org");
    await desactiverAmapien(page, "amapienref@test.amap-aura.org");

    await page.locator(".js-tableselect-all").click();

    await page.locator("a").getByText("Activer la sélection").click();
    await page.locator(".modal.in").getByText("OUI").click();

    await flashMessageShouldContain(
      page,
      "2 amapien(s) sélectionné(s) activé(s)."
    );

    await expectMailCount(2);
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapien@test.amap-aura.org",
    ]);
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapienref@test.amap-aura.org",
    ]);
    await checkAmapienActivated(page, "amapienref@test.amap-aura.org");
  });

  test("Activation de plusieurs amapiens depuis le profil amap - filtre inactifs", async ({
    page,
  }) => {
    await login(page, "amap");
    await goViaMenu(page, ["Gestionnaire AMAP", "Gestion des amapiens"]);
    await desactiverAmapien(page, "amapienref@test.amap-aura.org");

    await page.locator(".js-tableselect-all").click();

    await page.locator("a").getByText("Activer la sélection").click();
    await page.locator(".modal.in").getByText("OUI").click();

    await flashMessageShouldContain(
      page,
      "1 amapien(s) sélectionné(s) activé(s)."
    );

    await expectMailCount(1);
    await expectMailShoudBeReceived("Bienvenue sur Clic'AMAP", [
      "amapienref@test.amap-aura.org",
    ]);
    await checkAmapienActivated(page, "amapienref@test.amap-aura.org");
  });
});
