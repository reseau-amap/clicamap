import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { supprimerAmapien } from "../../support/shorcut";

const goToPage = async (page: Page) => {
  await login(page, "amap");
  await page
    .getByText(/Gestionnaire AMAP/)
    .first()
    .click();
  await page
    .getByText(/Gestion de mon AMAP/)
    .first()
    .click();
  await clickMenuLinkOnLine(
    page,
    "amap@test.amap-aura.org",
    "Modifier les rôles de l'AMAP"
  );
  await page
    .locator(".modal.in")
    .getByText("Le(s) correspondant(s) Clic'AMAP")
    .first()
    .click();
};

test.describe("Gestion du correspondant de l'amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Ajout d'un correspondant", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le correspondant Clic'AMAP a été ajouté avec succès."
    );
    await expect(page.locator("button.btn-primary")).toHaveText(
      /Prenom amapien NOM AMAPIEN/
    );
  });

  test("Suppression d'un correspondant", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le correspondant Clic'AMAP a été ajouté avec succès."
    );
    await page
      .locator("button.btn-primary")
      .getByText(/Prenom amapien NOM AMAPIEN/)
      .first()
      .click();
    await expect(page.locator(".modal.in")).toHaveText(
      /Voulez-vous vraiment retirer le rôle de correspondant Clic'AMAP à "Prenom amapien NOM AMAPIEN" \?/
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(
      page,
      "Le correspondant Clic'AMAP a été supprimé avec succès."
    );
    await expect(page.locator("button.btn-primary")).not.toBeVisible();
  });

  test("Ajout impossible d'un amapien supprimé", async ({ page }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPage(page);
    await expect(
      page
        .locator('[name="amap_referent[referent]"]')
        .getByText("NOM AMAPIEN Prenom amapien", { exact: true })
    ).toHaveCount(0);
  });

  test("Disparition amapien supprimé", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le correspondant Clic'AMAP a été ajouté avec succès."
    );

    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPage(page);
    await expect(page.locator("button.btn-primary")).not.toBeVisible();
  });
});
