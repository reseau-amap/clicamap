import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login, logout } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMainMenuLink, supprimerAmapien } from "../../support/shorcut";
import { mailClean, mailClickLink } from "../../support/shorcut_mail";
import { dateClean, dateSet } from "../../support/shorcut_date";

const createToken = async (page: Page, amapien: string) => {
  await login(page, "amap");
  await page.goto("/amapien/search_amap");
  await page.getByRole("link", { name: "Créer un nouvel amapien" }).click();
  await page.locator('[name="user_verification[user]"]').fill(amapien);
  await page
    .getByText(/Vérifier/)
    .first()
    .click();
  await flashMessageShouldContain(
    page,
    "L'amapien existe déjà sur clic'amap : un email lui a été envoyé pour qu'il rejoigne votre AMAP"
  );
  await logout(page);
};

test.describe("Activation d'une invitation comme amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Token invalide", async ({ page }) => {
    await page.goto("/amapien/verification_validation/invalid_token");
    await flashMessageShouldContain(
      page,
      "Le lien de validation est invalide ou expiré."
    );
    await expect(page).toHaveURL("http://localhost/portail/connexion");
  });

  test("Token expiré", async ({ page }) => {
    await createToken(page, "amapien2");
    dateSet("2030-01-01");
    await mailClickLink(page, 0);
    await flashMessageShouldContain(
      page,
      "Le lien de validation est invalide ou expiré."
    );
    await expect(page).toHaveURL("http://localhost/portail/connexion");
  });

  test("Token valide", async ({ page }) => {
    await createToken(page, "amapien2");
    await mailClickLink(page, 0);
    await flashMessageShouldContain(
      page,
      "Vous avez bien été ajouté à l'AMAP amap test."
    );
    await expect(page).toHaveURL("http://localhost/portail/connexion");
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await expect(page.locator(".container table")).toContainText(
      "NOM AMAPIEN 2"
    );
  });

  test("Token valide - amapien supprimé", async ({ page }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");
    await createToken(page, "amapien");
    await mailClickLink(page, 0);
    await flashMessageShouldContain(
      page,
      "Vous avez bien été ajouté à l'AMAP amap test."
    );
    await expect(page).toHaveURL("http://localhost/portail/connexion");
  });

  test("Token valide - amapien existant", async ({ page }) => {
    await createToken(page, "amapien2");
    await createToken(page, "amapien2");
    await mailClickLink(page, 0);
    await flashMessageShouldContain(
      page,
      "Vous avez bien été ajouté à l'AMAP amap test."
    );
    await mailClickLink(page, 1);
    await flashMessageShouldContain(
      page,
      "Le lien de validation est invalide ou expiré."
    );
    await expect(page).toHaveURL("http://localhost/portail/connexion");
  });
});
