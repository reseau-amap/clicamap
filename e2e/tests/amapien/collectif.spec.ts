import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { supprimerAmapien } from "../../support/shorcut";

const goToPage = async (page: Page) => {
  await login(page, "amap");
  await page.getByText("Gestionnaire AMAP").first().click();
  await page.getByText("Gestion de mon AMAP").first().click();
  await clickMenuLinkOnLine(
    page,
    "amap@test.amap-aura.org",
    "Modifier les rôles de l'AMAP"
  );
  await page.locator(".modal.in").getByText("Le collectif de l'AMAP").click();
};

test.describe("Gestion du collectif de l'amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Message par défaut sans collectif", async ({ page }) => {
    await goToPage(page);
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun rôle n'a encore été défini au sein du collectif de l'AMAP\./
    );
  });

  test("Ajout d'un du président", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[precision]"]')
      .fill("Test de précision");
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");
    await expect(page.locator(".container table")).toHaveText(/Président/);
    await expect(page.locator(".container table")).toHaveText(
      /Test de précision/
    );
    await expect(page.locator(".container table")).toHaveText(
      /amapien@test\.amap-aura\.org/
    );
  });

  test("Erreur ajout multiple même role même amapien", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page.getByText("Valider").first().click();
    await expect(page.locator(".alert-danger")).toHaveText(
      /Le rôle est déjà pris dans l'AMAP/
    );
    await expect(page.locator(".container table")).not.toHaveText(
      /amapienref@test\.amap-aura\.org/
    );
  });

  test("Erreur ajout multiple même role plusieurs amaps", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[precision]"]')
      .fill("Test de précision");
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");
    await login(page, "amap2");
    await page.getByText("Gestionnaire AMAP").first().click();
    await page.getByText("Gestion de mon AMAP").first().click();
    await clickMenuLinkOnLine(
      page,
      "amap2@test.amap-aura.org",
      "Modifier les rôles de l'AMAP"
    );
    await page
      .locator(".modal.in")
      .getByText("Le collectif de l'AMAP")
      .first()
      .click();
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[precision]"]')
      .fill("Test de précision");
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien 2 NOM AMAPIEN 2" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");
    await expect(page.locator(".container table")).toHaveText(
      /amapien2@test\.amap-aura\.org/
    );
  });

  test("Ajout impossible d'un amapien supprimé", async ({ page }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPage(page);
    await expect(
      page
        .locator('[name="amap_collectif[amapien]"] option')
        .getByText("Prenom amapien NOM AMAPIEN", { exact: true })
    ).toHaveCount(0);
  });

  test("Suppression amapien avec un rôle", async ({ page }) => {
    await goToPage(page);
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[precision]"]')
      .fill("Test de précision");
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prenom amapien NOM AMAPIEN" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");

    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPage(page);
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun rôle n'a encore été défini au sein du collectif de l'AMAP\./
    );
    await page
      .locator('[name="amap_collectif[profil]"]')
      .selectOption({ label: "Président" });
    await page
      .locator('[name="amap_collectif[precision]"]')
      .fill("Test de précision");
    await page
      .locator('[name="amap_collectif[amapien]"]')
      .selectOption({ label: "Prénom amapien référent NOM AMAPIEN RÉFÉRENT" });
    await page.getByText("Valider").first().click();
    await flashMessageShouldContain(page, "Le rôle a été ajouté avec succès.");
  });
});
