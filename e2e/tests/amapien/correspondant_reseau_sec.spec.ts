import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import { supprimerAmapien } from "../../support/shorcut";

const goToPage = async (page: Page) => {
  await clickMenuLinkOnLine(
    page,
    "amap@test.amap-aura.org",
    "Modifier les rôles de l'AMAP"
  );
  await page
    .locator(".modal.in")
    .getByText("Le second correspondant réseau AMAP")
    .first()
    .click();
};

const goToPageWithLogin = async (page: Page) => {
  await login(page, "amap");
  await page
    .getByText(/Gestionnaire AMAP/)
    .first()
    .click();
  await page
    .getByText(/Gestion de mon AMAP/)
    .first()
    .click();

  await goToPage(page);
};

test.describe("Gestion du second correspondant réseau de l'amap", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Message par défaut sans correspondant", async ({ page }) => {
    await goToPageWithLogin(page);
    await expect(page.locator(".alert-warning")).toHaveText(
      /L'AMAP n'a pas encore de second correspondant réseau AMAP\./
    );
  });

  test("Ajout d'un correspondant", async ({ page }) => {
    await goToPageWithLogin(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent réseau secondaire a été mis à jour avec succès."
    );

    await goToPage(page);
    await expect(page.locator("a.btn-primary")).toHaveText(
      /Prenom amapien NOM AMAPIEN/
    );
  });

  test("Suppression d'un correspondant", async ({ page }) => {
    await goToPageWithLogin(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent réseau secondaire a été mis à jour avec succès."
    );

    await goToPage(page);
    await page
      .locator("a.btn-primary")
      .getByText(/Prenom amapien NOM AMAPIEN/)
      .first()
      .click();
    await expect(page.locator(".modal.in")).toHaveText(
      /Voulez-vous vraiment supprimer le rôle de correspondant réseau secondaire à "Prenom amapien NOM AMAPIEN" \?/
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /L'AMAP n'a pas encore de second correspondant réseau AMAP\./
    );
  });

  test("Ajout impossible d'un amapien supprimé", async ({ page }) => {
    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPageWithLogin(page);
    await expect(
      page
        .locator('[name="amap_referent[referent]"] option')
        .getByText("NOM AMAPIEN Prenom amapien")
    ).toHaveCount(0);
  });

  test("Disparition amapien supprimé", async ({ page }) => {
    await goToPageWithLogin(page);
    await page
      .locator('[name="amap_referent[referent]"]')
      .selectOption({ label: "NOM AMAPIEN Prenom amapien" });
    await page
      .getByText(/Éditer/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le référent réseau secondaire a été mis à jour avec succès."
    );

    await supprimerAmapien(page, "amapien@test.amap-aura.org");

    await goToPageWithLogin(page);
    await expect(page.locator("button.btn-primary")).not.toBeVisible();
    await expect(page.locator(".alert-warning")).toHaveText(
      /L'AMAP n'a pas encore de second correspondant réseau AMAP\./
    );
  });
});
