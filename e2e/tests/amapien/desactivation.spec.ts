import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";
import {
  expectMailCount,
  expectMailShoudBeReceived,
  mailClean,
} from "../../support/shorcut_mail";

test.describe("Désactication d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
    await mailClean();
  });

  test("Désactivation d'un amapien depuis le profil amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Désactiver le compte de l'amapien"
    );
    await flashMessageShouldContain(
      page,
      "L'état de l'amapien a été modifié avec succès."
    );

    await expectMailCount(0);
    const amapienLine = page.locator("tr").filter({
      has: page
        .locator("td")
        .getByText(/amapien@test\.amap-aura\.org/)
        .first(),
    });
    await amapienLine.locator(".dropdown-toggle").click();
    await expect(amapienLine.locator(".dropdown-menu")).toHaveText(
      /Activer le compte de l'amapien/
    );
    await login(page, "amapien");
    await expect(page.locator("nav")).not.toHaveText(/Mes contrats/);
    await expect(page.locator("nav")).not.toHaveText(/Mon agenda/);
  });
});
