import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMainMenuLink } from "../../support/shorcut";

test.describe("Affichage de l'AMAP associée dans la liste des amapiens", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test(`En tant que super admin, je doit voir un lien avec l'amap associée dans la liste des amapiens`, async ({
    page,
  }) => {
    await login(page, "admin");
    await clickMainMenuLink(page, "Gestionnaire Admin");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await expect(page.locator(".container table thead")).toContainText("AMAP");
    await page
      .locator("table tbody tr:nth-child(1)")
      .getByRole("link", { name: "amap test" })
      .click();
    await expect(page.locator("h3").first()).toContainText("Détails de l'AMAP");
  });

  ["admin_aura", "admin_rhone"].forEach((user) => {
    test(`En tant qu'admin, je doit voir un lien avec l'amap associée dans la liste des amapiens ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await clickMainMenuLink(page, "Gestionnaire Admin");
      await clickMainMenuLink(page, "Gestion des amapiens");
      await expect(page.locator(".container table thead")).toContainText(
        "AMAP"
      );
      await page
        .locator("table tbody tr:nth-child(1)")
        .getByRole("link", { name: "amap test" })
        .click();
      await expect(page.locator("h3").first()).toContainText(
        "Détails de l'AMAP"
      );
    });
  });

  test(`En tant qu'amap, je ne doit pas voir un lien avec l'amap associée dans la liste des amapiens`, async ({
    page,
  }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des amapiens");
    await expect(page.locator('[name="search_amapien_amap[amap]"]')).toHaveText(
      "amap test"
    );
    await expect(
      page.locator('[name="search_amapien_amap[amap]"]')
    ).toHaveAttribute("disabled", /.*/);
    await expect(page.locator(".container table")).not.toContainText(
      "amap@test.amap-aura.org"
    );
  });
});
