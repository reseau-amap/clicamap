import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Afficher le profil d'un amapien", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Afficher le profil d'un amapien en admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await expect(page.locator(".container table")).toContainText(
      "amapien@test.amap-aura.org"
    );
    await expect(page.locator(".container table")).toContainText(
      "Adresse, 69001 LYON 1ER ARRONDISSEMENT"
    );
    await expect(page.locator(".container table")).toContainText(
      "Nom d'utilisateur"
    );
    await expect(page.locator(".container table")).toContainText("amapien");
  });

  test("Retour depuis le profil de l'amapien en admin", async ({ page }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });

  test("Retour depuis le profil de l'amapien en amap", async ({ page }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await page
      .getByText(/Retour/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });

  test("Vérification du retour via le fil d'ariane en tant qu'AMAP", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await page
      .locator("h5 a")
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });

  test("Vérification du retour via le fil d'ariane en tant qu'admin", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Gestionnaire Admin/)
      .first()
      .click();
    await page
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await page
      .locator('[name="search_amapien[region]"]')
      .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
    await clickMenuLinkOnLine(
      page,
      "amapien@test.amap-aura.org",
      "Profil de l'amapien"
    );
    await page
      .locator("h5 a")
      .getByText(/Gestion des amapiens/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Liste des amapiens");
  });
});
