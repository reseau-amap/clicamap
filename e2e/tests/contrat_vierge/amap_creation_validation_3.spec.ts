import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { getSelectedOption } from "../../support/shorcut_actions";

import {
  addProduit,
  associerAmapienReferentFerme,
  gotoContractStep3,
  setProduitRegul,
} from "../../support/shorcut";
import { dateClean } from "../../support/shorcut_date";

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await dateClean();
  });

  test("Validation étape 3 - aucun produit dans la ferme", async ({ page }) => {
    await associerAmapienReferentFerme(
      page,
      "amap",
      "Prénom amapien référent NOM AMAPIEN RÉFÉRENT",
      "Ferme 2"
    );
    await login(page, "amap");
    await gotoContractStep3(page, "Ferme 2");
    await expect(page.locator(".alert-warning")).toContainText(
      "La ferme n'a aucun produit."
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toContainText(
      "La ferme n'a aucun produit."
    );
  });

  test("Validation étape 3 - multiples produits identiques", async ({
    page,
  }) => {
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page.locator("#btn-plus").click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Impossible d'indiquer plusieurs fois le même produit"
    );
    await expect(
      await getSelectedOption(
        page.locator(
          '[name="model_contrat_form3[produits][0][typeProduction]"]'
        )
      )
    ).toBe("Autres / divers");
    await expect(
      await getSelectedOption(
        page.locator('[name="model_contrat_form3[produits][0][fermeProduit]"]')
      )
    ).toBe("produit 1 / cond1");
    await expect(
      await getSelectedOption(
        page.locator(
          '[name="model_contrat_form3[produits][2][typeProduction]"]'
        )
      )
    ).toBe("Autres / divers");
    await expect(
      await getSelectedOption(
        page.locator('[name="model_contrat_form3[produits][2][fermeProduit]"]')
      )
    ).toBe("produit 1 / cond1");
  });

  test("Validation étape 3 - aucun produit", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page.locator(".btn-remove").first().click();
    await page.locator(".btn-remove").first().click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Merci de sélectionner au moins un produit"
    );
  });

  test("Validation étape 3 - prix négatif", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page
      .locator('input[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
      .fill("-1");
    await page.getByText(/Étape suivante/).click();
    await expect(
      page.locator(
        "#model_contrat_form3_produits .well:nth-child(1) .alert-danger"
      )
    ).toContainText("Cette valeur doit être supérieure ou égale à 0.\n");
  });

  test("Validation étape 3 - MAJ auto au changement de catégorie", async ({
    page,
  }) => {
    await setProduitRegul(page, "paysan", "Produit 2", true);
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page
      .locator(
        'select[name="model_contrat_form3[produits][0][typeProduction]"]'
      )
      .selectOption("Céréales et légumineuses");
    await expect(
      await getSelectedOption(
        page.locator('[name="model_contrat_form3[produits][0][fermeProduit]"]')
      )
    ).toBe("produit 1 / cond1");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
    ).toHaveValue("2.00");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][regulPds]"]')
    ).toBeChecked();

    await page
      .locator(
        'select[name="model_contrat_form3[produits][0][typeProduction]"]'
      )
      .selectOption("Autres / divers");
    await expect(
      await getSelectedOption(
        page.locator('[name="model_contrat_form3[produits][0][fermeProduit]"]')
      )
    ).toBe("produit 2 / cond1");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
    ).toHaveValue("1.00");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][regulPds]"]')
    ).not.toBeChecked();
  });

  test("Validation étape 3 - MAJ auto au changement de produit", async ({
    page,
  }) => {
    await addProduit(
      page,
      "paysan",
      "Autres / divers",
      "Produit 3",
      "cond",
      "3.00",
      true
    );
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page
      .locator('select[name="model_contrat_form3[produits][0][fermeProduit]"]')
      .selectOption("Produit 3 / cond");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
    ).toHaveValue("3.00");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][regulPds]"]')
    ).toBeChecked();

    await page
      .locator('select[name="model_contrat_form3[produits][0][fermeProduit]"]')
      .selectOption("produit 1 / cond1");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
    ).toHaveValue("1.00");
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][regulPds]"]')
    ).not.toBeChecked();
  });
});
