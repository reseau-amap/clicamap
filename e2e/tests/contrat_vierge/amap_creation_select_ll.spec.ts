import { test, expect, Page } from "@playwright/test";
import {
  requestAddLlToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractCreation } from "../../support/shorcut";
import { getSelectedOption } from "../../support/shorcut_actions";

test.describe("Comme amap, je doit pré selectionner automatiquement le lieu de livraison si il est unique dans l'AMAP", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Un seul lieu de livraison par AMAP", async ({ page }) => {
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    await expect(
      page.locator(
        '[name="model_contrat_form1[livraisonLieu]"] option[selected]'
      )
    ).toHaveText("ll amap test");
  });

  test("Plusieurs lieux de livraison par AMAP", async ({ page }) => {
    await requestAddLlToAmap(page, "ll amap test 2", "amap@test.amap-aura.org");
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    expect(
      await getSelectedOption(
        page.locator('[name="model_contrat_form1[livraisonLieu]"]')
      )
    ).toBe("Sélectionner");
  });
});
