import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import { creerCopieContrat } from "../../support/shorcut";

test.describe("Comme amap, je doit pouvoir valider un contrat copié", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Valider le contrat copié avec les valeur par défaut", async ({
    page,
  }) => {
    await creerCopieContrat(
      page,
      "contrat 1 (fin de souscription : 01/01/2030)"
    );
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats vierges/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await clickMenuLinkOnLine(
      page,
      "Copie de contrat 1",
      "Modifier le contrat"
    );
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await fillDatepicker(
      page,
      '[name="model_contrat_form2[forclusion]"]',
      new Date("2000-01-01")
    );
    await fillDatepicker(
      page,
      '[name="date_livraison_debut"]',
      new Date("2030-02-01")
    );
    await fillDatepicker(
      page,
      '[name="date_livraison_fin"]',
      new Date("2030-03-01")
    );
    await page
      .locator('[name="model_contrat_form2[delaiModifContrat]"]')
      .fill("2");
    await page
      .getByText(/Générer des dates/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click({
        timeout: 10000,
      });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .locator('[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("#btn-plus").click();
    await fillDatepicker(
      page,
      '[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );

    await page.locator("h3").click(); // Hide popup
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".container table")).toHaveText(
      /Copie de contrat 1/
    );
    await expect(page.locator(".container table")).toHaveText(/En création/);
  });
});
