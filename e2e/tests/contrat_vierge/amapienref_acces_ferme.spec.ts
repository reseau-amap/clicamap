import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  associerAmapienReferentFerme,
  createContract,
  deAssocierAmapienReferentFerme,
} from "../../support/shorcut";

const initDb = async (page: Page) => {
  await requestAddUserToAmap(page, "amapienref", "amap test 2");
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prénom amapien référent NOM AMAPIEN RÉFÉRENT",
    "Ferme 2"
  );
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prenom amapien 2 NOM AMAPIEN 2",
    "Ferme 2"
  );
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prenom amapien 2 NOM AMAPIEN 2",
    "Ferme"
  );
};

test.describe("Comme amapien référent, je ne doit pouvoir lister les contrats que des fermes pour lesquelles je suis référent", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Ferme accessible dans la liste des fermes", async ({ page }) => {
    await initDb(page);
    await login(page, "amapienref");
    await page.goto("/contrat_vierge/accueil_refprod");
    await page
      .locator('[name="search_contrat_vierge_amap[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("ferme", { exact: true })
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("Ferme 2", { exact: true })
    ).toHaveCount(0);
    await page
      .locator('[name="search_contrat_vierge_amap[amap]"]')
      .selectOption({ label: "amap test 2" });
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("ferme", { exact: true })
    ).toHaveCount(0);
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("Ferme 2", { exact: true })
    ).toHaveCount(1);
  });

  test("Contrat accessible sans ferme selectionnée", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await login(page, "amapienref");
    await page.goto("/contrat_vierge/accueil_refprod");
    await expect(page.locator(".container table")).toContainText("En création");

    await deAssocierAmapienReferentFerme(
      page,
      "amap",
      "Prénom amapien référent NOM AMAPIEN RÉFÉRENT",
      "Ferme"
    );
    await associerAmapienReferentFerme(
      page,
      "amap",
      "Prénom amapien référent NOM AMAPIEN RÉFÉRENT",
      "Ferme 2"
    );
    await login(page, "amapienref");
    await page.goto("/contrat_vierge/accueil_refprod");
    await expect(page.locator(".alert-warning")).toContainText(
      "Aucun contrat n'a encore été créé."
    );
  });
});
