import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  clickMainMenuLink,
  fermeAssocieeRegroupement,
  gotoContractStep6,
} from "../../support/shorcut";
import { fillDatepicker } from "../../support/shorcut_actions";
import { readPdf } from "../../support/shorcut_pdf";

test.describe("Comme amapien, lors de la création d'un contrat associé à une ferme Cdp je ne doit pas pouvoir choisir le mode de paiement", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Creation d'un contrat associé à un paysan non Cdp", async ({
    page,
  }) => {
    await login(page, "amap");

    await gotoContractStep6(page, "ferme");
    await expect(page.locator("form")).toContainText("Chèque");
    await expect(page.locator("form")).toContainText("Virement");
    await expect(page.locator("form")).toContainText("SEPA");
    await expect(page.locator("form")).toContainText("Carte bancaire");
  });

  test("Creation d'un contrat associé à un paysan Cdp", async ({ page }) => {
    await fermeAssocieeRegroupement(page, "ferme", "Regroupement Cdp");
    await login(page, "amap");
    await gotoContractStep6(page, "ferme");

    await expect(page.locator("form")).toContainText("Carte bancaire");
    await expect(page.locator("form")).not.toContainText("Chèque");
    await expect(page.locator("form")).not.toContainText("Virement");
    await expect(page.locator("form")).not.toContainText("SEPA");

    await expect(
      page.locator('[name="model_contrat_form6[reglementType][]"]')
    ).toBeChecked();
    await expect(
      page.locator('[name="model_contrat_form6[reglementType][]"]')
    ).toBeDisabled();

    await page
      .locator('[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("#btn-plus").click();
    await fillDatepicker(
      page,
      '[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );
    await page.locator("h3").click();
    await page
      .locator('[name="model_contrat_form6[reglementModalite]"]')
      .fill("modalite");
    await page.getByRole("button", { name: "Étape suivante" }).click();

    await page.getByRole("button", { name: "Valider" }).click();
    await page.locator("table tbody tr:nth-child(1) .dropdown-toggle").click();
    const pdfText = await readPdf(
      page,
      page
        .locator("table tbody tr:nth-child(1) .dropdown-menu-right")
        .getByText(/Prévisualiser le contrat/)
    );
    expect(pdfText).toContain(
      `Le règlement se fait à l'ordre de ferme 1 ordre cheque par Carte bancaire.`
    );
  });
});
