import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  checkCheckbox,
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import {
  clickMainMenuLink,
  createContract,
  disableProduct,
  gotoContractStep5,
} from "../../support/shorcut";

const expectProductDeleted = async (page: Page) => {
  await login(page, "amap");
  await clickMainMenuLink(page, "Gestionnaire AMAP");
  await clickMainMenuLink(page, "Gestion des contrats vierges");
  await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
  await flashMessageShouldContain(
    page,
    "Attention : les produits suivants ont été supprimés car ils n'existent plus dans la ferme : produit 1"
  );
  await page.locator('input[name="model_contrat_form1[nom]"]').fill("Nom 2");

  await page.getByText(/Étape suivante/).click();
  await page.getByText(/Étape suivante/).click();
  await expect(
    page.locator("#model_contrat_form3_produits > .well")
  ).toHaveCount(1);
  await expect(
    page.locator(
      '#model_contrat_form3_produits > .well select[name="model_contrat_form3[produits][0][fermeProduit]"]'
    )
  ).toHaveText("produit 2 / cond1");

  await page.getByText(/Étape suivante/).click();
  await page.getByText(/Étape suivante/).click();
  await expect(page.locator("form th.text-center")).toHaveCount(1);
  await expect(page.locator("form th.text-center")).toHaveText("produit 2");
  await page.getByText(/Étape suivante/).click();
  await expect(
    page.locator('[name="model_contrat_form6[reglementNbMax]"]')
  ).toHaveValue("1");
  await expect(
    page.locator('[name="model_contrat_form6[dateReglements][1][date]"]')
  ).toHaveValue("2030-01-10");
  await page.getByText(/Étape suivante/).click();
  await page.getByText(/Valider/).click();

  await expect(
    page.locator(".container table tbody tr:nth-child(1) th")
  ).toHaveText("Nom 2");
};

test.describe("Comme amap, je doit pouvoir modifier le contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Modification du contrat", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page.locator('[name="model_contrat_form1[nom]"]').clear();
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nouveau nom");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click({
        timeout: 10000,
      });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".container table")).toContainText("nouveau nom");
  });

  test("Modification si les produits de la ferme ont été supprimés", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Supprimer le produit");
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await flashMessageShouldContain(page, "Produit supprimé avec succès.");

    await expectProductDeleted(page);
  });

  test("Modification si les produits de la ferme ont été désactivés", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await disableProduct(page, "paysan", "Produit 1");

    await expectProductDeleted(page);
  });

  test("Conservation des exclusions si les produits sont identiques", async ({
    page,
  }) => {
    await login(page, "amap");

    await gotoContractStep5(page, "ferme");
    await page
      .locator('[name="model_contrat_form5[exclusions][2030-02-01_1]"]')
      .uncheck();

    await page.getByRole("button", { name: "Étape suivante" }).click();

    await page
      .locator('input[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("button#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );
    await checkCheckbox(
      page,
      page.locator('input[name="model_contrat_form6[reglementType][]"]'),
      "Chèque"
    );
    await page
      .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
      .fill("modalite");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Valider" }).click();

    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Étape suivante" }).click();

    await expect(
      page.locator('[name="model_contrat_form5[exclusions][2030-02-01_1]"]')
    ).not.toBeChecked();
  });
});
