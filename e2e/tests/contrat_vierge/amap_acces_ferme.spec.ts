import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  associerAmapienReferentFerme,
  setAmapienAdmin,
} from "../../support/shorcut";

const initDb = async (page: Page) => {
  await requestAddUserToAmap(page, "amapienref", "amap test 2");
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prénom amapien référent NOM AMAPIEN RÉFÉRENT",
    "Ferme 2"
  );
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prenom amapien 2 NOM AMAPIEN 2",
    "Ferme 2"
  );
  await associerAmapienReferentFerme(
    page,
    "amap2",
    "Prenom amapien 2 NOM AMAPIEN 2",
    "Ferme"
  );
  await setAmapienAdmin(page, "amap", "amap2@test.amap-aura.org");
};

test.describe("Comme amap, les fermes selectionnables doivent être cohérentes avec l'amap selectionnée", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Liste des contrats vierges comme referent", async ({ page }) => {
    await initDb(page);
    await login(page, "amap");
    await page.goto("/contrat_vierge/accueil_amapadmin");
    await page
      .locator('[name="search_contrat_vierge_amap[amap]"]')
      .selectOption({ label: "amap test" });
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("ferme", { exact: true })
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("Ferme 2", { exact: true })
    ).toHaveCount(0);
    await page
      .locator('[name="search_contrat_vierge_amap[amap]"]')
      .selectOption({ label: "amap test 2" });
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("ferme", { exact: true })
    ).toHaveCount(1);
    await expect(
      page
        .locator('[name="search_contrat_vierge_amap[ferme]"] option')
        .getByText("Ferme 2", { exact: true })
    ).toHaveCount(1);
  });
});
