import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  clickMainMenuLink,
  fermeAssocieeRegroupement,
} from "../../support/shorcut";

const gotoContratCreation = async (page: Page) => {
  await login(page, "admin");
  await clickMainMenuLink(page, "Gestionnaire Admin");
  await clickMainMenuLink(page, "Gestion des contrats vierges");
  await page
    .locator('[name="search_contrat_vierge[region]"]')
    .selectOption({ label: "AUVERGNE-RHÔNE-ALPES" });
  await page
    .locator('[name="search_contrat_vierge[amapFerme][amap]"]')
    .selectOption({ label: "amap test" });
  await page
    .locator('[name="search_contrat_vierge[amapFerme][ferme]"]')
    .selectOption({ label: "ferme" });

  await page.getByText("Créer un nouveau contrat").click();
  await page.getByText("A partir de zéro").click();
  await page.waitForLoadState();
};
test.describe("Comme amapien, lors de la création d'un contrat associé à une ferme Cdp je doit voir un message d'avertissement", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Creation d'un contrat associé à un paysan non Cdp", async ({
    page,
  }) => {
    await gotoContratCreation(page);
    await expect(page.locator("form p.text-danger")).toBeHidden();
  });

  test("Creation d'un contrat associé à un paysan Cdp", async ({ page }) => {
    await fermeAssocieeRegroupement(page, "ferme", "Regroupement Cdp");
    await gotoContratCreation(page);

    await expect(page.locator("form p.text-danger")).toContainText(
      'Ce contrat est associée à une ferme "Les Champs des Possibles", aussi une interface de paiement sera présentée à l\'amapien au moment de la souscription du contrat.'
    );
  });
});
