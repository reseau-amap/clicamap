import { test, expect, Locator, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractStep4 } from "../../support/shorcut";
import { dateClean } from "../../support/shorcut_date";
import { waitForHTMXRequestEnd } from "../../support/shorcut_htmx";
import { checkRadio } from "../../support/shorcut_actions";

const expectAlertDangerCheckbox = async (
  page,
  locator: Locator,
  content: string
) => {
  await expect(
    page
      .locator(".form-group.row")
      .filter({
        has: locator,
      })
      .locator("+ .alert-danger")
  ).toHaveText(content);
};

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await dateClean();
  });

  test("Validation étape 4 - Validation par défaut", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(page.locator(".alert-danger")).toHaveCount(4);
    await expectAlertDangerCheckbox(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Merci de sélectionner une option"
    );
    await expectAlertDangerCheckbox(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiquePaysan]"]'),
      "Merci de sélectionner une option"
    );
    await expectAlertDangerCheckbox(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiqueAmapien]"]'),
      "Merci de sélectionner une option"
    );
    await expect(
      page.locator(
        '[name="model_contrat_form4[nblivPlancher]"] + .alert-danger'
      )
    ).toHaveText("Merci d'indiquer une valeur");
  });

  test("Validation étape 4 - Affichage dynamique modalités de report", async ({
    page,
  }) => {
    const expectDeliveryDisabled = async (page: Page) => {
      await waitForHTMXRequestEnd(page.locator("form"));
      await expect(
        page.locator(
          '[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
        )
      ).toBeHidden();
      await expect(
        page.locator(
          '[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
        )
      ).toBeHidden();
    };
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await expectDeliveryDisabled(page);
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Non"
    );
    await expectDeliveryDisabled(page);

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Oui"
    );
    await waitForHTMXRequestEnd(page.locator("form"));
    await expect(
      page
        .locator(
          '[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
        )
        .first()
    ).toBeVisible();
    await expect(
      page
        .locator(
          '[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
        )
        .first()
    ).toBeVisible();
  });

  test("Validation étape 4 - Affichage dynamique nb produits maximum", async ({
    page,
  }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Oui"
    );
    await waitForHTMXRequestEnd(page.locator("form"));
  });

  test("Validation étape 4 - Livraison plancher min", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await page
      .locator('[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("0");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(
      page.locator(
        '[name="model_contrat_form4[nblivPlancher]"] + .alert-danger'
      )
    ).toHaveText("Cette valeur doit être comprise entre 1 et 5.");
  });

  test("Validation étape 4 - Livraison plancher max", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await page
      .locator('[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("6");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(
      page.locator(
        '[name="model_contrat_form4[nblivPlancher]"] + .alert-danger'
      )
    ).toHaveText("Cette valeur doit être comprise entre 1 et 5.");
  });

  test("Validation étape 4 - Contrat valide sans report par défaut", async ({
    page,
  }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiquePaysan]"]'),
      "Oui"
    );
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiqueAmapien]"]'),
      "Oui"
    );
    await waitForHTMXRequestEnd(page.locator("form"));
    await page
      .locator('[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("1");
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[nblivPlancherDepassement]"]'),
      "Non"
    );
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Non"
    );

    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(page.locator("h3")).toHaveText(
      "nom : l'échéancier des règlements"
    );
  });

  test("Validation étape 4 - Champ obligatoire reports", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiquePaysan]"]'),
      "Non"
    );
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[produitsIdentiqueAmapien]"]'),
      "Non"
    );
    await waitForHTMXRequestEnd(page.locator("form"));
    await page
      .locator('[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("1");
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[nblivPlancherDepassement]"]'),
      "Non"
    );
    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Oui"
    );

    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expectAlertDangerCheckbox(
      page,
      page.locator(
        '[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
      ),
      "Merci de sélectionner une option"
    );
    await expectAlertDangerCheckbox(
      page,
      page.locator(
        '[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
      ),
      "Merci de sélectionner une option"
    );
  });

  test("Validation étape 4 - Affichage lien contact", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Oui"
    );

    await expect(page.locator(".alert-info")).toHaveCount(2);
    await expect(page.locator(".alert-info").nth(1)).toContainText(
      "Pour proposer vos suggestions sur la manière de contrôler automatiquement ce maximum"
    );
  });
});
