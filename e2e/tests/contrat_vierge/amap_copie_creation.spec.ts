import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine, select2 } from "../../support/shorcut_actions";
import {
  associerAmapienReferentFerme,
  clickMainMenuLink,
} from "../../support/shorcut";

test.describe("Comme amap, je doit pouvoir copier un contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Cacher le lien si aucun contrat n'est disponible pour copie", async ({
    page,
  }) => {
    await associerAmapienReferentFerme(
      page,
      "amap2",
      "Prenom amapien 2 NOM AMAPIEN 2",
      "Ferme 2"
    );
    await login(page, "amap2");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des contrats vierges");
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "Ferme 2" });

    await page.getByText(/Créer un nouveau contrat/).click();
    await expect(page.locator(".btn-group.open .dropdown-menu")).not.toHaveText(
      /A partir d'un contrat existant/
    );
  });

  test("Retour liste par le bouton de retour", async ({ page }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des contrats vierges");
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page.getByText(/Créer un nouveau contrat/).click();
    await page.getByText(/A partir d'un contrat existant/).click();

    await page.getByText(/Retour/).click();
    await expect(page.locator("h3")).toHaveText(/Gestion des contrats vierges/);
  });

  test("Retour liste par le fil d'Ariane", async ({ page }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des contrats vierges");
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page.getByText(/Créer un nouveau contrat/).click();
    await page.getByText(/A partir d'un contrat existant/).click();

    await page
      .locator("h5")
      .getByText(/Gestion des contrats vierges/)
      .click();
    await expect(page.locator("h3")).toHaveText(/Gestion des contrats vierges/);
  });

  test("Créer une nouvelle copie", async ({ page }) => {
    await login(page, "amap");
    await clickMainMenuLink(page, "Gestionnaire AMAP");
    await clickMainMenuLink(page, "Gestion des contrats vierges");
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page.getByText(/Créer un nouveau contrat/).click();
    await page.getByText(/A partir d'un contrat existant/).click();

    await select2(
      page,
      "modele_contrat_copy_form[mc]",
      "contrat 1 (fin de souscription : 01/01/2030)"
    );
    await page.getByRole("button", { name: "Ajouter" }).click();

    await flashMessageShouldContain(page, "Contrat vierge copié avec succès.");

    await expect(page.locator("h3")).toHaveText(/Gestion des contrats vierges/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) th:nth-child(1)")
    ).toHaveText(/Copie de contrat 1/);
    await expect(
      page.locator(".container table tbody tr:nth-child(1) td:nth-child(2)")
    ).toHaveText(/Brouillon/);
  });

  test("Créer une nouvelle copie d'un contrat sans date de fin de souscription", async ({
    page,
  }) => {
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats vierges/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await page
      .getByText(/Créer un nouveau contrat/)
      .first()
      .click();
    await page
      .getByText(/A partir de zéro/)
      .first()
      .click();
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom");
    await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
    await page
      .locator('[name="model_contrat_form1[livraisonLieu]"]')
      .selectOption({ label: "ll amap test" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await page
      .getByText(/Créer un nouveau contrat/)
      .first()
      .click();
    await page
      .getByText(/A partir d'un contrat existant/)
      .first()
      .click();
    await select2(
      page,
      "modele_contrat_copy_form[mc]",
      "nom (fin de souscription : indéfinie)"
    );
    await page
      .getByText(/Ajouter/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Contrat vierge copié avec succès.");
    await expect(page.locator("h3")).toHaveText(/Gestion des contrats vierges/);
    await expect(page.locator(".container table")).toHaveText(/Copie de nom/);
    await expect(page.locator(".container table")).toHaveText(/Brouillon/);
  });
});
