import { test, expect, Locator, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractStep4 } from "../../support/shorcut";
import { dateClean } from "../../support/shorcut_date";
import { waitForHTMXRequestEnd } from "../../support/shorcut_htmx";
import { checkRadio } from "../../support/shorcut_actions";

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await dateClean();
  });

  test("Validation étape 4 - Masquer lien contact", async ({ page }) => {
    await login(page, "amapienref");
    await gotoContractStep4(page, "ferme");

    await checkRadio(
      page,
      page.locator('[name="model_contrat_form4[amapienGestionDeplacement]"]'),
      "Oui"
    );

    await expect(page.locator(".alert-info")).toHaveCount(2);
    await expect(page.locator(".alert-info").nth(1)).not.toContainText(
      "Pour proposer vos suggestions sur la manière de contrôler automatiquement ce maximum"
    );
  });
});
