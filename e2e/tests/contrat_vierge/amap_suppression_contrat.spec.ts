import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { creerCopieContrat } from "../../support/shorcut";

test.describe("Comme amap, je doit pouvoir supprimer un contrat vierge", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Suppression du contrat vierge, si le contrat est en brouillon", async ({
    page,
  }) => {
    await creerCopieContrat(
      page,
      "contrat 1 (fin de souscription : 01/01/2030)"
    );
    await login(page, "amap");
    await page
      .getByText(/Gestionnaire AMAP/)
      .first()
      .click();
    await page
      .getByText(/Gestion des contrats vierges/)
      .first()
      .click();
    await page
      .locator('[name="search_contrat_vierge_amap[ferme]"]')
      .selectOption({ label: "ferme" });
    await clickMenuLinkOnLine(
      page,
      "Copie de contrat 1",
      "Supprimer le contrat"
    );
    await expect(page.locator(".modal.in")).toHaveText(
      /Voulez-vous vraiment supprimer le contrat Copie de contrat 1 \?/
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await expect(page.locator(".alert-warning")).toHaveText(
      /Aucun contrat n'a encore été créé\./
    );
  });
});
