import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractCreation } from "../../support/shorcut";
import { dateClean } from "../../support/shorcut_date";

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await dateClean();
  });

  test("Validation étape 1", async ({ page }) => {
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom");
    await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
    await page
      .locator('[name="model_contrat_form1[livraisonLieu]"]')
      .selectOption({ label: "Sélectionner" });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Merci de sélectionner un lieu de livraison"
    );
  });

  const dbFinSouscription = [
    ["2000-08-15", "2000-08-15", "2000-08-16"],
    ["2000-08-15", "2000-08-16", "2000-08-25"],
  ];
});
