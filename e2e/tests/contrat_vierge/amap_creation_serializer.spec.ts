import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { gotoContractStep4 } from "../../support/shorcut";

test.describe("Test des cas limites de la serialisation", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Comme amap, je doit pourvoir créer un contrat avec des produits comprenant une TVA entiere", async ({
    page,
  }) => {
    await login(page, "paysan");
    await page
      .getByText(/Gestionnaire ferme/)
      .first()
      .click();
    await page
      .getByText(/Mes produits/)
      .first()
      .click();
    await clickMenuLinkOnLine(page, "Produit 1", "Modifier le produit");
    await page
      .locator('[name="ferme_produit[prix][tva]"]')
      .selectOption({ label: "10 %" });
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Produit modifié avec succès");

    await login(page, "amap");
    await gotoContractStep4(page, "ferme");
    await expect(page.locator("h3")).toContainText("modalités du contrat");
  });
});
