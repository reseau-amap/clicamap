import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { fillDatepicker } from "../../support/shorcut_actions";

import { gotoContractStep6 } from "../../support/shorcut";
import { dateClean } from "../../support/shorcut_date";

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    await dateClean();
  });

  test("Validation étape 6 - dates de règlement identiques", async ({
    page,
  }) => {
    await login(page, "amap");
    await gotoContractStep6(page, "ferme");
    await page
      .locator('input[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("button#btn-plus").click();
    await page.locator("button#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );
    await fillDatepicker(
      page,
      'input[name="model_contrat_form6[dateReglements][2][date]"]',
      new Date("2030-01-10")
    );

    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(
      page
        .locator(".col-xs-12")
        .filter({
          has: page.locator(
            'label:has-text("Dates des règlements à effectuer : *")'
          ),
        })
        .locator(".alert-danger")
    ).toContainText("Les dates de règlement ne doivent pas être identiques");
  });
});
