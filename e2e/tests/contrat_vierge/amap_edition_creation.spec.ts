import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { createContract } from "../../support/shorcut";

test.describe("Comme amap, je ne doit pas pouvoir sauvegarder en brouillon un contrat existant en creation", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Brouillon étape 1", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 2", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 3", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 4", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 5", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 6", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });

  test("Brouillon étape 7", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).not.toBeVisible();
  });
});
