import { test, expect, Page } from "@playwright/test";
import {
  requestAddLlToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import { gotoContractCreation } from "../../support/shorcut";

test.describe("Comme amap, je doit pouvoir revenir à l'étape précédente lors de la création du contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Retour étape 1", async ({ page }) => {
    await requestAddLlToAmap(page, "ll amap test 2", "amap@test.amap-aura.org");
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom");
    await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
    await page
      .locator('[name="model_contrat_form1[livraisonLieu]"]')
      .selectOption({ label: "ll amap test 2" });
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape précédente/)
      .first()
      .click();
    await expect(page.locator('[name="model_contrat_form1[nom]"]')).toHaveValue(
      "nom"
    );
    await expect(
      page.locator('[name="model_contrat_form1[filiere]"]')
    ).toHaveValue("filiere");
    await expect(
      page.locator(
        '[name="model_contrat_form1[livraisonLieu]"] option[selected]'
      )
    ).toHaveText("ll amap test 2");
  });
});
