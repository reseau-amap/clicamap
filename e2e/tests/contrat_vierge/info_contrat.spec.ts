import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { checkRadio, clickMenuLinkOnLine } from "../../support/shorcut_actions";

import {
  createContract,
  createContractWithCustomDelivery,
} from "../../support/shorcut";
import { login } from "../../support/shorcut_auth";

const createContractPermission = async (
  page: Page,
  deplacement: string,
  report: string
) => {
  await createContractWithCustomDelivery(page, "ferme", async (page) => {
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[amapienGestionDeplacement]"]'
      ),
      "Oui"
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[produitsIdentiquePaysan]"]'
      ),
      "Oui"
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[produitsIdentiqueAmapien]"]'
      ),
      "Oui"
    );
    await expect(
      page.locator('[name="model_contrat_form4[nblivPlancher]"]')
    ).toBeEnabled();
    await page
      .locator('input[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("1");
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[nblivPlancherDepassement]"]'
      ),
      "Oui"
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[amapienPermissionDeplacementLivraison]"]'
      ),
      deplacement
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[amapienPermissionReportLivraison]"]'
      ),
      report
    );
  });

  await clickMenuLinkOnLine(page, "En création", "Tester le contrat");
};

test.describe("Comme utilisateur, je doit avoir accès à un encart qui reprends les informations de base du contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Affichage des infos déplacement - contrats sans report ni déplacement", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Tester le contrat");
    await expect(page.locator(".alert-info")).toHaveText(
      /Déplacement amapien sur date avec\/sans livraison : NON/
    );
  });

  test("Affichage des infos déplacement - contrats avec déplacement", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContractPermission(page, "Oui", "Non");
    await expect(page.locator(".alert-info")).toHaveText(
      /Déplacement amapien sur date avec\/sans livraison : Oui sur une date sans livraison/
    );
  });

  test("Affichage des infos déplacement - contrats avec report", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContractPermission(page, "Non", "Oui");
    await expect(page.locator(".alert-info")).toHaveText(
      /Déplacement amapien sur date avec\/sans livraison : Oui, sur une date avec livraison/
    );
  });
});
