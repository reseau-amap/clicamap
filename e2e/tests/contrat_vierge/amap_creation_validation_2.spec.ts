import { expect, test } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { fillDatepicker } from "../../support/shorcut_actions";

import {
  addAmapEtudianteAbsence,
  gotoContractStep2,
  setAmapEtudiante,
} from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";
import { enableExdebug } from "../../support/shorcut_debug";

test.describe("Comme amap, je doit avoir des messages d'erreurs en cas d'informations invalides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  const dbFinSouscription = [
    ["2000-08-15", "2000-08-15", "2000-08-16"],
    ["2000-08-15", "2000-08-16", "2000-08-25"],
  ];
  dbFinSouscription.forEach((row, i) => {
    test(`Validation étape 2 - date de fin de souscription ${i}`, async ({
      page,
    }) => {
      console.log(row);
      await login(page, "amap");
      await gotoContractStep2(page, "ferme");
      await page.locator("#btn-plus").click();
      await page.locator("#btn-plus").click();
      await fillDatepicker(
        page,
        '[name="model_contrat_form2[forclusion]"]',
        new Date(row[0])
      );
      await fillDatepicker(
        page,
        '[name="model_contrat_form2[dates][1][dateLivraison]"]',
        new Date(row[1])
      );
      await fillDatepicker(
        page,
        '[name="model_contrat_form2[dates][2][dateLivraison]"]',
        new Date(row[2])
      );
      await page
        .locator('[name="model_contrat_form2[delaiModifContrat]"]')
        .fill("2");

      await page
        .getByText(/Étape suivante/)
        .first()
        .click();
      await expect(page.locator(".alert-danger")).toContainText(
        "La date de fin de souscription doit se situer AVANT la date de la première livraison et doit laisser au paysan le temps de préparer les contrats (délai présent sur la ferme)"
      );
    });
  });

  test("Validation étape 2 - génération des dates", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await fillDatepicker(
      page,
      '[name="date_livraison_debut"]',
      new Date("2000-01-01")
    );
    await fillDatepicker(
      page,
      '[name="date_livraison_fin"]',
      new Date("2000-01-31")
    );

    await page
      .getByText(/Générer des dates/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form2[dates][1][dateLivraison]"]')
    ).toHaveValue("2000-01-01");
    await expect(
      page.locator('[name="model_contrat_form2[dates][2][dateLivraison]"]')
    ).toHaveValue("2000-01-08");
    await expect(
      page.locator('[name="model_contrat_form2[dates][3][dateLivraison]"]')
    ).toHaveValue("2000-01-15");
    await expect(
      page.locator('[name="model_contrat_form2[dates][4][dateLivraison]"]')
    ).toHaveValue("2000-01-22");
    await expect(
      page.locator('[name="model_contrat_form2[dates][5][dateLivraison]"]')
    ).toHaveValue("2000-01-29");
  });

  test("Validation étape 2 - au moins une date", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await fillDatepicker(
      page,
      '[name="model_contrat_form2[forclusion]"]',
      new Date("2000-01-01")
    );
    await page
      .locator('[name="model_contrat_form2[delaiModifContrat]"]')
      .fill("1");

    const button = page.getByText(/Étape suivante/);
    await expect(button).toHaveAttribute("disabled");

    await button.evaluate((node) => node.removeAttribute("disabled"));

    await button.click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Les dates de livraisons sont obligatoires, veuillez cliquer sur le bouton 'générer des dates'"
    );
  });

  test("Validation étape 2 - pas de date vide", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await page.locator("#btn-plus").click();
    await page
      .locator('[name="model_contrat_form2[forclusion]"]')
      .fill("2000-01-01");
    await page
      .locator('[name="model_contrat_form2[delaiModifContrat]"]')
      .fill("1");

    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Merci de sélectionner une date de livraison"
    );
  });

  test("Validation étape 2 - delais modif 0", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[forclusion]"]',
      new Date("2030-01-01")
    );
    await fillDatepicker(
      page,
      'input[name="date_livraison_debut"]',
      new Date("2030-02-01")
    );
    await fillDatepicker(
      page,
      'input[name="date_livraison_fin"]',
      new Date("2030-03-01")
    );

    await page.getByRole("button", { name: "Générer des dates" }).click();

    await page
      .locator('[name="model_contrat_form2[delaiModifContrat]"]')
      .fill("0");

    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Cette valeur doit être supérieure à 0."
    );
  });

  test("Validation étape 2 - delais modif vide", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[forclusion]"]',
      new Date("2030-01-01")
    );
    await fillDatepicker(
      page,
      'input[name="date_livraison_debut"]',
      new Date("2030-02-01")
    );
    await fillDatepicker(
      page,
      'input[name="date_livraison_fin"]',
      new Date("2030-03-01")
    );

    await page.getByRole("button", { name: "Générer des dates" }).click();

    await page.getByRole("button", { name: "Étape suivante" }).click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Cette valeur ne doit pas être vide."
    );
  });

  test("Validation étape 2 - date de livraison été", async ({ page }) => {
    await dateSet("2022-07-01");
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await page.locator("#btn-plus").click();

    await expect(page.locator(".erreur-livraison")).toContainText(
      "Pour rappel l'AMAP livre le Lundi en saison ete OU Mardi en saison hiver"
    );

    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2022-07-04")
    );
    await expect(page.locator(".erreur-livraison")).not.toBeVisible();

    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2022-07-05")
    );
    await expect(page.locator(".erreur-livraison")).toContainText(
      "Pour rappel l'AMAP livre le Lundi en saison ete OU Mardi en saison hiver"
    );
  });

  test("Validation étape 2 - date de livraison hiver", async ({ page }) => {
    await dateSet("2022-01-01");
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await page.locator("#btn-plus").click();

    await expect(page.locator(".erreur-livraison")).toContainText(
      "Pour rappel l'AMAP livre le Lundi en saison ete OU Mardi en saison hiver"
    );

    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2022-01-04")
    );
    await expect(page.locator(".erreur-livraison")).not.toBeVisible();

    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2022-01-05")
    );
    await expect(page.locator(".erreur-livraison")).toContainText(
      "Pour rappel l'AMAP livre le Lundi en saison ete OU Mardi en saison hiver"
    );
  });

  test("Validation étape 2 - afficher date absence amap étudiante", async ({
    page,
  }) => {
    await setAmapEtudiante(page, "amap@test.amap-aura.org", true);
    await addAmapEtudianteAbsence(
      page,
      "amap@test.amap-aura.org",
      "test",
      new Date("2000-01-01"),
      new Date("2000-01-06")
    );
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await page.locator("#btn-plus").click();
    await page.locator("#btn-plus").click();
    await page.locator("#btn-plus").click();
    await page.locator("#btn-plus").click();
    await page.locator("#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2000-01-01")
    );
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][2][dateLivraison]"]',
      new Date("2000-01-02")
    );
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][3][dateLivraison]"]',
      new Date("2000-01-05")
    );
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][4][dateLivraison]"]',
      new Date("2000-01-06")
    );
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][5][dateLivraison]"]',
      new Date("2000-01-07")
    );

    await expect(
      page.getByRole("button", { name: "Étape suivante" })
    ).toBeDisabled();
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][1][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeVisible();
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][1][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toHaveText(
      'Attention cette date est incluse sur la période d\'absence "test"'
    );
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][2][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeVisible();
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][2][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toHaveText(
      'Attention cette date est incluse sur la période d\'absence "test"'
    );
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][3][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeVisible();
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][3][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toHaveText(
      'Attention cette date est incluse sur la période d\'absence "test"'
    );
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][4][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeHidden();
    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][5][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeHidden();
    await expect(
      page.getByRole("button", { name: "Étape suivante" })
    ).toBeEnabled();
  });

  test("Validation étape 2 - masquer date absence amap non étudiante", async ({
    page,
  }) => {
    await setAmapEtudiante(page, "amap@test.amap-aura.org", true);
    await addAmapEtudianteAbsence(
      page,
      "amap@test.amap-aura.org",
      "test",
      new Date("2000-01-01"),
      new Date("2000-01-06")
    );
    await setAmapEtudiante(page, "amap@test.amap-aura.org", false);
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await page.locator("#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form2[dates][1][dateLivraison]"]',
      new Date("2000-01-02")
    );

    await expect(
      page.locator(
        '[name="model_contrat_form2[dates][1][dateLivraison]"] ~ .erreur-etudiant'
      )
    ).toBeHidden();
  });
});
