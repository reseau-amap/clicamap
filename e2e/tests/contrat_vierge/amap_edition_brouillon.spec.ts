import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

import { gotoContractCreation } from "../../support/shorcut";

test.describe("Comme amap, je ne doit pouvoir en brouillon un contrat existant en brouillon", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Brouillon étape 1", async ({ page }) => {
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom 1");
    await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Nom 1", "Modifier le contrat");
    await page.locator('[name="model_contrat_form1[nom]"]').clear();
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom 2");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await expect(page.locator(".container table tbody")).toHaveText(/Nom 2/);
    await expect(page.locator(".container table tbody")).not.toHaveText(
      /Nom 1/
    );
  });
});
