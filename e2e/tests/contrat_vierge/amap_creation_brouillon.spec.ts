import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import {
  gotoContractCreation,
  gotoContractStep2,
  gotoContractStep3,
  gotoContractStep4,
  gotoContractStep5,
  gotoContractStep6,
  gotoContractStep7,
} from "../../support/shorcut";
import { waitForHTMXRequestEnd } from "../../support/shorcut_htmx";

test.describe("Comme amap, je doit pouvoir sauvegarder le contrat en brouillon", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Brouillon étape 1", async ({ page }) => {
    await login(page, "amap");
    await gotoContractCreation(page, "ferme");
    await page.locator('[name="model_contrat_form1[nom]"]').fill("nom");
    await page.locator('[name="model_contrat_form1[filiere]"]').fill("filiere");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await expect(page.locator(".container table tbody")).toHaveText(/nom/);
    await expect(page.locator(".container table tbody")).toHaveText(
      /Brouillon/
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await expect(page.locator('[name="model_contrat_form1[nom]"]')).toHaveValue(
      "nom"
    );
    await expect(
      page.locator('[name="model_contrat_form1[filiere]"]')
    ).toHaveValue("filiere");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 2", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep2(page, "ferme");
    await fillDatepicker(
      page,
      '[name="model_contrat_form2[forclusion]"]',
      new Date("2000-01-01")
    );
    await fillDatepicker(
      page,
      '[name="date_livraison_debut"]',
      new Date("2030-02-01")
    );
    await fillDatepicker(
      page,
      '[name="date_livraison_fin"]',
      new Date("2030-03-01")
    );
    await page
      .locator('[name="model_contrat_form2[delaiModifContrat]"]')
      .fill("123");

    await page
      .getByText(/Générer des dates/)
      .first()
      .click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form2[forclusion]"]')
    ).toHaveValue("2000-01-01");
    await expect(page.locator('[name="date_livraison_debut"]')).toHaveValue("");
    await expect(page.locator('[name="date_livraison_fin"]')).toHaveValue("");
    await expect(
      page.locator('[name="model_contrat_form2[delaiModifContrat]"]')
    ).toHaveValue("123");
    await expect(
      page.locator('[name="model_contrat_form2[dates][1][dateLivraison]"]')
    ).toHaveValue("2030-02-01");
    await expect(
      page.locator('[name="model_contrat_form2[dates][2][dateLivraison]"]')
    ).toHaveValue("2030-02-08");
    await expect(
      page.locator('[name="model_contrat_form2[dates][3][dateLivraison]"]')
    ).toHaveValue("2030-02-15");
    await expect(
      page.locator('[name="model_contrat_form2[dates][4][dateLivraison]"]')
    ).toHaveValue("2030-02-22");
    await expect(
      page.locator('[name="model_contrat_form2[dates][5][dateLivraison]"]')
    ).toHaveValue("2030-03-01");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 2 - exclusions existantes", async ({ page }) => {
    // Test proper cleanup of exclusions if date deleted
    await login(page, "amap");
    await gotoContractStep5(page, "ferme");
    await page
      .locator('[name="model_contrat_form5[exclusions][2030-02-01_1]"]')
      .uncheck();
    await page
      .locator('[name="model_contrat_form5[exclusions][2030-02-08_1]"]')
      .uncheck();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");

    await page
      .getByText(/Étape suivante/)
      .first()
      .click();

    await page.locator(".btn-remove").first().click();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();

    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );

    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form5[exclusions][2030-02-08_1]"]')
    ).not.toBeChecked();
  });

  test("Brouillon étape 3", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");
    await page
      .locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
      .clear();
    await page
      .locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
      .fill("1.12");
    await page
      .locator('[name="model_contrat_form3[produits][1][prix][prixTTC]"]')
      .clear();
    await page
      .locator('[name="model_contrat_form3[produits][1][prix][prixTTC]"]')
      .fill("2.12");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form3[produits][0][prix][prixTTC]"]')
    ).toHaveValue("1.12");
    await expect(
      page.locator('[name="model_contrat_form3[produits][1][prix][prixTTC]"]')
    ).toHaveValue("2.12");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 4", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");
    await page
      .locator('[name="model_contrat_form4[amapienGestionDeplacement]"]')
      .locator('input[value="0"]:scope')
      .check();
    await page
      .locator('[name="model_contrat_form4[produitsIdentiquePaysan]"]')
      .locator('input[value="0"]:scope')
      .check();
    await page
      .locator('[name="model_contrat_form4[produitsIdentiqueAmapien]"]')
      .locator('input[value="0"]:scope')
      .check();
    await waitForHTMXRequestEnd(page.locator("form"));
    await page
      .locator('[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("1");
    await page
      .locator('[name="model_contrat_form4[nblivPlancherDepassement]"]')
      .locator('input[value="1"]:scope')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator(
        '[name="model_contrat_form4[produitsIdentiquePaysan]"][value="0"]'
      )
    ).toBeChecked();
    await expect(
      page.locator(
        '[name="model_contrat_form4[produitsIdentiqueAmapien]"][value="0"]'
      )
    ).toBeChecked();
    await expect(
      page.locator('[name="model_contrat_form4[nblivPlancher]"]')
    ).toHaveValue("1");
    await expect(
      page.locator(
        '[name="model_contrat_form4[nblivPlancherDepassement]"][value="1"]'
      )
    ).toBeChecked();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 5", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep5(page, "ferme");
    await page
      .locator('[name="model_contrat_form5[exclusions][2030-02-01_1]"]')
      .uncheck();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form5[exclusions][2030-02-01_1]"]')
    ).not.toBeChecked();
    await expect(
      page.locator('[name="model_contrat_form5[exclusions][2030-02-01_2]"]')
    ).toBeChecked();
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 6", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep6(page, "ferme");
    await page
      .locator('[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("#btn-plus").click();
    await page
      .locator('[name="model_contrat_form6[dateReglements][1][date]"]')
      .fill("2030-01-10", {
        force: true,
      });
    await page.locator("h3").click(); // Hide popup
    await page
      .locator('[name="model_contrat_form6[reglementType][]"]')
      .locator('input[value="cheque"]:scope')
      .check();
    await page
      .locator('[name="model_contrat_form6[reglementModalite]"]')
      .fill("modalite");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form6[reglementNbMax]"]')
    ).toHaveValue("1");
    await expect(
      page.locator('[name="model_contrat_form6[dateReglements][1][date]"]')
    ).toHaveValue("2030-01-10");
    await expect(
      page.locator(
        '[name="model_contrat_form6[reglementType][]"][value="cheque"]'
      )
    ).toBeChecked();
    await expect(
      page.locator('[name="model_contrat_form6[reglementModalite]"]')
    ).toHaveValue("modalite");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });

  test("Brouillon étape 7", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep7(page, "ferme");
    await page
      .locator('[name="model_contrat_form7[contratAnnexes]"]')
      .fill("annexes");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(
      page,
      "Le contrat a bien été sauvegardé en brouillon."
    );
    await clickMenuLinkOnLine(page, "Brouillon", "Modifier le contrat");
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await page
      .getByText(/Étape suivante/)
      .first()
      .click();
    await expect(
      page.locator('[name="model_contrat_form7[contratAnnexes]"]')
    ).toHaveValue("annexes");
    await expect(
      page
        .locator('input[type="submit"]')
        .getByText(/Sauvegarder/)
        .first()
    ).toBeVisible();
  });
});
