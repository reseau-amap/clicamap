import { test, expect, Page } from "@playwright/test";
import {
  requestAddUserToAmap,
  requestResetDb,
} from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

import {
  associerAmapienReferentFerme,
  clickMainMenuLink,
  createContract,
  deAssocierAmapienReferentFerme,
} from "../../support/shorcut";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

test.describe("Comme paysan, je doit pouvoir valider un nouveau contrat", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Validation contrat", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(
      page,
      "En création",
      "Envoyer le contrat au paysan pour validation"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();

    await login(page, "paysan");
    await clickMainMenuLink(page, "Gestion des contrats");
    await clickMainMenuLink(page, "Contrats à valider");
    const line = page.locator("body .container table tbody tr").filter({
      has: page.locator("td:nth-child(1)").getByText("nom", { exact: true }),
    });
    await line.getByRole("link", { name: "Voir le contrat" }).click();

    await page
      .locator('[name="modele_contrat_paysan_approval[confirmSign]"]')
      .check();
    await page
      .locator('[name="modele_contrat_paysan_approval[confirmCharter]"]')
      .check();
    await page
      .getByText(/Valider/)
      .first()
      .click();
    await expect(page.locator(".alert-warning")).toContainText(
      "Vous n'avez pas de contrats en attente de validation :-)"
    );
  });
});
