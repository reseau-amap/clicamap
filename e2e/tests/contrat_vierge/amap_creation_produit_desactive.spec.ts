import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import {
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import {
  disableProduct,
  gotoContractCreation,
  gotoContractStep2,
  gotoContractStep3,
  gotoContractStep4,
  gotoContractStep5,
  gotoContractStep6,
  gotoContractStep7,
} from "../../support/shorcut";

test.describe("Comme amap, je doit créer un contrat en masquant les produits désactivés", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Produits désactivés", async ({ page }) => {
    await disableProduct(page, "paysan", "Produit 1");
    await login(page, "amap");
    await gotoContractStep3(page, "ferme");

    await expect(
      page.locator("#model_contrat_form3_produits > .well")
    ).toHaveCount(1);
    await expect(
      page.locator(
        '#model_contrat_form3_produits > .well [name="model_contrat_form3[produits][0][fermeProduit]"] option'
      )
    ).toHaveCount(1);
    await expect(
      page
        .locator(
          '#model_contrat_form3_produits > .well [name="model_contrat_form3[produits][0][fermeProduit]"] option'
        )
        .nth(0)
    ).toHaveText("produit 2 / cond1");
  });
});
