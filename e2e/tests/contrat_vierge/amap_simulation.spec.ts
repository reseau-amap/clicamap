import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import {
  checkCheckbox,
  checkRadio,
  clickMenuLinkOnLine,
  fillDatepicker,
} from "../../support/shorcut_actions";

import { createContract, gotoContractStep4 } from "../../support/shorcut";
import { dateClean, dateSet } from "../../support/shorcut_date";

test.describe("Comme amap, je doit pouvoir simuler un contrat en création", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test.afterEach(async ({ page }) => {
    dateClean();
  });

  test("Calcul des valeurs du contrat", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Tester le contrat");

    const fieldsBegin = [
      "#js-total",
      "#js-total-produit-3",
      "#js-total-produit-4",
      "#js-total-date-19",
      "#js-total-date-20",
      "#js-total-date-21",
      "#js-total-date-22",
      "#js-total-date-23",
    ];
    for (let field of fieldsBegin) {
      await expect(page.locator(field)).toHaveValue("0,00 €");
    }

    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .pressSequentially("2");
    await page
      .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
      .pressSequentially("1");

    await expect(page.locator("#js-total")).toHaveValue("3,00 €");
    await expect(page.locator("#js-total-produit-3")).toHaveValue("3,00 €");
    await expect(page.locator("#js-total-date-19")).toHaveValue("2,00 €");
    await expect(page.locator("#js-total-date-20")).toHaveValue("1,00 €");

    const fieldsEnd = [
      "#js-total-produit-4",
      "#js-total-date-21",
      "#js-total-date-22",
      "#js-total-date-23",
    ];
    for (let field of fieldsEnd) {
      await expect(page.locator(field)).toHaveValue("0,00 €");
    }
  });

  test("Validation liv min", async ({ page }) => {
    await login(page, "amap");
    await createContract(page, "ferme");
    await clickMenuLinkOnLine(page, "En création", "Tester le contrat");

    await expect(page.locator(".alert-danger")).toHaveCount(0);
    await page.getByRole("button", { name: "Tester" }).click();

    await expect(page.locator(".alert-danger")).toHaveCount(1);
    await expect(page.locator(".alert-danger")).toContainText(
      "Vous devez sélectionner au moins 1 livraisons"
    );
  });

  test("Validation liv identiques", async ({ page }) => {
    await login(page, "amap");
    await gotoContractStep4(page, "ferme");
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[amapienGestionDeplacement]"]'
      ),
      "Non"
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[produitsIdentiquePaysan]"]'
      ),
      "Non"
    );
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[produitsIdentiqueAmapien]"]'
      ),
      "Oui"
    );
    await expect(
      page.locator('[name="model_contrat_form4[nblivPlancher]"]')
    ).toBeEnabled();
    await page
      .locator('input[name="model_contrat_form4[nblivPlancher]"]')
      .pressSequentially("1");
    await checkRadio(
      page,
      page.locator(
        'input[name="model_contrat_form4[nblivPlancherDepassement]"]'
      ),
      "Oui"
    );
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page
      .locator('input[name="model_contrat_form6[reglementNbMax]"]')
      .fill("1");
    await page.locator("button#btn-plus").click();
    await fillDatepicker(
      page,
      'input[name="model_contrat_form6[dateReglements][1][date]"]',
      new Date("2030-01-10")
    );
    await checkCheckbox(
      page,
      page.locator('input[name="model_contrat_form6[reglementType][]"]'),
      "Chèque"
    );
    await page
      .locator('textarea[name="model_contrat_form6[reglementModalite]"]')
      .fill("modalite");
    await page.getByRole("button", { name: "Étape suivante" }).click();
    await page.getByRole("button", { name: "Valider" }).click();

    await clickMenuLinkOnLine(page, "En création", "Tester le contrat");

    await expect(page.locator(".alert-danger")).toHaveCount(0);

    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
      .pressSequentially("2");
    await page
      .locator('[name="contrat_souscription_form1[items][20_4][qty]"]')
      .clear();
    await page
      .locator('[name="contrat_souscription_form1[items][20_4][qty]"]')
      .pressSequentially("1");

    await page.getByRole("button", { name: "Tester" }).click();

    await expect(page.locator(".alert-danger")).toHaveCount(1);
    await expect(page.locator(".alert-danger")).toContainText(
      "Toutes les livraisons doivent être identiques"
    );
  });

  test("Prise en compte du délais du contrat pour la simulation", async ({
    page,
  }) => {
    await login(page, "amap");
    await createContract(page, "ferme");

    dateSet("2030-01-31");
    await clickMenuLinkOnLine(page, "En création", "Tester le contrat");
    await page.waitForLoadState();

    await expect(
      page.locator('[name="contrat_souscription_form1[items][19_3][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][19_4][qty]"]')
    ).toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][20_3][qty]"]')
    ).not.toHaveAttribute("readonly");
    await expect(
      page.locator('[name="contrat_souscription_form1[items][20_4][qty]"]')
    ).not.toHaveAttribute("readonly");
  });
});
