import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme administrateur, je devrais pouvoir ajouter un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Documents - Accès par le menu comme admin", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/list_documents");
    await page
      .getByText(/Créer un nouveau document/)
      .first()
      .click();
    await expect(page.locator("form")).toContainText("Nom du document");
    await expect(page).toHaveURL(/\/documentation\/form/);
  });

  test("Vérification des champs obligatoires", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/form");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(
      page.locator('input[name="documentation[nom]"] ~ .alert-danger')
    ).toContainText('Le champ "Nom" est requis.');
    await expect(
      page.locator('input[name="documentation[lien]"] ~ .alert-danger')
    ).toContainText('Le champ "Lien" est requis.');
  });

  test("Vérificaion de la validité du lien", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/form");
    await page.locator('input[name="documentation[nom]"]').fill("test");
    await page
      .locator('input[name="documentation[lien]"]')
      .fill("lien_invalide");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(
      page.locator('input[name="documentation[lien]"] ~ .alert-danger')
    ).toContainText('Le champ "Lien" doit contenir une URL valide.');
  });

  test("Vérification selection type d'utilisateur", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/form");
    await page.locator('input[name="documentation[nom]"]').fill("test");
    await page
      .locator('input[name="documentation[lien]"]')
      .fill("http://www.amap-aura.org");
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await expect(page.locator(".alert-danger")).toContainText(
      "Merci de selectionner au moins un type d'utilisateur"
    );
  });

  test("Ajout d'un document", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/form");
    await page
      .locator('input[name="documentation[nom]"]')
      .fill("Nom du nouveau document");
    await page
      .locator('input[name="documentation[lien]"]')
      .fill("http://www.amap-aura.org");
    await page
      .locator('input[name="documentation[permissionAnonyme]"]')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Document correctement ajouté");
    await expect(page.locator(".container table")).toContainText(
      "Nom du nouveau document"
    );
  });

  test("Vérification nombre de documents public max", async ({ page }) => {
    const addDocument = async (page: Page) => {
      await page.goto("/documentation/form");
      await page
        .locator('input[name="documentation[nom]"]')
        .fill("Nom du nouveau document");
      await page
        .locator('input[name="documentation[lien]"]')
        .fill("http://www.amap-aura.org");
      await page
        .locator('input[name="documentation[permissionAnonyme]"]')
        .check();
      await page.getByText(/Sauvegarder/).click();
      await flashMessageShouldContain(page, "Document correctement ajouté");
    };
    await login(page, "admin");
    await addDocument(page);
    await addDocument(page);
    await addDocument(page);

    await page.goto("/documentation/form");
    await page
      .locator('input[name="documentation[nom]"]')
      .fill("Nom du nouveau document");
    await page
      .locator('input[name="documentation[lien]"]')
      .fill("http://www.amap-aura.org");
    await page
      .locator('input[name="documentation[permissionAnonyme]"]')
      .check();
    await page.getByText(/Sauvegarder/).click();

    await expect(page.locator(".alert-danger")).toContainText(
      "Trois documents maximum sont autorisés en page d'accueil"
    );
  });
});
