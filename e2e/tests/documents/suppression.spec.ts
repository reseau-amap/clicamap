import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { clickMenuLinkOnLine } from "../../support/shorcut_actions";

const context = async (page: Page) => {
  await login(page, "admin");
  await page.goto("/documentation/form");
  await page
    .locator('input[name="documentation[nom]"]')
    .fill("Nom du nouveau document");
  await page
    .locator('input[name="documentation[lien]"]')
    .fill("http://www.amap-aura.org");
  await page.locator('input[name="documentation[permissionAnonyme]"]').check();
  await page
    .getByText(/Sauvegarder/)
    .first()
    .click();
  await flashMessageShouldContain(page, "Document correctement ajouté");
};

test.describe("Comme administrateur, je devrais pouvoir supprimer un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Suppression du document", async ({ page }) => {
    await context(page);
    await page.goto("/documentation/list_documents");
    await clickMenuLinkOnLine(
      page,
      "Nom du nouveau document",
      "Supprimer le document"
    );
    await page.locator(".modal.in").getByText(/OUI/).first().click();
    await expect(page.locator(".container table")).not.toHaveText(
      /Nom du nouveau document/
    );
  });

  test("Annulation suppression du document", async ({ page }) => {
    await context(page);
    await page.goto("/documentation/list_documents");
    await clickMenuLinkOnLine(
      page,
      "Nom du nouveau document",
      "Supprimer le document"
    );
    await page.locator(".modal.in").getByText(/NON/).first().click();
    await expect(page.locator(".container table")).toHaveText(
      /Nom du nouveau document/
    );
  });
});
