import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login, logout } from "../../support/shorcut_auth";

const context = async (page: Page) => {
  await login(page, "admin");
  await page.goto("/documentation/form");
  await page
    .locator('input[name="documentation[nom]"]')
    .fill("Nom du nouveau document");
  await page
    .locator('input[name="documentation[lien]"]')
    .fill("http://www.amap-aura.org");
};

test.describe("Comme utilisateur, lors de l'ajout d'un document je devrais pouvoir le visualiser selon sa visibilité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  const portalUsers = ["paysan", "amap", "amapienref", "amapien"];
  portalUsers.forEach((user) => {
    test(`Visibilité portail ${user}`, async ({ page }) => {
      await context(page);
      await page
        .locator('input[name="documentation[permissionAnonyme]"]')
        .check();
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await logout(page);
      await page.goto("/");
      await expect(
        page.getByText(/Nom du nouveau document/).first()
      ).toHaveAttribute("href", "http://www.amap-aura.org");
      await login(page, user);
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).not.toContainText(
        "Nom du nouveau document"
      );
    });
  });

  const paysanUsers = ["admin", "amap", "amapienref", "amapien"];
  paysanUsers.forEach((user) => {
    test(`Visibilité paysan ${user}`, async ({ page }) => {
      await context(page);
      await page
        .locator('input[name="documentation[permissionPaysan]"]')
        .check();
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await logout(page);
      await login(page, "paysan");
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).toContainText(
        "Nom du nouveau document"
      );
      await login(page, user);
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).not.toContainText(
        "Nom du nouveau document"
      );
    });
  });

  const amapUsers = ["admin", "paysan", "amapienref", "amapien"];
  amapUsers.forEach((user) => {
    test(`Visibilité amap ${user}`, async ({ page }) => {
      await context(page);
      await page.locator('input[name="documentation[permissionAmap]"]').check();
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await logout(page);
      await login(page, "amap");
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).toContainText(
        "Nom du nouveau document"
      );
      await login(page, user);
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).not.toContainText(
        "Nom du nouveau document"
      );
    });
  });

  const amapienRefUsers = ["admin", "paysan", "amapien"];
  amapienRefUsers.forEach((user) => {
    test(`Visibilité amapien referent ${user}`, async ({ page }) => {
      await context(page);
      await page
        .locator('input[name="documentation[permissionAmapienref]"]')
        .check();
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await logout(page);
      await login(page, "amapienref");
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).toContainText(
        "Nom du nouveau document"
      );
      await login(page, user);
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).not.toContainText(
        "Nom du nouveau document"
      );
    });
  });

  const amapienUsers = ["admin", "paysan"];
  amapienUsers.forEach((user) => {
    test(`Visibilité amapien ${user}`, async ({ page }) => {
      await context(page);
      await page
        .locator('input[name="documentation[permissionAmapien]"]')
        .check();
      await page
        .getByText(/Sauvegarder/)
        .first()
        .click();
      await logout(page);
      await login(page, "amapien");
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).toContainText(
        "Nom du nouveau document"
      );
      await login(page, user);
      await page.goto("/documentation");
      await expect(page.locator(".container .well-lg")).not.toContainText(
        "Nom du nouveau document"
      );
    });
  });
});
