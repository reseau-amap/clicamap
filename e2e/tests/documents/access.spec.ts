import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";

test.describe("Comme utilisateur, je devrais pouvoir accéder à la Gestion des guides", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Gestion des guides - Accès par le menu comme admin", async ({
    page,
  }) => {
    await login(page, "admin");
    await page
      .getByText(/Communication/)
      .first()
      .click();
    await page
      .getByText(/Gestion des guides/)
      .first()
      .click();
    await expect(page.locator("h3")).toContainText("Gestion des guides");
    await expect(page).toHaveURL(/\/documentation\/list_documents/);
  });

  ["paysan", "amap"].forEach((user) => {
    test(`Pas d\'accès menu pour les autres utilisateurs ${user}`, async ({
      page,
    }) => {
      await login(page, user);
      await expect(page.locator("nav")).not.toContainText("Communication");
    });
  });
});
