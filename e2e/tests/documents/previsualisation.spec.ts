import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme utilisateur, je devrais pouvoir visualiser un document", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Visualisation du document", async ({ page }) => {
    await login(page, "admin");
    await page.goto("/documentation/form");
    await page
      .locator('input[name="documentation[nom]"]')
      .fill("Nom du nouveau document");
    await page
      .locator('input[name="documentation[lien]"]')
      .fill("http://www.amap-aura.org");
    await page
      .locator('input[name="documentation[permissionAnonyme]"]')
      .check();
    await page
      .getByText(/Sauvegarder/)
      .first()
      .click();
    await flashMessageShouldContain(page, "Document correctement ajouté");
    await page.locator(".container table .dropdown-toggle").click();
    await expect(
      page
        .locator(".container table .dropdown-menu")
        .getByText(/Voir le document/)
        .first()
    ).toHaveAttribute("href", "http://www.amap-aura.org");
  });
});
