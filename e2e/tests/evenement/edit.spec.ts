import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { fillNoteEditor, select2 } from "../../support/shorcut_actions";
import { createNewEvtAmap } from "../../support/shorcut";

test.describe("Comme utilisateur, je devrais pouvoir éditer une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Modifier une actualité", async ({ page }) => {
    await createNewEvtAmap(page, "Nouvelle actualité");
    await page.locator(".glyphicon-pencil").click();

    await expect(page.locator("h3")).toContainText("Modifier une actualité");

    await page
      .locator('[name="evenement[titre]"]')
      .fill("Nouvelle actualité 2");
    await page.getByRole("button", { name: "Sauvegarder" }).click();

    await flashMessageShouldContain(page, "L'actualité a bien été modifiée");

    await expect(page.locator(".block-evenement-titre")).toContainText(
      "Nouvelle actualité 2"
    );
  });
});
