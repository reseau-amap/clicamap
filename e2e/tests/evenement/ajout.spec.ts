import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { fillNoteEditor, select2 } from "../../support/shorcut_actions";

const loginAndCheckPage = async (page: Page, role: string) => {
  await login(page, role);
  await expect(page.locator("h3")).toContainText("Fil d'actualités");
};

const getButtonNew = (page: Page) => {
  return page.getByRole("link", { name: "Ajouter une actualité" });
};

const fillFormValideAndCheck = async (page: Page) => {
  await page.locator('[name="evenement[titre]"]').fill("Nouvelle actualité");
  await fillNoteEditor(
    page,
    "evenement[description]",
    "Description de l'évènement"
  );
  await fillNoteEditor(
    page,
    "evenement[descriptionCourte]",
    "Description courte de l'évènement"
  );
  await page.getByRole("button", { name: "Sauvegarder" }).click();

  await flashMessageShouldContain(page, "L'actualité a bien été ajoutée");
  await expect(page.locator("h3")).toContainText("Fil d'actualités");
  await expect(page.locator(".block-evenement-titre")).toContainText(
    "Nouvelle actualité"
  );
};

const gotoForm = async (page: Page) => {
  await loginAndCheckPage(page, "amap");

  await getButtonNew(page).click();
};

test.describe("Comme utilisateur, je devrais pouvoir ajouter une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Masquer le bouton pour les utilisateurs sans droit d'ajout", async ({
    page,
  }) => {
    await loginAndCheckPage(page, "empty");

    await expect(getButtonNew(page)).toHaveCount(0);
  });

  test("Valeur par défaut des champs", async ({ page }) => {
    await loginAndCheckPage(page, "amap");
    await getButtonNew(page).click();

    await expect(
      page.locator('[name="evenement[related]"] option')
    ).toHaveCount(1);
    await expect(
      page.locator('[name="evenement[related]"] option:nth-child(1)')
    ).toHaveText("amap test");
    await expect(page.locator('[name="evenement[titre]"]')).toHaveValue("");
    expect(
      await page.isChecked('[name="evenement[accesAbonnesDefautUniquement]"]')
    ).toBeFalsy();
  });

  test("Ajout valide avec un seul choix de type", async ({ page }) => {
    await loginAndCheckPage(page, "amap");

    await getButtonNew(page).click();
    await fillFormValideAndCheck(page);
  });

  test("Ajout valide avec plusieurs choix de type", async ({ page }) => {
    await loginAndCheckPage(page, "admin_aura");

    await getButtonNew(page).click();
    await page
      .locator('[name="evenement_creation_select_type[type]"]')
      .selectOption("Région");
    await page.getByRole("button", { name: "Suivant" }).click();
    await select2(page, "evenement[related]", "AUVERGNE-RHÔNE-ALPES");
    await fillFormValideAndCheck(page);
  });

  test("Validation champs vides", async ({ page }) => {
    await gotoForm(page);

    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await expect(
      page.locator('[name="evenement[titre]"] ~ .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page.locator('[name="evenement[description]"] ~ .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(
      page.locator('[name="evenement[descriptionCourte]"] ~ .alert-danger')
    ).toContainText("Cette valeur ne doit pas être vide.");
    await expect(page.locator(".alert-danger")).toHaveCount(3);
  });

  test("Validation taille max des champs", async ({ page }) => {
    await gotoForm(page);

    await page.locator('[name="evenement[titre]"]').fill("a".repeat(151));

    // This test trigger error message but not test exactly the length of the field
    // As the note editor add html tags
    test.setTimeout(120000); // Increase timeout caused by long typing
    await fillNoteEditor(page, "evenement[description]", "a".repeat(10001));
    await fillNoteEditor(page, "evenement[descriptionCourte]", "a".repeat(250));

    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await expect(
      page.locator('[name="evenement[titre]"] ~ .alert-danger')
    ).toContainText(
      "Cette chaîne est trop longue. Elle doit avoir au maximum 150 caractères."
    );
    await expect(
      page.locator('[name="evenement[description]"] ~ .alert-danger')
    ).toContainText("La description est trop longue");
    await expect(
      page.locator('[name="evenement[descriptionCourte]"] ~ .alert-danger')
    ).toContainText("La description est trop longue");
  });
});
