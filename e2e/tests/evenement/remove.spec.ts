import { test, expect, Page } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";
import { fillNoteEditor, select2 } from "../../support/shorcut_actions";
import { createNewEvtAmap } from "../../support/shorcut";

test.describe("Comme utilisateur, je devrais pouvoir supprimer une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Supprimer une actualité", async ({ page }) => {
    await createNewEvtAmap(page, "Nouvelle actualité");
    await page.locator(".glyphicon-trash").click();
    await page.locator(".modal.in").getByText(/OUI/).click();

    await flashMessageShouldContain(page, "Actualité supprimée");
    await expect(page.locator("h3")).toContainText("Fil d'actualités");
    await expect(page.locator(".block-evenement-titre")).toHaveCount(0);
  });
});
