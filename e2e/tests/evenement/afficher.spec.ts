import { test, expect } from "@playwright/test";
import { requestResetDb } from "../../support/shorcut_request";
import { login } from "../../support/shorcut_auth";
import { fillNoteEditor } from "../../support/shorcut_actions";
import { flashMessageShouldContain } from "../../support/shorcut_assertions";

test.describe("Comme utilisateur, je devrais pouvoir afficher le détail d'une actualité", () => {
  test.beforeEach(async ({ page }) => {
    await requestResetDb(page);
  });

  test("Afficher une description courte sur l'accueil et la description longue sur les détails", async ({
    page,
  }) => {
    await login(page, "amap");
    await page.getByRole("link", { name: "Ajouter une actualité" }).click();
    await page.locator('[name="evenement[titre]"]').fill("Nouvelle actualité");
    await fillNoteEditor(
      page,
      "evenement[description]",
      "Description de l'évènement"
    );
    await fillNoteEditor(
      page,
      "evenement[descriptionCourte]",
      "Description courte de l'évènement"
    );
    await page.getByRole("button", { name: "Sauvegarder" }).click();
    await flashMessageShouldContain(page, "L'actualité a bien été ajoutée");

    await expect(page.locator(".block-evenement-description")).toHaveText(
      "Description courte de l'évènement"
    );
    await expect(page.locator(".block-evenement-description")).not.toHaveText(
      "Description de l'évènement"
    );
    await page.getByRole("link", { name: "Voir plus" }).click();

    await expect(page.locator(".container")).toContainText(
      "Description de l'évènement"
    );
    await expect(page.locator(".container")).not.toContainText(
      "Description courte de l'évènement"
    );
  });
});
