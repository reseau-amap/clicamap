<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('libraries/htmlpurifier-4.10.0')
    ->exclude('libraries/tcpdf')
    ->exclude('third_party')
    ->exclude('logs')
    ->exclude('writable')
    ->in(__DIR__ . '/application')
    ->in(__DIR__ . '/PsrLib')
    ->in(__DIR__ . '/tests')
;

$config = new PhpCsFixer\Config();
return $config
    ->setRules([
        '@PSR1' => true,
        '@PSR2' => true,
        '@PSR12' => true,
        '@PhpCsFixer' => true,
        '@Symfony' => true,
        '@PHP80Migration:risky' => true,
        '@PHP81Migration' => true,
        'array_syntax' => ['syntax' => 'short'],
        'no_alternative_syntax' => false,
        'phpdoc_to_comment' => false,
        'echo_tag_syntax' => ['format' => 'short'],
    ])
    ->setFinder($finder)
;
