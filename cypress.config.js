const { defineConfig } = require("cypress");
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin')
const { readPdf } = require('./cypress/scripts/readPdf');

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      on('task', {downloadFile, readPdf});
    },
    experimentalStudio: true,
    projectId: "projectId",
    baseUrl: 'http://localhost',
    video: false
  },
  env: {
    MAILHOG_HOST: 'http://localhost:8025',
    SMOCKER_HOST: 'http://localhost:8081',
  },
});
